import sys
import os

def make_command_cross_platform(cmd):


    """
    function converts paths to cross-readable paths adding slash in start of the path
    samples:
        python render.py /loco000/tools/... -> //loco000/tols/...
        python render.py "/alpha/..." -> "//alpha/..."
    """

    TOOLS_DRIVE1 = 'y:/'
    TOOLS_DRIVE2 = 'Y:/'
    TOOLS_DIR = '//loco000/tools/'
    #PRVZ_TOOLS_PATH_ABS = os.getenv('PRVZ_TOOLS_PATH', '').lower().replace(TOOLS_DRIVE1, TOOLS_DIR).replace(TOOLS_DRIVE2, TOOLS_DIR)
    #PRVZ_TOOLS_PATH_CUSTOM_ABS = os.getenv('PRVZ_TOOLS_PATH_CUSTOM', '').lower().replace(TOOLS_DRIVE1, TOOLS_DIR).replace(TOOLS_DRIVE2, TOOLS_DIR)
    cmd = cmd.replace('\\', '/').replace(TOOLS_DRIVE1, TOOLS_DIR).replace(TOOLS_DRIVE2, TOOLS_DIR)


    print ('>>> Make_command_cross_platform by {0} '.format(__file__))
    new_cmd = ''
    for n, ch in enumerate(cmd):
        if n == 0:
            if ch == '/':
                if cmd[n+1] == '/':
                    new_cmd += ch
                    continue
                else:
                    new_cmd += (ch + '/')
                    continue
            else:
                new_cmd += ch
                continue

        if 0 < n < len(cmd):
            if ch == '/':
                if (ch == '/') and ((cmd[n - 1] == '/') or (cmd[n + 1] == '/')):
                    new_cmd += ch
                    continue
                elif cmd[n - 1] in ['\"', " "]:
                    new_cmd += (ch + '/')
                    continue
            else:
                new_cmd += ch
                continue
        new_cmd += ch

    print ('Old command: {0}'.format(cmd))
    print ('New command: {0}'.format(new_cmd))
    return new_cmd

#sample = r'python /loco000/tools/tools28d/custom/patrol/ass_deferred.py -r arnold -proj "/alpha/patrol/2_prod/ep777/sh0101" -rt 1 -rl defaultRenderLayer -ai:bass 1 -ai:lve 1 -ai:sppg "$ARNOLD_PLUGIN_PATH" -ai:sppr "" -ai:spsh "" -ai:sptx "" -s 1 -e 10 -b 1 "/alpha/patrol/2_prod/ep777/sh0101/work/ep777_sh0101_render_v444_deferred.ma"'
#sample = '/alpha/patrol/2_prod/ep777/sh0101/work/ep777_sh0101_render_v444_deferred.ma'

#print make_command_cross_platform(sample)


def append_sys_path_all_from(folder='', abspath=None):
    """
    add all dirs under `folder` to sys.path if any .py files are found.
    Use an abspath if you'd rather do it that way.

    Uses the current working directory as the location of using.py.
    Keep in mind that os.walk goes *all the way* down the directory tree.
    With that, try not to use this on something too close to '/'

    """
    add = set(sys.path)
    if abspath is None:
        cwd = os.path.abspath(os.path.curdir)
        abspath = os.path.join(cwd, folder)
    for root, dirs, files in os.walk(abspath):
        for f in files:
            if f[-3:] in '.py':
                add.add(root)
                break
    for i in add:
        sys.path.append(i)

# >>> import using, sys, pprint
# >>> using.all_from('py') #if in ~, /home/user/py/
# >>> pprint.pprint(sys.path)