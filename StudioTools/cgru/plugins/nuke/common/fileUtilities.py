import re
import glob
import os
import json
import sys
import subprocess


def openBrowser(path):
    diropen = os.path.dirname(path)
    diropen = os.path.normpath(diropen)
    if sys.platform=='win32':
        try:
            #subprocess.Popen(['start', diropen], shell= True)
            os.system('start %s ' % diropen)
        except Exception as err:
            print ('ERROR: open broser %s"  %s' % err)
        #os.system('start %s ' % diropen)
    elif sys.platform=='darwin':
        subprocess.Popen(['open', diropen])
    else:
        try:
            subprocess.Popen(['xdg-open', diropen])
        except OSError:
            print ('OS error open %s' % diropen)




def createDirectory(filepath):
    directory = os.path.dirname(filepath)
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
            print ('INFO: "%s" has been created.' % directory)
            return directory
        except Exception as err:
            print('ERROR: createDirecory() %s' % err)
            return None
    return directory





def increaseCounter(counter='000'):
    digit_rex = '\d+'
    if not re.match(digit_rex, counter):
        print ('ERROR: counter must be digits')
        return
    int_counter = int(counter)
    int_counter+=1
    res = (len(counter) - len(str(int_counter)))*'0' + str(int_counter)
    return res

def getLatestVersion(pathmask):
    # sample:
    # getLatestVersion(r"\\alpha\patrol\2_prod\ep777\sh0777\work\ep777_sh0777_anim_v???.ma")


    pathmaskDir = os.path.dirname(pathmask)
    if not os.path.lexists(pathmaskDir):
        print ('WARNING: "%s" directory not exists' % pathmaskDir)
        return

    if not '?' in pathmask:
        print ('ERROR: not correct mask. No one "?" simbol in mask')
        return

    files = glob.glob(pathmask)
    if not files:
        print ('Not found for mask "%s"' % pathmask)
        return
    #print files
    latestVersion = sorted(files)[-1].replace('\\', '/')
    #print 'sorted', latestVersion
    return latestVersion


def getLatestGroupVersion(pathmask):
    # sample:
    # getLatestGroupVersion(r"\\alpha\patrol\2_prod\ep777\sh0777\work\ep777_sh0777_*_v???.ma")
    # return last versioned files group
    print ('getLatestGroupVersion starts...')
    pathmaskDir = os.path.dirname(pathmask)
    if not os.path.lexists(pathmaskDir):
        print ('ERROR: "%s" directory not exists' % pathmaskDir)
        return

    if not '?' in pathmask:
        print ('ERROR: not correct mask. No one "?" simbol in mask')
        return

    # find any file with last index
    any_last = getLatestVersion(pathmask)
    if any_last is None:
        print ('Not found any last for mask "%s"' % pathmask)
        return

    #print ('Any last = %s' % any_last)
    padding = pathmask.count('?')
    #print padding

    rex = pathmask.replace('?' * padding, '(?P<version>' + '\\d' * padding + ')').replace('*', '(.*)')
    #print 'Rex = %s' % rex
    dictr = re.match(rex, any_last).groupdict()
    if dictr:
        #print dict
        pathmask = pathmask.replace('?' * padding, dictr['version'])
        print ('getLatestGroupVersion latest search mask = "%s"' % pathmask)
    else:
        print ('ERROR: getLatestGroupVersion: empty dict')
        return
    files = glob.glob(pathmask)
    if not files:
        print ('Not found for mask "%s"' % pathmask)
        return
    #print files
    return files


def getLatestVersionPlusOne(pathmask):
    # sample:
    # getLatestVersionPlusOne(r"\\alpha\patrol\2_prod\ep777\sh0777\work\ep777_sh0777_anim_v???.ma")
    pathmaskDir = os.path.dirname(pathmask).replace('\\', '/')
    if not os.path.lexists(pathmaskDir):
        print ('ERROR: "%s" directory not exists' % pathmaskDir)
        return

    if not '?' in pathmask:
        print ('ERROR: not correct mask. No one "?" simbol in mask')
        return

    #pathmask = pathmask.replace('\\', '/')
    #pathmaskBasename = pathmask.split('/')[-1]
    pathmaskBasename = os.path.basename(pathmask)
    padding = pathmask.count('?')

    files = glob.glob(pathmask)
    if not files:
        numplus = (padding-1)*'0' + '1'
        latestVersionPlus = pathmask.replace(('?' * padding), numplus)
        print ('First version = %s' % latestVersionPlus)
        return latestVersionPlus
    latestVersionFullPath = sorted(files)[-1].replace('\\', '/')
    latestVersion = os.path.basename(latestVersionFullPath)
    print ('Latest file = %s' % latestVersion)
    num =''
    for i, ch in enumerate(latestVersion):
        if not ch == pathmaskBasename[i]:
            num+=ch
    numplus = increaseCounter(num)
    #print numplus
    if numplus:
        latestVersionPlus = pathmask.replace(('?' * padding), numplus)
        print ('Latest+1 file = %s' % latestVersionPlus)
        return latestVersionPlus



def getDictFromJson(filename):
    dict = None
    try:
        fh=open(filename)
        dict = json.load(fh)
    except Exception as err:
        print('ERROR getDictFromJson "%s": %s' % (filename, err))
        return
    finally:
        if fh is not None:
            fh.close()
    return dict


def maya_asci_iterator(path):
    '''

    Args:
        path: path to ASCI file

    Returns:
        line iterator
    '''
    try:
        fh = open(path)
        res = ''
        for line in fh:
            line = line.strip()
            if line.startswith('//'):
                yield line
            else:
                res = res + ' ' + line
                if line.endswith(';'):
                    yield res.strip()
                    res = ''
    except (IOError, OSError) as err:
        print(err)
    finally:
        if fh is not None:
            fh.close()


def get_all_reference_assets(filepath, verbose=False):
    res = []
    if verbose:
        print ' ------------ Search in %s ------------------ ' % filepath
    pattern_search_abs = re.compile(r'.*(/|//|\\\\)(alpha|Alpha|ALPHA).*/0_assets/.*')
    pattern_search_abs_substring = re.compile(r'.*(?P<path>(\"/|//|\\\\)(alpha|Alpha|ALPHA).*/0_assets/.*_rig.ma).*')
    pattern_search_rel = re.compile(r'.*(%root%|%assets%).*_rig\.ma.*')
    pattern_search_rel_substring = re.compile(r'.*(?P<path>(%root%|%assets%).*_rig\.ma).*')
    if os.path.exists(filepath):
        iterator = maya_asci_iterator(filepath)
    else:
        raise AssertionError("File not exists : %s" % filepath)
    for line in iterator:
        if line.startswith('file -rdi') or line.startswith('file -r'):
            # if True:
            if pattern_search_abs.match(line):
                print '>> ERROR: Absolute path found in "%s" %s' % (os.path.basename(filepath), line)
                return
                #exit()
            if pattern_search_rel.match(line):
                if verbose:
                    print '>> FOUND CORRECT ASSETS: %s' % line
                os.environ['PRVZ_PROJECT_PATH'] = '//alpha/patrol'
                path = pattern_search_rel_substring.match(line).groupdict()['path']
                path = convert_paths_absolute(path, verbose=verbose)
                directory = os.path.dirname(path)
                directory3d = os.path.dirname(directory) #!up
                res.append(directory3d.lower().replace('/', '\\')) # FCKN MUSTDIE
        if line.startswith('requires'):
            if verbose:
                print ' ------------ End Search in %s ------------------ ' % filepath
            return list(set(res))

def convert_paths_absolute(path, verbose=False):
    """

    Args:
        path: path to data
    Returns: absolute path %root%

    """
    new_path = path
    if '%root%' in path:
        new_path = path.replace('%root%', os.environ['PRVZ_PROJECT_PATH'])
        if verbose:
            print (' convert_paths_absolute... Converted path %s >>> %s' % (path, new_path))
        return new_path

    if '%assets%' in path:
        # convert to %root% relative:
        new_path = path.replace('%assets%', '%root%/0_assets')
        new_path = new_path.replace('%root%', os.environ['PRVZ_PROJECT_PATH'])
        if verbose:
            print (' convert_paths_absolute... Converted path %s >>> %s' % (path, new_path))
    if verbose:
        print (' convert_paths_absolute... OK. Converting skiped >>> %s' % new_path)
    return new_path

'''
Create a nested dictionary from os.walk (Python recipe)
import os

def get_directory_structure(rootdir):
    """
    Creates a nested dictionary that represents the folder structure of rootdir
    """
    dir = {}
    rootdir = rootdir.rstrip(os.sep)
    start = rootdir.rfind(os.sep) + 1
    for path, dirs, files in os.walk(rootdir):
        folders = path[start:].split(os.sep)
        subdir = dict.fromkeys(files)
        parent = reduce(dict.get, folders[:-1], dir)
        parent[folders[-1]] = subdir
    return dir
'''
