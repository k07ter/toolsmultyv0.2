'''
last edit:
07.05.2016, robocop

type: tool script
This Tool script for assigning stereoscopic rig to existing camera.
'''


import nukescripts
import nuke
import os.path

def getNewName(oldName):
   i = 1
   while True:
       if nuke.exists(oldName + '_' + str(i) ) == False:
           return oldName + '_' + str(i)
       i += 1
       
def s3d_stereoRig(): 
    # check current scene for stereo views
    if not nuke.views() == ['left','right']: 
        ModalFramePanel('WARNING','No "left\right" stereo views found,\nplease setup default "stereo views" in composition global settings\n Script works only with "left" and "right" views names, not with "R" and "L" or\n something like that.').showModalDialog()
    else :
        camera=nuke.selectedNode()
        
        # Create planes .
        trans_name = 's3d_rig_transform'
        nearPlane_name = 'NearPlane_s3d'
        farPlane_name = 'FarPlane_s3d'
        scene_name = 's3d_rig_scene'
        
        # now check that new tol name is exclusive. In other case just rename it.
        if nuke.exists(trans_name): 
            trans_name = getNewName(trans_name)
        if nuke.exists(nearPlane_name): 
            nearPlane_name = getNewName(nearPlane_name)
        if nuke.exists(farPlane_name): 
            farPlane_name = getNewName(farPlane_name)
        if nuke.exists(scene_name): 
            scene_name = getNewName(scene_name)
                
        
        
        
        mycamPos = [camera.xpos() , camera.ypos()]
        mytransform = nuke.nodes.TransformGeo(xpos = mycamPos[0]+100, ypos = mycamPos[1]+20, name = trans_name)
        myScene = nuke.nodes.Scene(xpos = mytransform.xpos(), ypos = mytransform.ypos()-100, render_mode=0, name = scene_name)
        myNearPlane = nuke.nodes.Card(xpos = myScene.xpos(), ypos = myScene.ypos()-50, render_mode=0, name = nearPlane_name)
        myFarPlane = nuke.nodes.Card(xpos = myScene.xpos()+100, ypos = myScene.ypos()-50, render_mode=0, name = farPlane_name)        
        
        # connect them
        mytransform.setInput(1,camera)
        mytransform.setInput(0,myScene)
        myScene.setInput(0,myNearPlane)
        myScene.setInput(1,myFarPlane)
        
        #==== create custom knob for stereo rig math
        interaxial_knob = nuke.Double_Knob("interaxial",'Interaxial')
        zeroParallax_knob = nuke.Double_Knob("zeroParallax",'Zero Parallax')
        filmbackOffset_knob = nuke.Double_Knob("filmbackOffset",'Filmback offset')
        NearPlane_dist_knob = nuke.Double_Knob("NearPlane_dist",'Near Plane distance')
        FarPlane_dist_knob = nuke.Double_Knob("FarPlane_dist",'Far Plane distance')
        NearPlane_prlx_knob = nuke.Double_Knob("NearPlane_prlx",'Near plane parallax (pix)')
        FarPlane_prlx_knob = nuke.Double_Knob("FarPlane_prlx",'Far plane parallax (pix)')
        HorizontalResolution_knob = nuke.Double_Knob("HorizontalResolution",'Out Horizontal Resolution')
        ApertureW_knob = nuke.Double_Knob("ApertureW",'Cam horizontal aperture')
        
        
        #safeNegativeParallaxLimit_knob = nuke.Double_Knob("safeNegativeParallaxLimit",'Safe NegativeParallax Limit (pixels)')
        #safeNegativeParallaxDistance_knob = nuke.Double_Knob("safeNegativeParallaxDistance",'Safe Negative Parallax Distance')
        #interaxial_knob2 = nuke.Double_Knob("interaxial2",'Interaxial2')
        #zeroParallax_knob2 = nuke.Double_Knob("zeroParallax2",'Zero Parallax2')
        camera.addKnob(interaxial_knob)
        camera.addKnob(zeroParallax_knob)
        camera.addKnob(filmbackOffset_knob)
        camera.addKnob(NearPlane_dist_knob)
        camera.addKnob(FarPlane_dist_knob)
        camera.addKnob(NearPlane_prlx_knob)
        camera.addKnob(FarPlane_prlx_knob)
        camera.addKnob(HorizontalResolution_knob)
        camera.addKnob(ApertureW_knob)
        
        myNearPlane['translate'].setValueAt(-2,2)   # set z coordinate value
        myFarPlane['translate'].setValueAt(-6,2)    # set z coordinate value
        myNearPlane['translate'].setExpression('0',0)   # freeze coordinate from accidental move
        myNearPlane['translate'].setExpression('0',1)   # freeze coordinate from accidental move
        myFarPlane['translate'].setExpression('0',0)    # freeze coordinate from accidental move
        myFarPlane['translate'].setExpression('0',1)    # freeze coordinate from accidental move
        
        camera['ApertureW'].setExpression('haperture')
        camera['NearPlane_prlx'].setValue(-10)
        camera['FarPlane_prlx'].setValue(25)
        camera['HorizontalResolution'].setValue(2048)
        camera['NearPlane_dist'].setExpression('-{0}.translate.z'.format(myNearPlane.name()))
        camera['FarPlane_dist'].setExpression('-{0}.translate.z'.format(myFarPlane.name()))
        camera['filmbackOffset'].setExpression('((focal/10*interaxial)/(2*zeroParallax))/(haperture/10)')
        camera['interaxial'].setExpression('-NearPlane_dist * NearPlane_prlx *ApertureW / HorizontalResolution * zeroParallax / (focal *zeroParallax - focal *NearPlane_dist)')
        camera['zeroParallax'].setExpression('NearPlane_dist * FarPlane_dist * (NearPlane_prlx *ApertureW / HorizontalResolution -FarPlane_prlx  *ApertureW / HorizontalResolution) / (NearPlane_dist*NearPlane_prlx *ApertureW / HorizontalResolution - FarPlane_dist*FarPlane_prlx *ApertureW / HorizontalResolution)')
        
        camera['win_translate'].splitView('right')
        camera['win_translate'].setExpression('filmbackOffset*2', 0,view='left')
        camera['win_translate'].setExpression('-filmbackOffset*2', 0,view='right')
        camera['translate'].splitView('right')
        camera['translate'].setExpression('-interaxial/2',0,view='left')   # set left camera interaxial
        camera['translate'].setExpression('interaxial/2',0,view='right') # set right camera interaxial
        
'''turn off <limit plane> creation
# Create plane that show stereo limits for nearest point.
mycamPos = [camera.xpos() , camera.ypos()]
mytransform = nuke.nodes.TransformGeo(xpos = mycamPos[0]+100, ypos = mycamPos[1]+20)
myCard = nuke.nodes.Card(xpos = mycamPos[0]+100, ypos = mycamPos[1]-20, render_mode=0)
myText = nuke.nodes.Text(xpos = mycamPos[0]+100, ypos = mycamPos[1]-40,message = 'STEREO LIMIT', xjustify=1, yjustify=2, invert=1,font = r'C:/Windows/Fonts/arial.ttf',size =200)

mytransform.setInput(1,camera)
mytransform.setInput(0,myCard)
myCard.setInput(0,myText)
myText['color'].setValue([0.9, 0.6, 1.0, 1.0])
myText['box'].setValue(((camera.format().width(), 0.0, 0.0, camera.format().height())))
'''

#camera['interaxial'].setExpression('sqrt(pow(translate.left.x-translate.right.x,2)+pow(translate.left.y-translate.right.y,2)+pow(translate.left.z-translate.right.z,2))')
#camera['zeroParallax'].setExpression('win_translate==0?1:abs(interaxial*focal/(win_translate*haperture))')
#camera['safeNegativeParallaxDistance'].setExpression('1.0/(safeNegativeParallaxLimit*haperture/(width*focal*interaxial)-1.0/zeroParallax)')
#myCard['z'].setExpression('-{camName}.safeNegativeParallaxDistance'.format(camName=camera.name()))

#camera['pivot'].splitView('right')
#camera['pivot'].setExpression('interaxial/2',0,view='left')   # set left camera pivot
#camera['pivot'].setExpression('-interaxial/2',0,view='right')   # set left camera pivot
#camera['translate'].splitView('right')

#camera['translate'].setExpression('curve-interaxial/2',0,view='left')   # set left camera interaxial
#camera['translate'].setExpression('curve+interaxial/2',0,view='right') # set right camera interaxial
#camera['win_translate'].splitView('right')
#camera['win_translate'].setExpression('interaxial*focal/(zeroParallax*haperture)', 0,view='left')
#camera['win_translate'].setExpression('-interaxial*focal/(zeroParallax*haperture)',0,view='right')
#camera['win_translate'].setExpression('-win_translate.left.u',0,view='right')


#ModalFramePanel('jogi-jogi','Import stereo camera: succesefull').showModalDialog()
