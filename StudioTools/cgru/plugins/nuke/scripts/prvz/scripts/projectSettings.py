#! /usr/bin/env python
# emacs-mode: -*- python-*-
# -*- coding: utf-8 -*-
# Code writer E. Melkov (Parovoz studio,  Moscow 2015)
#===============================================================================

import os 
#import re 
import nuke 
#import nukescripts
import json
import projectUtilities as pu

def init():

    # get project path
    try:
        projPath = os.getenv('PRVZ_PROJECT_PATH')
        print('projectSettings.init(): PRVZ_PROJECT_PATH = %s' % projPath)
    except Exception as err:
        print('projectSettings.init(): ERROR getting PRVZ_PROJECT_PATH failed: %s' % err)
        return
    #nukeSettingsPath = os.path.join(os.getenv('PRVZ_PROJECT_SETTINGS_PATH'), '.settings', '.nuke')
    nukeSettingsPath = os.path.join('\\\\loco000', 'tools', 'tools28', 'projects_ini', 'projects_settings', '.default', '.settings', '.nuke')
    print(nukeSettingsPath)

    if os.path.isdir(nukeSettingsPath):
        nuke.pluginAddPath(nukeSettingsPath)
    settings_json_file = os.path.join(nukeSettingsPath, 'settings.json')
    print(settings_json_file, nukeSettingsPath)

    dict_settings = pu.getDictFromJson(settings_json_file)

    if not dict_settings is None:
        favs = dict_settings['favorites']
        for ftype in favs:
            for path in favs[ftype]:
                favpath = '%s/%s' % (projPath, path)
                #favpath = os.path.join(projPath, path)
                if os.path.isdir(favpath):
                    #nuke.addFavoriteDir(ftype, favpath, nuke.SCRIPT)
                    cmd = 'nuke.addFavoriteDir(favpath, favpath, nuke.%s)' % ftype.upper() #fist favpath create name of fav
                    exec(cmd)
                    #nuke.addFavoriteDir(ftype, favpath, nuke.IMAGE)
                    print ('Added favorites "%s" : "%s"' % (favpath, favpath))

        formats =  dict_settings['formats']
        for frmt in formats:
            if not frmt.split()[-1] in [f.name() for f in nuke.formats()]:
                #formatForFknNuke = str(frmt)
                try:
                    nuke.addFormat(frmt)
                    print ('Added format "%s"' % (frmt))
                except Exception as err:
                    print ('Error added format %s' % err)

