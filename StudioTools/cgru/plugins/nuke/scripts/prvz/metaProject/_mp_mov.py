


def Shots_list(ep_ren):    # sq_N , sq_L
    import os,re
    sq_N = []
    sq_L = {}
    try:

        SSS = os.listdir( ep_ren )
        for i in SSS :
            if re.match('.*(sh[0-9][0-9][0-9]).*',i):
                s = i.split('sh')[1][-1]
                if re.match('.*[0-9].*', s ):
                    sq_L[i[:5]] = []    
                else:
                    sq_L[i[:6]] = []

        sq_N = sq_L.keys()
        sq_N.sort()

    except : s=1        
    return sq_N , sq_L
                    # ep_ren = '//alpha/prj/princess/post/ep01' # sceni isxodniki rendera
                    # sq_N = [ sq83 , sq84 , sq85... ]
                    # sq_L = { sq83:['sh001','sh002','sh003','sh004',],... }        
                    # sq_N , sq_L


def Scripts_list( ep_comp , N_EP , Shot ): # return Scrip_list , versMax, _list_
    import os,re

    _list_ = []         
    Scrip_list = []     
    versMax = 0
    numV_n = []  
    Scr_files = ''
    try:
        
        Scr_files =  ep_comp[0] +'/'+  Shot +'/'+ ep_comp[1]

        _list_ = filter( lambda i: i.endswith('.nk') , os.listdir( Scr_files ) )   
        #Scrip_list = [ i for i in _list_ if re.match( '.*' + Shot + '.*' , i ) ]                    
        Scrip_list = [ i for i in _list_ if re.match( '.*' + Shot + '.*' , i ) if not re.match( '.*' + 'Write' + '.*' , i ) ]                    
        Scrip_list.sort()
    #select max vers
        if len(Scrip_list) == 1 :
            versMax = 0
        else :
            numV_n = max( [ '_v'+ i.split('_v')[1] for i in Scrip_list if re.match('.*(_v[0-9][0-9][0-9]).*',i) ])

            versMax = Scrip_list.index( max( [i for i in Scrip_list if re.match('.*'+ numV_n+'.*',i)] )) 
            
            #numV_n =[ i.split('_v')[1] for i in Scrip_list ]                
            #versMax = numV_n.index( max( numV_n )) 

    except : Scrip_list = ['no nk '+ Shot +'_______________']

    return Scrip_list , versMax , _list_ , Scr_files
                        # Shot = 'sh002'
                        # ep_comp =  ['//server/edge/fixies_serial/composing/115_koleso' ,  'comp' ] #compoz papki
                        # _list_  = all nuke (.nk) files [ep001_sc001_v001_.nk , ep001_sc002_v001_.nk , _sc003_v001_.nk , ... ]
                        # Scrip_list = only name have 'sc' nuke [ ep001_sc001_v001_.nk , ep001_sc001_v002_.nk , ep001_sc001_v003_.nk ]                    
                        # Scr_files = '//alpha/prj/princess/post/ep01/sq00/sh000/comp /
                        # return Scrip_list , versMax, _list_


def Show_scr_out( Ep_out , Shot , scale , error , versMax ): # return Scr_Imag , scr_out_file 
    import os,re
    Imag = ['<img src="','" width="'+str(120*scale)+'" height="'+str(60*scale)+'" />']
    scr_out_file = ''
    try:
        path = Ep_out[0] +'/'+  Shot +'/'+ Ep_out[1]
        scr_out_foder = path +'/'+ versMax
        scr_out = os.listdir(scr_out_foder)
        scr_out_file = scr_out_foder +'/'+ scr_out[ len(scr_out)/2 ]

        Scr_Imag = Imag[0]+ scr_out_file  +Imag[1]
    except : Scr_Imag = Imag[0]+ error +Imag[1]
       
    return Scr_Imag , scr_out_file 

                    # Ep_out =  '//server/edge/fixies_serial/output/render/115_koleso'
                    # Script= 'ep115_sc031_v01.nk'
                    # error = '//SERVER/tools/nuke/metaProject/Terror-Night.jpeg'
                    # scale =  1 #scale images N --- (150x75)*N
                    # scr_out_foder = '//server/edge/fixies_serial/output/render/115_koleso/ep115_sc031'
                    # return Scr_Imag , scr_out_file 



def open_nk(Scr_files , Script_nk):
    import nukescripts , nuke
    print Scr_files +'/'+ Script_nk   
    #nuke.scriptName() 
    nuke.scriptOpen( Scr_files +'/'+ Script_nk ) 


def tmp():
    import os,re, nukescripts ,nuke
    try:
        nuke.scriptName() 
    except:
        if os.path.isfile(os.getenv('NUKE_TEMP_DIR') + '/tmp.nk'):
            os.remove(os.getenv('NUKE_TEMP_DIR') + '/tmp.nk')
        nuke.scriptSaveAs(os.getenv('NUKE_TEMP_DIR') + '/tmp.nk')    



def Ep_Sc_File_( proj_file , N_EP):
    #proj_file = project_file[ Ep_choice ] 
    # N_EP = int()
    import os,re,string
    ep_n  = [ proj_file  ,  str( string.zfill( int(N_EP),3 ) )  , str( string.zfill( int(N_EP),3 ) ) ]

    ep_ren  =     ep_n[0][0]  +'/ep'+ ep_n[1] 
    try:
        ep_comp =               [ [ ep_n[0][1] + '/'+ i for i in os.listdir( ep_n[0][1] ) if re.match('.*' + ep_n[1] + '.*', i) ][0]    , 'comp' ]
    except: ep_comp =           [ ep_n[0][1] , 'comp']
    try:
        ep_out  =               [ [ ep_n[0][2] + '/'+ i for i in os.listdir( ep_n[0][2] ) if re.match('.*' + ep_n[1] + '.*', i) ][0]    , 'out' ]
    except: ep_out =            [ ep_n[0][2] , 'comp/out' ]
    
    return ep_n , ep_ren , ep_comp , ep_out 



def newScript( proj_file , ep , sh ):
    import os, re, nukescripts, nuke
    name = ep +'_'+ sh +'_comp_v001'
    file = proj_file +'/'+ ep +'/'+ sh +'/comp'
    for i in os.listdir(file):
        if re.match('.*' + name + '.*',i):
            return 'imeet'
    b = open(file +'/'+name+'.nk','w')
    b.write('Root {\n fps 25\n format "1920 1080 0 0 1920 1080 1 HD_1080"\n proxy_type scale\n proxy_format "1024 778 0 0 1024 778 1 1K_Super_35(full-ap)"\n}\nViewer {\n frame 1\n fps 25\n name Viewer1\n selected true\n xpos 0\n ypos 0\n}')
    b.close()
    
    return 'pusta'
