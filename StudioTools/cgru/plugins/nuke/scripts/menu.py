import os

# Add CGRU to main window menu:
cgru_menu = nuke.menu('Nuke').addMenu('CGRU')

cgru_menu.addCommand('RULES', 'cgru.rulesOpenShot()', 'F12')
cgru_menu.addCommand('Afanasy Node', 'nuke.createNode("afanasy")', 'F10')
cgru_menu.addCommand('Dailies Node', 'nuke.createNode("cgru_dailies")', 'F7')
#cgru_menu.addCommand('Dailies Node (Mod)', 'nuke.createNode("cgru_dailies_mod")')
cgru_menu.addCommand('Render Selected...', 'cgru.render()', 'F11')

cgru_submenu = cgru_menu.addMenu("Paths")
cgru_submenu.addCommand('Cut Working Directory', 'cgru.pathcurdir.cut()')
cgru_submenu.addCommand('Add Working Directory', 'cgru.pathcurdir.add()')
cgru_submenu.addCommand("-", "", "")
cgru_submenu.addCommand('Open Translated', 'cgru.pmOpenTranslated()')

cgru_menu.addCommand("-", "", "")

cgru_menu.addCommand('Documentation', 'cgru.docsNuke()')

# Add afanasy gizmo to nodes:
icons = os.path.join(os.environ['CGRU_LOCATION'], 'icons')
nuke.menu('Nodes').addMenu('CGRU', icon=os.path.join(icons, 'keeper.png'))
nuke.menu('Nodes').addCommand('CGRU/afanasy', 'nuke.createNode("afanasy")',
							  icon=os.path.join(icons, 'afanasy.png'))
#Baha tools
import browseSelected
#tb = nuke.toolbar( 'Nodes' )

ParMenu = cgru_menu.addMenu( 'Parovoz', 'Parovoz.png' )
ParMenu.addCommand( 'other_mask', 'nuke.createNode( "other_mask" )')
ParMenu.addCommand( 'XYZ Key_01', 'nuke.createNode( "xyzKey_v01" )'  )
ParMenu.addCommand( 'bg_cards', 'nuke.createNode( "bg_cards" )'  )
ParMenu.addCommand( 'fog_mask', 'nuke.createNode( "fog_mask" )'  )
ParMenu.addCommand( 'Pass_contr', 'nuke.createNode( "Pass_contr" )' )
ParMenu.addCommand( 'Pass_Denoise', 'nuke.createNode( "Pass_Denoise" )' )
ParMenu.addCommand( 'Light Rays/BahRay', 'nuke.createNode( "BahRay_v01" )')
ParMenu.addCommand( 'Light Rays/BahRay_Fog', 'nuke.createNode( "BahRay_Fog" )')
ParMenu.addSeparator()
ParMenu.addCommand( 'Open Explorer for Selected', 'browseSelected.browseSelected()', 'ctrl+e' )

# Baha tools
try:
    import animatedSnap3D
    animatedSnap3D.run()
except Exception as ex:
    print('EX: import animatedSnap3D: %s' % ex)

# Stereocamera tools:
import s3d_import_stereoCamera
import s3d_import_stereoCamera_LR
s3d_menu = cgru_menu.addMenu( 'S3D', 's3d.png' )
s3d_menu.addCommand('Import stereo camera', 's3d_import_stereoCamera.s3d_import_stereoCamera()')
s3d_menu.addCommand('Import stereo camera LR', 's3d_import_stereoCamera_LR.s3d_import_stereoCamera_LR()')
