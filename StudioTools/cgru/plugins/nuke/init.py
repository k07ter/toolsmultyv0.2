# -*- coding: utf-8 -*-

import os
import sys

cgru_scripts_path = os.environ['NUKE_CGRU_PATH'] + '\\scripts'
cgru_gizmos_path = os.environ['NUKE_CGRU_PATH'] + '\\gizmos'

sys.path.append(cgru_scripts_path)
sys.path.append(cgru_gizmos_path)

sys.path.append(os.environ['NUKE_CGRU_PATH'] + '\\metaProject')
sys.path.append(os.environ['NUKE_CGRU_PATH'] + '\\scripts')
sys.path.append(os.environ['NUKE_CGRU_PATH'] + '\\icon')
sys.path.append(os.environ['NUKE_CGRU_PATH'] + '\\gizmos')
#sys.path.append(os.environ['NUKE_CGRU_PATH'] + '\\metaProject')
sys.path.append(os.environ['NUKE_CGRU_PATH'] + '\\animatedSnap3D-master' )

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\icon' )
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\scripts' )
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\gizmos' )
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\metaProject' )
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\animatedSnap3D-master' )

sys.path.append('\\\\loco000\\tools\\tools28\\projects_ini')

nuke.pluginAddPath(cgru_scripts_path)
nuke.pluginAddPath(cgru_gizmos_path)

# Baha tools
nuke.pluginAddPath(os.environ['NUKE_CGRU_PATH'] + '/icon')
nuke.pluginAddPath(os.environ['NUKE_CGRU_PATH'] + '/metaProject')
nuke.pluginAddPath(os.environ['NUKE_CGRU_PATH'] + '/animatedSnap3D-master' )

#nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\icon' )
#nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\metaProject' )
#nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\animatedSnap3D-master' )
#nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\scripts' )

nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\icon' )
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\gizmos' )
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\scripts' )
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\metaProject' )
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\scripts\\prvz\\animatedSnap3D-master' )
try:
    import _metaProject
    reload(_metaProject)
except Exception as ex:

    print('EX: import _metaProject: %s' % ex)

#some scripts from cgru
sys.path.append(os.environ['CGRU_LOCATION'] + '/afanasy/python' )
#print(sys.path)

nuke.knobDefault("black_clamp", "false")
nuke.knobDefault("_jpeg_quality", "1")
nuke.knobDefault( 'Shuffle.label', '[value in]' )
nuke.knobDefault( 'Shuffle.note_font', 'Verdana Bold' )
nuke.knobDefault( 'ShuffleCopy.label', '[value in]\n[value out]' )
nuke.knobDefault( 'ShuffleCopy.note_font', 'Verdana Bold' )

#import prvz_dailies
#import projectSettings as ps

if not 'PRVZ_PROJECT_PATH' in os.environ.keys():
    os.environ['PRVZ_PROJECT_PATH'] = '\\\\loco000\\tools\\tools28\\projects_ini\\projects_settings\\.default'
    print ('WARNING: Set to default project : PRVZ_PROJECT_PATH = %s' % os.environ['PRVZ_PROJECT_PATH'])

try:
    ps.init()
except Exception as err:
    print ('%s: Exception: %s' % (__file__, err))
