import nuke
import os, re
import string
import nukescripts
import _mp_mov
import M_Scripts

# import createExrCamRenderMan

# NUKE_TEMP_DIR

class MetaProject(nukescripts.PythonPanel):
    def __init__(self):
        nukescripts.PythonPanel.__init__(self, 'MetaProject', 'com.ohufx.SearchReplace')
        # __Docum
        # \\ALDAN\ftm\DEV\COMPOSITING\sequence\movie
        # self.FFils = 'C:/Users/bzufarov/.nuke/metaProject/'
        # self.dokumum_file = self.FFils+ '_metaProject.txt'
        # self.dokum = open( self.dokumum_file, 'r' ).readlines()
        #
        # self.project_file = {}
        # self.project_key=[]
        # for i in self.dokum:
        #     self.k = i.replace('\\','/').strip().split( ' = ' )
        #     self.project_file[ self.k[0] ] = self.k[1].lower().split( ' <> ' )
        #     self.project_key.append( self.k[0] )
        # #{'Serial': ['//store1/edge/fixies/images', '//server/edge/fixies_serial/composing', '//server/edge/fixies_serial/output/render']}



        self.project_file = {}
        # self.File_Project = 'File_Project' #//alpha/leo/3_post
        self.File_Project = '//omega/moriki-doriki/3_post'
        self.project_key = ['moriki-doriki']

        self.project_file = {self.project_key[0]: [self.File_Project, self.File_Project, self.File_Project]}

        self.FFils = os.path.dirname(os.path.abspath(__file__)).replace('\\', '/') + '/'
        # self.error = os.path.join(self.FFils, 'Terror-Night.jpeg')
        self.error = self.FFils + 'Terror-Night.jpeg'

        # __KNOBS
        self.Ep_choice = nuke.Enumeration_Knob('Ep choice', '', self.project_key)
        self.Reload = nuke.PyScript_Knob('Reload', 'Reload')

        self.Proj_img = nuke.Text_Knob('', '',
                                       '<img src= "' + self.FFils + self.Ep_choice.value() + '.png" width="300" height="100" />')
        self.addKnob(self.Proj_img)

        # self.num_or_menu =      nuke.PyScript_Knob('num or menu','m '+ '<img src= "" width="10" height="7" />')
        self.num_or_menu = nuke.Boolean_Knob('num or menu', '', 1)
        self._EP = nuke.Enumeration_Knob('_EP', '', self.Ep_project_file())
        self.N_EP = nuke.Int_Knob('N_EP', '')
        self.sh_num_or_sh_menu = nuke.Boolean_Knob('sh_num or sh_menu', '', 1)
        self.Shots = nuke.Enumeration_Knob('Shots', '', ['Sh0000'])
        self.Shots_N = nuke.Int_Knob('Shots_N', '')
        self.Scripts = nuke.Enumeration_Knob('Scripts', '', ['Ep00_Sc000_v00__________.nk'])

        self.down_ = nuke.PyScript_Knob('down', '<img src= "' + self.FFils + 'down_.png" width="15" height="7" />')
        self.up_ = nuke.PyScript_Knob('up', '<img src= "' + self.FFils + 'up_.png" width="15" height="7" />')

        self.Open_Sc = nuke.PyScript_Knob('Open Script',
                                          '<img src= "' + self.FFils + 'nk.png" width="40" height="20" />')
        self.Op_Folder = nuke.PyScript_Knob('Open Folder',
                                            '<img src= "' + self.FFils + 'Open Folder.png" width="40" height="20" />')
        self.Create_sh = nuke.PyScript_Knob('Create sh',
                                            '<img src= "' + self.FFils + 'Create sh.png" width="40" height="20" />')
        # self.Create_precomp =        nuke.PyScript_Knob('Create precomp', 'Create precomp')

        self.Op_Render = nuke.PyScript_Knob('Open Render',
                                            '<img src= "' + self.FFils + 'Open Render.png" width="40" height="20" />')
        self.imp_Ren = nuke.PyScript_Knob('imp Ren',
                                          '<img src= "' + self.FFils + 'imp Ren.png" width="40" height="20" />')

        self.imp_Cam = nuke.PyScript_Knob('imp Cam',
                                          '<img src= "' + self.FFils + 'imp Cam.png" width="40" height="20" />')
        # self.imp_Wr =           nuke.PyScript_Knob('imp Wr', '<img src= "' +self.FFils+ 'imp Wr.png" width="40" height="20" />')
        self.Frame_Range = nuke.PyScript_Knob('Fr_Ra',
                                              '<img src= "' + self.FFils + 'Fr_Ra.png" width="40" height="20" />')

        self.Imp_Ca_Re_Il = nuke.PyScript_Knob('Imp_Ca_Re_Il', '<img src= "' +self.FFils+ 'Imp_Ca_Re_Il.png" width="40" height="20" />')
        self.Klon_sc = nuke.PyScript_Knob('Klon_sc', '<img src= "' +self.FFils+ 'Klon sc.png" width="40" height="20" />')

        self.Sc_shot = nuke.Text_Knob('')
        self.Op_Shot = nuke.PyScript_Knob('Open Shot',
                                          '<img src= "' + self.FFils + 'Open Shot.png" width="40" height="20" />')
        self.Op_Shot_Folder = nuke.PyScript_Knob('Op Shot Folder',
                                                 '<img src= "' + self.FFils + 'Op Shot Folder" width="40" height="20" />')

        # __add_KNOBS        self.addKnob(nuke.Tab_Knob('Script'))
        self.addKnob(nuke.Text_Knob(''))
        self.addKnob(self.Ep_choice)
        self.Ep_choice.setVisible(False)
        # self.addKnob(self.Reload)

        


        self.addKnob(self.num_or_menu)        
        
        self.addKnob(self._EP)
        self.addKnob(self.N_EP)
        self.N_EP.setVisible(False)

        self.addKnob(self.sh_num_or_sh_menu)
        self.addKnob(self.Shots)
        self.addKnob(self.Shots_N)
        self.Shots_N.setVisible(False)

        self.num_or_menu.setFlag(nuke.STARTLINE)
        self._EP.clearFlag(nuke.STARTLINE)
        self.N_EP.clearFlag(nuke.STARTLINE)

        self.Shots.clearFlag(nuke.STARTLINE)
        self.Shots_N.clearFlag(nuke.STARTLINE)
        # bb.setFlag(nuke.STARTLINE)
        # bb.clearFlag(nuke.STARTLINE)


        self.addKnob(self.down_)
        self.addKnob(self.up_)

        self.addKnob(self.Scripts)
        self.addKnob(self.Reload)

        # ________
        self.addKnob(nuke.Text_Knob(''))
        self.addKnob(self.Open_Sc)
        self.addKnob(self.Op_Folder)
        self.addKnob(self.Create_sh)
        # self.addKnob(self.Create_precomp)


        self.addKnob(self.imp_Ren)
        self.addKnob(self.Op_Render)

        # ________
        self.addKnob(nuke.Text_Knob(''))
        self.addKnob(self.imp_Cam)
        # self.addKnob(self.imp_Wr)
        self.addKnob(self.Frame_Range)

        self.addKnob(self.Imp_Ca_Re_Il)
        self.addKnob(self.Klon_sc)

        # ________

        self.addKnob(self.Sc_shot)
        self.addKnob(self.Op_Shot)
        self.addKnob(self.Op_Shot_Folder)

        # self.All_Sc =           nuke.PyScript_Knob('all_sc','all_sc')
        self.scale_scr = nuke.Enumeration_Knob('scale scr', '', ['1', '2', '3'])
        # self.menu_out =         nuke.Enumeration_Knob('menu out','',['out+nk','out','out_True','out_False'])



        # self.addKnob(nuke.Text_Knob(''))
        # self.addKnob(self.All_Sc)
        # self.All_Sc.setFlag(nuke.STARTLINE)
        self.addKnob(self.scale_scr)
        # self.scale_scr.clearFlag(nuke.STARTLINE)
        # self.addKnob(self.menu_out )
        # self.menu_out.clearFlag(nuke.STARTLINE)

        


        self.Master_scripts =   nuke.Enumeration_Knob( 'Master_scripts','', ['____________________________'])
        self.imp_mast_scr =     nuke.PyScript_Knob('imp_mast_scr','imp_mast_scr')
        self.addKnob(nuke.Text_Knob(''))
        self.addKnob(self.Master_scripts)
        self.addKnob(self.imp_mast_scr)
        self.M_Sc_list()

        ##########################################################################################################################

        self.end_sel_op()

    def knobChanged(self, knob):

        if nuke.thisKnob().name() == "Ep choice":
            self._EP.setValues(self.Ep_project_file())
            self.N_EP.setValue(int(self._EP.value().split('ep')[1]))

            self.Shots.setValues(self.Sh_list()[0])
            self.Shots.setValue(0)

            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])

            self.Op_Shot.setLabel(self.Show_shot()[0])

            self.M_Sc_list()

        if nuke.thisKnob().name() == "Reload":
            self.Reload_sh()
            self.M_Sc_list()
            # relord shot meta

        ##########################################################################################################################
        if nuke.thisKnob().name() == "num or menu":
            if self.num_or_menu.value() == 1:
                self._EP.setVisible(True)
                self.N_EP.setVisible(False)
                self._EP.setValue('ep' + str(string.zfill(self.N_EP.value(), 3)))
            else:
                self._EP.setVisible(False)
                self.N_EP.setVisible(True)
                self.N_EP.setValue(int(self._EP.value().split('ep')[1]))

        if nuke.thisKnob().name() == "_EP":
            self.N_EP.setValue(int(self._EP.value().split('ep')[1]))
            self.Shots.setValues(self.Sh_list()[0])
            self.Shots.setValue(0)
            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])
            self.Op_Shot.setLabel(self.Show_shot()[0])

            self.M_Sc_list()

        if nuke.thisKnob().name() == "N_EP":
            self._EP.setValue('ep' + str(string.zfill(self.N_EP.value(), 3)))
            self.Shots.setValues(self.Sh_list()[0])
            self.Shots.setValue(0)
            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])
            self.Op_Shot.setLabel(self.Show_shot()[0])

            self.M_Sc_list()

        ##########################################################################################################################
        if nuke.thisKnob().name() == "sh_num or sh_menu":
            if self.sh_num_or_sh_menu.value() == 1:
                self.Shots.setVisible(True)
                self.Shots_N.setVisible(False)
                self.Shots.setValue('sh' + str(string.zfill(self.Shots_N.value(), 3)))
            else:
                self.Shots.setVisible(False)
                self.Shots_N.setVisible(True)
                self.Shots_N.setValue(int(self.Shots.value().split('sh')[1]))

        if nuke.thisKnob().name() == "Shots":
            self.Shots_N.setValue(int(self.Shots.value().split('sh')[1]))
            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])
            self.Op_Shot.setLabel(self.Show_shot()[0])

        if nuke.thisKnob().name() == "Shots_N":
            self.Shots.setValue('sh' + str(string.zfill(self.Shots_N.value(), 3)))
            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])
            self.Op_Shot.setLabel(self.Show_shot()[0])

        if nuke.thisKnob().name() == "down":
            self.Shots.setValue(self.Shots.values().index(self.Shots.value()) - 1)
            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])
            self.Op_Shot.setLabel(self.Show_shot()[0])
            self.Shots_N.setValue(int(self.Shots.value().split('sh')[1]))

        if nuke.thisKnob().name() == "up":
            self.Shots.setValue(self.Shots.values().index(self.Shots.value()) + 1)
            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])
            self.Op_Shot.setLabel(self.Show_shot()[0])
            self.Shots_N.setValue(int(self.Shots.value().split('sh')[1]))

        ##########################################################################################################################
        if nuke.thisKnob().name() == "Scripts":
            self.Op_Shot.setLabel(self.Show_shot()[0])

        ##########################################################################################################################
        if nuke.thisKnob().name() == "Open Script":
            _mp_mov.tmp()
            _mp_mov.open_nk(self.Scripts_list()[3], self.Scripts.value())

        if nuke.thisKnob().name() == "Open Folder":
            os.startfile(self.Scripts_list()[3].replace('/', '\\'))

        if nuke.thisKnob().name() == "Create sh":
            _mp_mov.newScript(self.project_file[self.Ep_choice.value()][0], self._EP.value(), self.Shots.value())
            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])

        ##########################################################################################################################
        if nuke.thisKnob().name() == "imp Ren":
            self.Import_Render()
        if nuke.thisKnob().name() == "imp Cam":
            self.imCam()
        if nuke.thisKnob().name() == "Open Render":
            os.startfile((self.Ep_Sc_File_()[1] + '/' + self.Shots.value() + '/' + 'in').replace('/', '\\'))

        ##########################################################################################################################

        if nuke.thisKnob().name() == "Fr_Ra":
            self.rrr = nuke.selectedNodes('Read')
            self.fr = [i['last'].value() for i in self.rrr]
            self.rott = nuke.root()
            self.rott['first_frame'].setValue(self.rrr[self.fr.index(max(self.fr))]['first'].value())
            self.rott['last_frame'].setValue(self.rrr[self.fr.index(max(self.fr))]['last'].value())

        ##########################################################################################################################
        if nuke.thisKnob().name() == "Open Shot":
            os.startfile(self.Show_shot()[1].replace('/', '\\'))

        if nuke.thisKnob().name() == "Op Shot Folder":
            os.startfile(os.path.split(self.Show_shot()[1])[0].replace('/', '\\'))

            ##########################################################################################################################
        if nuke.thisKnob().name() == "scale scr":
            self.Op_Shot.setLabel(self.Show_shot()[0])

        ##########################################################################################################################
        if nuke.thisKnob().name() == "Imp_Ca_Re_Il":
            import IIIIIIImPoooort
            reload(IIIIIIImPoooort)
            IIIIIIImPoooort.import_render()
        if nuke.thisKnob().name() == "Klon_sc":
            import KlonNing
            reload(KlonNing)
            KlonNing.klon_sc()

        if nuke.thisKnob().name() == "imp_mast_scr" :
            M_Scripts.impt_M_script( self.File_Project , self._EP.value(), self.Master_scripts.value())
            #

    def Ep_project_file(self):
        Ep_proj = os.listdir(self.project_file[self.Ep_choice.value()][0])
        ep_ = []
        for i in Ep_proj:
            if re.match('.*ep[0-9].*', i):
                ep_.append(i)
        ep_.sort()
        ep_.append('______')
        return ep_

    def Ep_Sc_File_(self):
        ep_ren_comp_out = _mp_mov.Ep_Sc_File_(self.project_file[self.Ep_choice.value()], self.N_EP.value())
        return ep_ren_comp_out
        # ep_n , ep_ren , ep_comp , ep_out

    def Sh_list(self):
        Sq_n_l = _mp_mov.Shots_list(self.Ep_Sc_File_()[1])  # sc_N , sc_L
        return Sq_n_l[0], Sq_n_l[1]
        # ep_ren = '//alpha/prj/princess/post/ep01' # sceni isxodniki rendera
        # sq_N = [ sq083 , sq084 , sq085... ]
        # sq_L = { sc083:['sh001','sh002','sh003','sh004',],... }
        # sq_N , sq_L

    def Scripts_list(self):
        self.end_sel(self._EP.value(), self.Shots.value())
        Scr_l = _mp_mov.Scripts_list(self.Ep_Sc_File_()[2], self.Ep_Sc_File_()[0][1],
                                     self.Shots.value())  # return Scrip_list , versMax, _list_
        return Scr_l[0], Scr_l[1], Scr_l[2], Scr_l[3]

    def Show_shot(self):
        try:
            versMax = 'v' + self.Scripts.value().split('_v')[1][:3]
        except:
            versMax = 'v'

        sh_show = _mp_mov.Show_scr_out(self.Ep_Sc_File_()[3], self.Shots.value(), int(self.scale_scr.value()),
                                       self.error, versMax)  # return Scr_Imag , scr_out_file
        print sh_show
        return sh_show[0], sh_show[1]

    def Import_Render(self):

        self.path = self.Ep_Sc_File_()[1] + '/' + self.Shots.value() + '/' + 'in'
        os.system('echo ' + self.path.strip() + '| clip')
        nuke.nodePaste(nukescripts.cut_paste_file())
        os.system('echo off | clip')
        return

    def imCam(self):
        self.camFiles = self.Ep_Sc_File_()[1] + '/' + self.Shots.value() + '/' + 'data'
        for i in filter(lambda i: i.endswith('.abc'), os.listdir(self.camFiles)):
            if re.match('.*' + 'Cam' + '.*', i):
                self.Ca = nuke.createNode('Camera2', )
                self.Ca['read_from_file'].setValue(True)
                self.Ca['frame_rate'].setValue(25)
                self.Ca['file'].setValue(self.camFiles + '/' + i)
                self.Ca['name'].setValue(i.split('.abc')[0])

    def Reload_sh(self):
        try:

            self.Sc_name = os.path.splitext(os.path.basename(nuke.scriptName()))[0].split('_')

            for i in self.project_key:
                if re.match('.*' + self.project_file[i][0] + '.*', nuke.scriptName()):
                    self.Ep_choice.setValue(i)

            self._EP.setValues(self.Ep_project_file())
            self._EP.setValue(self.Sc_name[0])
            self.N_EP.setValue(int(self.Sc_name[0][2:]))

            self.Shots.setValues(self.Sh_list()[0])
            self.Shots.setValue(self.Sc_name[1])
            self.Shots_N.setValue(int(self.Sc_name[1][2:]))

            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[0].index(os.path.basename(nuke.scriptName())))

            # self.Sc_shot.setValue(self.Show_shot()[0])
            self.Op_Shot.setLabel(self.Show_shot()[0])
        except:
            self.s = 1

    def end_sel(self, ep, sh):  # ( in 'Scripts_list(self)' )self.end_sel(self._EP.value(),self.Shots.value())
        self.end_sel_file = open(os.getenv('NUKE_TEMP_DIR') + '/end_sel_sh.txt', 'w')
        self.end_sel_file.write(ep + '_' + sh)
        self.end_sel_file.close()

    def end_sel_op(self):  # self.end_sel_op()
        try:
            self.end_op_file = open(os.getenv('NUKE_TEMP_DIR') + '/end_sel_sh.txt', 'r')
            self.asd = self.end_op_file.read().split('_')
            self.end_op_file.close()

            self._EP.setValues(self.Ep_project_file())
            self._EP.setValue(self.asd[0])
            self.N_EP.setValue(int(self._EP.value().split('ep')[1]))
            self.Shots.setValues(self.Sh_list()[0])
            self.Shots.setValue(self.asd[1])
            self.Shots_N.setValue(int(self.Shots.value().split('sh')[1]))
            self.Scripts.setValues(self.Scripts_list()[0])
            self.Scripts.setValue(self.Scripts_list()[1])
            self.Op_Shot.setLabel(self.Show_shot()[0])
        except:
            self.s = 0
        self.M_Sc_list()

    def M_Sc_list(self):
        self.list_msc = M_Scripts.list_M_script( self.File_Project , self._EP.value() )
        self.Master_scripts.setValues( self.list_msc )


def addPanel_MP():
    global MetaProject_
    MetaProject_ = MetaProject().addToPane()
    return MetaProject_


menu = nuke.menu('Pane')
menu.addCommand('MetaProject', addPanel_MP, 'ctrl+m')
nukescripts.registerPanel('com.ohufx.SearchReplace', addPanel_MP)
