import os,string,re
import nuke
import nukescripts


#name = self.File_Project
#ep = self._EP.value()
#m_sc = self.M_script.value()
#name = '//alpha/envell'+ '/3_post'
#ep = 'ep009'
#m_sc = 'ep009_comp_LES_v02.nk'



def list_M_script( name , ep ): #['ep009_comp_LES_ZOKAT_v02.nk', 'ep009_comp_LES_v02.nk', ...
    path = name[:-7] +'/dev/comp_master/'+ ep
    if os.path.exists(path):
        nk_list = filter( lambda i: i.endswith('.nk') , os.listdir( path ) )   
        _comp_ =  [c for c in nk_list if re.match('.*_comp_.*', c)]
    else:
        _comp_ = ['comp_master_done']
    return _comp_

def impt_M_script(name , ep , m_sc):
    
    path = name[:-7] +'/dev/comp_master/'+ ep +'/'+ m_sc
    try:
        os.system( 'echo ' + path + '| clip' )
        nuke.nodePaste(nukescripts.cut_paste_file())
        os.system('echo off | clip')
    except: q=1    



#list_M_script( self.File_Project , self._EP.value() )
#impt_M_script(self.File_Project , self._EP.value() , self.M_script.value())