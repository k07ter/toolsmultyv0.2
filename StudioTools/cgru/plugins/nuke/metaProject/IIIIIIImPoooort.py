import os,string,re
import nuke
import nukescripts


def import_render():
    if nuke.scriptName().split('/')[3] == 'envell':
        input_readf_envell()
    else:
        input_readf()






def input_readf():
    
    F_R = file_readf()
    BB_key = F_R[0][0]
    beauty = F_R[0][1]
    illustr = F_R[1]
    ccc = F_R[3]
    x,y = BdN_r( BB_key , beauty , illustr )    
    reads_prem  = create_reaad(BB_key , beauty , illustr , x,y )
    
    # Camera
    Ca= nuke.nodes.Camera2()
    Ca['xpos'].setValue(x+25)
    Ca['ypos'].setValue(y+75)
    Ca['read_from_file'].setValue(True)
    Ca['frame_rate'].setValue(25)
    Ca['file'].setValue(ccc)
    Ca['name'].setValue(os.path.basename(ccc).split('.abc')[0])
    

    # bg_card end all contacts
    if len( illustr )>0:
        l_d = bgg_cards( reads_prem , x,y , Ca)


def input_readf_envell():
    
    nuke.Layer( 'bg_alpha',['bg_alpha.alpha'])
    nuke.Layer( 'pers_alpha',['pers_alpha.alpha'])
    nuke.Layer( 'illust_alpha',['illust_alpha.alpha'])
    
    F_R = file_readf()
    BB_key = F_R[0][0]
    beauty = F_R[0][1]
    illustr = F_R[1]
    ccc = F_R[3]
    x,y = BdN_r( BB_key , beauty , illustr )    
    reads_dot , reads_prem  = create_reaad_envell(BB_key , beauty , illustr , x,y )
    
    # Camera
    Ca= nuke.nodes.Camera2()
    Ca['xpos'].setValue(x+25)
    Ca['ypos'].setValue(y+75)
    Ca['read_from_file'].setValue(True)
    Ca['frame_rate'].setValue(25)
    Ca['file'].setValue(ccc)
    Ca['name'].setValue(os.path.basename(ccc).split('.abc')[0])
    # dot for Cam    
    Ca_d = nuke.nodes.Dot()
    Ca_d['xpos'].setValue(x+50)
    Ca_d['ypos'].setValue(y+450)
    Ca_d.setInput(0,Ca)
    Ca_d['name'].setValue('Cam')
    Ca_d['note_font_size'].setValue(25)
    Ca_d['label'].setValue('[value name]')

    # bg_card end all contacts
    if len( illustr )>0:
        l_d = bgg_cards_envell( reads_prem , x,y , Ca)
        reads_dot.insert(0, l_d)

    # Merge2 ,import dots posledovatelosti
    for i in nuke.selectedNodes():
        i['selected'].setValue(False)
    Dots = reads_dot
    main=nuke.createNode('Merge2')
    main['xpos'].setValue(x+500)
    main['ypos'].setValue(y+400)
    main['also_merge'].setValue('all')
    for i in range( len(Dots)):
        main.setInput(i+3,Dots[i])
    
    # create views (paralel poptoki) dots(name)
    c = ''
    for i in Dots:
        c = c+'\n{'+i.name()+' ""}'
    b = '{main #ffffff}' +c
    nuke.Root().knob('views').fromScript(b)
    # JoinViews import dots(name)
    J = nuke.createNode('JoinViews')
    J['xpos'].setValue(x+400)
    J['ypos'].setValue(y+400)
    for i in range( len(Dots)):
        J.setInput(i+1,Dots[i])
    # dot for Render
    J_d = nuke.nodes.Dot()
    J_d['xpos'].setValue(x+435)
    J_d['ypos'].setValue(y+450)
    J_d.setInput(0,J)
    J_d['name'].setValue('Render')
    J_d['note_font_size'].setValue(25)
    J_d['label'].setValue('[value name]')

    # poisk (in_cam , in_render ) podkluchenie cam dot out, render dot out
    ddd = nuke.allNodes('Dot')
    for i in ddd :
        if re.match('.*'+i['name'].value()+'.*','in_c'):
            print i['name'].value()
            i.setInput(0,Ca_d )
        if re.match('.*'+i['name'].value()+'.*','in_r'):
            print i['name'].value()
            i.setInput(0,J_d )


def file_readf(): # max(V)read. return [ BB_key , beauty ] , illustr , [ proj , ep , sh ] , ccc 

    ep_sh =[i[2:] for i in nuke.scriptName().split('/')[-1].split('_')[0:2]]
    ep = ep_sh[0]
    sh = ep_sh[1]
    proj = nuke.scriptName().split('/')[3]#'envell'#'leo'
    Epizod = '//omega/'+ proj +'/3_post/ep' +string.zfill( ep,3 )
    Shots = ['sh'+ string.zfill( i,3 ) for i in sh.split(',') ]

    #########################
    s = Shots[0]
    #########################
    #  max(V)read  ,  posledovatelnost ['bg','mg','membran','chars','fg','glass']
    #_beauty = {'chars_layer': '//alpha/envell/3_post/ep009/sh0307/in/v001/chars_layer',...>}
    # BB_key=['bg_layer', 'mg_layer', 'fg_layer', 'chars_layer', 'vik_glass_layer', 'kira_layer'] #
    beauty = {}
    fold = [f for f in os.listdir(Epizod +'/'+ s +'/in/') if re.match('.*'+'v'+'.*',f)]
    for ff in fold:
        for vv in os.listdir(Epizod +'/'+ s +'/in/'+ff+'/'):
            beauty[vv] = Epizod +'/'+ s +'/in/'+ff+'/'+vv
    k = ['bg','mg','shadow','membran','chars','glass','shield','fg']
    b_key = beauty.keys()
    b_key.sort()
    BB_key =[]
    for i in k :
        for f in b_key :
            if re.match('.*'+i+'.*',f):
                b_key.remove(f)
                BB_key.append(f)
    BB_key.extend(b_key)
    #########################
    #  illustr  ['bg','mid','fg']
    # illustr = ['//alpha/envell/3_post/ep009/sh0307/in/illustr/ep009_sh0307_bg_01_illustr_v001.png',...]
    illustr = []
    pfx = ['bg','mid','fg']
    f_ill = Epizod +'/'+ s +'/in/illustr/'
    try:
        illustr = [f_ill+ill for p in pfx for ill in os.listdir(f_ill) if re.match('.*'+p+'.*',ill) ]
    except: q=1    
    #########################
    #  cam 
    camFiles = Epizod +'/'+ s +'/'+ 'data'
    c = [i for i in filter( lambda i: i.endswith('.abc') , os.listdir( camFiles ) ) if re.match( '.*' + 'Cam' + '.*' , i )]
    ccc = camFiles +'/'+ max(c)

    return [ BB_key , beauty ] , illustr , [ proj , ep , sh ] , ccc

def BdN_r( BB_key , beauty , illustr ): #  Backdrop. return [x,y]
    d = nuke.createNode('Dot')
    x,y = d['xpos'].value(),d['ypos'].value()
    nuke.delete(d)
    BdN = nuke.nodes.BackdropNode(xpos =x, ypos = y )
    BdN['bdwidth'].setValue(230 + 100*len(illustr)+150*len([ beauty[v] for v in BB_key ]))
    BdN['bdheight'].setValue(500)

    return [x,y]

def create_reaad(BB_key , beauty , illustr , x,y ): #  Reads. return reads_prem

    reads_prem = []
    y=y+75
    x=x+200
    if len(illustr)>0:
        for F in illustr :
            try:
                os.system( 'echo ' + F + '| clip' )
                nuke.nodePaste(nukescripts.cut_paste_file())
                os.system('echo off | clip')
            except: q=1    
            r_node = nuke.selectedNode()
            r_node['xpos'].setValue(x)
            r_node['ypos'].setValue(y)
            x=x+100
            p = nuke.createNode('Premult')
            reads_prem.append(p)
        x=x+50

    for F in [ beauty[v] for v in BB_key ] :
        try:
            os.system( 'echo ' + F + '| clip' )
            nuke.nodePaste(nukescripts.cut_paste_file())
            os.system('echo off | clip')
        except: q=1    
        # position READ
        r_node = nuke.selectedNode()
        nam = r_node['file'].value().split('/')[-2]
        r_node['xpos'].setValue(x)
        r_node['ypos'].setValue(y)
        x=x+150
        y=y+10
        # Shuffle

    return reads_prem

def create_reaad_envell(BB_key , beauty , illustr , x,y ): #  Reads. return  reads_dot , reads_prem 

    reads_dot = []
    reads_prem = []
    y=y+75
    x=x+200
    if len(illustr)>0:
        for F in illustr :
            try:
                os.system( 'echo ' + F + '| clip' )
                nuke.nodePaste(nukescripts.cut_paste_file())
                os.system('echo off | clip')
            except: q=1    
            r_node = nuke.selectedNode()
            r_node['postage_stamp'].setValue(0)
            r_node['xpos'].setValue(x)
            r_node['ypos'].setValue(y)
            x=x+100
            p = nuke.createNode('Premult')
            reads_prem.append(p)
        x=x+50

    for F in [ beauty[v] for v in BB_key ] :
        try:
            os.system( 'echo ' + F + '| clip' )
            nuke.nodePaste(nukescripts.cut_paste_file())
            os.system('echo off | clip')
        except: q=1    
        # position READ
        nam=''
        r_node = nuke.selectedNode()
        r_node['postage_stamp'].setValue(0)
        nam = r_node['file'].value().split('/')[-2]
        r_node['xpos'].setValue(x)
        r_node['ypos'].setValue(y)
        x=x+150
        y=y+10
        # Shuffle
        if re.match('.*'+'bg'+'.*',nam.split('_layer')[0]) or re.match('.*'+'mg'+'.*',nam.split('_layer')[0]) or re.match('.*'+'fg'+'.*',nam.split('_layer')[0]):
            shu = nuke.createNode('Shuffle')
            shu['in'].setValue('alpha')#
            shu['out'].setValue('bg_alpha')#
        else:
            shu = nuke.createNode('Shuffle')
            shu['in'].setValue('alpha')
            shu['out'].setValue('pers_alpha')
        # Dot
        d = nuke.createNode('Dot')
        d['name'].setValue(nam.split('_layer')[0])
        d['note_font_size'].setValue(25)
        d['label'].setValue('[value name]')
        reads_dot.append(d)
    



    return     reads_dot , reads_prem 

def bgg_cards_envell( reads_prem , x,y ,Ca): # bg_card end all contacts. return l_d

    for i in nuke.selectedNodes():
        i['selected'].setValue(False)
    shu = nuke.createNode('Shuffle')
    shu['xpos'].setValue(x+300)
    shu['ypos'].setValue(y+300)
    shu['in'].setValue('alpha')
    shu['out'].setValue('illust_alpha')    
    l_d = nuke.createNode('Dot')
    l_d['name'].setValue('illust')
    l_d['note_font_size'].setValue(25)
    l_d['label'].setValue('[value name]')

    #bg_cards
    Prems = reads_prem
    #bg_c = nuke.nodes.bg_cards(xpos =x+150, ypos = y+200 )
    ###############################

    for i in nuke.selectedNodes():
        i['selected'].setValue(False)
    F = 'bg_cards {}'
    os.system( 'echo ' + F + '| clip' )
    nuke.nodePaste(nukescripts.cut_paste_file())
    os.system('echo off | clip')
    bg_c = nuke.selectedNode()
    bg_c ['xpos'].setValue(x+150)
    bg_c ['ypos'].setValue(y+200)

    ###############################
    for i in range( len(Prems)):
        bg_c.setInput(i,Prems[i])
    bg_c['card_01'].setValue(1000)
    bg_c['card_02'].setValue(750)
    bg_c['card_02'].setValue(750)
    bg_c['card_03'].setValue(650)
    bg_c['card_04'].setValue(550)
    bg_c['card_05'].setValue(450)
    bg_c['card_06'].setValue(400)
    bg_c['card_07'].setValue(350)
    bg_c['card_08'].setValue(300)
    bg_c['card_09'].setValue(250)
    bg_c['card_10'].setValue(200)

    F_H = nuke.nodes.FrameHold()
    F_H ['xpos'].setValue(x+105)
    F_H ['ypos'].setValue(y+225)
    F_H.setInput(0,Ca)

    tans = nuke.nodes.TransformGeo()
    tans['xpos'].setValue(x+150)
    tans['ypos'].setValue(y+275)
    tans.setInput(0,bg_c)
    tans.setInput(1,F_H)

    scan_R = nuke.nodes.ScanlineRender()
    scan_R['xpos'].setValue(x+65)
    scan_R['ypos'].setValue(y+305)
    scan_R.setInput(1,tans)
    scan_R.setInput(2,Ca)

    shu_color = nuke.nodes.Shuffle()
    shu_color['xpos'].setValue(x+65)
    shu_color['ypos'].setValue(y+335)
    shu_color['out'].setValue('color')
    shu_color.setInput(0,scan_R)

    expr = nuke.nodes.Expression()
    expr['xpos'].setValue(x+65)
    expr['ypos'].setValue(y+365)
    expr['expr3'].setValue('z!=0?1/z:0')
    expr['channel3'].setValue('depth.Z')
    expr.setInput(0,shu_color)

    shu.setInput(0,expr)

    return l_d
    
def bgg_cards( reads_prem , x,y ,Ca): # bg_card end all contacts.

    for i in nuke.selectedNodes():
        i['selected'].setValue(False)
    #bg_cards
    Prems = reads_prem
    #bg_c = nuke.nodes.bg_cards(xpos =x+150, ypos = y+200 )
    ###############################

    for i in nuke.selectedNodes():
        i['selected'].setValue(False)
    F = 'bg_cards {}'
    os.system( 'echo ' + F + '| clip' )
    nuke.nodePaste(nukescripts.cut_paste_file())
    os.system('echo off | clip')
    bg_c = nuke.selectedNode()
    bg_c ['xpos'].setValue(x+150)
    bg_c ['ypos'].setValue(y+200)

    ###############################
    for i in range( len(Prems)):
        bg_c.setInput(i,Prems[i])
    bg_c['card_01'].setValue(1000)
    bg_c['card_02'].setValue(750)
    bg_c['card_02'].setValue(750)
    bg_c['card_03'].setValue(650)
    bg_c['card_04'].setValue(550)
    bg_c['card_05'].setValue(450)
    bg_c['card_06'].setValue(400)
    bg_c['card_07'].setValue(350)
    bg_c['card_08'].setValue(300)
    bg_c['card_09'].setValue(250)
    bg_c['card_10'].setValue(200)

    F_H = nuke.nodes.FrameHold()
    F_H ['xpos'].setValue(x+105)
    F_H ['ypos'].setValue(y+225)
    F_H.setInput(0,Ca)

    tans = nuke.nodes.TransformGeo()
    tans['xpos'].setValue(x+150)
    tans['ypos'].setValue(y+275)
    tans.setInput(0,bg_c)
    tans.setInput(1,F_H)

    scan_R = nuke.nodes.ScanlineRender()
    scan_R['xpos'].setValue(x+65)
    scan_R['ypos'].setValue(y+305)
    scan_R.setInput(1,tans)
    scan_R.setInput(2,Ca)

    shu_color = nuke.nodes.Shuffle()
    shu_color['xpos'].setValue(x+65)
    shu_color['ypos'].setValue(y+335)
    shu_color['out'].setValue('color')
    shu_color.setInput(0,scan_R)

    expr = nuke.nodes.Expression()
    expr['xpos'].setValue(x+65)
    expr['ypos'].setValue(y+365)
    expr['expr3'].setValue('z!=0?1/z:0')
    expr['channel3'].setValue('depth.Z')
    expr.setInput(0,shu_color)

    return 
    
