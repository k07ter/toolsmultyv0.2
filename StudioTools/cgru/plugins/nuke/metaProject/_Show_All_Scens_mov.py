import nuke
import os, re
import nukescripts
import string
import _mp_mov
reload(_mp_mov)


class Show_All_Scens( nukescripts.PythonPanel ):
    def __init__( self, ep_choice , _N_EP , Sequence , scale_scr , menu_out ):
        #ep_choice = '//store1/EDGE/Fixies/images,//Server/edge/FIXIES_serial/composing,//server/EDGE/FIXIES_serial/OUTPUT/render'
        # _N_EP = '116'



#__Docum
        nukescripts.PythonPanel.__init__( self, 'Show All Scens' )

        self.dokumum_file = 'D:/nukeTools/metaProject/_metaProject.txt'  
        self.dokum = open( self.dokumum_file, 'r' ).readlines()

        self.project_file = {}
        self.project_key=[]
        for i in self.dokum:
            self.k = i.replace('\\','/').strip().split( ' = ' ) 
            self.project_file[ self.k[0] ] = self.k[1].lower().split( ' <> ' )
            self.project_key.append( self.k[0] )           

        self.proj_file =  self.project_file[ep_choice]      # ['//store1/EDGE/Fixies/images','//Server/edge/FIXIES_serial/composing','//server/EDGE/FIXIES_serial/OUTPUT/render']
        
        self.N_EP  =         _N_EP                     # 116
        self.scale =        int(scale_scr)
        self.menuOut=       menu_out                                   # 'out+nk','out','out_True','out_False'
#       
        self.EpScF = self.ep_sc_file( self.proj_file , self.N_EP )
        self.ep_ren =       self.EpScF[1]                              # '//store1/edge/fixies/images/_ep115' # sceni isxodniki rendera
        self.ep_comp =      self.EpScF[2]                              # '//server/edge/fixies_serial/composing/115_koleso' #compoz papki
        self.Ep_out =       self.EpScF[3]                              # '//server/edge/fixies_serial/output/render/115_koleso'
        self.sequence =     Sequence
                
        self.error =        'D:/nukeTools/metaProject/Terror-Night.jpeg'

        self.setMinimumSize(3*133* self.scale , 1000)
        
        
        
# CREATE knob_op DICTS IN A LISTS
#knob_op
        self.addKnob( nuke.PyScript_Knob('ddd' , '<img src= "D:/nukeTools/metaProject/down_.png" width="15" height="7" />') )
    #Imag_o knob_op
        self.All_file={}
        if self.menuOut == 'out_True':
            all_ = self.A_file()
            for i in all_.keys():
                if all_[i][4] == True : 
                    self.All_file[i] = all_[i]
        else :        
            self.All_file = self.A_file()    

        sc_n = self.All_file.keys()
        sc_n.sort()
        for i in range(0,len(sc_n),3):
            try:
                for f in range(3):
                    sc_k = sc_n[i+f]
                    self.knob_Im = nuke.PyScript_Knob( self.All_file[sc_k][2].split('.')[0] , self.All_file[sc_k][0] , "import os\nos.startfile(( '"+self.All_file[sc_k][1] +"').replace('/','\\\\'))" )
                    self.addKnob(self.knob_Im)
                    if f==0 : self.knob_Im.setFlag(nuke.STARTLINE)
            except: s=1                
            try:
                if self.menuOut == 'out+nk':
                    for f in range(3):
                        sc_k = sc_n[i+f]
                        self.knob_op = nuke.PyScript_Knob( self.All_file[sc_k][2].split('.')[0] ,  '________________________'+ self.All_file[sc_k][2].split('_')[2] +'________________________', "import _mp_mov\nreload(_mp_mov)\n_mp_mov.tmp()\n_mp_mov.open_nk('"+self.All_file[sc_k][3]+"','"+self.All_file[sc_k][2]+"')" )
                        self.addKnob(self.knob_op)
                        if f==0 : self.knob_op.setFlag(nuke.STARTLINE)

            except: self.addKnob(nuke.PyScript_Knob( 'xxx' , '___________________'))

#
        
    def knobChanged(self,knob):

        if nuke.thisKnob().name() == "ddd":
            for i in self.A_file().keys():
                print self.A_file()[i]


    def A_file(self):        # self.All_file = { sc001:[ scr_out_imag , scr_out_file, scr_nk[maxVers] , scr_file , (True/False) ] , ...}
        A_f = {}
        for Shot in self.Sequence_list(self.ep_ren)[1][self.sequence]:
            
            Scr_list = self.Scripts_list( self.ep_comp , self.N_EP ,  self.sequence , Shot )                # return Scrip_list , versMax, _list_              #['ep115_sc002_v01.nk', 0 , 'ep115_sc041_v01.nk', 'ep115_s
            Script = [   Scr_list[0][ Scr_list[1] ]   ,   Scr_list[3]    ]                        #'ep115_sc002_v01.nk' (file nk)
            
            scr_name = 'ep'+self.N_EP+'_'+self.sequence+'_'+Shot
    
            try:
                versMax = 'v'+Script[0].split('_v')[1][:2]    
            except: versMax = 'v'


            Show = self.Show_scr_out( self.Ep_out , self.sequence , Shot , self.scale , self.error , versMax  )          #self.Show = ['<img src="C:/Users/bakhodir.zufarov/.... , '//SERVER/tools...]
            out_life = True
            if re.match('.*'+ self.error +'.*', Show[0] ): 
                out_life = False

            A_f[ Shot ] = [ Show[0] , Show[1] , Script[0] , Script[1] , out_life ]
        return A_f


    def ep_sc_file(self, proj_file , N_EP ):
        return _mp_mov.Ep_Sc_File_( proj_file , N_EP )
        #ep_n , ep_ren , ep_comp ,ep_out 

    def Sequence_list(self , ep_ren):
        return _mp_mov.Sequence_list( ep_ren )
        #sc_n[] , sc_l{}

    def Scripts_list(self, ep_comp , N_EP , Sequence, Shot):
        return _mp_mov.Scripts_list( ep_comp , N_EP , Sequence, Shot )                
        #['ep115_sc002_v01.nk', 0 , 'ep115_sc041_v01.nk', 'ep115_s

    

    def Show_scr_out(self, Ep_out , Sequence , Shot , scale , error , versMax) :
        return _mp_mov.Show_scr_out( Ep_out , Sequence , Shot , scale , error , versMax )          
        #['<img src="C:/Users/bakhodir.zufarov/.... , '//SERVER/tools...]




        