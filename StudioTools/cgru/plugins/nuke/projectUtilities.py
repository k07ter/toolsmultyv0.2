import json
import os
import platform
import re#read settings:
import sys

'''
def getPathDependsSystem(path):
    if platform.system()=='Linux':
        path = path.replace('\\', '/')
        if path.startswith('//'):
            path = path.replace('//', '/')
        return path

    if platform.system()=='Windows':
        path = path.replace('/', '\\')
        if path.startswith('/'):
            path = path.replace('/', '\\\\')
        return path
'''
def printEnvironmentVars(startmask=''):
    '''
    print Environment Vars starting with startmask
    >>>printEnvironmentVars('PA')
    '''
    for i in os.environ:
        if i.startswith(startmask): print ('{0}={1}'.format(i,os.environ[i]))


def getDictFromJson(filename):
    fh = dict = None
    try:
        fh=open(filename)
        dict = json.load(fh)
    except Exception as err:
        print('ERROR getDictFromJson "%s": %s' % (filename, err))
        return
    finally:
        if fh is not None:
            fh.close()
    return dict

def readFile(filename):
    lines=[]
    fh=None
    try:
        fh=open(filename)
        for line in fh:
            if line.strip():
                line = re.sub(r"\r+\n+", "", line)
                lines.append(line)
    except (IOError, OSError) as err:
        print(err)
        return[]
    finally:
        if fh is not None:
            fh.close()
    return lines


def findProjectSettings(path):
    path = path.replace('\\', '/')
    try:
        json_projects_ini = '%s/projects_info.json' % os.path.dirname(__file__)
        dict_projects_ini = getDictFromJson(json_projects_ini)

        if dict_projects_ini is None:
            print ('ERROR getDictFromJson: get None dict')
            return
        prj_set_dict=dict()
        for id in dict_projects_ini.keys():
            prj_path = dict_projects_ini[id]['path']
            if 'linux' in sys.platform:
                 prj_path = prj_path.replace('//', '/')
            #print prj_path
            if prj_path in path:
                prj_set_dict['id'] = id
                prj_set_dict['name'] = dict_projects_ini[id]['name']
                prj_set_dict['path'] = dict_projects_ini[id]['path']
                return prj_set_dict
                #return line_list
        return
    except Exception as err:
        print('ERROR: findProjectSettings %s ' % err)


def dataInfo(app, file_path, debug=True):
    file_path = file_path.lower().replace('\\', '/')
    entity='unknown'
    entity_data_dict={}
    #get project!
    prj_path = None
    prj_name = None

    try:
        prj_path = os.environ['PRVZ_PROJECT_PATH'].lower().replace('\\', '/')
        if debug : print('DATA INFO: for this file PRVZ_PROJECT_PATH=%s' % prj_path)
    except Exception as err:
        print('ERROR: PRVZ_PROJECT_PATH not set: %s' % err)
        return

    if not prj_path in file_path:
        if debug: print ('WARNING: Current application "%s" working in "%s" project environment.\nFile "%s" from other project. \nStarting search correct project..' % (app, prj_path, file_path))
        ps = findProjectSettings(file_path)
        if ps is None:
            print('ERROR: No one project exists in "projects_info.json" for %s ' % file_path)
            return
        else:
            prj_path = ps['path']
            prj_name = ps['name']
            os.environ['PRVZ_PROJECT_SETTINGS_PATH'] = '%s/projects_settings/%s' % (os.environ['PRVZ_PROJECTS_INFO_PATH'], prj_name)
    #print (prj_path, prj_name, os.environ['PRVZ_PROJECTS_INFO_PATH'])
    json_entities_file = '%s/.settings/.%s/entities.json' % (os.environ['PRVZ_PROJECT_SETTINGS_PATH'], app)
    if debug : print ('DATA INFO:load entities json: "%s"...' % json_entities_file)
    dict_entities = getDictFromJson(json_entities_file)
    if dict_entities is None:
        print ('ERROR getDictFromJson: get None dict')
        return
    for ent in dict_entities:
        ent_rex = dict_entities[ent]["file_re"]
        #print (ent_rex)
        if re.match(ent_rex, file_path):
            if debug :print ('DATA INFO:This is a %s' % (ent))
            entity = ent
            vars_dict = re.match(ent_rex, file_path).groupdict()
            #print ('DATA INFO:Vars = ', vars_dict)
            for line in dict_entities[ent]:
                val = dict_entities[ent][line]
                if line == "file_re": continue
                #print('DATA INFO:line=',line, val)
                for var in vars_dict:
                    if ('<%s>' % var) in val:
                        val = val.replace('<%s>' % var, vars_dict[var])
                entity_data_dict[line]= val

    dict_res = {'prj_path':prj_path, 'entity':entity, 'entity_info': entity_data_dict}
    if debug : print ('DATA_INFO:', dict_res)
    #return (prj_path, entity , entity_data_dict)
    return dict_res



def readProjectsIniJson(id='00'):
    json_projects_ini = '%s/projects_info.json' % os.path.dirname(__file__)
    dict_projects_ini = getDictFromJson(json_projects_ini)
    if id not in dict_projects_ini.keys(): id='00'

    prj_name = dict_projects_ini[id]['name']
    prj_path = dict_projects_ini[id]['path']
    def_prj_path = dict_projects_ini['00']['path']

    if platform.system()=='Linux':
        prj_path = prj_path.replace('\\', '/')
        if prj_path.startswith('//'):
            prj_path = prj_path.replace('//', '/')

    print ('{0},{1},{2}'.format(def_prj_path, prj_path, prj_name ))


def readProjectsIniJsonOut(id):
    json_projects_ini = '%s/projects_info.json' % os.path.dirname(__file__)
    dict_projects_ini = getDictFromJson(json_projects_ini)
    if id not in dict_projects_ini.keys(): id='00'

    prj_name = dict_projects_ini[id]['name']
    prj_path = dict_projects_ini[id]['path']
    def_prj_path = dict_projects_ini['00']['path']

    if platform.system()=='Linux':
        prj_path = prj_path.replace('\\', '/')
        if prj_path.startswith('//'):
            prj_path = prj_path.replace('//', '/')
    return [def_prj_path, prj_path, prj_name]


if __name__ == "__main__":
    #print (sys.argv)
    #if len(sys.argv)>1: readProjectsIni(sys.argv[1])
    if len(sys.argv)>1: readProjectsIniJson(sys.argv[1])
    else: readProjectsIniJson()
