from afanasyRenderJob import *

class JobMaker(AfanasyRenderJob):
    def __init__(self):
        # super(AfanasyRenderJob, self).__init__()
        super(JobMaker, self).__init__()
        print("Job's settings up ...")
        # self.dispatcher = self.job_param['job_dispatcher']
        # self.description = self.job_param['job_description']
        self.name = str(self.job_param['job_name']).strip()
        if self.name == '':
            self.name = getMayaSceneName()
        self.cleanup_ass = self.job_param['job_cleanup_ass']
        self.cleanup_maya = self.job_param['job_cleanup_maya']
        self.cleanup_yeti = self.job_param['job_cleanup_yeti']

    @property
    def dispatcher(self):
        return self.job_param.get('job_dispatcher')

    @property
    def description(self):
        return self.job_param.get('job_description')

    # @property
    # def name(self):
    #	jn = self.job_param.get('job_name')
    #	if jn:
    #		return str(jn.strip())
    #	else:
    #		return getMayaSceneName()

    def make_task(self, name):
        self.name = name

    def make_frame(self, range):
        self.range = range
