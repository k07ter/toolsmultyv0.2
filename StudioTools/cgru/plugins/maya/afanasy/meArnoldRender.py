# -*- coding: utf-8 -*-
"""
	meArnoldRender

	Usage:
	You can add this code as shelf button :

from afanasy import meArnoldRender
reload( meArnoldRender ) # keep this just for debug purposes
meArnoldRender = meArnoldRender.meArnoldRender()

	Important!!!
	Do not use another object name.
	Only meArnoldRender.renderLayerSelected() will work with script job.

	Add %CGRU_LOCATION%\plugins\maya to %PYTHONPATH%

"""

import sys
import os
import string
import shutil
import maya.OpenMaya as OpenMaya
import maya.cmds as cmds
import maya.mel as mel
from functools import partial

TOOLS_PATH = os.getenv('TOOLS_PATH').replace('\\', '/')
sys.path.append(TOOLS_PATH)

from helper.EnvManagerV2 import *
from maya_ui_proc import *
from afanasyRenderJob import *

import previewCmd as prvc
from ui import *
#print(EnvVar('ARNOLD_PLUGIN_PATH').appendPath(["${FDBIN}/plugin"]))

self_prefix = 'meArnoldRender_'
meArnoldRenderVer = '0.3.4'
meArnoldRenderMainWnd = self_prefix + 'MainWnd'

ar_verbosity_list = [
	'none',
	'fatal',
	'error',
	'warning',
	'info',
	'progress',
	'debug',
	'details'
]

class meArnoldRender(object):
	"""
	meArnoldRender

	"""
	def_RG = 'defaultRenderGlobals'
	def_arnOpts = 'defaultArnoldRenderOptions'
	job_param = ass_param = ar_param = img_param = afanasy_param = bkburn_param = {}
	deferred_prefix = ''
	job = None

	def __init__(self, NoGUI=False):
			"""
			In case if NoGUI = True, it's possible to use external "wrapper"
			e.g. for Shotgun to call "generate_ass" or "submitJob" previously
			assign global variables  
			"""
			self.prefix = 'meArnoldRender_'
			self.meArnoldRenderVer = '0.3.4'
			self.meArnoldRenderMainWnd = self.prefix + 'MainWnd'
			self.winMain = ''
			self.NoGUI = NoGUI
			self.os = sys.platform
			if self.os.startswith('linux'):
				self.os = 'linux'
				self.ext = ''
			elif self.os == 'darwin':
				self.os = 'mac'
			elif self.os == 'win32':
				self.os = 'win'
				self.ext = '.exe'

			#print('sys.platform = %s self.os = %s' % (sys.platform, self.os))
			self.curr_render = cmds.getAttr('%s.currentRenderer' % self.def_RG)
			if self.curr_render != 'arnold':
				try:
					cmds.warning('Arnold is not current renderer! It will be switch ...')
					cmds.setAttr('%s.currentRenderer' % self.def_RG, 'arnold', type="string")
					#mel.eval('loadPreferredRenderGlobalsPreset(%s);' % self.curr_render)
					mel.eval('updateRenderOverride')
					# import sys; sys.modules['maya.app.renderSetup.model.renderSetup'].hasOverrideApplied('defaultRenderGlobals', 'arnold');
					self.curr_render = 'arnold'
				except:
					cmds.warning('Arnold is not current renderer! It can\'t be switched.')
					return

			print('Render \"%s\" starting ...' % self.curr_render)
			#self.scene_file = '//omega/bears/2_prod/ep170/sh010/render/ep170_sh010_render_v001.ma'
			self.scene_file = cmds.file(q=True, sn=True)
			self.ass_dirname = cmds.workspace(fileRuleEntry='ASS')
			#print(self.scene_file)
			#print(self.ass_dirname)
			if 'render' in self.scene_file:
				#mel.eval('setProject %s;' % self.proj_path)
				#self.scene_name = self.scene_file_name.split('.')[0]
				#mel.eval('updateRenderUI;')
				try:
					self.proj_path, self.scene_name = self.scene_file.split('/render/')
					cmds.warning(u'Проверка формата имени сцены: \"%s\"' % self.scene_file)
					self.ep, self.shot, self.step, self._version = self.scene_name.split('_')
					self.version = self._version.split('.')[0]
				except:
					#cmds.warning("Wrong scene's name!")
					cmds.warning(u'Ошибка формата имени.')
					return
			else:
				if self.scene_file:
					cmds.warning(u'Открыта сцена неправильного типа (не для рендера).')
				else:
					cmds.warning(u'Ошибка открытия сцены.')
				return
			#cmds.workspace(q=True, rootDirectory=True)
			#self.filename = getMayaSceneName()
			self.layer = cmds.editRenderLayerGlobals(
				query=True,
				currentRenderLayer=True
			)
			#self.deferred_prefix = ''
			#self.job = None
			#self.job_param = {}
			#self.ass_param = {}
			#self.ar_param = {}
			#self.img_param = {}
			#self.afanasy_param = {}
			#self.bkburn_param = {}

			pipline = PiplineTools(opts='maya')
			self.soft_tools_path = pipline.soft_tools_path
			self.app = pipline.app
			self.save_frame_bgc = [0, 0, 0]
			self.def_frame_bgc = [0.75, 0.5, 0]
			self.initParameters()
			if not self.NoGUI:
				self.ui = arnoldUI(self)
				#self.ui = self._setupUI()

	def initParameters(self):
			"""
			Init all parameters
			"""
			#
			# Job parameters
			#
			#Отключение конвертации в tx
			try:
				cmds.setAttr("%s.autotx" % self.def_arnOpts, 0)
			except:
				print(u'Ошибка отключения конвертации в tx (вероятно, не открыта сцена).')

			self.job_param['job_dispatcher'] = \
				getDefaultStrValue(self.prefix, 'job_dispatcher', 'afanasy')

			self.job_param['job_name'] = getMayaSceneName()
			#_jp = {'job': [['description', 'animation', 'start', 'stop', 'step', 'size', 'paused', 'priority', 'cleanup_ass', 'cleanup_maya', 'cleanup_yeti', 'padding'],
			#			   ['', 1, 1, 100, 1, 1, -1, 1, 1, 1, 4]]}
			jp = {
				"description": "",
				"animation": True,
				"start": 1,
				"end": 100,
				"step": 1,
				"size": 1,
				"paused": True,
				"priority": -1,
				"cleanup_ass": False,
				"cleanup_maya": False,
				"cleanup_yeti": False,
				"padding": 4,
				"separator": ".",
				"preview": True,
				"preview_shotgun": True,
				"overwrite_workspace": False,
				"new_user": ""
			}

			#*** Установка значений в полях формы
			def set_field(fname, pref):
				if type(fname) == str:
					fnc = getDefaultStrValue
				elif type(fname) == int:
					fnc = getDefaultIntValue
				if type(buf[ech]) == bool:
					if buf[ech]:
						vlu = 1
					else:
						vlu = 0
					return  fnc(self.prefix, pref + "_%s" % fname, vlu) is 1
				return fnc(self.prefix, pref + "_%s" % fname, buf[fname])

			buf = jp
			for ech in jp:
				self.job_param["job_%s" % ech] = set_field(ech, pref= "job")

			#
			# .ass generation parameters
			#
			self.ass_param['ass_reuse'] = \
				getDefaultIntValue(self.prefix, 'ass_reuse', 0) is 1

			#ass_dirname = cmds.workspace(fileRuleEntry='ASS')
			if self.ass_dirname == '':
				self.ass_dirname = 'ass'
				# ass_dirname = '//omega/moriki-doriki/temp/data/maya'
				cmds.workspace(fileRule=('ASS', self.ass_dirname))
			# cmds.workspace( saveWorkspace=True )

			ap = {
				"reuse": False,
				"dirname": self.ass_dirname,
				"perframe": False,
				"selection": False,
				"binary": True,
				"verbosity": "none",
				"deferred": True,
				"local_assgen": False,
				"def_task_size": 4,
				"export_all_layers": False,
				"compressed": False,
				"expand_procedurals": True,
				"export_bounds": False
			}

			buf = ap
			for ech in ap:
				self.ass_param["ass_%s" % ech] = set_field(ech, pref="ass")

			#
			# Arnold parameters
			#
			ar_p = {
				"options": "",
				"verbosity": "none",
				"threads": 0,
				"abs_tex_path": "",
				"abs_proc_path": ""
				#"plugin_path": "",
				#"shader_search_path": "",
				#"tex_search_path": ""
			}
			buf = ar_p
			for ech in ar_p:
				self.ar_param["ar_%s" % ech] = set_field(ech, pref="ar")

			# defaultArnoldRenderOptions.absoluteTexturePaths
			try:
				abs_tex_path = cmds.getAttr('%s.absoluteTexturePaths' % self.def_arnOpts)
			except:
				abs_tex_path = 1
			self.ar_param['ar_abs_tex_path'] = \
				getDefaultIntValue(self_prefix, 'ar_abs_tex_path', abs_tex_path) is 1
			#	cmds.getAttr('defaultArnoldRenderOptions.absoluteTexturePaths')) is 1

			# defaultArnoldRenderOptions.absoluteProceduralPaths
			try:
				abs_proc_path = cmds.getAttr('%s.absoluteProceduralPaths' % self.def_arnOpts)
			except:
				abs_proc_path = 1
			self.ar_param['ar_abs_proc_path'] = \
				getDefaultIntValue(self_prefix, 'ar_abs_proc_path', abs_proc_path) is 1

			# defaultArnoldRenderOptions.plugins_path
			try:
				plg_path = cmds.getAttr('%s.plugins_path' % self.def_arnOpts)
			except:
				plg_path = ''
			self.ar_param['ar_plugin_path'] = getDefaultStrValue(self_prefix, 'ar_plugin_path', plg_path)

			# defaultArnoldRenderOptions.procedural_searchpath
			try:
				proc_srch_path = cmds.getAttr('%s.procedural_searchpath' % self.def_arnOpts)
			except:
				proc_srch_path = ''
			self.ar_param['ar_proc_search_path'] = \
				getDefaultStrValue(self_prefix, 'ar_proc_search_path', proc_srch_path)

			# defaultArnoldRenderOptions.shader_searchpath
			try:
				shdr_gef_path = cmds.getAttr('%s.shaders_searchpath' % self.def_arnOpts)
			except:
				shdr_gef_path = '%s/arnold/mtoadeploy/%s/%s/shaders' % (self.app.version, self.app.ospa.os_t, self.soft_tools_path)
			self.ar_param['ar_shader_search_path'] = \
				getDefaultStrValue(self_prefix, 'ar_shader_search_path', shdr_gef_path)

			# defaultArnoldRenderOptions.texture_searchpath
			try:
				tex_def_path = cmds.getAttr('%s.texture_searchpath' % self.def_arnOpts)
			except:
				tex_def_path = ''
			self.ar_param['ar_tex_search_path'] = \
				getDefaultStrValue(self_prefix, 'ar_tex_search_path', tex_def_path)

			#
			# Image parameters
			#
			self.img_param['img_filename'] = self.getImageFileNamePrefix()
			self.img_param['img_format'] = self.getImageFormat()

			#
			# Afanasy parameters
			#
			af_p = {
				"capacity": 1000,
				"deferred_capacity": 1000,
				"use_var_capacity": False,
				"cap_min": 1.0,
				"cap_max": 1.0,
				"max_running_tasks": -1,
				"max_tasks_per_host": -1,
				"service": "arnold",
				"deferred_service": "'mayatoarnold'",
				"os": "",
				"hostsmask": "",
				"hostsexcl": "",
				"depmask": "",
				"depglbl": ""
			}

			buf = af_p
			for ech in af_p:
				self.afanasy_param["af_%s" % ech] = set_field(ech, pref="af")

			self.deferred_prefix = '_deferred' if ap.get('deferred') else ''

	def getImageFileNamePrefix(self):
		""" Get image file prefix name from RenderGlobals or use just maya scene name
				if name is empty string 
		"""
		fileNamePrefix = cmds.getAttr('%s.imageFilePrefix' % self.def_RG)
		if fileNamePrefix in ['None', '']:
			fileNamePrefix = getMayaSceneName()
		return fileNamePrefix

	def getImageFormat(self):
		"""Get image format extensions

		return: string of image format extension
		"""
		imageFormatStr = str(cmds.getAttr('%s.imfPluginKey' % self.def_RG))
		#mel.eval('getImfImageType')
		if imageFormatStr in ['deepexr', 'maya']:
			imageFormatStr = 'exr'
		return imageFormatStr

	def get_3_post_path(self, scene_name):
		return '%s/in/%s/%s.%s' % (self.proj_path.replace('2_prod', '3_post'), self.version, scene_name, self.getImageFormat())

	def set_layer_name(self, layer):
		# Убрать имя слоя из пути, если слой один
		print('---->>>', layer)
		if not layer or 'default' in layer:
			layer = ''
		else:
			if 'rs_' in layer:
				#layer = 'rs_' + layer
				layer = layer.replace('rs_', '')
			layer += '/'
		return layer

	def generate_ass(self, layer_name=1, isSubmitingJob=False):
		"""generate_ass

		:param isSubmitingJob: if job assumed to be submited after .ass generation
		"""
		ass_reuse = self.ass_param['ass_reuse']

		# TODO!!! check if files are exist and have to be overwritten
		if isSubmitingJob and ass_reuse:
			print('Skipping .ass files generation ...')
			return

		exportAllRenderLayers = self.ass_param['ass_export_all_layers']
		animation = self.job_param['job_animation']
		start = self.job_param['job_start']
		stop = self.job_param['job_end']
		step = self.job_param['job_step']
		separator = self.job_param['job_separator']
		ass_selection = self.ass_param['ass_selection']
		ass_dirname = self.ass_param['ass_dirname']
		ass_padding = self.job_param['job_padding']
		# ass_perframe = self.ass_param['ass_perframe']
		ass_deferred = self.ass_param['ass_deferred']
		ass_binary = self.ass_param['ass_binary']
		ass_expand_procedurals = self.ass_param['ass_expand_procedurals']
		ass_export_bounds = self.ass_param['ass_export_bounds']

		ar_abs_tex_path = self.ar_param['ar_abs_tex_path']
		ar_abs_proc_path = self.ar_param['ar_abs_proc_path']
		# print(self.ar_param['ar_plugin_path'])
		ar_plugin_path = self.ar_param.get('ar_plugin_path') #.replace('toolsMulty\\', 'toolsMultyV0.2\\')
		ar_proc_search_path = self.ar_param.get('ar_proc_search_path') #.replace('toolsMulty\\', 'toolsMultyV0.2\\')
		ar_shader_search_path = self.ar_param.get('ar_shader_search_path')
		ar_tex_search_path = self.ar_param.get('ar_tex_search_path')

		filename, ext = self.scene_file.split('.')
		if ext != 'ass':
			filename += '.' + ext
		dirname = os.path.dirname(filename)
		if not os.path.exists(dirname):
			print('path %s not exists... create it!' % dirname)
			os.mkdir(dirname)

		if not animation:
			start = stop = int(cmds.currentTime(q=True))
			step = 1

			##if True:
			#
			# save RenderGlobals
			#

		saveGlobals = {}
		#
		# override RenderGlobals
		#
		sep_nom = job_separator_list.index(separator)
		def_RG = {
				'extensionPadding': ass_padding,
				'animation': 1,
				'outFormatControl': 0,
				'putFrameBeforeExt': 1,
				'periodInExt': sep_nom
			}
		def_arnOpts = {
				'binaryAss': ass_binary,
				'expandProcedurals': ass_expand_procedurals,
				'outputAssBoundingBox': ass_export_bounds,
				'absoluteTexturePaths': ar_abs_tex_path,
				'absoluteProceduralPaths': ar_abs_proc_path,
				'plugins_path': ar_plugin_path,
				'procedural_searchpath': ar_proc_search_path,
				#'shader_searchpath': ar_shader_search_path,
				'texture_searchpath': ar_tex_search_path,
				'output_ass_filename': ''
			}
		map(lambda x: cmds.setAttr(self.def_RG + '.' + x, def_RG[x]), def_RG)
		for ech_atr in def_arnOpts:
			_attr_name = self.def_arnOpts + '.' + ech_atr
			if '_' in ech_atr:
				cmds.setAttr(_attr_name, def_arnOpts[ech_atr], type='string')
			else:
				cmds.setAttr(_attr_name, def_arnOpts[ech_atr])

			#
			# Clear .output_ass_filename to force using default filename from RenderGlobals
			#
			##cmds.setAttr('%s.output_ass_filename' % self.def_arnOpts, '', type='string')
			##cmds.workspace(fileRule=('ASS', ass_dirname))
			# cmds.workspace( saveWorkspace=True )

		# save current layer
		##current_layer = cmds.editRenderLayerGlobals(q=True, currentRenderLayer=True)
		#image_name = self.getImageFileNamePrefix()
		##renderLayers = [current_layer]
		##if exportAllRenderLayers: layer_name = 1
		##	renderLayers = getRenderLayersList(True)  # renderable only

		#if not isSubmitingJob:
		#	print('=====================')
		#	print(layer_name)
		#	print(self.layer)

		if self.deferred_prefix:
				# if len(self.deferred_prefix):
				#print(self.filename)
				# generate unique maya scene name and save it
				# with current render and .ass generation settings
				print('Use deferred .ass generation')
				#self.deferred_prefix = '_deferred'
				cmds.setAttr(self.def_RG + '.imageFilePrefix', self.deferred_prefix, type='string')
				saveGlobals['imageFilePrefix'] = str(cmds.getAttr(self.def_RG + '.imageFilePrefix'))
				#cmds.setAttr(self.def_RG + '.imageFilePrefix', dirname, type='string')  # self.filename

				# get scene name without extension
				#scene_name = getMayaSceneName(withoutSubdir=False)
				#print('562 -------', scene_name.split('/')[-1])
				#def_scene_name = '%s_deferred' % scene_name
				#print('11', filename)
				#print('22', self.filename, '33', self.scene_file, self.scene_name)

		#print('_', filename)
		#print('__', self.scene_name)
		#pth_ =  self.scene_name.split('.')
		#print(pth_, self.deferred_prefix)
		if self.scene_file != filename:
			#Если есть расширение
			#print(cmds.getAttr(self.def_RG + '.imageFilePrefix'))
			#cmds.setAttr(self.def_RG + '.imageFilePrefix', self.deferred_prefix, type='string')
			#saveGlobals['imageFilePrefix'] = str(cmds.getAttr(self.def_RG + '.imageFilePrefix'))

			cmds.file(rename=(filename + self.deferred_prefix))
			#print(def_scene_name)
			# save it with default extension
			cmds.file(save=True, de=True)
			#print(def_scene_name)
			# rename scene back
			#cmds.setAttr(self.def_RG + '.imageFilePrefix', self.deferred_prefix)
			#cmds.file(rename=pth_[0])
			#cmds.file(save=True, de=True)
			#cmds.setAttr(self.def_RG + '.imageFilePrefix', saveGlobals['imageFilePrefix'], type='string')

		##for layer in renderLayers:
		##if not layer_name or layer_name == 'defaultRenderLayer':
		##	layer_name = 1


		#try:
		#	saveGlobals['renderableLayer'] = str(cmds.getAttr(layer_name + '.renderable'))
		#except:
		#	if layer_name == 'masterLayer':
		#		layer_name = 'defaultRenderLayer'
		#layer_name = self.set_layer_name(layer_name)
		#print(layer_name)
		if 'master' in layer_name:
			layer_name = 'defaultRenderLayer'
		saveGlobals['renderableLayer'] = str(cmds.getAttr(layer_name + '.renderable'))

		#print(layer_name, 'status')
		cmds.setAttr(layer_name + '.renderable', True)
		cmds.editRenderLayerGlobals(currentRenderLayer=layer_name)

		# print(self.get_assgen_options(layer), '---]')
		# assgen_cmd = self.app.deferredCmdLine % (
		#			 TOOLS_PATH, self.rootDir, 1, current_layer, self.get_image_name())#(self.get_assgen_options(layer), start, stop, step)
		#			 (self.get_assgen_options(layer_name) % {'ARNOLD_PLUGIN_PATH': ''} +
		assgen_cmd = self.app.assgen_cmd % (self.ass_dirname, self.ep, self.shot, self.app.assgen_ml, start, stop, step)
		#if 'default' in layer_name:1
		#assgen_cmd = assgen_cmd.replace('<Scene>', '<RenderLayer>/<Scene>')
		return assgen_cmd

	def getRenderCmd(self, layer=None):
		"""Get command for render

		:param layer: current render layer
		:return: string for render command
		"""
		#def_scene_path = os.path.abspath(os.path.dirname(self.def_scene_name) + '\..').replace('\\', '/')
		#if self.ass_param['ass_deferred']:
		#	render_line = self.app.deferredCmdLine
		#else:
		#render_line = self.app.renderCmdLine
		#print(self.get_assgen_options(layer))
		#gen_cmd = ''
		#gen_options = []
		layer = self.set_layer_name(layer)
		ag_options = self.get_assgen_options() % ({"ARNOLD_PLUGIN_PATH": ""})
		#print('defer_status', self.deferred_prefix)
		if self.deferred_prefix:
			#gen_options.append(self.get_ass_name(layer))
			gen_options = [self.curr_render, self.proj_path, layer, ag_options]
			gen_cmd = self.app.deferredCmdLine()
		else:
			gen_options = [ag_options, self.get_image_name()[0]]
			gen_cmd = self.app.renderCmdLine()

		#print(gen_cmd)
		#print(gen_options)
		print('===========')
		gen_cmd_line = gen_cmd %  tuple(gen_options)  #% (layer, ';'.join(self.get_image_name()))
		print(gen_cmd_line)
		#print('====>>>', ";".join(self.get_image_name()))
		ar_verbosity_level = ar_verbosity_list.index(self.ar_param['ar_verbosity'])
		options = str(self.ar_param['ar_options']).strip()
		ar_threads = self.ar_param['ar_threads']

		if ar_verbosity_level < 2:
			ar_verbosity_level = 2

		# cmd_buffer = ['python ', TOOLS_PATH + '/arnold/kick.py']
		# app_line = '%s/arnold/ext/windows/mtoadeploy/2018/bin/kick%s' % (TOOLS_PATH, self.ext)
		# app_line = '%s/arnold/ext/windows/mtoadeploy/2018/bin/kick%s -nstdin -dw -dp' \
		# app_line = self.app.kickCmd #% TOOLS_PATH
		# self.def_kickCommand % TOOLS_PATH
		# cmd_buffer = [app_line]
		# ['Y:/toolsMultyV0.2/StudioTools/arnold/ext/windows/mtoadeploy/2018/bin/kick.exe']
		# cmd_buffer = ['python', '//loco000/tools/toolsMulty/StudioTools/arnold/kick.py']
		#if ar_verbosity_level != 0:
		#	# cmd_buffer.append(
		#	flags += ' -v %s' % ar_verbosity_level

		#if ar_threads != 0:
		#	# cmd_buffer.append(
		#	flags += ' -t %s' % ar_threads

		# cmd_buffer.append('-nstdin -dw -dp -nocrashpopup -i')
		# !!! -nocrashpopup flag doesn't support in kick linux version

		#app_line = self.app.renderCmd % (TOOLS_PATH, flags, self.get_image_name())
		# cmd_buffer.append('-o "%s" -i ' % self.get_image_name()) #self.outputPathForKick())
		if options != "":
			gen_cmd_line += options
			#	cmd_buffer.append(options)
			# return ' '.join(cmd_buffer) #).replace('rs_layer_', 'layer_')
		return gen_cmd_line

	def preview_cmd_line(self):
		print('prev')

	def submitJob ( self, param=None ) :
		"""
		submitJob

		:param param: dummy parameter
		"""

		def _preview(layer_name, prev_blck, index=0):
						data = [{
							'parser': 'movgen'
						}, {
							'title': 'for Shotgun',
							'preview_title': 'shotgun_',
							'parser': 'shotgun'
						}]
						print('Making preview %s...' % data[index].get('title', ''))
						block_title = 'preview_%s'
						if index == 0:
							block_title = data[index].get('preview_title', '') + block_title
						prev_blck = af.Block(block_title  % layer_name, data[index].get('parser'))
						prev_blck.setWorkingDirectory(self.job.work_dir)
						prev_blck.setFiles(self.get_image_name())
						prev_blck.input_files = self.get_ass_name(layer_name)
						#prev_blck.setDependMask(prev_blck.af_block.data["name"])

						#reload(prvc)
						prev_tsk = af.Task(self.job.name)
						currentScenePath = cmds.file(q=True, sn=True, expandName=True)
						prev_cmd = prvc.getCommand(currentScenePath, layer * exportAllRenderLayers,
												  self.job.work_dir, self.get_ass_name(layer_name),
												  self.job.af_job.data["user_name"])
						if not len(prev_cmd):
							# check ugly guard
							cmds.error('Error: Incorrect scene name or scene path')
							return None, None
						prev_tsk.setCommand(prev_cmd[0])
						prev_blck.tasks.append(prev_tsk)

						##	self.job.frames_blocks.append(prev_blck)
						# Append preview to job:
						self.job.af_job.blocks.append(prev_blck)
						return prev_blck, prev_cmd

		def make_preview(layer_name, prev_blck):
						print('Making preview ...')
						# if (cmds.checkBoxGrp('make_preview',q = True, v1 = True)):
						block_title = 'preview_%s'
						prv_block = af.Block(block_title % layer_name, 'movgen')
						prv_block.setWorkingDirectory(self.job.work_dir)
						prv_block.setFiles(self.get_image_name())
						prv_block.input_files = self.get_ass_name(layer_name)

						# Set depend mask of render task:
						prv_block.setDependMask(prev_blck.af_block.data["name"])

						# Generate preview task
						#reload(prvc)
						prv_task = af.Task(self.job.name)

						# Set block tasks command and append to preview block
						currentScenePath = cmds.file(q=True, sn=True, expandName=True)
						prv_commands = prvc.getCommand(currentScenePath, layer * exportAllRenderLayers,
												   self.job.work_dir, self.get_ass_name(layer_name),
												   self.job.af_job.data["user_name"])

						if not len(prv_commands):
							# check ugly guard
							cmds.error('Error: Incorrect scene name or scene path')
							return
						prv_task.setCommand(prv_commands[0])
						prv_block.tasks.append(prv_task)
						#print(prv_commands)

						# Append preview to job:
						self.job.af_job.blocks.append(prv_block)
						return prv_block, prv_commands

		def make_preview_shotgun(layer_name, prv_commands):
			if (self.job_param['job_preview_shotgun']):
				# ----------------- Generate shtgun block:
				print('Making preview for Shotgun ...')
				# shtgn_block = af.Block('shotgun_preview_%s' % layer_name, 'generic')
				shtgn_block = af.Block('shotgun_' + 'preview_%s' % layer_name, 'shotgun')
				# shtgn_block.setParser('generic')
				shtgn_block.setWorkingDirectory(self.job.work_dir)
				shtgn_block.setDependMask(shtgn_block.data["name"])
				shtgn_task = af.Task(self.job.name)
				shtgn_task.setCommand(prv_commands[1])
				shtgn_task.input_files = self.get_image_name()
				# shtgn_task.service('shotgun')

				shtgn_block.setTasksMaxRunTime(30)  # secs for task to error
				shtgn_block.setErrorsRetries(10)  # times to try
				shtgn_block.setErrorsAvoidHost(4)  # max times to try per host

				# Append shtgn task to  block:
				shtgn_block.tasks.append(shtgn_task)

				# Append send preview to Shotgun to job:
				self.job.af_job.blocks.append(shtgn_block)
			# -------------------------------------------------------

		class JobMaker(AfanasyRenderJob):

			def __init__(self):
				#super(AfanasyRenderJob, self).__init__()
				super(JobMaker, self).__init__()
				print("Job's settings up ...")
				#self.dispatcher = self.job_param['job_dispatcher']
				#self.description = self.job_param['job_description']
				self.name = str(self.job_param['job_name']).strip()
				#if self.name == '':
				#	self.name = getMayaSceneName()
				self.cleanup_ass = self.job_param['job_cleanup_ass']
				self.cleanup_maya = self.job_param['job_cleanup_maya']
				self.cleanup_yeti = self.job_param['job_cleanup_yeti']

			@property
			def dispatcher(self):
				return self.job_param.get('job_dispatcher')

			@property
			def description(self):
				return self.job_param.get('job_description')

			#@property
			#def name(self):
			#	jn = self.job_param.get('job_name')
			#	if jn:
			#		return str(jn.strip())
			#	else:
			#		return getMayaSceneName()

			def make_task(self, name):
				self.name = name

			def make_frame(self, range):
				self.range = range

		print('Job submiting ...')
		##mel.eval('setProject "%s"' % self.proj_path.replace('\\', '/'))
		##print('Set current project\'s path to %s.' % self.proj_path)
		job_dispatcher = self.job_param['job_dispatcher']
		job_description = self.job_param['job_description']
		job_name = str(self.job_param['job_name']).strip()
		#if job_name == '':
		#	job_name = getMayaSceneName()
		job_cleanup_ass = self.job_param['job_cleanup_ass']
		job_cleanup_maya = self.job_param['job_cleanup_maya']
		job_cleanup_yeti = self.job_param['job_cleanup_yeti']

		ass_deferred = self.ass_param['ass_deferred']
		ass_local_assgen = self.ass_param['ass_local_assgen']
		ass_def_task_size = self.ass_param['ass_def_task_size']
		ass_reuse = self.ass_param['ass_reuse']
		exportAllRenderLayers = self.ass_param['ass_export_all_layers']
		#print('sloi')
		#print(exportAllRenderLayers)

		if job_dispatcher == 'afanasy':
			self.job = AfanasyRenderJob(job_name, job_description)
			# self.job = JobMaker(...)  # альтернатива
			self.job.use_var_capacity   = self.afanasy_param['af_use_var_capacity']
			self.job.capacity_coeff_min = self.afanasy_param['af_cap_min']
			self.job.capacity_coeff_max = self.afanasy_param['af_cap_max']
			self.job.max_running_tasks  = self.afanasy_param['af_max_running_tasks']
			self.job.max_tasks_per_host = self.afanasy_param['af_max_tasks_per_host']
			self.job.hostsmask = str(self.afanasy_param['af_hostsmask']).strip()
			self.job.hostsexcl = str(self.afanasy_param['af_hostsexcl']).strip()
			self.job.depmask = str(self.afanasy_param['af_depmask']).strip()
			self.job.depglbl = str(self.afanasy_param['af_depglbl']).strip()
			self.job.need_os = str(self.afanasy_param['af_os']).strip()

			service = str(self.afanasy_param['af_service']).strip()
			capacity = self.afanasy_param['af_capacity']
			deferred_service = str(self.afanasy_param['af_deferred_service']).strip()
			deferred_capacity = self.afanasy_param['af_deferred_capacity']

		elif job_dispatcher == 'backburner':
			print('backburner not supported in this version')
			# self.job = ArnoldBackburnerJob ( job_name, job_description )
			return
		else:
			ass_deferred = False
			self.job = RenderJob(job_name, job_description)

		#self.rootDir = self.def_scene_name.split('/render')[0]
		#if self.def_scene_name:
		#print('wrkd')
		#print(cmds.file(q=True, sn=True))
		#print(os.path.abspath(os.path.dirname(self.def_scene_name) + '/..'))
		self.job.work_dir = self.proj_path #os.path.abspath(os.path.dirname(cmds.file(q=True, sn=True)) + '/..' + '/..')
		self.job.padding = self.job_param['job_padding']
		self.job.priority = self.job_param['job_priority']
		self.job.paused = self.job_param['job_paused']
		self.job.task_size = self.job_param['job_size']
		self.job.animation = self.job_param['job_animation']
		self.job.start = self.job_param['job_start']
		self.job.stop = self.job_param['job_end']
		self.job.step = self.job_param['job_step']

		#print('wd = %s' % self.job.work_dir)
		#self.rootDir = self.def_scene_name.split('/render')[0]
		#self.job.work_dir = self.rootDir
		#cmds.setWorkingDirectory(self.rootDir)
		#print('746 wrr_d2 = %s' % self.job.work_dir)

		# save current layer
		current_layer = \
			cmds.editRenderLayerGlobals(
				q=True,
				currentRenderLayer=True
			)

		print('rl ', current_layer, self.layer)
		renderLayers = []
		if exportAllRenderLayers:
			renderLayers = getRenderLayersList(True)  # renderable only
			if len(renderLayers) == 1:
				# if not exportAllRenderLayers:
				##layer = 'defaultRenderLayer'
				self.app.assgen_ml = ''
		else:
			renderLayers.append(current_layer)

		#if job_dispatcher == 'afanasy':
		print(renderLayers, '!!!', self.app.assgen_ml)
		# Перебор всех слоев
		for layer in renderLayers:
				cmds.editRenderLayerGlobals(currentRenderLayer=layer)
				layer_name = layer
				if layer == 'defaultRenderLayer':
					layer_name = 'masterLayer'
				#print (layer)
				#self.job.setup()
				#print('+++++++++')
				#print(self.get_ass_name(True, layer_name))
				#print(self.get_image_name())
				#print(self.filename)

				files_to_rend = ''
				frame_block = \
					AfanasyRenderBlock(
						'render_%s' % layer_name,
						service,
						self.job
					)

				#if len(self.deferred_prefix) > 0 and not ass_reuse:
				#print('lyr ... ', layer)
				#self.deferred_prefix = ''
				files_to_rend = self.get_ass_name(True, layer_name)
				if ass_deferred and not ass_reuse:
					print('Using deferred generation ...')
					#self.deferred_prefix = '_deferred'
					##if exportAllRenderLayers:
					##	#	gen_cmd = self.getDeferredCmd(None)
					##	# else:
					##	#	gen_cmd = self.getDeferredCmd(current_layer)
					##	layer = 1
					#gen_cmd = self.app.deferredCmdLine() % (
					#	TOOLS_PATH, self.rootDir, 1, current_layer, self.get_image_name())
					##gen_cmd = self.app.deferredCmdLine() % (1, layer_name)
					#gen_cmd += self.get_assgen_options(layer_name)
					# gen_cmd += ' -s @#@'
					# gen_cmd += ' -e @#@'
					# gen_cmd += ' -b %s' % self.job.step
					# print('867 ', self.def_scene_name)

					self.job.setup_range(int(cmds.currentTime(q=True)))
					self.job.setup()
					job_block = \
						AfanasyRenderBlock('generate_ass',
										   deferred_service,
										   self.job,
										   ass_local_assgen)
					job_block.capacity = deferred_capacity
					job_block.task_size = min(ass_def_task_size, self.job.num_tasks)

					# Set block Post command for cleanup
					job_block.cmd = self.getRenderCmd(layer_name)
					job_block.input_files = '"%s"' % files_to_rend

					#job_block.input_files = '"%s"' % self.scene_file
					#print('defer -->')
					#print(files_to_rend)
					#print(self.job.gen_block)
					#print(self.job.gen_block.parentJob)
					#print(dir(self.job.gen_block.parentJob))
					#print(self.job.gen_block.parentJob.af_job.blocks)
					#print(self.job.gen_block.parentJob.af_job.data)
					#print(self.job.gen_block.af_block.data)
					#print(self.job.gen_block.af_block.tasks)
					#print(self.job.gen_block.parentJob)
					#self.job.gen_block.out_files = files_to_rend

					#print(self.job.gen_block.af_block.__dict__)
					#print('862', ass_gen_res)
					#print(self.job.gen_block.af_block)
					##self.job.gen_block.files = self.get_image_name()
					#print(ass_gen_res)
					#fLst = mel.eval(ass_gen_res)
					#print(fLst)

					###if job_cleanup_maya:
					###	self.job.gen_block.af_block.setCmdPost(self.app.cleanup_cmd + self.job.gen_block.input_files)
					self.job.gen_block = job_block
					self.job.gen_block.setup()
					#self.job.setup_range(int(cmds.currentTime(q=True)))
					#print(self.job.gen_block.af_block, self.job.gen_block.input_files)

					#if exportAllRenderLayers:
					#frame_block = \
					#			AfanasyRenderBlock(
					#				'gen_ass_%s' % layer,
					#				service,
					#				self.job
					#			)
					#frame_block.name = 'gen_ass_%s' % layer
					#frame_block.service = deferred_service
					#frame_block.capacity = deferred_capacity
					#frame_block.out_files = self.get_image_name()[0]  # self.get_ass_name(True, layer_name)
					#print(dir(frame_block.af_block.gen_block))
					#print(self.job.frames_blocks)
					#frame_block.setup()
					#self.job.frames_blocks.append(frame_block)

					###if layer != 'defaultRenderLayer':
					###	self.job.af_job.blocks.append(job_block)
				else:
					ass_gen_res_line = self.generate_ass(layer_name=layer_name, isSubmitingJob=True)
					#print('ga', layer, ass_gen_res_line)
					ass_gen_res = mel.eval(ass_gen_res_line)
					# isSubmitingJob=True
					print('active_res:')
					if len(ass_gen_res) > 1:
						files_to_rend = ";".join(ass_gen_res)
						#.replace('defaultRenderLayer/', '')
					elif ass_gen_res:
						files_to_rend = ass_gen_res[0]
					print(files_to_rend)

				#print('next job: render', layer)
				self.deferred_prefix = ''
				frame_block.capacity = capacity
				frame_block.input_files = '"%s"' % self.get_ass_name(True, layer_name)
				frame_block.out_files = self.get_image_name()[0]     #self.get_ass_name(True, layer_name)
				frame_block.cmd = self.getRenderCmd(layer_name)    #.replace('defaultRenderLayer/', '')   #mel.eval(self.app.assgen_cmd)
				#if img_name:

				#frame_block.out_files = self.get_image_name()[0]
				#print('frm', frame_block.__dict__)
				#frame_block.out_files = self.get_ass_name() # self.get_ass_name(True, layer_name)
				#frame_block.af_block.setFiles(self.get_image_name()[0]) #[files_to_rend])  # = ";".join()
				# .replace('maya', 'exr') #self.outputPathForKick(self.get_image_name())

				# Set block Post command for cleanup
				if job_cleanup_ass:
						import re
						# replace frame number '@####@' with '*' wildcard
						input_files = re.sub('@[#]+@', '*', frame_block.input_files)
						frame_block.af_block.setCmdPost(self.app.cleanupCmd + input_files)

				frame_block.setup()
				self.job.frames_blocks.append(frame_block)

				# -----------------generate preview block --------------
				# self.job_param['job_preview'] = \
				# getDefaultIntValue(self_prefix, 'job_preview', 1) is 1

				# self.job_param['job_preview_shotgun'] = \
				# getDefaultIntValue(self_prefix, 'job_preview_shotgun', 1) is 1

				if self.job_param['job_preview']:
					prv_block, prv_commands = _preview(layer, frame_block)
					if prv_commands:
						make_preview_shotgun(layer, prv_commands)

				if self.job_param['job_new_user']:
					self.job.af_job.data["user_name"] = self.job_param['job_new_user']
					print('New user was set: %s' % self.job.af_job.data["user_name"])
				self.job.process()

		#if exportAllRenderLayers:
		#		# restore current layer
		#		cmds.editRenderLayerGlobals(currentRenderLayer=current_layer)
		print('Job submited.')

	def get_ass_name(self, suffix=True, layer=None, decorator='@'):
		"""get_ass_name

		:param suffix: if it is True -- return pattern string name name.@###@.ass
									 otherwise -- return just .ass filename
		:param layer: current render layer
		:param decorator: symbol for padding string decoration
		:return: .ass filename 
		"""
		#ass_deferred = self.ass_param['ass_deferred']
		separator = self.job_param['job_separator']
		if separator == 'none':
			separator = ''

		#filename = '' #self.ass_dirname   #param['ass_dirname'] #self.ass_param['ass_dirname']
		#if layer is not None:
		#	if layer == 'defaultRenderLayer':
		#		layer = 'masterLayer'
		#else:
		#print('993 filename', self.layer, layer)
		#print('929 filename', filename)
		#print('930 filename', self.rootDir)
		#print('931 filename', self.ass_dirname)
		#print('932 filename', self.ass_param['ass_dirname'])

		#deferred_prefix = '_deferred' if self.ass_param['ass_deferred'] else ''
		#cmds.setAttr('%s.imageFilePrefix' % self.def_RG, deferred_prefix, type="string")
		#print('show_res', self.layer,
		#cmds.setAttr('%s.imageFilePrefix' % self.def_RG, '<Scene>%s' % self.deferred_prefix, type="string")
		#scenename = getMayaSceneName()
		print('Checking scene: %s ...' % self.scene_name)
		#if len(scenename.split('_')) != 4:
		#	return "Wrong scene's name: %s" % scenename
		#ep, shot, step, version_ext = scenename.split('_')
		#version = version_ext.split('.')[0]

		#слеяние имени файла с именем слоя
		##if not layer or layer == 'defaultRenderLayer':
		##	layer = 1
		##	layer_name = ''
		##else:
		##	#if len(getRenderLayersList(False)) > 1:
		layer = self.set_layer_name(layer)
		#if layer:
		#	layer += '/'
		#	if '_' in layer:
		#		layer = layer.replace('rs_', '')
		ass_name_mask = '{ass_dirname}/{ep}/{shot}/{layer}{scene_name}'
		filename = '/'.join([self.ass_dirname, self.ep, self.shot, layer + self.scene_name.split('.')[0]])

		#filename += '.ass'
		##scn_prts = scenename.split('.')
		##if len(scn_prts) == 2:
		##	scenename = scn_prts[0]
		#print('774 msn2: %s' % scenename)

		##deferred_suff = '%s_deferred' if ass_deferred else '%s'
		#elif 'deferred' in scenename:
		#	scenename = scenename.split('_deferred')[0]
		#	#self.def_scene_name = self.filename.split('.')[0]
		# scenename, ext = os.path.splitext(
		#	os.path.basename(self.def_scene_name)
		# )
		##print('1010: %s' % scenename)
		##self.def_scene_name = deferred_suff % scenename
		#filename += '/'.join(scenename.split('_')[:3]
		##filename += '/'.join([''] + scenename.split('_')[:2] +[scenename])
		print('1016 file: %s' % filename)
		#print('1017 file: %s' % filename)
		#scene_path = cmds.workspace(expandName=scenename)
		file_path = cmds.workspace(expandName=filename)
		file_path += self.deferred_prefix
		# = filename.split('_deferred')[0]
		if suffix:
			pad_str = getPadStr(self.job_param['job_padding'], True)

			# if self.ass_param['ass_perframe'] :
			#print(decorator, separator, pad_str, '.ass')
			#print('orig  ', filename)
			#print('orig2  ', self.filename)
			file_path += decorator.join([separator, pad_str, '.ass'])
			#print('793 file: %s' % filename)
			# else :
			#	filename += '.ass'
			if self.ass_param['ass_compressed']:
				# !!! There is error in maya translator
				# for "defaultArnoldRenderOptions.output_ass_compressed" flag
				if not self.deferred_prefix:
					file_path += '.gz'

		#print(file_path)
		_filename = ass_name_mask.format(ass_dirname=self.ass_dirname, ep=self.ep, shot=self.shot,
										 layer=layer, scene_name=self.scene_name)
		#print(_filename)
		#filename += ' -filename "%s"' % filename
		return file_path

	def get_image_name(self):
		"""get_image_name
		"""
		import re
		pad_str = getPadStr(self.job_param['job_padding'], True)

		images = cmds.renderSettings(
			fullPath=True,
			genericFrameImageName=('@%s@' % pad_str)
		)
		# imageFileName = ';'.join (images)
		for ech_img in images:
			ext = re.findall('\.(\W+)\$', ech_img)
			if ext: ext = ext[0]
			#if ext:
			#fileName = fileName.split('/render')[-1]
			# print(os.path.join(*(ifn_parts[:-4] + ifn_parts[-1].split('_')[:2])))
		  	#print(ext, fileName)
		  	if ext != 'exr': continue
			img_dir = os.path.dirname(ech_img)
			img_dir_in = img_dir + 'in/'
			##file_ext = fileName.split('_')
			##vlu = re.findall('(\w+[0-9]+_\w+[0-9]+)', fileName)[0]
			##vlu = vlu.replace('_', '/')
			version = re.findall('_(v[0-9]+)\.', ech_img)
			if version:
					version = version[0]
					img_dir_ver = img_dir_in + version

					#print (version)
					#version = 'v' + imageFileName.split('_v')[1]
					##to_repl = re.findall('(\w+[0-9]+/sh[0-9]+)', fileName)[0]
					##print(ver, to_repl)

					##ifn = fileName.replace(to_repl, vlu)
					if '3_post' in ech_img:
						print (ech_img)
						images[images.index(ech_img)] = self.get_3_post_path(ech_img) #fromNativePath(ech_img.replace('in/', 'in/%s/' % version))

				#print('848 self.filename', self.filename)
				#print('849 self.filename', os.path.basename(self.filename))

			##imageFileName = ifn + '.' + self.getImageFormat()
			#imgDir = os.path.dirname(ech_img)
			#print(self.filename, ech_img, os.path.basename(self.filename))
			#if not os.path.exists(imgDir + '/' + os.path.basename(self.filename)):
			#	os.makedirs(imgDir)

		#return fromNativePath(ech_img)
		return images

	def get_assgen_options ( self, layer=None ) :
		"""get_assgen_options

		:param layer:
		:return:
		"""
		print('Getting options ...')
		animation = self.job_param['job_animation']
		start = self.job_param['job_start']
		stop = self.job_param['job_end']
		step = self.job_param['job_step']
		ass_reuse = self.ass_param['ass_reuse']
		ass_selection = self.ass_param['ass_selection']
		ass_dirname = self.ass_param['ass_dirname']
		ass_padding = self.job_param['job_padding']
		# ass_perframe = self.ass_param['ass_perframe']
		ass_deferred = self.ass_param['ass_deferred']

		ass_binary = self.ass_param['ass_binary']
		ass_compressed = self.ass_param['ass_compressed']
		ass_expand_procedurals = self.ass_param['ass_expand_procedurals']
		ass_export_bounds = self.ass_param['ass_export_bounds']

		ar_abs_tex_path = self.ar_param['ar_abs_tex_path']
		ar_abs_proc_path = self.ar_param['ar_abs_proc_path']

		ar_plugin_path = self.ar_param['ar_plugin_path'] #.replace('toolsMulty\\S', 'toolsMultyV0.2\\S')
		ar_proc_search_path = self.ar_param['ar_proc_search_path'] #.replace('toolsMulty\\S', 'toolsMultyV0.2\\S')
		#ar_shader_search_path = self.ar_param['ar_shader_search_path']
		ar_tex_search_path = self.ar_param['ar_tex_search_path']

		assgen_cmd = ''
		# ass_dirname =  '\\\\omega\\moriki-doriki\\2_prod\\ep001\\sh046\\ass'  #'//omega/moriki-doriki/temp/maya/data'

		#filename = cmds.workspace(expandName=ass_dirname)
		#print('866_fname: %s' % filename)
		#filename, ext = os.path.splitext(filename)
		#if ext in ['', '.']:
		#	ext = '.ass'

		#print('871_curr_ext: %s' % ext)
		#if layer is not None:
		#	layer_in_filename = layer
		#	if layer == 'defaultRenderLayer':
		#		layer_in_filename = 'masterLayer'
		#else:
		#	layer = 1
		#filename += ext #'_' + layer_in_filename
		#assgen_cmd += ' -rl %s' % layer

		if ass_deferred:
			#assgen_cmd = ' -r arnold'
			# if 'rs_' in layer_in_filename:
			#	layer_in_filename = layer_in_filename.replace('rs_', '')
			# if 'layer_' in layer_in_filename:
			#	layer_in_filename = layer_in_filename.replace('rs_', '')
			# filename += '_%s_deferred' % (layer_in_filename + ext)
			if ass_binary:
				assgen_cmd += ' -ai:bass 1'
			if ass_export_bounds:
				assgen_cmd += ' -ai:exbb 1'
			assgen_cmd += ' -ai:lve 1'
			assgen_cmd += ' -ai:sppg "%(ARNOLD_PLUGIN_PATH)s"'  # ' + ar_plugin_path + '"'
			assgen_cmd += ' -ai:sppr ""'  #' + ar_proc_search_path + '"'
			assgen_cmd += ' -ai:spsh ""'  #' + ar_shader_search_path + '"'
			assgen_cmd += ' -ai:sptx ""'  #+ ar_tex_search_path + '"'

			# assgen_cmd += ' -filename \"' + filename + '\"'
			# print('-----filename-----')
			# print(self.get_ass_name(False, layer_in_filename))
			# print('==')
			# assgen_cmd +=
			#filename += '_deferred'
		else:
			# if '_' in layer_in_filename:
			#	layer_in_filename = layer_in_filename
			# for deferred generation we do not add layer name to filename
			# this will be done by Maya
			if ass_compressed:
				assgen_cmd += ' -compressed'

			if not ass_binary:
				assgen_cmd += ' -asciiAss'

			if ass_selection:
				assgen_cmd += ' -selected'

			if ass_expand_procedurals:
				assgen_cmd += ' -expandProcedurals'

			if ass_export_bounds:
				assgen_cmd += ' -boundingBox'

			flags = ''
			flag_set = {'verbosity_level': ('t', 0), 'threads': ('v', 2)}
			for flag in flag_set:
				if flag_set[flag][0]:
					flags += ' -%s %s' % flag_set[flag]
			#gen_cmd += flags
			assgen_cmd += flags

			#self.filename = self.get_ass_name(False, layer)

		#if '###' in self.filename:
		#	self.filename.replace('#', '').replace('..', '.')
		#print('939', self.filename)
		#assgen_cmd += ' -filename "%s"' % self.filename
		return assgen_cmd

	def jobFileNameOptionsChanged(self, name, value):
		"""jobFileNameOptionsChanged

		:param name:
		:param value:
		"""
		if name == 'job_padding':
			setDefaultIntValue(self.prefix, name, self.job_param, value)
			cmds.setAttr('%s.extensionPadding' % self.def_RG, value)
		elif name == 'job_separator':
			setDefaultStrValue(self.prefix, name, self.job_param, value)
			cmds.setAttr('%s.outFormatControl' % self.def_RG, 0)
			if value == 'none':
				cmds.setAttr('%s.periodInExt' % self.def_RG, 0)
			elif value == '.':
				cmds.setAttr('%s.periodInExt' % self.def_RG, 1)
			else:
				cmds.setAttr('%s.periodInExt' % self.def_RG, 2)
		self.setResolvedPath()

	def assDirNameChanged(self, name, value):
		"""assDirNameChanged

		:param name:
		:param value:

		"""
		if name == 'ass_dirname':
			setDefaultStrValue(self.prefix, name, self.ass_param, value)
			cmds.workspace(fileRule=('ASS', value))
			# cmds.workspace( saveWorkspace=True )
		elif name == 'ass_compressed':
			setDefaultIntValue(self.prefix, name, self.ass_param, value)
			cmds.setAttr('%s.output_ass_compressed' % self.def_arnOpts, value)
		else:  # ass_padding, ass_perframe,
			setDefaultIntValue(self.prefix, name, self.ass_param, value)
		self.setResolvedPath()

	def setResolvedPath(self):
		"""Displays preview of current .ass full filename in textFieldGrp
		"""
		self.deferred_prefix = '_deferred' if self.ass_param['ass_deferred'] else ''
		self.filename = self.get_ass_name(True, self.layer, '')
		#print(self.filename)
		#cmds.setAttr('%s.imageFilePrefix' % self.def_RG, self.deferred_prefix, type="string")
		#print('show_res', self.layer, cmds.getAttr('%s.imageFilePrefix' % self.def_RG))

		cmds.textFieldGrp(
			'%s|f0|t0|tc1|fr2|fc2|ass_resolved_path' % self.winMain,
			edit=True,
			text=self.filename
		)

	def enable_range(self, arg):
		"""enable_range

		:param arg: Missing documentation of arg argument
		:return:
		"""
		setDefaultIntValue(self.prefix, 'job_animation', self.job_param, arg)
		cmds.intFieldGrp(
			'%s|f0|t0|tc0|fr1|fc1|job_range' % self.winMain,
			edit=True,
			enable=arg
		)

	def enable_var_capacity(self, arg):
		"""enable_var_capacity

		:param arg: True if enable_var_capacity
		:return:
		"""
		setDefaultIntValue(
			self.prefix,
			'af_use_var_capacity',
			self.afanasy_param,
			arg
		)

		cmds.floatFieldGrp(
			'%s|f0|t0|tc3|fr1|fc1|af_var_capacity' % self.winMain,
			edit=True,
			enable=arg
		)

	def enable_deferred(self, arg):
		"""enable_deferred

		:param arg: True if enabled
		"""
		ass_def_frame = self.winMain + '|f0|t0|tc1|fr3'
		ass_def = ass_def_frame + '|fc3|'
		ass_compressed = self.winMain + '|f0|t0|tc1|fr1|fc1|ass_compressed'

		setDefaultIntValue(
			self.prefix,
			'ass_deferred',
			self.ass_param,
			arg
		)
		#self.deferred_prefix = '_deferred' if arg else ''
		cmds.setAttr('%s.imageFilePrefix' % self.def_RG, '<Scene>%s' % self.deferred_prefix, type="string")

		cmds.checkBoxGrp(
			ass_def + 'ass_local_assgen',
			edit=True,
			enable=arg
		)
		cmds.intFieldGrp(
			ass_def + 'ass_def_task_size',
			edit=True,
			enable=arg
		)
		cmds.checkBoxGrp(
			ass_compressed,
			e=True,
			enable=not arg
		)

		bg_color = self.save_frame_bgc
		if arg:
			bg_color = self.def_frame_bgc

		cmds.frameLayout(
			ass_def_frame,
			edit=True,
			bgc=bg_color
		)  # , enableBackground=False

		self.setResolvedPath()

	def set_needed_os(self, arg):
		"""set_needed_os

		:param arg: dummy boolean value
		"""
		af_os_chk = self.winMain + '|f0|t0|tc3|fr1|fc1|af_os'

		val1 = cmds.checkBoxGrp(af_os_chk, q=True, value1=True)
		val2 = cmds.checkBoxGrp(af_os_chk, q=True, value2=True)
		val3 = cmds.checkBoxGrp(af_os_chk, q=True, value3=True)
		af_os_lst = []
		if val1:
			af_os_lst.append('windows')
		if val2:
			af_os_lst.append('linux')
		if val3:
			af_os_lst.append('mac')
		af_os_str = '|'.join(af_os_lst)

		setDefaultStrValue(self.prefix, 'af_os', self.afanasy_param, af_os_str)

	def onRenderLayerSelected(self, arg):
		"""onRenderLayerSelected

		:param arg: selected render layer
		"""
		#
		self.layer = arg
		if self.layer == 'masterLayer':
			arg = 'defaultRenderLayer'

		# print '* onRenderLayerSelected %s' % self.layer

		cmds.evalDeferred(
			'import maya.OpenMaya; '
			'maya.cmds.editRenderLayerGlobals('
			'   currentRenderLayer = "%s"'
			')' % arg,
			lowestPriority=True
		)

	def renderLayerSelected(self):
		"""renderLayerSelected
		"""
		self.layer = cmds.editRenderLayerGlobals(
			query=True,
			currentRenderLayer=True
		)

		if self.layer == 'defaultRenderLayer':
			self.layer = 'masterLayer'

		cmds.optionMenuGrp(
			self.winMain + '|f0|c0|r0|' + 'layer_selector',
			e=True,
			value=self.layer
		)

		self.setResolvedPath()

	def renderLayerRenamed(self):
		"""renderLayerRenamed
		"""
		self.layer = cmds.editRenderLayerGlobals(
			query=True,
			currentRenderLayer=True
		)
		cmds.evalDeferred(
			partial(self.updateRenderLayerMenu),
			lowestPriority=True
		)

	def renderLayerChanged(self):
		"""renderLayerChanged
		"""
		self.layer = cmds.editRenderLayerGlobals(
			query=True,
			currentRenderLayer=True
		)
		cmds.evalDeferred(
			partial(self.updateRenderLayerMenu),
			lowestPriority=True
		)
		return True

	def updateRenderLayerMenu(self):
		"""updateRenderLayerMenu
		"""
		list_items = cmds.optionMenuGrp(
			self.winMain + '|f0|c0|r0|' + 'layer_selector',
			q=True,
			itemListLong=True
		)

		if list_items is not None:
			# clear OptionMenu
			for item in list_items:
				cmds.deleteUI(item)

		renderLayers = getRenderLayersList(False)
		for layer in renderLayers:
			if layer == 'defaultRenderLayer':
				layer = 'masterLayer'
			cmds.menuItem(
				label=layer,
				parent=(
					self.winMain + '|f0|c0|r0|' + 'layer_selector|OptionMenu')
			)

		self.layer = cmds.editRenderLayerGlobals(
			query=True,
			currentRenderLayer=True
		)

		if self.layer == 'defaultRenderLayer':
			self.layer = 'masterLayer'

		cmds.optionMenuGrp(
			self.winMain + '|f0|c0|r0|' + 'layer_selector',
			e=True,
			value=self.layer
		)

		cmds.evalDeferred(
			partial(self.renderLayersSetup, renderLayers),
			lowestPriority=True
		)

	def renderLayersSetup(self, layers):
		"""renderLayersSetup

		:param layers:
		:return:
		"""
		# add script job for renaming render layer
		selector = self.winMain + '|f0|c0|r0|layer_selector'
		firstRun = True
		for layer in layers:
			if layer != 'defaultRenderLayer':
				# cmds.scriptJob(
				#     nodeNameChanged=[layer, partial(self.renderLayerRenamed)],
				#     parent=top,
				#     replacePrevious=firstRun
				# )
				cmds.scriptJob(
					nodeNameChanged=[
						layer,
						'import maya.OpenMaya; '
						'maya.cmds.evalDeferred('
						'    "meArnoldRender.renderLayerRenamed()",'
						'    lowestPriority=True'
						')'
					],
					parent=selector,
					replacePrevious=firstRun
				)
				firstRun = False

	def _setupUI(self):
		"""
		setupUI

		Main window setup

		"""
		self.deleteUI(True)
		self.winMain = \
			cmds.window(
				self.meArnoldRenderMainWnd,
				title='meArnoldRender ver.%s (%s)' % (self.meArnoldRenderVer,
													  self.os),
				menuBar=True,
				retain=False,
				widthHeight=(500, 460)
			)

		self.mainMenu = cmds.menu(label='Commands', tearOff=False)
		cmds.menuItem(label='Render Globals ...', command=maya_render_globals)
		cmds.menuItem(label='Check Texture Paths ...', command=checkTextures)
		cmds.menuItem(label='Generate .ass', command=self.generate_ass)
		cmds.menuItem(label='Submit Job', command=self.submitJob)
		cmds.menuItem(divider=True)
		cmds.menuItem(label='Close', command=self.deleteUI)

		#
		# setup render layers script jobs
		#
		cmds.scriptJob(
			attributeChange=[
				'renderLayerManager.currentRenderLayer',
				'import maya.OpenMaya; '
				'maya.cmds.evalDeferred('
				'    "meArnoldRender.renderLayerSelected()",'
				'    lowestPriority=True'
				')'
			],
			parent=self.winMain
		)
		cmds.scriptJob(
			event=[
				'renderLayerChange',
				'import maya.OpenMaya; '
				'maya.cmds.evalDeferred('
				'    "meArnoldRender.renderLayerChanged()",'
				'    lowestPriority=True'
				')'
			],
			parent=self.winMain
		)

		cw1 = 120
		cw2 = 60
		cw3 = 20

		ar_hi = 8

		form = cmds.formLayout('f0', numberOfDivisions=100)
		proj = cmds.columnLayout(
			'c0',
			columnAttach=('left', 0),
			rowSpacing=2,
			adjustableColumn=True,
			height=50
		)
		cmds.textFieldGrp(
			cw=(1, 70),
			adj=2,
			label='Project Root ',
			text=self.proj_path,
			editable=False
		)  # , bgc=(0,0,0)

		cmds.rowLayout('r0', numberOfColumns=4)
		layer_selector = cmds.optionMenuGrp(
			'layer_selector',
			cw=((1, 70),),
			cal=(1, 'right'),
			label='Render Layer ',
			cc=partial(self.onRenderLayerSelected)
		)
		self.updateRenderLayerMenu()

		cmds.checkBoxGrp(
			'ass_export_all_layers',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Export All Renderable ',
			value1=self.ass_param['ass_export_all_layers'],
			cc=partial(
				setDefaultIntValue,
				self_prefix,
				'ass_export_all_layers',
				self.ass_param
			)
		)

		# --- job_preview

		def jobPreviewChanged(*args):
			setDefaultIntValue(*args)
			cmds.checkBoxGrp('job_preview_shotgun', e=1, enable=args[3])

		cmds.checkBoxGrp(
			'job_preview',
			ann='Automatically create preview',
			ebg=True,
			bgc=[0.2, 0.3, 0.2],
			cw=((1, cw2), (2, cw1 * 2)),
			label='Preview ',
			value1=self.job_param['job_preview'],
			cc=partial(
				jobPreviewChanged,
				self.prefix,
				'job_preview',
				self.job_param
			)
		)

		# -- job_preview_shotgun

		cmds.checkBoxGrp(
			'job_preview_shotgun',
			ann='Automatically send preview to "Shotgun"',
			cw=((1, cw2), (2, cw1 * 2)),
			ebg=True,
			bgc=[0.2, 0.3, 0.2],
			label='Send to Shotgun ',
			value1=self.job_param['job_preview_shotgun'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_preview_shotgun',
				self.job_param
			),
			enable=self.job_param['job_preview']
		)

		cmds.setParent('..')
		cmds.setParent('..')

		#
		# setup tabs
		#
		tab = cmds.tabLayout(
			't0',
			scrollable=True,
			childResizable=True
		)  # tabLayout -scr true -cr true  tabs; //

		#
		# Job tab
		#
		tab_job = cmds.columnLayout(
			'tc0',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.frameLayout(
			'fr1',
			label=' Parameters ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc1',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		job_dispatcher = cmds.optionMenuGrp(
			'job_dispatcher',
			cw=((1, cw1),),
			cal=(1, 'right'),
			label='Job Dispatcher ',
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'job_dispatcher',
				self.job_param
			)
		)

		for name in ('none', 'afanasy'):
			cmds.menuItem(label=name)  # 'backburner',

		cmds.optionMenuGrp(
			job_dispatcher,
			e=True,
			value=self.job_param['job_dispatcher']
		)

		cmds.text(label='')

		cmds.textFieldGrp(
			'job_name',
			cw=(1, cw1),
			adj=2,
			label='Job Name ',
			text=self.job_param['job_name'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'job_name',
				self.job_param
			)
		)

		cmds.textFieldGrp(
			'job_description',
			cw=(1, cw1),
			adj=2,
			label='Description ',
			text=self.job_param['job_description'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'job_description',
				self.job_param
			)
		)

		cmds.checkBoxGrp(
			'job_paused',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Start Paused ',
			value1=self.job_param['job_paused'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_paused',
				self.job_param
			)
		)

		cmds.text(label='')
		cmds.checkBoxGrp(
			'job_animation',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Animation ',
			value1=self.job_param['job_animation'],
			cc=partial(self.enable_range)
		)

		cmds.intFieldGrp(
			'job_range',
			cw=((1, cw1), (2, cw2), (3, cw2), (4, cw2)),
			nf=3,
			label='Start/Stop/By ',
			value1=self.job_param['job_start'],
			value2=self.job_param['job_end'],
			value3=self.job_param['job_step'],
			enable=self.job_param['job_animation'],
			cc=partial(
				setDefaultIntValue3,
				self.prefix,
				('job_start', 'job_end', 'job_step'),
				self.job_param
			)
		)

		cmds.intFieldGrp(
			'job_size',
			cw=((1, cw1), (2, cw2)),
			label='Task Size ',
			ann='Should be smaller then number of frames to render',
			value1=self.job_param['job_size'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_size',
				self.job_param
			)
		)

		cmds.intFieldGrp(
			'job_priority',
			cw=((1, cw1), (2, cw2)),
			label='Priority ',
			value1=self.job_param['job_priority'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_priority',
				self.job_param
			)
		)
		# -- job_overwrite_workspace
		cmds.checkBoxGrp(
			'job_overwrite_workspace',
			cw=((1, cw1), (2, cw1 * 2)),
			ebg=True,
			bgc=[0.2, 0.3, 0.2],
			ann='Default is ON. Uncheck for your own risk.\n You can have errors with file paths',
			label='Overwrite workspace',
			value1=self.job_param['job_overwrite_workspace'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_overwrite_workspace',
				self.job_param
			)
		)

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.frameLayout(
			'fr2',
			label=' File Names Options ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi,
			cll=True,
			cl=True
		)

		cmds.columnLayout(
			'fc2',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.intFieldGrp(
			'job_padding',
			cw=((1, cw1), (2, cw2)),
			label='Frame Padding ',
			value1=self.job_param['job_padding'],
			cc=partial(self.jobFileNameOptionsChanged, 'job_padding')
		)

		job_separator = cmds.optionMenuGrp(
			'job_separator',
			cw=((1, cw1),),
			cal=(1, 'right'),
			label='Separator ',
			cc=partial(self.jobFileNameOptionsChanged, 'job_separator')
		)

		for name in job_separator_list:
			cmds.menuItem(label=name)

		cmds.optionMenuGrp(
			job_separator,
			e=True,
			value=self.job_param['job_separator']
		)

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.frameLayout(
			'fr3',
			label=' Cleanup ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi,
			cll=True,
			cl=True
		)

		cmds.columnLayout(
			'fc3',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.checkBoxGrp(
			'job_cleanup_ass',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' .ass files',
			value1=self.job_param['job_cleanup_ass'],
			enable=True,
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_cleanup_ass',
				self.job_param
			)
		)

		cmds.checkBoxGrp(
			'job_cleanup_maya',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' maya _deferred file',
			value1=self.job_param['job_cleanup_maya'],
			enable=True,
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_cleanup_maya',
				self.job_param
			)
		)

		cmds.text(label='\n   Note: Files will be cleaned after the job delete', al='left')

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.setParent('..')

		#
		# .ass files generation tab
		#
		tab_assparam = cmds.columnLayout(
			'tc1',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.frameLayout(
			'fr3',
			label=' Deferred .ass generation ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi,
			cll=True,
			cl=True
		)

		cmds.columnLayout(
			'fc3',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.checkBoxGrp(
			'ass_deferred',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Use deferred ',
			ann='Generate .ass files in background process',
			value1=self.ass_param['ass_deferred'],
			cc=partial(self.enable_deferred)
		)

		cmds.checkBoxGrp(
			'ass_local_assgen',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=" Only on localhost ",
			ann="Do not use remote hosts",
			value1=self.ass_param['ass_local_assgen'],
			enable=self.ass_param['ass_deferred'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_local_assgen',
				self.ass_param
			)
		)

		cmds.intFieldGrp(
			'ass_def_task_size',
			cw=((1, cw1), (2, cw2)),
			label='Task Size ',
			value1=self.ass_param['ass_def_task_size'],
			enable=self.ass_param['ass_deferred'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_def_task_size',
				self.ass_param
			)
		)

		# self.save_frame_bgc = cmds.frameLayout ( 'fr3', query = True, bgc = True )
		# self.def_frame_bgc = [ 0.75, 0.5, 0 ]
		bg_color = self.save_frame_bgc

		if self.ass_param['ass_deferred']:
			bg_color = self.def_frame_bgc

		cmds.frameLayout(
			self.winMain + '|f0|t0|tc1|fr3',
			edit=True,
			bgc=bg_color
		)  # , enableBackground=False

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.frameLayout(
			'fr1',
			label=' Export Settings ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc1',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.checkBoxGrp(
			'ass_reuse',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Use existing .ass files ',
			ann='Do not generate .ass files if they are exist',
			value1=self.ass_param['ass_reuse'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_reuse',
				self.ass_param
			)
		)

		cmds.text(label='')

		ass_dirname = cmds.textFieldButtonGrp(
			'ass_dirname',
			cw=(1, cw1),
			enable=True,
			adj=2,
			label='Directory Name ',
			buttonLabel='...',
			text=self.ass_param['ass_dirname'],
			cc=partial(self.assDirNameChanged, 'ass_dirname')
		)

		cmds.textFieldButtonGrp(
			ass_dirname,
			edit=True,
			bc=partial(
				browseDirectory,
				self.proj_path,
				ass_dirname
			),
			cc=partial(self.assDirNameChanged, 'ass_dirname')
		)
		"""
		cmds.checkBoxGrp(
			'ass_perframe',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' File Per Frame ',
			value1=self.ass_param['ass_perframe'],
			cc=partial(self.assDirNameChanged, 'ass_perframe')
		)
		"""
		cmds.checkBoxGrp(
			'ass_selection',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Export Only Selected Objects',
			value1=self.ass_param['ass_selection'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_selection',
				self.ass_param
			)
		)

		cmds.checkBoxGrp(
			'ass_binary',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Binary',
			value1=self.ass_param['ass_binary'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_binary',
				self.ass_param
			)
		)

		cmds.checkBoxGrp(
			'ass_compressed',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Compression',
			value1=self.ass_param['ass_compressed'],
			enable=not self.ass_param['ass_deferred'],
			cc=partial(self.assDirNameChanged, 'ass_compressed')
		)

		cmds.checkBoxGrp(
			'ass_expand_procedurals',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Expand Procedurals',
			value1=self.ass_param['ass_expand_procedurals'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_expand_procedurals',
				self.ass_param
			)
		)

		cmds.checkBoxGrp(
			'ass_export_bounds',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Export Bounding Box (.asstoc)',
			value1=self.ass_param['ass_export_bounds'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_export_bounds',
				self.ass_param
			)
		)

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.frameLayout(
			'fr2',
			label=' Resolved Path ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc2',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.textFieldGrp(
			'ass_resolved_path',
			cw=(1, 0),
			adj=2,
			label='',
			text='',
			editable=False
		)

		self.setResolvedPath()

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.setParent('..')

		#
		# Renderer tab
		#
		tab_render = cmds.columnLayout(
			'tc2',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.frameLayout(
			'fr1',
			label=' Arnold options ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc1',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.textFieldGrp(
			'ar_options',
			cw=(1, cw1),
			adj=2,
			label='Additional Options ',
			text=self.ar_param['ar_options'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'ar_options',
				self.ar_param
			)
		)

		cmds.text(label='')

		ar_verbosity = cmds.optionMenuGrp(
			'ar_verbosity',
			cw=((1, cw1),),
			cal=(1, 'right'),
			label='Verbosity ',
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'ar_verbosity',
				self.ar_param
			)
		)

		for name in ar_verbosity_list:
			cmds.menuItem(label=name)

		cmds.optionMenuGrp(
			ar_verbosity,
			e=True,
			value=self.ar_param['ar_verbosity']
		)

		cmds.intFieldGrp(
			'ar_threads',
			cw=((1, cw1), (2, cw2)),
			label='Threads ',
			ann='The number of threads',
			value1=self.ar_param['ar_threads'],
			cc=partial(
				setDefaultIntValue,
				self_prefix,
				'ar_threads',
				self.ar_param
			)
		)

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.frameLayout(
			'fr2',
			label=' Search Paths ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi,
			cll=True,
			cl=True
		)

		cmds.columnLayout(
			'fc2',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.checkBoxGrp(
			'ar_abs_tex_path',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Absolute Texture Paths',
			value1=self.ar_param['ar_abs_tex_path'],
			cc=partial(
				setDefaultIntValue,
				self_prefix,
				'ar_abs_tex_path',
				self.ar_param
			)
		)

		cmds.checkBoxGrp(
			'ar_abs_proc_path',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Absolute Procedural Paths',
			value1=self.ar_param['ar_abs_proc_path'],
			cc=partial(
				setDefaultIntValue,
				self_prefix,
				'ar_abs_proc_path',
				self.ar_param
			)
		)

		cmds.textFieldGrp(
			'ar_plugin_path',
			cw=(1, cw1),
			adj=2,
			label='Plug-ins Path ',
			text=self.ar_param['ar_plugin_path'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'ar_plugin_path',
				self.ar_param
			)
		)

		cmds.textFieldGrp(
			'ar_proc_search_path',
			cw=(1, cw1),
			adj=2,
			label='Procedural Search Path ',
			text=self.ar_param['ar_proc_search_path'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'ar_proc_search_path',
				self.ar_param
			)
		)

		cmds.textFieldGrp(
			'ar_shader_search_path',
			cw=(1, cw1),
			adj=2,
			label='Shaders Search Path ',
			text=self.ar_param['ar_shader_search_path'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'ar_shader_search_path',
				self.ar_param
			)
		)

		cmds.textFieldGrp(
			'ar_tex_search_path',
			cw=(1, cw1),
			adj=2,
			label='Textures Search Path ',
			text=self.ar_param['ar_tex_search_path'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'ar_tex_search_path',
				self.ar_param
			)
		)

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.setParent('..')

		#
		# Afanasy tab
		#
		tab_afanasy = cmds.columnLayout(
			'tc3',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.frameLayout(
			'fr1',
			label=' Parameters ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc1',
			columnAttach=('left', 4),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.intFieldGrp(
			'af_capacity',
			cw=((1, cw1), (2, cw2)),
			label='Task Capacity ',
			value1=self.afanasy_param['af_capacity'],
			cc=partial(
				setDefaultIntValue,
				self_prefix,
				'af_capacity',
				self.afanasy_param
			)
		)

		cmds.intFieldGrp(
			'af_deferred_capacity',
			cw=((1, cw1), (2, cw2)),
			label='Deferred Capacity ',
			value1=self.afanasy_param['af_deferred_capacity'],
			cc=partial(
				setDefaultIntValue,
				self_prefix,
				'af_deferred_capacity',
				self.afanasy_param
			)
		)

		cmds.checkBoxGrp(
			'af_use_var_capacity',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Use Variable Capacity ',
			ann='Block can generate tasks with capacity*coefficient to fit '
				'free render capacity',
			value1=self.afanasy_param['af_use_var_capacity'],
			cc=partial(self.enable_var_capacity)
		)

		cmds.floatFieldGrp(
			'af_var_capacity',
			cw=((1, cw1), (2, cw2), (3, cw2), (4, cw2)),
			nf=2,
			pre=2,
			label='Min/Max coefficient ',
			value1=self.afanasy_param['af_cap_min'],
			value2=self.afanasy_param['af_cap_max'],
			enable=self.afanasy_param['af_use_var_capacity'],
			cc=partial(
				setDefaultFloatValue2,
				self_prefix,
				('af_cap_min', 'af_cap_max'),
				self.afanasy_param
			)
		)

		cmds.intFieldGrp(
			'af_max_running_tasks',
			cw=((1, cw1), (2, cw2)),
			label='Max Running Tasks ',
			value1=self.afanasy_param['af_max_running_tasks'],
			cc=partial(
				setDefaultIntValue,
				self_prefix,
				'af_max_running_tasks',
				self.afanasy_param
			)
		)

		cmds.intFieldGrp(
			'af_max_tasks_per_host',
			cw=((1, cw1), (2, cw2)),
			label='Max Tasks Per Host ',
			value1=self.afanasy_param['af_max_tasks_per_host'],
			cc=partial(
				setDefaultIntValue,
				self_prefix,
				'af_max_tasks_per_host',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_service',
			cw=(1, cw1),
			adj=2,
			label='Service ',
			text=self.afanasy_param['af_service'],
			enable=False,
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'af_service',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_deferred_service',
			cw=(1, cw1),
			adj=2,
			label='Deferred Service ',
			text=self.afanasy_param['af_deferred_service'],
			enable=False,
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'af_deferred_service',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_hostsmask',
			cw=(1, cw1),
			adj=2,
			label='Hosts Mask ',
			ann='Job run only on renders which host name matches this mask\n'
				'e.g.  .* or host.*',
			text=self.afanasy_param['af_hostsmask'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'af_hostsmask',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_hostsexcl',
			cw=(1, cw1),
			adj=2,
			label='Exclude Hosts Mask ',
			ann='Job can not run on renders which host name matches this '
				'mask\n e.g.  host.* or host01|host02',
			text=self.afanasy_param['af_hostsexcl'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'af_hostsexcl',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_depmask',
			cw=(1, cw1),
			adj=2,
			label='Depend Mask ',
			ann='Job will wait other user jobs which name matches this mask',
			text=self.afanasy_param['af_depmask'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'af_depmask',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_depglbl',
			cw=(1, cw1),
			adj=2,
			label='Global Depend Mask ',
			ann='Job will wait other jobs from any user which name matches '
				'this mask',
			text=self.afanasy_param['af_depglbl'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'af_depglbl',
				self.afanasy_param
			)
		)

		"""
		cmds.textFieldGrp(
			'af_os',
			cw=(1, cw1),
			adj=2,
			label='Needed OS ',
			ann='windows linux mac',
			text=self.afanasy_param['af_os'],
			cc=partial(
				setDefaultStrValue,
				self_prefix,
				'af_os',
				self.afanasy_param
			)
		)
		"""
		cmds.checkBoxGrp(
			'af_os',
			cw=((1, cw1), (2, cw1 / 2), (3, cw1 / 2), (4, cw1 / 2)),
			numberOfCheckBoxes=3,
			label='Needed OS  ',
			label1='windows',
			value1=(self.afanasy_param['af_os'].find('windows') != -1),
			label2='linux',
			value2=(self.afanasy_param['af_os'].find('linux') != -1),
			label3='mac',
			value3=(self.afanasy_param['af_os'].find('mac') != -1),
			cc=partial(self.set_needed_os)
		)

		cmds.setParent('..')
		cmds.setParent('..')
		cmds.setParent('..')

		cmds.tabLayout(
			tab,
			edit=True,
			tabLabel=(
				(tab_job, "Job"),
				(tab_assparam, ".ass"),
				(tab_render, "Renderer"),
				(tab_afanasy, "Afanasy")
			)
		)

		cmds.setParent(form)

		btn_sbm = cmds.button(
			label='Submit',
			command=self.submitJob,
			ann='Generate .ass files and submit to dispatcher'
		)

		btn_gen = cmds.button(
			label='Generate .ass',
			command=self.generate_ass,
			ann='Force .ass files generation'
		)
		btn_cls = cmds.button(label='Close', command=self.deleteUI)

		cmds.formLayout(
			form,
			edit=True,
			attachForm=(
				(proj, 'top', 0),
				(proj, 'left', 0),
				(proj, 'right', 0),
				(tab, 'left', 0),
				(tab, 'right', 0),
				(btn_cls, 'bottom', 0),
				(btn_gen, 'bottom', 0),
				(btn_sbm, 'bottom', 0),
				(btn_sbm, 'left', 0),
				(btn_cls, 'right', 0)
			),
			attachControl=(
				(tab, 'top', 0, proj),
				(tab, 'bottom', 0, btn_sbm),
				(btn_gen, 'left', 0, btn_sbm),
				(btn_gen, 'right', 0, btn_cls)
			),
			attachPosition=(
				(btn_sbm, 'right', 0, 33),
				(btn_gen, 'right', 0, 66),
				(btn_cls, 'left', 0, 66)
			)
		)

		if not self.NoGUI:
			cmds.showWindow(self.winMain)

		return form

	def deleteUI ( self, param=None) :
		"""deleteUI
		:param param: dummy parameter
		"""
		winMain = self.meArnoldRenderMainWnd
		if cmds.window(winMain, exists=True):
			cmds.deleteUI(winMain, window=True)

		if cmds.windowPref(winMain, exists=True):
			cmds.windowPref(winMain, remove=True)

print('meArnoldRender sourced ...')
