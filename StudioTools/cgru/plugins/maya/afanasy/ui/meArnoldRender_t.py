# -*- coding: utf-8 -*-
"""
	meArnoldRender

	Usage:
	You can add this code as shelf button :

from afanasy import meArnoldRender
reload( meArnoldRender ) # keep this just for debug purposes
meArnoldRender = meArnoldRender.meArnoldRender()

	Important!!!
	Do not use another object name.
	Only meArnoldRender.renderLayerSelected() will work with script job.

	Add %CGRU_LOCATION%\plugins\maya to %PYTHONPATH%

"""

import sys
import os
import string
import shutil
import maya.OpenMaya as OpenMaya
import maya.cmds as cmds
import maya.mel as mel
from functools import partial

print('check: ', os.getenv('PROJECT_PATH'))
TOOLS_PATH = os.getenv('TOOLS_PATH')
sys.path.append(TOOLS_PATH)

from helper.EnvManagerV2 import *
from maya_ui_proc import *
from afanasyRenderJob import *

import previewCmd as prvc
from ui import *
print(EnvVar('ARNOLD_PLUGIN_PATH').appendPath(["${FDBIN}/plugin"]))

self_prefix = 'meArnoldRender_'
self_prefix = 'meArnoldRender_'
meArnoldRenderVer = '0.3.4'
meArnoldRenderMainWnd = self_prefix + 'MainWnd'

ar_verbosity_list = [
	'none',
	'fatal',
	'error',
	'warning',
	'info',
	'progress',
	'debug',
	'details'
]


class meArnoldRender(object):
	"""
	meArnoldRender

	"""
	def __init__(self, NoGUI=False):
			"""
			In case if NoGUI = True, it's possible to use external "wrapper"
			e.g. for Shotgun to call "generate_ass" or "submitJob" previously
			assign global variables  
			"""
			self.prefix = 'meArnoldRender_'
			self.meArnoldRenderVer = '0.3.4'
			self.meArnoldRenderMainWnd = self.prefix + 'MainWnd'
			self.winMain = ''
			self.NoGUI = NoGUI
			self.os = sys.platform
			if self.os.startswith('linux'):
				self.os = 'linux'
				self.ext = ''
			elif self.os == 'darwin':
				self.os = 'mac'
			elif self.os == 'win32':
				self.os = 'win'
				self.ext = '.exe'

			print('sys.platform = %s self.os = %s' % (sys.platform, self.os))
			print('render starting ...')
			curr_render = cmds.getAttr('defaultRenderGlobals.currentRenderer')
			if curr_render != 'arnold':
				cmds.warning('Arnold is not current renderer!')
				return

			self.rootDir = cmds.workspace(q=True, rootDirectory=True)
			ass_dirname = cmds.workspace(fileRuleEntry='ASS')
			filename = getMayaSceneName()
			self.layer = cmds.editRenderLayerGlobals(
				query=True,
				currentRenderLayer=True
			)

			self.job = None
			self.job_param = {}
			self.ass_param = {}
			self.ar_param = {}
			self.img_param = {}
			self.afanasy_param = {}
			self.bkburn_param = {}

			print('ad',ass_dirname)
			print(filename)
			print(self.rootDir)

			pipline = PiplineTools(opts='maya')
			self.soft_tools_path = pipline.soft_tools_path
			self.app = pipline.app

			# self.assgenCommand = 'arnoldExportAss'
			# self.def_assgenCommand = 'python ' + os.getenv('TOOLS_PATH') +'/maya/ass_deferred.py -r %s' % curr_render
			# 'python ' + os.getenv('TOOLS_PATH') +'/maya/ass_deferred.py -r %s' % curr_render

			# maya scene name used for deferred .ass generation
			self.def_scene_name = ''

			# 'deletefiles' script is located in $CGRU_LOCATION\bin
			# self.cleanup_cmd = 'deletefiles'

			self.save_frame_bgc = [0, 0, 0]
			self.def_frame_bgc = [0.75, 0.5, 0]
			self.initParameters()
			if not self.NoGUI:
				self.ui = arnoldUI(self)

	def initParameters(self):
			"""
			Init all parameters
			"""
			#
			# Job parameters
			#
			self.job_param['job_dispatcher'] = \
				getDefaultStrValue(self.prefix, 'job_dispatcher', 'afanasy')

			self.job_param['job_name'] = getMayaSceneName()
			self.job_param['job_description'] = \
				getDefaultStrValue(self.prefix, 'job_description', '')

			self.job_param['job_animation'] = \
				getDefaultIntValue(self.prefix, 'job_animation', 1) is 1

			self.job_param['job_start'] = \
				getDefaultIntValue(self.prefix, 'job_start', 1)

			self.job_param['job_end'] = \
				getDefaultIntValue(self.prefix, 'job_end', 100)

			self.job_param['job_step'] = \
				getDefaultIntValue(self.prefix, 'job_step', 1)

			self.job_param['job_size'] = \
				getDefaultIntValue(self.prefix, 'job_size', 1)

			self.job_param['job_paused'] = \
				getDefaultIntValue(self.prefix, 'job_paused', 1) is 1

			self.job_param['job_priority'] = \
				getDefaultIntValue(self.prefix, 'job_priority', -1)

			self.job_param['job_cleanup_ass'] = \
				getDefaultIntValue(self.prefix, 'job_cleanup_ass', 0) is 1

			self.job_param['job_cleanup_maya'] = \
				getDefaultIntValue(self.prefix, 'job_cleanup_maya', 0) is 1

			self.job_param['job_cleanup_yeti'] = \
				getDefaultIntValue(self.prefix, 'job_cleanup_yeti', 0) is 1

			self.job_param['job_padding'] = \
				getDefaultIntValue(self.prefix, 'job_padding', 4)

			# job_separator = { 'none', '.', '_' }
			self.job_param['job_separator'] = \
				getDefaultStrValue(self.prefix, 'job_separator', '.')

			# --- evm extra parameters:
			self.job_param['job_preview'] = \
				getDefaultIntValue(self.prefix, 'job_preview', 1) is 1

			self.job_param['job_preview_shotgun'] = \
				getDefaultIntValue(self.prefix, 'job_preview_shotgun', 1) is 1

			self.job_param['job_overwrite_workspace'] = \
				getDefaultIntValue(self.prefix, 'job_overwrite_workspace', 0) is 1

			self.job_param['job_new_user'] = ''  # for overwrite user if needed

			#
			# .ass generation parameters
			#
			self.ass_param['ass_reuse'] = \
				getDefaultIntValue(self.prefix, 'ass_reuse', 0) is 1

			ass_dirname = cmds.workspace(fileRuleEntry='ASS')
			if ass_dirname == '':
				ass_dirname = 'ass'
				# ass_dirname = '//omega/moriki-doriki/temp/data/maya'
				cmds.workspace(fileRule=('ASS', ass_dirname))
			# cmds.workspace( saveWorkspace=True )

			self.ass_param['ass_dirname'] = \
				getDefaultStrValue(self.prefix, 'ass_dirname', ass_dirname)

			# self.ass_param['ass_perframe'] = \
			#	getDefaultIntValue(self_prefix, 'ass_perframe', 1) is 1

			self.ass_param['ass_selection'] = \
				getDefaultIntValue(self.prefix, 'ass_selection', 0) is 1

			self.ass_param['ass_binary'] = \
				getDefaultIntValue(self.prefix, 'ass_binary',
								   cmds.getAttr('defaultArnoldRenderOptions.binaryAss')) is 1

			self.ass_param['ass_verbosity'] = \
				getDefaultStrValue(self.prefix, 'ass_verbosity', 'none')

			self.ass_param['ass_deferred'] = \
				getDefaultIntValue(self.prefix, 'ass_deferred', 0) is 1

			self.ass_param['ass_local_assgen'] = \
				getDefaultIntValue(self.prefix, 'ass_local_assgen', 1) is 1

			self.ass_param['ass_def_task_size'] = \
				getDefaultIntValue(self.prefix, 'ass_def_task_size', 4)

			self.ass_param['ass_export_all_layers'] = \
				getDefaultIntValue(self.prefix, 'ass_export_all_layers', 0) is 1

			self.ass_param['ass_compressed'] = \
				getDefaultStrValue(self.prefix, 'ass_compressed',
								   cmds.getAttr('defaultArnoldRenderOptions.output_ass_compressed')) is 1

			# defaultArnoldRenderOptions.expandProcedurals
			self.ass_param['ass_expand_procedurals'] = \
				getDefaultIntValue(self.prefix, 'ass_expand_procedurals',
								   cmds.getAttr('defaultArnoldRenderOptions.expandProcedurals')) is 1

			# defaultArnoldRenderOptions.outputAssBoundingBox
			self.ass_param['ass_export_bounds'] = \
				getDefaultIntValue(self.prefix, 'ass_export_bounds',
								   cmds.getAttr('defaultArnoldRenderOptions.outputAssBoundingBox')) is 1

			#
			# Arnold parameters
			#
			self.ar_param['ar_options'] = \
				getDefaultStrValue(self.prefix, 'ar_options', '')

			self.ar_param['ar_verbosity'] = \
				getDefaultStrValue(self_prefix, 'ar_verbosity', 'none')

			self.ar_param['ar_threads'] = \
				getDefaultIntValue(self_prefix, 'ar_threads', 0)

			# defaultArnoldRenderOptions.absoluteTexturePaths
			self.ar_param['ar_abs_tex_path'] = \
				getDefaultIntValue(self_prefix, 'ar_abs_tex_path',
								   cmds.getAttr('defaultArnoldRenderOptions.absoluteTexturePaths')) is 1

			# defaultArnoldRenderOptions.absoluteProceduralPaths
			self.ar_param['ar_abs_proc_path'] = \
				getDefaultIntValue(self_prefix, 'ar_abs_proc_path',
								   cmds.getAttr('defaultArnoldRenderOptions.absoluteProceduralPaths')) is 1

			# defaultArnoldRenderOptions.plugins_path
			self.ar_param['ar_plugin_path'] = \
				getDefaultStrValue(self_prefix, 'ar_plugin_path',
								   cmds.getAttr('defaultArnoldRenderOptions.plugins_path'))

			# defaultArnoldRenderOptions.procedural_searchpath
			self.ar_param['ar_proc_search_path'] = \
				getDefaultStrValue(self_prefix, 'ar_proc_search_path',
								   cmds.getAttr('defaultArnoldRenderOptions.procedural_searchpath'))

			# defaultArnoldRenderOptions.shader_searchpath
			self.ar_param['ar_shader_search_path'] = r'%s\arnold\mtoadeploy\2018\windows\shaders' % self.soft_tools_path
			#	getDefaultStrValue(self_prefix, 'ar_shader_search_path',
			#					   				os.getenv('TOOLS_PATH').replace('Y:', ))
			#			cmds.getAttr('defaultArnoldRenderOptions.shader_searchpath'))

			# defaultArnoldRenderOptions.texture_searchpath
			self.ar_param['ar_tex_search_path'] = \
				getDefaultStrValue(self_prefix, 'ar_tex_search_path', '')
			#			cmds.getAttr('defaultArnoldRenderOptions.texture_searchpath'))
			#
			# Image parameters
			#
			self.img_param['img_filename'] = self.getImageFileNamePrefix()
			self.img_param['img_format'] = self.getImageFormat()
			#
			# Afanasy parameters
			#
			self.afanasy_param['af_capacity'] = \
				getDefaultIntValue(self_prefix, 'af_capacity', 1000)

			self.afanasy_param['af_deferred_capacity'] = \
				getDefaultIntValue(self_prefix, 'af_deferred_capacity', 1000)

			self.afanasy_param['af_use_var_capacity'] = \
				getDefaultIntValue(self_prefix, 'af_use_var_capacity', 0) is 1

			self.afanasy_param['af_cap_min'] = \
				getDefaultFloatValue(self_prefix, 'af_cap_min', 1.0)

			self.afanasy_param['af_cap_max'] = \
				getDefaultFloatValue(self_prefix, 'af_cap_max', 1.0)

			self.afanasy_param['af_max_running_tasks'] = \
				getDefaultIntValue(self_prefix, 'af_max_running_tasks', -1)

			self.afanasy_param['af_max_tasks_per_host'] = \
				getDefaultIntValue(self_prefix, 'af_max_tasks_per_host', -1)

			self.afanasy_param['af_service'] = \
				getDefaultStrValue(self_prefix, 'af_service', 'arnold')

			self.afanasy_param['af_deferred_service'] = \
				getDefaultStrValue(
					self_prefix,
					'af_deferred_service',
					'mayatoarnold'
				)

			self.afanasy_param['af_os'] = \
				getDefaultStrValue(self_prefix, 'af_os', '')  # linux mac windows

			self.afanasy_param['af_hostsmask'] = \
				getDefaultStrValue(self_prefix, 'af_hostsmask', '')

			self.afanasy_param['af_hostsexcl'] = \
				getDefaultStrValue(self_prefix, 'af_hostsexcl', '')

			self.afanasy_param['af_depmask'] = \
				getDefaultStrValue(self_prefix, 'af_depmask', '')

			self.afanasy_param['af_depglbl'] = \
				getDefaultStrValue(self_prefix, 'af_depglbl', '')

	def getImageFileNamePrefix(self):
		""" Get image file prefix name from RenderGlobals or use just maya scene name
				if name is empty string 
		"""
		fileNamePrefix = cmds.getAttr('defaultRenderGlobals.imageFilePrefix')
		if fileNamePrefix in [None, '']:
			fileNamePrefix = getMayaSceneName()
		return fileNamePrefix

	def getImageFormat(self):
		"""Get image format extensions

		return: string of image format extension
		"""
		imageFormatStr = mel.eval('getImfImageType')
		if imageFormatStr in ['deepexr', 'maya']:
			imageFormatStr = 'exr'
		return imageFormatStr

	def generate_ass(self, isSubmitingJob=False):
		"""generate_ass

		:param isSubmitingJob: if job assumed to be submited after .ass generation
		"""
		print('Ass generation init ...')
		ass_reuse = self.ass_param['ass_reuse']

		# TODO!!! check if files are exist and have to be overwritten
		if isSubmitingJob and ass_reuse:
			print('Skipping .ass files generation ...')

		exportAllRenderLayers = self.ass_param['ass_export_all_layers']
		animation = self.job_param['job_animation']
		start = self.job_param['job_start']
		stop = self.job_param['job_end']
		step = self.job_param['job_step']
		separator = self.job_param['job_separator']
		ass_selection = self.ass_param['ass_selection']
		ass_dirname = self.ass_param['ass_dirname']
		ass_padding = self.job_param['job_padding']
		# ass_perframe = self.ass_param['ass_perframe']
		ass_deferred = self.ass_param['ass_deferred']

		ass_binary = self.ass_param['ass_binary']
		ass_expand_procedurals = self.ass_param['ass_expand_procedurals']
		ass_export_bounds = self.ass_param['ass_export_bounds']

		ar_abs_tex_path = self.ar_param['ar_abs_tex_path']
		ar_abs_proc_path = self.ar_param['ar_abs_proc_path']

		# print(self.ar_param['ar_plugin_path'])
		ar_plugin_path = self.ar_param['ar_plugin_path'].replace('toolsMulty\\', 'toolsMultyV0.2\\')
		ar_proc_search_path = self.ar_param['ar_proc_search_path'].replace('toolsMulty\\', 'toolsMultyV0.2\\')
		ar_shader_search_path = self.ar_param['ar_shader_search_path']
		ar_tex_search_path = self.ar_param['ar_tex_search_path']

		print('400 : ', self.filename)
		print('401 : ', self.ass_dirname)
		filename = cmds.workspace(expandName=ass_dirname)
		print('403 : ', filename)
		filename, ext = os.path.splitext(filename)
		print('405 ass_gen filename: ', filename)

		if ext == '' or ext == '.':
			ext = '.ass'
		filename += ext
		dirname = os.path.dirname(filename)
		if not os.path.exists(dirname):
			print('path %s not exists... create it!' % dirname)
			os.mkdir(dirname)

		if not animation:
			start = stop = int(cmds.currentTime(q=True))
			step = 1

		if True:
			#
			# save RenderGlobals
			#
			defGlobals = 'defaultRenderGlobals'
			aiGlobals = 'defaultArnoldRenderOptions'
			saveGlobals = {}
			#
			# override RenderGlobals
			#
			cmds.setAttr(defGlobals + '.extensionPadding', ass_padding)
			cmds.setAttr(defGlobals + '.animation', 1)  # always use 'name.#.ext' format
			cmds.setAttr(defGlobals + '.outFormatControl', 0)
			cmds.setAttr(defGlobals + '.putFrameBeforeExt', 1)
			# if separator == 'none':
			#	cmds.setAttr(defGlobals + '.periodInExt', 0)
			# elif separator == '.':
			#	cmds.setAttr(defGlobals + '.periodInExt', 1)
			# else:
			sep_nom = job_separator_list.index(separator)
			cmds.setAttr(defGlobals + '.periodInExt', sep_nom)
			cmds.setAttr(aiGlobals + '.binaryAss', ass_binary)
			cmds.setAttr(aiGlobals + '.expandProcedurals', ass_expand_procedurals)
			cmds.setAttr(aiGlobals + '.outputAssBoundingBox', ass_export_bounds)
			cmds.setAttr(aiGlobals + '.absoluteTexturePaths', ar_abs_tex_path)
			cmds.setAttr(aiGlobals + '.absoluteProceduralPaths', ar_abs_proc_path)
			cmds.setAttr(aiGlobals + '.plugins_path', ar_plugin_path, type='string')
			cmds.setAttr(aiGlobals + '.procedural_searchpath', ar_proc_search_path, type='string')
			# cmds.setAttr(aiGlobals + '.shader_searchpath', ar_shader_search_path, type='string' )
			# cmds.setAttr(aiGlobals + '.texture_searchpath', ar_tex_search_path, type='string' )
			#
			# Clear .output_ass_filename to force using default filename from RenderGlobals
			#
			cmds.setAttr(aiGlobals + '.output_ass_filename', '', type='string')
			cmds.workspace(fileRule=('ASS', ass_dirname))
			# cmds.workspace( saveWorkspace=True )

			# save current layer
			current_layer = cmds.editRenderLayerGlobals(q=True, currentRenderLayer=True)
			# image_name = self.getImageFileNamePrefix()
			renderLayers = [current_layer]
			if exportAllRenderLayers:
				renderLayers = getRenderLayersList(True)  # renderable only

			if ass_deferred:
				# generate unique maya scene name and save it
				# with current render and .ass generation settings
				print('Use deferred .ass generation')
				# print(image_name)
				saveGlobals['imageFilePrefix'] = str(cmds.getAttr(defGlobals + '.imageFilePrefix'))
				cmds.setAttr(defGlobals + '.imageFilePrefix', self.filename, type='string')

				# get scene name without extension
				# scene_name = getMayaSceneName(withoutSubdir=False)
				def_scene_name = self.filename + '_deferred'
				cmds.file(rename=def_scene_name)
				print(def_scene_name)
				# save it with default extension
				self.def_scene_name = cmds.file(save=True, de=True)
				print(self.def_scene_name)
				# rename scene back
				cmds.file(rename=self.filename)
				cmds.setAttr(defGlobals + '.imageFilePrefix', saveGlobals['imageFilePrefix'], type='string')

			for layer in renderLayers:

				if layer == 'masterLayer':
					layer = 'defaultRenderLayer'

				saveGlobals['renderableLayer'] = cmds.getAttr(layer + '.renderable')
				cmds.setAttr(layer + '.renderable', True)
				# print 'set current layer renderable (%s)' % layer
				cmds.editRenderLayerGlobals(currentRenderLayer=layer)
				# print(self.get_assgen_options(layer), '---]')
				# assgen_cmd = self.app.deferredCmdLine % (
				#			 TOOLS_PATH, self.rootDir, 1, current_layer, self.get_image_name())#(self.get_assgen_options(layer), start, stop, step)
				assgen_cmd = self.app.assgen_сmd % (self.get_assgen_options(layer), start, stop, step)
				# assgen_cmd += ' -startFrame %d' % start
				# assgen_cmd += ' -endFrame %d' % stop
				# assgen_cmd += ' -frameStep %d' % step
				# cmds.setAttr( defGlobals + '.imageFilePrefix', image_name, type='string' )
				# will use MayaSceneName if empty
				print(assgen_cmd)
				mel.eval(assgen_cmd)

				cmds.setAttr(
					layer + '.renderable',
					saveGlobals['renderableLayer']
				)

			if exportAllRenderLayers:
				# restore current layer
				cmds.editRenderLayerGlobals(
					currentRenderLayer=current_layer
				)

	def submitJob(self, param=None):
		"""
		submitJob

		:param param: dummy parameter
		"""
		print('Job submited')

		"""

		job_dispatcher = self.job_param['job_dispatcher']
		job_description = self.job_param['job_description']
		job_name = str(self.job_param['job_name']).strip()
		if job_name == '':
			job_name = getMayaSceneName()
		job_cleanup_ass = self.job_param['job_cleanup_ass']
		job_cleanup_maya = self.job_param['job_cleanup_maya']

		ass_deferred = self.ass_param['ass_deferred']
		ass_local_assgen = self.ass_param['ass_local_assgen']
		ass_def_task_size = self.ass_param['ass_def_task_size']
		ass_reuse = self.ass_param['ass_reuse']
		exportAllRenderLayers = self.ass_param['ass_export_all_layers']

		if job_dispatcher == 'afanasy':
			self.job = AfanasyRenderJob(job_name, job_description)
			self.job.use_var_capacity = self.afanasy_param[
				'af_use_var_capacity']
			self.job.capacity_coeff_min = self.afanasy_param['af_cap_min']
			self.job.capacity_coeff_max = self.afanasy_param['af_cap_max']
			self.job.max_running_tasks = self.afanasy_param[
				'af_max_running_tasks']
			self.job.max_tasks_per_host = self.afanasy_param[
				'af_max_tasks_per_host']
			self.job.hostsmask = str(
				self.afanasy_param['af_hostsmask']).strip()
			self.job.hostsexcl = str(
				self.afanasy_param['af_hostsexcl']).strip()
			self.job.depmask = str(self.afanasy_param['af_depmask']).strip()
			self.job.depglbl = str(self.afanasy_param['af_depglbl']).strip()
			self.job.need_os = str(self.afanasy_param['af_os']).strip()

			service = str(self.afanasy_param['af_service']).strip()
			deferred_service = \
				str(self.afanasy_param['af_deferred_service']).strip()

			capacity = self.afanasy_param['af_capacity']
			deferred_capacity = self.afanasy_param['af_deferred_capacity']

		elif job_dispatcher == 'backburner':
			print('backburner not supported in this version')
			# self.job = ArnoldBackburnerJob ( job_name, job_description )
			return
		else:
			ass_deferred = False
			self.job = RenderJob(job_name, job_description)

		self.job.work_dir = self.rootDir
		self.rootDir = self.def_scene_name.split('/render')[0]
		self.job.padding = self.job_param['job_padding']
		self.job.priority = self.job_param['job_priority']
		self.job.paused = self.job_param['job_paused']
		self.job.task_size = self.job_param['job_size']
		self.job.animation = self.job_param['job_animation']
		self.job.start = self.job_param['job_start']
		self.job.stop = self.job_param['job_end']
		self.job.step = self.job_param['job_step']

		# print('wd = %s' % self.job.work_dir)
		self.generate_ass(True)  # isSubmitingJob=True
		#self.rootDir = self.def_scene_name.split('/render')[0]
		self.job.work_dir = self.rootDir
		# cmds.setWorkingDirectory(self.rootDir)
		print('591 wrr_d2 = %s' % self.job.work_dir)

		self.job.setup_range(int(cmds.currentTime(q=True)))
		self.job.setup()

		# save current layer
		current_layer = \
			cmds.editRenderLayerGlobals(
				q=True,
				currentRenderLayer=True
			)

		renderLayers = [current_layer]
		if exportAllRenderLayers:
			renderLayers = getRenderLayersList(True)  # renderable only

		for layer in renderLayers:
		  cmds.editRenderLayerGlobals(currentRenderLayer=layer)
		  layer_name = layer
		  if layer == 'defaultRenderLayer':
				layer_name = 'masterLayer'

		  if job_dispatcher == 'afanasy':
			if ass_deferred and not ass_reuse:
				# if exportAllRenderLayers:
				#	gen_cmd = self.getDeferredCmd(None)
				# else:
				#	gen_cmd = self.getDeferredCmd(current_layer)

				if current_layer is None:
					current_layer = 1
				#gen_cmd = self.app.deferredCmdLine() % (
				#	TOOLS_PATH, self.rootDir, 1, current_layer, self.get_image_name())
				gen_cmd = self.processCmd(1, current_layer)
				gen_cmd += self.get_assgen_options(current_layer)
				# gen_cmd += ' -s @#@'
				# gen_cmd += ' -e @#@'
				# gen_cmd += ' -b %s' % self.job.step
				# print('867 ', self.def_scene_name)
				# print('887 gen_cmd = %s >..< %s' % (gen_cmd, self.def_scene_name))

				self.job.gen_block = \
					AfanasyRenderBlock(
						'generate_ass',
						deferred_service,
						self.job,
						ass_local_assgen
					)
				self.job.gen_block.capacity = deferred_capacity
				self.job.gen_block.cmd = gen_cmd
				self.job.gen_block.input_files = '"%s"' % self.get_ass_name(True, current_layer)
				self.job.gen_block.task_size = min(ass_def_task_size, self.job.num_tasks)
				# Eself.job.gen_block.out_files = self.get_image_name() #.replace('maya', 'exr') #self.outputPathForKick(self.get_image_name())
				#print('848 gb_inpf:', self.job.gen_block.input_files)

				# Set block Post command for cleanup
				if job_cleanup_maya:
					self.job.gen_block.af_block.setCmdPost(self.app.cleanupCmd + self.job.gen_block.input_files)
				self.job.gen_block.setup()
			else:
				frame_block = \
					AfanasyRenderBlock(
						'render_%s' % layer_name,
						service,
						self.job
					)

				frame_block.capacity = capacity
				frame_block.input_files = '"%s"' % self.filename   #get_ass_name(True, layer_name)
				frame_block.out_files = self.get_image_name()  # .replace('maya', 'exr') #self.outputPathForKick(self.get_image_name())
				print('fls', frame_block.out_files)
				print('fls_inp', frame_block.input_files)
				frame_block.cmd = self.getRenderCmd(layer)  # .replace('rs_layer_', 'layer_')

				# Set block Post command for cleanup
				if job_cleanup_ass:
					import re
					# replace frame number '@####@' with '*' wildcard
					input_files = re.sub('@[#]+@', '*', frame_block.input_files)
					frame_block.af_block.setCmdPost(self.app.cleanupCmd + input_files)

				frame_block.setup()
				self.job.frames_blocks.append(frame_block)

				# -----------------generate preview block --------------
				# self.job_param['job_preview'] = \
				# getDefaultIntValue(self_prefix, 'job_preview', 1) is 1

				# self.job_param['job_preview_shotgun'] = \
				# getDefaultIntValue(self_prefix, 'job_preview_shotgun', 1) is 1
				if (self.job_param['job_preview']):
					# if (cmds.checkBoxGrp('make_preview',q = True, v1 = True)):
					prv_block = af.Block('preview_%s' % layer_name, 'movgen')
					prv_block.setWorkingDirectory(self.job.work_dir)
					print('s.w-d:', self.job.work_dir)
					prv_block.setFiles([self.get_image_name()])

					# Set depend mask of render task:
					prv_block.setDependMask(frame_block.af_block.data["name"])

					# Generate preview task
					reload(prvc)
					prv_task = af.Task(getMayaSceneName())

					# Set block tasks command and append to preview block
					currentScenePath = cmds.file(query=True, sceneName=True)
					print('scene_path:', currentScenePath)
					prv_commands = prvc.getCommand(currentScenePath, layer * exportAllRenderLayers,
												   self.job.work_dir,
												   self.get_image_name(),
												   self.job.af_job.data["user_name"])

					if not len(prv_commands):
						# check ugly guard
						cmds.error('Error: Incorrect scene name or scene path')
						return
					prv_task.setCommand(prv_commands[0])
					prv_block.tasks.append(prv_task)
					print(prv_commands)

					# Append preview to job:
					self.job.af_job.blocks.append(prv_block)

					if (self.job_param['job_preview_shotgun']):
						# ----------------- Generate shtgun block:
						# shtgn_block = af.Block('shotgun_preview_%s' % layer_name, 'generic')
						shtgn_block = af.Block('shotgun_preview_%s' % layer_name, 'shotgun')
						# shtgn_block.setParser('generic')
						shtgn_block.setWorkingDirectory(self.job.work_dir)
						shtgn_block.setDependMask(prv_block.data["name"])
						shtgn_task = af.Task(getMayaSceneName())
						shtgn_task.setCommand(prv_commands[1])
						# shtgn_task.service('shotgun')

						shtgn_block.setTasksMaxRunTime(30)  # secs for task to error
						shtgn_block.setErrorsRetries(10)  # times to try
						shtgn_block.setErrorsAvoidHost(4)  # max times to try per host

						# Append shtgn task to  block:
						shtgn_block.tasks.append(shtgn_task)

						# Append send preview to Shotgun to job:
						self.job.af_job.blocks.append(shtgn_block)

					# -------------------------------------------------------

		    if self.job_param['job_new_user']:
			  self.job.af_job.data["user_name"] = self.job_param['job_new_user']
				print('New user was set: %s' % self.job.af_job.data["user_name"])
			self.job.process()

		if exportAllRenderLayers:
			# restore current layer
			cmds.editRenderLayerGlobals(currentRenderLayer=current_layer)
		"""

	def get_ass_name(self, suffix=True, layer=None, decorator='@'):
		"""get_ass_name

		:param suffix: if it is True -- return pattern string name name.@###@.ass
									 otherwise -- return just .ass filename
		:param layer: current render layer
		:param decorator: symbol for padding string decoration
		:return: .ass filename 
		"""
		ass_deferred = self.ass_param['ass_deferred']
		separator = self.job_param['job_separator']
		if separator == 'none':
			separator = ''

		filename = self.ass_param['ass_dirname']
		if layer is not None:
			if layer == 'defaultRenderLayer':
				layer = 'masterLayer'

			print('curr_lr: %s' % layer)
			if not (len(getRenderLayersList(False)) == 1):
				filename += '/' + layer

		scenename = getMayaSceneName()
		print('msn: %s' % scenename)

		if ass_deferred:
			scenename += '_deferred'
		# scenename, ext = os.path.splitext(
		#	os.path.basename(self.def_scene_name)
		# )

		filename += '/%s' % scenename
		filename = cmds.workspace(expandName=filename)
		print('wrksp_file: %s' % filename)

		if suffix:
			pad_str = getPadStr(self.job_param['job_padding'], True)

			# if self.ass_param['ass_perframe'] :
			filename += decorator.join([separator, pad_str, '.ass'])
			# else :
			#	filename += '.ass'
			if self.ass_param['ass_compressed']:
				# !!! There is error in maya translator
				# for "defaultArnoldRenderOptions.output_ass_compressed" flag
				if not ass_deferred:
					filename += '.gz'

		return filename

	def get_image_name(self):
		"""get_image_name
		"""
		import re
		pad_str = getPadStr(self.job_param['job_padding'], True)

		images = cmds.renderSettings(
			fullPath=True,
			genericFrameImageName=('@%s@' % pad_str)
		)

		# imageFileName = ';'.join (images)
		imageFileName = str(images[0])
		(fileName, ext) = os.path.splitext(imageFileName)

		# print(os.path.join(*(ifn_parts[:-4] + ifn_parts[-1].split('_')[:2])))
		vlu = re.findall('(\w+[0-9]+_sh[0-9]+)', fileName)[0]
		vlu = vlu.replace('_', '/')
		ver = re.findall('_(v[0-9]+)\.', fileName)[0]
		to_repl = re.findall('(\w+[0-9]+/sh[0-9]+)', fileName)[0]
		ifn = fileName.replace(to_repl, vlu)
		if not 'in/v' in ifn:
			ifn = ifn.replace('in', 'in/%s' % ver)

		imageFileName = ifn + '.' + self.getImageFormat()
		imgDir = os.path.dirname(imageFileName)
		if not os.path.exists(imgDir):
			os.makedirs(imgDir)

		return fromNativePath(imageFileName)
