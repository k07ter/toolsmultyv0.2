#coding: utf-8
import maya.OpenMaya as OpenMaya
import maya.cmds as cmds
import maya.mel as mel
from functools import partial
from maya_ui_proc import *

job_separator_list = ['none', '.', '_']
ar_verbosity_list = [
	'none',
	'fatal',
	'error',
	'warning',
	'info',
	'progress',
	'debug',
	'details'
]

def arnoldUI(self):
	
		"""
		setupUI
		
		Main window setup
		
		"""
		self.deleteUI(True)
		self.winMain = \
			cmds.window(
				self.meArnoldRenderMainWnd,
				title='meArnoldRender ver.%s (%s)' % (self.meArnoldRenderVer, self.os),
				menuBar=True,
				retain=False,
				widthHeight=(500, 460)
			)

		self.mainMenu = cmds.menu(label='Commands', tearOff=False)
		cmds.menuItem(label='Render Globals ...', command=maya_render_globals)
		cmds.menuItem(label='Check Texture Paths ...', command = checkTextures)
		cmds.menuItem(label='Generate .ass', command=self.generate_ass)
		cmds.menuItem(label='Submit Job', command=self.submitJob)
		cmds.menuItem(divider=True)
		cmds.menuItem(label='Close', command=self.deleteUI)

		#
		# setup render layers script jobs
		#
		cmds.scriptJob(
			attributeChange=[
				'renderLayerManager.currentRenderLayer',
				'import maya.OpenMaya; '
				'maya.cmds.evalDeferred('
				'    "meArnoldRender.renderLayerSelected()",'
				'    lowestPriority=True'
				')'
			],
			parent=self.winMain
		)
		cmds.scriptJob(
			event=[
				'renderLayerChange',
				'import maya.OpenMaya; '
				'maya.cmds.evalDeferred('
				'    "meArnoldRender.renderLayerChanged()",'
				'    lowestPriority=True'
				')'
			],
			parent=self.winMain
		)

		cw1 = 120
		cw2 = 60
		cw3 = 20

		ar_hi = 8

		form = cmds.formLayout('f0', numberOfDivisions=100)
		proj = cmds.columnLayout(
			'c0',
			columnAttach=('left', 0),
			rowSpacing=2,
			adjustableColumn=True,
			height=50
		)
		cmds.textFieldGrp(
			cw=(1, 70),
			adj=2,
			label='Project Root ',
			text=self.proj_path,
			editable=False
		)  # , bgc=(0,0,0)

		cmds.rowLayout('r0', numberOfColumns=4)
		layer_selector = cmds.optionMenuGrp(
			'layer_selector',
			cw=((1, 70),),
			cal=(1, 'right'),
			label='Render Layer ',
			cc=partial(self.onRenderLayerSelected)
		)
		self.updateRenderLayerMenu()

		cmds.checkBoxGrp(
			'ass_export_all_layers',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Export All Renderable ',
			value1=self.ass_param['ass_export_all_layers'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_export_all_layers',
				self.ass_param
			)
		)

		# --- job_preview

		def jobPreviewChanged(*args):
			setDefaultIntValue(*args)
			cmds.checkBoxGrp('job_preview_shotgun', e=1, enable=args[3])

		cmds.checkBoxGrp(
			'job_preview',
			ann='Automatically create preview',
			ebg=True,
			bgc=[0.2, 0.3, 0.2],
			cw=((1, cw2), (2, cw1 * 2)),
			label='Preview ',
			value1=self.job_param['job_preview'],
			cc=partial(
				jobPreviewChanged,
				self.prefix,
				'job_preview',
				self.job_param
			)
		)

		# -- job_preview_shotgun

		cmds.checkBoxGrp(
			'job_preview_shotgun',
			ann='Automatically send preview to "Shotgun"',
			cw=((1, cw2), (2, cw1 * 2)),
			ebg=True,
			bgc=[0.2, 0.3, 0.2],
			label='Send to Shotgun ',
			value1=self.job_param['job_preview_shotgun'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_preview_shotgun',
				self.job_param
			),
			enable=self.job_param['job_preview']
		)

		cmds.setParent('..')
		cmds.setParent('..')

		#
		# setup tabs
		#
		tab = cmds.tabLayout(
			't0',
			scrollable=True,
			childResizable=True
		)  # tabLayout -scr true -cr true  tabs; //

		#
		# Job tab
		#
		tab_job = cmds.columnLayout(
			'tc0',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.frameLayout(
			'fr1',
			label=' Parameters ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc1',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		job_dispatcher = cmds.optionMenuGrp(
			'job_dispatcher',
			cw=((1, cw1),),
			cal=(1, 'right'),
			label='Job Dispatcher ',
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'job_dispatcher',
				self.job_param
			)
		)

		for name in ('none', 'afanasy'):
			cmds.menuItem(label=name)  # 'backburner',

		cmds.optionMenuGrp(
			job_dispatcher,
			e=True,
			value=self.job_param['job_dispatcher']
		)

		cmds.text(label='')

		cmds.textFieldGrp(
			'job_name',
			cw=(1, cw1),
			adj=2,
			label='Job Name ',
			text=self.job_param['job_name'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'job_name',
				self.job_param
			)
		)

		cmds.textFieldGrp(
			'job_description',
			cw=(1, cw1),
			adj=2,
			label='Description ',
			text=self.job_param['job_description'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'job_description',
				self.job_param
			)
		)

		cmds.checkBoxGrp(
			'job_paused',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Start Paused ',
			value1=self.job_param['job_paused'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_paused',
				self.job_param
			)
		)

		cmds.text(label='')
		cmds.checkBoxGrp(
			'job_animation',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Animation ',
			value1=self.job_param['job_animation'],
			cc=partial(self.enable_range)
		)

		cmds.intFieldGrp(
			'job_range',
			cw=((1, cw1), (2, cw2), (3, cw2), (4, cw2)),
			nf=3,
			label='Start/Stop/By ',
			value1=self.job_param['job_start'],
			value2=self.job_param['job_end'],
			value3=self.job_param['job_step'],
			enable=self.job_param['job_animation'],
			cc=partial(
				setDefaultIntValue3,
				self.prefix,
				('job_start', 'job_end', 'job_step'),
				self.job_param
			)
		)

		cmds.intFieldGrp(
			'job_size',
			cw=((1, cw1), (2, cw2)),
			label='Task Size ',
			ann='Should be smaller then number of frames to render',
			value1=self.job_param['job_size'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_size',
				self.job_param
			)
		)

		cmds.intFieldGrp(
			'job_priority',
			cw=((1, cw1), (2, cw2)),
			label='Priority ',
			value1=self.job_param['job_priority'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_priority',
				self.job_param
			)
		)
		# -- job_overwrite_workspace
		cmds.checkBoxGrp(
			'job_overwrite_workspace',
			cw=((1, cw1), (2, cw1 * 2)),
			ebg=True,
			bgc=[0.2, 0.3, 0.2],
			ann='Default is ON. Uncheck for your own risk.\n You can have errors with file paths',
			label='Overwrite workspace',
			value1=self.job_param['job_overwrite_workspace'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_overwrite_workspace',
				self.job_param
			)
		)

		cmds.setParent('..')
		cmds.setParent('..')
		
		cmds.frameLayout(
			'fr2',
			label=' File Names Options ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi,
			cll=True,
			cl=True
		)

		cmds.columnLayout(
			'fc2',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)
		
		cmds.intFieldGrp(
			'job_padding',
			cw=((1, cw1), (2, cw2)),
			label='Frame Padding ',
			value1=self.job_param['job_padding'],
			cc=partial(self.jobFileNameOptionsChanged, 'job_padding')
		)
		
		job_separator = cmds.optionMenuGrp(
			'job_separator',
			cw=((1, cw1),),
			cal=(1, 'right'),
			label='Separator ',
			cc=partial( self.jobFileNameOptionsChanged, 'job_separator' ) 
		)

		for name in job_separator_list:
			cmds.menuItem(label=name)  

		cmds.optionMenuGrp(
			job_separator,
			e=True,
			value=self.job_param['job_separator']
		)
		
		cmds.setParent('..')
		cmds.setParent('..')

		cmds.frameLayout(
			'fr3',
			label=' Cleanup ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi,
			cll=True,
			cl=True
		)

		cmds.columnLayout(
			'fc3',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)
		
		cmds.checkBoxGrp(
			'job_cleanup_ass',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' .ass files',
			value1=self.job_param['job_cleanup_ass'],
			enable=True,
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_cleanup_ass',
				self.job_param
			)
		)

		cmds.checkBoxGrp(
			'job_cleanup_maya',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' maya _deferred file',
			value1=self.job_param['job_cleanup_maya'],
			enable=True,
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'job_cleanup_maya',
				self.job_param
			)
		)
		
		cmds.text( label ='\n   Note: Files will be cleaned after the job delete', al='left' )
		
		cmds.setParent('..')
		cmds.setParent('..')
		
		cmds.setParent('..')

		#
		# .ass files generation tab
		#
		tab_assparam = cmds.columnLayout(
			'tc1',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.frameLayout(
			'fr3',
			label=' Deferred .ass generation ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi,
			cll=True,
			cl=True
		)

		cmds.columnLayout(
			'fc3',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.checkBoxGrp(
			'ass_deferred',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Use deferred ',
			ann='Generate .ass files in background process',
			value1=self.ass_param['ass_deferred'],
			cc=partial(self.enable_deferred)
		)

		cmds.checkBoxGrp(
			'ass_local_assgen',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=" Only on localhost ",
			ann="Do not use remote hosts",
			value1=self.ass_param['ass_local_assgen'],
			enable=self.ass_param['ass_deferred'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_local_assgen',
				self.ass_param
			)
		)

		cmds.intFieldGrp(
			'ass_def_task_size',
			cw=((1, cw1), (2, cw2)),
			label='Task Size ',
			value1=self.ass_param['ass_def_task_size'],
			enable=self.ass_param['ass_deferred'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_def_task_size',
				self.ass_param
			)
		)

		#self.save_frame_bgc = cmds.frameLayout ( 'fr3', query = True, bgc = True )
		#self.def_frame_bgc = [ 0.75, 0.5, 0 ]
		bg_color = self.save_frame_bgc

		if self.ass_param['ass_deferred']:
			bg_color = self.def_frame_bgc

		cmds.frameLayout(
			self.winMain + '|f0|t0|tc1|fr3',
			edit=True,
			bgc=bg_color
		)  # , enableBackground=False

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.frameLayout(
			'fr1',
			label=' Export Settings ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc1',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.checkBoxGrp(
			'ass_reuse',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Use existing .ass files ',
			ann='Do not generate .ass files if they are exist',
			value1=self.ass_param['ass_reuse'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_reuse',
				self.ass_param
			)
		)

		cmds.text(label='')

		ass_dirname = cmds.textFieldButtonGrp(
			'ass_dirname',
			cw=(1, cw1),
			enable=True,
			adj=2,
			label='Directory Name ',
			buttonLabel='...',
			text=self.ass_param['ass_dirname'],
			cc=partial(self.assDirNameChanged, 'ass_dirname')
		)

		cmds.textFieldButtonGrp(
			ass_dirname,
			edit=True,
			bc=partial(
				browseDirectory,
				self.proj_path,
				ass_dirname
			),
			cc=partial(self.assDirNameChanged, 'ass_dirname')
		)
		"""
		cmds.checkBoxGrp(
			'ass_perframe',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' File Per Frame ',
			value1=self.ass_param['ass_perframe'],
			cc=partial(self.assDirNameChanged, 'ass_perframe')
		)
		"""
		cmds.checkBoxGrp(
			'ass_selection',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Export Only Selected Objects',
			value1=self.ass_param['ass_selection'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_selection',
				self.ass_param
			)
		)

		cmds.checkBoxGrp(
			'ass_binary',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Binary',
			value1=self.ass_param['ass_binary'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_binary',
				self.ass_param
			)
		)

		cmds.checkBoxGrp(
			'ass_compressed',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Compression',
			value1=self.ass_param['ass_compressed'],
			enable=not self.ass_param['ass_deferred'],
			cc=partial(self.assDirNameChanged, 'ass_compressed')
		)
	
		cmds.checkBoxGrp(
			'ass_expand_procedurals',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Expand Procedurals',
			value1=self.ass_param['ass_expand_procedurals'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_expand_procedurals',
				self.ass_param
			)
		)
		
		cmds.checkBoxGrp(
			'ass_export_bounds',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Export Bounding Box (.asstoc)',
			value1=self.ass_param['ass_export_bounds'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ass_export_bounds',
				self.ass_param
			)
		)

		cmds.setParent('..')
		cmds.setParent('..')
		

		cmds.frameLayout(
			'fr2',
			label=' Resolved Path ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc2',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.textFieldGrp(
			'ass_resolved_path',
			cw=(1, 0),
			adj=2,
			label='',
			text='',
			editable=False
		)

		self.setResolvedPath()

		cmds.setParent('..')
		cmds.setParent('..')

		cmds.setParent('..')

		#
		# Renderer tab
		#
		tab_render = cmds.columnLayout(
			'tc2',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)


		cmds.frameLayout(
			'fr1',
			label=' Arnold options ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc1',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.textFieldGrp(
			'ar_options',
			cw=(1, cw1),
			adj=2,
			label='Additional Options ',
			text=self.ar_param['ar_options'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'ar_options',
				self.ar_param
			)
		)

		cmds.text(label='')

		ar_verbosity = cmds.optionMenuGrp(
			'ar_verbosity',
			cw=((1, cw1),),
			cal=(1, 'right'),
			label='Verbosity ',
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'ar_verbosity',
				self.ar_param
			)
		)

		for name in ar_verbosity_list:
			cmds.menuItem(label=name)

		cmds.optionMenuGrp(
			ar_verbosity,
			e=True,
			value=self.ar_param['ar_verbosity']
		)

		cmds.intFieldGrp(
			'ar_threads',
			cw=((1, cw1), (2, cw2)),
			label='Threads ',
			ann='The number of threads',
			value1=self.ar_param['ar_threads'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ar_threads',
				self.ar_param
			)
		)

		cmds.setParent('..')
		cmds.setParent('..')
		
		cmds.frameLayout(
			'fr2',
			label=' Search Paths ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi,
			cll=True,
			cl=True
		)

		cmds.columnLayout(
			'fc2',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.checkBoxGrp(
			'ar_abs_tex_path',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Absolute Texture Paths',
			value1=self.ar_param['ar_abs_tex_path'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ar_abs_tex_path',
				self.ar_param
			)
		)
		
		cmds.checkBoxGrp(
			'ar_abs_proc_path',
			cw=((1, cw1), (2, cw1 * 2)),
			label='',
			label1=' Absolute Procedural Paths',
			value1=self.ar_param['ar_abs_proc_path'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'ar_abs_proc_path',
				self.ar_param
			)
		)
		
		cmds.textFieldGrp(
			'ar_plugin_path',
			cw=(1, cw1),
			adj=2,
			label='Plug-ins Path ',
			text=self.ar_param['ar_plugin_path'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'ar_plugin_path',
				self.ar_param
			)
		)
		
		cmds.textFieldGrp(
			'ar_proc_search_path',
			cw=(1, cw1),
			adj=2,
			label='Procedural Search Path ',
			text=self.ar_param['ar_proc_search_path'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'ar_proc_search_path',
				self.ar_param
			)
		)
		
		cmds.textFieldGrp(
			'ar_shader_search_path',
			cw=(1, cw1),
			adj=2,
			label='Shaders Search Path ',
			text=self.ar_param['ar_shader_search_path'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'ar_shader_search_path',
				self.ar_param
			)
		)
		
		cmds.textFieldGrp(
			'ar_tex_search_path',
			cw=(1, cw1),
			adj=2,
			label='Textures Search Path ',
			text=self.ar_param['ar_tex_search_path'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'ar_tex_search_path',
				self.ar_param
			)
		)
		
		cmds.setParent('..')
		cmds.setParent('..')

		cmds.setParent('..')

		#
		# Afanasy tab
		#
		tab_afanasy = cmds.columnLayout(
			'tc3',
			columnAttach=('left', 0),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.frameLayout(
			'fr1',
			label=' Parameters ',
			borderVisible=True,
			borderStyle='etchedIn',
			marginHeight=ar_hi
		)

		cmds.columnLayout(
			'fc1',
			columnAttach=('left', 4),
			rowSpacing=0,
			adjustableColumn=True
		)

		cmds.intFieldGrp(
			'af_capacity',
			cw=((1, cw1), (2, cw2)),
			label='Task Capacity ',
			value1=self.afanasy_param['af_capacity'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'af_capacity',
				self.afanasy_param
			)
		)

		cmds.intFieldGrp(
			'af_deferred_capacity',
			cw=((1, cw1), (2, cw2)),
			label='Deferred Capacity ',
			value1=self.afanasy_param['af_deferred_capacity'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'af_deferred_capacity',
				self.afanasy_param
			)
		)

		cmds.checkBoxGrp(
			'af_use_var_capacity',
			cw=((1, cw1), (2, cw1 * 2)),
			label='Use Variable Capacity ',
			ann='Block can generate tasks with capacity*coefficient to fit '
				'free render capacity',
			value1=self.afanasy_param['af_use_var_capacity'],
			cc=partial(self.enable_var_capacity)
		)

		cmds.floatFieldGrp(
			'af_var_capacity',
			cw=((1, cw1), (2, cw2), (3, cw2), (4, cw2)),
			nf=2,
			pre=2,
			label='Min/Max coefficient ',
			value1=self.afanasy_param['af_cap_min'],
			value2=self.afanasy_param['af_cap_max'],
			enable=self.afanasy_param['af_use_var_capacity'],
			cc=partial(
				setDefaultFloatValue2,
				self.prefix,
				('af_cap_min', 'af_cap_max'),
				self.afanasy_param
			)
		)

		cmds.intFieldGrp(
			'af_max_running_tasks',
			cw=((1, cw1), (2, cw2)),
			label='Max Running Tasks ',
			value1=self.afanasy_param['af_max_running_tasks'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'af_max_running_tasks',
				self.afanasy_param
			)
		)

		cmds.intFieldGrp(
			'af_max_tasks_per_host',
			cw=((1, cw1), (2, cw2)),
			label='Max Tasks Per Host ',
			value1=self.afanasy_param['af_max_tasks_per_host'],
			cc=partial(
				setDefaultIntValue,
				self.prefix,
				'af_max_tasks_per_host',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_service',
			cw=(1, cw1),
			adj=2,
			label='Service ',
			text=self.afanasy_param['af_service'],
			enable=False,
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'af_service',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_deferred_service',
			cw=(1, cw1),
			adj=2,
			label='Deferred Service ',
			text=self.afanasy_param['af_deferred_service'],
			enable=False,
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'af_deferred_service',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_hostsmask',
			cw=(1, cw1),
			adj=2,
			label='Hosts Mask ',
			ann='Job run only on renders which host name matches this mask\n'
				'e.g.  .* or host.*',
			text=self.afanasy_param['af_hostsmask'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'af_hostsmask',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_hostsexcl',
			cw=(1, cw1),
			adj=2,
			label='Exclude Hosts Mask ',
			ann='Job can not run on renders which host name matches this '
				'mask\n e.g.  host.* or host01|host02',
			text=self.afanasy_param['af_hostsexcl'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'af_hostsexcl',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_depmask',
			cw=(1, cw1),
			adj=2,
			label='Depend Mask ',
			ann='Job will wait other user jobs which name matches this mask',
			text=self.afanasy_param['af_depmask'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'af_depmask',
				self.afanasy_param
			)
		)

		cmds.textFieldGrp(
			'af_depglbl',
			cw=(1, cw1),
			adj=2,
			label='Global Depend Mask ',
			ann='Job will wait other jobs from any user which name matches '
				'this mask',
			text=self.afanasy_param['af_depglbl'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'af_depglbl',
				self.afanasy_param
			)
		)

		"""
		cmds.textFieldGrp(
			'af_os',
			cw=(1, cw1),
			adj=2,
			label='Needed OS ',
			ann='windows linux mac',
			text=self.afanasy_param['af_os'],
			cc=partial(
				setDefaultStrValue,
				self.prefix,
				'af_os',
				self.afanasy_param
			)
		)
		"""
		cmds.checkBoxGrp(
			'af_os',
			cw=((1, cw1), (2, cw1/2), (3, cw1/2), (4, cw1/2)),
			numberOfCheckBoxes = 3,
			label='Needed OS  ',
			label1='windows',
			value1=( self.afanasy_param['af_os'].find('windows') != -1 ),
			label2='linux',
			value2=( self.afanasy_param['af_os'].find('linux') != -1 ),
			label3='mac',
			value3=( self.afanasy_param['af_os'].find('mac') != -1 ),
			cc=partial( self.set_needed_os )
		)

		cmds.setParent('..')
		cmds.setParent('..')
		cmds.setParent('..')

		cmds.tabLayout(
			tab,
			edit=True,
			tabLabel=(
				(tab_job, "Job"),
				(tab_assparam, ".ass"),
				(tab_render, "Renderer"),
				(tab_afanasy, "Afanasy")
			)
		)

		cmds.setParent(form)

		btn_sbm = cmds.button(
			label='Submit',
			command=self.submitJob,
			ann='Generate .ass files and submit to dispatcher'
		)

		btn_gen = cmds.button(
			label='Generate .ass',
			command=self.generate_ass,
			ann='Force .ass files generation'
		)
		btn_cls = cmds.button(label='Close', command=self.deleteUI)

		cmds.formLayout(
			form,
			edit=True,
			attachForm=(
				(proj, 'top', 0),
				(proj, 'left', 0),
				(proj, 'right', 0),
				(tab, 'left', 0),
				(tab, 'right', 0),
				(btn_cls, 'bottom', 0),
				(btn_gen, 'bottom', 0),
				(btn_sbm, 'bottom', 0),
				(btn_sbm, 'left', 0),
				(btn_cls, 'right', 0)
			),
			attachControl=(
				(tab, 'top', 0, proj),
				(tab, 'bottom', 0, btn_sbm),
				(btn_gen, 'left', 0, btn_sbm),
				(btn_gen, 'right', 0, btn_cls)
			),
			attachPosition=(
				(btn_sbm, 'right', 0, 33),
				(btn_gen, 'right', 0, 66),
				(btn_cls, 'left', 0, 66)
			)
		)

		if not self.NoGUI :
			cmds.showWindow(self.winMain)

		return form
