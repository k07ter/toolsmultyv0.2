#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
import pprint
import os
import subprocess

sys.path.append('//parovoz-frm01/shotgun_parovoz')
sys.path.append('//parovoz-frm01/shotgun/utils')
sys.path.append('//parovoz-frm01/shotgun/utils2')

def upload_movie(path_to_movie, project_name, user_code):
    from shotgun_helpers import create_preview

    dumb_logger = get_dumb_logger()
    sg = get_sg()

    project_id = get_project_id(sg, project_name)
    user = get_user_entity(sg, user_code)

    preview = create_preview(sg, path_to_movie, project_id=project_id, user=user, external_logger=dumb_logger)
    pprint.pprint(preview)


def get_dumb_logger():
    import logging

    dumb_logger = logging.getLogger('dumb_logger')
    handler = logging.StreamHandler(sys.stdout)
    dumb_logger.addHandler(handler)

    return dumb_logger


def get_sg():
    from shotgun_api3 import Shotgun

    server_path = 'https://parovoz.shotgunstudio.com'
    script_name = 'previewUploader'
    script_key = 'zcxpLtcmhbfme3codyobcr~ab'

    sg = Shotgun(server_path, script_name, script_key)

    return sg


def get_project_id(sg, project):
    sg_project = sg.find_one('Project', [['sg_code', 'is', project]])

    return sg_project['id']


def get_user_entity(sg, user):
    user_entity = sg.find_one('HumanUser', [['sg_code', 'is', user]])

    return user_entity

def getCommand(mayaScenePath, layer, rootDir, files, user):
        import maya.mel as mm
        import uglyGuard as ugly
        sys.path.append('%s/plugins/maya/afanasy' % os.getenv('CGRU_LOCATION'))

        reload(ugly)
        try:
            parentUser = os.environ['BATCH_INITIAL_USER']
            print('\n***createRenderJob: BATCH_INITIAL_USER = %s') % parentUser
            user = parentUser
        except:
            print('\nWARNING: BATCH_INITIAL_USER not set. Send to shotgun task will be created under computer name')
            parentUser = os.environ['COMPUTERNAME']

        os.environ['PROJECT_PATH'] = rootDir
        prjLoc = os.environ['PROJECT_PATH'].replace('\\', '/')
        #		if ugly.checkMayaSceneProduction(mayaScenePath) is None:
        #			print ('Error: Incorrect scene name or scene path')
        #			return ''
        #		else:
        #			print ('Correct scene name and scene path')
        #			print (prjLoc, mayaScenePath)
        print(os.getenv('CGRU_LOCATION'))
        print(rootDir, mayaScenePath)
        print('PROJ_LOCATION  = %s' % prjLoc)
        projectName = 'moriki-doriki' #mayaScenePath.split(prjLoc)[1].split('/')[1]
        # print 'projectName = %s' % projectName
        files = files.replace('@', '')
        sceneNameNoExt = mayaScenePath.split('/')[-1].split('.')[0]
        # regex = '^%s/(moriki-doriki|crafts|lantern|patrol)/2_prod/ep[0-9]*/sh[0-9]*[a-z]{0,1}(_sub[0-9]*)*' % prjLoc
        # prev = re.match(regex, mayaScenePath).group(0).replace('2_prod', '3_post') + '/preview/%s%s%s' % (sceneNameNoExt,'_'*(len(layer)>0),layer)
        prev = mayaScenePath.replace('2_prod', '3_post').replace('/render/', '/preview/').split('.')[0]
        print('preview base name = %s' % prev)
        cgru = os.environ['CGRU_LOCATION'].replace('\\', '/')
        mvmk = "%s/utilities/moviemaker/makemovie.py" % "//loco000/tools/toolsMultyV0.2/StudioTools/cgru"
        f = mm.eval("currentTimeUnitToFPS")
        codec = "%s/utilities/moviemaker/codecs/h264rgb_high.ffmpeg" % cgru  # codec with 2000k video bitrate
        tmpformat = "tga"
        res = "1920x1080"
        template = "%s/utilities/moviemaker/templates/parovoz_frame_template" % cgru
        shot = sceneNameNoExt
        cmd = 'python //loco000/tools/toolsDev/StudioTools/cgru/makemovieproxy.py {0} -f "{1}" -c "{2}" --tmpformat "{3}" -r "{4}" -t "{5}" --project "{6}" --shot "{7}" '.format(
            mvmk, f, codec, tmpformat, res, template, projectName, shot)
        ver = "001"
        activity = "render"
        draw169 = "0"
        draw235 = "0"
        lgfpath = "//loco000/tools/toolsDev/StudioTools/logo/parovoz_logo.png"  # here
        lgfsize = "0"
        lgfgrav = "NorthEast"
        n = 'mov'
        cmd += '--ver {0} --artist "{1}" --activity "{2}" --draw169 "{3}" --draw235 "{4}" --lgfpath "{5}" --lgfsize {6} --lgfgrav {7} -n {8} '.format(
            ver, user, activity, draw169, draw235, lgfpath, lgfsize, lgfgrav, n)
        if not files.startswith('//') and files.startswith('/'):
            files = '/' + files

        cmd += '{0} {1} '.format(files, prev)
        cmds = list()
        cmds.append(cmd)
        # =======================================================================
        # shotgun command:
        # =======================================================================
        thisModule = __file__.replace('.pyc', '.py').replace('\\', '/')
        shtgn_cmd = 'python {0} {1}.{2} {3} {4}'.format(thisModule, prev, n, projectName, user)
        cmds.append(shtgn_cmd)
        return cmds

if __name__ == '__main__':
    print(sys.argv)
    this_file = __file__

    path_to_movie = sys.argv[-3]
    project_name = sys.argv[-2]
    user_code = sys.argv[-1]

    do_upload = os.getenv('DO_UPLOAD', None)
    if do_upload:
        upload_movie(path_to_movie, project_name, user_code)

    else:
        print('INFO: Will resubmit the task from python27')
        os.environ['PYTHONPATH'] = ''
        os.environ['PYTHONHOME'] = ''
        os.environ['DO_UPLOAD'] = '1'

        command = ['C:/Program Files/Shotgun/Python/python.exe', this_file, path_to_movie, project_name,
                   user_code]

        proc = subprocess.Popen(command)
        proc.wait()
