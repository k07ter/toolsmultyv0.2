#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import glob
from pprint import pprint


from common import fileUtilities as Fut
from core.python.shotgun import find_tools
reload(find_tools)


try:
    import pymel.core as pm
    import maya.cmds as cmds

    if not pm.pluginInfo('sceneAssembly', loaded=True, query=True):
        pm.loadPlugin('sceneAssembly')
except Exception as err:
    print('Unable to load Maya Assembly Reference Plugin, %s' % err)


PRVZ_PROJECT_PATH = os.getenv('PRVZ_PROJECT_PATH')


def create_ad(gpu_path=None, scn_path=None, loc_name='loc'):
    """
    Создаём assemblyDefinition ноду. Есть в аргументах есть
    гпу и сцена, то для соответствующих делаем свои представления.
    :param gpu_path: путь к алембику для гпу представления.
    :param scn_path: путь к сцене для представления майской сцены.
    :param loc_name: имя локатора или ещё чего-то, не ясно чего.
    :return: assemblyDefinition нода.
    """

    ad_node = pm.assembly(n='ad', type='assemblyDefinition')

    if gpu_path:
        gpu_path = gpu_path.replace(PRVZ_PROJECT_PATH, '%root%')
        add_cache_repr(ad_node, gpu_path)
        ad_node.setDefaultType('gpu')

    if scn_path:
        scn_path = scn_path.replace(PRVZ_PROJECT_PATH, '%root%')
        add_scn_repr(ad_node, scn_path)

    add_loc_repr(ad_node, loc_name)

    return ad_node


def create_ar(ad_path, ar_name):
    """Сюда референсим сцену с ad.
    Абсолютный путь так же заменяет на относительный."""
    ar_node = pm.assembly(name=ar_name, type='assemblyReference')

    ad_path = ad_path.replace(PRVZ_PROJECT_PATH, '%root%')
    ar_node.definition.set(ad_path)

    return ar_node


def add_loc_repr(ad_node, loc_name='loc'):
    """Добавление представления локатора к assemblyDefinition ноде."""
    pm.assembly(ad_node, edit=True, createRepresentation='Locator', input=loc_name, repName='loc')
    pm.assembly(ad_node, edit=True, repLabel='loc', newRepLabel='loc')
    pm.assembly(ad_node, edit=True, active='loc')


def add_cache_repr(ad_node, gpu_path):
    """Добавление представления кеша к assemblyDefinition ноде."""
    pm.assembly(ad_node, edit=True, createRepresentation='Cache', input=gpu_path, repName='gpu')
    pm.assembly(ad_node, edit=True, repLabel='gpu', newRepLabel='gpu')
    pm.assembly(ad_node, edit=True, active='gpu')


def add_scn_repr(ad_node, scn_path):
    """Добавление представления сцены к assemblyDefinition ноде."""
    pm.assembly(ad_node, edit=True, createRepresentation='Scene', input=scn_path, repName='scn', repLabel='scn')
    pm.assembly(ad_node, edit=True, repLabel='scn', newRepLabel='scn')
    # pm.assembly(ad_node, edit=True, active='scn')


def get_objs():
    """
    Функция получает список обжей по группам в корне сцены.
    :return: список трансформов.
    """
    groups = filter(lambda x: x.getShape() is None and pm.nodeType(x) == 'transform', pm.ls(typ='transform'))
    objs = filter(lambda x: x.getParent() is None, groups)

    return objs


def ad_from_ars(ar_nodes=None):
    ar_nodes = pm.ls(et='assemblyReference') if not ar_nodes else ar_nodes
    if not ar_nodes:
        print('WARNING: No Assembly References in this scene...')
        return

    # Get root ad
    context = find_tools.get_context()
    ad_dict = {'Asset': context.entity['name'],
               'Step': 'assembly'}

    root_ad = find_tools.get_path_by_template_name('maya_asset_ad', ad_dict)
    new_ver_ad = find_tools.get_new_version('maya_asset_ad_ver', ad_dict)
    print(root_ad)
    print(new_ver_ad)

    pass


def generate_ad_from_current_scene():
    """
    :return: Наверное, будет возвращать путь к сцене,
    чтобы потом его запублишить в SG
    и ещё куда-нибудь.
    """

    # Сналачал получаем список подобъектов.
    objs = get_objs()
    if not objs:
        # Какая-то левая сцена. Валим отсюда.
        print('No OBJ in this scene. Nothing to do here!')
        return

    context = find_tools.get_context()
    tk = find_tools.get_tk()
    fields = context.as_template_fields(tk.templates['maya_asset_work'])
    ad_dict = {'Asset': fields['Asset'],
               'Step': 'assembly'}

    root_ad = find_tools.get_path_by_template_name('maya_asset_ad', ad_dict)
    new_ver_ad = find_tools.get_new_version('maya_asset_ad_ver', ad_dict)
    print(root_ad)
    print(new_ver_ad)

    assembly_dir = os.path.dirname(root_ad)
    if not os.path.isdir(assembly_dir):
        os.mkdir(assembly_dir)

    if len(objs) == 1:
        # return
        obj = objs[0]
        # Имя объекта, т.е. просто имя трансформа в корне сцены.
        ar_dict = ad_dict.copy()
        ar_dict.update({'material': 'scn'})
        # Нужно получить путь к сцене.
        scn_path = find_tools.get_new_version('maya_asset_ar', ar_dict)
        ar_dir = os.path.dirname(scn_path)
        if not os.path.isdir(ar_dir):
            os.mkdir(ar_dir)

        # нужно получить путь к гпу.
        gpu_path = scn_path.replace('_scn_', '_gpu_').replace('.ma', '.abc')
        obj.select()
        obj_matrix = obj.getTransformation()
        obj.setTransformation(
            [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0],
             [0.0, 0.0, 0.0, 1.0]])
        cmds.file(scn_path, f=True, exportSelected=1, type='mayaAscii')

        gpu_name = os.path.basename(gpu_path).rsplit('.', 1)[0]
        pm.gpuCache(obj, directory=ar_dir, fileName=gpu_name, writeMaterials=1,
                    writeUVs=1, endTime=0, startTime=0,
                    dataFormat='ogawa')
        #
        obj.setTransformation(obj_matrix)
        pm.select(clear=True)

        ad_node = create_ad(gpu_path=gpu_path, scn_path=scn_path)
        ad_node.select()

        cmds.file(root_ad, exportSelected=True, type='mayaAscii', f=True)
        cmds.file(new_ver_ad, exportSelected=True, type='mayaAscii', f=True)

        pm.delete(ad_node)

    else:
        # Случай с дочерними объектами.
        ar_nodes = []

        for obj in objs:
            # Имя объекта, т.е. просто имя трансформа в корне сцены.
            obj_name = obj.name()
            ar_dict = ad_dict.copy()
            ar_dict.update({'name': obj_name, 'material': 'scn'})
            # Нужно получить путь к сцене.
            scn_path = find_tools.get_new_version('maya_asset_ar', ar_dict)
            ar_dir = os.path.dirname(scn_path)
            if not os.path.isdir(ar_dir):
                os.mkdir(ar_dir)

            obj_ad_path = scn_path.replace('_scn_', '_ad_')
            # нужно получить путь к гпу.
            gpu_path = scn_path.replace('_scn_', '_gpu_').replace('.ma', '.abc')
            print(scn_path)
            print(obj_ad_path)
            print(gpu_path)

            # Ну, делаем экспорт 3 компонентов обжей
            # gpu
            # scn
            # создаём ad

            # continue
            obj.select()
            obj_matrix = obj.getTransformation()
            obj.setTransformation(
                [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0],
                 [0.0, 0.0, 1.0, 0.0],
                 [0.0, 0.0, 0.0, 1.0]])
            cmds.file(scn_path, f=True, exportSelected=1, type='mayaAscii')

            gpu_name = os.path.basename(gpu_path).rsplit('.', 1)[0]
            pm.gpuCache(obj, directory=ar_dir, fileName=gpu_name,
                        writeMaterials=1,
                        writeUVs=1, endTime=0, startTime=0,
                        dataFormat='ogawa')

            obj.setTransformation(obj_matrix)
            pm.select(clear=True)

            ad_node = create_ad(gpu_path, scn_path)
            switch_repr(ad_node, 'gpu')
            ad_node.select()

            cmds.file(obj_ad_path, exportSelected=True, type='mayaAscii', f=True)

            pm.delete(ad_node)

            namespace = '%s_ad' % obj_name
            ar_name = '%s_ar' % obj
            ar_node = create_ar(obj_ad_path, ar_name)
            ar_node.repNamespace.set(namespace)
            ar_node.setTransformation(obj_matrix)

            ar_nodes.append(ar_node)

        # Экспорт в общий scn
        scn_dict = ad_dict.copy()
        pprint(scn_dict)
        scn_dict.update({'material': 'scn'})
        # Нужно получить путь к сцене.
        collect_scn = find_tools.get_new_version('maya_asset_ar', scn_dict)
        print(collect_scn)
        pm.select(ar_nodes)
        cmds.file(collect_scn, exportSelected=True, type='mayaAscii', f=True)
        pm.delete(ar_nodes)

        ad_node = create_ad(scn_path=collect_scn)
        ad_node.select()

        cmds.file(root_ad, exportSelected=True, type='mayaAscii', f=True)
        cmds.file(new_ver_ad, exportSelected=True, type='mayaAscii', f=True)

        pm.delete(ad_node)

    # Sync
    from core.python.sync import koshchey_kostil
    reload(koshchey_kostil)
    koshchey_kostil.sync_ma_recursive(root_ad)
    return root_ad

# ГПу кеш вынести в parovoz_cache_work
# Тут ещё нужно где-то определить путь к общим(версионному...
# и безверсионному) ad ассета.
# И ещё какой-то путь к ar.


def switch_repr(node, ar_type):
    active_label = node.getActiveLabel()
    list_of_reprs = node.getListRepresentations()

    if ar_type in list_of_reprs:
        if ar_type != active_label:
            node.setActiveLabel(ar_type)


def is_exportable(ar):
    reprs = ar.getListRepresentations()
    if 'scn' in reprs and 'loc' in reprs and 'gpu' in reprs:
        return True
    return False


def switch_to_gpu():
    ar_nodes = pm.ls(et='assemblyReference')

    for ar in ar_nodes:
        if is_exportable(ar):
            active_label = ar.getActiveLabel()
            if active_label != 'gpu':
                ar.setActiveLabel('gpu')
                print('%s is reference. Let us switch it to gpu' % ar.name())


def switch_all_to_gpu():
    for x in range(99):
        switch_to_gpu()


def test_switch():
    import timeit
    start_time = timeit.default_timer()

    counter = 1

    ar_nodes = pm.ls(et='assemblyReference')

    for ar in ar_nodes:
        # active_label = ar.getActiveLabel()
        list_of_reprs = ar.getListRepresentations()

        if 'gpu' in list_of_reprs and 'scn' in list_of_reprs:
            ar.setActiveLabel('scn')
            ar.setActiveLabel('gpu')

        counter += 1

    print('Processed %s objects' % counter)
    elapsed = timeit.default_timer() - start_time
    print('Eval time %s seconds' % elapsed)


def test_export_cache(abc_dir=None):
    import timeit
    start_time = timeit.default_timer()
    cut_in = int(cmds.playbackOptions(q=True, min=True))
    cut_out = int(cmds.playbackOptions(q=True, max=True))

    import parovoz_cache_work as pcw
    reload(pcw)

    # abc_dir = 'c:/temp/abc'
    if not os.path.isdir(abc_dir):
        os.makedirs(abc_dir)

    # Разворачиваем иерархию.
    counter = 1
    # switch_all_to_gpu()
    objects = get_objects()

    if objects:
        for obj in objects:
            obj_ns = obj.namespace()
            repr_ns = obj.repNamespace.get()
            meshes_ns = obj_ns + repr_ns + ':'
            objs_to_process = []

            if is_true_obj(obj):
                pass
                print('%s is True Object' % obj.name())
                objs_to_process.append(obj)
            else:
                childrens = get_ar_childrens(obj)
                print('%s Contain Sub Objects:' % obj.name())
                for c in childrens:
                    print('         %s' % c.name())
                    objs_to_process.append(c)
                print('')

            print('%s / %s' % (counter, len(objects)))
            # Переключаем всё в scn
            for o in objs_to_process:
                switch_repr(o, 'scn')

            transforms = pcw.CacheWorker().select_geo_by_mat(namespace=meshes_ns)
            pcw.CacheWorker().fake_face_set(transforms)
            write_cd(transforms)
            obj_name = obj.name().replace(':', '__')
            abc_path = '%s/obj_%s_.abc' % (abc_dir, obj_name)
            meshes = pcw.CacheWorker().transforms_to_string(transforms)
            pcw.CacheWorker().export_alembic(cut_in, cut_in, meshes, abc_path)

            # Возвращаем обратно в гпу
            for o in objs_to_process:
                switch_repr(o, 'gpu')

            counter += 1
            # break

    print('==References==')
    exporter = pcw.CacheWorker()
    for asset in exporter.namespaces:
        abc_path = '%s/%s.abc' % (abc_dir, asset.replace(':', '__'))
        exporter.export_shot_asset(asset, abc_path)

    print('\nProcessed %s objects' % counter)
    elapsed = timeit.default_timer() - start_time
    print('\nEval time %s seconds' % elapsed)


def test_import_asses(path_to):
    import timeit
    start_time = timeit.default_timer()

    import pymel.core as pm

    if not os.path.isdir(path_to):
        print('%s is unavailable' % path_to)
        return

    assets = glob.glob('%s/*.ass' % path_to)
    for asset in assets:
        import_back_ass(asset)


def import_back_ass(ass_file):
    pass

    node_name = os.path.basename(ass_file).rsplit('.', 1)[0]

    if not pm.objExists('ArnoldStandInDefaultLightSet'):
        pm.createNode("objectSet", name="ArnoldStandInDefaultLightSet", shared=True)
        pm.lightlink(object='ArnoldStandInDefaultLightSet', light='defaultLightSet')

    stand_in = pm.createNode('aiStandIn', n='%s_Shape' % node_name)
    pm.sets('ArnoldStandInDefaultLightSet', add=stand_in.name())
    transform = stand_in.getParent()
    transform.rename(node_name)

    stand_in.dso.set(ass_file)
    # stand_in.mode.set(0)  # bbox
    # stand_in.mode.set(4)  # point cloud
    # stand_in.mode.set(6)  # shaded
    # stand_in.mode.set(3)  # wireframe

    return transform


def test_export_asses():
    import timeit
    import maya.mel as mm

    start_time = timeit.default_timer()

    import parovoz_cache_work as pcw
    reload(pcw)

    abc_dir = 'c:/temp/abc'
    if not os.path.isdir(abc_dir):
        os.makedirs(abc_dir)

    # Разворачиваем иерархию.
    switch_all_to_gpu()
    objects = get_objects()
    counter = 1

    if not objects:
        print('INFO: No Assembly Objects in this shot')
        return

    for obj in objects:
        obj_ns = obj.namespace()
        repr_ns = obj.repNamespace.get()
        meshes_ns = obj_ns + repr_ns + ':'

        objs_to_process = []

        if is_true_obj(obj):
            pass
            print('%s is True Object' % obj.name())
            objs_to_process.append(obj)
        else:
            childrens = get_ar_childrens(obj)
            print('%s Contain Sub Objects:' % obj.name())
            for c in childrens:
                print('         %s' % c.name())
                objs_to_process.append(c)
            print('')

        print('%s / %s' % (counter, len(objects)))
        # Переключаем всё в scn
        for o in objs_to_process:
            switch_repr(o, 'scn')

        transforms = pcw.CacheWorker().select_geo_by_mat(namespace=meshes_ns)
        write_cd(transforms)
        set_subdiv(transforms)

        file_name = meshes_ns.replace(':', '__') + '.ass'
        file_path = '%s/%s' % (abc_dir, file_name)

        pm.select(transforms)
        command = 'arnoldExportAss -f "{0}" -s -mask 24 -lightLinks 0 -boundingBox -shadowLinks 0'.format(
            file_path)
        try:
            mm.eval(command)
        except:
            pass
        pm.select(clear=True)

        # Возвращаем обратно в гпу
        for o in objs_to_process:
            switch_repr(o, 'gpu')

        counter += 1
        # break

    print('\nProcessed %s objects' % counter)
    elapsed = timeit.default_timer() - start_time
    print('\nEval time %s seconds' % elapsed)


def set_subdiv(transforms):
    for t in transforms:
        mesh = t.getShape()
        mesh.aiSubdivType.set(1)
        mesh.aiSubdivIterations.set(1)


def is_true_obj(ar_node):
    list_of_reprs = ar_node.getListRepresentations()
    if 'gpu' in list_of_reprs:
        return True
    else:
        return False


def get_meshes():
    import parovoz_cache_work as pcw
    reload(pcw)

    all_transforms = []

    meshes = pm.ls(et='mesh')

    for m in meshes:
        t = m.parent(0)

        if t not in all_transforms:
            all_transforms.append(t)

    all_transforms = list(set(all_transforms))
    return pcw.CacheWorker().transforms_to_string(all_transforms)


def get_objects():
    pre_objs = []

    ar_nodes = pm.ls(et='assemblyReference')

    # Сначала получаем список всех ар, где есть гпу и сцена.
    for ar in ar_nodes:
        list_of_reprs = ar.getListRepresentations()
        if 'scn' in list_of_reprs and 'gpu' in list_of_reprs:
            pre_objs.append(ar)

    # Теперь нам нужно понять, какие из них настоящие obj, а какие sub_obj.
    # Для этого мы анализируем неймспейсы.
    true_objs = []
    for obj in pre_objs:
        parent_ar = get_parent_ar(obj)
        if not parent_ar:
            true_objs.append(obj)
            continue

        if is_obj_def(obj):
            true_objs.append(obj)
        else:
            true_objs.append(parent_ar)

    true_objs = list(set(true_objs))
    return true_objs


def is_obj_def(obj):
    def_path = obj.definition.get()
    def_dir = os.path.basename(os.path.dirname(def_path))

    if def_dir == 'assembly':
        return True
    else:
        return False


def get_parent_ar(obj):
    parent_ar = None
    all_parents = obj.getAllParents()
    if not all_parents:
        return None

    for p in all_parents:
        if p.nodeType() == 'assemblyReference':
            if 'scn' in p.getListRepresentations():
                return p

    return parent_ar


def get_ar_childrens(ar_node):
    childs = []

    all_childs = ar_node.listRelatives(children=True)
    if not all_childs:
        return []

    for child in all_childs:
        if child.nodeType() == 'assemblyReference':
            childs.append(child)

    return childs


def write_cd(transforms):
    for m in transforms:
        m = m.getShape()
        sg = m.listConnections(type='shadingEngine')
        if not sg:
            break

        sg = sg[0]
        lambert = sg.surfaceShader.inputs()[0]
        color = lambert.color.get()
        if pm.attributeQuery('Cd', node=m, exists=True):
            # print('Cd already Here')
            m.Cd.set(color)
        else:
            pm.addAttr(m, ln='Cd', type='float3')
            m.Cd.set(color)


def make_hou_scene():
    # from core.python.maya import assembly_reference
    # reload(assembly_reference)
    #
    # assembly_reference.make_hou_scene()

    import hou
    import glob

    abc_dir = '//omega/koshchey/temp/render_machine/' \
              'hou/sq020/sq020_sh0010/abc/objs'
    files = glob.glob('%s/*.abc' % abc_dir)

    files = [f.replace('\\', '/') for f in files]

    for f in files:
        print(f)
        base_name = os.path.basename(f)
        container = hou.node('/obj').createNode('geo', node_name=base_name,
                                                load_contents=False)
        container.parm('vm_rendersubd').set(True)
        container.parm('vm_shadingquality').set(0.5)
        for child in container.children():
            child.destroy()
        alembic_node = container.createNode('alembic',
                                            node_name='render_cache1')
        # alembic_node.parm('viewportlod').set(1)
        alembic_node.parm('curveFilter').set(0)
        alembic_node.parm('NURBSFilter').set(0)
        alembic_node.parm('pointsFilter').set(0)
        alembic_node.parm('subdFilter').set(0)

        alembic_node.parm('fileName').set(f)
