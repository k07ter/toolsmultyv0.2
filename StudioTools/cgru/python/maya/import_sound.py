#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import pymel.core as pm
from maya import cmds

from core.python.EntitySettings import EntitySettings
from core.python.utils_file import npath, get_latest, convert_path_relative_var
from core.python.shotgun import find_tools
reload(find_tools)


PRVZ_PROJECT_PATH = os.getenv('PRVZ_PROJECT_PATH')
TEMPLATE_NAME = 'shot_editorial_sound'


def run(offset=1, relative_project=True):
    scene = pm.system.sceneName()
    es = EntitySettings(scene, os.environ['PRVZ_TOOLS_PATH'])
    sound_file = get_latest(es.data['sound_mask'])
    # clean up
    if sound_file is not None:
        all_audio = pm.ls(et="audio")
        try:
            pm.delete(all_audio)
        except Exception as err:
            print 'ERROR: cannot clean up sound nodes: %s' % err
            return
        pm.importFile(sound_file, type="audio", ra=True, ignoreVersion=True, mergeNamespacesOnClash=False, options=("o=%s" % offset), pr=True)
        audio_node = pm.ls(et="audio")[0]
        if audio_node and relative_project:
            # set relative path
            attr_str = convert_path_relative_var(sound_file, os.environ['PRVZ_PROJECT_PATH'], verbose=True)
            if attr_str is not None:
                audio_node.filename.set(attr_str)
            return attr_str


def do_import(sound_files=None, offset=1):
    all_audio = pm.ls(et="audio")

    for s in sound_files:
        for a in all_audio:
            a.setLocked(lock=False)
            pm.delete(a)

        pm.importFile(s, type="audio", ra=True, ignoreVersion=True,
                      mergeNamespacesOnClash=False, options=("o=%s" % offset),
                      pr=True)
        audio_node = pm.ls(et="audio")[0]
        audio_node.filename.set(
            s.replace(PRVZ_PROJECT_PATH, '%root%'))


def import_sounds(offset=1):
    this_file = cmds.file(sn=True, q=True)

    tk = find_tools.get_tk()
    context = tk.context_from_path(this_file)

    template = tk.templates[TEMPLATE_NAME]
    fields = context.as_template_fields(template)
    fields['Step'] = 'editorial'
    latest_sound = find_tools.get_last_version_path(TEMPLATE_NAME, fields)

    if latest_sound:
        do_import([latest_sound], offset=offset)
    else:
        cmds.confirmDialog(t='Error!', m='This shot does not have any sounds!',
                           icon='Critical', button=['Ok'])
