

try:
    # for 2015 maya
    from PySide.QtGui import QWidget, QGridLayout, QTextEdit, QProgressBar, QDialog,  QPushButton #, QVBoxLayout, QWidget, QPushButton, QCheckBox, QLabel, QMenuBar, QMenu
    import shiboken
except ImportError:
    # for 2017 maya
    from PySide2.QtWidgets import QWidget, QGridLayout, QTextEdit, QProgressBar, QDialog, QPushButton
    import shiboken2 as shiboken


import pymel.core as pm
import maya.cmds as cmds
import re
import shutil
import logging
from core.python.utils import find_toools
#from PySide import QtCore, QtGui, QtNetwork
import os
from auto.classes.studio.ProjectMayaFile import ProjectMayaFile
'''
sot names must be sh0001
cmamera names must be 'cam_sh0001',
'''


if 'PRVZ_TOOLS_PATH' not in os.environ:
    os.environ['PRVZ_TOOLS_PATH'] = find_toools(pattern='tools3')
from core.python.logs import get_logger4, NoneLogger
log_storage = os.path.join(os.environ['PRVZ_TOOLS_PATH'], 'core', 'logs', 'sequence_exploder')
logger = get_logger4(__name__, log_storage=log_storage, roll_days=2, console=True)
logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)
#logger.setLevel(logging.ERROR)


class LogWindow(QDialog):
    def __init__(self, parent=None, title='So lonely ...', logger=None):
        super(LogWindow, self).__init__(parent)
        self.logger = logger or NoneLogger()
        #vb_layout = QtGui.QVBoxLayout()
        vb_layout = QGridLayout()

        self.setLayout(vb_layout)
        self.te_log = QTextEdit()
        self.resize(600, 100)
        self.setWindowTitle(title)

        #self.label = QtGui.QLabel()
        #icon = os.path.join(os.path.dirname(__file__), 'split_seq.jpg')
        #pixmap = QtGui.QPixmap(icon)
        #self.label.setPixmap(pixmap)
        #vb_layout.addWidget(self.label)

        # progress bar
        self.saw_progress = QProgressBar()
        self.saw_progress.setTextVisible(False)

        # button
        self.btn_saw = QPushButton()
        self.btn_saw.setText('Saw it')
        self.btn_saw.pressed.connect(self.btn_saw_pressed)


        #vb_layout.addWidget(self.label, 0, 0)

        vb_layout.addWidget(self.te_log, 0, 1)
        vb_layout.addWidget(self.btn_saw, 1, 0)
        vb_layout.addWidget(self.saw_progress, 1, 1)


    def info(self, message):
        self.te_log.append('[INFO] %s' % message)

    def error(self, message):
        self.te_log.append('[ERROR] %s' % message)

    def btn_saw_pressed(self):
        self.saw()

    def saw(self):
        src_scene = pm.Env().sceneName()
        if not src_scene:
            self.error('Scene not saved.')
            raise Exception('Scene not saved.')
        # save scene
        cmds.file(save=True)
        self.info('Scene saved ... ')

        # generate shots list:
        shots_list = []
        all_shots = pm.ls(type='shot')
        all_cameras = pm.ls(type='camera')
        for shot in all_shots:
            sh_obj = MayaShot(shot, logwin=self)
            #sh_obj.check() todo: remove?
            shots_list.append(sh_obj)

        # if ok starts...
        self.info('Writing to temp file ... ')

        ep = os.path.basename(src_scene).split('_')[0]
        self.info('Source scene %s' % src_scene)
        temp_scene = src_scene.replace('.ma', '_temp.ma')

        # dlg.info('Renaming to temp file ... ')
        cmds.file(rename=temp_scene)
        cmds.file(save=True)
        self.info('Temp scene %s' % temp_scene)

        # dlg.info('Generating ubercam ...')
        ucam_name = 'UBERCAMERA'
        pm.ubercam(ucam_name)
        self.info('Ubercam "%s" created' % ucam_name)

        self.info('Delete all shot nodes.')
        pm.delete(all_shots)
        self.info('Delete all cameras.')

        for cam in all_cameras:
            try:
                pm.delete(cam.parent(0))
            except Exception as err:
                self.debug('cannot delete %s.' % err)
        for n, sh in enumerate(shots_list):
            self.info('=== Generating shot %s ===' % sh.shotname)
            assert isinstance(sh, MayaShot)
            self.info('Change camera name: %s.' % sh.camera)
            pm.rename(ucam_name, sh.camera)
            ucam_name = sh.camera
            self.info('Save current scene.')
            cmds.file(save=True)
            shot_file_name = '%s_%s_v001.ma' % (ep, sh.shotname)
            self.info('Generate shot name: %s' % shot_file_name)
            shot_path = os.path.join(os.path.dirname(temp_scene), shot_file_name)
            self.info('Copy current file %s > %s.' % (temp_scene, shot_path))
            shutil.copy(temp_scene, shot_path)
            self.info('Apply time offset on  %s' % shot_path)
            pmf = ProjectMayaFile(shot_path, logger=self.logger)
            offset = -(sh.start - 1.)
            self.info('Offset = %s' % offset)

            #(!!!!!)comment coz we need set start-end frames:
            # if offset != 0.0:
            #     pmf.offset_animation(shot_path, shift=offset, frange=[1, sh.end + offset])
            # else:
            #     self.info('Offset skipped.')
            pmf.offset_animation(shot_path, shift=offset, frange=[1, sh.end + offset])

            self.saw_progress.setValue(float(n+1)/len(shots_list)*100)

class MayaShot(object):
    def __init__(self, shot, logwin=None):
        self.shot = shot
        self.shotname = self.shot.getName()
        self.__logwin = logwin
        self.camera = shot.getCurrentCamera()
        self.start = shot.getStartTime()
        self.end = shot.getEndTime()
        #self.__shrex = r'(?P<shot>[a-z]+)(?P<id>[0-9]+[a-z]*(_sub[0-9]+)*)'
        self.__shrex = 'sh\d\d\d\d[a-zA-Z]*$'
        #self.__filerex = 'ep'
    def check(self):
        if re.match(self.__shrex, self.shotname):
            pass
            #print 'OK..'
            # if not 'cam_%s' % self.shot.getName() == self.camera:
            #     err = 'Incorrect camera name: [%s] for shot [%s]' % (self.camera, self.shotname)
            #     self.__logwin.error(err)
            #     raise BaseException(err)
            # else:
            #     pass
            #     #print ('OK: camera name: [%s] for shot [%s]' % (self.camera, self.shot.getName()))
        else:
            raise BaseException('Incorrect name: %s' % (self.shotname))
        pass
    def generate_file(self):
        pass

    def __str__(self):
        return '[{shot:<15}{cam:<15} in: {start:<15} out :{end:<15}]'.format(shot=self.shotname, start=self.end, end=self.end, cam=self.camera)

# import auto.functions.explode_seq as es
# reload(es)
# es.run()

def start_win():
    # window:
    import maya.OpenMayaUI as omui
    #import shiboken
    def mayaMainWindow():
        return shiboken.wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)
    dlg = LogWindow(parent=mayaMainWindow(), title='Resawing sequence machine', logger=logger)
    dlg.show()
    return


# def saw(dlg):
#     src_scene = pm.Env().sceneName()
#     if not src_scene:
#         dlg.error('Scene not saved.')
#         raise Exception('Scene not saved.')
#     # save scene
#     cmds.file(save=True)
#     dlg.info('Scene saved ... ')
#
#     # generate shots list:
#     shots_list = []
#     all_shots = pm.ls(type='shot')
#     all_cameras = pm.ls(type='camera')
#     for shot in all_shots:
#         sh_obj = MayaShot(shot, logwin=dlg)
#         sh_obj.check()
#         shots_list.append(sh_obj)
#
#     # if ok starts...
#     dlg.info('Writing to temp file ... ')
#
#     ep = os.path.basename(src_scene).split('_')[0]
#     dlg.info('Source scene %s' % src_scene)
#     temp_scene = src_scene.replace('.ma', '_temp.ma')
#
#     #dlg.info('Renaming to temp file ... ')
#     cmds.file(rename=temp_scene)
#     cmds.file(save=True)
#     dlg.info('Temp scene %s' % temp_scene)
#
#     #dlg.info('Generating ubercam ...')
#     ucam_name = 'UBERCAMERA'
#     pm.ubercam(ucam_name)
#     dlg.info('Ubercam "%s" created' % ucam_name)
#
#     dlg.info('Delete all shot nodes.')
#     pm.delete(all_shots)
#     dlg.info('Delete all cameras.')
#
#     for cam in all_cameras:
#         try:
#             pm.delete(cam.parent(0))
#         except Exception as err :
#             dlg.debug('cannot delete %s.' % err)
#     for sh in shots_list:
#         dlg.info('=== Generating shot %s ===' % sh.shotname)
#         assert isinstance(sh, MayaShot)
#         dlg.info('Change camera name: %s.' % sh.camera)
#         pm.rename(ucam_name, sh.camera)
#         ucam_name = sh.camera
#         dlg.info('Save current scene.')
#         cmds.file(save=True)
#         shot_file_name = '%s_%s_v001.ma' % (ep, sh.shotname)
#         dlg.info('Generate shot name: %s' % shot_file_name)
#         shot_path = os.path.join(os.path.dirname(temp_scene), shot_file_name)
#         dlg.info('Copy current file %s > %s.' % (temp_scene, shot_path))
#         shutil.copy(temp_scene, shot_path)
#         dlg.info('Apply time offset on  %s' % shot_path)
#         pmf = ProjectMayaFile(shot_path)
#         offset = -(sh.start - 1.)
#         dlg.info('Offset = %s' % offset)
#         if offset != 0.0:
#             pmf.offset_animation(shot_path, shift=offset, frange=[1, sh.end + offset])
#         else:
#             dlg.debug('Offset skipped.')
#
#
