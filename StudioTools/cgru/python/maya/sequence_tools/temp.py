#!/usr/bin/python
# -*- coding: utf-8 -*-
import os

import re
import os
from core.python.utils import find_toools
import logging
from core.python.utils_file import convert_path_absolute, convert_path_relative_var
from xml_utils import final_cut_load
from core.python.utils_file import convert_path_relative_var

if 'PRVZ_TOOLS_PATH' not in os.environ:
    os.environ['PRVZ_TOOLS_PATH'] = find_toools(pattern='tools3')
from core.python.logs import get_logger4
log_storage = os.path.join(os.environ['PRVZ_TOOLS_PATH'], 'core', 'logs', 'sequence_builder')
logger = get_logger4(__name__, log_storage=log_storage, roll_days=2, console=True)
#logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)
logger.setLevel(logging.ERROR)

from pprint import pprint
import tank as sgtk
from tank_vendor.shotgun_authentication import ShotgunAuthenticator


class ShotCustomClass(object):
    def __init__(self, **sg_dict):
        self.sg_dict = sg_dict
        self.name = sg_dict['sg_name']#.split('_')[1]
        self.episode = sg_dict['Episode']
        # self.fps = None
        #self.cut_in = sg_dict['sg_cut_in']
        #self.cut_out = sg_dict['sg_cut_out']

        # В майском стиле - количество кадров (в премьере это диапазон) [0-1] премьер ==
        self.head_in = sg_dict['sg_head_in']
        self.duration = sg_dict['sg_duration_frames']
        #self.path = sg_dict['sg_reference']['local_path_windows']

        #fields = {'taskname': 'cut', 'version': None}

        self.camera_name = 'cam_%s_%s' % (self.episode, self.name)
        self.wav_last = sg_dict['wav_last']
        self.mov_last = sg_dict['mov_last']
        logger.debug('Camera name %s' % self.camera_name)

    def __cmp__(self, other):
        return cmp(self.name, other.name)

    def __repr__(self):
        return 'Shot: {sg_name} [head_in: {sg_head_in} Duration :{sg_duration_frames}]'.format(**self.sg_dict)


def run():
    os.environ['PRVZ_PROJECT_PATH'] = '//alpha/envell'
    path = r'//alpha/envell/2_prod/ep777/ep777_sq001/work/ep777_sq001_layout_v011.ma'

    SCRIPT_NAME = 'mytests'
    SCRIPT_KEY = '9f00d064d9bd22be554cfed822819fc0fca8711980d2f3e4a7da395a733ebaac'

    cdm = sgtk.authentication.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.create_script_user(SCRIPT_NAME, SCRIPT_KEY)
    sgtk.set_authenticated_user(user)

    tk = sgtk.sgtk_from_path(os.environ.get('PRVZ_PROJECT_PATH'))
    sg = tk.shotgun
    ctx = tk.context_from_path(path)






    sq_fieleds = ctx.as_template_fields(tk.templates['maya_sequence_work_area'])
    print sq_fieleds
    sg_shots_data = sg.find('Shot',
                            [['sg_sequence', 'is', ctx.entity]],
                            ['sg_head_in', 'sg_duration_frames', 'sg_fps', 'sg_name'])

    #print sg_shots_data
    all_shots = []
    fields = {'taskname': 'cut', 'version': None, 'Episode': sq_fieleds['CustomEntity01']}
    for shot_dict in sg_shots_data:
        shot_dict['Shot'] = shot_dict['sg_name']
        #shot_dict['Episode'] = sq_fieleds['CustomEntity01'] #shit
        #fields.update(shot_dict)
        pprint(fields)
        cuts_mov = tk.paths_from_template(tk.templates['shot_editorial_cut'], fields)
        if cuts_mov:
            cuts_mov.sort()
            shot_dict['mov_last'] = cuts_mov[-1]
        else:
            shot_dict['mov_last'] = None
        cuts_wav = tk.paths_from_template(tk.templates['shot_editorial_sound'], fields)
        if cuts_wav:
            cuts_wav.sort()
            shot_dict['wav_last'] = cuts_wav[-1]
        else:
            shot_dict['wav_last'] = None

        shot_dict.update(fields)
        #shot_data =
        all_shots.append(ShotCustomClass(**shot_dict))
        #print shot_data, shot_data.camera_name, shot_data.mov_last, shot_data.wav_last
    #pprint(sg_shots_data)
run()


#run()