#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import os
from core.python.utils import find_toools
import logging
from pprint import pprint
import time
from core.python.utils_file import convert_path_absolute, convert_path_relative_var
from xml_utils import final_cut_load
from core.python.utils_file import convert_path_relative_var

if 'PRVZ_TOOLS_PATH' not in os.environ:
    os.environ['PRVZ_TOOLS_PATH'] = find_toools(pattern='tools3')
from core.python.logs import get_logger4, NoneLogger
log_storage = os.path.join(os.environ['PRVZ_TOOLS_PATH'], 'core', 'logs', 'sequence_builder')
logger = get_logger4(__name__, log_storage=log_storage, roll_days=2, console=True)
#logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)
logger.setLevel(logging.ERROR)

try:
    import pymel.core as pm

except Exception as ex:
    logger.exception('Import shotgun and Maya modules: %s' % ex)

try:
    import tank as sgtk
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator

except Exception as ex:
    logger.exception('Import sgtk: %s' % ex)

CAMERA_CONFIG_ = {"centerOfInterest": 5,
                 "focalLength": 35,
                 "lensSqueezeRatio": 1,
                 "cameraScale": 1,
                 "horizontalFilmAperture": 1.679,
                 "horizontalFilmOffset": 0,
                 "verticalFilmAperture": 0.945,
                 "verticalFilmOffset": 0,
                 "filmFit": 'Fill',
                 "overscan": 1,
                 "motionBlur": 0,
                 "shutterAngle": 144,
                 "nearClipPlane": 0.1,
                 "farClipPlane": 10000,
                 "orthographic": 0,
                 "orthographicWidth": 30,
                 "panZoomEnabled": 0,
                 "horizontalPan": 0,
                 "verticalPan": 0,
                 "position": [0, 10, 0],
                 "zoom": 1
                 }


def error_dialog(message, title='Error', width=40):
    frmt = '{:<%s}' % width
    return pm.confirmDialog(title=frmt.format(title),
                     message=message,
                     button=['Ok'],
                     cancelButton='Ok')


def confirm_dialog(message, title='Warning', width=40):
    frmt = '{:<%s}' % width
    return pm.confirmDialog(title=frmt.format(title),
                     message=message,
                     button=['Ok'],
                     cancelButton='Ok')

#
# class CameraObject(object):
#     base_config = CAMERA_CONFIG_
#
#     def __init__(self):
#         # Maya nodes:
#         self.camera_node_tr = None  # type: pm.nodetypes.Transform
#         self.camera_node_sh = None  # type: pm.nodetypes.Camera
#         self.implane_node_sh = None  # type: pm.nodetypes.ImShape
#         self.video = None  # type: str
#         self.parent_shot = None  # type: ShotObject
#         pass
#
#

# class ShotgunGuts(object):
#     SCRIPT_NAME = 'mytests'
#     SCRIPT_KEY = '9f00d064d9bd22be554cfed822819fc0fca8711980d2f3e4a7da395a733ebaac'
#     cdm = sgtk.authentication.CoreDefaultsManager()
#     authenticator = ShotgunAuthenticator(cdm)
#     user = authenticator.create_script_user(SCRIPT_NAME, SCRIPT_KEY)
#     sgtk.set_authenticated_user(user)
#     tk = sgtk.sgtk_from_path(os.environ.get('PRVZ_PROJECT_PATH'))
#     sg = tk.shotgun



class MyException(Exception):
    pass

class ShotObject(object):
    maya_start = 1

    def __init__(self, **sg_dict):
        self.sg_dict = sg_dict  # type: dict
        # Maya nodes:
        self.shot_node = None  # type:

        self.camera_tr = None  # type: pm.nodetypes.Transform
        self.camera_sh = None  # type: pm.nodetypes.Camera
        self.name = sg_dict['sg_name']  # type: str
        self.episode = sg_dict['Episode']  # type: str
        # В майском стиле - количество кадров (в премьере это диапазон) [0-1] премьер ==
        self.head_in = sg_dict['sg_head_in']  # type: int
        self.duration = sg_dict['sg_duration_frames']  # type: int

        #self.audio = None  # type: str


        self.wav_last = sg_dict['wav_last']
        self.mov_last = sg_dict['mov_last']
        #self.camera_name = 'cam_%s_%s' % (self.episode, self.name)
        self.camera_name = 'cam_%s' % self.name
        # logger.debug('Camera name %s' % self.camera_name)
        pass
    def __repr__(self):
        info = '<< ShotObject >>: <shot node:{}> <camera node:{}> \n\tname:{}\n\tcamera_name:{}\n\tepisode:{} \n\thead_in:{}\n\tduration:{}\n\tA:{}\n\tV:{}'.\
            format(self.shot_node,
                   self.camera_sh,
                    self.name,
                   self.camera_name,
                   self.episode,
                   self.head_in,
                   self.duration,
                   self.wav_last,
                   self.mov_last
                    )
        return info

    def create_shot_node(self):
        pass

    def attach_camera(self):
        pass




#def create_maya_camera(shot_obj):


class MayaSequenceObject(object):
    camera_config = CAMERA_CONFIG_
    def __init__(self, logger=None):
        self.logger = logger or NoneLogger()
        #path = r'//alpha/envell/2_prod/ep777/ep777_sq001/work/ep777_sq001_layout_v011.ma'
        self.seq_obj = SequenceObject(pm.system.sceneName())
        self.seq_obj.generate_shot_objs()
        self.offset = 1 - self.seq_obj.fist_frame_in_queue

    def create_maya_camera(self, shot_obj):
        config = self.camera_config
        config['name'] = shot_obj.camera_name
        camera_tr, camera_sh = pm.camera(**config)
        # strange bug...
        pm.rename(camera_tr, config['name'])
        pm.rename(camera_sh, '%sShape' % config['name'])
        return camera_tr, camera_sh

    def animatic_in_camera(self, shot_obj):

        implane_tr, implane_sh = pm.imagePlane(
            camera=shot_obj.camera_sh)  # type: pm.nodetypes.Transform, pm.nodetypes.imagePlane
        implane_sh.attr('type').set(2)
        implane_sh.fileName(convert_path_relative_var(shot_obj.mov_last,
                                                      os.environ.get('PRVZ_PROJECT_PATH'),
                                                      verbose=True))

        expr_str = '{}.frameExtension=frame-{}'.format(implane_sh.name(), self.offset + shot_obj.head_in - 1)
        ex = pm.expression()
        ex.setString(expr_str)
        implane_sh.useFrameExtension.set(1)
        implane_sh.fit.set(0)
        implane_sh.alphaGain.set(0.5)
        implane_sh.depth.set(2)
        implane_sh.displayOnlyIfCurrent.set(1) #frameOffset" 10

    def create_maya_shots(self):
        for shot_obj in self.seq_obj.shots_objs:
            shot_obj.shot_node = pm.shot(startTime=shot_obj.head_in + self.offset,
                                 endTime=shot_obj.head_in + shot_obj.duration - 1 + self.offset,
                                 sequenceStartTime=shot_obj.head_in + self.offset,
                                 sequenceEndTime=shot_obj.head_in + shot_obj.duration - 1 + self.offset,
                                 clip='', shotName=shot_obj.name)


            pm.rename(shot_obj.shot_node, shot_obj.name)  # fix bug
            shot_obj.shot_node.setAudio(shot_obj.wav_last)
            shot_obj.shot_node.scale.lock()

            shot_obj.camera_tr, shot_obj.camera_sh = self.create_maya_camera(shot_obj)
            self.animatic_in_camera(shot_obj) #todo: check if commented
            self.load_assets()
            # attach camera:
            shot_obj.shot_node.setCurrentCamera(shot_obj.camera_tr)
            print shot_obj

    def load_assets(self):
        logger.info('Loading assets')
        for path in self.seq_obj.assets_paths:
            asset_namespace = os.path.basename(path).split('.')[0]
            try:
                path = convert_path_relative_var(path, os.environ['PRVZ_PROJECT_PATH'])
                pm.createReference(path, namespace=asset_namespace, mergeNamespacesOnClash=False)
                logger.info('OK: Referenced: "%s"' % path)

            except Exception as err:
                logger.exception('ERROR: cannot reference "%s": %s' % (path, err))


class SequenceObject(object):
    os.environ['PRVZ_PROJECT_PATH'] = '//alpha/envell'  # todo remove
    #path = r'//alpha/envell/2_prod/ep777/ep777_sq001/work/ep777_sq001_layout_v011.ma'
    #context = None  # tk.context_from_path(path)
    SCRIPT_NAME = 'mytests'
    SCRIPT_KEY = '9f00d064d9bd22be554cfed822819fc0fca8711980d2f3e4a7da395a733ebaac'
    cdm = sgtk.authentication.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.create_script_user(SCRIPT_NAME, SCRIPT_KEY)
    sgtk.set_authenticated_user(user)
    tk = sgtk.sgtk_from_path(os.environ.get('PRVZ_PROJECT_PATH'))
    sg = tk.shotgun

    def __init__(self, path, logger=None):
        self.logger = logger or NoneLogger()
        self.path = path
        self.logger = logger
        self.ctx = self.tk.context_from_path(self.path)

        self.fps = None
        self.shots_objs = []  # type: list[ShotObject]
        self.assets_paths = []  # type: list[str]
        self.sg_shots_data = []  # type: list[dict]
        self.fist_frame_in_queue = -1 # type: int
        pass


    def get_shots_sg_data(self):
        '''
        Запрос к шотгану о шотах
        :return:
        '''
        self.sg_shots_data = self.tk.shotgun.find('Shot', [['sg_sequence', 'is', self.ctx.entity]],
                                             ['sg_head_in', 'sg_duration_frames', 'sg_fps', 'sg_name'])

        assert self.sg_shots_data, Exception('Shots request empty. Check shots')
        self.fps = self.sg_shots_data[0]['sg_fps']

    def info(self):
        return {'Sequence Info': self.path,
                'fist_frame_in_queue': self.fist_frame_in_queue,
                'shots': self.sg_shots_data,
                'assets': self.assets_paths}


    def get_assets_sg_data(self):
        '''
        запрос к шотгану об ассетах
        :return:
        '''
        self.assets_paths = []  # clean list
        entity_id = self.ctx.entity['id']
        filters = [['id', 'is', entity_id]]
        fields = ['assets']
        arr_assets = self.sg.find('Sequence', filters=filters, fields=fields)[0]['assets']
        for asset in arr_assets:
            logger.debug('Asset: %s' % str(asset))
            sg_asset = self.sg.find_one('Asset',
                                        [['id', 'is', asset['id']]],
                                        ['code', 'sg_index', 'sg_asset_type', 'sg_type_one', 'sg_type_two'])
            if not sg_asset:
                raise Exception('Unknown Asset %s!', asset)
            reference_template = self.tk.templates["maya_asset_reference"]

            asset_path = reference_template.apply_fields({'Asset': sg_asset['sg_index'],
                                                          'type_one': sg_asset['sg_type_one']['name'],
                                                          'type_two': sg_asset['sg_type_two']['name'],
                                                          'task_process': 'rig'})
            self.assets_paths.append(asset_path)
            assert self.assets_paths, Exception('Assets request empty. Check assets')
        pass


    def check_data(self):
        # todo: code!
        pass

    def generate_shot_objs(self):
        self.get_shots_sg_data()
        self.shots_objs = []
        sq_fieleds = self.ctx.as_template_fields(self.tk.templates['maya_sequence_work_area'])
        fields = {'taskname': 'cut', 'version': None, 'Episode': sq_fieleds['CustomEntity01']}

        for shot_dict in self.sg_shots_data:
            shot_dict['Shot'] = shot_dict['sg_name']
            shot_dict.update(fields)
            cuts_mov = self.tk.paths_from_template(self.tk.templates['shot_editorial_cut'], shot_dict)
            if cuts_mov:
                cuts_mov.sort()
                shot_dict['mov_last'] = cuts_mov[-1]
            else:
                shot_dict['mov_last'] = None
            cuts_wav = self.tk.paths_from_template(self.tk.templates['shot_editorial_sound'], shot_dict)
            if cuts_wav:
                cuts_wav.sort()
                shot_dict['wav_last'] = cuts_wav[-1]
            else:
                shot_dict['wav_last'] = None
            shot_obj = ShotObject(**shot_dict)
            self.shots_objs.append(shot_obj)

        self.fist_frame_in_queue = min([sh.head_in for sh in self.shots_objs])
        pprint (self.shots_objs)


def run():
    mso = MayaSequenceObject()
    mso.create_maya_shots()
if __name__ == "__main__":
    path = r'//alpha/envell/2_prod/ep777/ep777_sq001/work/ep777_sq001_layout_v011.ma'
    so = SequenceObject(path)
    so.generate_shot_objs()
    #so.get_shots_sg_data()
    #so.get_assets_sg_data()


    pprint(so.info())

    #p = r'\\alpha\envell\2_prod\ep777\ep777_sq001\cut\ep777_sq001_layout_v001.xml'
    p = r'c:\_temp\xml\ep068_sq001_shots_v001.xml '
    p = r'c:\_temp\xml\test_project.xml'
    p = r'c:\_temp\xml\ep068_clean.xml' #<name>id555_sh1303</name>
    #read_xml(p)