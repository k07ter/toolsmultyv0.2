# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET

def get_item_textOLD(item, str_child, out_item):
    str_item = item.find(str_child)
    if str_item is not None:
        out_item[str_child] = str_item.text
        return True
    
    return None


def get_item_text(item, str_child, out_item):
    str_item = item.findall(str_child)
    if str_item is not None:
        if len(str_item) > 1:
            for n, item in enumerate(str_item):
                out_item['%s%s' % (str_child, n)] = item.text
        else:
            out_item[str_child] = str_item.text
            return True

    return None

def get_item_int(item, str_child, out_item):
    if not get_item_text(item, str_child, out_item):
        return None
    
    out_item[str_child] = int(float(out_item[str_child]))
    return True
    
def get_item_bool(item, str_child, out_item):
    if not get_item_text(item, str_child, out_item):
        return None
    
    if out_item[str_child].lower() == "true":
        out_item[str_child] = True
    else:
        out_item[str_child] = False    

    return True

def final_cut_load(file_xml):
    tree = ET.parse(file_xml)
    root = tree.getroot()
    if root.find('project') != None:
        root = root.find('project').find('children')
    
    arr_ret = {'tracks':[], 'files':{}}
    
    video = root.find('sequence').find('media').find('video')
    for track in video.findall('track'):
        track_ret = {'clipitems':[]}
        get_item_bool(track, "enabled", track_ret)
        for clipitem in track.findall('clipitem'):
            #for child in clipitem:
            #   print child.tag, child.attrib

            #continue
            
            clipitem_ret = {}
            get_item_text(clipitem, "name", clipitem_ret)
            get_item_bool(clipitem, "enabled", clipitem_ret)
            get_item_int(clipitem, "duration", clipitem_ret)
            get_item_int(clipitem, "start", clipitem_ret)
            get_item_int(clipitem, "end", clipitem_ret)
            get_item_int(clipitem, "in", clipitem_ret)
            get_item_int(clipitem, "out", clipitem_ret)
            get_item_int(clipitem.find("rate"), "timebase", clipitem_ret)
            
            file = clipitem.find("file")
            file_id = file.attrib['id']
            clipitem_id = clipitem.attrib['id']
            
            pathurl = file.find("pathurl")
            if pathurl is not None:
                pathurl = pathurl.text

            if file_id:
                if pathurl and file_id not in arr_ret['files']:
                    file_ret = {'pathurl':pathurl}
                    get_item_text(file.find('rate'), "timebase" , file_ret)
                    get_item_int(file, "duration", file_ret)
                    arr_ret['files'][file_id] = file_ret                    
                    
                clipitem_ret['file_id'] = file_id
            
            clipitem_ret['id'] = clipitem_id
            track_ret['clipitems'].append(clipitem_ret)
        
        if len(track_ret['clipitems']):
            arr_ret['tracks'].append(track_ret)
    
    return arr_ret
    
