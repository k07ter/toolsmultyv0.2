#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import os
from core.python.utils import find_toools
import logging
import time
from core.python.utils_file import convert_path_absolute, convert_path_relative_var
from xml_utils import final_cut_load
from core.python.utils_file import convert_path_relative_var

if 'PRVZ_TOOLS_PATH' not in os.environ:
    os.environ['PRVZ_TOOLS_PATH'] = find_toools(pattern='tools3')
from core.python.logs import get_logger4
log_storage = os.path.join(os.environ['PRVZ_TOOLS_PATH'], 'core', 'logs', 'sequence_builder')
logger = get_logger4(__name__, log_storage=log_storage, roll_days=2, console=True)
#logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)
logger.setLevel(logging.ERROR)

try:
    import pymel.core as pm
    import tank as sgtk
    from tank_vendor.shotgun_authentication import ShotgunAuthenticator
except Exception as ex:
    logger.exception('Import shotgun and Maya modules: %s' % ex)


CAMERA_CONFIG_ = {"centerOfInterest": 5,
                 "focalLength": 35,
                 "lensSqueezeRatio": 1,
                 "cameraScale": 1,
                 "horizontalFilmAperture": 1.679,
                 "horizontalFilmOffset": 0,
                 "verticalFilmAperture": 0.945,
                 "verticalFilmOffset": 0,
                 "filmFit": 'Fill',
                 "overscan": 1,
                 "motionBlur": 0,
                 "shutterAngle": 144,
                 "nearClipPlane": 0.1,
                 "farClipPlane": 10000,
                 "orthographic": 0,
                 "orthographicWidth": 30,
                 "panZoomEnabled": 0,
                 "horizontalPan": 0,
                 "verticalPan": 0,
                 "position": [0, 10, 0],
                 "zoom": 1
                 }


def error_dialog(message, title='Error', width=40):
    frmt = '{:<%s}' % width
    pm.confirmDialog(title=frmt.format(title),
                     message=message,
                     button=['Ok'],
                     cancelButton='Ok')


def confirm_dialog(message, title='Warning', width=40):
    frmt = '{:<%s}' % width
    pm.confirmDialog(title=frmt.format(title),
                     message=message,
                     button=['Ok'],
                     cancelButton='Ok')


def read_xml(path):
    CutClips.parse_cut_xml(path)
    data = CutClips.clips_index.values()

    data = map(lambda x: str(x) + '\n', data)
    with open(r'c:\_temp\xml\%s_SCRIPT.txt' % os.path.basename(path), 'w') as fh:
        fh.writelines(data)


class CutClips(object):
    clips_index = {}  # {clip_name: CutClip}

    def __init__(self, name):
        self.name = name
        self.cut_in = 0
        self.cut_out = 0
        self.timebase = 0
        self.start = 0
        self.end = 0
        CutClips.clips_index[name] = self

    @classmethod
    def get_by_name(cls, name):
        return cls.clips_index[name] if name in cls.clips_index else cls(name)

    @staticmethod
    def parse_cut_xml(xml_name):
        shot_cuts = final_cut_load(xml_name)
        for track in shot_cuts['tracks']:
            for clip in track['clipitems']:
                if clip['enabled']:
                    cut_clip = CutClips.get_by_name(clip['name'])
                    cut_clip.cut_in = clip['in']
                    cut_clip.cut_out = clip['out']
                    cut_clip.start = clip['start']
                    cut_clip.end = clip['end']
                    cut_clip.timebase = clip['timebase']

    def __repr__(self):
        return 'cut clip <name: %s in: %d out: %d start: %d end: %d fps: %d' % (self.name,
                                                                                self.cut_in, self.cut_out,
                                                                                self.start, self.end,
                                                                                self.timebase)


class CameraCustom(object):
    config = CAMERA_CONFIG_

    def __init__(self, custom_shot, offset):#, **kwargs):
        self.custom_shot = custom_shot # type: ShotCustomClass
        self.offset = offset
        self.config['name'] = self.custom_shot.camera_name # type: str
        logger.debug('Camera config: %s' % self.config)
        self.trans, self.shape = pm.camera(**self.config) #type: pm.nodetypes.Transform, pm.nodetypes.Shape
        # strange bug...
        pm.rename(self.trans, self.config['name'])
        pm.rename(self.shape, '%sShape' % self.config['name'])
        self.implane_apply()

    def implane_apply(self):
        logger.info('implane_apply path: %s' % str(self.custom_shot.mov_last))
        if os.path.isfile(str(self.custom_shot.mov_last)): # str() if got None
            implane_tr, implane_sh = pm.imagePlane(camera=self.shape) #type: pm.nodetypes.Transform, pm.nodetypes.imagePlane
            implane_sh.attr('type').set(2)
            implane_sh.fileName(convert_path_relative_var(self.custom_shot.mov_last,
                                                                   os.environ.get('PRVZ_PROJECT_PATH'),
                                                                   verbose=True))

            expr_str = '{}.frameExtension=frame - {}'.format(implane_sh.name(),
                                                             self.offset + self.custom_shot.head_in - 1)
            ex = pm.expression()
            ex.setString(expr_str)
            implane_sh.useFrameExtension.set(1)
            implane_sh.fit.set(0)
            implane_sh.alphaGain.set(0.5)
            implane_sh.depth.set(2)
            implane_sh.displayOnlyIfCurrent.set(1)


            logger.info('added animatic "%s" ' % self.custom_shot.mov_last)
        else:
            logger.info('Cannot find animatic. Path: %s' % self.custom_shot.mov_last)


class ShotCustomClass(object):
    def __init__(self, **sg_dict):
        self.sg_dict = sg_dict
        self.name = sg_dict['sg_name']
        self.episode = sg_dict['Episode']
        # В майском стиле - количество кадров (в премьере это диапазон) [0-1] премьер ==
        self.head_in = sg_dict['sg_head_in']
        self.duration = sg_dict['sg_duration_frames']
        self.camera_name = 'cam_%s_%s' % (self.episode, self.name)
        self.wav_last = sg_dict['wav_last']
        self.mov_last = sg_dict['mov_last']
        logger.debug('Camera name %s' % self.camera_name)

    def __cmp__(self, other):
        return cmp(self.name, other.name)

    def __repr__(self):
        return 'Shot: {sg_name} [head_in: {sg_head_in} Duration :{sg_duration_frames}]'.format(**self.sg_dict)


class SequenceCustom(object):
    #os.environ['PRVZ_PROJECT_PATH'] = '//alpha/envell'
    #path = r'//alpha/envell/2_prod/ep777/ep777_sq001/work/ep777_sq001_layout_v011.ma'
    SCRIPT_NAME = 'mytests'
    SCRIPT_KEY = '9f00d064d9bd22be554cfed822819fc0fca8711980d2f3e4a7da395a733ebaac'
    cdm = sgtk.authentication.CoreDefaultsManager()
    authenticator = ShotgunAuthenticator(cdm)
    user = authenticator.create_script_user(SCRIPT_NAME, SCRIPT_KEY)
    sgtk.set_authenticated_user(user)

    tk = sgtk.sgtk_from_path(os.environ.get('PRVZ_PROJECT_PATH'))
    sg = tk.shotgun


    def __init__(self, path):
        self.ctx = self.tk.context_from_path(path)
        self.custom_shots = [] #type: list[ShotCustomClass]
        self.scene_shots = [] #type: list[pm.nodetypes.Shot]
        self.last_frame = 0
        self.assets_paths = []  #type: list[str]
        self.ignore_asset_types_list = ['ill']
        #CutClips.clips_index = {} #для реюза

    def load_assets(self):
        logger.info('Loading assets')
        for path in self.assets_paths:
            asset_namespace = os.path.basename(path).split('.')[0]
            try:
                path = convert_path_relative_var(path, os.environ['PRVZ_PROJECT_PATH'])
                pm.createReference(path, namespace=asset_namespace, mergeNamespacesOnClash=False)
                logger.info('OK: Referenced: "%s"' % path)

            except Exception as err:
                logger.exception('ERROR: cannot reference "%s": %s' % (path, err))

    def sort_by_head_in(self, custom_sh):
        return int(custom_sh.head_in)

    def stack_shots(self):

        sg_shots_data = self.tk.shotgun.find('Shot', [['sg_sequence', 'is', self.ctx.entity]],
                                             ['sg_head_in', 'sg_duration_frames', 'sg_fps', 'sg_name'])
        # use first shot for fps setting:
        fps = sg_shots_data[0]['sg_fps']
        logger.info('sg_shots_data: %s' % str(sg_shots_data))
        if fps is None:
            error_dialog('Set fps in shotgun at least for first shot')
            return

        pm.currentUnit(time='%sfps' % fps)
        sq_fieleds = self.ctx.as_template_fields(self.tk.templates['maya_sequence_work_area'])
        fields = {'taskname': 'cut', 'version': None, 'Episode': sq_fieleds['CustomEntity01']}

        for shot_dict in sg_shots_data:
            shot_dict['Shot'] = shot_dict['sg_name']
            shot_dict.update(fields)
            cuts_mov = self.tk.paths_from_template(self.tk.templates['shot_editorial_cut'], shot_dict)
            if cuts_mov:
                cuts_mov.sort()
                shot_dict['mov_last'] = cuts_mov[-1]
            else:
                shot_dict['mov_last'] = None
            cuts_wav = self.tk.paths_from_template(self.tk.templates['shot_editorial_sound'], shot_dict)
            if cuts_wav:
                cuts_wav.sort()
                shot_dict['wav_last'] = cuts_wav[-1]
            else:
                shot_dict['wav_last'] = None
            # shot_data =
            self.custom_shots.append(ShotCustomClass(**shot_dict))
        print self.custom_shots
        fist_frame_in_queue = min([sh.head_in for sh in self.custom_shots])
        logger.info('First frame in queue: %s' % fist_frame_in_queue)
        # create shots in sequencer:
        for custom_shot in self.custom_shots:
            self.add_shot_scene(custom_shot, offset=1-fist_frame_in_queue)
        # set playback options:
        pm.playbackOptions(animationStartTime=1, animationEndTime=self.last_frame,
                           minTime=1, maxTime=self.last_frame)
        pm.cmds.currentTime(1)
        # import sound?...
        print('Import sound (not implemented yet)')
        return True

    def add_shot_scene(self, custom_shot, offset=0):
        logger.info('Appending shot %s' % custom_shot)
        shot_scene = pm.shot(startTime=custom_shot.head_in + offset,
                             endTime=custom_shot.head_in + custom_shot.duration - 1 + offset,
                             sequenceStartTime=custom_shot.head_in + offset,
                             sequenceEndTime=custom_shot.head_in + custom_shot.duration - 1 + offset,
                             clip='', shotName=custom_shot.name)

        pm.rename(shot_scene, custom_shot.name) #fix bug
        shot_scene.setAudio(custom_shot.wav_last)
        shot_scene.scale.lock()
        camera = CameraCustom(custom_shot, offset)
        shot_scene.setCurrentCamera(camera.trans)
        self.last_frame = custom_shot.head_in + custom_shot.duration - 1 + offset


    def get_assets_paths(self):
        entity_id = self.ctx.entity['id']
        filters = [['id', 'is', entity_id]]
        fields = ['assets']
        arr_assets = self.sg.find('Sequence', filters=filters, fields=fields)[0]['assets']
        for asset in arr_assets:
            logger.debug('Asset: %s' % str(asset))
            sg_asset = self.sg.find_one('Asset',
                                   [['id', 'is', asset['id']]],
                                   ['code', 'sg_index', 'sg_asset_type', 'sg_type_one', 'sg_type_two'])
            if not sg_asset:
                raise Exception('Unknown Asset %s!', asset)
            reference_template = self.tk.templates["maya_asset_reference"]

            path = reference_template.apply_fields({'Asset': sg_asset['sg_index'],
                                                    'type_one': sg_asset['sg_type_one']['name'],
                                                    'type_two': sg_asset['sg_type_two']['name'],
                                                    'task_process': 'rig'})
            self.assets_paths.append(path)

        for path in self.assets_paths:
            if not os.path.isfile(path):
                message = 'WARNING:\nAsset file was not found:\n %s' % path
                is_go_on = pm.confirmDialog(title='Continue?', message=message, button=['Yes', 'No'],
                                            defaultButton='Yes',
                                            cancelButton='No', dismissString='No')
                if is_go_on == 'No':
                    return False

        return True


def load_assets():
    scene_path = pm.system.sceneName()
    if not scene_path:
        error_dialog('Save the scene: "Shotgun > File Save..."', title='ERROR: Scene is not saved', width=80)
        return
    sq = SequenceCustom(scene_path)
    if sq.get_assets_paths():
        sq.load_assets()

def run():
    try:
        scene_path = pm.system.sceneName()
        if not scene_path:
            error_dialog('Save the scene: "Shotgun > File Save..."', title='ERROR: Scene is not saved', width=80)
            return
        sq = SequenceCustom(scene_path)
        if sq.get_assets_paths() and sq.stack_shots():
                sq.load_assets()
    except Exception as ex:
        logger.exception(ex)

if __name__ == "__main__":
    #p = r'\\alpha\envell\2_prod\ep777\ep777_sq001\cut\ep777_sq001_layout_v001.xml'
    p = r'c:\_temp\xml\ep068_sq001_shots_v001.xml '
    p = r'c:\_temp\xml\test_project.xml'
    p = r'c:\_temp\xml\ep068_clean.xml' #<name>id555_sh1303</name>
    read_xml(p)