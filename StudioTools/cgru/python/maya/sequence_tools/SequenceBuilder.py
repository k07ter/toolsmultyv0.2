#!/usr/bin/python
# -*- coding: utf-8 -*-


import os
from core.python.utils import find_toools
import logging
from core.python.utils_file import convert_path_absolute, convert_path_relative_var
from xml_utils import final_cut_load

if 'PRVZ_TOOLS_PATH' not in os.environ:
    os.environ['PRVZ_TOOLS_PATH'] = find_toools(pattern='tools3')
from core.python.logs import get_logger3
log_storage = os.path.join(os.environ['PRVZ_TOOLS_PATH'], 'core', 'logs', 'sequence_builder')
logger = get_logger3(__name__, log_storage=log_storage, roll_days=2, console=True)
logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)
logger.setLevel(logging.ERROR)

from core.python.maya.utils import confirm_dialog, error_dialog

try:
    import sgtk
    #import context
    import pymel.core as pm
except Exception as ex:
    logger.exception('Import shotgun and Maya modules: %s' % ex)


"""
Locations:
*.xml - .../cut/
sound.wav - .../cut
animatic - .../cut

"""
# from core.shotgun_api3 import Shotgun
# SERVER_PATH = 'https://parovoz.shotgunstudio.com'  # make sure to change this to https if your studio uses it.
# SCRIPT_NAME = 'tools'
# SCRIPT_KEY = '594e7097c7dfdebe9bd83c84d6eb0733eb73666581db2c12f433e0635a6d7099'


camera_config = {"centerOfInterest": 5,
                 "focalLength": 35,
                 "lensSqueezeRatio": 1,
                 "cameraScale": 1,
                 "horizontalFilmAperture": 1.679,
                 "horizontalFilmOffset": 0,
                 "verticalFilmAperture": 0.945,
                 "verticalFilmOffset": 0,
                 "filmFit": 'Fill',
                 "overscan": 1,
                 "motionBlur": 0,
                 "shutterAngle": 144,
                 "nearClipPlane": 0.1,
                 "farClipPlane": 10000,
                 "orthographic": 0,
                 "orthographicWidth": 30,
                 "panZoomEnabled": 0,
                 "horizontalPan": 0,
                 "verticalPan": 0,
                 "position": [0, 10, 0],
                 "zoom": 1
                 }




def read_xml(path, ep):
    CutClips.parse_cut_xml(path)
    print CutClips.clips_index.values()


class CutClips(object):
    clips_index = {}  # {clip_name: CutClip}

    def __init__(self, name):
        self.name = name
        self.cut_in = 0
        self.cut_out = 0
        self.time_base = 0

        CutClips.clips_index[name] = self

    @classmethod
    def get_by_name(cls, name):
        return cls.clips_index[name] if name in cls.clips_index else cls(name)

    @staticmethod
    def parse_cut_xml(xml_name):
        shot_cuts = final_cut_load(xml_name)
        for track in shot_cuts['tracks']:
            for clip in track['clipitems']:
                if clip['enabled']:
                    cut_clip = CutClips.get_by_name(clip['name'])
                    cut_clip.cut_in = clip['in']
                    cut_clip.cut_out = clip['out']
                    cut_clip.time_base = clip['timebase']

    def __repr__(self):
        return 'cut clip <name: %s in: %d out: %d ' % (self.name, self.cut_in, self.cut_out)


class CameraCustom(object):
    def __init__(self, **kwargs):
        self.config = camera_config
        self.config.update(kwargs)
        logger.debug('Camera config: %s' % self.config)
        # cam = pm.camera(**self.config)
        #         # pm.rename(cam)
        #         # self.trans = cam[0]
        self.trans, self.shape = pm.camera(**self.config)
        # strange bug...
        pm.rename(self.trans, self.config['name'])
        pm.rename(self.shape, '%sShape' % self.config['name'])
    def rig(self):
        pass


class Sound(object):
    def __init__(self):
        pass

class ShotCustomClass(object):
    def __init__(self, **sg_dict):
        self.sg_dict = sg_dict
        self.name = sg_dict['code'].split('_')[1]
        # self.fps = None
        self.cut_in = sg_dict['sg_cut_in']
        self.cut_out = sg_dict['sg_cut_out']
        self.camera_name = 'cam_%s' % self.name
        logger.debug('Camera name %s' % self.camera_name)

    def __cmp__(self, other):
        return cmp(self.name, other.name)

    def __repr__(self):
        return 'Shot: {code} [{sg_cut_in} --> {sg_cut_out}]'.format(**self.sg_dict)


class SequenceCustom(object):
    engine = sgtk.platform.current_engine()

    def __init__(self):
        self.custom_shots = []
        self.scene_shots = []
        self.last_frame = 0
        #self.first_frame = 1
        #self.end_frame = 100
        self.assets_paths = []
        self.ignore_asset_types_list = ['ill']

    def load_assets(self):
        logger.info('Loading assets')
        for path in self.assets_paths:
            asset_namespace = os.path.basename(path).split('.')[0]
            try:
                path = convert_path_relative_var(path, os.environ['PRVZ_PROJECT_PATH'])
                pm.createReference(path, namespace=asset_namespace, mergeNamespacesOnClash=False)
                logger.info('OK: Referenced: "%s"' % path)

            except Exception as err:
                logger.exception('ERROR: cannot reference "%s": %s' % (path, err))

    def stack_shots(self, xml_file=None):
        inf_ = 1000000000
        #engine = sgtk.platform.current_engine()
        fist_frame_in_queue = inf_  # infinity
        if xml_file:
            CutClips.parse_cut_xml(xml_file)
            #print CutClips.clips_index.values()
            # Lets convert to sg-like dict:
            # {'sg_cut_in': 12, 'code': 'ep777_sh0101', 'sg_cut_out': 44, 'sg_fps': 25, 'type': 'Shot', 'id': 28216}
            sq_name = self.engine.context.entity['name']
            sq_index = sq_name[-2:]
            sg_shots_data = []
            for clip in CutClips.clips_index.values():
                if clip.name[-4:-2] == sq_index:
                    sg_shot_dict = {}
                    sg_shot_dict['code'] = clip.name
                    #sg_shot_dict['sg_cut_in'] = 1  # default start for maya
                    #sg_shot_dict['sg_cut_out'] = clip.cut_out - clip.cut_in
                    sg_shot_dict['sg_fps'] = clip.time_base
                    # УТОЧНИТЬ - похоже стыковать шоты не нужно. Нужно выставлять их по таймлайй исходя из xml
                    if clip.cut_in < fist_frame_in_queue:
                        fist_frame_in_queue = clip.cut_in
                    sg_shot_dict['sg_cut_in'] = clip.cut_in
                    sg_shot_dict['sg_cut_out'] = clip.cut_out
                    print '----'
                    print sg_shot_dict
                    sg_shots_data.append(sg_shot_dict)
            logger.info('First absolute frame number in queue: %s' % fist_frame_in_queue)
            assert inf_ != fist_frame_in_queue, 'Not found correct delta!'
        else:
            # Depricated? .... xml is the best!
            sg_shots_data = self.engine.shotgun.find('Shot', [['sg_sequence', 'is', self.engine.context.entity]],
                                                        ['code', 'sg_cut_in', 'sg_cut_out', 'sg_fps'])
            # use first shot for fps setting:
        fps = sg_shots_data[0]['sg_fps']
        logger.info(str(sg_shots_data))
        if fps is None:
            error_dialog('Set fps in shotgun at least for first shot')
            return

        pm.currentUnit(time='%sfps' % fps)
        for sg_sh_dict in sg_shots_data:
            print sg_sh_dict
            custom_shot = ShotCustomClass(**sg_sh_dict)
            self.custom_shots.append(custom_shot)
            #logger.info('Added: %s' % Shot(**s))
            print(custom_shot)
        # ... sorting etc
        self.custom_shots.sort()

        # check shots:
        numbers = [int(sh.name[-4:]) for sh in self.custom_shots]
        numbers_lost = range(numbers[0], numbers[-1] + 1)
        for n in numbers:
            numbers_lost.remove(n)
        print numbers_lost

        if numbers_lost:
            message = 'WARNING:\nSome shots are lost:\n %s' % numbers_lost
            is_go_on = pm.confirmDialog(title='Continue?', message=message, button=['Yes', 'No'], defaultButton='Yes',
                             cancelButton='No', dismissString='No')
            if is_go_on == 'No':
                return False

        # create shots in sequencer:
        for custom_shot in self.custom_shots:
            self.add_shot_scene(custom_shot, delta=fist_frame_in_queue)
        # set playback options:
        pm.playbackOptions(animationStartTime=1, animationEndTime=self.last_frame,
                           minTime=1, maxTime=self.last_frame)
        pm.cmds.currentTime(1)
        # import sound?...
        print('Import sound (not implemented yet)')
        return True

    def add_shot_scene(self, custom_shot, delta=0):
        camera = CameraCustom(name=custom_shot.camera_name)
        logger.info('Appending shot %s' % custom_shot)
        shot_scene = pm.shot(startTime=custom_shot.cut_in + 1 - delta,
                             endTime=custom_shot.cut_out - delta,
                             sequenceStartTime=custom_shot.cut_in + 1 - delta,
                             sequenceEndTime=custom_shot.cut_out - delta,
                             currentCamera=camera.trans, clip='', shotName=custom_shot.name)
        pm.rename(shot_scene, custom_shot.name)
        self.last_frame += (custom_shot.cut_out - custom_shot.cut_in)


    def get_assets_paths(self):
        entity_id = self.engine.context.entity['id']
        filters = [['id', 'is', entity_id]]
        fields = ['assets']
        arr_assets = self.engine.shotgun.find('Sequence', filters=filters, fields=fields)[0]['assets']
        for asset in arr_assets:
            logger.debug('Asset: %s' % str(asset))
            sg_asset = self.engine.shotgun.find_one('Asset',
                                   [['id', 'is', asset['id']]],
                                   ['code', 'sg_index', 'sg_asset_type', 'sg_type_one', 'sg_type_two'])
            if not sg_asset:
                raise Exception('Unknown Asset %s!', asset)
            reference_template = self.engine.sgtk.templates["maya_asset_reference"]

            path = reference_template.apply_fields({'Asset': sg_asset['sg_index'],
                                                    'type_one': sg_asset['sg_type_one']['name'],
                                                    'type_two': sg_asset['sg_type_two']['name'],
                                                    'task_process': 'rig'})
            self.assets_paths.append(path)

        for path in self.assets_paths:
            if not os.path.isfile(path):
                message = 'WARNING:\nAsset file was not found:\n %s' % path
                is_go_on = pm.confirmDialog(title='Continue?', message=message, button=['Yes', 'No'],
                                            defaultButton='Yes',
                                            cancelButton='No', dismissString='No')
                if is_go_on == 'No':
                    return False

        return True

def run():
    try:
        scene_path = pm.system.sceneName()
        if not scene_path:
            error_dialog('Save the scene: "Shotgun > File Save..."', title='ERROR: Scene is not saved', width=80)
            return
        # Temp kostil:
        #xml_file = [r'\\alpha\envell\2_prod\ep777\ep777_sq001\cut\ep777_sq001_layout_v001.xml']
        xml_file = pm.fileDialog2(fileFilter="*.xml", dialogStyle=2, fileMode=1)
        if xml_file:
            sq = SequenceCustom()
            if sq.get_assets_paths() and sq.stack_shots(xml_file=xml_file[0]):
                    sq.load_assets()
    except Exception as ex:
        logger.exception(ex)

if __name__ == "__main__":
    #p = r'\\alpha\envell\2_prod\ep777\ep777_sq001\cut\ep777_sq001_layout_v001.xml'
    p = r'\\alpha\envell\2_prod\ep777\ep777_sq001\cut\ep022_sq007_shots_v001.xml'
    read_xml(p, 'ep777')