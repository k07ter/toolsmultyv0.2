#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import os
import logging
from distutils import dir_util, file_util
from tempfile import NamedTemporaryFile
from core.python.utils import find_toools


if 'PRVZ_TOOLS_PATH' not in os.environ:
    os.environ['PRVZ_TOOLS_PATH'] = find_toools(pattern='tools30')
from core.python.logs import get_logger3
log_storage = os.path.join(os.environ['PRVZ_TOOLS_PATH'], 'core', 'logs', 'asset_cloner')
logger = get_logger3(__name__, log_storage=log_storage, roll_days=2)
logger.setLevel(logging.DEBUG)
logger.setLevel(logging.ERROR)

try:
    from core.python.sync.iterators import maya_asci_iterator
    from core.python.utils import npath, timeit
    #import kooo
except Exception as ex:
    logger.exception(ex)
    raise

def printer(msg):
    print (msg)


def change_path (path, src_asset_name, dst_asset_name):
    src_asset_name_pat = '/%s' % src_asset_name
    dst_asset_name_pat = '/%s' % dst_asset_name
    assert (src_asset_name_pat != dst_asset_name_pat), 'change_path() for %s  - No result!' % path
    result = path.replace(src_asset_name_pat, dst_asset_name_pat)
    #print '\t\t%s --> %s' % (path, result)
    return result


@timeit
def clone(src_path, dst_path, printer = printer):
    #print 1/0.0
    src_asset_name = os.path.basename(src_path)
    dst_asset_name = os.path.basename(dst_path)
    src_asset_name_pat = os.sep + src_asset_name
    dst_asset_name_pat = os.sep + dst_asset_name
    print src_asset_name, dst_asset_name
    for root, dirs, files in os.walk(src_path):
        for name in files:
            src_file = os.path.join(root, name)
            dst_file = os.path.join(root.replace(src_asset_name_pat, dst_asset_name_pat, 1),
                                    name.replace(src_asset_name, dst_asset_name, 1))
            printer('Coping: %s -> %s' % (src_file, dst_file))
            if not os.path.isdir(os.path.dirname(dst_file)):
                dir_util.mkpath(os.path.dirname(dst_file))
            file_util.copy_file(src_file, dst_file)
            if dst_file.endswith('.ma'):
                printer ('\t\tFix paths in %s' % dst_file)
                the_file = None
                try:
                    iterator = maya_asci_iterator(dst_file)
                    the_file = NamedTemporaryFile(delete=False, mode='w')
                    #print '>>>>>>> ', the_file.name
                    for line in iterator:
                        if line.startswith('setAttr ".ftn" -type "string"'):
                            the_file.write ('%s\n' % change_path(line, src_asset_name, dst_asset_name))
                        else:
                            the_file.write('%s\n' % line)
                except Exception as ex:
                    printer ('Some problems! %s' % ex)
                finally:
                    if the_file is not None:
                        the_file.close()
                file_util.copy_file(the_file.name, dst_file)
                os.remove(the_file.name)

def test():
    try:
        #printer ('LOG>>> %s' % logger.getChild(__name__))
        asset_src = r'\\alpha\grandadventure\0_assets\chars\minor\yaga'
        asset_dst = r'\\alpha\grandadventure\0_assets\chars\minor\yaga_xxx'
        clone(asset_src, asset_dst)
    except Exception as ex:
        logger.exception(ex)

if __name__ == "__main__":
    test()

