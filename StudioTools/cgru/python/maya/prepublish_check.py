#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov


import os
import sys

import pymel.core as pm

try:
    from PySide import QtGui, QtCore
    from PySide.QtGui import *
    from PySide.QtCore import *
    import shiboken
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets
    from PySide2.QtGui import *
    from PySide2.QtCore import *
    from PySide2.QtWidgets import *
    import shiboken2 as shiboken


import logging
from core.python.checkers.MayaChecker import MayaChecker
from core.python.checkers.MayaCheckersStack import MayaCheckersStack
from core.python.utils import timeit, find_toools
from core.python.maya.utils import save_correct_version
from core.python.EntitySettings import EntitySettings

#new logger:
if 'PRVZ_TOOLS_PATH' not in os.environ:
    os.environ['PRVZ_TOOLS_PATH'] = find_toools()
from core.python.logs import get_logger3
log_storage = os.path.join(os.environ['PRVZ_TOOLS_PATH'], 'core', 'logs', 'prepublish_check')
logger = get_logger3(__name__, log_storage=log_storage, roll_days=2)
#logger.setLevel(logging.DEBUG)
logger.setLevel(logging.ERROR)


class Printer(QTextEdit):
    def __init__(self, parent=None):
        super(Printer, self).__init__(parent)
    def write(self, text):
        self.setTextColor(QColor(200, 200, 200))
        current_color = self.textColor()
        self.setFontPointSize(10)
        if 'Checker: [' in text:
            self.setTextColor(QColor(200, 200, 255))
            self.setFontUnderline(True)
        if 'ERROR' in text or 'FAIL' in text:
            self.setTextColor(QColor('red'))
        if 'WARNING' in text:
            self.setTextColor(QColor(255, 255, 0))
        elif 'OK' in text:
            self.setTextColor(QColor(0, 150, 0))
        if text in ['\n', '\r\n']:
            return
        self.append(text)
        QtCore.QCoreApplication.processEvents() #??? magic
        # Warning! Append() doesn't change the cursor position
        # Because we want to reset the current color **at the end** of
        # the console in order to give the proper color to new messages
        # we must update the cursor position **before** the current
        # color is reset
        self.moveCursor(QTextCursor.EndOfLine)
        self.setFontUnderline(False)
        self.setTextColor(current_color)


class ActionWindow(QDialog):
    def __init__(self, parent=None):
        super(ActionWindow, self).__init__(parent=parent)
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.text_log = Printer()
        self.layout.addWidget(self.text_log)
        self.setWindowTitle('action')
        self.resize(650, 400)
        """
        setWindowModality

        Qt::NonModal          0  The window is not modal and does not block input to other windows.
        Qt::WindowModal       1  The window is modal to a single window hierarchy and blocks input to its parent window,
                                all grandparent windows, and all siblings of its parent and grandparent windows.
        Qt::ApplicationModal  2  The window is modal to the application and blocks input to all windows.
        """


class CheckerWidget(QWidget):
    def __init__(self, parent=None, checker=None):
        super(CheckerWidget, self).__init__(parent=parent)
        assert isinstance(checker, MayaChecker)
        self.checker = checker
        self.action_win = ActionWindow(parent=CheckerWidget.get_maya_win())

        self.grid_layout = QGridLayout()
        self.grid_layout.setSpacing(5)
        self.setLayout(self.grid_layout)

        self.ch_button = QPushButton()
        try:
            ref_check = self.checker.args['include_refs']
            if ref_check:
                ref_check = '  [+ refs]'
            else:
                ref_check = ''
        except Exception:
            ref_check = '  [ ? refs]'
        if not self.checker.pretty_name:
            self.ch_button.setText(self.checker.name + ref_check)
        else:
            self.ch_button.setText(self.checker.pretty_name + ref_check)
        # --- checker button
        self.ch_button.pressed.connect(self.ch_button_pressed)

        # --- checker action button
        self.ch_button_action = QPushButton()
        self.ch_button_action.setText('Try to fix')

        self.ch_button_action.setEnabled(self.checker.args['action_on'])
        self.ch_button_action.pressed.connect(self.ch_button_action_pressed)

        # --- checker info button
        self.ch_button_info = QPushButton()
        self.ch_button_info.setText('?')
        self.ch_button_info.pressed.connect(self.ch_button_info_pressed)

        self.ch_button.setMinimumWidth(250)
        self.ch_button_action.setMinimumWidth(100)
        self.ch_button_info.setMinimumWidth(30)
        self.ch_button_info.setMaximumWidth(30)
        self.grid_layout.addWidget(self.ch_button, 0, 0, 0, 15)
        self.grid_layout.addWidget(self.ch_button_action, 0, 16, 0, 10)
        self.grid_layout.addWidget(self.ch_button_info, 0, 26, 0, 3)

    def refresh_button(self):
        passed = self.checker.get_state()
        if passed:
            self.ch_button.setStyleSheet("background-color:rgb(0,70,0)")
            self.ch_button.repaint()
            # tun off correction
            self.ch_button_action.setEnabled(False)
        else:
            self.ch_button.setStyleSheet("background-color:rgb(150,0,0)")
            self.ch_button.repaint()
            self.ch_button_action.setEnabled(self.checker.args['action_on'])


    @QtCore.Slot()
    def ch_button_action_pressed(self):
        self.action_win.text_log.clear()
        # output to win
        mem_stdout = sys.stdout
        sys.stdout = self.action_win.text_log
        self.checker.action()
        print self.checker.info
        self.refresh_button()
        sys.stdout = mem_stdout
        if not self.checker.get_state():
            self.action_win.show()

    @QtCore.Slot()
    def ch_button_pressed(self):
        self.checker.test()
        self.action_win.text_log.clear()
        # output to win
        mem_stdout = sys.stdout
        sys.stdout = self.action_win.text_log
        if self.checker.errors:
            print '------------- Errors found  ---------------'
            print self.checker.errors
            print self.checker.info
        else:
            print '------------- Nothing to fix ---------------'
            print  self.checker.info

        # --- colorize button
        print 'checking finished ...'
        self.refresh_button()
        sys.stdout = mem_stdout

        if not self.checker.get_state():
            self.action_win.show()


    @QtCore.Slot()
    def ch_button_info_pressed(self):
        try:
            link = self.checker.args['help_link']
            pm.launch(web=link)
        except Exception as ex:
            print 'Cannot launch help link: %s' % ex

    @staticmethod
    def get_maya_win():
        import maya.OpenMayaUI as omui
        #import shiboken
        return shiboken.wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)


class CheckersStackWindow(QDialog):
    def __init__(self, parent=None, ch_stack=None, call_back=None):
        assert isinstance(ch_stack, MayaCheckersStack)
        self.ch_stack = ch_stack
        self.call_back = call_back
        self.state = False
        super(CheckersStackWindow, self).__init__(parent)
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.checker_widgets = []

        self.setWindowTitle('Scene checkers')
        self.setFixedHeight(600)

        # Container Widget
        cont_widget = QWidget()
        # Layout of Container Widget
        cont_layout = QVBoxLayout(self)
        self.ch_stack.checkers.sort(key=checker_key)
        for checker in self.ch_stack.checkers:
            checker_widget = CheckerWidget(parent=self, checker=checker)
            cont_layout.addWidget(checker_widget)
            self.checker_widgets.append(checker_widget)

        cont_widget.setLayout(cont_layout)
        # Scroll Area Properties
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(False)
        scroll.setWidget(cont_widget)
        self.layout.addWidget(scroll)

        self.btn_check = QPushButton()
        self.btn_check.setText('Check all again and publish')

        self.layout.addWidget(self.btn_check)
        self.btn_check.pressed.connect(self.test_all)

    def test_all(self):
        #reset colors:
        for checker_widget in self.checker_widgets:
            checker_widget.ch_button.setStyleSheet("background-color:rgb(0,0,0)")
            checker_widget.ch_button.repaint()

        pub_flag = True
        for checker_widget in self.checker_widgets:
            passed = checker_widget.checker.test()
            checker_widget.refresh_button()
            if not passed:
                pub_flag = False

        if pub_flag:
            children = self.findChildren(ActionWindow)
            for ch in children:
                ch.close()
            self.close()
            if self.call_back is not None:
                self.call_back()


def checker_key(checker):
    return checker.pretty_name

@timeit
def start_win(call_back=None, debug=False):
    # Here is costil
    try:
        import core.silver.pre_check as slvr
        slvr.clean_scene()
    except Exception as err:
        print ('Errors in Silver %s ' % err)
    # window:
    try:
        import maya.OpenMayaUI as omui
        def mayaMainWindow():
            return shiboken.wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)

        path = os.path.normcase(pm.system.sceneName())
        entsett = EntitySettings(path, os.environ['PRVZ_TOOLS_PATH'])
        if not entsett.data:
            logger.error('Unknown object!')
            raise Exception('UFO! terminated!')
            return
        if not debug:
            saved = save_correct_version(entsett=entsett)
            if saved is None:
                return
        checkers_stack = MayaCheckersStack(entsett)
        logger.debug('STACK:' + str(checkers_stack))
        dlg = CheckersStackWindow(parent=mayaMainWindow(), ch_stack=checkers_stack, call_back=call_back)
        dlg.show()
        dlg.test_all()

    except Exception as ex:
        print (ex)
        logger.exception(ex)

