import maya.cmds as mc
import pymel.core as pm
import os
from pprint import pprint

"""
--== Camera rig creation ==-- v 1.0
Autor: Aleksander Tarkhov for Parovoz Studio

key module: start()
This script is intended to create reference 'camera_supertechno' rig in scenes. Also it's create camera that name's based on scene's name and constraint the camera to the rig.   
"""

settings = [
    {
        'translate': (-33.0, 8.0, 0.0),
        'rotate': (0.0, 90.0, 0.0),
        'scale': (1.0, 1.0, 1.0)
    },
    {
        'fl': 32,
        'horizontalFilmAperture': 2.2582632,
        'verticalFilmAperture': 0.94488,
        'cameraScale': 1.0,
        'nearClipPlane': 1.0,
        'filmFit': 'Fill',
        'renderable': 1,
        'displayFilmGate': 1,
        'overscan': 1.3,
        'centerOfInterest': 72.2894987344,
        'tumblePivotX': -35.1231997557,
        'tumblePivotY': 8.0
    }
]


def lastVersion(path='', fullPath=0):
    if len(path) is 0:
        mc.error("--== You have to input correct path ==--")
    fileList = mc.getFileList(fld=path)
    fileNum = len(fileList)
    if fileNum is 0:
        return None
    versions = [int(f[-6:-3]) for f in fileList]
    i = versions.index(max(versions))
    if fullPath is 0:
        return fileList[i]
    if fullPath is 1:
        return path + fileList[i]


def start():
    window = pm.window(title="Choose a camera", widthHeight=(240, 55), mxb = 0, s = 0)
    pm.columnLayout(adj = 1, rs = 5)
    pm.button(label='Camera', command=lambda *args: camera(window))
    pm.button(label='Camera and supertechno', command=lambda *args: supertechno(window))
    pm.showWindow()


def get_camera_name():
    sceneName = mc.file(q=1, sn=1)
    if '//omega/koshchey' not in sceneName:
        mc.confirmDialog(m="Looks like your scene path is incorrect!", b="Me bad :(")
        mc.error("--== You have to make your scene path correct! ==--")

    filename = sceneName.split('/')[len(sceneName.split('/')) - 1]
    sq = filename.split('_')[0]
    sh = filename.split('_')[1]

    cameraName = 'cam_%s_%s' % (sq, sh)

    if mc.objExists(cameraName):
        mc.confirmDialog(m="'%s' is already in the scene!" % (cameraName), b="Oooops!")
        mc.error("--== Just relax and go on! ==--")

    return cameraName


def camera(window):
    cameraName = get_camera_name()
    cam = pm.camera()
    cam[0].rename(cameraName)

    for i in range(len(settings)):
        for s in settings[i]:
            cam[i].attr('%s' % s).unlock()
            cam[i].attr('%s' % s).set(settings[i][s])
    pm.deleteUI(window)


def supertechno(window):
    camRigPath = "//omega/koshchey/0_assets/props/prop_camera_supertechno/rig/publish/maya/"
    camFiles = mc.getFileList(fld=camRigPath)
    camRigName = "persp_rig"
    baseNsp = 'prop_camera_supertechno'

    refInScene = mc.ls(type="reference")
    cameraName = get_camera_name()

    camRig = ''

    if not os.path.exists(camRigPath):
        mc.confirmDialog(m="Path does not exist:  %s" % (camRigPath), b="OK")
        mc.error("Path does not exist: "+camRigPath)

    for one in refInScene:
        if baseNsp in one:
            nsp = mc.referenceQuery(one, ns=1)
            camRig = '%s:%s' % (nsp, camRigName)
    if len(camRig) is 0:
        mc.file(lastVersion(path=camRigPath, fullPath=1), r=1, ns=baseNsp)
        camRig = '%s:%s' % (baseNsp, camRigName)

    dupList = mc.duplicate(camRig, n=cameraName, ic=0, po=0, rc=1)
    mc.delete(dupList[1:])
    mc.setAttr('%sShape.v' % (camRig), 0)
    sdk = mc.listConnections('%sShape.focalLength' % (camRig))[0]
    dupSdk = mc.duplicate(sdk, n=cameraName, ic=0, po=0)[0]

    mc.setAttr('%sShape.focalLength' % (cameraName), l=0)
    [mc.setAttr('%s.%s%s' % (cameraName, ch, axis), l=0) for ch in 'trs' for axis in 'xyz']
    mc.connectAttr('%s.focalLength' % (cameraName), '%s.input' % (dupSdk))
    mc.connectAttr('%s.output' % (dupSdk), '%sShape.focalLength' % (cameraName))
    mc.setAttr('%sShape.focalLength' % (cameraName), l=1)
    mc.parent(cameraName, w=1)
    mc.parentConstraint(camRig, cameraName)
    [mc.setAttr('%s.%s%s' % (cameraName, ch, axis), l=1) for ch in 'trs' for axis in 'xyz']

    pm.deleteUI(window)
