# encoding: utf8
import sys
import os
import maya.cmds as cmds
import maya.utils as ut
import pymel.core as pm

from pprint import pprint

import sgtk, tank
from tank import TankError

engine = sgtk.platform.current_engine()
tk = engine.sgtk
sg = tk.shotgun


def set_layout():
    entity = engine.context.entity
    set_cut(False)

    f_assets = sg.find("Shot", [['id', 'is', entity['id']]], ['assets'])
    for assets in f_assets[0]['assets']:
        f_asset_type = sg.find("Asset", [['id', 'is', assets['id']]], ['sg_asset_type'])
        ver_num = get_version(engine, sg, assets['id'])
        path = get_path(assets['name'], tk, ver_num, f_asset_type[0]['sg_asset_type'])
        # set_reference(path[0], assets['name'], path[1])
        ut.executeInMainThreadWithResult(set_reference, path[0], assets['name'], path[1])

        # path = get_path_two(engine, sg, assets['id'])
        # set_reference(path, assets['name'])
        pprint(path[0])


def set_cut(cut_check):
    entity = engine.context.entity

    min = cmds.playbackOptions(query = True, min = True)
    max = cmds.playbackOptions(query = True, max = True)

    f_shot = sg.find(entity['type'], [['id', 'is', entity['id']]], ['sg_cut_in', 'sg_cut_out', 'sg_cut_order'])
    cut_in = f_shot[0]['sg_cut_in']
    cut_out = f_shot[0]['sg_cut_out']
    if not cut_in or not cut_out:
        cmds.confirmDialog(t='Warning!', m="Cut In and Cut Out are not sen in SG",
                           icon='Critical', button=['Ok'])
    if cut_check:
        if(min!=cut_in or max!=cut_out):
            conf = cmds.confirmDialog(m="Max and min frame range does not match. Change?", b=["Yes", "No"], cancelButton = "No")
            if(conf=="Yes"):
                print("OOOO DA")
                cmds.playbackOptions(ast=cut_in, aet=cut_out, min=cut_in, max=cut_out)
                cmds.currentTime(cut_in)
    else:
        cmds.playbackOptions(ast=cut_in, aet=cut_out, min=cut_in, max=cut_out)
        cmds.currentTime(cut_in)


def set_reference(path, name, assembly):
    if not assembly:
        cmds.file(path, r=1, ns=name)
    else:
        ar = pm.assembly(name=name, type='assemblyReference')
        ar.definition.set(path)

def load_asset(name, version = None):
    #\\parovoz-frm01\tank\koshchey_clone\config\hooks\tk-maya\tk-multi-loader2\jz_tk-maya_actions.py
    step = {
        'ch': 'rig',
        'prop': 'rig',
        'obj': 'assembly',
        'loc': 'assembly'
    }

    if name:
        type = step[name.split('_')[0]]
        if type != 'assembly':
            assembly = False
            template = tk.templates["maya_asset_publish"]
        else:
            assembly = True
            template = tk.templates["maya_asset_ad"]

        fields = {
            'Asset': name,
            'Step': type,
            'extension': 'ma',
            'version': version
        }
        if version != None:
            fields['version'] = version

        path = tk.paths_from_template(template, fields)

        if path:
            path = path[0].replace('\\', '/')
            prvz_project_path = os.getenv('PRVZ_PROJECT_PATH')
            path = path.replace(prvz_project_path, '%root%')
            set_reference(path, name, assembly)

def get_path(name, tk, ver_number, asset_type):
    a_type = {
        'Character': 'chars',
        'Prop': 'props',
        'Object': 'objs',
        'Location': 'locs'
    }

    step = {
        'Character': 'rig',
        'Prop': 'rig',
        'Object': 'assembly',
        'Location': 'assembly'
    }

    task_name = {
        'Character': 'rig',
        'Prop': 'rig',
        'Object': 'ad',
        'Location': 'ad'
    }

    if step[asset_type] != 'assembly':
        assembly = False
        template_obj = tk.templates["maya_asset_publish"]
    else:
        assembly = True
        template_obj = tk.templates["maya_asset_ad"]

    fields = {
        'Asset': name,
        'Step': step[asset_type],
        'extension': 'ma',
        'sg_asset_type': a_type[asset_type],
        'taskname': task_name[asset_type],
        'version': ver_number
    }

    path = template_obj.apply_fields(fields)
    path = path.replace('\\', '/')
    prvz_project_path = os.getenv('PRVZ_PROJECT_PATH')
    path = path.replace(prvz_project_path, '%root%')

    return path, assembly

    # path = '//omega/koshchey/0_assets/chars/ch_koshchey/rig/publish/maya/ch_koshchey_rig_v139.ma'
    # ff = template_obj.get_fields(path)
    # pprint(ff)


def get_version(engine, sg, assets_id):
    project = engine.context.project
    filters = [
        ['project', 'is', {'type': 'Project', 'id': project['id']}],
        ['entity.Asset.id', 'is', assets_id]
    ]
    fields = ['version_number']
    v = sg.find("PublishedFile", filters, fields, [{'field_name': 'version_number', 'direction': 'desc'}], "all", 1)

    ver_number = v[0]['version_number']
    return ver_number


def get_path_two(engine, sg, assets_id):
    project = engine.context.project
    filters = [
        ['project', 'is', {'type': 'Project', 'id': project['id']}],
        ['entity.Asset.id', 'is', assets_id]
    ]
    fields = ['path']
    v = sg.find("PublishedFile", filters, fields, [{'field_name': 'version_number', 'direction': 'desc'}], "all", 1)

    prvz_project_path = os.getenv('PRVZ_PROJECT_PATH')

    path = v[0]['path']['local_path']
    path = path.replace('\\', '/')
    path = path.replace(prvz_project_path, '%root%')

    return path
