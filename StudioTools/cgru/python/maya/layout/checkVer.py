# encoding: utf8


import sgtk


engine = sgtk.platform.current_engine()
breakdown_app = engine.apps["tk-multi-breakdown"]


def check():
    items = breakdown_app.analyze_scene()

    invalid_versions = []

    for item in items:
        ver = item["fields"]["version"]
        last_ver = breakdown_app.compute_highest_version(item["template"], item["fields"])
        if ver < last_ver:
            invalid_versions.append(ver)

    if invalid_versions:
        breakdown_app.show_breakdown_dialog()
