from rv import rvtypes, commands, extra_commands, runtime, rvui
import base64, json

# deserialize global variable "arguments" that comes from calling application
arguments = json.loads(base64.b64decode(arguments))


def postLoad():
    if arguments.get("activate") != False:
        # bring main window to front
        import os, subprocess
        if os.name == 'posix':
            ## @note use this solution as KDE and GNOME prevent focus stealing by default
            t = runtime.eval('mainWindowWidget().windowTitle();',
                             ["qt", "commands"])
            t = t.strip(
                '\"')  # some why returned string is surrounded by double quotes
            cmd = ["wmctrl", "-a", t]
            if subprocess.call(cmd) != 0:
                print "Failed to activate RV with \"%s\" command" % ' '.join(
                    cmd)
        else:
            ## @todo window activation sometimes fails on Windows
            runtime.eval('mainWindowWidget().activateWindow();',
                         ["qt", "commands"])

    # actualize Default Sequence
    ## @note it is pain to write something for RV, so we use many magic here
    if "media" in arguments:
        addedSources = commands.sources()[-len(arguments["media"]):]
        length = sum([x[2] - x[1] for x in addedSources])

        commands.setInPoint(commands.frameEnd() - length)
        commands.setOutPoint(commands.frameEnd())
        commands.setFrame(commands.inPoint())

    # determine if user yet switched to particular media
    if commands.viewNode().startswith('sourceGroup') and (
            "media" in arguments and len(arguments["media"]) == 1):
        commands.setViewNode(commands.viewNodes()[-1])
        if "autoplay" not in arguments: arguments["autoplay"] = True

    if commands.viewNode() == 'defaultSequence' and "autoplay" not in arguments:
        arguments["autoplay"] = True

    if arguments.get("autoplay") == True:
        commands.play()


# add media if provided
if "media" in arguments:
    # is current session initial?
    ## @todo make more correct initial session detection
    if len(commands.viewNodes()) == 3:
        if "autoplay" not in arguments:
            arguments["autoplay"] = True
        if "activate" not in arguments:
            arguments["activate"] = False

    media = []
    for m in arguments["media"]:
        if type(m) is list:
            # process layers for media
            media.append("[")
            for mm in m:
                media.append(mm)
            media.append("]")
        else:
            media.append(m)

    # we need addSources that works asynchronously, to avoid problems where color nodes do not exists yet
    commands.addSources(media)

    # wait for media to be loaded
    def postLoadHandler(event):
        postLoad()
        commands.unbind("default", "global", "after-progressive-loading")

    commands.bind("default", "global", "after-progressive-loading",
                  postLoadHandler, "")
else:
    # if this file is executing only to process arguments
    postLoad()
