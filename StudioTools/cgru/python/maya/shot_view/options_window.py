# -*- coding: utf-8 -*-
try:
    from PySide.QtGui import *
    from PySide.QtCore import *
    import shiboken
except ImportError:
    from PySide2.QtGui import *
    from PySide2.QtCore import *
    from PySide2.QtWidgets import *
    import shiboken2 as shiboken

from settings import RESOLUTIONS


class OptionsWindow(QDialog):
    """
    Класс окна настроек вьюпорта
    """

    def __init__(self, parent=None):
        super(OptionsWindow, self).__init__()
        self.parent = parent
        self.setWindowTitle('Options')
        self.setFixedSize(250, 250)
        self.setModal(True)


        _resolutions = [str(res[0]) + 'x' + str(res[1]) for res in RESOLUTIONS]

        self.resolution_combobox_w = QComboBox()
        self.resolution_combobox_w.addItems(_resolutions)
        self.safe_frame_checkbox_w = QCheckBox()
        self.center_frame_checkbox_w = QCheckBox()
        self.wireframe_on_shaded_w = QCheckBox()
        self.grab_settings_button_w = QPushButton(
            "Capture Current Viewport")
        self.default_settings_button_w = QPushButton(
            "Reset to Default Settings")

        self.work_settings_form = QFormLayout()
        self.work_settings_form.addRow('Show Safe Frame',
                                       self.safe_frame_checkbox_w)
        self.work_settings_form.addRow('Show Frame Center',
                                       self.center_frame_checkbox_w)
        self.work_settings_form.addRow('Wireframe On Shaded',
                                       self.wireframe_on_shaded_w)

        self.work_settings_layout = QVBoxLayout()
        self.work_settings_layout.addLayout(self.work_settings_form)
        self.work_settings_layout.addWidget(self.grab_settings_button_w)
        self.work_settings_layout.addWidget(self.default_settings_button_w)

        self.resolution_combobox_p = QComboBox()
        self.resolution_combobox_p.addItems(_resolutions)
        self.safe_frame_checkbox_p = QCheckBox()
        self.center_frame_checkbox_p = QCheckBox()
        self.wireframe_on_shaded_p = QCheckBox()
        self.grab_settings_button_p = QPushButton(
            "Capture Current Viewport")
        self.default_settings_button_p = QPushButton(
            "Reset to Default Settings")

        self.playblast_settings_form = QFormLayout()
        self.playblast_settings_form.addRow('Resolution',
                                            self.resolution_combobox_p)
        self.playblast_settings_form.addRow('Show Safe Frame',
                                            self.safe_frame_checkbox_p)
        self.playblast_settings_form.addRow('Show Frame Center',
                                            self.center_frame_checkbox_p)
        self.playblast_settings_form.addRow('Wireframe On Shaded',
                                            self.wireframe_on_shaded_p)

        self.playblast_settings_layout = QVBoxLayout()
        self.playblast_settings_layout.addLayout(self.playblast_settings_form)
        self.playblast_settings_layout.addWidget(self.grab_settings_button_p)
        self.playblast_settings_layout.addWidget(
            self.default_settings_button_p)

        self.work_settings_frame = QFrame()
        self.work_settings_frame.setLayout(self.work_settings_layout)

        self.playblast_settings_frame = QFrame()
        self.playblast_settings_frame.setLayout(self.playblast_settings_layout)

        self.tabs = QTabWidget()
        self.tabs.addTab(self.work_settings_frame, 'Work')
        self.tabs.addTab(self.playblast_settings_frame, 'Playblast')

        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok |
                                                 QDialogButtonBox.Cancel)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.main_layout = QVBoxLayout()
        self.main_layout.addWidget(self.tabs)
        self.main_layout.addStretch()
        self.main_layout.addWidget(self.button_box)
        self.setLayout(self.main_layout)