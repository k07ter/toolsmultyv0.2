import glob
import os
import shutil
import sys
import datetime
import tempfile
import logging
import getpass
import subprocess
import traceback

import win32com.client
from win32com.client import constants

stuff_root = "//a-vfx/stuff"
if not os.path.exists(stuff_root):
    stuff_root = 'c:/stuff'

python_libs = stuff_root + '/tools/lib/python/vendor/shotgun'
if sys.path[0] != python_libs:
    sys.path.insert(0, python_libs)

python_libs = stuff_root + '/tools/lib/python'
if python_libs not in sys.path:
    sys.path.append(python_libs)

from shotgun_api3 import Shotgun

SERVER_PATH = "https://a-vfx.shotgunstudio.com"
SCRIPT_NAME = 'get_shotgun_data'
SCRIPT_KEY = '27093f70443c89ed5cc51fd3ab2e93a03efab98f4a42c591d8eb0f2242036f80'

import sgtk
engine = sgtk.platform.current_engine()
sg = engine.shotgun
ctx = engine.context
entity = ctx.entity

#sg = Shotgun(SERVER_PATH, SCRIPT_NAME, SCRIPT_KEY)

shot = sg.find_one("Shot", [["id", "is", entity["id"]]],
                   ["sg_cut_duration", "sg_timecode_2"])
shot_timecode = shot["sg_timecode_2"]

clip_list = cmds.ls(type="audio")
clips = []
start_time = 99999999
end_time = 0

for c in clip_list:
    if cmds.getAttr(c + ".mute"): continue
    of = cmds.getAttr(c + ".offset")
    fn = cmds.getAttr(c + ".filename")
    ss = cmds.getAttr(c + ".sourceStart")
    se = cmds.getAttr(c + ".sourceEnd")
    dr = cmds.getAttr(c + ".duration")

    if of < 0:
        ss -= of
        se -= of
        of = 0
    ss = min(ss, dr)
    se = min(se, dr, shot_timecode)
    if ss >= se: continue
    if of >= shot_timecode: continue

    clips.append({
        "name": c,
        "offset": of,
        "filename": fn,
        "start": ss,
        "end": se
    })
    start_time = min(start_time, of)
    end_time = max(end_time, of + se - ss)

end_time = min(end_time, shot_timecode)
duration_time = end_time - start_time

audio_input = ""
audio_filter = [
    "-filter_complex \"", "", "", "amix=inputs=" + str(len(clips)) +
    ",atrim=duration=" + str(duration_time / 1000) + "\""
]
for i, c in enumerate(clips, 0):
    audio_input += "-i \"" + c["filename"] + "\" "
    if c["offset"] > 0 or c["start"] > 0:
        filters = []
        if c["offset"] > 0:
            filters.append("adelay=%s|%s" % (c["offset"], c["offset"]))

        if c["start"] > 0:
            filters.append("atrim=%s:%s" %
                           (c["start"] / 1000, c["end"] / 1000))
        filters = ",".join(filters)
        audio_filter[1] += "[%d]" % i + filters + "[audio%d];" % i
        audio_filter[2] += "[audio%d]" % i

audio_settings = "".join(audio_filter)
ffmpeg_cmd = "ffmpg " + audio_input + " " + "".join(audio_filter)


def DO(Episode="",
       ProjectScene="",
       shot_id=None,
       project_id=None,
       log_file="",
       exit=True):
    if not os.path.exists(os.path.dirname(log_file)):
        os.makedirs(os.path.dirname(log_file))

    f = open(log_file, 'a')
    sys.stdout = f
    sys.stderr = f

    print "--- auto_dailies.py ---"
    print "Episode: " + Episode + " ProjectScene: " + ProjectScene + " project_id: " + str(
        int(project_id))

    project = os.environ.get('project')

    if not project:
        print "Missing Project Enviroment."
        print 'INHOUSE_ERROR'
        return -1

    #Application.ExecuteScript(stuff_root.replace("/","\\")+"\\tools\\XSI\\farmscripts\\xsi_publish_v.py", "Python", "DO", (Episode, ProjectScene, shot_id, False))

    plus_one_version = True
    return_code = Application.ExecuteScript(
        stuff_root.replace("/", "\\") +
        "\\tools\\lib\\python\\inhouse\\xsi\\xsi_publish_v.py", "Python", "DO",
        (Episode, ProjectScene, shot_id, False))
    print 'return_code:', return_code

    if return_code[0] == -1:
        XSIUIToolkit.MsgBox('publish error')
        return -1

    if return_code[0] == 119616:
        plus_one_version = False

    scene_file_path = Application.ActiveProject3.ActiveScene.Parameters(
        "Filename").Value
    scene_file_root = os.path.dirname(scene_file_path)

    if plus_one_version:
        glob_path = scene_file_root + "\\" + ProjectScene + "_v???.scn"
    else:
        glob_path = scene_file_root + "\\work\\" + ProjectScene + "_v???.scn"
    scn_files = glob.glob(glob_path)
    if not scn_files:
        print "Coudn't find scenes by mask: %s" % glob_path
        print 'INHOUSE_ERROR'
        return -1
    scn_files.sort()

    if plus_one_version:
        scene_name = os.path.basename(scn_files[-2])[:-4]
    else:
        scene_name = os.path.basename(scn_files[-1])[:-4]

    render_camera = None

    if Application.GetValue(
            "PlayControl.Format") != 7 and Application.GetValue(
                "PlayControl.Rate") != 24:
        print "Current FPS is %s. Set 24." % str(
            Application.GetValue("PlayControl.Rate"))
        Application.SetValue("PlayControl.Format", 7)

    if Application.ActiveProject3.Name not in scene_name:
        print "Mismatch of Project and Scene names:\n\nProject: %s\nScene: %s" % (
            Application.ActiveProject3.Name, scene_name)
        print 'INHOUSE_ERROR'
        return -1

    sg = Shotgun(SERVER_PATH, SCRIPT_NAME, SCRIPT_KEY)

    project_name = Application.ActiveProject3.Name
    shot_name = project_name

    dd, mm, yy = datetime.datetime.now().strftime('%d %m %y').split()
    dailies_path = project + "\\dailies\\" + yy + mm + dd

    version_path = os.path.join(Application.ActiveProject3.FullName,
                                "Versions", scene_name)
    publish_path = os.path.join(Application.ActiveProject3.FullName,
                                "Scenes/publish")
    #camera_path = os.path.join(Application.ActiveProject3.FullName,"InOut/camera")
    audio_path = os.path.join(Application.ActiveProject3.FullName, "Audio")

    server_image_folder = dailies_path + '\\' + shot_name

    temp_image_folder = tempfile.mkdtemp()

    image_name = scene_name

    start_frame = int(round(Application.GetValue("PlayControl.In")))
    end_frame = int(round(Application.GetValue("PlayControl.Out")))

    start_time = Application.GetValue("PlayControl.In") / Application.GetValue(
        "PlayControl.Rate")

    logging.basicConfig(
        format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    logger.debug("scene_file_path: %s" % scene_file_path)
    logger.debug("dailies_path: %s" % dailies_path)
    logger.debug("project: %s, %s" % (project, os.path.basename(project)))
    logger.debug("temp_image_folder: %s" % temp_image_folder)

    logger.debug("shot_name: %s" % shot_name)
    logger.debug("version_path: %s" % version_path)
    logger.debug("publish_path: %s" % publish_path)
    #logger.debug("camera_path: %s" % camera_path)
    logger.debug("server_image_folder: %s" % server_image_folder)
    logger.debug("image_name: %s" % image_name)
    logger.debug("start_frame: %s" % start_frame)
    logger.debug("start_time: %s" % start_time)

    # try:
    #     logger.debug("Update Shot sg_publish: rtupd")
    #     sg.update("Shot", int(shot_id), data={'sg_publish':'rtupd'})
    # except Exception, e:
    #     print "Error:", str(e)
    #     print "Trace:", traceback.format_exc()

    #     print 'INHOUSE_ERROR'
    #     return -1

    audio_clips = []
    assets = []

    models = Application.FindObjects2(constants.siModelID)
    for model in models:
        if model.HasMixer():
            for track in model.Mixer.Tracks:
                if track.Type == constants.siTrackAudioType:
                    if track.Parameters('Active').Value:
                        if track.Clips.Count:
                            for clip in track.Clips:
                                if clip.Parameters('Active').Value:
                                    audio_file = clip.Source.Parameters(
                                        'FileName').Value

                                    if audio_file.startswith('$(project)'):
                                        audio_file = audio_file.replace(
                                            '$(project)', project)
                                    elif audio_file.startswith('Audio'):
                                        audio_file = audio_file.replace(
                                            'Audio', audio_path, 1)

                                    if os.path.exists(audio_file):
                                        delay = int(
                                            (clip.Parameters('ContainerStart')
                                             .Value - start_time) * 1000)
                                        if delay >= 0:
                                            audio_clips.append(
                                                (audio_file, delay))
                                        else:
                                            logger.error(
                                                "Audio %s has negative delay: skipped (%d)"
                                                % (audio_file, delay))
                                    else:
                                        logger.error(
                                            "Couldn't find the audio: %s" %
                                            audio_file)

    filters = [
        ['id', 'is', int(project_id)],
    ]
    fields = ['name']
    sg_project = sg.find_one('Project', filters, fields)
    logger.debug(sg_project)

    if not sg_project:
        logger.error("Couldn't find project with id: %d" % int(project_id))
        print 'INHOUSE_ERROR'
        return -1

    # Render

    models = Application.FindObjects2(constants.siModelID)

    pre_view_values = []

    for model in models:
        name = model.FullName
        if name.startswith("char_") and Application.Dictionary.GetObject(
                name + ".CTRLs_face", False):
            pre_view_values.append(
                (name, Application.GetValue(name + ".CTRLs_face.viewvis")))
            Application.SetValue(name + ".CTRLs_face.viewvis", 0)

    render_camera = "camA.camera_center"
    render_viewport = False
    #render_viewport = True

    if render_viewport:
        viewport_capture_parameters = {
            "FormatType": None,
            "DSCodec": None,
            "Filename": None,
            "Padding": None,
            "IsMovie": None,
            "ImageWidth": None,
            "ImageHeight": None,
            "ImageScale": None,
            "UserPixelRatio": None,
            "FrameRate": None,
            "WriteAlpha": None,
            "Start": None,
            "End": None,
            "CustomTimeRange": None,
            "CaptureAudio": None,
            "ReturnToInitialFrame": None,
            "LaunchFlipbook": None,
            "UseNativePlayer": None,
            "OpenGLAntiAliasing": None
        }
        for parameter in viewport_capture_parameters:
            viewport_capture_parameters[parameter] = Application.GetValue(
                "ViewportCapture." + parameter)

        camera_parameters = {
            "camvis.camerainfo": None,
            "camvis.constructionlevel": None,
            "camvis.gridaxisvis": None
        }
        for parameter in camera_parameters:
            camera_parameters[parameter] = Application.GetValue(
                render_camera + "." + parameter)

        view_cube_show = Application.GetValue("preferences.ViewCube.show")

        Application.SetValue("ViewportCapture.FormatType", 12)
        Application.SetValue("ViewportCapture.DSCodec", "AQABAAAAIAAAAFA=")
        Application.SetValue("ViewportCapture.Filename",
                             os.path.join(temp_image_folder,
                                          image_name + "..png"))
        Application.SetValue("ViewportCapture.Padding", "(fn)####(ext)")
        Application.SetValue("ViewportCapture.IsMovie", False)
        Application.SetValue("ViewportCapture.ImageWidth", 1280)
        Application.SetValue("ViewportCapture.ImageHeight", 720)
        Application.SetValue("ViewportCapture.ImageScale", 1)
        Application.SetValue("ViewportCapture.UserPixelRatio", False)
        Application.SetValue("ViewportCapture.FrameRate", 24)
        Application.SetValue("ViewportCapture.WriteAlpha", False)
        Application.SetValue("ViewportCapture.Start", start_frame)
        Application.SetValue("ViewportCapture.End", end_frame)
        Application.SetValue("ViewportCapture.CustomTimeRange", False)
        Application.SetValue("ViewportCapture.CaptureAudio", True)
        Application.SetValue("ViewportCapture.ReturnToInitialFrame", True)
        Application.SetValue("ViewportCapture.LaunchFlipbook", False)
        Application.SetValue("ViewportCapture.UseNativePlayer", False)
        Application.SetValue("ViewportCapture.OpenGLAntiAliasing", 1)

        view_manager = Application.Desktop.ActiveLayout.Views('vm')
        focused_viewport = view_manager.GetAttributeValue('focusedviewport')
        focused_viewport_type = view_manager.GetAttributeValue(
            'viewport:' + focused_viewport)

        if focused_viewport_type == 'Viewer':
            active_camera = view_manager.GetAttributeValue('activecamera:' +
                                                           focused_viewport)
        else:
            view_manager.SetAttributeValue('viewport:' + focused_viewport,
                                           'Viewer')

        view_manager.SetAttributeValue('activecamera:' + focused_viewport,
                                       render_camera)

        for parameter in camera_parameters:
            Application.SetValue(render_camera + "." + parameter, False)

        Application.SetValue("preferences.ViewCube.show", False)

        # with default parameters the view under cursor will be captured, use focused
        # use "_" beacause of base 1 for viewports
        Application.CaptureViewport('_ABCD'.find(focused_viewport))

        for parameter in viewport_capture_parameters:
            Application.SetValue("ViewportCapture." + parameter,
                                 viewport_capture_parameters[parameter])

        if focused_viewport_type == 'Viewer':
            view_manager.SetAttributeValue('activecamera:' + focused_viewport,
                                           active_camera)
        else:
            view_manager.SetAttributeValue('viewport:' + focused_viewport,
                                           focused_viewport_type)

        for parameter in camera_parameters:
            Application.SetValue(render_camera + "." + parameter,
                                 camera_parameters[parameter])

        Application.SetValue("preferences.ViewCube.show", view_cube_show)
    else:
        Application.SetValue("Passes.Default_Pass.Main.Enabled", True)
        Application.SetValue("Passes.RenderOptions.OutputDir",
                             temp_image_folder)
        Application.SetValue("Passes.Default_Pass.Main.Format", "png")
        Application.SetValue("Passes.Default_Pass.Main.Filename", image_name)
        Application.SetValue("Passes.Default_Pass.Renderer",
                             "Hardware Renderer")
        Application.SetValue("Passes.RenderOptions.Renderer",
                             "Hardware Renderer")
        Application.SetValue("Passes.Default_Pass.HardwareRenderer.RenderType",
                             "textured")
        Application.SetValue("Passes.RenderOptions.FramePadding", 4)
        Application.SetValue("Passes.Default_Pass.FrameRangeSource", 2)
        Application.SetValue("Passes.Default_Pass.Camera", render_camera)
        Application.SetValue("Passes.RenderOptions.ImageFormatPreset", -20)
        Application.SetValue("Passes.RenderOptions.FrameStart", start_frame)
        Application.SetValue("Passes.RenderOptions.FrameEnd", end_frame)
        Application.SetValue("Passes.HardwareRenderer.BackgroundColorGreen", 1)
        Application.SetCurrentPass("Passes.Default_Pass")

        Application.RenderPasses()

    for pre_view_value in pre_view_values:
        Application.SetValue(pre_view_value[0] + ".CTRLs_face.viewvis",
                             pre_view_value[1])

    # Burnin

    logger.debug(getpass.getuser())
    filters = [
        ['sg_domain_name', 'is', getpass.getuser()],
    ]
    fields = ['name']
    user = sg.find_one('HumanUser', filters, fields)

    logger.debug(user)

    filters = [
        ['entity', 'is', {
            'type': 'Shot',
            'id': int(shot_id)
        }],
        ['content', 'is', 'animation'],
    ]
    fields = [
        'step', 'entity', 'content', 'sg_status_list', 'sg_tk_file_suffix'
    ]
    task = sg.find_one('Task', filters, fields)

    logger.debug(task)

    filters = [
        ['id', 'is', task['step']['id']],
    ]
    fields = ['color', 'code']
    step = sg.find_one('Step', filters, fields)

    logger.debug(step)

    try:
        # Converter

        logger.info('start image burnin')
        if image_converter(temp_image_folder, image_name, start_frame, user,
                           task, step, render_camera, logger):
            logger.info('image burnin complete')
        else:
            logger.error("image burnin faild")
            print 'INHOUSE_ERROR'
            return -1

        # FFMPEG

        meta_data = task.get('sg_tk_file_suffix')
        if not meta_data:
            meta_data = step['code']

        v_index = image_name.rfind('_v')
        movie_file = temp_image_folder + "\\" + image_name[:
                                                           v_index] + "_" + meta_data + image_name[
                                                               v_index:] + ".mp4"

        audio_input = ""
        audio_settings = ""

        movie_duration = "%.4f" % ((end_frame - start_frame + 1) /
                                   Application.GetValue("PlayControl.Rate"))

        audio_filter = [
            "-filter_complex \"", "", "", "amix=inputs=" +
            str(len(audio_clips)) + ",atrim=duration=" + movie_duration + "\""
        ]

        if audio_clips:
            for i, audio_clip in enumerate(audio_clips, 1):
                audio_input += "-i \"" + audio_clip[0] + "\" "
                audio_delay = str(audio_clip[1])
                if audio_delay != "0":
                    audio_filter[1] += "[" + str(
                        i
                    ) + "]adelay=" + audio_delay + "|" + audio_delay + "[audio" + str(
                        i) + "];"
                    audio_filter[2] += "[audio" + str(i) + "]"
            audio_settings = "-acodec aac -ab 320k -ac 2 -strict experimental " + "".join(
                audio_filter)

        converter_ffmpeg = stuff_root + "\\programs\\win\\ffmpeg\\ffmpeg.exe"

        if os.path.exists(converter_ffmpeg):
            logger.debug("copy converter_ffmpeg: %s to %s" %
                         (converter_ffmpeg, temp_image_folder))
            shutil.copy2(converter_ffmpeg, temp_image_folder)
            converter_ffmpeg = os.path.join(temp_image_folder, 'ffmpeg.exe')
        else:
            logger.debug("couldn't find ffmpeg.exe file: %s" %
                         os.path.dirname(converter_ffmpeg))

            print 'INHOUSE_ERROR'
            return -1

        converter_ffmpeg += " -v error -y -r 24 -start_number " + str(
            start_frame
        ) + " -i " + temp_image_folder + "\\" + image_name + ".%04d.jpg " + audio_input + " -vcodec libx264 -pix_fmt yuv420p -vf scale=trunc((a*oh)/2)*2:720 -b:v 8000k " + audio_settings + " -r 24 -f mp4 " + movie_file

        if audio_input:
            logger.info('start ffmpeg converter with audio')
            for audio_clip in audio_clips:
                logger.info(audio_clip[0])
        else:
            logger.info('start ffmpeg converter')
        logger.debug(converter_ffmpeg)

        subprocess.call(converter_ffmpeg, shell=True)

        logger.info('conversion ffmpeg complete')

        # Copy

        if not os.path.exists(movie_file):
            logger.error("Couldn't create the movie file.")

            print 'INHOUSE_ERROR'
            return -1

        if not os.path.exists(server_image_folder + "\\movie"):
            os.makedirs(server_image_folder + "\\movie")
            logger.debug("create folder: %s" % server_image_folder + "\\movie")

        for f in glob.glob(server_image_folder + "\\" + image_name + ".*.jpg"):
            os.remove(f)
            logger.debug("delete: %s" % f)

        for f in glob.glob(temp_image_folder + "\\" + image_name + ".*.jpg"):
            shutil.copy2(f, server_image_folder)
            logger.debug("copy: %s to %s" % (f, server_image_folder))

        server_movie_file = server_image_folder + "\\movie\\" + image_name[:v_index] + "_" + meta_data + image_name[
            v_index:] + ".mp4"

        logger.debug("copy movie: %s to %s" % (movie_file, server_movie_file))
        shutil.copy2(movie_file, server_movie_file)

        if not os.path.exists(version_path):
            os.makedirs(version_path)
            logger.debug("create version folder: %s" % version_path)

        for f in glob.glob(version_path + "\\" + image_name + ".*.jpg"):
            os.remove(f)
            logger.debug("delete version: %s" % f)

        for f in glob.glob(temp_image_folder + "\\" + image_name + ".*.jpg"):
            shutil.copy2(f, version_path)
            logger.debug("copy version: %s to %s" % (f, version_path))

        server_movie_file = os.path.join(
            version_path, (image_name[:v_index] + "_" + meta_data +
                           image_name[v_index:] + ".mp4"))
        server_movie_file = server_movie_file.replace("\\", "/")

        logger.debug("copy version movie: %s to %s" %
                     (movie_file, server_movie_file))
        shutil.copy2(movie_file, server_movie_file)

        # SG

        frames = os.path.join(version_path, (image_name + ".#.jpg"))
        frames = frames.replace("\\", "/")

        version_name = image_name[:v_index] + "_" + meta_data + image_name[
            v_index:]

        data = {
            'project': {
                'type': 'Project',
                'id': int(project_id)
            },
            'code': version_name,
            'description': 'Autodailies',
            'sg_path_to_frames': frames,
            'sg_path_to_movie': server_movie_file,
            'sg_status_list': 'rev',
            'entity': {
                'type': 'Shot',
                'id': int(shot_id)
            },
            'sg_task': {
                'type': 'Task',
                'id': task['id']
            }
        }

        if not user:
            logger.error("Couldn't find the user: %s in Shotgun" %
                         getpass.getuser())
            data['description'] += "\nPublished by: %s" % getpass.getuser()
        else:
            data['created_by'] = {'type': 'HumanUser', 'id': user['id']}
            data['user'] = {'type': 'HumanUser', 'id': user['id']}

        logger.debug(data)

        result_v = sg.create('Version', data)

        logger.debug(result_v)

        if not result_v:
            logger.error("Couldn't create new version.")
            print 'INHOUSE_ERROR'
            return -1

        sg.follow(user, result_v)

        result_u = sg.upload('Version', result_v['id'], server_movie_file,
                             'sg_uploaded_movie')

        logger.debug(result_u)

        if not result_u:
            logger.error("Couldn't upload movie.")
            print 'INHOUSE_ERROR'
            return -1

        #logger.debug("Update Shot sg_publish: fin")
        #sg.update("Shot", int(shot_id), data={'sg_publish':'fin', 'sg_anim_export':'rtupd'})

        logger.debug("https://a-vfx.shotgunstudio.com/detail/Version/%s" %
                     str(result_v['id']))

    except Exception, e:
        print "Error:", str(e)
        print "Trace:", traceback.format_exc()

    shutil.rmtree(temp_image_folder)

    if exit:
        print "os._exit(0)"
        f.close()
        os._exit(0)

    f.close()

    return 0


def image_converter(image_folder, image_name, start_frame, user, task,
                    pipeline_step, render_camera, logger):
    border_height_percent = 8
    vertical_offset_percent = 5
    horizontal_offset_percent = 1.5
    lines_number = 2
    column_number = 100

    a_vfx_logo_offset_percent = 2
    a_vfx_logo_opacity = 0.3

    converter = stuff_root + '\\programs\\win\\ImageMagick\\convert.exe'
    identify = stuff_root + '\\programs\\win\\ImageMagick\\identify.exe'

    if os.path.exists(converter) and os.path.exists(identify):
        logger.debug("copy convert: %s to %s" % (converter, image_folder))
        shutil.copy2(converter, image_folder)
        converter = os.path.join(image_folder, 'convert.exe')

        logger.debug("copy identify: %s to %s" % (identify, image_folder))
        shutil.copy2(identify, image_folder)
        identify = os.path.join(image_folder, 'identify.exe')
    else:
        logger.debug("couldn't find convert.exe or identify.exe file: %s" %
                     os.path.dirname(converter))

        return False

    images = glob.glob(image_folder + '\\*.png')
    images.sort()

    cmd = []
    cmd.append(identify)
    options = ['-format %[fx:w]x%[fx:h]']
    for option in options:
        cmd.append(option)
    cmd.append(images[0])
    cmd = ' '.join(cmd)
    p = subprocess.Popen(
        cmd,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    output, err = p.communicate()
    rc = p.returncode
    if not rc:
        w, h = output.split('x')
    else:
        w = 1280.0
        h = 720.0
    image_height = int(h)
    image_width = int(w)

    width = float(image_width)
    height = float(image_height)

    border_height = height * (border_height_percent / 100.0)
    vertical_offset = border_height * (vertical_offset_percent / 100.0)
    horizontal_offset = width * (horizontal_offset_percent / 100.0)

    step = lines_number

    text_size = (border_height - vertical_offset * 2) / step
    vertical_step = text_size

    horizontal_step = width / column_number

    user_column = 0  #0
    user_row = 1
    date_column = user_column
    date_row = 0
    camera_column = 30  #30
    camera_row = 1
    shot_column = camera_column
    shot_row = 0
    convergence_column = 49
    convergence_row = 1
    parallax_column = convergence_column
    parallax_row = 0
    step_column = 65  #52
    step_row = 1
    task_column = step_column
    task_row = 0
    frame_column = 82  #82
    frame_row = 1
    version_column = frame_column
    version_row = 0

    # need to fix
    text_font_coefficient = 0.545  #0.545
    #text_font_coefficient = 1.9/3.0
    column_separator = 2

    options = [
        '-alpha off -resize ' + str(width) + 'x' + str(height),
        '-background red', '-gravity center',
        '-extent ' + str(width) + 'x' + str(height), '-quality 100%'
    ]

    options.extend([
        '-fill graya(0%,0.5)', '-draw "rectangle 0,' + str(height) + ' ' +
        str(width) + ',' + str(height *
                               (1 - border_height_percent / 100.0)) + '"'
    ])

    name = user['name']

    if (len(name) - column_separator
        ) * text_font_coefficient * text_size >= horizontal_step * (
            camera_column - user_column - column_separator):
        k = horizontal_step * (camera_column - user_column - column_separator
                               ) / ((len(name) - column_separator) *
                                    text_font_coefficient * text_size)
    else:
        k = 1.0

    text_format = [
        '-fill gray(60%)', '-pointsize ' + str(text_size),
        '-gravity SouthWest', '-font Consolas'
    ]
    text_format_r = [
        '-fill rgb(150,15,15)', '-pointsize ' + str(text_size * k),
        '-gravity SouthWest', '-font Consolas'
    ]
    text_format_k = [
        '-fill gray(60%)', '-pointsize ' + str(text_size * k),
        '-gravity SouthWest', '-font Consolas'
    ]

    s = task['content']
    if (len(s) - column_separator
        ) * text_font_coefficient * text_size >= horizontal_step * (
            version_column - task_column):
        s = s[:int((horizontal_step * (version_column - task_column)) / (
            text_font_coefficient * text_size)) - column_separator] + '..'

    (fd, filename) = tempfile.mkstemp()
    template_file = os.fdopen(fd, "w")
    template_file.write(s)
    template_file.close()

    options.extend(text_format_k + [
        '-annotate +' + str(horizontal_offset + horizontal_step * user_column)
        + '+' + str(vertical_step * user_row +
                    vertical_offset) + ' "artist: ' + name + '"'
    ])
    options.extend(text_format + [
        '-annotate +' + str(horizontal_offset + horizontal_step * date_column)
        + '+' + str(vertical_step * date_row + vertical_offset) + ' "date:   '
        + datetime.datetime.now().strftime('%d %b %Y') + '"'
    ])

    #camera_name = Application.GetValue("Passes.Default_Pass.Camera")
    camera_name = render_camera

    camera_sensor_width = Application.GetValue(camera_name +
                                               ".camera.projplanewidth")
    camera_focal_length = Application.GetValue(camera_name +
                                               ".camera.projplanedist")

    focal_length_35 = camera_focal_length

    camera_burn = " " + camera_name[3] + " f%.1f" % focal_length_35

    options.extend(text_format + [
        '-annotate +' + str(
            horizontal_offset + horizontal_step * camera_column) + '+' + str(
                vertical_step * camera_row + vertical_offset) + ' "cam: ' +
        camera_burn + '"'
    ])

    options.extend(text_format + [
        '-annotate +' + str(horizontal_offset + horizontal_step * shot_column)
        + '+' + str(vertical_step * shot_row + vertical_offset) + ' "shot: ' +
        task['entity']['name'] + '"'
    ])

    camera_convergence = Application.GetValue(
        camera_name.split('.')[0] + ".StereoCamera_Root.Stereo.Separation")
    camera_parallax = Application.GetValue(
        camera_name.split('.')[0] + ".StereoCamera_Root.Stereo.ZeroParallax")

    options.extend(text_format + [
        '-annotate +' + str(horizontal_offset + horizontal_step *
                            convergence_column) + '+' +
        str(vertical_step * convergence_row + vertical_offset) + (
            ' "c: %.2f' % camera_convergence) + '"'
    ])
    options.extend(text_format + [
        '-annotate +' + str(
            horizontal_offset + horizontal_step * parallax_column) + '+' + str(
                vertical_step * parallax_row + vertical_offset) + (
                    ' "p: %.1f' % camera_parallax) + '"'
    ])

    options.extend(text_format + [
        '-annotate +' + str(horizontal_offset + horizontal_step * step_column)
        + '+' + str(vertical_step * step_row +
                    vertical_offset) + ' "' + pipeline_step['code'] + '"'
    ])
    options.extend(text_format + [
        '-annotate +' + str(horizontal_offset + horizontal_step * task_column)
        + '+' + str(vertical_step * task_row +
                    vertical_offset) + ' @' + filename
    ])

    options.extend(text_format + [
        '-annotate +' + str(
            horizontal_offset + horizontal_step * version_column) + '+' + str(
                vertical_step * version_row + vertical_offset) + ' "ver:   ' +
        image_name[image_name.rfind('_v') + 2:] + '"'
    ])

    k1 = 2.0
    x = horizontal_offset + horizontal_step * step_column - text_font_coefficient * text_size * 1.5
    y = height - vertical_offset - text_size / 2.0 - vertical_step * step_row - text_size / (
        k1 * 2 - (k1 - 1))
    options.extend([
        '-fill rgb(' + pipeline_step['color'] + ')', '-stroke graya(0%)',
        '-strokewidth 1', '-draw "rectangle ' + str(x) + ',' + str(y) + ' ' +
        str(x + text_size / k1) + ',' + str(y + text_size / k1) + '"'
    ])

    actual_task_status = task['sg_status_list']

    status_file = stuff_root + '\\tools\\lib\\python\\inhouse\\shotgun\\sg_status_images\\' + actual_task_status + '.png'

    temp_status_file = image_folder + '\\status.png'
    use_status_file = False

    if os.path.exists(status_file):
        logger.debug("copy: %s to %s" % (status_file, temp_status_file))
        shutil.copy2(status_file, temp_status_file)
        use_status_file = True
    else:
        logger.debug("couldn't find status file: %s" % status_file)

    a_vfx_logo_file = stuff_root + '\\tools\\lib\\python\\inhouse\\shotgun\\sg_status_images\\a_vfx_logo.png'

    temp_a_vfx_logo_file = image_folder + '\\a_vfx_logo.png'
    use_a_vfx_logo_file = False

    if os.path.exists(a_vfx_logo_file):
        logger.debug("copy: %s to %s" %
                     (a_vfx_logo_file, temp_a_vfx_logo_file))
        shutil.copy2(a_vfx_logo_file, temp_a_vfx_logo_file)
        use_a_vfx_logo_file = True
    else:
        logger.debug("couldn't find a-vfx logo file: %s" % a_vfx_logo_file)

    cmd_1 = []
    for option in options:
        cmd_1.append(option)
    cmd_1 = ' '.join(cmd_1)
    options = []

    enumerate_images = []
    for i, image in enumerate(images):
        enumerate_images.append([i, image])

    pack_lentgh = 8
    packed_images = []
    for pack_index in range(len(enumerate_images) / pack_lentgh):
        packed_images.append(enumerate_images[pack_index * pack_lentgh:(
            pack_index + 1) * pack_lentgh])

    if len(enumerate_images) % pack_lentgh:
        packed_images.append(enumerate_images[(len(enumerate_images) /
                                               pack_lentgh) * pack_lentgh:])

    for pack in packed_images:
        pack_processes = []
        pack_processes_logo_cmds = []
        pack_processes_logo = []

        for image_pair in pack:
            i = image_pair[0]
            image = image_pair[1]

            cmd = []
            cmd.append(converter)

            cmd_logo = []
            cmd_logo.append(converter)

            cmd.append(image)

            cmd.append(cmd_1)

            options = ['-stroke none'] + text_format + [
                '-annotate +' +
                str(horizontal_offset + horizontal_step * frame_column) + '+' +
                str(vertical_step * frame_row + vertical_offset) + ' "frame:' +
                str(int(start_frame) + i).rjust(4) + '/' +
                str(int(start_frame) + len(images) - 1) + '"'
            ]
            for option in options:
                cmd.append(option)

            if use_status_file:
                cmd.append(temp_status_file)
                k2 = 1.75
                x = horizontal_offset + horizontal_step * task_column - text_font_coefficient * text_size * (
                    1.5 + (1 / k2 - (1 / (k1 / 2.0)) / 2.0))
                y = height - vertical_offset - text_size / 2.0 - vertical_step * task_row - text_size / (
                    k2 * 2 - (k2 - 1))
                cmd.append("-gravity None -geometry x" + str(text_size / k2) +
                           "+" + str(x) + "+" + str(y) + " -composite")

            if use_a_vfx_logo_file:
                cmd.append(image[:-4] + '_for_logo.png')

                cmd_logo.append(image[:-4] + '_for_logo.png -quality 100%')
                cmd_logo.append(temp_a_vfx_logo_file)

                x = (width / 100) * a_vfx_logo_offset_percent
                y = (height / 100) * a_vfx_logo_offset_percent

                cmd_logo.append("-gravity NorthEast -geometry +" + str(
                    y) + "+" + str(y) + " -channel a -evaluate multiply " +
                                str(a_vfx_logo_opacity) + " -composite")
                cmd_logo.append(image[:-4] + '.jpg')

                cmd_logo = ' '.join(cmd_logo)

                pack_processes_logo_cmds.append(cmd_logo)
            else:
                cmd.append(image[:-4] + '.jpg')

            cmd = ' '.join(cmd)

            pack_processes.append(subprocess.Popen(cmd, shell=True))

        for pack_process in pack_processes:
            pack_process.wait()

        for pack_processes_logo_cmd in pack_processes_logo_cmds:
            pack_processes_logo.append(
                subprocess.Popen(pack_processes_logo_cmd, shell=True))

        for pack_process_logo in pack_processes_logo:
            pack_process_logo.wait()

    os.remove(filename)

    return True
