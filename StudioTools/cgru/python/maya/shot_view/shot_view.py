# -*- coding: utf-8 -*-
import os
import sys
import tempfile
import subprocess
from pprint import pprint
import re
import getpass
import json
import glob
import shutil
from random import randint

import maya
import maya.cmds as cmds
import maya.OpenMayaUI as mui

try:
    from PySide.QtGui import *
    from PySide.QtCore import *
    import shiboken
except ImportError:
    from PySide2.QtGui import *
    from PySide2.QtCore import *
    from PySide2.QtWidgets import *
    import shiboken2 as shiboken

from core.python.utilities.shotgun import ShotgunBuddy
from core.python.EntitySettings import EntitySettings

#import sgtk

from .config import (
    RVPUSH,
    FFMPEG,
    HEAD_IN_FIELD,
    TAIL_OUT_FIELD,
    TIMECODE_FIELD,
    ANIMATION_STEPS,
    RV_HANDLER_PATH,
    OUT_STEP_NAME,
)
from .settings import MODEL_EDITOR_SETTINGS
from .settings import PLAYBLAST_SETTINGS, RESOLUTIONS
from .options_window import OptionsWindow

MAYA_APP_DIR = os.environ['MAYA_APP_DIR']

CUR_DIR = os.path.dirname(os.path.abspath(__file__))
SETTINGS_JSON_FILENAME = 'shot_view_settings.json'
DEFAULT_SETTINGS_JSON = os.path.join(CUR_DIR, SETTINGS_JSON_FILENAME)
SETTINGS_JSON = os.path.join(MAYA_APP_DIR, SETTINGS_JSON_FILENAME)

ICONS_DIR = os.path.join(CUR_DIR, 'icons')

PLUS_ICON = os.path.join(ICONS_DIR, 'size_up.png')
MINUS_ICON = os.path.join(ICONS_DIR, 'size_down.png')
PLAYBLAST_ICON = os.path.join(ICONS_DIR, 'playblast.png')
PLAYBLAST_HOOKUP_ICON = os.path.join(ICONS_DIR, 'playblast_hookup.png')
DAILIES_ICON = os.path.join(ICONS_DIR, 'dailies.png')
CAMERA_LOCK_ICON = os.path.join(ICONS_DIR, 'camera.png')
UP_VERSION_ICON = os.path.join(ICONS_DIR, 'up_version.png')
OPTIONS_ICON = os.path.join(ICONS_DIR, 'options.png')


"""
Usage:

from core.python.maya.shot_view import shot_view as shv
reload(shv)
shv.run()

"""


class ShotView(QDialog):
    """
    Главный класс окна Shot View
    """

    def __init__(self, parent, **kwargs):
        super(ShotView, self).__init__(parent, **kwargs)
        self.parent = parent
        self.setObjectName("MyWindow")
        self.setWindowTitle("Shot View")
        self.setWindowFlags(Qt.WindowCloseButtonHint |
                            self.windowFlags())

        self.verticalLayout = QVBoxLayout(self)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.playblast_viewport = "playblast_viewport_" + str(
            randint(1, 100000))

        # if cmds.modelEditor('playblast_viewport', query=True, exists=True):
        try:
            cmds.deleteUI(self.playblast_viewport, panel=True)
        except:
            pass

        # need to set a name so it can be referenced by maya node path
        self.verticalLayout.setObjectName("mainLayout")

        # First use SIP to unwrap the layout into a pointer
        # Then get the full path to the UI in maya as a string
        layout = mui.MQtUtil.fullName(
            long(shiboken.getCppPointer(self.verticalLayout)[0]))
        cmds.setParent(layout)

        self.paneLayoutName = cmds.paneLayout(w=640, h=360)

        # Find a pointer to the paneLayout that we just created
        ptr = mui.MQtUtil.findControl(self.paneLayoutName)

        # Wrap the pointer into a python QObject
        self.paneLayout = shiboken.wrapInstance(long(ptr), QWidget)

        # obtaint default camera, not precise but good for now
        self.camera_name = (
            cmds.ls('cam*', cameras=1) or
            # cmds.ls('cam?:left*', cameras=1) or
            # cmds.ls('ext_cam?:center*', cameras = 1) or
            ['perspShape'])[0]
        self.camera_name = cmds.listRelatives(self.camera_name, parent=True)[0]

        # get data
        self.scene_file = cmds.file(q=True, sn=True)
        est = EntitySettings(self.scene_file, os.environ["PRVZ_TOOLS_PATH"])
        self.shot_name = '{episode}_{shot}{id}'.format(**est.dyn_vars)

        if self.scene_file.startswith('//'):
            self.scene_file = self.scene_file[1:]

        #self.engine = sgtk.platform.current_engine()
        #self.sg = self.engine.shotgun

        sgb = ShotgunBuddy()
        self.sg = sgb.sg
        #self.ctx = self.engine.context
        #self.entity = self.ctx.entity

        self.settings = []
        self.current_settings = []

        #self.project_name = self.ctx.project["name"]
        #new:
        self.project_name = os.environ['PRVZ_PROJECT_NAME']
        project_id = int(os.environ['PRVZ_PROJECT_ID'])

        #if self.entity["type"] == "Shot":
        if est.dyn_vars['process'] in ['anim', 'asmbl', 'layout']:
            #self.shot_name = self.entity["name"]

            fields = [
                "sg_cut_duration",
                TIMECODE_FIELD,
                HEAD_IN_FIELD,
                TAIL_OUT_FIELD,
                "sg_fps",
                "project.Project.sg_fps",
                "sg_sequence.Sequence.code"
            ]
                
            self.shot = self.sg.find_one(
                "Shot",
                [['project.Project.id', 'is', project_id], ['code', 'is', self.shot_name]],
                fields)

            self.sequence_name = self.shot["sg_sequence.Sequence.code"]
            self.fps = self.shot["sg_fps"]
            if self.fps is None or self.fps == 0:
                self.fps = self.shot["project.Project.sg_fps"]

            self.shot_timecode = self.shot[TIMECODE_FIELD]
        else:
            self.shot_timecode = 0

        name = os.path.splitext(os.path.basename(self.scene_file))
        version_string = re.findall(r'_v\d{3}$', name[0])
        if version_string:
            self.version = int(version_string[0][-3:])
        else:
            self.version = 1

        self.modelPanelName = cmds.modelPanel(
            self.playblast_viewport,
            label="ModelPanel",
            cam=self.camera_name,
            mbv=False)
        maya.mel.eval('lookThroughModelPanel %s %s;' %
                      (self.camera_name, self.modelPanelName))
        # Find a pointer to the modelPanel that we just created
        ptr = mui.MQtUtil.findControl(self.modelPanelName)

        # Wrap the pointer into a python QObject
        self.modelPanel = shiboken.wrapInstance(long(ptr), QWidget)

        cmds.camera(
            self.camera_name,
            e=True,
            displayResolution=False,
            displayFilmGate=False,
            overscan=1.0)
        cmds.setFocus(self.modelPanelName)

        # add our QObject reference to the paneLayout to our layout
        self.verticalLayout.addWidget(self.paneLayout)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setContentsMargins(0, 0, 2, 2)

        ptr = mui.MQtUtil.findControl(cmds.timePort())
        self.timePort = shiboken.wrapInstance(long(ptr), QWidget)
        self.timePort.setFixedHeight(30)
        self.horizontalLayout.addWidget(self.timePort)

        self.up_size_button = QPushButton()
        self.up_size_button.setToolTip("Size up")
        self.up_size_button.clicked.connect(self.changeSize)
        self.up_size_button.setIcon(QIcon(PLUS_ICON))

        self.down_size_button = QPushButton()
        self.down_size_button.setToolTip("Size down")
        self.down_size_button.clicked.connect(self.changeSize)
        self.down_size_button.setIcon(QIcon(MINUS_ICON))

        self.playblast_menu = QMenu()
        self.hook_up_action = self.playblast_menu.addAction('HookUp')

        self.hook_up_action.triggered.connect(self.hookUp)

        self.compare_action = self.playblast_menu.addAction('Compare')

        self.compare_action.triggered.connect(self.compare)

        self.options_window = OptionsWindow(self)
        self.options_window.accepted.connect(self.setOptions)

        self.playblast_button = QPushButton()
        self.playblast_button.setToolTip("Make playblast")
        self.playblast_button.clicked.connect(self.playBlast)
        self.playblast_button.setIcon(
            QIcon(PLAYBLAST_ICON))

        self.hook_up_button = QPushButton()
        self.hook_up_button.setToolTip(
            "Make playblast and connect to closest shots")
        self.hook_up_button.setIcon(
            QIcon(PLAYBLAST_HOOKUP_ICON))
        self.hook_up_button.clicked.connect(self.hookUp)

        #if self.entity["type"] != "Shot": self.hook_up_button.setEnabled(False) # todo Normal check
        self.hook_up_button.setEnabled(True)

        self.compare_button = QPushButton('Compare')
        self.compare_button.clicked.connect(self.compare)

        self.dailies_button = QPushButton()
        self.dailies_button.setIcon(
            QIcon(DAILIES_ICON))
        # self.dailies_button.clicked.connect(self.createDailies)

        self.lock_camera_button = QPushButton()
        self.lock_camera_button.setToolTip("Select Camera")
        self.lock_camera_button.setIcon(
            QIcon(CAMERA_LOCK_ICON))
        #        self.lock_camera_button.setCheckable(True)
        #        self.lock_camera_button.toggled.connect(self.lockCamera)

        self.up_version_button = QPushButton()
        self.up_version_button.setIcon(
            QIcon(UP_VERSION_ICON))
        self.up_version_button.clicked.connect(self.upVersion)

        self.options_button = QPushButton()
        self.options_button.setToolTip("Options")
        self.options_button.setIcon(
            QIcon(OPTIONS_ICON))
        self.options_button.clicked.connect(self.showOptions)
        self.options_button.pressed.connect(self.onmousePressEvent)
        self.options_button.released.connect(self.onmouseReleaseEvent)

        self.tool_bar = QHBoxLayout()

        self.tool_bar.addWidget(self.down_size_button)
        self.tool_bar.addWidget(self.up_size_button)
        self.tool_bar.addWidget(self.playblast_button)
        self.tool_bar.addWidget(self.hook_up_button)
        # self.tool_bar.addWidget(self.compare_button)
        # self.tool_bar.addWidget(self.dailies_button)
        self.tool_bar.addWidget(self.lock_camera_button)
        # self.tool_bar.addWidget(self.up_version_button)
        self.tool_bar.addWidget(self.options_button)

        for index in range(self.tool_bar.count()):
            button = self.tool_bar.itemAt(index).widget()
            button.setIconSize(QSize(25, 25))
            button.setFixedSize(45, 35)

        self.horizontalLayout.addLayout(self.tool_bar)
        # self.horizontalLayout.addWidget(self.button1)

        self.verticalLayout.addLayout(self.horizontalLayout)

        self.options_window.grab_settings_button_w.clicked.connect(
            self.grabWorkSettings)
        self.options_window.grab_settings_button_p.clicked.connect(
            self.grabPlayblastSettings)

        self.options_window.default_settings_button_w.clicked.connect(
            self.setDefaultWorkSettings)
        self.options_window.default_settings_button_p.clicked.connect(
            self.setDefaultPlayblastSettings)

        self.options_button.customContextMenuRequested.connect(
            self.optionMenuEvent)
        self.options_button.setContextMenuPolicy(Qt.CustomContextMenu)

        self.lock_camera_button.customContextMenuRequested.connect(
            self.cameraMenuEvent)
        self.lock_camera_button.setContextMenuPolicy(
            Qt.CustomContextMenu)

        self.loadSettings()

    def optionMenuEvent(self, event):
        menu = QMenu(self)
        work_action = QAction('Work View', self)
        work_action.triggered.connect(self.work_action_do)
        menu.addAction(work_action)

        pb_action = QAction('Playblast View', self)
        pb_action.triggered.connect(self.pb_action_do)
        menu.addAction(pb_action)

        menu.popup(QCursor.pos())

    def work_action_do(self):
        self.loadSettings()
        self.setSettings(self.settings["work"])

    def pb_action_do(self):
        self.loadSettings()
        self.setSettings(self.settings["playblast"])

    def cameraMenuEvent(self, event):
        self.camera_menu = QMenu(self)
        self.camera_action = []
        camera_shapes = [
            cmds.listRelatives(i, parent=True)[0]
            for i in cmds.ls(cameras=True)
        ]
        for cam in camera_shapes:
            c_action = QAction(cam, self)
            c_action.triggered.connect(lambda cam=cam: self.c_action_do(cam))
            self.camera_menu.addAction(c_action)
            self.camera_action.append(c_action)
        self.camera_menu.popup(QCursor.pos())

    def c_action_do(self, cam):
        self.camera_name = cam
        maya.mel.eval('lookThroughModelPanel %s %s;' %
                      (self.camera_name, self.modelPanelName))

    def reject(self):
        # При закрытие окна удаляем вьюпорт
        cmds.deleteUI(self.playblast_viewport, panel=True)
        self.close()

    def savePos(self):
        """
        Сохраняет позицию окна
        """
        x = self.pos().x()
        y = self.pos().y()
        self.settings['work']['position'] = [x, y]

    def resizeEvent(self, resizeEvent):
        """
        Обработчик изменения размера окна.
        Сохраняет заданые пропорции.
        """
        width = self.width()
        height = self.height()
        self.setFixedHeight(int(width / 1.684210526))

    def saveSize(self):
        """
        Запоминает размер окна
        """
        width = self.width()
        height = self.height()
        self.settings['work']['resolution'] = [width, height]

    def changeSize(self):
        """
        Обработчик кнопок +, -.
        После изменения размера окна
        перемещает курсор на кноку,
        что позволяет не двигать курсор,
        при многократном нажатиии.
        """
        button = self.sender()
        if button == self.up_size_button:
            factor = 1.1
        else:
            factor = 0.9

        self.resize(int(self.width() * factor), int(self.height() * factor))
        button_center = self.mapToGlobal(button.geometry().center())
        QCursor.setPos(button_center)
        self.saveSize()

    def playBlast(self, play=True, image_path=None):
        """
        Создает плейбласт и при неоходимости
        открывает его в RV
        """
        #        self.camera_name = self.options_window.camera_work.currentText()
        maya.mel.eval('lookThroughModelPanel %s %s;' %
                      (self.camera_name, self.modelPanelName))

        def cleanup(shot_folder, playblast_name, playblast_version):
            for i in range(10):
                if playblast_version - i - 10 < 1: continue
                version_name = "%s_v%03d" % (playblast_name,
                                             playblast_version - i - 10)
                image_folder = os.path.join(shot_folder, version_name)
                if os.path.exists(image_folder): shutil.rmtree(image_folder)

        if image_path is None:

            #            playblast_name = self.shot_name or "playblast"
            playblast_name = "playblast"

            temp_folder = os.path.join(tempfile.gettempdir(), 'media_grabber')
            shot_folder = os.path.join(temp_folder, self.shot_name)
            playblast_version = 1
            playblasts = glob.glob(
                os.path.join(shot_folder, "%s_v[0-9][0-9][0-9]/" %
                             playblast_name))
            if playblasts:
                playblasts.sort()
                last_playblast = playblasts[-1]
                version = re.findall(
                    r'_v(?P<version>\d{3})[/\\]$',
                    last_playblast)
                if version:
                    playblast_version = int(version[-1]) + 1
            version_name = "%s_v%03d" % (playblast_name, playblast_version)
            image_folder = os.path.join(shot_folder, version_name)
            cleanup(shot_folder, playblast_name, playblast_version)
            image_path = os.path.join(shot_folder, version_name, version_name)

        aPlayBackSliderPython = maya.mel.eval('$tmpVar=$gPlayBackSlider')
        time_range_value = cmds.timeControl(
            aPlayBackSliderPython, q=True, ra=True)

        if (time_range_value[1] - time_range_value[0]) > 2:
            self.start_frame = time_range_value[0]
            self.end_frame = time_range_value[1]
        else:
            self.start_frame = cmds.playbackOptions(q=True, minTime=True)
            self.end_frame = cmds.playbackOptions(q=True, maxTime=True)

        resolution = self.settings['playblast']['resolution']
        safe_frame = self.settings['playblast']['safe_frame']
        center_frame = self.settings['playblast']['center_frame']

        self.current_settings = self.grabSettings()
        self.setSettings(self.settings["playblast"])

        cmds.setAttr("hardwareRenderingGlobals.ssaoAmount", 1)
        cmds.setAttr("hardwareRenderingGlobals.multiSampleEnable", 1)
        cmds.setAttr("hardwareRenderingGlobals.ssaoEnable", 1)
        cmds.setAttr("hardwareRenderingGlobals.ssaoRadius", 13)
        cmds.setAttr("hardwareRenderingGlobals.ssaoSamples", 32)
        cmds.setAttr("hardwareRenderingGlobals.gammaCorrectionEnable", 1)
        cmds.setAttr("hardwareRenderingGlobals.gammaValue", 1.4)

        cmds.setFocus(self.modelPanelName)
        cmds.select(self.camera_name, add=True)

        cmds.playblast(
            filename=image_path,
            st=self.start_frame,
            et=self.end_frame,
            w=resolution[0],
            h=resolution[1],
            **PLAYBLAST_SETTINGS)

        self.setSettings(self.current_settings)
        cmds.select(self.camera_name, d=True)

        image_path_jpg = image_path + '.####.jpg'
        audio_path = image_path + ".wav"
        audio_is_maked = self.makeAudio(
            audio_path, self.start_frame, self.end_frame)
        if audio_is_maked and self.shot_timecode:
            sources = [image_path_jpg, audio_path]
        else:
            sources = image_path_jpg
        if play:
            self.launchRV([sources])
        else:
            return sources

    def makeAudio(self, audio_path, start_frame, end_frame):
        clip_list = cmds.ls(type="audio")
        clips = []

        for c in clip_list:
            if cmds.getAttr(c + ".mute"): continue
            of = cmds.getAttr(c + ".offset")
            fn = cmds.workspace(expandName=cmds.getAttr(c + ".filename"))
            ss = cmds.getAttr(c + ".sourceStart")
            se = cmds.getAttr(c + ".sourceEnd")
            start = start_frame + ss
            end = end_frame + se

            clips.append({
                "name": c,
                "offset": of,
                "filename": fn,
                "start": start,
                "end": end
            })

        duration_time = (end_frame - start_frame + 1) / float(self.fps)

        audio_input = ""
        audio_filter = [
            "-filter_complex \"", "", "", "amix=inputs=" + str(len(clips)) +
            ",atrim=duration=" + str(duration_time) + "\""
        ]
        for i, c in enumerate(clips, 0):
            audio_input += "-i \"" + c["filename"] + "\" "
            filters = []
            if c["offset"] > 0:
                offset = int(c["offset"] * 1000 / float(self.fps))
                filters.append("adelay={0}|{0}".format(offset))

            if c["start"] > 0:
                start = c["start"] / float(self.fps)
                end = c["end"] / float(self.fps)
                filters.append("atrim=%s:%s" % (start, end))
            if filters:
                filters = ",".join(filters)
                audio_filter[1] += "[%d]" % i + filters + "[audio%d];" % i
                audio_filter[2] += "[audio%d]" % i

        audio_settings = "".join(audio_filter)
        ffmpeg_cmd = FFMPEG + " " + audio_input + " " + "".join(
            audio_filter) + " " + audio_path
       
        if duration_time <= 0:
            return False

        returncode = subprocess.call(ffmpeg_cmd, shell=True)
        if returncode == 0:
            return True
        else:
            return False

    def lockCamera(self, checked):
        """
        Обработчик кнопки заблокировать камеру.
        """
        cam_shape = cmds.listRelatives(self.camera_name, parent=1)
        asset = None
        try:
            asset = cmds.container(q=True, findContainer=self.camera_name)
        except RuntimeError:
            pass

        if asset:
            node = asset
        else:
            node = self.camera_name

        attrs = cmds.listAttr(asset, k=True)
        for attr in attrs:
            if not 'visibility' in attr:
                if checked:
                    cmds.setAttr(node + "." + attr, lock=True)
                else:
                    cmds.setAttr(node + "." + attr, lock=False)

    def hookUp(self):
        """
        Создает плейбласт и открывает его в RV с
        соседними шотами
        """
        sg_project_name = self.project_name
        sg_sequence_name = self.sequence_name
        sg_shot_name = self.shot_name

        filters = [
            ['sg_sequence', 'name_is', sg_sequence_name],
            ['project', 'name_is', sg_project_name],
            ['sg_status_list', 'is_not', 'omt'],
            #["sg_shot_type", "is_not", "out of cut"],
        ]

        fields = ['code', 'sg_shot_type', 'sg_cut_duration']
        order = [{'field_name': 'code', 'direction': 'asc'}]
        shots = self.sg.find('Shot', filters, fields, order)

        # shots = [
        #     s for s in shots
        #     if s["code"].find("cam") == -1
        # ]
        shot_position = [
            index for (index, s) in enumerate(shots)
            if s["code"] == sg_shot_name
        ][0]
        #from pprint import pprint
        versions = []
        for shot in range(
                max(0, shot_position - 1), min(len(shots), shot_position + 2)):
            if shot == shot_position:
                versions.append(self.playBlast(play=False))
            else:
                filters_variants = [
                    [['entity', 'is', shots[shot]],
                     ["sg_task.Task.step.Step.short_name", "in", ANIMATION_STEPS]],
                ]
                fields = [
                    'id', 'code', 'sg_path_to_movie', 'sg_path_to_frames',
                    'sg_task', 'created_at'
                ]
                order = [{'field_name': 'created_at', 'direction': 'desc'}]
                for f in filters_variants:
                    v = self.sg.find_one("Version", f, fields, order)
                    # pprint (f)
                    # print ('V =', v)
                    # try:
                    #     print ('-----', v['sg_path_to_movie'])
                    # except Exception as ex:
                    #     print ('ex -----', ex)

                    if v and v['sg_path_to_movie']:
                        #if  os.path.exists(v['sg_path_to_movie']):
                        p = v['sg_path_to_movie']
                        print ('p = %s' %  p)
                        pp = [x for x in p.split('\\') if x]
                        print ('pp = %s' % pp)
                        p = os.path.normcase(os.path.join(os.environ['PRVZ_PROJECT_PATH'], *pp[2:]))
                        print ('new p = %s' % p)
                        if os.path.exists(p):
                            print ('new p found = %s' % p)
                            versions.append(p)
                            break
        self.launchRV(versions)

    def compare(self):
        """
        Создает плейбласт и открывает его
        с предыдущей версией в RV
        """
        sg_project_name = self.project_name
        sg_sequence_name = self.sequence_name
        sg_shot_name = self.shot_name

        fields = [
            'id', 'code', 'sg_path_to_movie', 'sg_path_to_frames', 'sg_task',
            'created_at'
        ]
        filters = [
            ['project', 'name_is', sg_project_name],
            ['entity', 'name_is', sg_shot_name],
            ['sg_task', 'name_is', 'animation'],
        ]
        order = [{'field_name': 'created_at', 'direction': 'desc'}]
        prev_version = self.sg.find_one("Version", filters, fields, order)

        versions = [self.playBlast(play=False)]

        if prev_version and prev_version[
                'sg_path_to_movie'] and os.path.exists(
                    prev_version['sg_path_to_movie']):
            versions.insert(0, prev_version['sg_path_to_movie'])

        self.launchRV(versions)

    def createDailies(self):
        """
        Запускает сдачу дейлиза. Устарело.
        """
        pass
        # self.window = AnimationDailiesWindow(self)
        # self.window.show()

    def upVersion(self):
        """
        Сохраняет сцену под новой версией.
        """
        return #
        scene_folder = os.path.dirname(self.scene_file)

        next_version = self.version + 1
        new_scene_file = os.path.join(scene_folder, '%s_ANIM_v%03d.ma' %
                                      (self.shot_name, next_version))

        cmds.file(rename=new_scene_file)
        cmds.file(save=True, type='mayaAscii')

        self.version = next_version
        self.scene_file = new_scene_file

        QMessageBox.information(self, "Information!",
                                      "New version saved!")

    def onmousePressEvent(self):
        self.current_settings = self.grabSettings()
        self.setSettings(self.settings["playblast"])

    def onmouseReleaseEvent(self):
        self.setSettings(self.current_settings)

    def showOptions(self):
        """
        Открывает окно настроек вьюпорта
        """
        self.options_window.safe_frame_checkbox_w.setChecked(
            self.settings['work']['safe_frame'])
        self.options_window.center_frame_checkbox_w.setChecked(
            self.settings['work']['center_frame'])
        if 'wireframe_shaded' in self.settings['work']:
            self.options_window.wireframe_on_shaded_w.setChecked(
                self.settings['work']['wireframe_shaded'])

        self.options_window.safe_frame_checkbox_p.setChecked(
            self.settings['playblast']['safe_frame'])
        self.options_window.center_frame_checkbox_p.setChecked(
            self.settings['playblast']['center_frame'])
        if 'wireframe_shaded' in self.settings['playblast']:
            self.options_window.wireframe_on_shaded_p.setChecked(
                self.settings['playblast']['wireframe_shaded'])

        if 'resolution' in self.settings['playblast']:
            resolution = tuple(self.settings['playblast']['resolution'])
            print ('resolution = %s' % str(resolution))
            try:
                self.options_window.resolution_combobox_p.setCurrentIndex(RESOLUTIONS.index(resolution))
            except Exception as ex:
                print ('ERROR: cant set resolution combo: %s' % ex)
            # self.options_window.wireframe_on_shaded_p.setChecked(
            #     self.settings['playblast']['wireframe_shaded'])

#        camera_name = self.camera_name
#        self.options_window.camera_work.clear()
#        self.options_window.camera_shapes = [cmds.listRelatives(i, parent=True)[0] for i in cmds.ls(cameras = True)]
#        self.options_window.camera_work.addItems(self.options_window.camera_shapes)
#        self.options_window.camera_work.setCurrentIndex(self.options_window.camera_shapes.index(camera_name))

        self.options_window.show()

    def setOptions(self):
        """
        Считывает настройки из окна настроек и запоминает
        """
        self.settings['work'][
            'safe_frame'] = self.options_window.safe_frame_checkbox_w.isChecked(
            )
        self.settings['work'][
            'center_frame'] = self.options_window.center_frame_checkbox_w.isChecked(
            )
        self.settings['work'][
            'wireframe_shaded'] = self.options_window.wireframe_on_shaded_w.isChecked(
            )
        self.settings['playblast'][
            'safe_frame'] = self.options_window.safe_frame_checkbox_p.isChecked(
            )
        self.settings['playblast'][
            'center_frame'] = self.options_window.center_frame_checkbox_p.isChecked(
            )
        self.settings['playblast'][
            'wireframe_shaded'] = self.options_window.wireframe_on_shaded_p.isChecked(
            )

        palyblast_resolution = self.options_window.resolution_combobox_p.currentText(
        ).split('x')
        palyblast_resolution = map(lambda x: int(x), palyblast_resolution)

        self.settings['playblast']['resolution'] = palyblast_resolution
        self.saveSize()

        self.setSettings(self.settings["work"])
        #        self.setAdvanced(self.settings["work"])
        self.saveSettings()
        self.options_window.hide()

    def grabSettings(self):
        """
        Считывает и запоминает настройки вьюпорта.
        """
        settings = {'model_editor': {}}

        for item in MODEL_EDITOR_SETTINGS:
            queue = {'q': True, item: True}
            try:
                res = cmds.modelEditor(self.modelPanelName, **queue)
                res = res if isinstance(res, bool) or isinstance(
                    res, unicode) else None
                settings['model_editor'][item] = res
            except Exception:
                pass

        safe_frame = cmds.camera(
            self.camera_name, q=True, displaySafeAction=True)
        center_frame = cmds.camera(
            self.camera_name, q=True, displayFilmPivot=True)

        settings['safe_frame'] = safe_frame
        settings['center_frame'] = center_frame
        settings['wireframe_shaded'] = settings['model_editor']['wireframeOnShaded'] #!!!!!! раньше это не сканировалось - поле не записывалось.(evm)

        return settings

    def loadSettings(self):
        """
        Загружает настройки из JSON файла
        """
        import stat
        if not os.path.exists(SETTINGS_JSON):
            if not os.path.exists(MAYA_APP_DIR):
                os.makedirs(MAYA_APP_DIR)
            shutil.copy(DEFAULT_SETTINGS_JSON, SETTINGS_JSON)
            os.chmod(SETTINGS_JSON, stat.S_IWRITE)

        with open(SETTINGS_JSON, 'r') as fp:
            self.settings = json.load(fp)

        with open(DEFAULT_SETTINGS_JSON, 'r') as fp:
            self._default_settings = json.load(fp)
        self.saveSettings()

    def saveSettings(self):
        """
        Сохраняет настройки в JSON файл
        """
        with open(SETTINGS_JSON, 'w+') as fp:
            json.dump(self.settings, fp, indent=4)

    def setSettings(self, settings):
        """
        Устанавливает настройки вьюпорта
        """
        for item in settings['model_editor']:
            if item not in MODEL_EDITOR_SETTINGS: continue
            if settings['model_editor'][item] == None: continue
            edit = {'e': True, str(item): settings['model_editor'][item]}
            try:
                cmds.modelEditor(self.modelPanelName, **edit)
            except Exception:
                pass

        cmds.camera(
            self.camera_name, e=True, displaySafeAction=settings['safe_frame'])
        cmds.camera(
            self.camera_name,
            e=True,
            displayFilmPivot=settings['center_frame'])
        if 'wireframe_shaded' in settings:
            if settings['wireframe_shaded']:
                maya.mel.eval("setWireframeOnShadedOption true %s;" %
                              self.playblast_viewport)
            else:
                maya.mel.eval("setWireframeOnShadedOption false %s;" %
                              self.playblast_viewport)

    def setAdvanced(self, settings):
        self.resize(settings['resolution'][0], settings['resolution'][1])
        self.move(settings['position'][0], settings['position'][1])

    def grabWorkSettings(self):
        """
        Считывает настройки вьюпорта
        и сохраняет их как рабочие
        """
        res = self.grabSettings()
        self.settings['work'] = res
        self.savePos()
        self.saveSize()
        self.options_window.safe_frame_checkbox_w.setChecked(res['safe_frame'])
        self.options_window.center_frame_checkbox_w.setChecked(
            res['center_frame'])
        self.options_window.wireframe_on_shaded_w.setChecked(
            res['wireframe_shaded'])

    def grabPlayblastSettings(self):
        """
        Считывает настройки вьюпорта
        и сохраняет их для плейбластов
        """
        res = self.grabSettings()
        self.settings['playblast'] = res
        self.options_window.safe_frame_checkbox_p.setChecked(res['safe_frame'])
        self.options_window.center_frame_checkbox_p.setChecked(
            res['center_frame'])
        self.options_window.wireframe_on_shaded_p.setChecked(
            res['wireframe_shaded'])

    def setDefaultWorkSettings(self):
        """
        Устанавливает рабочие настройки вьюпорта по умолчанию
        """
        res = self._default_settings['work']
        self.settings['work'] = res

        self.options_window.safe_frame_checkbox_w.setChecked(res['safe_frame'])
        self.options_window.center_frame_checkbox_w.setChecked(
            res['center_frame'])
        self.options_window.wireframe_on_shaded_w.setChecked(
            res['wireframe_shaded'])

    def setDefaultPlayblastSettings(self):
        """
        Устанавливает настройки вьюпорта для плейбласта по умолчанию
        """
        res = self._default_settings['playblast']
        self.settings['playblast'] = res

        self.options_window.safe_frame_checkbox_p.setChecked(res['safe_frame'])
        self.options_window.center_frame_checkbox_p.setChecked(
            res['center_frame'])
        self.options_window.wireframe_on_shaded_p.setChecked(
            res['wireframe_shaded'])
        self.options_window.resolution_combobox_p.setCurrentIndex(1)

    def showEvent(self, event):
        """
        При открытие окна устанавливает рабочие настройки вьюпорта
        """
        super(ShotView, self).showEvent(event)

        # maya can lag in how it repaints UI. Force it to repaint
        # when we show the window.
        self.modelPanel.repaint()
        self.setSettings(self.settings["work"])
        self.setAdvanced(self.settings["work"])
        # self.changeSize(resolution)
        # print self.size()

    def closeEvent(self, event):
        """
        При закрытии окна удаляет вьюпорт
        """
        self.savePos()
        self.saveSize()

        self.saveSettings()
        cmds.deleteUI(self.playblast_viewport, panel=True)
        self.deleteLater()
        event.accept()

    def showErrorMessage(self, text):
        """
        Показывает окно об ошибки с заданым текстов.
        """
        msg = QMessageBox(self)
        msg.setWindowTitle('Error!')
        msg.setIcon(QMessageBox.Critical)
        msg.setText('<p align="center">%s</p>' % text)
        msg.show()

    def launchRV(self, media):
        """
        Routine to communicate with RV

        : param versions    Is list of media to be loaded into RV.
                            Can contain of strings and lists of strings if media has layers.
        """
        # need clean PYTHONPATH to RV to find all it's modules
        child_env = dict(os.environ.items() + [('PYTHONPATH', '')])

        import base64, json

        # delegate further processing to handler
        arguments = {"media": media}
        arguments_string = base64.b64encode(json.dumps(arguments))
        cmd = [
            RVPUSH, '-tag', 'playblast', 'py-eval-return',
            'execfile("%s", {"arguments":"%s"})' % (
                RV_HANDLER_PATH, arguments_string)
        ]
        # print(' '.join(cmd))
        call = subprocess.call(cmd, env=child_env)


def show():
    # get a pointer to the maya main window
    ptr = mui.MQtUtil.mainWindow()
    # use sip to wrap the pointer into a QObject
    win = shiboken.wrapInstance(long(ptr), QWidget)
    d = ShotView(win)
    d.show()

    return d


def run():
    global shot_view_window
    global msg

    if cmds.file(q=True, sn=True):
        # try:
        #     shot_view_window.deleteLater()
        #     del shot_view_window
        # except:
        #     pass
        shot_view_window = show()
    else:
        msg = QMessageBox()
        msg.setWindowTitle('Error!')
        msg.setIcon(QMessageBox.Critical)
        msg.setText('<p align="center">Please open scene!</p>')
        msg.show()
