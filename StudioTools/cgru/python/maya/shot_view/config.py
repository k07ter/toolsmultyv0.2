# -*- coding: utf-8 -*-
import os
import sys

CUR_DIR = os.path.dirname(os.path.abspath(__file__))

# HEAD_IN_FIELD = "sg_head_in"
# TAIL_OUT_FIELD = "sg_tail_out"
# TIMECODE_FIELD = "sg_timecode_2"
# ANIMATION_STEPS = [
#     "ANIM",
#     "BLK",
#     "LAY",
#     "CAM",
# ]
# OUT_STEP_NAME = "OUT"

HEAD_IN_FIELD = "sg_frame_from"
TAIL_OUT_FIELD = "sg_frame_to"
TIMECODE_FIELD = "sg_timecode_25"
ANIMATION_STEPS = [
    "comp",
    "anim",
    "layout",
    "cut",
]
OUT_STEP_NAME = "comp"


RV_HANDLER_PATH = os.path.join(CUR_DIR, 'rv_handler.py')


if sys.platform == 'win32':
    RVPUSH = r'C:\tools\RV\bin\rvpush.exe'
    if not os.path.isfile(RVPUSH):
        RVPUSH = r'C:\Program files\RV\bin\rvpush.exe'

    FFMPEG = os.path.join(CUR_DIR, 'bin/win32/ffmpeg/ffmpeg.exe')
    RV_HANDLER_PATH = RV_HANDLER_PATH.replace('\\', '\\\\')
elif sys.platform == 'darwin':
    raise RuntimeError(
        'Path to rvpush and ffmpeg does not provided for this platform')
elif sys.platform == 'linux2':
    raise RuntimeError(
        'Path to rvpush and ffmpeg does not provided for this platform')
else:
    raise RuntimeError(
        'Path to rvpush and ffmpeg does not provided for this platform')

