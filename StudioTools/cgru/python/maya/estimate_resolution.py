#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2018.
# Coder: EMelkov

import os
import maya.api.OpenMaya as om
import maya.api.OpenMayaUI as omu
import pymel.core as pm

from math import sqrt
from core.python.utils import timeit

try:
    # for 2015 maya
    from PySide.QtGui import QDialog, QVBoxLayout, QWidget, QPushButton, QCheckBox, QLabel, QMenuBar, QMenu
    import shiboken
except ImportError:
    # for 2017 maya
    from PySide2.QtWidgets import QDialog, QVBoxLayout, QWidget, QPushButton, QCheckBox, QLabel, QMenuBar, QMenu
    import shiboken2 as shiboken


"""
from core.python.maya import estimate_resolution as txr
reload(txr)
txr.show()
"""


def error_dialog(message):
    pm.confirmDialog(title='ERROR      ',
                     message=message,
                     button=['Ok'],
                     cancelButton='Ok')


class EstimateTexture(QDialog):
    def __init__(self, parent, **kwargs):
        super(EstimateTexture, self).__init__(parent, **kwargs)
        self.parent = parent
        self.setWindowTitle("Estimate Resolution")
        # self.setWindowFlags(Qt.WindowCloseButtonHint |
        #                     self.windowFlags())
        self.layout = QVBoxLayout(self)

        # HELP
        # self.menuBar = QMenuBar(self)
        #
        # self.fileMenu = QMenu("&Help", self)
        # self.menuBar.addMenu(self.fileMenu)
        #
        # self.fileMenu.addAction("&Help...", self.open_wiki, "Ctrl+H")
        # self.layout.addWidget(self.menuBar)

        self.btn_help = QPushButton()
        self.btn_help.setFixedSize(40, 15)
        self.btn_help.setText('help')

        self.layout.addWidget(self.btn_help)
        self.btn_help.pressed.connect(self.open_wiki)



        self.ch_visibile  = QCheckBox('Faces in viewport')
        self.layout.addWidget(self.ch_visibile)

        self.label = QLabel()
        self.label.setText("Model must have UVs (simple automap)")
        self.layout.addWidget(self.label)

        self.btn_inspect = QPushButton()
        self.btn_inspect.setText('Calculate')

        self.layout.addWidget(self.btn_inspect)
        self.btn_inspect.pressed.connect(self.inspect)

        self.label_result = QLabel()
        self.label_result.setText("Resolution: ")
        self.layout.addWidget(self.label_result)

    def inspect(self):
        est = estimate(only_vis=self.ch_visibile.isChecked())
        res = est[1]
        self.label_result.setText("Resolution: %s x %s" % (res, res))

    def open_wiki(self):
        os.startfile('https://wiki.parovoz.tv/index.php/Estimate_resolution')


def show():
    try:
        import maya.OpenMayaUI as omui
        def mayaMainWindow():
            return shiboken.wrapInstance(long(omui.MQtUtil.mainWindow()), QWidget)
        dlg = EstimateTexture(parent=mayaMainWindow())
        dlg.show()
    except Exception as ex:
        print(ex)

def area2d(points):
    v0 = points[0]
    for i in range(1, 4):
        for j in range(2):
            points[i][j] = points[i][j] - v0[j]
    s = 0.5 * ( abs(points[1][0] * points[2][1] - points[1][1] * points[2][0]) +
                abs(points[3][0] * points[2][1] - points[3][1] * points[2][0]))
    return s



def get_low_limit_res(areas_list):
    if not areas_list:
        error_dialog('Check mapping. Incorrect UVs')
        return -1, -1
    d = [sqrt(i[0]/i[1]) for i in areas_list]
    print max(d)
    low_limit_res = int(max(d))
    n = 0
    while 2**n < low_limit_res:
        n += 1
        assert n < 100, 'ERROR: Max iteration: 2**n'
    recom = 2**n
    return low_limit_res, recom

def estimate(only_vis=True, debug_select=True):
    print ('.....')
    active_editor = pm.playblast(activeEditor=1)
    camera_tr = pm.modelEditor(active_editor, q=1, camera=1)
    camera_shp = camera_tr.getShape()
    portview = omu.M3dView.active3dView()
    portview_width = portview.portWidth()
    #portview_height = portview.portHeight()
    default_resolution = pm.PyNode('defaultResolution')
    render_width = default_resolution.width.get()
    #render_height = default_resolution.height.get()

    area_scale = (float(render_width)/portview_width)**2
    print 'port width = ', portview_width
    print 'render width = ', render_width
    print 'area scale factor = ', area_scale

    # setup viewport:
    camera_shp.setFilmFit('horizontalFilmFit')
    camera_shp.setDisplayResolution(True)
    camera_shp.setDisplayGateMask(True)
    camera_shp.overscan.set(1.)

    selection = pm.ls(sl=1, fl=1)
    areas_list = []
    selected_faces = False
    if not selection:
        error_dialog('Nothing selected')
        return
    elif type(selection[0]) == pm.nodetypes.Transform:
        try:
            meshes = map(lambda x: x.getShape(), selection)
            for m in meshes:
                if type(m) != pm.nodetypes.Mesh:
                    error_dialog('Select only gemetry')
                    return
        except Exception:
            error_dialog('Select faces or polymesh transforms')
            return


    elif type(selection[0]) == pm.MeshFace:
        meshes = [pm.listRelatives(selection[0], p=1)[0]]
        selected_faces = True
    else:
        error_dialog('Select faces or polymesh transforms')
        return
    debug_select_faces = []
    for mesh in meshes:
        print 'Mesh: %s' % mesh
        if selected_faces:
            print('Selected face mode..')
            iterate = selection
        else:
            print('Selected geo mode..')
            iterate = mesh.faces

        for f in iterate:
            points3d = f.getPoints(space='world')
            points3d_cnt = len(points3d)

            if points3d_cnt != 4:
                continue

            if only_vis:
                points2d = [p[:-1] for p in map(lambda x: portview.worldToView(om.MPoint(x)), points3d) if p[-1]]
                if len(points2d) != 4:
                    continue
            else:
                points2d = [p[:-1] for p in map(lambda x: portview.worldToView(om.MPoint(x)), points3d)]

            port_area = area2d(points2d) * area_scale
            #print 'Area: ', port_area, 'Side: ', sqrt(port_area)
            if port_area <= 1e-6: # non-convex
                continue
            uv_area = f.getUVArea()
            if uv_area <= 1e-6:
                continue
            # port-0
            #lines.append('%s %s\n' % (port_area, uv_area))
            if debug_select:
                debug_select_faces.append(f)
            areas_list.append([port_area, uv_area])

    low_limit = get_low_limit_res(areas_list)
    print 'Min/Recomended texture res : ', low_limit

    if debug_select:
        pm.select(debug_select_faces, r=1)
    return low_limit


