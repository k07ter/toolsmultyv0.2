#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2018.
# Coder: EMelkov

import os
import pymel.core as pm
from core.python.FileVersion import get_correct_version
from core.python.EntitySettings import EntitySettings

def save_correct_version(entsett=None):
    if entsett is None:
        path = os.path.normcase(pm.system.sceneName())
        entsett = EntitySettings(path, os.environ['PRVZ_TOOLS_PATH'])

    if not entsett.path:
        pm.confirmDialog(title='ERROR      ',
                         message='File is not saved',
                         button=['Bye'],
                         cancelButton='Bye')
        return

    correct_version = get_correct_version(entsett).path
    print ('correct_version %s' % correct_version)
    if not correct_version == entsett.path:
        result = pm.confirmDialog(title='Check version', message='Save correct version and publish: \n%s ?' % os.path.basename(correct_version),
                                  button=['Yes', 'No'], defaultButton='Yes',
                                    cancelButton='No', dismissString='No')
        if result == 'No':
            return
        try:
            assert not os.path.isfile(correct_version), 'Alarm! File %s exists!' % correct_version
            pm.system.saveAs(correct_version)
            print ('Correct version saved: %s' % correct_version)
            return correct_version
        except Exception as ex:
            print ('ERROR: Not saved: %s' % ex)
            return
    else:
        pm.system.saveFile()
        print ('Current version saved: %s' % entsett.path)
        return entsett.path


def restart_sg_toolkit():
    import os
    import sgtk

    try:
        # Kill Current Engine
        engine = sgtk.platform.current_engine()
        engine.destroy()
    except:
        pass

    mgr = sgtk.bootstrap.ToolkitManager()
    mgr.base_configuration = "sgtk:descriptor:app_store?name=tk-config-basic"
    e = mgr.bootstrap_engine("tk-maya", entity={"type": "Project", "id": int(os.getenv('PRVZ_PROJECT_ID'))})