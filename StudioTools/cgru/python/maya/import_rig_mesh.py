#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2018.
# Coder: EMelkov


# logger:
import logging
from core.python import logs as lg
reload(lg)
logger = lg.get_logger(__name__)
logger.setLevel(logging.ERROR)
#logger.setLevel(logging.DEBUG)

import pymel.core as pm
import random
import os
from core.python.EntitySettings import EntitySettings
from parovozUVsnapShot import findAsset_SG_MAT

def get_random_color(low=0.3, high=1.0):
    return [random.uniform(low, high), random.uniform(low, high), random.uniform(low, high), 1]

def get_downstream_meshes(transforms, shapes_storage=None):
    if shapes_storage is None:
        shapes_storage = []

    for tr in transforms:
        dowstr_sh = pm.listRelatives(tr, fullPath=True, shapes=1, noIntermediate=1)
        dowstr_tr = pm.listRelatives(tr, fullPath=True, type='transform', noIntermediate=1)
        shapes_storage += dowstr_sh
        if dowstr_tr:
            get_downstream_meshes(dowstr_tr, shapes_storage=shapes_storage)
    return shapes_storage

# def duplacate_assigned_shadersOLD(mesh_node):
#     '''
#     Old version - just duplicate
#     Args:
#         mesh_node:
#
#     Returns:
#
#     '''
#     transform = mesh_node.getParent()
#     print 'transform', transform
#     sgs_ref = mesh_node.shadingGroups()
#     for sg_ref in sgs_ref:
#         if 'initialShadingGroup' in str(sg_ref): #initialParticleSE
#             continue
#         mbrs = sg_ref.members()
#         mbrs_filtered = [m for m in mbrs if not m.namespace()]
#         print ('Members:', mbrs_filtered)
#
#
#         new_sh_nwork = pm.duplicate(sg_ref, upstreamNodes=True)
#         #new_sh_nwork.addMembers(mbrsf)
#         print 'new_sh_nwork SG:', new_sh_nwork
#         new_shg = [n for n in new_sh_nwork if n.type() == 'shadingEngine'][0]
#         print 'New SG:', new_shg
#         #new_shg.addMembers(mbrsf)
#         for mbr in mbrs_filtered:
#             pm.sets(new_shg, forceElement=mbr)

def duplacate_assigned_shaders(mesh_nodes, asset_prefix=None):
    '''
    Args:
        mesh_node:

    Returns:
         referenceFile()

    referenceQuery -file Return the reference file to which this object belongs. None if object is not referenced
    '''

    shading_groups_to_duplicate = []
    for mesh_node in mesh_nodes:
        shading_groups_to_duplicate += mesh_node.shadingGroups()
    shading_groups_to_duplicate = list(set(shading_groups_to_duplicate))

    for sg_ref in shading_groups_to_duplicate:
        if 'initialShadingGroup' in str(sg_ref): #initialParticleSE
            continue
        mbrs = sg_ref.members()
        mbrs_filtered = [m for m in mbrs if not m.namespace()]
        logger.debug ('Members: %s' % mbrs_filtered)
        new_sh_nwork = pm.duplicate(sg_ref, upstreamNodes=True)
        # dict for new shader:
        shader = {'sg': None, 'lambert': None}

        # clean up new shader:
        for node in new_sh_nwork:
            if node.type() == 'shadingEngine':
                shader['sg'] = node
            elif node.type() == 'lambert':
                shader['lambert'] = node
            else:
                try:
                    node.unlock()
                    pm.delete(node)
                except Exception as err:
                    logger.debug ('EX: %s' % err)

        logger.debug ('clean_shader SG: %s' % shader)

        if asset_prefix is not None:
            #  get data like this (u'alyonka_fairy', u'//alpha/patrol/0_assets/chars/basic/alyonka_fairy/3d')
            source_asset_data = findAsset_SG_MAT(shader['lambert'])
            source_asset_name = source_asset_data[0]

            sg_new_name = shader['sg'].name().replace(source_asset_name, asset_prefix, 1)
            lambert_new_name = shader['lambert'].name().replace(source_asset_name, asset_prefix, 1)
            shader['sg'].rename(sg_new_name)
            shader['lambert'].rename(lambert_new_name)

        logger.debug ('New SG: %s' % shader)

        for mbr in mbrs_filtered:
            pm.sets(shader['sg'], forceElement=mbr)

        # set color:
        shader['lambert'].setColor(get_random_color())


def zero_envelope_inputs(node, depth=5):
    for n in node.inputs():
        try:
            n.envelope.set(0)
        except Exception as ex:
            print(ex)
        if depth > 0:
            depth = depth - 1
            return zero_envelope_inputs(n, depth=depth)
        else:
            return

def unlock_downstream_transforms(root):
    kids = pm.PyNode(root).getChildren()
    kids = [k for k in kids if k.type() == 'transform']
    if kids:
        # unlock all
        for node in kids:
            try:
                for attr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz','sx', 'sy', 'sz', 'v']:
                    node.attr(attr).unlock()
            except Exception as ex:
                print ('Unlock exeption: %s' % ex)
            unlock_downstream_transforms(node)



def start(remove_deform=True, group=True):
    tools = os.environ['PRVZ_TOOLS_PATH']
    path = os.path.normcase(pm.system.sceneName())
    if not path:
        pm.confirmDialog(title='ERROR      ',
                         message='File is not saved',
                         button=['Bye'],
                         cancelButton='Bye')
        return

    es = EntitySettings(path, tools)
    asset_prefix = es.data['asset_name']

    if  es.task!='model' and es.stage!='work':
        pm.confirmDialog(title='Incorrect file',
                         message='File must be "modelling & work"',
                         button=['Bye'],
                         cancelButton='Bye')
        return
    #---------------
    transforms = pm.ls(sl=1, et='transform')
    meshes_to_duplicate = get_downstream_meshes(transforms)
    for mesh in meshes_to_duplicate:
        logger.debug ('Mesh for detransform %s' % mesh)
        if remove_deform:
            zero_envelope_inputs(mesh)

    duplicated_trs = []
    for tr in transforms:
        try: # Возникла странная проблема при дублировании одной группы.
            duplicated_tr = pm.duplicate(tr)
            logger.debug ('duplicated_tr %s' % duplicated_tr)
            duplicated_trs += duplicated_tr
        except Exception as ex:
            logger.exception(ex)

    duplicated_tr_free = pm.parent(duplicated_trs, world=1)
    group_name = '|IMPORTED_MESH'
    if group:
        if pm.uniqueObjExists(group_name):
            pm.parent(duplicated_trs, pm.PyNode(group_name))
        else:
            pm.group(duplicated_trs, name=group_name)

    duplicated_meshes = get_downstream_meshes(duplicated_tr_free)
    duplacate_assigned_shaders(duplicated_meshes, asset_prefix=asset_prefix)
    unlock_downstream_transforms(pm.PyNode(group_name))
