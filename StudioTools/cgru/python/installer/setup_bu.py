#coding: utf-8
import os, sys, urllib2, subprocess
import yaml

sys.path.append(r'\\loco000\tools\tools30d\core\python')
sys.path.append(r'\\loco000\tools\tools30d\utilities\prvz_thread')
sys.path.append(r'\\loco000\tools\tools30d\utilities\batch_man')
sys.path.append(r'\\loco000\tools\tools\cgru-windows\afanasy\python')
#sys.path.append(r'\\loco000\tools\tools30d\core\python\moho')
sys.path.append(r'\\loco000\tools\toolsMultyV0.2\StudioTools\cgru\lib\python')
#import PySide

#from sandbox.gui2 import *
#from batch_man import batch_man
#import batch_man as bm
import thread_widget as tw

class SetupShell(tw.MainWindow):

	def __init__(self, req_soft=None):
		if req_soft and os.path.isfile(req_soft):
			try:
				self.req_soft = yaml.load(file(req_soft))
			except:
				print(u'Некорректная конфигурация!')
				return
		else:
			self.req_soft = {
				'git': ['git', 'https://git'],
				'':'',
				'parocloud': ['parocloud', 'https://parocloud']
            }
		self._makeGUI()

	def _install_each(self, soft_name):
		_location = self.req_soft[soft_name]
		if not os.path.isfile(_location[0]):
			print (u'Не найден инсталлятор ПО. Запуск скачивания инсталлятора ...\n')
			#dist = urllib2.urlopen(_location[1]).read()
			#with open(_location[0], 'wb') as dl:
			#	dl.write(dist)
			#soft_location = request.get(soft_location[1])
		else:
			print (u'Запуск установки ...\n')
			#subprocess.Popen('cmd.exe %s' % -_location[0])
		print(soft_name, _location)

	def _makeGUI(self, parent=None):
		self.app = tw.QtGui.QApplication([])
		super(SetupShell, self).__init__(parent)
		main_widget = self.setupUI('_')
		#self.setCentralWidget(main_widget)
		print(self.__dict__)
		self.show()

	def setup(self):
		for soft in self.req_soft:
			if soft and self.req_soft[soft]:
				self._install_each(soft)
		sys.exit(self.app.exec_())

#class setupUI(tw.MainWidget):

	def setupUI(self, command, return_code = 0, parent=None):
		#self.mw = tw.MainWidget()
		#super(setupUI, self).__init__(parent)

		self.setWindowTitle('Software installation ...')
		self.setObjectName('Parovoz Software')

		self.app_dir = os.path.dirname(__file__).replace('\\', '/')
		#self.command = command
		self.menuBar = tw.QtGui.QMenuBar(self)

		#Источники софта
		self.src_soft_lb = tw.QtGui.QLabel('Расположение дистрибутива: ')
		self.src_soft = tw.QtGui.QComboBox()
		self.src_soft.addItem('localhost')
		self.src_soft.addItem('web')

		# HELP
		helpMenu = tw.QtGui.QMenu("&Help", self)
		helpMenu.addAction("&Help...", self.open_wiki, "Ctrl+H")
		self.menuBar.addMenu(helpMenu)

		#SOFT
		softMenu = tw.QtGui.QMenu("&Soft", self)
		softMenu.addAction("&Open config", self.load_soft_list, "Ctrl+L")
		self.menuBar.addMenu(softMenu)

		self.installButton = tw.QtGui.QPushButton('Install', self)

		#if 'layout' not in self.__dict__:
		#self.layout = tw.QtGui.QVBoxLayout(self)
		#self.layout().removeWidget(self.menuBar)

		print(self.menuBar.__dict__)
		vbox = tw.QtGui.QVBoxLayout(self)
		vbox.addWidget(self.menuBar)
		vbox.addWidget(self.src_soft_lb)
		vbox.addWidget(self.src_soft)
		vbox.addWidget(self.installButton)

		self.grid_lo = tw.QtGui.QGridLayout(self)
		self.grid_lo.addLayout(vbox, 0, 0)

		# Display WIdget
	 	#self.label = tw.QtGui.QLabel()
		#self.set_status()
		#self.layout.addWidget(self.label)

		# Text Widget
		#self.setLayout(self.layout)
		#self.text = tw.QtGui.QTextEdit(self)
		#self.layout.addWidget(self.text)

		# Start Button
		#if 'layout' in self.__dict__:
		#	if 'addWidget' in self.layout.__dict__:
		#		print('Добавление кнопки')
		#self.layout.addWidget(startButton)

		# self.run_event()
		#self.text.setText('')
		#self.text.append('Press Start Button!\n Following command will be executed:\n %s' % command)
		#self.start_button.clicked.connect(self.run_event)
		self.resize(500, 300)

	def set_status(self, status='press_start_button'):

		def set_image(image):
			self.label.setPixmap(image)

		def play_movie(movie):
			self.label.clear()
			self.label.setMovie(movie)
			movie.start()

		if status == 'press_start_button':
			set_image(tw.QtGui.QPixmap(r'%s/press_start_button.png' % self.app_dir))
		elif status == 'in_progress':
			play_movie(tw.QtGui.QMovie(r'%s/what_is_love_v001.gif' % self.app_dir))
		elif status == 'completed':
			set_image(tw.QtGui.QPixmap(r'%s/respect_plus.png' % self.app_dir))
			tw.QtGui.QSound(r'%s/prvz_thread_sound.wav' % self.app_dir).play()
			self.text.append('\nCompleted!')
		elif status == 'wasted':
			set_image(tw.QtGui.QPixmap(r'%s/wasted.png' % self.app_dir))
			self.text.append('\nError!!! See log above for details!')

	def open_wiki(self):
		#os.startfile('%s/Render_anim2d_context_menu' % '---')
		print('---')

	def load_soft_list(self):
		#os.startfile('%s/Render_anim2d_context_menu' % '===')
		print( '===')

if __name__ == '__main__':
	ss = SetupShell()
	ss.setup()