#coding: utf-8
import os, sys, urllib2
import subprocess as sp
from threading import Thread
from Queue import Queue
import yaml, requests, shutil, urllib2
from PySide import QtGui, QtCore

class setupUI(QtGui.QWidget):

	def __init__(self, command, return_code = 0, parent=None):
		super(setupUI, self).__init__(parent)
		self.app_dir = os.path.dirname(__file__).replace('\\', '/')
		#self.command = command
		self.mnu_items = {
			'Help': self.open_wiki,
			'Soft': self.load_soft_list
			}
		self.title = [u'Дистрибутив: ', u'Статус: ']
		self.build()

	def build(self):
		#
		# Метод построения содержимого формы
		#
		self.menuBar = QtGui.QMenuBar(self)

		# HELP
		self.helpMenu = QtGui.QMenu("&Help", self)
		self.helpMenu.addAction("&Help...", self.open_wiki, "Ctrl+H")
		self.menuBar.addMenu(self.helpMenu)

		#SOFT
		self.softMenu = QtGui.QMenu("&Soft", self)
		self.softMenu.addAction("&Open config", self.load_soft_list, "Ctrl+L")
		self.menuBar.addMenu(self.softMenu)
		self.menuBar.setGeometry(QtCore.QRect(0, 0, 450, 20))

		#self.vbox = QtGui.QVBoxLayout(self)

		#Источники софта
		self.src_soft_ttl = QtGui.QLabel(u'Дистрибутив:')
		self.src_soft_ttl.setFixedHeight(20)
		self.sft_lst = QtGui.QWidget(self)

		self.sft_lst.lw = QtGui.QListWidget(self.sft_lst)
		self.sft_lst.lw.clicked.connect(self.show_settings)
		self.sft_lst.lw.setFixedHeight(4 * 20)
		self.sft_lst.lw.setFixedWidth(420)

		sft_count = self.parent().req_soft.keys()
		for itm in sft_count:
			if not itm: continue
			_itm = QtGui.QListWidgetItem(itm.capitalize())
			#_itm = QtGui.QListWidgetItem(itm, True)
			#_itm = QtGui.QCheckBox(QtGui.QListWidgetItem(itm))
			_itm.setFlags(QtCore.Qt.ItemIsUserCheckable |
						  QtCore.Qt.ItemIsEnabled)
			_itm.setCheckState(QtCore.Qt.Unchecked)
			self.sft_lst.lw.addItem(_itm)

		self.gr_box = QtGui.QGroupBox(u"Выбор расположения:")
		self.gr_box.setFixedHeight(120)
		#Geometry(QtCore.QRect(10, 10, 50, 60))
		self.r_box1 = QtGui.QRadioButton(u"Расположение на диске")
		self.r_box1.clicked.connect(self.enab_loc)
		self.loc_path = QtGui.QLineEdit()
		#self.r_box1.setObjectName("localePath")
		self.r_box2 = QtGui.QRadioButton(u"Адрес для скачивания")
		self.web_link = QtGui.QLineEdit()
		self.r_box2.clicked.connect(self.enab_link)
		#self.r_box2.setObjectName("webLink")
		self.r_box1.setChecked(True)
		self.web_link.setEnabled(False)

		self.gr_box2 = QtGui.QGroupBox(u"Статус установки:")
		self.gr_box2.setFixedHeight(80)
		#setGeometry(QtCore.QRect(30, 20, 50, 60))
		self.current_step = QtGui.QLabel(u'Текущая операция:')
		self.current_step.setFixedHeight(15)
		self.current_stat = QtGui.QLabel('')
		self.current_stat.setFixedHeight(15)
		self.progress = QtGui.QProgressBar(self)
		#QLabel(u'Выполнено: ') # % (self.prc, self.ld_sz))
		self.progress.setValue(0)

		self.vchk = QtGui.QVBoxLayout(self.gr_box)
		self.vchk.addWidget(self.r_box1)
		self.vchk.addWidget(self.loc_path)
		self.vchk.addWidget(self.r_box2)
		self.vchk.addWidget(self.web_link)

		self.vchk2 = QtGui.QVBoxLayout(self.gr_box2)
		self.vchk2.addWidget(self.current_step)
		self.vchk2.addWidget(self.current_stat)
		self.vchk2.addWidget(self.progress)

		#Привязка методов класса к кнопкам
		self.installButton = QtGui.QPushButton('Install')
		self.installButton.clicked.connect(self.parent().setup)
		self.closeButton = QtGui.QPushButton('Close')
		self.closeButton.clicked.connect(QtCore.QCoreApplication.instance().quit)

		self.grid_lo = QtGui.QGridLayout(self)
		#print(dir(self.grid_lo.layout()))
		#self.grid_lo.setFixedHeight(500)
		self.grid_lo.setContentsMargins(10,20,10,10)
		#self.grid_lo.setCellStretch(1,2)
		#self.grid_lo.columnCount().
		#self.grid_lo.setHorizontalSpacing(10)
		#.addWidget(self.menuBar, 0, 0)
		#self.grid_lo.addWidget(QtGui.QLabel(), 0, 0)

		self.grid_lo.addWidget(self.src_soft_ttl, 0, 0, 1, 2)
		#self.grid_lo.addWidget(self.src_soft_loc, 1, 1)
		self.grid_lo.addWidget(self.sft_lst, 1, 0, 1, 2)
		#self.grid_lo.addWidget(self.treeWdg, 2, 1)
		self.grid_lo.addWidget(self.gr_box, 2, 0, 1, 2)
		#self.grid_lo.addWidget(self.src_soft, 3, 1)
		#self.grid_lo.addWidget(QtGui.QLabel(), 4, 0)
		self.grid_lo.addWidget(self.gr_box2, 3, 0, 1, 2)
		#self.grid_lo.addWidget(self.src_soft, 3, 1)
		#self.grid_lo.addWidget(QtGui.QLabel(), 6, 0)
		#self.grid_lo.addWidget(QtGui.QLabel(), 4, 0)
		#self.grid_lo.addLayout(self.hbox, 0, 0)
		self.grid_lo.addWidget(self.installButton, 4, 0)
		self.grid_lo.addWidget(self.closeButton, 4, 1)
		#self.grid_lo.addWidget(QtGui.QLabel(), 6, 0)

		#self.grid_lo.addWidget(self.hbox2, 0, 0)
		#self.setLayout(self.vbox)
		#self.setGeometry(100,100, 200,150)

	def enab_loc(self):
		self.loc_path.setEnabled(True)
		self.web_link.setEnabled(False)
		self.parent().req_soft[self.sft_lst.lw.currentItem().text().lower()]['mode'] = 0

	def enab_link(self):
		self.loc_path.setEnabled(False)
		self.web_link.setEnabled(True)
		self.parent().req_soft[self.sft_lst.lw.currentItem().text().lower()]['mode'] = 1

	#def show_install_status(self, data):
	#	self.progress.setText(data)

	def set_status(self, status='press_start_button'):

		def set_image(image):
			self.label.setPixmap(image)

		def play_movie(movie):
			self.label.clear()
			self.label.setMovie(movie)
			movie.start()

		#if status == 'press_start_button':
		#	set_image(QtGui.QPixmap(r'%s/press_start_button.png' % self.app_dir))
		#elif status == 'in_progress':
		#	play_movie(QtGui.QMovie(r'%s/what_is_love_v001.gif' % self.app_dir))
		#elif status == 'completed':
		#	set_image(QtGui.QPixmap(r'%s/respect_plus.png' % self.app_dir))
		#	QtGui.QSound(r'%s/prvz_thread_sound.wav' % self.app_dir).play()
		#	self.text.append('\nCompleted!')
		#elif status == 'wasted':
		#	set_image(QtGui.QPixmap(r'%s/wasted.png' % self.app_dir))
		#	self.text.append('\nError!!! See log above for details!')

	def open_wiki(self):
		#os.startfile('%s/Render_anim2d_context_menu' % '---')
		print('---')

	def show_settings(self):
		#print(dir(self.sft_lst.lw.currentItem()))
		_currItem = self.sft_lst.lw.currentItem().text().lower()
		try:
			info = u"Пути не заданы!"
			sftConfig = self.parent().req_soft.get(_currItem)
			if sftConfig:
				info = u"Ошибка установки путей."
				if self.parent().req_soft[_currItem]['mode']:
					self.r_box2.setChecked(True)
				else:
					self.r_box1.setChecked(True)

				self.loc_path.setText(sftConfig.get('dist_path'))
				self.web_link.setText(sftConfig.get('web_link'))
				#ci = self.sft_lst.lw.currentItem()
				# print(ci.checkState() == 'Checked')
		except:
			print(info)

	def load_soft_list(self):
		#os.startfile('%s/Render_anim2d_context_menu' % '===')
		print( '===')

class SetupShell(QtGui.QMainWindow):
	block_size = 32

	def __init__(self, req_soft=None, bs=None):
		if req_soft and os.path.isfile(req_soft):
			try:
				self.req_soft = yaml.load(file(req_soft))
			except:
				print(u'Некорректная конфигурация!')
				return
		else:
			self.req_soft = {
				'git': {
					'dist_path': 'C:\\Git.exe',
					'web_link': 'localhost://git.exe',
					'mode': 0
				},
				'':{
					'dist_path': 'labuda',
					'mode': 0
				}
            }
		if bs: self.block_size = bs
		self._makeGUI()

	def _makeGUI(self, parent=None):
		self.app = QtGui.QApplication([])
		#pThread = QtCore.QThread(target=self.app)
		#pThread.finished.connect(self.app.exit)
		#pThread.start()
		super(SetupShell, self).__init__(parent)
		self.data = 0
		self.file_length = 0
		self.file_name = None
		self.main_widget = setupUI('_', parent=self)
		self.setCentralWidget(self.main_widget)
		self.setWindowTitle('Software installation ...')
		self.setObjectName('ParovozSoftware')
		self.resize(450, 400)
		#self.data = 0
		self.show()
		sys.exit(self.app.exec_())

	def _progress_bar(self, data):
		self.main_widget.progress.setValue(data) #setText(data)
		self.repaint()

	def _loadByLink(self): #, soft_name, web_link):
		#
		# Метод закачки дистрибутива по ссылке
		#
		if not self.soft_loc:
			print(u"Адрес скачивания не указан: %s." % self.soft_loc)
			return

		#self.main_widget.update()
		self.main_widget.setFocus()
		lnk_file = urllib2.urlopen(self.soft_loc)
		info = u"Файл недоступен."
		pref = self.main_widget.current_step.text().split(':')[0]
		self.main_widget.current_step.setText(u'%s: Установка "%s"' % (pref, self.soft_name))

		#self.main_widget.current_step.repaint()
		if lnk_file.getcode() == 200:
			info = u'Ошибка получения данных.'
			file_info = lnk_file.headers
			if file_info:
				self.file_length = int(file_info['content-length'])
				file_name = os.path.basename(self.soft_loc)

				if self.file_length:
					info = u"Файл '%s' найден. Размер: %d." % (file_name, self.file_length)
					if os.path.exists(file_name):
						if self.file_length == os.stat(file_name).st_size:
							print(u"Файл уже скачан.")
							return file_name
			print(info)
			DOZE = self.block_size * 1024

			info = u'Подготовка к скачиванию "%s" ' % self.soft_name
			print(info)
			sys.stdout.write(info + "...")

			with open(file_name, "wb") as rcv:
					data = lnk_file.read(DOZE)
					self.data = len(data)
					#complete_mask = u'Выполнено: [ %3.f%% / %.2f M ]'
					while data:
								rcv.write(data)
								if len(data) < DOZE: rcv.flush()
								#os.stat(rcv.name).st_size
								ldd_data =  self.data * 100/self.file_length
								#from_mask = complete_mask % (ldd_data, file_length/1024^2)
								#self.main_widget.current_stat.setText(u'%s %s ( %d B / %d B ) ...' %
								#									  (info, file_name, self.data, file_length))
								self.main_widget.progress.setValue(ldd_data)
								data = lnk_file.read(DOZE)
								self.data += len(data)

			if os.stat(file_name).st_size == int(self.file_length):
				info = u' выполнено.'
				self.file_name = file_name
			else:
				info = u' не выполнено.'
				file_name = ''

		# shutil.copyfileobj(lnk.raw, rcv)
		print(info)
		#return file_name

	def open_file(self):
		try:
			filename = QtGui.QFileDialog.getOpenFileName(None, 'Open File', os.getenv('HOME'))
			self.loc_path.text = filename
			#		with open(filename[0], 'r') as f:
			#			file_text = f.read()
		except:
			pass

	def install_each(self, soft_name, soft_loc):
		#
		# Метод установки дистрибутива
		#
		#self.connect(self.thr, QtCore.SIGNAL("finished()"), self, QtCore.SLOT("threadFinished()"))
		print (u'Запуск установки "%s" из "%s" ...\n' % (self.soft_name, self.soft_loc))
		self.soft_name = soft_name
		self.soft_loc = soft_loc
		proc = sp.Popen(self.soft_loc, shell=True) #, stdout=sp.PIPE, stderr=sp.PIPE)
		proc.communicate()
		#self.currThr.run()
		#print(self.thr.res)
		return proc.res

	def setup(self):

		class instThr():
			#
			# Класс создания потока, в котором выполняется скачивание файла
			#
			data = 0

			def __init__(self, mw, func=None, parent=None): #, soft_name=None, soft_loc=None):
				#self.parent = parent
				self.soft_name = parent.soft_name
				self.soft_loc = parent.soft_loc
				self.mw = mw
				self.thr_proc = Thread(target=func)
				self.thr_proc.setName(parent.soft_name)
				#self.t = Thread(target=func)
				#self.t.setName(parent.soft_name)
				#print(self.t.__dict__)
				#print(dir(self.t._Thread__target))
				#print(dir(self.t._Thread__kwargs))
				#print(self.t._Thread__target())
				#print(self.t.name)
				#print(dir(self.t._note))
				#print(self.t._Thread__block.Condition__lock, self.t._Verbose__verbose)
				#pref = mw.current_step.text().split(':')[0]
				#mw.current_step.setText(u'%s: Установка "%s"' % (pref, self.soft_name))
				self.res = None

			def start(self):
				#QtCore.QThread.run()
				import time
				#print (u'Запуск установки "%s" из "%s" ...\n' % (self.soft_name, self.soft_loc))
				pref = self.mw.current_step.text().split(':')[0]
				self.mw.current_step.setText(u'%s: Установка "%s"' % (pref, self.soft_name))

				#if self.parent.file_length:
				#self.t.daemon = True
				#print(u'Выполнено: %s' % self.parent.file_length)
				self.thr_proc.run()
				#while self.is_alive:
						#if not self.parent.res is None:
						#	self.parent.filename = self.soft_name
						#else:
						#	self.parent.filename = ''
						#print(u'Ошибка скачивания.')
						#self.t.yield
						#self.mw.current_stat.setText(u'%s %s ( %d B / %d B ) ...' % self.parent.data)
						#self.mw.current_step.setText(u'%s: Установка "%s"' % (pref, self.soft_name))
						#info = u"Скачивание дистрибутива '%s' " % self.soft_name
						##ldd_data = self.parent.data * 100 / self.parent.file_length
						#self.mw.current_stat.setText(u'%s %s ( %d B / %d B ) ...' %
						#									  (info, self.parent.file_name, os.stat(self.parent.file_name).st_size, self.parent.file_length))
						##self.mw.progress.setValue(ldd_data)
						#print(self.t.name)
						#time.sleep(3)
						#self.mw.gr_box2.repaint()

				#print(self.t.__dict__)
				#self.path = os.path.basename(self.soft_loc)
				#proc = sp.Popen(self.soft_loc, shell=True) #, stdout=sp.PIPE, stderr=sp.PIPE)
				#proc.communicate()
				#self.res = self.parent.res
				#_path = self._loadByLink(soft_name, _pths.get('web_link'))

		cnt_i = 0
		#for si in range(self.main_widget.sft_lst.lw.count()):
		soft_itm = self.main_widget.sft_lst.lw
		sft_count = soft_itm.count()
		self.queue = Queue()

		while cnt_i < sft_count:
			soft_itm = self.main_widget.sft_lst.lw.item(cnt_i)
			if soft_itm.checkState() == QtCore.Qt.Checked:
				soft_name = soft_itm.text().lower()
				_pths = self.req_soft[soft_name]
				if _pths.get('mode'):
					self.soft_name = soft_name
					self.soft_loc = _pths.get('web_link')
					self.currThr = instThr(self.main_widget, self._loadByLink, parent=self)
					#self.queue.put(self.currThr)
					#self.currThr = instThr(target=self._loadByLink(soft_name, _pths.get('web_link')))
					self.currThr.start()
					#_path = self._loadByLink(soft_name, _pths.get('web_link'))
					#if not self.file_name is None:
					_path = self.file_name if self.file_name else ''
				else:
					_path = _pths.get('dist_path')

				try:
					if os.path.isfile(_path):
						#self.currThr = instThr(self.main_widget, self.self.install_each, parent=self)
						# self.currThr = instThr(target=self._loadByLink(soft_name, _pths.get('web_link')))
						#self.currThr.run()
						res = self.install_each(soft_name, _path)
						if res:
							print(u"Установка прервана.")
						else:
							print(u"Установка выпонена.")
				except:
					print(u'Ошибка установки дистрибутива!')

			#soft_itm = self.main_widget.sft_lst.lw.pop()
			cnt_i += 1

if __name__ == '__main__':
	config_name = os.path.basename(__file__).split('.')[0]
	ss = SetupShell(req_soft='%s.yml' % config_name)
