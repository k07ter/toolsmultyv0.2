#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


# logger:
# from core.python import logs as lg
# reload(lg)
# logger = lg.get_logger(__name__)
# from core.python.maya.debug_set import CHECKER_LOG_LEVEL
# logger.setLevel(CHECKER_LOG_LEVEL)


class ZeroXformMayaChecker(MayaChecker):
    freeze_matrix = pm.datatypes.Matrix([[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
    zero_pivots = (pm.datatypes.Vector([0.0, 0.0, 0.0]), pm.datatypes.Vector([0.0, 0.0, 0.0]))

    def __init__(self, path, tools):
        super(ZeroXformMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Zero transforms'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        excluded_xforms = self.args['exclude']
        include_refs = self.args['include_refs']
        all_transforms = pm.ls(et='transform')

        for t in all_transforms:
            if not include_refs and ':' in t.name():
                continue
            no_namespace_tr = t.nodeName().split(':')[-1]
            if no_namespace_tr not in excluded_xforms:
                matrix = t.getMatrix()
                pivots = t.getPivots()
                if matrix != self.freeze_matrix:
                #if matrix != self.freeze_matrix or pivots != self.zero_pivots:
                    if not len(self.errors):
                        #self.add_error('ERROR - Not freezed transformations found or pivots not in origin:')
                        self.add_error('ERROR - Not freezed transformations found:')
                    self.problem_nodes.add(t)
                    self.add_error('"%s"' % t.nodeName())
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Freeze selected transforms.' % self.name)
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
