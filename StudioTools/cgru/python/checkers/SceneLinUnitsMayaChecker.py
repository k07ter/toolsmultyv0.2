#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class SceneLinUnitsMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(SceneLinUnitsMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Scene linear units'

    def _test(self):
        correct_lin_units = self.args['lin_units']
        current_lin_units = pm.currentUnit( query=True, linear=True )

        if correct_lin_units != current_lin_units:
            self.add_error('ERROR - Incorrect units.')
            self.add_error('Units is "%s". Must be "%s"' % (current_lin_units.upper(), correct_lin_units.upper()))

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            correct_lin_unit = self.args['lin_units']
            pm.currentUnit(linear=correct_lin_unit)
            self.add_info ('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)
        # DO IT!!!!
        #self._test()

