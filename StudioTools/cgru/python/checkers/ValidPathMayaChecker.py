#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import glob
import os

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker
from core.python.utils_file import convert_path_absolute, relative_path_is_valid


class ValidPathMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(ValidPathMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Valid paths'
        self.attrs_dict = {'file': 'fileTextureName',
                           'audio':'filename',
                           'imagePlane': 'imageName',
                           'psdFileTex':'fileTextureName'
                           }
        self.problem_nodes = set()

    def _test(self):
        self.problem_nodes = set()
        include_refs = self.args['include_refs']

        all_nodes = pm.ls(et=self.attrs_dict.keys())
        for node in all_nodes:
            if not include_refs and ':' in node.name():
                continue
            attr = pm.Attribute('%s.%s' % (node.name(), self.attrs_dict[node.type()]))
            unresolved_path = attr.get()

            if not unresolved_path:
                if not len(self.errors):
                    self.add_error('ERROR - Path is not valid:')
                self.problem_nodes.add(node)
                self.add_error('Empty file path: %s' % node.name())
                continue

            print ('unresolved_path === ', unresolved_path)
            if not relative_path_is_valid(unresolved_path, os.environ['PRVZ_PROJECT_PATH']):
                if not len(self.errors):
                    self.add_error('ERROR - Path is not valid:')
                self.problem_nodes.add(node)
                self.add_error('Path is not relative or file has not been found: %s: "%s"' % (node.name(), unresolved_path))
                continue

            resolved_path = convert_path_absolute(unresolved_path, os.environ['PRVZ_PROJECT_PATH'], relative_var='%root%', verbose=False)
            print 'resolved_path:', resolved_path
            # Check if file path is sequence:
            if '.<f>.' in resolved_path:
                resolved_path_mask = resolved_path.replace('.<f>.', '.*.')
                found = glob.glob(resolved_path_mask)
                if not found:
                    self.add_error('ERROR - Path is not valid:')
                    self.problem_nodes.add(node)
                    self.add_error('Sequence not exists in node: %s: "%s"' % (node.name(), unresolved_path))
            else:
                if not os.path.isfile(resolved_path):
                    if not len(self.errors):
                        self.add_error('ERROR - Path is not valid:')
                    self.problem_nodes.add(node)
                    self.add_error('File not exists in node: %s: "%s"' %(node.name(), unresolved_path))

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))

