#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class UnknownNodesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(UnknownNodesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Unknown nodes'
        self.problem_nodes = set()
    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        unknown_nodes = pm.ls(type='unknown')
        if unknown_nodes:
            for node in unknown_nodes:
                if not include_refs and ':' in node.name():
                    continue
                self.problem_nodes.add(node)

        if self.problem_nodes:
            self.add_error('ERROR - Found unknown nodes:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        #print '>>', self.data_set
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            #print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
