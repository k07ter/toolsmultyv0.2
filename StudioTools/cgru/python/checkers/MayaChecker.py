#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import pymel.core as pm

from core.python.checkers.BaseChecker import BaseChecker
from core.python.utils import npath


class MayaChecker(BaseChecker):
    def __init__(self, path, tools):
        print 'PRINT===========', type(self),' ==== ' ,self.__class__
        super(MayaChecker, self).__init__()
        #assert isinstance(entsett, EntitySettings), 'Argument entsett must be <EntitySettings> type'
        self.path = npath(path)
        self.tools = npath(tools)
        self.soft = 'maya'
        self.problem_nodes = set()
        self.pretty_name = ''

    def _action(self):
        """
        Переопределение функции постпроверки.

        Returns: None

        """
        self.problem_nodes = set()
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))



def testMayaChecker():
    os.environ['PRVZ_TOOLS_PATH'] = os.path.normpath('y:/tools28d/')
    _TOOLS = os.environ['PRVZ_TOOLS_PATH']
    path = r'\\AlPha\grandadventure\2_prod\ep004\sh0101\work\ep004_sh0101_anim_v002.ma'
    ch = MayaChecker()


if __name__ == '__main__':
    testMayaChecker()