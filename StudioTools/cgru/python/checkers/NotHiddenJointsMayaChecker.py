#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class NotHiddenJointsMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(NotHiddenJointsMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Not hidden joints'
        self.problem_nodes = set()
    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        joints = pm.ls(type='joint')
        if joints:
            for node in joints:
                if not include_refs and node.isReferenced():
                    continue
                if node.drawStyle.get()!=2:
                    self.problem_nodes.add(node)

        if self.problem_nodes:
            self.add_error('ERROR - Joints with incorrect drawing style in the scene:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        #print '>>', self.data_set
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            for node in self.problem_nodes:
                node.drawStyle.set(2)
            self.add_info('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)
