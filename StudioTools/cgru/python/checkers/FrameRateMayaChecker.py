#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class FrameRateMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(FrameRateMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Frame rate'

    def _test(self):
        correct_time_unit = self.args['fps']
        current_time_unit = pm.currentUnit(q=1, time=1)

        if correct_time_unit != current_time_unit:
            self.add_error('ERROR - Incorrect frame rate:')
            self.add_error('Frame rate is "%s". Must be "%s"' % (current_time_unit.upper(), correct_time_unit.upper()))

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            correct_time_unit = self.args['fps']
            pm.currentUnit(time=correct_time_unit)
            self.add_info ('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)
        # DO IT!!!!
        #self._test()

