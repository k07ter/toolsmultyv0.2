#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2018.
# Coder: EMelkov


import pymel.core as pm
from core.python.checkers.MayaChecker import MayaChecker


class ArnoldAttrsOnShapesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(ArnoldAttrsOnShapesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Arnold attributes on curve shapes'

        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        nurbs_curve_shapes = pm.ls(et='nurbsCurve')

        for curve_shape in nurbs_curve_shapes:
            if not include_refs and curve_shape.isReferenced():
                continue
            else:
                for a in self.args['ai_attributes']:
                    if curve_shape.hasAttr(a) and (not curve_shape.attr(a).isLocked()) and curve_shape.attr(a).isKeyable():
                        self.problem_nodes.add(curve_shape)
        if self.problem_nodes:
            self.add_error('ERROR - Curve shapes has unlocked and keyable arnold attrs:')
            for node_name in self.problem_nodes:
                self.add_error(node_name)

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            for curve_shape in self.problem_nodes:
                for a in self.args['ai_attributes']:
                    curve_shape.attr(a).set(l=1, k=0)
            self.add_info('%s: correction finished' % self.name)
            self.set_state(True)

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
