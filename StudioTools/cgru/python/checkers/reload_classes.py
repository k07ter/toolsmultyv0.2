#!/usr/bin/python
# -*- coding: utf-8 -*-

try:
    from core.python.checkers import ParentMasterGrpsIkMayaChecker as mod_01
    reload(mod_01)

    from core.python.checkers import ArnoldAttrsOnShapesMayaChecker as mod_02
    reload(mod_02)

    from core.python.checkers import DisplayHandlesMayaChecker as mod_03
    reload(mod_03)

    from core.python.checkers import NotHiddenJointsMayaChecker as mod_04
    reload(mod_04)

    from core.python.checkers import ControlSetsMayaChecker as mod_05
    reload(mod_05)

    from core.python.checkers import ReferencesMayaChecker as mod_06
    reload(mod_06)

    from core.python.checkers import ValidPathMayaChecker as mod_07
    reload(mod_07)

    from core.python.checkers import SoundsMayaChecker as mod_08
    reload(mod_08)

except Exception as err:
    print ('Reload failed.. ')
print ('Successful reload checkers.')