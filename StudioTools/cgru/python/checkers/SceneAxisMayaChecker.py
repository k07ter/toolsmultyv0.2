#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class SceneAxisMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(SceneAxisMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Scene orientation'
    def _test(self):
        correct_up_axis = self.args['up_axis']
        current_up_axis = pm.Env().getUpAxis()

        if correct_up_axis != current_up_axis:
            self.add_error('ERROR - Incorrect up axis.')
            self.add_error('Up axis is "%s". Must be "%s"' % (current_up_axis, correct_up_axis))

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            correct_time_unit = self.args['up_axis']
            pm.Env().setUpAxis(correct_time_unit)
            self.add_info ('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)
        # DO IT!!!!
        #self._test()

