#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import maya.OpenMaya as om

from core.python.checkers.MayaChecker import MayaChecker


class ZeroEdgesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(ZeroEdgesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Zero edges'
        self.problem_selection = om.MSelectionList()

    def _test(self):
        self.problem_selection.clear()
        include_refs = self.args['include_refs']
        dutil = om.MScriptUtil()
        dptr = dutil.asDoublePtr()
        itermesh = om.MItDependencyNodes(om.MFn.kMesh)
        while not itermesh.isDone():
            #print itermesh.item()
            mesh_dag_path = om.MDagPath.getAPathTo(itermesh.item())
            if not include_refs and (':' in str(mesh_dag_path.fullPathName())):
                itermesh.next()
                continue
            iteredge = om.MItMeshEdge(itermesh.thisNode())
            while not iteredge.isDone():
                iteredge.getLength(dptr, om.MSpace.kObject)
                d = dutil.getDouble(dptr)
                if d < 0.00001:
                    print ('Zero length edge found: %s : %s' % (iteredge.index(), d))
                    edge = iteredge.currentItem()
                    if not len(self.errors):
                        self.add_error('ERROR - Zero length edes found:')
                    self.problem_selection.add(mesh_dag_path, edge)
                iteredge.next()
            itermesh.next()
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Edges selected.' % self.name)
            om.MGlobal.setActiveSelectionList(self.problem_selection)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
