#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm
import re
from core.python.checkers.MayaChecker import MayaChecker



class MaterialsNamingMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(MaterialsNamingMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'SGs and materials naming'
        self.problem_nodes = set()
    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        mat_rex = r'.*_shdrtex_(256|512|1024|2048|4096|8192)(resSG|res)$'
        ignoring = ['initialParticleSE', 'initialShadingGroup', 'lambert1']
        include_refs = self.args['include_refs']
        all_sgs = [s for s in pm.ls(type='shadingEngine')]
        all_mats = [m for m in pm.ls(type='lambert')]
        all_nodes = all_sgs + all_mats

        for node in all_nodes:
            if not include_refs and node.isReferenced():
                continue
            node_name = node.nodeName(stripNamespace=True)
            if node_name in ignoring:
                continue

            if re.match(mat_rex, node_name):
                print (node_name)
                continue
            self.problem_nodes.add(node)

        if self.problem_nodes:
            self.add_error('ERROR - Incorrect shading nodes names in the scene:')
            for node in self.problem_nodes:
                self.add_error(node.name())

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            pm.select(self.problem_nodes, replace=True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
