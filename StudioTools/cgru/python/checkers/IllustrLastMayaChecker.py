#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import re

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker
from core.python.utils_file import convert_path_absolute, get_latest, convert_path_relative_var


def illustrUpdate():
    #print ('\n----- illustrUpdate ----')
    illustr_meshes = pm.ls('*_illustr*', et='mesh')
    if illustr_meshes:
        for i_mesh in illustr_meshes:
            shgs = i_mesh.listConnections( type='shadingEngine')
            if not shgs:
                #print ('ERROR:Not found illustr material for %s' % i_mesh)
                continue

            filenodes = shgs[0].listHistory(type='file', pruneDagObjects=True)
            if not filenodes:
                #print ('ERROR:Not found file node for %s' % shgs[0])
                continue

            texture_path_current = filenodes[0].fileTextureName.get()
            texture_path_current_abs = convert_path_absolute(texture_path_current)

            if not texture_path_current_abs.endswith('.png'):
                #print ('ERROR: Not "png" asigned  %s' % shgs[0])
                continue

            texure_path_mask = re.sub('v\d+\.', 'v???.', texture_path_current_abs)
            texture_path_last = get_latest(texure_path_mask)

            #texture_path_m
            if texture_path_current_abs!=texture_path_last:
                print ('>> INFO: Found last version "%s". Replacing old "%s"...' % (texture_path_last, texture_path_current))

                # CORRECT
                texture_path_last_rel = convert_path_relative_var(texture_path_last)
                filenodes[0].fileTextureName.set(texture_path_last_rel)

            else:
                #filenodes[0].fileTextureName.set(mut.convert_paths_relative_project(texture_path_current_abs))
                print ('>> INFO: version "%s" is up-to-date ...' % (texture_path_last))


class IllustrLastMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(IllustrLastMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Illustrations'
        self.problem_nodes = set()
        self.to_update = dict()

    def _test(self):
        self.problem_nodes = set()

        illustr_meshes = pm.ls('*_illustr*', et='mesh')
        if illustr_meshes:
            for i_mesh in illustr_meshes:
                shgs = i_mesh.listConnections(type='shadingEngine')
                if not shgs:
                    # print ('ERROR:Not found illustr material for %s' % i_mesh)
                    continue
                filenodes = shgs[0].listHistory(type='file', pruneDagObjects=True)
                if not filenodes:
                    # print ('ERROR:Not found file node for %s' % shgs[0])
                    continue
                texture_path_current = filenodes[0].fileTextureName.get()
                texture_path_current_abs = convert_path_absolute(texture_path_current, os.environ['PRVZ_PROJECT_PATH'])

                if not texture_path_current_abs.endswith('.png'):
                    # print ('ERROR: Not "png" asigned  %s' % shgs[0])
                    continue
                texure_path_mask = re.sub('v\d+\.', 'v???.', texture_path_current_abs)
                texture_path_last = get_latest(texure_path_mask)

                if texture_path_current_abs != texture_path_last:
                    if not len(self.errors):
                        self.add_error('ERROR - Found newest version of illustrations:')
                    self.add_error(' New version found: "%s".' % texture_path_last)

                    #print ('>> INFO: Found last version "%s". Replacing old "%s"...' % (texture_path_last, texture_path_current))
                    self.problem_nodes.add(filenodes[0])
                    self.to_update[filenodes[0]] = texture_path_last

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            for filenode in self.to_update:
                texture_path_last_rel = convert_path_relative_var(self.to_update[filenode],  os.environ['PRVZ_PROJECT_PATH'])
                filenode.fileTextureName.set(texture_path_last_rel)
            self.add_info ('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)
