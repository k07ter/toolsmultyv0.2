#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import os

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker
from core.python.utilities.shotgun import ShotgunBuddy


class FrameRangeMayaChecker(MayaChecker):

    def __init__(self, path, tools):
        super(FrameRangeMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Frame range'
        self.sg = ShotgunBuddy()
        self.prj_id = int(os.environ['PRVZ_PROJECT_ID'])

    def _test(self):
        sg_data = self.sg.get_frame_range(self.prj_id, pm.system.sceneName())
        got_value_start = sg_data['sg_cut_in']
        got_value_end = sg_data['sg_cut_out']
        # Max :
        if got_value_start is None and got_value_end is None:
            self.set_state(True)
            return

        if got_value_start is None or got_value_end is None:
            self.set_state(False)
            self.add_error('ERROR - Start or end of the animation range is unknown. See Shotgun data.')
            return

        self.correct_start = int(got_value_start)
        self.correct_end = int(got_value_end)

        # If plakat!
        if int(got_value_start) == int(got_value_end):
            self.correct_end = self.correct_start + 1

        # skip if not set:
        current_start_anim = int(pm.playbackOptions(q=1, animationStartTime=1))
        current_end_anim = int(pm.playbackOptions(q=1, animationEndTime=1))

        current_min = int(pm.playbackOptions(q=1, minTime=1))
        current_max = int(pm.playbackOptions(q=1, maxTime=1))



        if (current_start_anim != self.correct_start) or (current_end_anim != self.correct_end):
            self.add_error('ERROR - Incorrect frame range:')
            self.add_error('Frame range is [%s] - [%s]. Must be [%s] - [%s]' % (current_start_anim, current_end_anim, self.correct_start, self.correct_end))

        if (current_min != self.correct_start) or (current_max != self.correct_end):
            self.add_error('ERROR - Incorrect min/max time:')
            self.add_error('Time range is [%s] - [%s]. Must be [%s] - [%s]' % (current_min, current_max, self.correct_start, self.correct_end))

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            pm.playbackOptions(animationStartTime=self.correct_start, animationEndTime=self.correct_end, minTime=self.correct_start, maxTime=self.correct_end)
            self.add_info ('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)

