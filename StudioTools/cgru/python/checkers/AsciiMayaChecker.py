#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker
from core.python.utils import is_ascii


class AsciiMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(AsciiMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Not valid names'
        self.problem_nodes = set()
    def _test(self):
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        sel = pm.ls()
        for node in sel:
            if not include_refs and ':' in node.name():
                continue
            if not is_ascii(node.name()):
                self.problem_nodes.add(node)

        if self.problem_nodes:
            self.add_error('ERROR - Not ASCII names found:')
            #pym.select(incorrect_nodes, r=True)
            for node in self.problem_nodes:
                self.add_error(node.name())


        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            pm.select(self.problem_nodes, replace=True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
