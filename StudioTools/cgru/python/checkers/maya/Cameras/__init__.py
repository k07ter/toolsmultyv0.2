#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class CamerasMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(CamerasMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Cameras'
    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        excluded_cameras = self.args['exclude']
        must_be_cameras = self.args['must_be']
        lock_cameras = self.args['lock']
        attributes = self.args['attributes']
        current_cameras_sh = [cam_sh for cam_sh in pm.ls(et='camera')]
        current_cameras_tr = [cam_sh.getParent() for cam_sh in current_cameras_sh]
        #no_namespace_cams_tr = [cam.nodeName(stripNamespace=True) for cam in current_cameras_tr]
        no_namespace_cams_tr = []
        for cam_tr in current_cameras_tr:
            if not include_refs and cam_tr.isReferenced():
                continue
            no_namespace = cam_tr.nodeName(stripNamespace=True)
            no_namespace_cams_tr.append(no_namespace)
            # "exclude": ["*"] - all incorrect camera names are ignored
            if (no_namespace not in excluded_cameras) and ('*' not in excluded_cameras):
                if not len(self.errors):
                    self.add_error('ERROR - Not allowed cameras found or necessary cameras not present:')
                self.problem_nodes.add(cam_tr)
                self.add_error('"%s". Must be deleted' % cam_tr)

        for must_be_camera in must_be_cameras:
            if must_be_camera not in no_namespace_cams_tr:
                self.add_error('ERROR - "%s". Must present in scene' % must_be_camera)
                continue
            #checking atributes:
            for a_name in attributes:
                cam_shape = pm.PyNode(must_be_camera).getShape() # must_be_camera -  in the scene !
                delta = float(cam_shape.attr(a_name).get()) - float(attributes[a_name])
                print '#### --- delta', a_name, '---> ', attributes[a_name], delta
                if abs(delta) >= 0.0005:
                    print '#### Error value', a_name, '---> ', attributes[a_name]
                    self.add_error('ERROR - Incorrect attribute value %s = %s  CORRECT: %s ' % (cam_shape.attr(a_name),
                                                                                              cam_shape.attr(a_name).get(),
                                                                                              attributes[a_name]))

                    self.problem_nodes.add(cam_shape)


        for lock_camera in lock_cameras:
            if lock_camera in no_namespace_cams_tr:
                # find index
                ind = no_namespace_cams_tr.index(lock_camera)
                current_cameras_tr[ind].t.lock()
                current_cameras_tr[ind].r.lock()

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Check camera attributes. Delete not allowed cameras or create necessary cameras\n(Press red button to see problems.).' % self.name)
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))


