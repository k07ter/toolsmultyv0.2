#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class AnimatedControlsMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(AnimatedControlsMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Animated controls'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        all_sets = pm.ls(et='objectSet')
        animated_controls = []
        for s in all_sets:
            set_name_wtht_namespace = s.name().split(':')[-1]
            if set_name_wtht_namespace in self.args['check_sets']:
                all_controls = s.flattened()
                for control in all_controls:
                    check_TU_anim = control.inputs(type='animCurveTU')
                    check_TA_anim = control.inputs(type='animCurveTA')
                    check_TL_anim = control.inputs(type='animCurveTL')
                    if check_TU_anim or check_TA_anim or check_TL_anim:
                        self.problem_nodes.add(control)
        if self.problem_nodes:
            self.add_error('ERROR - Found animated controls:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        #print '>>', self.data_set
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            #print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
