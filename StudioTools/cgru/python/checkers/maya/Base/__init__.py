#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import os
print(__file__)
print(os.walk(os.path.realpath(__file__)).next())

checkers = {
	"AnimatedControls": "",
	ArnoldAttrsOnShapes
Ascii
Base
}

# logger:
from core.python import logs as lg
# import core.python.Settings as utls
from core.python.EntitySettings import EntitySettings

reload(lg)
logger = lg.get_logger(__name__)
from core.python.maya.debug_set import CHECKER_LOG_LEVEL
logger.setLevel(CHECKER_LOG_LEVEL)


class BaseChecker(object):
    def __init__(self):
        #self.name = '"Base checker"'
        self.name = self.__class__.__name__
        self.__ok = False
        self.args = dict()
        #self.correct = False
        self.gui = False
        self.errors = ''
        self.info = ''
        #self.problem_nodes = set()
        #self.data_set = set() # some data storage (DAG Nodes for example)
    #def __cleanup_errors(self):

    def __reset(self):
        self.errors = ''
        self.info = ''
        #self.problem_nodes = set()
        self.__ok = False

    def action(self):
        self.__reset()
        self._action()
        return self.__ok

    def test(self):
        self.__reset()
        self._test()
        return self.__ok

    # def run(self):
    #     # clean up errors string!
    #     self.errors = ''
    #     #self.msg('%s: test starts...' % self.name)
    #     logger.debug ('%s: test starts' % self.name)
    #     logger.debug ('%s: args: %s' % (self.name, self.args))
    #
    #     self.testing()
    #
    #     if not self.__ok:
    #         #self.msg('test failed...')
    #         logger.debug ('%s: test failed.' % self.name)
    #         if self.correct:
    #             self.__corrector()
    #         else:
    #             #self.msg('corrector disabled...')
    #             logger.debug ('%s: corrector disabled....' % self.name)
    #     else:
    #         #self.msg('test completed...')
    #         logger.debug ('%s: test completed....' % self.name)
    #         return self.__ok

    # MUST BE REDEFINE!
    def _action(self):
        self.add_error('testing: Test errors1')
        self.add_error('testing: Test errors2')
        self.add_error('testing: Test errors3')
        self.add_info('testing: Test info1')
        self.add_info('testing: Test info2')
        self.add_info('testing: Test info3')


    # MUST BE REDEFINE!
    def _test(self):
        self.add_error('testing: Test errors1')
        self.add_error('testing: Test errors2')
        self.add_error('testing: Test errors3')
        self.add_info('testing: Test info1')
        self.add_info('testing: Test info2')
        self.add_info('testing: Test info3')



    def set_state(self, state):
        self.__ok = state

    # ------------------
    def get_state(self):
        return self.__ok


    def add_error(self, line):
        self.errors += '%s\n' % line

    def add_info(self, line):
        self.info += '%s\n' % line








def testBaseChecker():
    ch = BaseChecker()
    ch.test()
    #ch.print_msg()
    print ch.args

    print ch.info
    print ch.errors



def testMayaChecker():
    os.environ['PRVZ_TOOLS_PATH'] = os.path.normpath('y:/tools28d/')
    _TOOLS = os.environ['PRVZ_TOOLS_PATH']

    path = r'\\AlPha\grandadventure\2_prod\ep004\sh0101\work\ep004_sh0101_anim_v002.ma'
    entsett = EntitySettings(path, _TOOLS)

    #stack = MayaCheckersStack(entsett)
    # #find all configs for checkers:
    # config_mask = '*{0}Checker.json'.format(sett.soft.capitalize())
    # print 'xxx', config_mask
    # print sett.find_all_configs(config_mask)
    #
    #
    # # to find all checkers:
    #
    # # for checker in ....
    # mch = MayaChecker(path, _TOOLS, sett)
    # mch.run()
    # mch.print_msg()
    # print mch.entity_settings.data




if __name__ == '__main__':
    testBaseChecker()
    #testMayaChecker()
