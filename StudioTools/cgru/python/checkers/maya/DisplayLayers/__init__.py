#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class DisplayLayersMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(DisplayLayersMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Display layers'

    def _test(self):
        excluded_display_layers = self.args['exclude']
        include_refs = self.args['include_refs']

        current_display_layers = [dl.getName() for dl in pm.ls(et='displayLayer')]

        for lr in current_display_layers:
            if not include_refs and ':' in lr:
                continue
            no_namespace_lr = lr.split(':')[-1]
            if no_namespace_lr not in excluded_display_layers:
                if not len(self.errors):
                    self.add_error('ERROR - Not allowed display layers found:')
                self.add_error('"%s". Must be deleted' % lr)
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Delete not allowed display layers.' % self.name)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))

