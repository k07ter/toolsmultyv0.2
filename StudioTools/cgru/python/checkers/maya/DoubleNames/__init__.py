#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class DoubleNamesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(DoubleNamesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Double names'
        self.problem_nodes = set()
    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        all_namespaces = pm.namespaceInfo(lon=True, recurse=True) + [':']
        all_namespaces.append(u':')
        exclude_types = self.args['exclude_types']
        include_refs = self.args['include_refs']
        allow_instance = self.args['allow_instance']
        #dbl_names = set()

        for ns in all_namespaces:
            # skip special namespaces
            if ns in ('shared', 'UI'):
                continue
            nodes_in_ns = pm.namespaceInfo(ns, lod=True)
            for node in nodes_in_ns:
                if not include_refs and ':' in node.name():
                    continue
                if node.type() in exclude_types:
                    continue

                # skip or not instances:
                if '|' in node.name():
                    if allow_instance and node.isInstanced():
                        continue
                    self.problem_nodes.add(node)

        if self.problem_nodes:
            self.add_error('ERROR - Found double names:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        #print '>>', self.data_set
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            #print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
