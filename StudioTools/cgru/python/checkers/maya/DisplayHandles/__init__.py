#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2018.
# Coder: EMelkov


import pymel.core as pm
from core.python.checkers.MayaChecker import MayaChecker


class DisplayHandlesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(DisplayHandlesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Display handles'

        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        all_nodes_with_handles = [n for n in pm.ls() if n.hasAttr('displayHandle')]
        for node in all_nodes_with_handles:
            if not include_refs and node.isReferenced():
                continue
            else:
                if node.displayHandle.get():
                    self.problem_nodes.add(node)
        if self.problem_nodes:
            self.add_error('ERROR - Some nodes have display handle turned on:')
            for node_name in self.problem_nodes:
                self.add_error(node_name)

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            for node in self.problem_nodes:
                node.displayHandle.set(0)
            self.add_info('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
