#!/usr/bin/python
# -*- coding: utf-8 -*-

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class DoubleNamespacesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(DoubleNamespacesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Double namespaces'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        refs = pm.listReferences(recursive=True, refNodes=True)
        namespaces = []
        double = []
        for ref in refs:
            ns = ref[1].fullNamespace
            if ns not in namespaces:
                namespaces.append(ns)
            else:
                self.problem_nodes.add(ref[1].refNode)
                double.append(ns)

        if self.problem_nodes:
            self.add_error('ERROR - In references double namespaces found:')
            for n, node in enumerate(self.problem_nodes):
                self.add_error('node: %s  ---> namespace: %s' % (node.name(), double[n]))
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            # print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
