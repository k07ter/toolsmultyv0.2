#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class MeshHistoryMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(MeshHistoryMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Mesh history'
        self.problem_nodes = set()
    def _test(self):
        self.problem_nodes = set()
        include_refs = self.args['include_refs']

        all_meshes = pm.ls(et='mesh')
        for m in all_meshes:
            if not include_refs and ':' in m.name():
                continue
            hist = [n for n in m.history(il=1) if not isinstance(n, pm.nodetypes.GeometryFilter)]

            if len(hist) == 1 and m.name() == hist[0].name():
                continue
            if len(hist) == 2 and hist[1].name() == 'initialShadingGroup':
                continue

            if not len(self.errors):
                self.add_error('ERROR - Not deleted history:')
            self.problem_nodes.add(m)
            self.add_error('"%s"' % m.nodeName())

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            pm.select(self.problem_nodes, replace=True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))

