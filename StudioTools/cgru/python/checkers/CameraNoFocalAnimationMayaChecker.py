#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class CameraNoFocalAnimationMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(CameraNoFocalAnimationMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Camera focal animation'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        all_cameraShapes = pm.ls(et='camera')
        for cam in all_cameraShapes:
            if not include_refs and ':' in cam.name():
                continue

            if cam.getParent().name() in self.args['exclude']:
                continue
            connections = pm.listConnections('%s.focalLength' % cam, destination=True)
            if connections and isinstance(connections[0], pm.nt.AnimCurve):
                self.problem_nodes.add(cam)
        if self.problem_nodes:
            self.add_error('ERROR - Found animated camera focal length:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        #print '>>', self.data_set
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            #print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
