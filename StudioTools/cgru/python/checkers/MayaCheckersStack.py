# -*- coding: utf-8 -*-

import ast
import os

try:
    from PySide import QtGui, QtCore
    from PySide.QtGui import *
    from PySide.QtCore import *
    import shiboken
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets
    from PySide2.QtGui import *
    from PySide2.QtCore import *
    from PySide2.QtWidgets import *
    import shiboken2 as shiboken

# logger:
from core.python import logs as lg
from core.python.utils import json_to_dict, merge_dicts
from core.python.EntitySettings import EntitySettings

reload(lg)
logger = lg.get_logger(__name__)
from core.python.maya.debug_set import CHECKER_LOG_LEVEL
logger.setLevel(CHECKER_LOG_LEVEL)


def get_class(kls):
    print 'kls = ', kls
    parts = kls.split('.')
    module = ".".join(parts[:-1])
    print 'module = ', module
    m = __import__(module)
    print 'm = ', m
    for comp in parts[1:]:
        print 'comp = ', comp
        m = getattr(m, comp)

        print 'new m = ', m
    return m





def resolve_dyn_vars(target_dict, dyn_vars_dict):
    """
        Функция для получения словаря аргументов на базе динамических переменных из settings.dyn_vars
        и аргументов из конфигуратора в config_json.
        Args:
            target_dict: словарь с неразрешенными переменными
            dyn_vars_dict: словарь переменных для разрешения
        Returns:
            dict() - разрешенный словарь
        Examples:
            a = {'path': '<episode>_<shot>_rig.ma'}
            b = {'episode':'ep001', 'shot':'sh0101'}
            print resolve_dyn_vars(a,b)
            >> {'path': 'ep001_0101_rig.ma'}
        """
    result_str = str(target_dict)
    # covert to string:
    # print '...', result_str
    # print '...dyn_vars', dyn_vars
    for dyn_var in dyn_vars_dict:  # dyn_var - is a key
        if '<%s>' % dyn_var in result_str:
            result_str = result_str.replace('<%s>' % dyn_var, dyn_vars_dict[dyn_var])
    if '<' in result_str or '>' in result_str:
        raise Exception('Not all dyn_vars resolved.')
    else:
        return ast.literal_eval(result_str)


class MayaCheckersStack(object):
    def __init__(self, entity_settings):
        assert isinstance(entity_settings, EntitySettings), 'Argument entsett must be <EntitySettings> type'
        self.entity_settings = entity_settings

        logger.debug('entity_settings.path = %s' % entity_settings.path)
        logger.debug('entity_settings.tools = %s' % entity_settings.tools)
        #assert entity_settings.path , 'File is not saved'
        if not entity_settings.path:
            QMessageBox.information(None, "Unable To Publish!", "Path to scene is not defined")
            raise Exception('Path to scene is not defined.')



        self.checkers = []
        self.errors =dict() # dict {checker(str):error(str)}
        checkers_config_mask = '*{0}Checker.json'.format(entity_settings.soft.capitalize())
        all_configs = self.entity_settings.find_all_configs(checkers_config_mask) # dict()  {'MayaChecker.json': 'y:\\tools28d\\settings\\MayaChecker.json'

        # To turn off checker create Off[Soft]Checker.json in folder:
        switch_off = 'Off{0}Checker.json'.format(entity_settings.soft.capitalize())
        if switch_off in all_configs.keys():
            print ()
            return

        for config in all_configs:
            config_path = all_configs[config]
            class_name = os.path.splitext(config)[0]
            print '\nFound config: %s' % config_path
            try:
                checker_args = self.resolve_args(self.entity_settings.dyn_vars, config_path)
                logger.debug('Checker %s args: %s ' % (class_name, checker_args))

            except Exception as ex:
                print 'error: Cannot get args for %s: %s' % (class_name, ex)
                logger.exception(ex)
                continue

            if not checker_args['active']:
                print 'warning: Checker %s not activated.' % class_name
                continue

            try:
                class_fullname = 'core.python.checkers.%s.%s' % (class_name, class_name)
                path = entity_settings.path
                tools = entity_settings.tools
                checker_class = get_class(class_fullname)
                print 'checker class = ', checker_class
                print 'checker class type= ', type(checker_class)
                print 'checker class clas= ', checker_class.__class__
                path = 'xxxx'
                tools = 'zzzzz'
                checker = checker_class(path, tools) # экземпляр класса (баз. MayaChecker)
                #assert isinstance(checker, BaseChecker), 'xxxxxxxxxxxxx'
            except Exception as ex:
                print 'error: Cannot create %s: %s' % (class_name, ex)
                logger.exception(ex)
                continue

            #set attributes for correction and gui:
            #checker.action = checker_args['action']
            #checker.gui = checker_args['gui']

            checker.args = checker_args
            self.checkers.append(checker)
            print 'Checker %s added.' % class_name

    def resolve_args(self, dyn_vars, config_json):
        """
           Функция для получения словаря аргументов на базе динамических переменных из settings.dyn_vars
           и аргументов из конфигуратора в config_json.
           Args:
               dyn_vars:
               config_json:
           Returns:
               dict()

           """

        # get config json dict
        config_json_dict = json_to_dict(config_json)
        task = self.entity_settings.task
        logger.debug('config_json_dict = %s'% config_json_dict)
        args_dict_default = config_json_dict['default']
        # если нет аргументов под таск - нулевые аргументы:
        if task in config_json_dict.keys():
            args_dict_task = config_json_dict[task]
        else:
            args_dict_task = {}

        # аргументы под таск перезаписывают дефолтные или дополняют их
        result = merge_dicts(args_dict_default, args_dict_task)
        # resolving vars
        resolving_vars = resolve_dyn_vars(result, dyn_vars)
        logger.debug('resolving_vars = %s' % resolving_vars)
        return resolving_vars

    def inspect(self):
        print '\n\n ----- Inspecting ----- \n'
        for checker in self.checkers:
            print '\nChecker: [ %s ]' % checker.name
            #print 'Args:', checker.args
            passed = checker.run()
            if passed:
                #print ('OK: %s ' % checker.name)
                print '-- OK'
            else:
                #print ('CHECK FAIL: %s ' % checker.name)
                # #print ('ERRORS:')
                self.errors[checker.name] = checker.errors
                print ' -- ERRORS:'
                print checker.errors

        if  self.errors:
            pass
            # print '[ ERRORS ]'
            # for ch in self.errors:
            #     print '<< %s >>\n%s' % (ch, self.errors[ch])


    def check(self):
        print '\n\n ---- Checkers starting...'
        for checker in self.checkers:
            #print '**** ', checker.name
            #print 'Args:', checker.args
            passed = checker.run()
            if passed:
                print ('OK: %s ' % checker.name)
                continue
            else:
                print ('CHECK FAIL: %s ' % checker.name)
                # #print ('ERRORS:')
                # self.errors[checker.name] = checker.errors
                break

        # if self.errors:
        #     return self.errors
        # else:
        #     return None


"""
Правила:
Каждый чекер реализован в отдельном модуле с именем:
'<Type><Maya>Checker.json'

Должен иметь конфигуратор:
'<Type><Maya>Checker.json'
"""


def testMayaChecker():
    os.environ['PRVZ_TOOLS_PATH'] = os.path.normpath('y:/tools28d/')
    _TOOLS = os.environ['PRVZ_TOOLS_PATH']

    path = r'\\AlPha\grandadventure\2_prod\ep004\sh0101\work\ep004_sh0101_anim_v002.ma'
    entsett = EntitySettings(path, _TOOLS)

    stack = MayaCheckersStack(entsett)
    stack.check()


if __name__ == '__main__':
    #testBaseChecker()
    testMayaChecker()