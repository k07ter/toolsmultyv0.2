#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import re

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class ValidNamespacesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(ValidNamespacesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Valid namespaces'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        refs = pm.listReferences(recursive=True, refNodes=True)
        #namespaces = []
        ivalid = []
        for ref in refs:
            ns = ref[1].namespace # not full!
            file_path = ref[1].path
            namespace_base = os.path.splitext(os.path.basename(file_path))[0]
            rex = r'^%s[0-9]*$' % namespace_base
            print namespace_base, ns, file_path, rex
            if not re.match(rex, ns):
                self.problem_nodes.add(ref[1].refNode)
                ivalid.append(ns)

        if self.problem_nodes:
            self.add_error('ERROR - Found incorrect namespaces:')
            for n, node in enumerate(self.problem_nodes):
                self.add_error('Node %s has incorrect namespace: %s' % (node.name(), ivalid[n]))
                #self.add_error(node.name())
        #print '>>', self.data_set
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            # print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
