#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2018.
# Coder: EMelkov


import pymel.core as pm
from core.python.checkers.MayaChecker import MayaChecker


class ParentMasterGrpsIkMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(ParentMasterGrpsIkMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Parent Master Groups on IK controls'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        ik_controls = self.args['ik_controls']

        for node_name in ik_controls:
            if not include_refs and ':' in node_name:
                continue
            if not pm.objExists('%s_SN' % node_name) and pm.objExists(node_name):
                self.problem_nodes.add(node_name)

        if self.problem_nodes:
            self.add_error('ERROR - zvPM groups don\'t exist on:')
            for node_name in self.problem_nodes:
                self.add_error(node_name)

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            import ZvParentMaster
            for control_name in self.problem_nodes:
                pm.select(control_name, r=1)
                ZvParentMaster.createParentGroups()

            if pm.objExists('transform'):
                pm.PyNode('transform').inheritsTransform.set(1)

            self.add_info('%s: correction finished' % self.name)
            self.set_state(True)

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
