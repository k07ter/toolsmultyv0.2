#!/usr/bin/python
# -*- coding: utf-8 -*-

import pymel.core as pm

import core.python.maya.import_sound as imps
from core.python.checkers.MayaChecker import MayaChecker


class SoundsMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(SoundsMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Sounds'
        self.problem_nodes = set()
    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()

        # # get info about audio
        # layout_path = npath(pm.system.sceneName())
        # es = EntitySettings(layout_path, os.environ['PRVZ_TOOLS_PATH'])
        # sound_mask = es.data['sound_mask']
        # actual_sound_file = get_latest(sound_mask)


        current_sounds = pm.ls(et='audio')
        if not self.args['sounds_allowed']:

            for audio in current_sounds:
                #no_namespace_audio = audio.split(':')[-1]

                if not len(self.errors):
                    self.add_error('ERROR - Not allowed sound nodes found:')
                self.problem_nodes.add(audio)
                self.add_error('"%s". Must be deleted' % audio)
        else:
            #check correct sound:
            if len(current_sounds) > 1:
                #error - many sounds
                for audio in current_sounds:
                    # no_namespace_audio = audio.split(':')[-1]

                    if not len(self.errors):
                        self.add_error('ERROR - Too many sound nodes found:')
                    self.problem_nodes.add(audio)
                    self.add_error('"%s".' % audio)

            if len(current_sounds) == 1 or len(current_sounds) == 0:
                sound = imps.run()
                if sound is None:
                    self.add_info('%s: Warning: Actual sound not found. Not imported.' % self.name)

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            if self.args['sounds_allowed']:
                sound = imps.run()
                if sound is None:
                    self.add_info('%s: Warning: actual sound file not found. Not imported.' % self.name)
                else:
                    self.add_info('%s: imported "%s".' % (self.name, sound))
                current_sounds = pm.ls(et='audio')
                if len(current_sounds) < 2:
                    self.set_state(True)
                else:
                    self.add_info('%s: correction failed: more then one sound. Is it referenced sound?' % (self.name))
                    self.set_state(False)

            else:
                self.add_info('%s: No auto correction. Nodes selected.' % self.name)
                # print self.problem_nodes
                if self.problem_nodes:
                    pm.select(self.problem_nodes, replace=True)
                else:
                    self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)


