#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class SmoothedMeshMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(SmoothedMeshMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Smoothed mesh'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        meshes = pm.ls(et='mesh')
        for mesh in meshes:
            if not include_refs and mesh.isReferenced():
                continue
            if mesh.displaySmoothMesh.get() != 0:
                self.problem_nodes.add(mesh)

        if self.problem_nodes:
            self.add_error('ERROR - Smoothed geometry:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            for mesh in self.problem_nodes:
                mesh.displaySmoothMesh.set(0)
            self.add_info('%s: correction finished' % self.name)
            self.set_state(True)

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
