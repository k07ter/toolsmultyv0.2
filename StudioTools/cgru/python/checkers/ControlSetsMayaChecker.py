#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class ControlSetsMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(ControlSetsMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Control sets'
        self.problem_nodes = set()
    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs'] # Now just dummy. No orders.  ...
        # control_sets - dict =  { set1 : [control1, control2...], set2 : [.....], }
        #control_sets =

        for control_set in  self.args['control_sets']:
            if not pm.objExists(control_set):
                continue
                #self.problem_nodes.add(control_set)
                #self.add_error('Set %s - not found' % control_set)

            for control in self.args['control_sets'][control_set]:
                if not pm.objExists(control):
                    # self.problem_nodes.add(control_set)
                    # self.add_error('Control %s - not found' % control)
                    continue
                if not pm.sets(control_set, isMember=control):
                    self.problem_nodes.add(control)
                    self.problem_nodes.add(control_set)
                    self.add_error('Control %s is not in %s' % (control, control_set))

                # if not include_refs and control.isReferenced():
                #     continue
        if self.problem_nodes:
            self.errors = 'ERROR - Controls and control sets:\n' + self.errors

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            for control_set in self.args['control_sets']:
                for control in self.args['control_sets'][control_set]:
                    if pm.objExists(control_set) and pm.objExists(control):
                        if not pm.sets(control_set, isMember=control):
                            pm.sets(control_set, addElement=control)
                    # else:
                    #     raise Exception('Set "%s" or control "%s" - not found' % (control_set, control))
            self.add_info('%s: correction finished' % self.name)
            self.set_state(True)

        except Exception as err:
            self.add_info('ERROR - %s: correction failed: %s' % (self.name, err))
            self.set_state(False)