#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class IntermediateMeshesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(IntermediateMeshesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Intermediate meshes'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']

        for shape in pm.ls(type='mesh', intermediateObjects=True):
            if not include_refs and ':' in shape.name():
                continue
            self.problem_nodes.add(shape)

        if self.problem_nodes:
            self.add_error('ERROR - Found intermediate shapes:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            pm.delete(self.problem_nodes)
            self.add_info('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
