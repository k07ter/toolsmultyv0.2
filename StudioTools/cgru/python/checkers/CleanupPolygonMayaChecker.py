#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class CleanUpMesh(object):
    pass

class CleanupPolygonMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(CleanupPolygonMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Cleanup polymesh'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        pm.mel.eval('polyCleanupArgList 3 { "1","2","1","0","0","0","0","0","1","1e-005","1","1e-005","1","1e-005","0","1","1" };')
        bugs = pm.ls(sl=1)
        pm.select(cl=1)
        for bug in bugs:
            if not include_refs and ':' in bug:
                continue
            if not len(self.errors):
                self.add_error('ERROR - Incorrect polymesh found:')
            self.add_error('"%s"' % bug)
            self.problem_nodes.add(bug)
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Use cleanup polygons.' % self.name)
            # print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
