#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class NegativeUVMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(NegativeUVMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Negative UV'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        meshes = pm.ls(et='mesh')
        for mesh in meshes:
            if not include_refs and ':' in mesh.name():
                continue

            uv_values = mesh.getUVs()
            if not uv_values[0]:
                continue
            if min(uv_values[0]) < 0.0 or min(uv_values[1]) < 0.0:
                self.problem_nodes.add(mesh)

        if self.problem_nodes:
            self.add_error('ERROR - Found negative uv values:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            # print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
