#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class SampleMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(SampleMayaChecker, self).__init__(path, tools)

        self.problem_nodes = set()
    def _test(self):
        """
            Здесь основной код проверки
        """
        self.problem_nodes = set()
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        """
        Здесь либо селекшн проблемных нод, дибо исправление.
        Returns:

        """
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            pm.select(self.problem_nodes, replace=True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
