#!/usr/bin/python
# -*- coding: utf-8 -*-
import os

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class FileFormatMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(FileFormatMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'File format'
    def _test(self):
        self.set_state(True)
        correct_format = self.args['format']
        current_format = os.path.splitext(pm.system.sceneName())[1]

        if correct_format != current_format:
            self.add_error('ERROR - Incorrect file format:')
            self.add_error('File format is "%s". Must be "%s"' % (current_format, correct_format))

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Save scene in correct format.' % self.name)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))

