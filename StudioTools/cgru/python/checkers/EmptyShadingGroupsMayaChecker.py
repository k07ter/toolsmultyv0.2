#!/usr/bin/python
# -*- coding: utf-8 -*-
import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class EmptyShadingGroupsMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(EmptyShadingGroupsMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Empty shading groups'
        self.problem_nodes = set()
    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']
        all_sgs = [s for s in pm.ls(type='shadingEngine')]

        if all_sgs:
            for node in all_sgs:
                if not include_refs and node.isReferenced():
                    continue
                if pm.sets(node, q=1, size=1) == 0 and node.name() not in ['initialParticleSE', 'initialShadingGroup']:
                    self.problem_nodes.add(node)

        if self.problem_nodes:
            self.add_error('ERROR - Empty shading nodes in the scene:')
            for node in self.problem_nodes:
                self.add_error(node.name())

        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            pm.mel.eval('MLdeleteUnused;')
            self.add_info('%s: correction finished' % self.name)
            self.set_state(True)
        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)
