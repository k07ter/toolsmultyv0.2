#!/usr/bin/python
# -*- coding: utf-8 -*-
# Studio Parovoz. Moscow 2017.
# Coder: EMelkov

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker


class MeshEmptyMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(MeshEmptyMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'Empty mesh'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        include_refs = self.args['include_refs']

        for shape in pm.ls(type='mesh'):
            if not include_refs and ':' in shape.name():
                continue
            if len(shape.vtx) == 0:
                self.problem_nodes.add(shape)

        if self.problem_nodes:
            self.add_error('ERROR - Found empty shapes:')
            for node in self.problem_nodes:
                self.add_error(node.name())
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            self.add_info('%s: No auto correction. Nodes selected.' % self.name)
            # print self.problem_nodes
            if self.problem_nodes:
                pm.select(self.problem_nodes, replace=True)
            else:
                self.add_info('Nothing to select.')

        except Exception as err:
            self.add_error('%s: correction failed: %s' % (self.name, err))
