#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import pymel.core as pm

from core.python.checkers.MayaChecker import MayaChecker
from core.python.utils import is_parovoz
from core.python.utils_file import convert_path_relative_var, relative_path_is_valid


class ReferencesMayaChecker(MayaChecker):
    def __init__(self, path, tools):
        super(ReferencesMayaChecker, self).__init__(path, tools)
        self.pretty_name = 'References'
        self.problem_nodes = set()

    def _test(self):
        # reset problem nodes:
        self.problem_nodes = set()
        ref_nodes = pm.ls(type='reference')
        if ref_nodes:
            for refn in ref_nodes:
                file_ref_node = refn.referenceFile()
                if file_ref_node is None:
                    self.add_info("WARNING: invalid reference node %s has been deleted." % refn)
                    pm.lockNode(refn, lock=False)
                    pm.delete(refn)
                if file_ref_node:
                    unresolved_path = file_ref_node.unresolvedPath()
                    #print unresolved_path
                    if (not relative_path_is_valid(unresolved_path, os.environ['PRVZ_PROJECT_PATH'])) or (not unresolved_path.endswith('_rig.ma')):
                        if not len(self.errors):
                            self.add_error('ERROR - Reference path is not relative "root" or not "rig" type:')
                        self.problem_nodes.add(refn)
                        self.add_error(unresolved_path)
        if self.errors:
            self.set_state(False)
        else:
            self.set_state(True)

    def _action(self):
        try:
            not_rig_refs = set()
            for ref in self.problem_nodes:
                file_ref_node = ref.referenceFile()
                unresolved_path = file_ref_node.unresolvedPath()
                if not unresolved_path.endswith('_rig.ma'):
                    not_rig_refs.add(ref)
                    continue
                # fix paths:
                self.add_info('unresolved_path = %s' % unresolved_path)
                relative_path = convert_path_relative_var(unresolved_path, os.environ['PRVZ_PROJECT_PATH'], use_assets_list=is_parovoz())
                self.add_info('relative_path = %s' % relative_path)
                # print relative_path, unresolved_path
                if relative_path is None:
                    # not resolved
                    not_rig_refs.add(ref)
                    continue
                self.add_info('Start replace with %s' % relative_path)
                f = file_ref_node.replaceWith(relative_path)
                self.add_info('file_ref_node.replaceWith %s' % f)
                '''
                # Warning: Replacing a nested reference file (i.e. a reference which is not a
                // child of the main scene) is a temporary action. The reference to the new
                // file will not be saved with the main scene. To make this action permanent,
                // open the parent file of the reference and replace it there. # 
                '''


            if not_rig_refs:
                self.add_info('No auto correction. Not valid references selected.')
                pm.select(self.problem_nodes, replace=True)
                self.set_state(False)
            else:
                self.add_info('%s: correction finished' % self.name)
                self.set_state(True)

        except Exception as err:
            print err
            self.add_error('%s: correction failed: %s' % (self.name, err))
            self.set_state(False)

        finally:
            #Упорно не хочет пробивать пути во вложенные рефы с первого раза(сыпет варнинги). Поэтому тут эта фигня:
            self._test()
