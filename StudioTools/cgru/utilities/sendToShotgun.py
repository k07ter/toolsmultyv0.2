import sys
import subprocess as sp
import os


def run():
    #print (sys.argv)
    sendProcCmd=''
    movProcCmd=[]
    print ('DO NOT CLOSE THIS WINDOW...')
    user=sys.argv[-1] 
    project=sys.argv[-2]
    movPath=sys.argv[-3]
    sendProcCmd = 'python %s %s %s %s' % (os.environ['CGRU_LOCATION'] + '/plugins/maya/afanasy/previewCmd.py', movPath , project, user)
    movProcCmd = sys.argv[1:-5]
    
    #print (sendProcCmd)
    #print (movProcCmd)
    print ('Moviemaker is starting...')
    childMakeMov = sp.Popen(movProcCmd, stdout=sp.PIPE , shell=True)
    childMakeMov.wait()
    res = childMakeMov.communicate()
    
    ext =''
    for extension in ['mov', 'mp4', 'avi', 'mpeg']:
        if os.path.isfile(movPath.replace('EXT',extension)): 
            ext = extension
            #print ('EXTENSION = %s' % ext)
            sendProcCmd = sendProcCmd.replace('EXT', ext)
            break
    else:
        print ('ERROR: unknown file extension.')
        return 
        
    childSendMov = sp.Popen(sendProcCmd, stdout=sp.PIPE , shell=True)
    s = ' '
    #res = ''
    print ('Attempting to send to Shotgun ...')
    while s:
        s=childSendMov.stdout.readline()
        print (s.rstrip())
    out = input("Press any key or close windows...")  
if __name__ == "__main__":
    run()
    