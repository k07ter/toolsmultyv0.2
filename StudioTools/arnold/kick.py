import os
import os.path
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from helper.EnvManagerV2 import *


def main():
    pipline = PiplineTools()

    maya_version = '2018'

    mtoa_path = pipline.get_mtoa_path(maya_version=maya_version)


    license_path = '\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\maya\\ext\\Yeti\\rlm\\yeti.lic'
    #license_path = r'C:\Yeti-v2.2.1_Maya2018\rlm\yeti.lic'
    #os.environ['peregrinel_LICENSE'] = license_path
    #os.environ['PEREGRINEL_LICENSE'] = license_path
    os.environ['peregrinel_LICENSE'] = r'\\pc999\lic\yeti\yeti_2.0.24.lic'

    print(EnvVar('ARNOLD_PLUGIN_PATH')
          #.appendPath([r'\\\\loco000\\tools\\toolsMulty\\StudioTools\\arnolf\\ext\\windows\\mtoadeploy\\2018'])
          #.appendPath([r'\\\\loco000\\tools\\toolsMulty\\StudioTools\\arnolf\\ext\\windows\\mtoadeploy\\2018\\bin'])
          #.appendPath([r'\\\\loco000\\tools\\toolsMulty\\StudioTools\\arnolf\\ext\\windows\\mtoadeploy\\2018\\plug-ins'])
          #.appendPath([r'\\\\loco000\\tools\\toolsMulty\\StudioTools\\arnolf\\ext\\windows\\mtoadeploy\\2018\\plugins'])
          #.appendPath([r'\\\\loco000\\tools\\toolsMulty\\StudioTools\\arnolf\\ext\\linux\\mtoadeploy\\2018'])
          #.appendPath([r'\\\\loco000\\tools\\toolsMulty\\StudioTools\\arnolf\\ext\\linux\\mtoadeploy\\2018\\bin'])
          #.appendPath([r'\\\\loco000\\tools\\toolsMulty\\StudioTools\\arnolf\\ext\\linux\\mtoadeploy\\2018\\plug-ins'])
          ##.appendPath(['\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\arnold\\ext\\linux\\mtoadeploy\\2018\\plugins'])
          #.appendPath(['\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\maya\\ext\\yeti\\v2.2.1\\linux\\plug-ins'])
          ##.appendPath(['//loco000/tools/toolsMultyV0.2/StudioTools/arnold/ext/Yeti/v2.2.1/linux'])
          ##.appendPath(['\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\arnold\\ext\\Yeti\\v2.2.1\\windows'])
          .appendPath([mtoa_path])
          .appendPath([mtoa_path, 'shaders'])
          .appendPath(["${FDBIN}/plugin"]))

    print(EnvVar('ARNOLD_PLUGIN_PATH')
          .appendPath([mtoa_path, 'shaders']))

    print(EnvVar('MTOA_EXTENSIONS_PATH')
          .appendPath([mtoa_path, 'extensions']))

    print(EnvVar('MTOA_EXTENSIONS')
          .appendVar('MTOA_EXTENSIONS_PATH'))

    print(EnvVar('MTOA_PROCEDURALS')
          .appendPath([mtoa_path, 'procedurals']))

    print(EnvVar('PATH')
          .appendPath([mtoa_path, 'bin'])
          .appendPath([mtoa_path, 'plug-ins'])
          ##.appendPath(['\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\maya\\ext\\yeti\\v2.2.1\\linux\\bin'])
          .appendVar('ARNOLD_PLUGIN_PATH')
          .appendVar('MTOA_EXTENSIONS')
          .appendVar('MTOA_PROCEDURALS'))

    exe_path = os.path.join(mtoa_path, 'bin', 'kick.exe') if OsExtension.is_windows()\
        else os.path.join(mtoa_path, 'bin', 'kick')

    return Engine.run(exe_path, sys.argv[1:])

if __name__ == "__main__":
    sys.exit(main())
