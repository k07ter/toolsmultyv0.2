# -*- coding: utf-8 -*-
import os
import sgtk, tank
from tank import TankError
import maya.cmds as cmds
import shotgun_api3 as sg

_saved_engine = None
_saved_context = None

def _get_next_work_file_version(work_template, fields):
    """
    Find the next available version for the specified work_file
    """
    engine = sgtk.platform.current_engine()
    existing_versions = engine.tank.paths_from_template(work_template, fields, ["version"])
    version_numbers = [work_template.get_fields(v).get("version") for v in existing_versions]
    curr_v_no = fields["version"]
    max_v_no = max(version_numbers)
    return max(curr_v_no, max_v_no) + 1


def _deslash(str_or_array, raw=False):
    #print type(str_or_array)
    
    if not str_or_array:
        return str_or_array
        
    if type(str_or_array) is list:
        for i in range(len(str_or_array)):
            str_or_array[i] = _deslash(str_or_array[i])

        return str_or_array
    
    if type(str_or_array) in [str, unicode]:
        #print str_or_array
        return str_or_array.replace("\\", "/")
    
    return str_or_array
    
    
def _template_keys(context):
    if not context.entity or not context.task:
        raise TankError("Can not found context!")
        
    version_key = None
    maya_work_key = None
    maya_publish_key = None
    
    if context.entity['type'] == 'Sequence':
        version_key = 'maya_sequence_version'
        maya_work_key = 'maya_sequence_work'
        maya_publish_key = 'maya_sequence_publish'
    
    if context.entity['type'] == 'Shot':
        version_key = 'maya_shot_version'
        maya_work_key = 'maya_shot_work'
        maya_publish_key = 'maya_shot_publish'

    if context.entity['type'] == 'Asset':
        version_key = 'maya_asset_version'
        maya_work_key = 'maya_asset_work'
        maya_publish_key = 'maya_asset_publish'

    if not version_key:
        raise TankError("Unknown context (Sequence, Shot or Asset only Task)!")
    
    return version_key, maya_work_key, maya_publish_key


def commands():
    engine = sgtk.platform.current_engine()
    return engine.commands
    

def command(cmd_str = 'Publish...'):
    engine = sgtk.platform.current_engine()
    cmd = engine.commands[cmd_str]
    if cmd:
        cmd['callback']()
    

def restore_context():
    # dummy for back compatibility
    
    return
    """
    global _saved_engine, _saved_context
    
    if not _saved_context:
        raise TankError("Can not restore Context, unable to process! Please call context.plus_one() function for Save Context.")
        #scene_path = os.path.abspath(cmds.file(query=True, sn=True))
        #new_sgtk = sgtk.sgtk_from_path(scene_path)
        #_saved_context = new_sgtk.context_from_path(scene_path, previous_context=_saved_context)
    
    current_engine = sgtk.platform.current_engine()
    current_engine_name = current_engine.name
    if current_engine.context.task and current_engine.context.task == _saved_context.task:
        return 
    
    current_engine.destroy()
    _saved_engine = tank.platform.start_engine(current_engine_name, _saved_context.tank, _saved_context)
    _saved_context = _saved_engine.context
    
    return
    """
    
    
def plus_one(raw=False):
    #global _saved_engine, _saved_context

    engine = sgtk.platform.current_engine()
    #_saved_engine = engine
    #_saved_context = engine.context
    
    context = engine.context
    version_key, maya_work_key, maya_publish_key = _template_keys(context)
    
    tk = engine.sgtk
    work_template = tk.templates[maya_work_key]
    scene_path = os.path.abspath(cmds.file(query=True, sn=True))
    
    if not scene_path or not scene_path.lower().endswith((".ma", ".mb")):
        command("Shotgun Save As...")
        scene_path = os.path.abspath(cmds.file(query=True, sn=True))
        if not work_template.validate(scene_path):
            return None
        if raw:
            return scene_path
        return _deslash(scene_path)        
            
    if not work_template.validate(scene_path):
        raise TankError("File '%s' is not a valid path, unable to process!" % scene_path)
    
    fields = work_template.get_fields(scene_path)
    next_version = _get_next_work_file_version(work_template, fields)
    fields["version"] = next_version 
    #fields['version'] = fields['version'] + 1
    work_file = work_template.apply_fields(fields)

    #cmds.file(rename=work_file)
    #cmds.file(save=True) 
    app_multi_workfiles = engine.apps['tk-multi-workfiles']
    kwargs = {'operation':"save_as", 'file_path':work_file, 'context':context, 'parent_action':'save_file_as', 'file_version':None, 'read_only':False}
    ret_tmp = app_multi_workfiles.execute_hook("hook_scene_operation", **kwargs)
    
    if raw:
        return work_file
        
    return _deslash(work_file)
    

def get_context():
    engine = sgtk.platform.current_engine()
    tk = engine.sgtk
    sg = tk.shotgun
    context = {'project':dict(engine.context.project), 'entity':dict(engine.context.entity), 'task':dict(engine.context.task) }
    #context['user'] = sgtk.util.get_current_user(tk)

    if context['project']:
        ds_project = sg.find_one(context['project']['type'], [['id', 'is', context['project']['id']]], ['sg_code'])
        context['project']['code'] = ds_project['sg_code']

    
    if context['entity']:
        if context['entity']['type'] == 'Asset':
            ds_entity = sg.find_one(context['entity']['type'], [['id', 'is', context['entity']['id']]], ['sg_asset_type', 'sg_type_one', 'sg_type_two'])
            
            if 'asset_type' in ds_entity:
                context['entity']['asset_type'] = ds_entity['sg_asset_type']

            if 'sg_type_one' in ds_entity:
                context['entity']['asset_type'] = ds_entity['sg_type_one']['name']
               
        if context['entity']['type'] == 'Shot':
            sg_shot = sg.find_one(context['entity']['type'], [['id', 'is', context['entity']['id']]], ['sg_cut_in', 'sg_cut_out', 'sg_cut_order'])
            context['entity']['cut_in'] = sg_shot['sg_cut_in']
            context['entity']['cut_out'] = sg_shot['sg_cut_out']
            context['entity']['cut_order'] = sg_shot['sg_cut_order']
    
    if context['task']:
        ds_task = sg.find_one(context['task']['type'], [['id', 'is', context['task']['id']]], ['sg_process', 'sg_status_list'])
        context['task']['sg_process'] = ds_task['sg_process']
        context['task']['sg_status_list'] = ds_task['sg_status_list']
    
    return context

    
def get_user():
    engine = sgtk.platform.current_engine()
    tk = engine.sgtk
    user = sgtk.util.get_current_user(tk)
    return user
    
    
def get_assets():
    engine = sgtk.platform.current_engine()
    tk = engine.sgtk
    sg = tk.shotgun
    context = engine.context
    if context.entity['type'] not in ['Sequence', 'Shot']:
        raise TankError("Unknown context (Sequence and Shot only Task)!")
    
    arr_assets = []
    sequence = context.entity
    if context.entity['type'] == 'Shot':
        sg_shot = sg.find_one("Shot", [['id', 'is', context.entity['id']], ['project', 'is', context.project]], ['sg_sequence', 'assets'])
        if not sg_shot or not sg_shot['sg_sequence']:
            raise TankError("Unknown Sequence for Shot Task 1!")

        if 'assets' in sg_shot and sg_shot['assets']:
            arr_assets.extend(sg_shot['assets'])
        
        sequence = sg_shot['sg_sequence']

    if sequence:
        sg_sequence = sg.find_one("Sequence", [['id', 'is', sequence['id']], ['project', 'is', context.project]], ['assets'])
        if not sg_sequence:
            raise TankError("Unknown Sequence for Shot Task 2!")
            
        if 'assets' in sg_sequence and sg_sequence['assets']:
            arr_assets.extend(sg_sequence['assets'])
        
    arr_return = []
    for asset in arr_assets:
        sg_asset = sg.find_one("Asset", [['id', 'is', asset['id']]], ['code', 'sg_index', 'sg_asset_type', 'sg_type_one', 'sg_type_two'])
        if not sg_asset:
            raise TankError("Unknown Asset %s!", (asset))
        
        a_path = tk.paths_from_entity(asset['type'], asset['id'])
        
        a_tk = sgtk.sgtk_from_path(a_path[0])
        reference_template = a_tk.templates["maya_asset_reference"]
        #print(str(sg_asset))
        
        paths = a_tk.paths_from_template(reference_template, {"Asset": sg_asset['sg_index'], "type_one": sg_asset['sg_type_one']['name'], "type_two": sg_asset['sg_type_two']['name']})
        #print(str(paths))
        #print dir(a_tk)
        #print reference_template
        #[{"type":"chars", "status":"pndng", "process":"rig", "path":"c/temp/char.ma"}]
        
        arr_paths = _deslash(paths)
        asset_out = {'name':sg_asset['code'], 'type': sg_asset['sg_type_one']['name'], 'path':None}
        if arr_paths:
            asset_out['path'] = arr_paths[0]
        
        
        arr_return.append(asset_out)
    
    
    return arr_return
    
    
def get_version_path(raw=False):
    engine = sgtk.platform.current_engine()
    context = engine.context
    version_key, maya_work_key, maya_publish_key = _template_keys(context)
    
    tk = engine.sgtk
    version_template = tk.templates[version_key]
    work_template = tk.templates[maya_work_key]
    scene_path = os.path.abspath(cmds.file(query=True, sn=True))
    
    if not work_template.validate(scene_path):
        raise TankError("File '%s' is not a valid path, unable to process!" % scene_path)
    
    # use templates to convert to publish path:
    fields = work_template.get_fields(scene_path)
    version_path = version_template.apply_fields(fields)

    if raw:
        return version_path
        
    return _deslash(version_path)
    
    
def upload_version():
    engine = sgtk.platform.current_engine()
    tk = engine.sgtk
    sg = tk.shotgun
    context = engine.context
    version_path = get_version_path(raw=True)
    if not os.path.isfile(version_path):
        return None

    version_key, maya_work_key, maya_publish_key = _template_keys(context)
    
    version_template = tk.templates[version_key]
    work_template = tk.templates[maya_work_key]
    publish_template = tk.templates[maya_publish_key]
    
    scene_path = os.path.abspath(cmds.file(query=True, sn=True))
    if not work_template.validate(scene_path):
        raise TankError("File '%s' is not a valid path, unable to process!" % scene_path)
    
    fields = work_template.get_fields(scene_path)
    publish_path = publish_template.apply_fields(fields)
    
    iPos = version_path.rfind('\\')
    if iPos < 0:
        iPos = version_path.rfind('/')
        
    code_file = version_path[(iPos + 1):]
    code_version = code_file[:code_file.rfind('.')]
    
    data = { 'project': context.project,
             'code': code_version,
             'description': publish_path,
             'entity': context.entity,
             'sg_task': context.task,                         
             'sg_status_list': 'rev',
             'user': context.user,
             'created_by' : context.user, 
             }
        
    result = None
    try:
        version = sg.create('Version', data)
        result = sg.upload("Version", version['id'], version_path, "sg_uploaded_movie", code_file)
        #print result
    except Exception:
        pass
    
    return result

if __name__ == '__main__':
    import sys
    sys.path.append( "\\\\alpha\\tools\\tools\\shotgun\\scripts" )
    import context
    context = reload(context)
    print(context.get_context())
    
    