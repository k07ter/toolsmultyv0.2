# -*- coding: utf-8 -*-
import urllib, sys, os
sys.path.append(r'\\parovoz-frm01\Shotgun\utils')
sys.path.append(r'\\parovoz-frm01\Shotgun\utils2')
sys.path.append(r'C:\Python27\Lib\site-packages')
sys.path.append("\\\\parovoz-frm01.parovoz.local\\studio\\tools\\shotgun\\scripts")
#import shotgun_config
#import requests

try:
    import urllib.request as urllib2
    from urllib.parse import urlencode
except ImportError:
    import urllib2
    from urllib import urlencode

url = 'http://parovoz-frm01.parovoz.local/scripts/upload_version.py'
headers = {'User-Agent' : 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)'}
ROOT_DIR = os.getcwd()

"""
import os
import requests

def go(url, project, file_path):
    params = {'project': project, 'file_name':os.path.basename(file_path)}
    files = {'file': (file_path, open(file_path, 'rb'), 'video/quicktime')}
    r = requests.put(url, params=params, files=files)
    return r.text
"""

def go(project, net_file_path, user=""):
    query_args = {'project': project, 'file_path': net_file_path, 'user':user}
    #print(query_args)
    #,'Content-Disposition': 'form-data; name="up_file";filename='+net_file_path+"'", 'Content-Type': 'text/ma'}
    #query_args.update(
    #files = {'file': (query_args['file_path'], open(query_args['file_path'], 'rb'), 'text/plain\n')}
    #query_args.update(files)
    #query_args.update(values)
    encoded_args = urlencode(query_args)
    data = encoded_args.encode('utf-8')

    #rsp = requests.post( url, files=files)
    req = urllib2.Request(url, headers=headers)
    # 'files': open(net_file_path, 'rb').read()
    #sg = shotgun_config.init_scripts()
    #sg_elm = sg.find_one('Project', [['name', 'contains', project]],
                                     #['content', 'is', 'Layout']],
                                     #['link', 'is', net_file_path.split('preview/')[1].split('_layout')[0]]],
                                     #['task', 'sg_code', 'name', 'sg_reference'])
    #sg_elm = sg.find_one('Project', [['entity', 'is', {'type': 'Project', 'id': 168}]],
    #                                 #['content', 'is', 'Layout']],
    #                                 #['link', 'is', net_file_path.split('preview/')[1].split('_layout')[0]]],
    #                                 ['task', 'sg_code', 'name', 'sg_reference'])
    #print(data)
    try:
        response = urllib2.urlopen(req, data)
        html = response.read().decode('utf-8')
        return html
    except:
        return None

if __name__ == '__main__':
    import sys
    project = 'moriki-doriki'
    net_file_path = '\\\\omega\\moriki-doriki\\2_prod\\ep001\\sh080\\preview\\ep001_sh080_layout_v001.mov'
    user = 'kgorbunov'
    print(str(go(project, net_file_path, user)))