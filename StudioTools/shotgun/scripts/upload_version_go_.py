# -*- coding: utf-8 -*-
import urllib

try:
    import urllib.request as urllib2
    from urllib.parse import urlencode
except ImportError:
    import urllib2
    from urllib import urlencode

url = 'http://parovoz-frm01.parovoz.local/scripts/upload_version.py'
headers = {'User-Agent' : 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)'}

"""
import os
import requests

def go(url, project, file_path):
    params = {'project': project, 'file_name':os.path.basename(file_path)}
    files = {'file': (file_path, open(file_path, 'rb'), 'video/quicktime')}
    r = requests.put(url, params=params, files=files)
    return r.text
"""

def go(project, net_file_path, user=""):
    query_args = {'project': project, 'file_path':net_file_path, 'user':user}
    encoded_args = urlencode(query_args)
    request = urllib2.Request(url, headers=headers)
    data = encoded_args.encode('utf-8')
    
    try:
        response = urllib2.urlopen(request, data)
        html = response.read().decode('utf8')
        return html
    except:
        return None

        
if __name__ == '__main__':
    import sys
    sys.path.append( "\\\\parovoz-frm01.parovoz.local\\studio\\tools\\shotgun\\scripts" )
    from upload_version_go import go
    
    project = 'sandbox' # bears lantern patrol crafts sandbox
    net_file_path = '\\\\alpha\\studio\\temp\\batch_test\\ep002_sh009_anim_v001.mov'
    user = "aokhota"
    print(str(go(project, net_file_path, user)))
    
    