# -*- coding: utf-8 -*-
import maya.cmds as cmds
import context
context = reload(context)

def check_pre_publish():
    # test maya scene before publish
    # returns None if all ok and error string if not ok

    task_context = context.get_context()
    project = task_context['project']
    entity = task_context['entity']
    task = task_context['task']
    
    return None
    