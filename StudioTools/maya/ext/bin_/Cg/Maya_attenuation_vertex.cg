struct vert2frag
{
    float4 hPosition            : POSITION;
    float2 wLightVectorXY       : TEXCOORD0;
    float2 wLightVectorZW       : TEXCOORD1;
};

// define inputs from application
struct app2vert
{
    float4 oPosition            : POSITION;
};

vert2frag main(app2vert IN, 
               uniform float4x4 modelViewProj : _GL_MVP,
               uniform float4x4 objToWorldMatrix,
			   uniform float lightDiameterReciprocal,
			   uniform float4 wLightPos)
{
    vert2frag OUT;

	// Compute the world-space vertex position.
	//
	float4 wPosition = mul(objToWorldMatrix, IN.oPosition);

	// Compute the light direction vector, expressed in world space.
	//
	float3 wLightVector = (wLightPos - wPosition).xyz;
	
	// Move the light direction vector from world space to "light attenuation texture space".
	// (Think of a the point light as a sphere with a diameter D in world space.
	// In light attenuation texture space, that light would always have a diameter of 1,
	// and the center of the light would be positioned at (0.5, 0.5, 0.5))
	//
	float3 textureVector = wLightVector * lightDiameterReciprocal + 0.5f;

    OUT.hPosition = mul(modelViewProj, IN.oPosition);
	OUT.wLightVectorXY = textureVector.xy;
	OUT.wLightVectorZW = float2(textureVector.z, 0.5);

	return OUT;
}