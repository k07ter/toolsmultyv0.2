//**************************************************************************/
// Copyright (c) 2011 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.

#include "Common.cgh"

// The source texture
texture gSourceTex : SourceTexture
<
    string UIName = "Source Texture";
>;

// Sampler of source texture
sampler2D gSourceSamp = sampler_state
{
    Texture = <gSourceTex>;
};

// The 2nd source texture
texture gSourceTex2 : SourceTexture2
<
    string UIName = "Source Texture 2";
>;

// Sampler of 2nd source texture
sampler2D gSourceSamp2 = sampler_state
{
    Texture = <gSourceTex2>;
};

float4 gUVTransform : RelativeViewportDimensions;

// Red / cyan anaglpyh between 2 images
//
float4 PS_Anaglyph(VS_TO_PS_ScreenQuad In) : COLOR0
{
    float4 source = tex2D(gSourceSamp, In.UV * gUVTransform.zw + gUVTransform.xy);
    float4 source2 = tex2D(gSourceSamp2, In.UV * gUVTransform.zw + gUVTransform.xy);

	float4 result;
	result.x = source.x;
	result.yz = source2.yz;
	result.w = saturate(source.w + source2.w);
    return result;
}

// Luminance anaglyph between 2 images
//
float4 PS_Anaglyph_Luminance(VS_TO_PS_ScreenQuad In) : COLOR0
{
    float4 source = tex2D(gSourceSamp, In.UV * gUVTransform.zw + gUVTransform.xy);
    float4 source2 = tex2D(gSourceSamp2, In.UV * gUVTransform.zw + gUVTransform.xy);

	float3 RGBtoLum = { 0.1140, 0.5870, 0.2989 };
	float sourceLuminance = dot( RGBtoLum, source.xyz );
	float source2Luminance = dot( RGBtoLum, source2.xyz ); 

	float4 result;
	result.x = sourceLuminance;
	result.y = source2Luminance;
	result.z = source2Luminance;
	result.w  = source2.w;
	return result;
}

// The default technique.
technique Main
{
    pass p0
    {
        VertexShader = compile glslv VS_ScreenQuad();
        PixelShader = compile glslf PS_Anaglyph();
    }
}

technique AnaglyphLuminance
{
    pass p0
    {
        VertexShader = compile glslv VS_ScreenQuad();
        PixelShader = compile glslf PS_Anaglyph_Luminance();
    }
}
