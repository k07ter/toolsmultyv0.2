//**************************************************************************/
// Copyright (c) 2010 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.

#include "Common.cgh"

int gIterate <
    float UIMin = 2;
    float UIMax = 200;
    string UIName = "Iteration Count";
> = 50;

float gScale <
    string UIWidget = "slider";
    float UIMin = 0.001;
    float UIMax = 3.0;
    float UIStep = 0.001;
> = 0.423;

// float2 gCenter = {0.5,0.1};
float gCenterX <
    string UIWidget = "slider";
    string UIName = "X Center";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.001;
> = 0.0f;

float gCenterY <
    string UIWidget = "slider";
    string UIName = "Y Center";
    float UIMin = 0.0;
    float UIMax = 1.0;
    float UIStep = 0.001;
> = 0.379f;

float gRange <
    string UIWidget = "slider";
    float UIMin = 0.0;
    float UIMax = 0.05;
    float UIStep = 0.001;
    string UIName = "Outer Color Gradation";
> = 0.05f;

float3 gInColor <
    string UIWidget = "Color";
    string UIName = "Inside Region";
> = {0.05,0.225,0.3};

float3 gOutColorA <
    string UIWidget = "Color";
    string UIName = "Outer Region";
> = {0.4,0.6,0.9};

float3 gOutColorB <
    string UIWidget = "Color";
    string UIName = "Edge Region";
> = {1,0.8,0.1};

/////////////////////////////////////////////////////

// Pixel shader.
// NOTE: This expects screen quad vertex shader output.
float4 PS_Mandelbrot(VS_TO_PS_ScreenQuad IN) : COLOR0 
{
	gIterate = 50;
	gScale = 0.423;
	gCenterX = 0.0f;
	gCenterY = 0.379f;
	gRange = 0.05f;
	gInColor = float3(0.05,0.225,0.30);
	gOutColorA = float3(0.4,0.6,0.9);
	gOutColorB = float3(1,0.8,0.1);

    float2 pos = IN.UV.xy; // frac(IN.UV.xy);
    pos.y = 1 - pos.y;
    //float real = ((pos.x - 0.5)*gScale)-gCenterX;
    //float imag = ((pos.y - 0.5)*gScale)-gCenterY;
    float real = pos.x *gScale + (gCenterX);
    float imag = pos.y *gScale + (gCenterY);
    float Creal = real;
    float Cimag = imag;
    Creal = -0.765f;
    Cimag = 0.11f;
    float r2 = 0.0f;
    float i;
    for (i=0; (i<gIterate) && (r2<4.0); i++) {
		float tempreal = real;
		real = (tempreal*tempreal) - (imag*imag) + Creal;
		imag = 2.0*tempreal*imag + Cimag;
		r2 = (real*real) + (imag*imag);
    }
    float3 finalColor;
    if (r2 < 4.0f) {
       finalColor = gInColor;
    } else {
    	finalColor = lerp(gOutColorA,gOutColorB, frac(i * gRange));
    }
    return float4(finalColor,0);
}


#ifndef FX_COMPOSER

// Main technique.
technique Main
{
    pass p0
    {
        VertexShader = compile glslv VS_ScreenQuad();
        PixelShader = compile glslf PS_Mandelbrot();        
    }
}

#endif