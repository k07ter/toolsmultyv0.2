/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/

/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////
#include "mgear_solvers.h"

/////////////////////////////////////////////////
// GLOBAL
/////////////////////////////////////////////////
MTypeId mgear_slideCurve2::id(0x0011FECA);

MObject mgear_slideCurve2::master_crv; 
MObject mgear_slideCurve2::master_mat;

MObject mgear_slideCurve2::slave_length; 
MObject mgear_slideCurve2::master_length; 
MObject mgear_slideCurve2::position; 
MObject mgear_slideCurve2::maxstretch; 
MObject mgear_slideCurve2::maxsquash; 
MObject mgear_slideCurve2::softness; 
 
/////////////////////////////////////////////////
// METHODS
/////////////////////////////////////////////////
// CREATOR ======================================
void* mgear_slideCurve2::creator() { return new mgear_slideCurve2; }
 
// INIT =========================================
MStatus mgear_slideCurve2::initialize()
{
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnNumericAttribute nAttr;
	MStatus stat;

	// INPUTS MESH
	
    master_crv = tAttr.create("master_crv", "mcrv", MFnData::kNurbsCurve);
    stat = addAttribute( master_crv );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	master_mat = mAttr.create( "master_mat", "mmat" );
	mAttr.setStorable(true);
    mAttr.setReadable(false);
	stat = addAttribute( master_mat );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	// SLIDERS
	slave_length = nAttr.create("slave_length", "sl", MFnNumericData::kFloat, 1);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( slave_length );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	master_length = nAttr.create("master_length", "ml", MFnNumericData::kFloat, 1);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( master_length );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	position = nAttr.create("position", "p", MFnNumericData::kFloat, 0.0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setMin(0);
	nAttr.setMax(1);
    stat = addAttribute( position );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	maxstretch = nAttr.create("maxstretch", "mst", MFnNumericData::kFloat, 1.5);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setMin(1);
    stat = addAttribute( maxstretch );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
	maxsquash = nAttr.create("maxsquash", "msq", MFnNumericData::kFloat, .5);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setMin(0);
	nAttr.setMax(1);
    stat = addAttribute( maxsquash );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
	softness = nAttr.create("softness", "s", MFnNumericData::kFloat, 0.5);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setMin(0);
	nAttr.setMax(1);
    stat = addAttribute( softness );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
	// CONNECTIONS
	stat = attributeAffects( master_crv, outputGeom );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( master_mat, outputGeom );
		if (!stat) { stat.perror("attributeAffects"); return stat;}

	stat = attributeAffects( master_length, outputGeom );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( slave_length, outputGeom );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( position, outputGeom );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( maxstretch, outputGeom );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( maxsquash, outputGeom );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( softness, outputGeom );
		if (!stat) { stat.perror("attributeAffects"); return stat;}

    return MS::kSuccess;
}

// COMPUTE ======================================
MStatus mgear_slideCurve2::deform( MDataBlock& data, MItGeometry& iter, const MMatrix &mat, unsigned int mIndex )
{
    MStatus returnStatus;
	
    // Inputs ---------------------------------------------------------
    // Input NurbsCurve
	// Curve
    MObject crvObj = data.inputValue( master_crv ).asNurbsCurve();
	MFnNurbsCurve crv(crvObj);
    MMatrix m = data.inputValue(master_mat).asMatrix();
        
    // Input Sliders
    double in_sl = (double)data.inputValue(slave_length).asFloat();
    double in_ml = (double)data.inputValue(master_length).asFloat();
    double in_position = (double)data.inputValue(position).asFloat();
    double in_maxstretch = (double)data.inputValue(maxstretch).asFloat();
	double in_maxsquash = (double)data.inputValue(maxsquash).asFloat();
    double in_softness = (double)data.inputValue(softness).asFloat();
	
    // Init -----------------------------------------------------------
    double mstCrvLength = crv.length();

    int slvPointCount = iter.exactCount(); // Can we use .count() ? 
    int mstPointCount = crv.numCVs();
	
    // Stretch --------------------------------------------------------
	double expo = 1;
    if ((mstCrvLength > in_ml) && (in_maxstretch > 1)){
        if (in_softness != 0){
            double stretch = (mstCrvLength - in_ml) / (in_sl * in_maxstretch);
            expo = 1 - exp(-(stretch) / in_softness);
		}

        double ext = std::min(in_sl * (in_maxstretch - 1) * expo, mstCrvLength - in_ml);

        in_sl += ext;
	}
    else if ((mstCrvLength < in_ml) && (in_maxsquash < 1)){
        if (in_softness != 0){
            double squash = (in_ml - mstCrvLength) / (in_sl * in_maxsquash);
            expo = 1 - exp(-(squash) / in_softness);
		}

        double ext = std::min(in_sl * (1 - in_maxsquash) * expo, in_ml - mstCrvLength);

        in_sl -= ext;
	}
		
    // Position --------------------------------------------------------
    double size = in_sl / mstCrvLength;
    double sizeLeft = 1 - size;

    double start = in_position * sizeLeft;
    double end = start + size;

	double tStart, tEnd;
	crv.getKnotDomain(tStart, tEnd);
	
    // Process --------------------------------------------------------
    double step = (end - start) / (slvPointCount - 1.0);
    MPoint pt;
	MVector tan;
    while (! iter.isDone()){
        double perc = start + (iter.index() * step);

        double u = crv.findParamFromLength(perc * mstCrvLength);

        if ((0 <= perc) && (perc <= 1))
            crv.getPointAtParam(u, pt, MSpace::kWorld);
        else{
			double overPerc;
            if (perc < 0){
                overPerc = perc;
                crv.getPointAtParam(0, pt, MSpace::kWorld);
                tan = crv.tangent(0);
			}
            else{
                overPerc = perc - 1;
                crv.getPointAtParam(mstPointCount-3.0, pt, MSpace::kWorld);
                tan = crv.tangent(mstPointCount-3.0);

            tan.normalize();
            tan *= mstCrvLength * overPerc;

            pt += tan;
			}
		}

        pt *= mat.inverse();
        pt *= m;
        iter.setPosition(pt);
        iter.next();
	}
 
    return MS::kSuccess;
}
 
 