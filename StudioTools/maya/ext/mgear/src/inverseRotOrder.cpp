/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/
/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////
#include "mgear_solvers.h"

/////////////////////////////////////////////////
// GLOBAL
/////////////////////////////////////////////////
MTypeId mgear_inverseRotOrder::id(0x0011FEC5);

// Define the Node's attribute specifiers

MObject mgear_inverseRotOrder::rotOrder; 
MObject mgear_inverseRotOrder::output; 

mgear_inverseRotOrder::mgear_inverseRotOrder() {} // constructor
mgear_inverseRotOrder::~mgear_inverseRotOrder() {} // destructor

/////////////////////////////////////////////////
// METHODS
/////////////////////////////////////////////////
// CREATOR ======================================
void* mgear_inverseRotOrder::creator()
{
   return new mgear_inverseRotOrder();
}

// INIT =========================================
MStatus mgear_inverseRotOrder::initialize()
{
   MFnNumericAttribute nAttr;
   MFnEnumAttribute eAttr;
   MStatus stat;
   
    // Inputs
    rotOrder = eAttr.create( "rotOrder", "ro", 0 );
    eAttr.addField("xyz", 0);
    eAttr.addField("yzx", 1);
    eAttr.addField("zxy", 2);
    eAttr.addField("xzy", 3);
    eAttr.addField("yxz", 4);
    eAttr.addField("zyx", 5);
    eAttr.setWritable(true);
    eAttr.setStorable(true);
    eAttr.setReadable(true);
    eAttr.setKeyable(true);
	stat = addAttribute( rotOrder );
		if (!stat) {stat.perror("addAttribute"); return stat;}
    
    // Outputs
	output = nAttr.create( "output", "out", MFnNumericData::kShort, 0 );
	nAttr.setWritable(false);
	nAttr.setStorable(true);
    nAttr.setReadable(true);
	nAttr.setKeyable(false);
	stat = addAttribute( output );
		if (!stat) {stat.perror("addAttribute"); return stat;}

    // Connections
	stat = attributeAffects( rotOrder, output );
		if (!stat) { stat.perror("attributeAffects"); return stat;}

   return MS::kSuccess;
}
// COMPUTE ======================================
MStatus mgear_inverseRotOrder::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus returnStatus;

	if( plug != output )
		return MS::kUnknownParameter;

	// Input
	int ro  = data.inputValue( rotOrder ).asShort();
	int inv_ro [6] = {5, 3, 4, 1, 2, 0};

	// Output
	MDataHandle h_output = data.outputValue( output );
	h_output.setShort(inv_ro[ro]);
	data.setClean(plug);

	return MS::kSuccess;
}

