/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/
/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////
#include "mgear_solvers.h"

/////////////////////////////////////////////////
// GLOBAL
/////////////////////////////////////////////////
MTypeId mgear_spinePointAt::id(0x0011FECB);

// Define the Node's attribute specifiers

MObject mgear_spinePointAt::rotA;
MObject mgear_spinePointAt::rotAx;
MObject mgear_spinePointAt::rotAy;
MObject mgear_spinePointAt::rotAz;
MObject mgear_spinePointAt::rotB;
MObject mgear_spinePointAt::rotBx;
MObject mgear_spinePointAt::rotBy;
MObject mgear_spinePointAt::rotBz;
MObject mgear_spinePointAt::axe;
MObject mgear_spinePointAt::blend;
MObject mgear_spinePointAt::pointAt;

mgear_spinePointAt::mgear_spinePointAt() {} // constructor
mgear_spinePointAt::~mgear_spinePointAt() {} // destructor

/////////////////////////////////////////////////
// METHODS
/////////////////////////////////////////////////
// CREATOR ======================================
void* mgear_spinePointAt::creator()
{
   return new mgear_spinePointAt();
}

// INIT =========================================
MStatus mgear_spinePointAt::initialize()
{
   MFnNumericAttribute nAttr;
   MFnEnumAttribute eAttr;
   MStatus stat;
   
    // Inputs 
    rotA = nAttr.createPoint("rotA", "ra" );
    rotAx = nAttr.child(0);
    rotAy = nAttr.child(1);
    rotAz = nAttr.child(2);
    nAttr.setWritable(true);
    nAttr.setStorable(true);
    nAttr.setReadable(true);
    nAttr.setKeyable(false);
    stat = addAttribute( rotA );
		if (!stat) {stat.perror("addAttribute"); return stat;}
    
    rotB = nAttr.createPoint("rotB", "rb" );
    rotBx = nAttr.child(0);
    rotBy = nAttr.child(1);
    rotBz = nAttr.child(2);
    nAttr.setWritable(true);
    nAttr.setStorable(true);
    nAttr.setReadable(true);
    nAttr.setKeyable(false);
    stat = addAttribute( rotB );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
    axe = eAttr.create( "axe", "a", 2 );
    eAttr.addField("X", 0);
    eAttr.addField("Y", 1);
    eAttr.addField("Z", 2);
    eAttr.addField("-X", 3);
    eAttr.addField("-Y", 4);
    eAttr.addField("-Z", 5);
    eAttr.setWritable(true);
    eAttr.setStorable(true);
    eAttr.setReadable(true);
    eAttr.setKeyable(false);
    stat = addAttribute( axe );
		if (!stat) {stat.perror("addAttribute"); return stat;}
    
    blend = nAttr.create( "blend", "b", MFnNumericData::kFloat, 0.5 );
    nAttr.setWritable(true);
    nAttr.setStorable(true);
    nAttr.setReadable(true);
    nAttr.setKeyable(true);
    nAttr.setMin(0);
    nAttr.setMax(1);
    stat = addAttribute( blend );
		if (!stat) {stat.perror("addAttribute"); return stat;}

    // Outputs
	pointAt = nAttr.createPoint("pointAt", "pa" );
    nAttr.setWritable(false);
    nAttr.setStorable(false);
    nAttr.setReadable(true);
    stat = addAttribute( pointAt );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
    // Connections
    stat = attributeAffects ( rotA, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( rotAx, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( rotAy, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( rotAz, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( rotB, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( rotBx, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( rotBy, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( rotBz, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( axe, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( blend, pointAt );
		if (!stat) { stat.perror("attributeAffects"); return stat;}

   return MS::kSuccess;
}
// COMPUTE ======================================
MStatus mgear_spinePointAt::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus returnStatus;

	if( plug != pointAt )
		return MS::kUnknownParameter;
	
	MString sx, sy, sz, sw;

    // Get inputs
	MDataHandle h;
	MVector v;
    h = data.inputValue( rotA );
	v = h.asFloatVector();
    double rAx = v.x;
    double rAy = v.y;
    double rAz = v.z;

    h = data.inputValue( rotB );
	v = h.asFloatVector();
    double rBx = v.x;
    double rBy = v.y;
    double rBz = v.z;
        
	h = data.inputValue( axe );
    int axe = h.asShort();
        
	h = data.inputValue( blend );
    double in_blend = (double)h.asFloat();
	
    // Process
    // There is no such thing as siTransformation in Maya, 
    // so what we really need to compute this +/-360 roll is the global rotation of the object
    // We then need to convert this eulerRotation to Quaternion
    // Maybe it would be faster to use the MEulerRotation class, but anyway, this code can do it
    MQuaternion qA = e2q(rAx, rAy, rAz);
    MQuaternion qB = e2q(rBx, rBy, rBz);
	
    MQuaternion qC = slerp2(qA, qB, in_blend);

	MVector vOut;
	switch ( axe )
	{
		case 0:
			vOut = MVector(1,0,0);
			break;
		case 1:
			vOut = MVector(0,1,0);
			break;
		case 2:
			vOut = MVector(0,0,1);
			break;
		case 3:
			vOut = MVector(-1,0,0);
			break;
		case 4:
			vOut = MVector(0,-1,0);
			break;
		case 5:
			vOut = MVector(0,0,-1);
			break;
	}
	
    vOut = vOut.rotateBy(qC);
    float x = (float)vOut.x;
    float y = (float)vOut.y;
    float z = (float)vOut.z;

    // Output
    h = data.outputValue( pointAt );
	h.set3Float( x, y, z );

	// This doesn't work
    // h.setMVector( vOut );
    // h.set3Double( vOut );

    data.setClean( plug );

	return MS::kSuccess;
}

