/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/


#include "mgear_solvers.h"

MTypeId		mgear_linearInterpolate3DvectorNode::id(0x0011FEC6);

//Static variables

MObject		mgear_linearInterpolate3DvectorNode::vecA;
MObject		mgear_linearInterpolate3DvectorNode::vecAx;
MObject		mgear_linearInterpolate3DvectorNode::vecAy;
MObject		mgear_linearInterpolate3DvectorNode::vecAz;

MObject		mgear_linearInterpolate3DvectorNode::vecB;
MObject		mgear_linearInterpolate3DvectorNode::vecBx;
MObject		mgear_linearInterpolate3DvectorNode::vecBy;
MObject		mgear_linearInterpolate3DvectorNode::vecBz;

MObject		mgear_linearInterpolate3DvectorNode::blend;

MObject		mgear_linearInterpolate3DvectorNode::outVec;
//MObject		mgear_linearInterpolate3DvectorNode::outVecX;
//MObject		mgear_linearInterpolate3DvectorNode::outVecY;
//MObject		mgear_linearInterpolate3DvectorNode::outVecZ;





mgear_linearInterpolate3DvectorNode::mgear_linearInterpolate3DvectorNode(){}

mgear_linearInterpolate3DvectorNode::~mgear_linearInterpolate3DvectorNode(){}


void* mgear_linearInterpolate3DvectorNode::creator()
{
	return new mgear_linearInterpolate3DvectorNode();
}


/// INIT
MStatus mgear_linearInterpolate3DvectorNode::initialize()
{
	MStatus status;
	MFnNumericAttribute nAttr;
	//Inputs
	vecA = nAttr.createPoint("vectorA", "vectorA");
	vecAx = nAttr.child(0);
	vecAy = nAttr.child(1);
	vecAz = nAttr.child(2);
	nAttr.setKeyable(true);
	nAttr.setStorable(false);
	addAttribute(vecA);


	vecB = nAttr.createPoint("vectorB", "vectorB");
	vecBx = nAttr.child(0);
	vecBy = nAttr.child(1);
	vecBz = nAttr.child(2);
	nAttr.setKeyable(true);
	nAttr.setStorable(false);
	addAttribute(vecB);


	blend = nAttr.create("blend", "blend", MFnNumericData::kFloat, 0.0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setMin(0);
	nAttr.setMax(1);
	addAttribute(blend);


	// ouput
	outVec = nAttr.createPoint("outVector", "outVector");
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	nAttr.setReadable(true);
	addAttribute(outVec);

	// connections
	attributeAffects(vecA, outVec);
	attributeAffects(vecAx, outVec);
	attributeAffects(vecAy, outVec);
	attributeAffects(vecAz, outVec);
	attributeAffects(vecB, outVec);
	attributeAffects(vecBx, outVec);
	attributeAffects(vecBy, outVec);
	attributeAffects(vecBz, outVec);
	attributeAffects(blend, outVec);

	return MS::kSuccess;
}

// COMPUTE

MStatus mgear_linearInterpolate3DvectorNode::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus status;

	if (plug != outVec)
	{
		return MS::kUnknownParameter;
	}


	// inputs

	MDataHandle h;
	MVector v;
	h = data.inputValue(vecA);
	v = h.asFloatVector();
	float vecAx = v.x;
	float vecAy = v.y;
	float vecAz = v.z;

	h = data.inputValue(vecB);
	v = h.asFloatVector();
	float vecBx = v.x;
	float vecBy = v.y;
	float vecBz = v.z;

	//h = data.inputValue( blend );
	//double in_blend = (double)h.asFloat();
	float in_blend = (float)data.inputValue(blend, &status).asFloat();

	// Compute
	float vecCx = (vecBx * in_blend) + ((1.0 - in_blend) * vecAx);
	float vecCy = (vecBy * in_blend) + ((1.0 - in_blend) * vecAy);
	float vecCz = (vecBz * in_blend) + ((1.0 - in_blend) * vecAz);

	// Output
	MDataHandle hOut = data.outputValue(outVec, &status);
	hOut.set3Float(vecCx, vecCy, vecCz);
	//h.setClean();
	data.setClean(plug);

	return MS::kSuccess;
}
