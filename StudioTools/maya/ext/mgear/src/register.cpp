/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/
/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////

#include "mgear_solvers.h"

#include <maya/MFnPlugin.h>

/////////////////////////////////////////////////
// LOAD / UNLOAD
/////////////////////////////////////////////////
// INIT =========================================
MStatus initializePlugin( MObject obj )
{ 
	MStatus status;
	MFnPlugin plugin( obj, "Jeremie Passerin, Miquel Campos", "1.0.2", "Any");

	status = plugin.registerNode( "mgear_curveCns", mgear_curveCns::id, mgear_curveCns::creator, mgear_curveCns::initialize, MPxNode::kDeformerNode );
		if (!status) {status.perror("registerNode() failed."); return status;}
	
	status = plugin.registerNode( "mgear_rollSplineKine", mgear_rollSplineKine::id, mgear_rollSplineKine::creator, mgear_rollSplineKine::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_slideCurve2", mgear_slideCurve2::id, mgear_slideCurve2::creator, mgear_slideCurve2::initialize, MPxNode::kDeformerNode );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_squashStretch2", mgear_squashStretch2::id, mgear_squashStretch2::creator, mgear_squashStretch2::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_ikfk2Bone", mgear_ikfk2Bone::id, mgear_ikfk2Bone::creator, mgear_ikfk2Bone::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_inverseRotOrder", mgear_inverseRotOrder::id, mgear_inverseRotOrder::creator, mgear_inverseRotOrder::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_mulMatrix", mgear_mulMatrix::id, mgear_mulMatrix::creator, mgear_mulMatrix::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_intMatrix", mgear_intMatrix::id, mgear_intMatrix::creator, mgear_intMatrix::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_percentageToU", mgear_percentageToU::id, mgear_percentageToU::creator, mgear_percentageToU::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_spinePointAt", mgear_spinePointAt::id, mgear_spinePointAt::creator, mgear_spinePointAt::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode( "mgear_uToPercentage", mgear_uToPercentage::id, mgear_uToPercentage::creator, mgear_uToPercentage::initialize );
		if (!status) {status.perror("registerNode() failed."); return status;}

	status = plugin.registerNode("mgear_springNode", mgear_springNode::id, mgear_springNode::creator, mgear_springNode::initialize);
		if (!status) { status.perror("registerNode() failed."); return status; }

	status = plugin.registerNode("mgear_linearInterpolate3DvectorNode", mgear_linearInterpolate3DvectorNode::id, mgear_linearInterpolate3DvectorNode::creator, mgear_linearInterpolate3DvectorNode::initialize);
		if (!status) { status.perror("registerNode() failed."); return status; }

	status = plugin.registerNode("mgear_add10scalarNode", mgear_add10scalarNode::id, mgear_add10scalarNode::creator, mgear_add10scalarNode::initialize);
		if (!status) { status.perror("registerNode() failed."); return status; }
		


	return status;
}

// UNINIT =======================================
MStatus uninitializePlugin( MObject obj)
{
	MStatus status;
	MFnPlugin plugin( obj );
	
	status = plugin.deregisterNode( mgear_curveCns::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode( mgear_slideCurve2::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode( mgear_rollSplineKine::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode( mgear_squashStretch2::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
		
   
	status = plugin.deregisterNode( mgear_ikfk2Bone::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode( mgear_inverseRotOrder::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode( mgear_mulMatrix::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode( mgear_intMatrix::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode( mgear_percentageToU::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}

	status = plugin.deregisterNode( mgear_spinePointAt::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode( mgear_uToPercentage::id );
		if (!status) {status.perror("deregisterNode() failed."); return status;}
	status = plugin.deregisterNode(mgear_springNode::id);
		if (!status) { status.perror("deregisterNode() failed."); return status; }
	status = plugin.deregisterNode(mgear_linearInterpolate3DvectorNode::id);
		if (!status) { status.perror("deregisterNode() failed."); return status; }
	status = plugin.deregisterNode(mgear_add10scalarNode::id);
		if (!status) { status.perror("deregisterNode() failed."); return status; }


		
	return MS::kSuccess;
}