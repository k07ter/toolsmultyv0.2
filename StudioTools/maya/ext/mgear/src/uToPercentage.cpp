/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/
/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////
#include "mgear_solvers.h"

/////////////////////////////////////////////////
// GLOBAL
/////////////////////////////////////////////////
MTypeId mgear_uToPercentage::id(0x0011FECD);

// Define the Node's attribute specifiers

MObject mgear_uToPercentage::curve;
MObject mgear_uToPercentage::normalizedU;
MObject mgear_uToPercentage::u;
MObject mgear_uToPercentage::steps;
MObject mgear_uToPercentage::percentage;

mgear_uToPercentage::mgear_uToPercentage() {} // constructor
mgear_uToPercentage::~mgear_uToPercentage() {} // destructor

/////////////////////////////////////////////////
// METHODS
/////////////////////////////////////////////////
// CREATOR ======================================
void* mgear_uToPercentage::creator()
{
   return new mgear_uToPercentage();
}

// INIT =========================================
MStatus mgear_uToPercentage::initialize()
{
	MFnTypedAttribute tAttr;
	MFnNumericAttribute nAttr;
	MStatus stat;
	
    // Curve
    curve = tAttr.create("curve", "crv", MFnData::kNurbsCurve);
    stat = addAttribute( curve );
		if (!stat) {stat.perror("addAttribute"); return stat;}

		
   // Sliders
    normalizedU = nAttr.create("normalizedU", "n", MFnNumericData::kBoolean, false);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( normalizedU );
		if (!stat) {stat.perror("addAttribute"); return stat;}

    u = nAttr.create("u", "u", MFnNumericData::kFloat, .5, 0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( u );
		if (!stat) {stat.perror("addAttribute"); return stat;}

    steps = nAttr.create("steps", "s", MFnNumericData::kShort, 40);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( steps );
		if (!stat) {stat.perror("addAttribute"); return stat;}

    // Outputs
	percentage = nAttr.create( "percentage", "p", MFnNumericData::kFloat, 0 );
    nAttr.setWritable(false);
    nAttr.setStorable(false);
    nAttr.setReadable(true);
    nAttr.setKeyable(false);
    stat = addAttribute( percentage );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
    // Connections 
    stat = attributeAffects ( curve, percentage );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( steps, percentage );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( u, percentage );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( normalizedU, percentage );
		if (!stat) {stat.perror("attributeAffects"); return stat;}

   

   return MS::kSuccess;
}
// COMPUTE ======================================
MStatus mgear_uToPercentage::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus returnStatus;
	// Error check
    if (plug != percentage)
        return MS::kUnknownParameter;

	// Curve
   MObject crvObj = data.inputValue( curve ).asNurbsCurve();
   
	MFnNurbsCurve crv(crvObj);

	// Sliders
	bool in_normU = data.inputValue( normalizedU ).asBool();
	double in_u = (double)data.inputValue( u ).asFloat();
	unsigned in_steps = data.inputValue( steps ).asShort();

	// Process
	if (in_normU)
		in_u = normalizedUToU(in_u, crv.numCVs());

	// Get length
	MVectorArray u_subpos(in_steps);
	MVectorArray t_subpos(in_steps);
	MPoint pt;
	double step;
	for (unsigned i = 0; i < in_steps ; i++){

		step = i * in_u / (in_steps - 1.0);
		crv.getPointAtParam(step, pt, MSpace::kWorld);
		u_subpos[i] = MVector(pt);

        step = i/(in_steps - 1.0);
		crv.getPointAtParam(step, pt, MSpace::kWorld);
		t_subpos[i] = MVector(pt);

	}
	
	double u_length = 0;
	double t_length = 0;
	MVector v;
	for (unsigned i = 0; i < in_steps ; i++){
		if (i>0){
			v = u_subpos[i] - u_subpos[i-1];
			u_length += v.length();
			v = t_subpos[i] - t_subpos[i-1];
			t_length += v.length();
		}
	}

	double out_perc = (u_length / t_length) * 100;
		
	// Output
    MDataHandle h = data.outputValue( percentage );
    h.setDouble( out_perc );
    data.setClean( plug );

	return MS::kSuccess;
}

