/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/
/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////
#include "mgear_solvers.h"

/////////////////////////////////////////////////
// GLOBAL
/////////////////////////////////////////////////
MTypeId mgear_squashStretch2::id(0x0011FECC);

// Define the Node's attribute specifiers

MObject mgear_squashStretch2::global_scale;
MObject mgear_squashStretch2::global_scalex;
MObject mgear_squashStretch2::global_scaley;
MObject mgear_squashStretch2::global_scalez;

MObject mgear_squashStretch2::blend;
MObject mgear_squashStretch2::driver;
MObject mgear_squashStretch2::driver_min;
MObject mgear_squashStretch2::driver_ctr;
MObject mgear_squashStretch2::driver_max;

MObject mgear_squashStretch2::axis;
MObject mgear_squashStretch2::squash;
MObject mgear_squashStretch2::stretch;

MObject mgear_squashStretch2::output;

mgear_squashStretch2::mgear_squashStretch2() {} // constructor
mgear_squashStretch2::~mgear_squashStretch2() {} // destructor

/////////////////////////////////////////////////
// METHODS
/////////////////////////////////////////////////
// CREATOR ======================================
void* mgear_squashStretch2::creator()
{
   return new mgear_squashStretch2();
}

// INIT =========================================
MStatus mgear_squashStretch2::initialize()
{
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MStatus stat;

    // Inputs 
    global_scale = nAttr.createPoint("global_scale", "gs" );
    global_scalex = nAttr.child(0);
    global_scaley = nAttr.child(1);
    global_scalez = nAttr.child(2);
    nAttr.setWritable(true);
    nAttr.setStorable(true);
    nAttr.setReadable(true);
    nAttr.setKeyable(false);
    stat = addAttribute( global_scale );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	// Sliders
	blend = nAttr.create("blend", "b", MFnNumericData::kFloat, 1);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setMin(0);
    stat = addAttribute( blend );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
	driver = nAttr.create("driver", "d", MFnNumericData::kFloat, 3);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( driver );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
	driver_min = nAttr.create("driver_min", "dmin", MFnNumericData::kFloat, 1);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( driver_min );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
	driver_ctr = nAttr.create("driver_ctr", "dctr", MFnNumericData::kFloat, 3);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( driver_ctr );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
	driver_max = nAttr.create("driver_max", "dmax", MFnNumericData::kFloat, 6);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
    stat = addAttribute( driver_max );
		if (!stat) {stat.perror("addAttribute"); return stat;}

    axis = eAttr.create( "axis", "a", 0 );
    eAttr.addField("x", 0);
    eAttr.addField("y", 1);
    eAttr.addField("z", 2);
    eAttr.setWritable(true);
    eAttr.setStorable(true);
    eAttr.setReadable(true);
    eAttr.setKeyable(false);
    addAttribute( axis );
	
	squash = nAttr.create("squash", "sq", MFnNumericData::kFloat, .5);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setMin(-1);
    stat = addAttribute( squash );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	stretch = nAttr.create("stretch", "st", MFnNumericData::kFloat, -.5);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setMin(-1);
    stat = addAttribute( stretch );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	// Outputs
	output = nAttr.createPoint("output", "out" );
    nAttr.setWritable(false);
    nAttr.setStorable(false);
    nAttr.setReadable(true);
    addAttribute( output );

	// Connections
    stat = attributeAffects ( global_scale, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( blend, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( driver, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( driver_min, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( driver_ctr, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( driver_max, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( axis, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( squash, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}
    stat = attributeAffects ( stretch, output );
		if (!stat) {stat.perror("attributeAffects"); return stat;}


   return MS::kSuccess;
}
// COMPUTE ======================================
MStatus mgear_squashStretch2::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus returnStatus;
	// Error check
    if (plug != output)
        return MS::kUnknownParameter;

	// Inputs 
    MVector gscale = data.inputValue( global_scale ).asFloatVector();
	double sx = gscale.x;
	double sy = gscale.y;
	double sz = gscale.z;

	// Sliders
	double in_blend = (double)data.inputValue( blend ).asFloat();
	double in_driver = (double)data.inputValue( driver ).asFloat();
	double in_dmin = (double)data.inputValue( driver_min ).asFloat();
	double in_dctr = (double)data.inputValue( driver_ctr ).asFloat();
	double in_dmax = (double)data.inputValue( driver_max ).asFloat();
	int in_axis = data.inputValue( axis ).asShort();
	double in_sq = (double)data.inputValue( squash ).asFloat();
	double in_st = (double)data.inputValue( stretch ).asFloat();
	
	// Process
    in_st *= clamp(std::max(in_driver - in_dctr, 0.0) / std::max(in_dmax - in_dctr, 0.0001), 0.0, 1.0);
    in_sq *= clamp(std::max(in_dctr - in_driver, 0.0) / std::max(in_dctr - in_dmin, 0.0001), 0.0, 1.0);

    if (in_axis != 0)
        sx *= std::max( 0.0, 1.0 + in_sq + in_st );

    if (in_axis != 1)
        sy *= std::max( 0.0, 1.0 + in_sq + in_st );

    if (in_axis != 2)
        sz *= std::max( 0.0, 1.0 + in_sq + in_st );
	
    MVector scl = MVector(sx, sy, sz);
    scl = linearInterpolate(gscale, scl, in_blend);
	
	// Output
    MDataHandle h = data.outputValue( output );
	h.set3Float( (float)scl.x, (float)scl.y, (float)scl.z );
    data.setClean( plug );

	return MS::kSuccess;
}

