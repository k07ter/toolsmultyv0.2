/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/

/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////
#include "mgear_solvers.h"

/////////////////////////////////////////////////
// GLOBAL
/////////////////////////////////////////////////
MTypeId		mgear_add10scalarNode::id(0x0011FEC0);

//Static variables

MObject		mgear_add10scalarNode::aOutValue;
MObject		mgear_add10scalarNode::aInValue0;
MObject 	mgear_add10scalarNode::aInValue1;
MObject 	mgear_add10scalarNode::aInValue2;
MObject 	mgear_add10scalarNode::aInValue3;
MObject 	mgear_add10scalarNode::aInValue4;
MObject 	mgear_add10scalarNode::aInValue5;
MObject 	mgear_add10scalarNode::aInValue6;
MObject 	mgear_add10scalarNode::aInValue7;
MObject 	mgear_add10scalarNode::aInValue8;
MObject 	mgear_add10scalarNode::aInValue9;



mgear_add10scalarNode::mgear_add10scalarNode()
{
}

mgear_add10scalarNode::~mgear_add10scalarNode()
{
}


void* mgear_add10scalarNode::creator()
{
	return new mgear_add10scalarNode();
}


/// INIT
MStatus mgear_add10scalarNode::initialize()
{
	MStatus status;
	MFnNumericAttribute nAttr;

	aOutValue = nAttr.create("outValue", "outValue", MFnNumericData::kFloat);
	nAttr.setWritable(false); // es un attribute de salida por eso bloqueamos que se pueda meter una conexion como input
	nAttr.setStorable(false); // lo mismo
	addAttribute(aOutValue);

	aInValue0 = nAttr.create("inValue0", "inValue0", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue0);
	attributeAffects(aInValue0, aOutValue);

	aInValue1 = nAttr.create("inValue1", "inValue1", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue1);
	attributeAffects(aInValue1, aOutValue);

	aInValue2 = nAttr.create("inValue2", "inValue2", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue2);
	attributeAffects(aInValue2, aOutValue);

	aInValue3 = nAttr.create("inValue3", "inValue3", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue3);
	attributeAffects(aInValue3, aOutValue);

	aInValue4 = nAttr.create("inValue4", "inValue4", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue4);
	attributeAffects(aInValue4, aOutValue);

	aInValue5 = nAttr.create("inValue5", "inValue5", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue5);
	attributeAffects(aInValue5, aOutValue);

	aInValue6 = nAttr.create("inValue6", "inValue6", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue6);
	attributeAffects(aInValue6, aOutValue);

	aInValue7 = nAttr.create("inValue7", "inValue7", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue7);
	attributeAffects(aInValue7, aOutValue);

	aInValue8 = nAttr.create("inValue8", "inValue8", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue8);
	attributeAffects(aInValue8, aOutValue);

	aInValue9 = nAttr.create("inValue9", "inValue9", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	addAttribute(aInValue9);
	attributeAffects(aInValue9, aOutValue);



	return MS::kSuccess;
}

// COMPUTE

MStatus mgear_add10scalarNode::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus status;

	if (plug != aOutValue)
	{
		return MS::kUnknownParameter;
	}

	// preparing the values from the attributes
	float inputValue0 = data.inputValue(aInValue0, &status).asFloat();
	float inputValue1 = data.inputValue(aInValue1, &status).asFloat();
	float inputValue2 = data.inputValue(aInValue2, &status).asFloat();
	float inputValue3 = data.inputValue(aInValue3, &status).asFloat();
	float inputValue4 = data.inputValue(aInValue4, &status).asFloat();
	float inputValue5 = data.inputValue(aInValue5, &status).asFloat();
	float inputValue6 = data.inputValue(aInValue6, &status).asFloat();
	float inputValue7 = data.inputValue(aInValue7, &status).asFloat();
	float inputValue8 = data.inputValue(aInValue8, &status).asFloat();
	float inputValue9 = data.inputValue(aInValue9, &status).asFloat();



	// making the calculation

	float output = inputValue0 + inputValue1 + inputValue2 + inputValue3 + inputValue4 + inputValue5 + inputValue6 + inputValue7 + inputValue8 + inputValue9;

	MDataHandle hOutput = data.outputValue(aOutValue, &status);      //lower "h" for indicate that the variable is a handle
	CHECK_MSTATUS_AND_RETURN_IT(status); // mirar este statment en la documentacion de Maya
	hOutput.setFloat(output);
	hOutput.setClean();
	data.setClean(plug); // Esta linea es un ejemplo. hace lo mismo que la anteriro ya que el pointer apunta \
			// a la mis memoria. pero trabajar con handles es mas rapido que con el plug directamente.

	return MS::kSuccess;
}
