/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/
/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////
#include "mgear_solvers.h"

/////////////////////////////////////////////////
// GLOBAL
/////////////////////////////////////////////////
MTypeId mgear_intMatrix::id(0x0011FEC4);

// Define the Node's attribute specifiers
MObject mgear_intMatrix::blend;

MObject mgear_intMatrix::matrixA; 
MObject mgear_intMatrix::matrixB; 
MObject mgear_intMatrix::output; 

mgear_intMatrix::mgear_intMatrix() {} // constructor
mgear_intMatrix::~mgear_intMatrix() {} // destructor

/////////////////////////////////////////////////
// METHODS
/////////////////////////////////////////////////
// CREATOR ======================================
void* mgear_intMatrix::creator()
{
   return new mgear_intMatrix();
}

// INIT =========================================
MStatus mgear_intMatrix::initialize()
{
  MFnNumericAttribute nAttr;		
  MFnMatrixAttribute mAttr;
	MStatus stat;

	// ATTRIBUTES
   blend = nAttr.create( "blend", "b", MFnNumericData::kFloat, 0.0 );
   nAttr.setStorable(true);
   nAttr.setKeyable(true);
   nAttr.setMin(0);
   nAttr.setMax(1);
	stat = addAttribute( blend );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	// INPUTS
	matrixA = mAttr.create( "matrixA", "mA" );
	mAttr.setStorable(true);
	mAttr.setKeyable(true);
	mAttr.setConnectable(true);
	stat = addAttribute( matrixA );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	matrixB = mAttr.create( "matrixB", "mB" );
	mAttr.setStorable(true);
	mAttr.setKeyable(true);
	mAttr.setConnectable(true);
	stat = addAttribute( matrixB );
		if (!stat) {stat.perror("addAttribute"); return stat;}
		
	// OUTPUTS
	output = mAttr.create( "output", "out" );
	mAttr.setStorable(false);
	mAttr.setKeyable(false);
	mAttr.setConnectable(true);
	stat = addAttribute( output );
		if (!stat) {stat.perror("addAttribute"); return stat;}

	// CONNECTIONS
	stat = attributeAffects( matrixA, output );
		if (!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( matrixB, output );
		if (!stat) { stat.perror("attributeAffects"); return stat;}

   return MS::kSuccess;
}
// COMPUTE ======================================
MStatus mgear_intMatrix::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus returnStatus;

	if( plug != output )
		return MS::kUnknownParameter;

	// Input
	MMatrix mA = data.inputValue( matrixA ).asMatrix();
	MMatrix mB = data.inputValue( matrixB ).asMatrix();

	MTransformationMatrix mAm= MTransformationMatrix(mA);
	MTransformationMatrix mBm = MTransformationMatrix(mB);

	// SLIDERS
	double in_blend = (double)data.inputValue( blend ).asFloat();

	MTransformationMatrix mCm = interpolateTransform(mAm, mBm, in_blend);

	MMatrix mC = mCm.asMatrix();
	//MMatrix mC = (mA * in_blend) +( (1 - in_blend) * mB);
	// double i = mC.matrix[0][0];
	

	

	// Output
	MDataHandle h;
	h = data.outputValue( output );
	h.setMMatrix( mC );
	data.setClean(plug);

	return MS::kSuccess;
}

