/*

This file is part of mGear.

Mmgear is free software: you can redistribute it and/or modify
it under the terms of the FreeBSD License

Copyright (c) 2014, Jeremie Passerin, Miquel Campos
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Projectself.

Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
Date:       2014 / 11 / 26

*/
#ifndef _rigSolvers
#define _rigSolvers

#define McheckErr(stat,msg)         \
    if ( MS::kSuccess != stat ) {   \
                cerr << msg;                \
                return MS::kFailure;        \
        }

/////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////


#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MPxDeformerNode.h>

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnUnitAttribute.h>

#include <maya/MQuaternion.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVector.h>
#include <maya/MMatrix.h>
#include <maya/MMatrixArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MDoubleArray.h>
#include <maya/MEulerRotation.h>
#include <maya/MTime.h>


#include <maya/MFnMesh.h>
#include <maya/MItGeometry.h>
#include <maya/MDagModifier.h>
#include <maya/MPointArray.h>

#include <maya/MFnNurbsCurve.h>
#include <maya/MPoint.h>

#include <maya/MTypeId.h> 


#include <maya/MStatus.h>
//#include <minmax.h>
#include <math.h>
 
 
 

#define PI 3.14159265


/////////////////////////////////////////////////
// STRUCTS
/////////////////////////////////////////////////
struct s_GetFKTransform
{
   double lengthA;
   double lengthB;
   bool negate;
   MTransformationMatrix root;
   MTransformationMatrix bone1;
   MTransformationMatrix bone2;
   MTransformationMatrix eff;
};

struct s_GetIKTransform
{
   double lengthA;
   double lengthB;
   bool negate;
   double roll;
   double scaleA;
   double scaleB;
   double maxstretch;
   double softness;
   double slide;
   double reverse;
   MTransformationMatrix root;
   MTransformationMatrix eff;
   MTransformationMatrix	 upv;
};

/////////////////////////////////////////////////
// CLASSES
/////////////////////////////////////////////////
class mgear_slideCurve2 : public MPxDeformerNode
{
public:
                    mgear_slideCurve2() {};
    virtual MStatus deform( MDataBlock& data, MItGeometry& itGeo, const MMatrix &localToWorldMatrix, unsigned int mIndex );
    static  void*   creator();
    static  MStatus initialize();
 
    static MTypeId      id;

	// Input
	static MObject	 master_crv;
	static MObject	 master_mat;

	static MObject	 slave_length;
	static MObject	 master_length;
	static MObject	 position;

	static MObject	 maxstretch;
	static MObject	 maxsquash;
	static MObject	 softness;
};

class mgear_curveCns : public MPxDeformerNode
{
public:
                    mgear_curveCns() {};
    virtual MStatus deform( MDataBlock& data, MItGeometry& itGeo, const MMatrix &localToWorldMatrix, unsigned int mIndex );
    static  void*   creator();
    static  MStatus initialize();
 
    static MTypeId      id;
    static  MObject     inputs; 
};

class mgear_rollSplineKine : public MPxNode
{
 public:
      mgear_rollSplineKine();	
   virtual	 ~mgear_rollSplineKine();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();

 public:
	static MTypeId id;

	// Input
	static MObject	 ctlParent;
	static MObject	 inputs;
	static MObject	 inputsRoll;
	static MObject	 outputParent;

	static MObject	 u;
	static MObject	 resample;
	static MObject	 subdiv;
	static MObject	 absolute;

	// Output
	static MObject	 output;

};
class mgear_squashStretch2 : public MPxNode
{
 public:
      mgear_squashStretch2();	
   virtual	 ~mgear_squashStretch2();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();

 public:
	static MTypeId id;
	
	// Input
	static MObject	 global_scale;
	static MObject	 global_scalex;
	static MObject	 global_scaley;
	static MObject	 global_scalez;

	static MObject	 blend;
	static MObject	 driver;
	static MObject	 driver_min;
	static MObject	 driver_ctr;
	static MObject	 driver_max;
	static MObject	 axis;
	static MObject	 squash;
	static MObject	 stretch;

	// Output
	static MObject	 output;

};

class mgear_percentageToU : public MPxNode
{
 public:
      mgear_percentageToU();	
   virtual	 ~mgear_percentageToU();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();

 public:
	static MTypeId id;
	
	// Input
	static MObject	 curve;
	static MObject	 normalizedU;
	static MObject	 percentage;
	static MObject	 steps;

	// Output
	static MObject	 u;

};

class mgear_uToPercentage : public MPxNode
{
 public:
      mgear_uToPercentage();	
   virtual	 ~mgear_uToPercentage();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();

 public:
	static MTypeId id;
	
	// Input
	static MObject	 curve;
	static MObject	 normalizedU;
	static MObject	 u;
	static MObject	 steps;

	// Output
	static MObject	 percentage;

};

class mgear_spinePointAt : public MPxNode
{
 public:
      mgear_spinePointAt();	
   virtual	 ~mgear_spinePointAt();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();

 public:
	static MTypeId id;
	
	// Input
	static MObject	 rotA;
	static MObject	 rotAx;
	static MObject	 rotAy;
	static MObject	 rotAz;
	static MObject	 rotB;
	static MObject	 rotBx;
	static MObject	 rotBy;
	static MObject	 rotBz;
	static MObject	 axe;
	static MObject	 blend;

	// Output
	static MObject	 pointAt;

};

class mgear_inverseRotOrder : public MPxNode
{
 public:
      mgear_inverseRotOrder();	
   virtual	 ~mgear_inverseRotOrder();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();

 public:
	static MTypeId id;

	// Input
	static MObject	 rotOrder;

	// Output
	static MObject	 output;

};

class mgear_mulMatrix : public MPxNode
{
 public:
      mgear_mulMatrix();	
   virtual	 ~mgear_mulMatrix();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();

 public:
	static MTypeId id;

	// Input
	static MObject	 matrixA;
	static MObject	 matrixB;

	// Output
	static MObject	 output;

};

class mgear_intMatrix : public MPxNode
{
 public:
      mgear_intMatrix();	
   virtual	 ~mgear_intMatrix();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();

 public:
	static MTypeId id;

	// ATTRIBUTES
	static MObject	 blend;

	// Input
	static MObject	 matrixA;
	static MObject	 matrixB;

	// Output
	static MObject	 output;

};

class mgear_ikfk2Bone : public MPxNode
{
 public:
      mgear_ikfk2Bone();	
   virtual	 ~mgear_ikfk2Bone();	

   virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   static void* creator();
   static MStatus initialize();
   MTransformationMatrix getIKTransform(s_GetIKTransform values, MString outportName);
   MTransformationMatrix getFKTransform(s_GetFKTransform values, MString outportName);

 public:

	// ATTRIBUTES
	static MObject	 blend;

	static MObject	 lengthA;
	static MObject	 lengthB;
	static MObject	 negate;

	static MObject	 scaleA;
	static MObject	 scaleB;
	static MObject	 roll;	

	static MObject	 maxstretch;
	static MObject	 slide;
	static MObject	 softness;	
	static MObject	 reverse;	
	
	// INPUTS
	static MObject	 root;
	static MObject	 ikref;
	static MObject	 upv;
	static MObject	 fk0;
	static MObject	 fk1;
	static MObject	 fk2;

	// OUTPUTS
	static MObject	 inAparent;
	static MObject	 inBparent;
	static MObject	 inCenterparent;
	static MObject	 inEffparent;

	static MObject	 outA;
	static MObject	 outB;
	static MObject	 outCenter;
	static MObject	 outEff;

	static MTypeId id;
};


class mgear_add10scalarNode : public MPxNode
{
public:
	mgear_add10scalarNode();
	virtual			~mgear_add10scalarNode();
	static	void*	creator();

	virtual MStatus		compute(const MPlug& plug, MDataBlock& data);
	static MStatus		initialize();

	static MTypeId id;
	static MObject aOutValue;  //start with "a" lower case to indicate attribute
	static MObject aInValue0;
	static MObject aInValue1;
	static MObject aInValue2;
	static MObject aInValue3;
	static MObject aInValue4;
	static MObject aInValue5;
	static MObject aInValue6;
	static MObject aInValue7;
	static MObject aInValue8;
	static MObject aInValue9;

};

class mgear_linearInterpolate3DvectorNode : public MPxNode
{
public:
	mgear_linearInterpolate3DvectorNode();
	virtual			~mgear_linearInterpolate3DvectorNode();
	static	void*	creator();

	virtual MStatus		compute(const MPlug& plug, MDataBlock& data);
	static MStatus		initialize();

	static MTypeId id;

	//Inputs
	static MObject	 vecA;
	static MObject	 vecAx;
	static MObject	 vecAy;
	static MObject	 vecAz;
	static MObject	 vecB;
	static MObject	 vecBx;
	static MObject	 vecBy;
	static MObject	 vecBz;
	static MObject	 blend;

	//Outputs
	static MObject outVec;
	//static MObject outVecX;
	//static MObject outVecY;
	//static MObject outVecZ;

};

class mgear_springNode : public MPxNode
{
public:
	mgear_springNode();
	virtual			~mgear_springNode();
	static	void*	creator();

	virtual MStatus		compute(const MPlug& plug, MDataBlock& data);
	static MStatus		initialize();

	static MTypeId id;
	static MObject aOutput;
	static MObject aGoal;
	static MObject aGoalX;
	static MObject aGoalY;
	static MObject aGoalZ;
	static MObject aDamping;
	static MObject aStiffness;
	static MObject aTime;
	//static MObject aParentInverse;
	static MObject aSpringIntensity;

	////variables

	bool _initialized;
	MTime _previousTime;
	MFloatVector _currentPosition;
	MFloatVector _previousPosition;

};

/////////////////////////////////////////////////
// METHODS
/////////////////////////////////////////////////
MQuaternion e2q(double x, double y, double z);
MQuaternion slerp2(MQuaternion qA, MQuaternion qB, double blend);
double clamp(double d, double min_value, double max_value);
int clamp(int d, int min_value, int max_value);
double getDot(MQuaternion qA, MQuaternion qB);
double radians2degrees(double a);
double degrees2radians(double a);
double round(const double value, const int precision);
double normalizedUToU(double u, int point_count);
double uToNormalizedU(double u, int point_count);
unsigned findClosestInArray(double value, MDoubleArray in_array);  
double set01range(double value, double first, double second);
double linearInterpolate(double first, double second, double blend);
MVector linearInterpolate(MVector v0, MVector v1, double blend);
MVectorArray bezier4point( MVector a, MVector tan_a, MVector d, MVector tan_d, double u);
MVector rotateVectorAlongAxis(MVector v, MVector axis, double a);
MQuaternion getQuaternionFromAxes(MVector vx, MVector vy, MVector vz);
MTransformationMatrix mapWorldPoseToObjectSpace(MTransformationMatrix objectSpace, MTransformationMatrix pose);
MTransformationMatrix mapObjectPoseToWorldSpace(MTransformationMatrix objectSpace, MTransformationMatrix pose);
MTransformationMatrix interpolateTransform(MTransformationMatrix xf1, MTransformationMatrix xf2, double blend);


#endif