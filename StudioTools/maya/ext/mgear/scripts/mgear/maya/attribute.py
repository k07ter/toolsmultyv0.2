'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.attribute
# @author Jeremie Passerin, Miquel Campos
#
#############################################
# GLOBAL
#############################################
import mgear
import pymel.core as pm
import pymel.core.datatypes as dt


#############################################
# NODE
#############################################

# ========================================================
## Add attribute
# @param node Transform or Shape - The object to add the new attribute
# @param longName String - The attribute name
# @param attributeType String - The Attribute Type
# @param defaultValue  -
# @param niceName  -
# @param shortName  -
# @param minValue  -
# @param maxValue  -
# @param keyable  -
# @param keyable  -
# @param readable  -
# @param storable  -
# @param writable  -
# @return - Teh long name of the new attribute
def addAttribute(node, longName, attributeType, defaultValue, niceName=None, shortName=None, minValue=None, maxValue=None, keyable=True, readable=True, storable=True, writable=True):

    if node.hasAttr(longName):
        mgear.log("Attribute already exists", mgear.error)
        return

    data = {}

    if shortName is not None:
        data["shortName"] = shortName
    if niceName is not None:
        data["niceName"] = niceName
    if attributeType == "string":
        data["dataType"] = attributeType
    else:
        data["attributeType"] = attributeType

    if minValue is not None and minValue is not False:
        data["minValue"] = minValue
    if maxValue is not None and maxValue is not False:
        data["maxValue"] = maxValue

    data["keyable"] = keyable
    data["readable"] = readable
    data["storable"] = storable
    data["writable"] = writable
    node.addAttr(longName, **data)
    if defaultValue is not None:
        node.setAttr(longName, defaultValue)
    return node.attr(longName)

def addColorAttribute(node, longName, defaultValue=False, keyable=True, readable=True, storable=True,
                        writable=True, niceName=None, shortName=None):

    if node.hasAttr(longName):
        mgear.log("Attribute already exists", mgear.error)
        return

    data = {}

    data["attributeType"] = "float3"
    if shortName is not None:
        data["shortName"] = shortName
    if niceName is not None:
        data["niceName"] = niceName

    data["usedAsColor"] = True
    data["keyable"] = keyable
    data["readable"] = readable
    data["storable"] = storable
    data["writable"] = writable

    #child nested attr
    dataChild = {}
    dataChild["attributeType"] = 'float'
    dataChild["parent"] = longName

    node.addAttr(longName, **data)
    node.addAttr(longName + "_r", **dataChild)
    node.addAttr(longName + "_g", **dataChild)
    node.addAttr(longName + "_b", **dataChild)

    if defaultValue:
        node.setAttr(longName+ "_r", defaultValue[0])
        node.setAttr(longName+ "_g", defaultValue[1])
        node.setAttr(longName+ "_b", defaultValue[2])

    return node.attr(longName)


def addEnumAttribute(node, longName, defaultValue, enum, niceName=None, shortName=None, keyable=True, readable=True, storable=True, writable=True):

    if node.hasAttr(longName):
        mgear.log("Attribute '"+longName+"' already exists", mgear.sev_warning)
        return

    data = {}

    if shortName is not None:
        data["shortName"] = shortName
    if niceName is not None:
        data["niceName"] = niceName

    data["attributeType"] = "enum"
    data["en"] = ":".join(enum)

    data["keyable"] = keyable
    data["readable"] = readable
    data["storable"] = storable
    data["writable"] = writable

    node.addAttr(longName, **data)
    node.setAttr(longName, defaultValue)

    return node.attr(longName)

def lockAttribute(node, attributes=["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz", "v"]):

    for attr_name in attributes:
        node.setAttr(attr_name, lock=True, keyable=False)

# ========================================================
## Set Capabilities of given local parameters to keyable or nonKeyable
# @param node X3DObject - The object to set.
# @param params List of String - The local parameter to set as keyable. params not in the list will be locked (expression + readonly)\n
# if None, ["tx", "ty", "tz", "rorder", "rx", "ry", "rz", "sx", "sy", "sz"] is used
# @return
def setKeyableAttributes(nodes, params=["tx", "ty", "tz", "ro", "rx", "ry", "rz", "sx", "sy", "sz"]):

    localParams = ["tx", "ty", "tz", "ro", "rx", "ry", "rz", "sx", "sy", "sz", "v"]

    if not isinstance(nodes, list):
        nodes = [nodes]

    for attr_name in params:
        for node in nodes:
            node.setAttr(attr_name, lock=False, keyable=True)

    for attr_name in localParams:
        if attr_name not in params:
            for node in nodes:
                node.setAttr(attr_name, lock=True, keyable=False)

# ========================================================
## Set the rotorder of the object
# @param node X3DObject - The object to set the rot order on
# @param s String - Value of the rotorder. Possible values : ("XYZ", "XZY", "YXZ", "YZX", "ZXY", "ZYX")
# @return
def setRotOrder(node, s="XYZ"):

    a = ["XYZ", "YZX", "ZXY", "XZY", "YXZ", "ZYX"]

    if s not in a:
        mgear.log("Invalid Rotorder : "+s, mgear.siError)
        return False

    # Unless Softimage there is no event on the rotorder parameter to automatically adapt the angle values
    # So let's do it manually using the EulerRotation class

    er = dt.EulerRotation([pm.getAttr(node+".rx"),pm.getAttr(node+".ry"),pm.getAttr(node+".rz")], unit="degrees")
    er.reorderIt(s)

    node.setAttr("ro", a.index(s))
    node.setAttr("rotate", er.x, er.y, er.z)


# ========================================================
## Set invert mirror pose values
# @param node X3DObject - The object to set invert mirror Values
# @param params List of String - The  parameter to invert in a mirror pose\n
# @return
def setInvertMirror(node, invList=None):

    aDic = {"tx":"invTx", "ty":"invTy", "tz":"invTz", "rx":"invRx", "ry":"invRy", "rz":"invRz", "sx":"invSx", "sy":"invSy", "sz":"invSz"}

    for axis in invList:
        if axis not in aDic:
            mgear.log("Invalid Invert Axis : "+axis, mgear.siError)
            return False

        node.setAttr(aDic[axis], True)

########################################################
# FCurve attribute (just a animCurveUU node connected to an attribute)
#######################################################
# =====================================================
## FCurve(animCurveUU) attribute
# @param node X3DObject - The object to add the new fcurve attribute
# @param name String - The  attribute name
# @return Fcurve and attribute name
def addFCurve( node, name="fcurve", keys=[]):

    attr_name = addAttribute(node, name, "double", 0)
    attrDummy_name = addAttribute(node, name + "_dummy", "double", 0)


    for key in keys:
        # we use setDrivenKeyframe, because is the only workaround that I found to create an animCurveUU with keyframes
        # fCurve = pm.PyNode(pm.createNode("animCurveUU"))
        pm.setDrivenKeyframe(attr_name, cd= attrDummy_name, dv=key[0], v=key[1], itt=key[2], ott=key[2])

    #clean dummy attr
    pm.deleteAttr(attrDummy_name)

    fCurve = pm.PyNode(attr_name).listConnections(type="animCurveUU")[0]

    return fCurve, attr_name

##########################################################
# PARAMETER DEFINITION
##########################################################
# ========================================================
class ParamDef(object):

    ## Init Method.
    # @param self
    # @param scriptName String - Parameter scriptname
    # @return ParamDef - The stored parameter definition
    def __init__(self, scriptName):

        self.scriptName = scriptName
        self.value = None
        self.valueType = None

    ## Add a parameter to property using the parameter definition.
    # @param self
    # @param prop Property - The property to add the parameter to.
    def create(self, node):

        attr_name = addAttribute(node, self.scriptName, self.valueType, self.value, self.niceName, self.shortName, self.minimum, self.maximum, self.keyable, self.readable, self.storable, self.writable)

        return node, attr_name

class ParamDef2(ParamDef):

    ## Init Method.
    # @param self
    # @param scriptName String - Parameter scriptname
    # @param valueType Integer - siVariantType
    # @param value Variant - Default parameter value
    # @param minimum Variant - mininum value
    # @param maximum Variant - maximum value
    # @param sugMinimum Variant - suggested mininum value
    # @param sugMaximum Variant - suggested maximum value
    # @param classification Integer - parameter classification
    # @param capabilities Integer - parameter capabilities
    # @return ParamDef - The stored parameter definition
    def __init__(self, scriptName, valueType, value, niceName=None, shortName=None, minimum=None, maximum=None, keyable=True, readable=True, storable=True, writable=True):

    # def __init__(self, scriptName, valueType, value, minimum=None, maximum=None, keyable=True, readable=True, storable=True, writable=True, niceName=None, shortName=None):

        self.scriptName = scriptName
        self.niceName = niceName
        self.shortName = shortName
        self.valueType = valueType
        self.value = value
        self.minimum = minimum
        self.maximum = maximum
        self.keyable = keyable
        self.readable = readable
        self.storable = storable
        self.writable = writable


## Create an Fcurve parameter definition.\n
class FCurveParamDef(ParamDef):

    ## Init Method.
    # @param self
    # @param scriptName String - Parameter scriptname
    # @return ParamDef - The stored parameter definition
    def __init__(self, scriptName, keys=None, interpolation=0, extrapolation=0):

        self.scriptName = scriptName
        self.keys = keys
        self.interpolation = interpolation
        self.extrapolation = extrapolation
        self.value = None
        self.valueType = None

    ## Add a parameter to property using the parameter definition.
    # @param self
    # @param prop Property - The property to add the parameter to.
    def create(self, node):

        attr_name = addAttribute(node, self.scriptName, "double", 0)
        attrDummy_name = addAttribute(node, self.scriptName + "_dummy", "double", 0)

        # fcv_node = pm.PyNode(pm.createNode("animCurveUU"))
        # fcv_node.connectAttr("output", attr_name)
        for key in self.keys:
            pm.setDrivenKeyframe(attr_name, cd= attrDummy_name, dv=key[0], v=key[1])

        #clean dummy attr
        pm.deleteAttr(attrDummy_name)

        return node, attr_name

## Create an Color parameter definition.\n
class colorParamDef(ParamDef):

    ## Init Method.
    # @param self
    # @param scriptName String - Parameter scriptname
    # @return ParamDef - The stored parameter definition
    def __init__(self, scriptName, defaultValue=False):

        self.scriptName = scriptName
        self.defaultValue = defaultValue
    ## Add a parameter to property using the parameter definition.
    # @param self
    def create(self, node):

        attr_name = addColorAttribute(node, self.scriptName, defaultValue=self.defaultValue)

        return node, attr_name

## Create an enumarator parameter definition.\n
class enumParamDef(ParamDef):

    ## Init Method.
    # @param self
    # @param scriptName String - Parameter scriptname
    # @return ParamDef - The stored parameter definition
    def __init__(self, scriptName, enum, defaultValue=0 ):

        self.scriptName = scriptName
        self.defaultValue = defaultValue
        self.enum = enum
    ## Add a parameter to property using the parameter definition.
    # @param self
    def create(self, node):

        attr_name = addEnumAttribute(node, self.scriptName, enum=self.enum, defaultValue=self.defaultValue)

        return node, attr_name