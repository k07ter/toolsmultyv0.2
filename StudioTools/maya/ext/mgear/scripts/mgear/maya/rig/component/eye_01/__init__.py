'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.rig.component.eye_01
# @author Miquel Campos
#

##########################################################
# GLOBAL
##########################################################
# Maya
import pymel.core as pm

# mgear
from mgear.maya.rig.component import MainComponent

import mgear.maya.primitive as pri
import mgear.maya.transform as tra
import mgear.maya.applyop as aop

##########################################################
# COMPONENT
##########################################################
## The main component class.
class Component(MainComponent):

    # =====================================================
    # OBJECTS
    # =====================================================

    ## Add all the objects needed to create the component.
    # @param self
    def addObjects(self):

        t = tra.getTransformFromPos(self.guide.pos["root"])
        self.eyeOver_npo = pri.addTransform(self.root, self.getName("eyeOver_npo"), t)
        self.eyeOver_ctl = self.addCtl(self.eyeOver_npo, "Over_ctl", t, self.color_fk, "sphere", w=1)
        self.eye_npo = pri.addTransform(self.root, self.getName("eye_npo"), t)
        self.eyeFK_ctl = self.addCtl(self.eye_npo, "fk_ctl", t, self.color_fk, "arrow", w=1)
        # sq st controls
        self.sqUp_ctl = self.addCtl(self.root, "squashUp_ctl", t, self.color_ik, "arrow", w=.1)
        self.sqLow_ctl = self.addCtl(self.root, "squashDown_ctl", t, self.color_ik, "arrow", w=.1)

        # look at
        t = tra.getTransformFromPos(self.guide.pos["look"])
        self.ik_cns = pri.addTransform(self.root, self.getName("ik_cns"), t)
        self.eyeIK_npo = pri.addTransform(self.ik_cns, self.getName("ik_npo"), t)
        self.eyeIK_ctl = self.addCtl(self.eyeIK_npo, "ik_ctl", t, self.color_fk, "circle", w=.5)

        self.addShadow(self.eyeFK_ctl, "eye")
        self.addShadow(self.eyeOver_ctl, "eyeOver")

        #Envelopes for lattice
        self.addShadow(self.sqUp_ctl, "LatticeSqUp")
        self.addShadow(self.sqLow_ctl, "LatticeSqLow")






    # =====================================================
    # PROPERTY
    # =====================================================
    ## Add parameters to the anim and setup properties to control the component.
    # @param self
    def addAttributes(self):

        self.trackingUpEyelid_att = self.addAnimParam("trackupeyelid", "Eyelid Up Tracking", "double", 0, 0, 1)
        self.trackingLowEyelid_att = self.addAnimParam("trackloweyelid", "Eyelid Low Tracking", "double", 0, 0, 1)


        # Ref
        if self.settings["ikrefarray"]:
            ref_names = self.settings["ikrefarray"].split(",")
            if len(ref_names) > 1:
                self.ikref_att = self.addAnimEnumParam("ikref", "Ik Ref", 0, self.settings["ikrefarray"].split(","))

    # =====================================================
    # OPERATORS
    # =====================================================
    ## Apply operators, constraints, expressions to the hierarchy.\n
    # In order to keep the code clean and easier to debug,
    # we shouldn't create any new object in this method.
    # @param self
    def addOperators(self):

        cns = aop.aimCns(self.eye_npo, self.eyeIK_ctl, "zy", 2, [0,1,0], self.root, False)

        pm.scaleConstraint(self.eyeOver_ctl, self.eye_npo, maintainOffset=False)
        pm.pointConstraint(self.eyeOver_ctl, self.eye_npo, maintainOffset=False)


    # =====================================================
    # CONNECTOR
    # =====================================================
    ## Set the relation between object from guide to rig.\n
    # @param self
    def setRelation(self):
        self.relatives["root"] = self.root


    ## standard connection definition.
    # @param self
    def connect_standard(self):
        self.connect_standardWithSimpleIkRef()