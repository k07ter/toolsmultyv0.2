'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.rig.component.chain_03
# @author Jeremie Passerin
#

##########################################################
# GLOBAL
##########################################################
# Maya
import pymel.core as pm
import pymel.core.datatypes as dt


# mgear
from mgear.maya.rig.component import MainComponent

import mgear.maya.primitive as pri
import mgear.maya.transform as tra
import mgear.maya.vector as vec
import mgear.maya.applyop as aop

##########################################################
# COMPONENT
##########################################################
## The main component class.
class Component(MainComponent):

    # =====================================================
    # OBJECTS
    # =====================================================
    ## Add all the objects needed to create the component.
    # @param self
    def addObjects(self):

        self.normal = self.guide.blades["blade"].z*-1
        self.binormal = self.guide.blades["blade"].x


        self.fk_npo = []
        self.fk_ctl = []
        self.spring_cns = []
        self.spring_lvl = []
        self.spring_ref = []
        self.spring_npo = []
        self.spring_target = []
        parent = self.root
        for i, t in enumerate(tra.getChainTransform(self.guide.apos, self.normal, self.negate)):
            dist = vec.getDistance(self.guide.apos[i], self.guide.apos[i+1])

            fk_npo = pri.addTransform(parent, self.getName("fk%s_npo"%i), t)
            spring_cns = pri.addTransform(fk_npo, self.getName("spring%s_cns"%i), t)
            fk_ctl = self.addCtl(spring_cns, "fk%s_ctl"%i, t, self.color_fk, "cube", w=dist, h=self.size*.1, d=self.size*.1, po=dt.Vector(dist*.5*self.n_factor,0,0))


            t = tra.getTransformFromPos(self.guide.apos[i+1])
            spring_npo = pri.addTransform(parent, self.getName("spring%s_npo"%i), t)
            spring_target = pri.addTransform(spring_npo, self.getName("spring%s"%i), t)


            parent = fk_ctl

            self.spring_cns.append(spring_cns)

            self.addToGroup(spring_cns, "PLOT")

            self.fk_npo.append(fk_npo)
            self.fk_ctl.append(fk_ctl)

            self.spring_target.append(spring_target)


        # Chain of deformers -------------------------------
        self.loc = []
        parent = self.root
        for i, t in enumerate(tra.getChainTransform(self.guide.apos, self.normal, self.negate)):
            loc = pri.addTransform(parent, self.getName("%s_loc"%i), t)
            self.addShadow(loc, i)

            self.loc.append(loc)
            parent = loc

    # =====================================================
    # PROPERTY
    # =====================================================
    ## Add parameters to the anim and setup properties to control the component.
    # @param self
    def addAttributes(self):

        # Anim -------------------------------------------
        self.aDamping = []
        self.aStiffness = []
        self.aSpring_active = self.addAnimParam("spring_active", "Spring active", "double", 1, 0, 1)
        self.aSpring_intensity = self.addAnimParam("spring_intensity", "Spring chain intensity", "double", 0, 0, 1)
        for i, tar in enumerate(self.spring_target):
            aDamping = self.addAnimParam( "damping_%s"%i, "damping_%s"%i, "double", 0.5, 0, 1)
            aStiffness = self.addAnimParam( "stiffness_%s"%i, "stiffness_%s"%i, "double", 0.5, 0, 1)
            self.aDamping.append(aDamping)
            self.aStiffness.append(aStiffness)


    # =====================================================
    # OPERATORS
    # =====================================================
    ## Apply operators, constraints, expressions to the hierarchy.\n
    # In order to keep the code clean and easier to debug,
    # we shouldn't create any new object in this method.
    # @param self
    def addOperators(self):


        # spring operators

        #settings aim contraints
        for i, tranCns in enumerate(self.spring_cns):
            if self.negate:
                aimAxis = "-xy"
            else:
                aimAxis = "xy"
            cns = aop.aimCns(tranCns, self.spring_target[i], aimAxis, 2, [0,1,0], self.fk_npo[i], False)

            #change from fcurves to spring
            pb_node = pm.createNode("pairBlend")

            pm.connectAttr(cns+".constraintRotateX", pb_node+".inRotateX2")
            pm.connectAttr(cns+".constraintRotateY", pb_node+".inRotateY2")
            pm.connectAttr(cns+".constraintRotateZ", pb_node+".inRotateZ2")
            pm.setAttr(pb_node+".translateXMode", 2)
            pm.setAttr(pb_node+".translateYMode", 2)
            pm.setAttr(pb_node+".translateZMode", 2)


            pm.connectAttr( pb_node+".outRotateX", tranCns+".rotateX", f=True)
            pm.connectAttr( pb_node+".outRotateY", tranCns+".rotateY", f=True)
            pm.connectAttr( pb_node+".outRotateZ", tranCns+".rotateZ", f=True)
            pm.setKeyframe( tranCns, at="rotateX")
            pm.setKeyframe( tranCns, at="rotateY")
            pm.setKeyframe( tranCns, at="rotateZ")

            pm.connectAttr(self.aSpring_active, pb_node+".weight")


            springOP = aop.gear_spring_op(self.spring_target[i])

            pm.connectAttr(self.aSpring_intensity, springOP+".intensity")
            pm.connectAttr(self.aDamping[i], springOP+".damping")
            pm.connectAttr(self.aStiffness[i], springOP+".stiffness")



    # =====================================================
    # CONNECTOR
    # =====================================================
    ## Set the relation beetween object from guide to rig.\n
    # @param self
    def setRelation(self):

        self.relatives["root"] = self.loc[0]
        for i in range(1, len(self.loc)):
            self.relatives["%s_loc"%i] = self.loc[i]
        self.relatives["%s_loc"%(len(self.loc)-1)] = self.loc[-1]
