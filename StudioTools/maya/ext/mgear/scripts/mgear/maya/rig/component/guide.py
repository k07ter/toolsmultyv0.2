'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.rig.component.guide
# @author Jeremie Passerin, Miquel Campos
#

##########################################################
# GLOBAL
##########################################################
from functools import partial

# pyMel
# from pymel.core.general import *
import pymel.core as pm
import pymel.core.datatypes as dt

# mgear
import mgear.string as string
import mgear.maya.dag as dag
# import mgear.maya.transform as tra
import mgear.maya.vector as vec
import mgear.maya.primitive as pri
import mgear.maya.transform as tra
import mgear.maya.icon as ico
import mgear.maya.curve as cur
import mgear.maya.utils as uti
import mgear.maya.applyop as aop

import mgear
from mgear.maya.rig.guide import MainGuide


##########################################################
# COMPONENT GUIDE
##########################################################
## Main class for component guide creation.\n
# This class handles all the parameters and objectDefs creation.\n
# It also know how to parse its own hierachy of object to retrieve position and transform.\n
# Finally it also now how to export itself as xml_node.
class ComponentGuide(MainGuide):

    compType = "component"  ## Component type
    compName = "component"  ## Component default name
    compSide = "C"
    compIndex = 0 ## Component default index

    description = "" ## Description of the component

    connectors = []
    compatible = []
    ctl_grp = ""

    # ====================================================
    ## Init method.
    # @param self
    # @param ref an xml definition or a SI3DObject
    def __init__(self):

        # Parameters names, definition and values.
        self.paramNames = [] ## List of parameter name cause it's actually important to keep them sorted.
        self.paramDefs = {} ## Dictionary of parameter definition.
        self.values = {} ## Dictionary of options values.

        # We will check a few things and make sure the guide we are loading is up to date.
        # If parameters or object are missing a warning message will be display and the guide should be updated.
        self.valid = True

        self.root = None
        self.id = None

        # parent component identification
        self.parentComponent = None
        self.parentLocalName = None

        # List and dictionary used during the creation of the component
        self.tra = {} ## dictionary of global transform
        self.atra = [] ## list of global transform
        self.pos = {} ## dictionary of global postion
        self.apos = [] ## list of global position
        self.prim = {} ## List of primitive
        self.blades = {}
        self.size = .1
        self.root_size = None

        # List and dictionary used to define data of the guide that should be saved
        self.pick_transform = [] ## User will have to pick the position of this object name
        self.save_transform = [] ## Transform of object name in this list will be saved
        self.save_primitive = [] ## Primitive of object name in this list will be saved
        self.save_blade = [] ## Normal and BiNormal of object will be saved
        self.minmax = {} ## Define the min and max object for multi location objects

        # Init the guide
        self.postInit()
        self.initialHierarchy()
        self.addParameters()

    ## Define the objects name and categories.\n
    # REIMPLEMENT. This method should be reimplemented in each component.
    # @param self
    def postInit(self):
        self.save_transform = ["root"]
        return

    # ====================================================
    # OBJECTS AND PARAMETERS
    ## Initial hierachy. It's no more than the basic set of parameters and layout needed for the setting property.
    # @param self
    def initialHierarchy(self):

        # Parameters --------------------------------------
        # This are the necessary parameter for component guide definition
        self.pCompType = self.addParam("comp_type",  "string", self.compType)
        self.pCompName = self.addParam("comp_name",  "string", self.compName)
        self.pCompSide = self.addParam("comp_side",  "string", self.compSide)
        self.pCompIndex = self.addParam("comp_index",  "long", self.compIndex, 0)
        self.pConnector = self.addParam("connector",  "string", "standard")
        self.pUIHost = self.addParam("ui_host",  "string", "")

        # Items -------------------------------------------
        typeItems = [self.compType, self.compType]
        for type in self.compatible:
            typeItems.append(type)
            typeItems.append(type)

        connectorItems = ["standard", "standard"]
        for item in self.connectors:
            connectorItems.append(item)
            connectorItems.append(item)

        # self.setFromHierarchy(self.root)
    ## Create the objects of the guide.\n
    # REIMPLEMENT. This method should be reimplemented in each component.
    # @param self
    def addObjects(self):
        self.root = self.addRoot()

    ## Create the parameter definitions of the guide.\n
    # REIMPLEMENT. This method should be reimplemented in each component.
    # @param self
    def addParameters(self):
        return

    # ====================================================
    # SET / GET
    def setFromHierarchy(self, root):

        self.root = root
        self.model = self.root.getParent(generations=-1)

        # ---------------------------------------------------
        # First check and set the settings
        if not self.root.hasAttr("comp_type"):
            mgear.log("%s is not a proper guide."%self.root.longName(), mgear.sev_error)
            self.valid = False
            return

        self.setParamDefValuesFromProperty(self.root)

        # ---------------------------------------------------
        # Then get the objects
        for name in self.save_transform:
            if "#" in name:
                i = 0
                while not self.minmax[name].max > 0 or i < self.minmax[name].max:
                    localName = string.replaceSharpWithPadding(name, i)

                    node = dag.findChild(self.model, self.getName(localName))
                    if not node:
                        break

                    self.tra[localName] = node.getMatrix(worldSpace=True)
                    self.atra.append(node.getMatrix(worldSpace=True))
                    self.pos[localName] = node.getTranslation(space="world")
                    self.apos.append(node.getTranslation(space="world"))

                    i += 1

                if i < self.minmax[name].min:
                    mgear.log("Minimum of object requiered for "+name+" hasn't been reached!!", mgear.sev_warning)
                    self.valid = False
                    continue

            else:
                node = dag.findChild(self.model, self.getName(name))
                if not node:
                    mgear.log("Object missing : %s"%name, mgear.sev_warning)
                    self.valid = False
                    continue

                self.tra[name] = node.getMatrix(worldSpace=True)
                self.atra.append(node.getMatrix(worldSpace=True))
                self.pos[name] = node.getTranslation(space="world")
                self.apos.append(node.getTranslation(space="world"))
        
        # for name in self.save_primitive:
        #     obj = self.model.FindChild(self.getName(name))
        #     if not obj:
        #         mgear.log("Object missing : %s"%name, mgear.sev_warning)
        #         self.valid = False
        #         continue

        #     self.prim[name] = pri.getPrimitive(obj)

        for name in self.save_blade:

            node = dag.findChild(self.model, self.getName(name))
            if not node:
                mgear.log("Object missing : %s"%name, mgear.sev_warning)
                self.valid = False
                continue

            # self.blades[name] = node.getMatrix(worldSpace=True)
            self.blades[name] = vec.Blade(node.getMatrix(worldSpace=True))

        self.size = self.getSize()
    # ====================================================
    # DRAW

    ## Draw the guide in the scene.
    # param self
    # param parent X3DObject - Parent object.
    def draw(self, parent):
        self.parent = parent
        self.setIndex(self.parent)
        self.addObjects()
        pm.select(self.root)

        #TODO: add function to scale the points of the icons
        # Set the size of the root
        # self.root.size = self.root_size

    ## Launch a pick session to get the position of the guide, then draw the guide in the scene.
    # param self
    # param parent X3DObject - Parent object.
    def drawFromUI(self, parent):
        if not self.modalPositions():
            mgear.log("aborded", mgear.sev_warning)
            return

        self.parent = parent
        self.setIndex(self.parent)
        self.addObjects()
        pm.select(self.root)


     ## Launch a modal dialog to set position of the guide.
    # param self
    # return Boolean - False if pick session was aborded at anytime.
    def modalPositions(self):

        self.jNumberVal = False
        self.dirAxisVal = False
        self.jSpacVal = False


        for name in self.save_transform:

            if "#" in name:

                def _addLocMultiOptions():

                    pm.setParent(q=True)

                    pm.columnLayout( adjustableColumn=True, cal="right" )
                    pm.text(l='', al="center")

                    fl = pm.formLayout()
                    jNumber = pm.intFieldGrp(v1=3, l="Joint Number")
                    pm.setParent( '..' )
                    pm.formLayout(fl, e=True, af=(jNumber, "left",-30))

                    dirSet = ["X", "-X", "Y", "-Y", "Z", "-Z"]
                    fl = pm.formLayout()
                    dirAxis = pm.optionMenu(l="Direction")
                    dirAxis.addMenuItems(dirSet)
                    pm.setParent( '..' )
                    pm.formLayout(fl, e=True, af=(dirAxis, "left", 70))

                    fl = pm.formLayout()
                    jSpac = pm.floatFieldGrp(v1=1.0, l="spacing")
                    pm.setParent( '..' )
                    pm.formLayout(fl, e=True, af=(jSpac, "left",-30))

                    pm.text(l='', al="center")

                    pm.button(l='Continue', c=partial(_retriveOptions, jNumber, dirAxis, jSpac))
                    pm.setParent( '..' )

                def _retriveOptions(jNumber, dirAxis, jSpac, *args):
                    self.jNumberVal = jNumber.getValue()[0]
                    self.dirAxisVal = dirAxis.getValue()
                    self.jSpacVal = jSpac.getValue()[0]

                    pm.layoutDialog( dismiss="Continue" )

                def _show():

                    pm.layoutDialog(ui=_addLocMultiOptions)

                _show()

                if self.jNumberVal:
                    if self.dirAxisVal == "X":
                        offVec = dt.Vector(self.jSpacVal, 0, 0)
                    elif self.dirAxisVal == "-X":
                        offVec = dt.Vector(self.jSpacVal*-1, 0, 0)
                    elif self.dirAxisVal == "Y":
                        offVec = dt.Vector(0, self.jSpacVal, 0)
                    elif self.dirAxisVal == "-Y":
                        offVec = dt.Vector(0, self.jSpacVal*-1, 0)
                    elif self.dirAxisVal == "Z":
                        offVec = dt.Vector(0, 0, self.jSpacVal)
                    elif self.dirAxisVal == "-Z":
                        offVec = dt.Vector(0, 0, self.jSpacVal*-1)

                    # newPosition = offVec + self.root.getTranslation(space="world")
                    newPosition = dt.Vector(0, 0, 0)
                    for i in range(self.jNumberVal):
                        newPosition = offVec + newPosition
                        localName = string.replaceSharpWithPadding(name, i)
                        self.tra[localName] = tra.getTransformFromPos(newPosition)
        return True
    # ====================================================
    # UPDATE

    ## Update the component index to get the next valid one.
    # @param self
    # @param parent X3DObject - The parent object of the guide.
    def setIndex(self, model):

        self.model =  model.getParent(generations=-1)

        # Find next index available
        while True:
            obj = dag.findChild(self.model, self.getName("root"))
            if not obj or (self.root and obj == self.root):
                break
            self.setParamDefValue("comp_index", self.values["comp_index"] + 1)

    ## Inverse the transform of each element of the guide.
    # @param self
    def symmetrize(self):

        if self.values["comp_side"] not in ["R", "L"]:
            mgear.log("Can't symmetrize central component", mgear.sev_error)
            return False
        for name, paramDef in self.paramDefs.items():
            if paramDef.valueType == "string":
                self.setParamDefValue(name, uti.convertRLName(self.values[name]))
        for name, t in self.tra.items():
            self.tra[name] = tra.getSymmetricalTransform(t)
        for name, blade in self.blades.items():
            self.blades[name] = vec.Blade(tra.getSymmetricalTransform(blade.transform))
            # self.blades[name] = tra.getSymmetricalTransform(blade)

        return True

    def rename(self, root, newName, newSide, newIndex):
        self.parent = root

        # store old properties
        oldIndex = self.parent.attr("comp_index").get()
        oldSide = self.parent.attr("comp_side").get()
        oldName = self.parent.attr("comp_name").get()
        oldSideIndex = oldSide + str(oldIndex)

        # change attr side in root
        self.parent.attr("comp_name").set(newName)
        self.parent.attr("comp_side").set(newSide)
        # set new index and update to the next valid
        self.setParamDefValue("comp_name",newName)
        self.setParamDefValue("comp_side",newSide)

        self.setParamDefValue("comp_index",newIndex)
        self.setIndex(self.parent)

        self.parent.attr("comp_index").set( self.values["comp_index"])


        objList =  dag.findComponentChildren(self.parent, oldName, oldSideIndex)
        newSideIndex = newSide +  str(self.values["comp_index"])
        objList.append(self.parent)
        for obj in objList:
            suffix =  obj.name().split("_")[-1]
            if len(obj.name().split("_")) == 3:
                new_name = "_".join([newName, newSideIndex, suffix])
            else:
                subIndex =  obj.name().split("_")[-2]
                new_name = "_".join([newName, newSideIndex, subIndex, suffix])
            pm.rename(obj, new_name)





    # ====================================================
    # ELEMENTS

    ## Add a root object to the guide.\n
    ## This mehod can initialize the object or draw it.\n
    ## Root object is a simple null with a specific display and a setting property.
    # @param self
    # return X3DObject - The created root null.
    def addRoot(self):
        if "root" not in self.tra.keys():
            self.tra["root"] = tra.getTransformFromPos(dt.Vector(0,0,0))

        self.root = ico.guideRootIcon(self.parent, self.getName("root"), color=13,  m=self.tra["root"] )

        #Add Parameters from parameter definition list.
        for scriptName in self.paramNames:
            paramDef = self.paramDefs[scriptName]
            paramDef.create(self.root)

        return self.root

    # Add a loc object to the guide.\n
    ## This mehod can initialize the object or draw it.\n
    ## Loc object is a simple null to define a position or a tranformation in the guide.
    # @param self
    # @param name String - Local name of the element.
    # @param parent X3DObject - The parent of the element.
    # @param position SIVector3 - The default position of the element. Pick sesssion is launch if none.
    # return X3DObject - The created loc null.
    def addLoc(self, name, parent, position=None):

        if name not in self.tra.keys():
            self.tra[name] = tra.getTransformFromPos(position)
        if name in self.prim.keys():
            # this functionality is not implemented. The actual design from softimage Gear should be review to fit in Maya.
            loc = self.prim[name].create(parent, self.getName(name), self.tra[name],  color=17)
        else:
            loc = ico.guideLocatorIcon(parent, self.getName(name), color=17, m=self.tra[name])

        return loc

    ## Add multiple loc objects to the guide.\n
    ## This mehod can initialize the object or draw it.\n
    ## Loc object is a simple null to define a position or a tranformation in the guide.
    # @param self
    # @param name String - Local name of the element.
    # @param parent X3DObject - The parent of the element.
    # @param minimum Int - The minimum number of loc.
    # @param maximum Int - The maximum number of loc.
    # @param updateParent - update the parent reference or keep the same for all loc
    # return List of X3DObject - The created loc nulls in a list.
    def addLocMulti(self, name, parent, updateParent = True):

        if "#" not in name:
            mgear.log("You need to put a '#' in the name of multiple location.", mgear.sev_error)
            return False

        locs = []
        i = 0
        while True:
            localName = string.replaceSharpWithPadding(name, i)
            if localName not in self.tra.keys():
                break

            loc = ico.guideLocatorIcon(parent, self.getName(localName), color=17, m=self.tra[localName])
            locs.append(loc)
            if updateParent:
                parent = loc

            i += 1
        return locs
    ## Add a blade object to the guide.\n
    ## This mehod can initialize the object or draw it.\n
    ## Blade object is a 3points curve to define a plan in the guide.
    # @param self
    # @param name String - Local name of the element.
    # @param parentPos X3DObject - The parent of the element.
    # @param parentDir X3DObject - The direction constraint of the element.
    # return X3DObject - The created blade curve.
    def addBlade(self, name, parentPos, parentDir, negate = False):
        if name not in self.blades.keys():
            self.blades[name] = vec.Blade(tra.getTransformFromPos(dt.Vector(0,0,0)))
            offset = False
        else:
            offset = True

        # Draw from UI
        dist = .6 * self.root.attr("scaleX").get()
        blade = ico.guideBladeIcon(parent=parentPos, name=self.getName(name),lenX=dist, color=13, m=self.blades[name].transform )
        aop.aimCns(blade, parentDir, axis="xy", wupType=2, wupVector=[0,1,0], wupObject=self.root, maintainOffset=offset)
        # blade = ico.guideBladeIcon(parent=parentPos, name=self.getName(name),lenX=dist, color=13 )
        # aop.aimCns(blade, parentDir, axis="xy", wupType=2, wupVector=[0,1,0], wupObject=self.root, maintainOffset=False)
        pm.pointConstraint(parentPos, blade )

        return blade


    ## Add a display curve object to the guide.\n
    ## This mehod can initialize the object or draw it.\n
    ## Display curve object is a simple curve to show the connection between different guide element.
    # @param self
    # @param name String - Local name of the element.
    # @param centers List of X3DObject - List of parent of the curve.
    # @param degree Int4 - Curve degree.
    # return X3DObject - The created display curve.
    def addDispCurve(self, name, centers=[], degree=1):

        crv = cur.addCnsCurve(centers[0], self.getName(name), centers, degree)
        crv.attr("overrideEnabled").set(1)
        crv.attr("overrideDisplayType").set(1)


        return crv

    # ====================================================
    # MISC
    ##
    # @param self
    # @paran model
    # @return Dictionary of X3DObject

    def getObjects(self, model, includeShapes=True):

        objects = {}
        if includeShapes:
            children = pm.listRelatives(model, ad=True)
        else:
            children = pm.listRelatives(model, ad=True, typ='transform')
        pm.select(children)
        for child in pm.ls(self.fullName+"_*", selection=True):
            objects[child[child.index(self.fullName+"_")+len(self.fullName+"_"):]] = child

        return objects


    # @param self
    def addMinMax(self, name, minimum=1, maximum=-1):
        if "#" not in name:
            mgear.log("Invalid definition for min/max. You should have a '#' in the name", mgear.sev_error)
        self.minmax[name] = MinMax(minimum, maximum)

    ##
    # @param self
    # @return Double
    def getSize(self):

        # size
        size = .01
        for pos in self.apos:
            d = vec.getDistance(self.pos["root"], pos)
            size = max(size, d)
        size = max(size, .01)

        return size

    ## Return the fullname of given element of the component.
    # @param self
    # @param name String - Localname of the element.
    def getName(self, name):
        return self.fullName + "_" + name

    ## Return the fullname of the component.
    # @param self
    def getFullName(self):
        return self.values["comp_name"] + "_" + self.values["comp_side"] + str(self.values["comp_index"])

    ## Return the type of the component.
    # @param self
    def getType(self):
        return self.compType

    ##
    # @param self
    def getObjectNames(self):

        names = set()
        names.update(self.save_transform)
        names.update(self.save_primitive)
        names.update(self.save_blade)

        return names

    def getVersion(self):
        return ".".join([str(i) for i in self.version])

    fullName = property(getFullName)
    type = property(getType)
    objectNames = property(getObjectNames)

##########################################################
# OTHER CLASSES
##########################################################
class MinMax(object):

    def __init__(self, minimum=1, maximum=-1):
        self.min = minimum
        self.max = maximum