'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.rig.gui
# @author Jeremie Passerin, Miquel Campos
#

##########################################################
# GLOBAL
##########################################################
# Built-in
import os

from functools import partial

# pymel
import pymel.core as pm
import pymel.core.datatypes as dt

# mgear
import mgear
import mgear.maya.attribute as att
import mgear.maya.dag as dag
import mgear.maya.vector as vec

import mgear.maya.rig as rig



#
GUIDE_UI_WINDOW_NAME = "guide_UI_window"
GUIDE_DOCK_NAME = "Guide_Components"

COMPONENT_PATH = os.path.join(os.path.dirname(__file__), "component")
TEMPLATE_PATH = os.path.join(COMPONENT_PATH, "templates")
VERSION = 1.0


##############################
# CLASS
##############################
class Guide_UI(object):

    def __init__(self):

        # Remove existing window
        if pm.window(GUIDE_UI_WINDOW_NAME, exists=True):
            print "deleting win"
            pm.deleteUI(GUIDE_UI_WINDOW_NAME)


        if pm.dockControl( GUIDE_DOCK_NAME, exists=True):
            print "deleting dock"
            pm.deleteUI(GUIDE_DOCK_NAME)

        panelWeight = 200
        scrollHight = 720

        # Create Window and main tab
        self.ui_window = pm.window(GUIDE_UI_WINDOW_NAME, width=panelWeight, title="Guide Tools", sizeable=True)
        self.ui_topLevelColumn = pm.columnLayout(adjustableColumn=True, columnAlign="center")

        #
        pm.columnLayout()
        pm.rowLayout( numberOfColumns=1,  columnWidth=[(1, panelWeight)]  )
        pm.button(label="Properties", w=panelWeight, h=30, bgc=[.042, .351, .615], command=partial(self.inspectProperties)  )
        pm.setParent( '..' )
        pm.rowLayout( numberOfColumns=3,  columnWidth=[(1, (panelWeight/3)-1), (2, (panelWeight/3)-1), (3, (panelWeight/3)-1)] )
        pm.button(label="Dupl.", w=(panelWeight/3)-1, h=23, bgc=[.311, .635, 0], command=partial(self.duplicate, False)  )
        pm.button(label="Dupl. Sym", w=(panelWeight/3)-1, h=23, bgc=[.465, .785, .159], command=partial(self.duplicate, True)  )
        pm.button(label="Extr. Ctl", w=(panelWeight/3)-1, h=23, bgc=[.835, .792, .042], command=partial(self.extractControls)  )
        pm.setParent( '..' )
        pm.rowLayout( numberOfColumns=1,  columnWidth=[(1, panelWeight)]  )
        pm.button(label="Build from selection", w=panelWeight, h=30, bgc=[.912, .427, .176], command=partial(self.buildFromSelection)  )
        pm.setParent( '..' )

        self.ui_tabs = pm.tabLayout(width=panelWeight, innerMarginWidth=5, innerMarginHeight=5)
        tabWidth = pm.tabLayout(self.ui_tabs, q=True, width=True)

        #
        self.ui_compColumn = pm.columnLayout(adj=True, rs=3)
        self.ui_compFrameLayout = pm.frameLayout(height=scrollHight, collapsable=False, borderVisible=False, labelVisible=False)
        self.ui_compList_Scroll = pm.scrollLayout(hst=0)
        self.ui_compList_column = pm.columnLayout(columnWidth=panelWeight, adj=True, rs=2)
        pm.separator()

        # List of components
        import mgear.maya.rig.component as comp
        path = os.path.dirname(comp.__file__)
        for comp_name in os.listdir(path):

            if not os.path.exists(os.path.join(path, comp_name, "__init__.py")):
                continue

            # module = __import__("mgear.maya.rig.component."+comp_name, globals(), locals(), ["*"], -1)
            module = __import__("mgear.maya.rig.component."+comp_name+".guide", globals(), locals(), ["*"], -1)
            reload(module)
            image = os.path.join(path, comp_name, "icon.jpg")

            buttonSize = 25
            textDesc = "Name: "+module.NAME+"\nType:: "+module.TYPE+"\n===========\nAuthor: "+module.AUTHOR+"\nWeb: "+module.URL+\
                        "\nEmail: "+module.EMAIL+"\n===========\nDescription:\n"+module.DESCRIPTION

            row = pm.rowLayout(numberOfColumns=2, columnWidth=([1, buttonSize]), adjustableColumn=2, columnAttach=([1, "both", 0], [2, "both", 5]))
            pm.symbolButton(ann=textDesc, width=buttonSize, height=buttonSize, bgc=[0,0,0], ebg=False, i=image, command=partial(self.drawComp, module.TYPE))
            textColumn = pm.columnLayout(columnAlign="center")
            pm.text(align="center", width=panelWeight*.6, label=module.TYPE, ann=textDesc, fn="plainLabelFont")

            pm.setParent(self.ui_compList_column)
            pm.separator()

        # Display the window
        pm.tabLayout(self.ui_tabs, edit=True, tabLabelIndex=([1, "Components"]))
        allowedAreas = ['right', 'left']
        pm.dockControl( GUIDE_DOCK_NAME, area='right', content=self.ui_window, allowedArea=allowedAreas, width=panelWeight , s=True)

    def drawComp(self, compType, *args):

        guide = rig.RigGuide()

        if pm.selected():
            parent = pm.selected()[0]
        else:
            parent = None

        guide.drawNewComponent(parent, compType)

    def buildFromSelection(self, *args):

        print mgear.logInfos()
        rg = rig.Rig()
        rg.buildFromSelection()

    def duplicate(self, sym, *args):
        oSel = pm.selected()
        if oSel:
            root = oSel[0]
            guide = rig.RigGuide()
            guide.duplicate(root, sym)
        else:
            mgear.log("Select one component root to edit properties", mgear.sev_error)
            return


    def inspectProperties(self, *args):

        modeSet = ["FK", "IK", "IK/FK"]
        rotOrderSet = ["XYZ","YZX", "ZXY", "XZY", "YXZ", "ZYX"]
        guideModeSet = ["Final", "WIP"]

        # apply changes
        def applyCloseGuide( root, *args):
            if pm.attributeQuery("mode", node=root, ex=True):
                root.attr("mode").set(guideModeSet.index(pMode.getValue()))
            pm.select(root, r=True)

            pm.deleteUI(window, window=True)

        def applyCloseComp( root, *args):
            newName = pName.getText()
            newSide = pSide.getValue()
            newIndex = pIndex.getValue1()
            if pm.attributeQuery("mode", node=root, ex=True):
                root.attr("mode").set(modeSet.index(pMode.getValue()))
            if pm.attributeQuery("default_rotorder", node=root, ex=True):
                root.attr("default_rotorder").set(rotOrderSet.index(pRotOrder.getValue()))

            guide = rig.RigGuide()
            guide.updateProperties(root, newName, newSide, newIndex)
            pm.select(root, r=True)

            pm.deleteUI(window, window=True)

        if pm.window("compProperties", exists=True):
            pm.deleteUI("compProperties")


        oSel = pm.selected()
        if oSel:
            root = oSel[0]
        else:
            mgear.log("Select one root Guide or component to edit properties", mgear.sev_error)
            return


        if pm.attributeQuery("comp_type", node=root, ex=True):
            #property window constructor
            customAttr = pm.listAttr( root, ud=True)

            window = pm.window( title=root.name() )
            pm.columnLayout( adjustableColumn=True, cal="right" )

            for attr in customAttr:

                if attr == "comp_name":
                    fl = pm.formLayout()
                    oriVal = root.attr("comp_name").get()
                    pName = pm.textFieldGrp(l="comp_name")
                    pm.setParent( '..' )
                    pm.formLayout(fl, e=True, af=(pName, "left", 0))
                    pName.setText(oriVal)

                elif attr == "comp_side":
                    sideSet = ["C", "L", "R"]
                    fl = pm.formLayout()
                    pSide = pm.optionMenu(l="comp_side")
                    pSide.addMenuItems(sideSet)
                    pSide.setWidth(120)
                    pm.setParent( '..' )
                    pm.formLayout(fl, e=1, af=(pSide, "left", 90))
                    oriVal = root.attr("comp_side").get()
                    pSide.setValue(oriVal )
                    # if oriVal in sideSet:
                    #     pSide.setValue(oriVal )
                    # else:
                    #     pSide.setValue(sideSet[0])
                elif attr == "mode":
                    fl = pm.formLayout()
                    pMode = pm.optionMenu(l="mode")
                    pMode.addMenuItems(modeSet)
                    pMode.setWidth(120)
                    pm.setParent( '..' )
                    pm.formLayout(fl, e=1, af=(pMode, "left", 115))
                    oriVal = root.attr("mode").get()
                    pMode.setValue(modeSet[oriVal])

                elif attr == "default_rotorder":
                    fl = pm.formLayout()
                    pRotOrder = pm.optionMenu(l="default_rotorder")
                    pRotOrder.addMenuItems(rotOrderSet)
                    pRotOrder.setWidth(140)
                    pm.setParent( '..' )
                    pm.formLayout(fl, e=1, af=(pRotOrder, "left", 60))
                    oriVal = root.attr("default_rotorder").get()
                    pRotOrder.setValue(rotOrderSet[oriVal])

                elif attr == "comp_index":
                    fl = pm.formLayout()
                    oriVal = root.attr("comp_index").get()
                    pIndex = pm.intFieldGrp(v1=oriVal, l="comp_index")
                    pm.setParent( '..' )
                    pm.formLayout(fl, e=True, af=(pIndex, "left", 0))

                else:
                    editable = True
                    if attr == "comp_type":
                        editable = False
                    pm.columnLayout( cal="right" )
                    pm.attrControlGrp(label=attr, attribute= root.attr(attr), po=True, en=editable)
                    pm.setParent( '..' )

            pm.button( label='Apply', command=partial(applyCloseComp, root))
            pm.setParent( '..' )
            pm.showWindow( window )

        elif pm.attributeQuery("ismodel", node=root, ex=True):
            #property window constructor
            customAttr = pm.listAttr( root, ud=True)

            window = pm.window( title=root.name() )
            pm.columnLayout( adjustableColumn=True, cal="right" )

            for attr in customAttr:
                if attr.split("_")[-1] not in ["r", "g", "b"]:
                    if attr == "mode":
                        fl = pm.formLayout()
                        pMode = pm.optionMenu(l="mode")
                        pMode.addMenuItems(guideModeSet)
                        pMode.setWidth(120)
                        pm.setParent( '..' )
                        pm.formLayout(fl, e=1, af=(pMode, "left", 115))
                        oriVal = root.attr("mode").get()
                        print oriVal
                        pMode.setValue(guideModeSet[oriVal])
                    else:
                        pm.columnLayout( cal="right" )
                        pm.attrControlGrp(label=attr, attribute= root.attr(attr), po=True)
                        pm.setParent( '..' )

            pm.button( label='Apply', command=partial(applyCloseGuide, root ))
            pm.setParent( '..' )
            pm.showWindow( window )

        else:
            mgear.log("Select a root Guide or component to edit properties", mgear.sev_error)
            return

    def extractControls(self, *args):

        oSel = pm.selected()

        try:
            cGrp = pm.PyNode("controllers_org")
        except:
            cGrp = False
            mgear.log("Not controller group in the scene or the group is not unique", mgear.sev_error )
        for x in oSel:
            try:
                old = pm.PyNode(cGrp.name() + "|" + x.name().split("|")[-1])
                pm.delete(old)
            except:
                pass
            new = pm.duplicate(x)[0]
            pm.parent(new, cGrp, a=True)
            pm.rename(new, x.name())
            toDel = new.getChildren(type="transform")
            pm.delete(toDel)
            pm.sets( "rig_controllers_grp", remove=new )
