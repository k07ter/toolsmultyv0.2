'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.primitive
# @author Jeremie Passerin
#
#############################################
# GLOBAL
#############################################
# Maya
# from pymel.core import *
import pymel.core as pm
import pymel.core.datatypes as dt

# mgear
import mgear.maya.transform as tra
# import mgear.maya.icon as ico
# import mgear.maya.vector as vec

#############################################
# PRIMITIVE
#############################################
# ===========================================
# TRANSFORM
def addTransform(parent, name, m=dt.Matrix()):

    node = pm.PyNode(pm.createNode("transform", n=name))
    node.setTransformation(m)

    if parent is not None:
        parent.addChild(node)

    return node

def addTransformFromPos(parent, name, pos=dt.Vector(0,0,0)):

    node = pm.PyNode(pm.createNode("transform", n=name))
    node.setTranslation(pos, space="world")

    if parent is not None:
        parent.addChild(node)

    return node

# ===========================================
# LOCATOR
def addLocator(parent, name, m=dt.Matrix(), size=1):

    node = pm.PyNode(pm.createNode("locator")).getParent()
    node.rename(name)
    node.setTransformation(m)
    node.setAttr("localScale", size, size, size)

    if parent is not None:
        parent.addChild(node)

    return node

def addLocatorFromPos(parent, name, pos=dt.Vector(0,0,0), size=1):

    node = pm.PyNode(pm.createNode("locator")).getParent()
    node.rename(name)
    node.setTranslation(pos, space="world")
    node.setAttr("localScale", size, size, size)

    if parent is not None:
        parent.addChild(node)

    return node

# ===========================================
# JOINT
def addJoint(parent, name, m=dt.Matrix()):

    # I'm not using the joint() comand because this is parenting
    # the newly created joint to current selection which might not be desired
    node = pm.PyNode(pm.createNode("joint", n=name))
    node.setTransformation(m)

    if parent is not None:
        parent.addChild(node)

    return node

def addJointFromPos(parent, name, pos=dt.Vector(0,0,0)):

    # I'm not using the joint() comand because this is parenting
    # the newly created joint to current selection which might not be desired
    node = pm.PyNode(pm.createNode("joint", n=name))
    node.setTranslation(pos, space="world")

    if parent is not None:
        parent.addChild(node)

    return node

def add2DChain(parent, name, positions, normal, negate=False, vis=False):

    if not "%s" in name:
        name += "%s"

    transforms = tra.getChainTransform(positions, normal, negate)
    t = tra.setMatrixPosition(transforms[-1], positions[-1])
    transforms.append(t)

    chain = []
    for i, t in enumerate(transforms):
        node = addJoint(parent, name%i, t)
        node.setAttr("visibility", vis)
        chain.append(node)
        parent = node

    # moving rotation value to joint orient
    for i, jnt in enumerate(chain):

        if i == 0:
            jnt.setAttr("jointOrient", jnt.getAttr("rotate"))
        elif i == len(chain)-1:
            jnt.setAttr("jointOrient", 0, 0, 0)
        else:
            # This will fail if chain is not always oriented the same way (like Z chain)
            v0 = positions[i] - positions[i-1]
            v1 = positions[i+1] - positions[i]

            jnt.setAttr("jointOrient", 0, 0, dt.degrees(v0.angle(v1)))

        jnt.setAttr("rotate", 0, 0, 0)
        jnt.setAttr("radius", 1.5)

    return chain

# ===========================================
# IK HANDLE
def addIkHandle(parent, name, chn, solver="ikRPsolver"):

    node = pm.ikHandle(n=name, sj=chn[0], ee=chn[-1], solver=solver)[0]

    if parent is not None:
        parent.addChild(node)

    return node



