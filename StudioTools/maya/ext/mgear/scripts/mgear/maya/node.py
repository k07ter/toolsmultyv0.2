'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.node
# @author Jeremie Passerin, Miquel Campos
#
#############################################
# GLOBAL
#############################################
import pymel.core as pm
#############################################
# CREATE SIMPLE NODES
#############################################
# ===========================================
## Decompose Matrix
def createDecomposeMatrixNode(m):

    node = pm.createNode("decomposeMatrix")

    pm.connectAttr(m, node+".inputMatrix")

    return node

# ===========================================
## Distance Node
def createDistNode(objA, objB):

    node = pm.createNode("distanceBetween")

    dm_nodeA = pm.createNode("decomposeMatrix")
    dm_nodeB = pm.createNode("decomposeMatrix")

    pm.connectAttr(objA+".worldMatrix", dm_nodeA+".inputMatrix")
    pm.connectAttr(objB+".worldMatrix", dm_nodeB+".inputMatrix")

    pm.connectAttr(dm_nodeA+".outputTranslate", node+".point1")
    pm.connectAttr(dm_nodeB+".outputTranslate", node+".point2")

    return node

# ===========================================
## Condition Node
# operator index
#   ==  0
#   !=  1
#   >   2
#   >=  3
#   <   4
#   <=  5
def createConditionNode(firstTerm=False, secondTerm=False, operator=0, ifTrue=False, ifFalse=False):


    node = pm.createNode("condition")
    pm.setAttr(node+".operation", operator)
    if firstTerm:
        pm.connectAttr(firstTerm, node+".firstTerm")
    if secondTerm:
        pm.connectAttr(secondTerm, node+".secondTerm")
    if ifTrue:
        pm.connectAttr(ifTrue, node+".colorIfTrueR")
    if ifFalse:
        pm.connectAttr(ifFalse, node+".colorIfFalseR")

    return node

# ===========================================
## Blend Node
def createBlendNode(inputA, inputB, blender=.5):

    node = pm.createNode("blendColors")

    if not isinstance(inputA, list):
        inputA = [inputA]

    if not isinstance(inputB, list):
        inputB = [inputB]

    for item, s in zip(inputA, "RGB"):
        if isinstance(item, str) or isinstance(item, unicode) or isinstance(item, pm.Attribute):
            pm.connectAttr(item, node+".color1"+s)
        else:
            pm.setAttr(node+".color1"+s, item)

    for item, s in zip(inputB, "RGB"):
        if isinstance(item, str) or isinstance(item, unicode) or isinstance(item, pm.Attribute):
            pm.connectAttr(item, node+".color2"+s)
        else:
            pm.setAttr(node+".color2"+s, item)

    if isinstance(blender, str) or isinstance(blender, unicode) or isinstance(blender, pm.Attribute):
        pm.connectAttr(blender, node+".blender")
    else:
        pm.setAttr(node+".blender", blender)

    return node

# ===========================================
## pairBlend Node
def createPairBlend(inputA, inputB, blender=.5, rotInterpolation=0):
    node = pm.createNode("pairBlend")
    node.attr("rotInterpolation").set(rotInterpolation)

    pm.connectAttr(inputA+".translate", node+".inTranslate1")
    pm.connectAttr(inputB+".translate", node+".inTranslate2")
    pm.connectAttr(inputA+".rotate", node+".inRotate1")
    pm.connectAttr(inputB+".rotate", node+".inRotate2")

    if isinstance(blender, str) or isinstance(blender, unicode) or isinstance(blender, pm.Attribute):
        pm.connectAttr(blender, node+".weight")
    else:
        pm.setAttr(node+".weight", blender)
    return node

# ===========================================
## Reverse Node
def createReverseNode(input):

    node = pm.createNode("reverse")

    if not isinstance(input, list):
        input = [input]

    for item, s in zip(input, "XYZ"):
        if isinstance(item, str) or isinstance(item, unicode) or isinstance(item, pm.Attribute):
            pm.connectAttr(item, node+".input"+s)
        else:
            pm.setAttr(node+".input"+s, item)

    return node

# ===========================================
## CurveInfo Node
def createCurveInfoNode(crv):

    node = pm.createNode("curveInfo")

    shape = pm.listRelatives(crv, shapes=True)[0]

    pm.connectAttr(shape+".local", node+".inputCurve")

    return node

# ===========================================
## Add Node
def createAddNode(inputA, inputB):

    node = pm.createNode("addDoubleLinear")

    if isinstance(inputA, str) or isinstance(inputA, unicode) or isinstance(inputA, pm.Attribute):
        pm.connectAttr(inputA, node+".input1")
    else:
        pm.setAttr(node+".input1", inputA)

    if isinstance(inputB, str) or isinstance(inputB, unicode) or isinstance(inputB, pm.Attribute):
        pm.connectAttr(inputB, node+".input2")
    else:
        pm.setAttr(node+".input2", inputB)

    return node

# ===========================================
## Sub Node
def createSubNode(inputA, inputB):

    node = pm.createNode("addDoubleLinear")

    if isinstance(inputA, str) or isinstance(inputA, unicode) or isinstance(inputA, pm.Attribute):
        pm.connectAttr(inputA, node+".input1")
    else:
        pm.setAttr(node+".input1", inputA)

    if isinstance(inputB, str) or isinstance(inputB, unicode) or isinstance(inputB, pm.Attribute):
        neg_node = pm.createNode("multiplyDivide")
        pm.connectAttr(inputB, neg_node+".input1X")
        pm.setAttr(neg_node+".input2X", -1)
        pm.connectAttr(neg_node+".outputX", node+".input2")
    else:
        pm.setAttr(node+".input2", -inputB)

    return node

# ===========================================
## Multiply Node
def createMulNode(inputA, inputB):

    return createMulDivNode(inputA, inputB, 1)

# ===========================================
## Divide Node
def createDivNode(inputA, inputB):

    return createMulDivNode(inputA, inputB, 2)

# ===========================================
## MultiplyDivide Node
def createMulDivNode(inputA, inputB, operation=1):

    node = pm.createNode("multiplyDivide")
    pm.setAttr(node+".operation", operation)

    if not isinstance(inputA, list):
        inputA = [inputA]

    if not isinstance(inputB, list):
        inputB = [inputB]

    for item, s in zip(inputA, "XYZ"):
        if isinstance(item, str) or isinstance(item, unicode) or isinstance(item, pm.Attribute):
            pm.connectAttr(item, node+".input1"+s)
        else:
            pm.setAttr(node+".input1"+s, item)

    for item, s in zip(inputB, "XYZ"):
        if isinstance(item, str) or isinstance(item, unicode) or isinstance(item, pm.Attribute):
            pm.connectAttr(item, node+".input2"+s)
        else:
            pm.setAttr(node+".input2"+s, item)

    return node

# ===========================================
## Clamp Node
def createClampNode(input, in_min, in_max):

    node = pm.createNode("clamp")

    if not isinstance(input, list):
        input = [input]
    if not isinstance(in_min, list):
        in_min = [in_min]
    if not isinstance(in_max, list):
        in_max = [in_max]

    for in_item, min_item, max_item, s in zip(input, in_min, in_max, "RGB"):

        if isinstance(in_item, str) or isinstance(in_item, unicode) or isinstance(in_item, pm.Attribute):
            pm.connectAttr(in_item, node+".input"+s)
        else:
            pm.setAttr(node+".input"+s, in_item)

        if isinstance(min_item, str) or isinstance(min_item, unicode) or isinstance(min_item, pm.Attribute):
            pm.connectAttr(min_item, node+".min"+s)
        else:
            pm.setAttr(node+".min"+s, min_item)

        if isinstance(max_item, str) or isinstance(max_item, unicode) or isinstance(max_item, pm.Attribute):
            pm.connectAttr(max_item, node+".max"+s)
        else:
            pm.setAttr(node+".max"+s, max_item)

    return node

#############################################
# CREATE MULTI NODES
#############################################
# ===========================================
## Negate Node
def createNegateNodeMulti(name, inputs=[]):

    s = "XYZ"
    count=0
    i=0
    outputs = []
    for input in inputs:
        if count==0:
            real_name = name+"_"+str(i)
            node_name = pm.createNode("multiplyDivide", n=real_name)
            i+=1

        pm.connectAttr(input, node_name+".input1"+s[count], f=True)
        pm.setAttr(node_name+".input2"+s[count], -1)

        outputs.append(node_name+".output"+s[count])
        count = (count+1)%3

    return outputs

# ===========================================
## Add Node
def createAddNodeMulti(inputs=[]):

    outputs = [inputs[0]]

    for i, input in enumerate(inputs[1:]):
        node_name = pm.createNode("addDoubleLinear")

        if isinstance(outputs[-1], str) or isinstance(outputs[-1], unicode) or isinstance(outputs[-1], pm.Attribute):
            pm.connectAttr(outputs[-1], node_name+".input1", f=True)
        else:
            pm.setAttr(node_name+".input1", outputs[-1])

        if isinstance(input, str) or isinstance(input, unicode) or isinstance(input, pm.Attribute):
            pm.connectAttr(input, node_name+".input2", f=True)
        else:
            pm.setAttr(node_name+".input2", input)

        outputs.append(node_name+".output")

    return outputs

# ===========================================
## Mul Node
def createMulNodeMulti(name, inputs=[]):

    outputs = [inputs[0]]

    for i, input in enumerate(inputs[1:]):
        real_name = name+"_"+str(i)
        node_name = pm.createNode("multiplyDivide", n=real_name)
        pm.setAttr(node_name+".operation", 1)

        if isinstance(outputs[-1], str) or isinstance(outputs[-1], unicode) or isinstance(outputs[-1], pm.Attribute):
            pm.connectAttr(outputs[-1], node_name+".input1X", f=True)
        else:
            pm.setAttr(node_name+".input1X", outputs[-1])

        if isinstance(input, str) or isinstance(input, unicode) or isinstance(input, pm.Attribute):
            pm.connectAttr(input, node_name+".input2X", f=True)
        else:
            pm.setAttr(node_name+".input2X", input)

        outputs.append(node_name+".output")

    return outputs

# ===========================================
## Div Node
def createDivNodeMulti(name, inputs1=[], inputs2=[]):

    for i, input in enumerate(pm.inputs[1:]):
        real_name = name+"_"+str(i)
        node_name = pm.createNode("multiplyDivide", n=real_name)
        pm.setAttr(node_name+".operation", 2)

        if isinstance(pm.outputs[-1], str) or isinstance(pm.outputs[-1], unicode) or isinstance(pm.outputs[-1], pm.Attribute):
            pm.connectAttr(pm.outputs[-1], node_name+".input1X", f=True)
        else:
            pm.setAttr(node_name+".input1X", pm.outputs[-1])

        if isinstance(input, str) or isinstance(input, unicode) or isinstance(input, pm.Attribute):
            pm.connectAttr(input, node_name+".input2X", f=True)
        else:
            pm.setAttr(node_name+".input2X", input)

        pm.outputs.append(node_name+".output")

    return pm.outputs

# ===========================================
## Clamp Node
def createClampNodeMulti(name, inputs=[], in_min=[], in_max=[]):

    s = "RGB"
    count=0
    i=0
    outputs = []
    for input, min, max in zip(inputs, in_min, in_max):
        if count==0:
            real_name = name+"_"+str(i)
            node_name = pm.createNode("clamp", n=real_name)
            i+=1

        pm.connectAttr(input, node_name+".input"+s[count], f=True)

        if isinstance(min, str) or isinstance(min, unicode) or isinstance(min, pm.Attribute):
            pm.connectAttr(min, node_name+".min"+s[count], f=True)
        else:
            pm.setAttr(node_name+".min"+s[count], min)

        if isinstance(max, str) or isinstance(max, unicode) or isinstance(max, pm.Attribute):
            pm.connectAttr(max, node_name+".max"+s[count], f=True)
        else:
            pm.setAttr(node_name+".max"+s[count], max)

        outputs.append(node_name+".output"+s[count])
        count = (count+1)%3

    return outputs



