'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.applyop
# @author Jeremie Passerin, Miquel Campos
#
#############################################
# GLOBAL
#############################################
import pymel.core as pm


#############################################
# BUILT IN NODES
#############################################
#splineIK=================================================
## Apply a splineIK solver to a chain.
# @param name String - name of the new node.
# @param chn List of dagNode - List of joints. At less 2 joints should be in the list.
# @param parent dagNode - Parent for the ikHandle.
# @param cParent dagNode - Parent for the curve.
# @param curve dagNode- Specifies the curve to be used by the ikSplineHandle. This param is optional.
def splineIK(name, chn, parent=None, cParent=None, curve=None ):

    data = {}
    data["n"] = name
    data["solver"] = "ikSplineSolver"
    data["ccv"] = True
    data["startJoint"] = chn[0]
    data["endEffector"] = chn[-1]
    if curve is not None:
        data["curve"] = curve


    node, effector, splineCrv = pm.ikHandle(**data)
    node.setAttr("visibility", False)
    splineCrv.setAttr("visibility", False)
    pm.rename(splineCrv, name + "_crv")
    pm.rename(effector, name + "_eff")
    if parent is not None:
        parent.addChild(node)
    if cParent is not None:
        cParent.addChild(splineCrv)

    return node, splineCrv

# oriCons ================================================
## Apply orientation constraint changing XYZ  defalult conexions by rotate compound conexions
# NOTE: we found an evaluation diference in the values if the conexion is compound or by axis
# @param driver dagNode - driver object.
# @param driven dagNode - driver object.
# @param maintainOffset bool - keep the offset
# @return the orientation contraint node
def oriCns(driver, driven, maintainOffset=False):
    oriCns = pm.orientConstraint(driver, driven, maintainOffset=maintainOffset)
    for axis in ["X", "Y", "Z"]:
        pm.disconnectAttr(oriCns+".constraintRotate"+axis, driven+".rotate"+axis)
    pm.connectAttr(oriCns+".constraintRotate", driven+".rotate", f=True)

    return oriCns


# PathCns ================================================
## Apply a path constraint or curve constraint.
# @param obj dagNode - Constrained object.
# @param curve Nurbcurve - Constraining Curve.
# @param cnsType Integer - 0 for Path Constraint ; 1 for Curve Constraint (Parametric).
# @param u Double - Position of the object on the curve (from 0 to 100 for path constraint, from 0 to 1 for Curve cns).
# @param tangent Boolean - Active tangent.
# @param upv dagNode - Object that act as up vector.
# @param comp Boolean - Active constraint compensation.
# @return the newly created constraint.
def pathCns(obj, curve, cnsType=False, u=0, tangent=False):

    node = pm.PyNode(pm.createNode("motionPath"))
    node.setAttr("uValue", u)
    node.setAttr("fractionMode", not cnsType)
    node.setAttr("follow", tangent)

    pm.connectAttr(curve.attr("worldSpace"), node.attr("geometryPath"))
    pm.connectAttr(node.attr("allCoordinates"), obj.attr("translate"))
    pm.connectAttr(node.attr("rotate"), obj.attr("rotate"))
    pm.connectAttr(node.attr("rotateOrder"), obj.attr("rotateOrder"))
    pm.connectAttr(node.attr("message"), obj.attr("specifiedManipLocation"))

    return node

# ========================================================
## Apply a direction constraint
# @param obj dagNode - Constrained object.
# @param master dagNode - Constraining Object.
# @param upv dagNode - None of you don't want to use up vector
# @param comp Boolean - Active constraint compensation.
# @param axis String - Define pointing axis and upvector axis
# @return the newly created constraint.
def aimCns(obj, master, axis="xy", wupType=4, wupVector=[0,1,0], wupObject=None, maintainOffset=False):
    #TODO: review function to make wupObject optional

    node = pm.aimConstraint(master, obj, worldUpType=wupType, worldUpVector=wupVector, worldUpObject=wupObject, maintainOffset=maintainOffset)

    if axis == "xy": a = [1,0,0,0,1,0]
    elif axis == "xz": a = [1,0,0,0,0,1]
    elif axis == "yx": a = [0,1,0,1,0,0]
    elif axis == "yz": a = [0,1,0,0,0,1]
    elif axis == "zx": a = [0,0,1,1,0,0]
    elif axis == "zy": a = [0,0,1,0,1,0]

    elif axis == "-xy": a = [-1,0,0,0,1,0]
    elif axis == "-xz": a = [-1,0,0,0,0,1]
    elif axis == "-yx": a = [0,-1,0,1,0,0]
    elif axis == "-yz": a = [0,-1,0,0,0,1]
    elif axis == "-zx": a = [0,0,-1,1,0,0]
    elif axis == "-zy": a = [0,0,-1,0,1,0]

    elif axis == "x-y": a = [1,0,0,0,-1,0]
    elif axis == "x-z": a = [1,0,0,0,0,-1]
    elif axis == "y-x": a = [0,1,0,-1,0,0]
    elif axis == "y-z": a = [0,1,0,0,0,-1]
    elif axis == "z-x": a = [0,0,1,-1,0,0]
    elif axis == "z-y": a = [0,0,1,0,-1,0]

    elif axis == "-x-y": a = [-1,0,0,0,-1,0]
    elif axis == "-x-z": a = [-1,0,0,0,0,-1]
    elif axis == "-y-x": a = [0,-1,0,-1,0,0]
    elif axis == "-y-z": a = [0,-1,0,0,0,-1]
    elif axis == "-z-x": a = [0,0,-1,-1,0,0]
    elif axis == "-z-y": a = [0,0,-1,0,-1,0]

    for i, name in enumerate(["aimVectorX", "aimVectorY", "aimVectorZ", "upVectorX", "upVectorY", "upVectorZ"]):
        pm.setAttr(node+"."+name, a[i])

    return node

#############################################
# CUSTOM NODES
#############################################

## Apply mGear spring node.
# @param in_obj dagNode - Constrained object.
# @param goal dagNode - By default is False.
def gear_spring_op(in_obj, goal=False):


    if not goal:
        goal = in_obj

    node = pm.createNode("mgear_springNode")

    pm.connectAttr("time1.outTime", node+".time")
    dm_node = pm.createNode("decomposeMatrix")
    pm.connectAttr(goal+".parentMatrix", dm_node+".inputMatrix")
    pm.connectAttr(dm_node+".outputTranslate", node+".goal")

    cm_node = pm.createNode("composeMatrix")
    pm.connectAttr(node+".output", cm_node+".inputTranslate")

    mm_node = pm.createNode("mgear_mulMatrix")


    pm.connectAttr(cm_node+".outputMatrix", mm_node+".matrixA")
    pm.connectAttr(in_obj+".parentInverseMatrix", mm_node+".matrixB")

    dm_node2 = pm.createNode("decomposeMatrix")
    pm.connectAttr(mm_node+".output", dm_node2+".inputMatrix")
    pm.connectAttr(dm_node2+".outputTranslate", in_obj+".translate")

    pm.setAttr(node+".stiffness", 0.5)
    pm.setAttr(node+".damping", 0.5)

    return node


## Create mGear multiply Matrix node.
# @param mA matrix - input matrix A.
# @param mB matrix - input matrix B.
# @return node  - mGear_multMatrix node.
def gear_mulmatrix_op(mA, mB):


    node = pm.createNode("mgear_mulMatrix")


    pm.connectAttr(mA, node+".matrixA")
    pm.connectAttr(mB, node+".matrixB")

    return node

## create mGear interpolate Matrix node.
# @param mA matrix - input matrix A.
# @param mB matrix - input matrix B.
# @param blend float or connection - blending value
# @return node  - mGear_intMatrix node.
def gear_intmatrix_op(mA, mB, blend=0):

    node = pm.createNode("mgear_intMatrix")

    pm.connectAttr(mA, node+".matrixA")
    pm.connectAttr(mB, node+".matrixB")

    if isinstance(blend, str) or isinstance(blend, unicode) or isinstance(blend, pm.Attribute):
        pm.connectAttr(blend, node+".blend")
    else:
        pm.setAttr(node+".blend", blend)

    return node

## create mGear curvecns node.
# @param crv nurbsCurve - Nurbs curve.
# @param inputs List of dagNodes - input objecto to drive the curve. Should be same number as crv points. \n
# Also the order should be the same as the points .
# @return node - the curvecns node.
def gear_curvecns_op(crv, inputs=[]):

    pm.select(crv)
    node = pm.deformer(type="mgear_curveCns")[0]

    for i, item in enumerate(inputs):
        pm.connectAttr(item+".worldMatrix", node+".inputs[%s]"%i)

    return node

# gear_curveslide2_op =====================================
## Apply a sn_curveslide2_op operator
# @param outcrv NurbsCurve - Out Curve.
# @param incrv NurbsCurve - In Curve.
# @param position Double - Default position value (from 0 to 1).
# @param maxstretch Double - Default maxstretch value (from 1 to infinite).
# @param maxsquash Double - Default maxsquash value (from 0 to 1).
# @param softness Double - Default softness value (from 0 to 1).
# @return the newly created operator.
def gear_curveslide2_op(outcrv, incrv, position=0, maxstretch=1, maxsquash=1, softness=0):

    pm.select(outcrv)
    node = pm.deformer(type="mgear_slideCurve2")[0]

    pm.connectAttr(incrv+".local", node+".master_crv")
    pm.connectAttr(incrv+".worldMatrix", node+".master_mat")

    pm.setAttr(node+".master_length", pm.arclen(incrv))
    pm.setAttr(node+".slave_length", pm.arclen(incrv))
    pm.setAttr(node+".position", 0)
    pm.setAttr(node+".maxstretch", 1)
    pm.setAttr(node+".maxsquash", 1)
    pm.setAttr(node+".softness", 0)

    return node

# Spine Point At ========================================
## Apply a SpinePointAt operator
# @param cns Constraint - The constraint to apply the operator on (must be a curve, path or direction constraint)
# @param startobj dagNode - Start Reference.
# @param endobj dagNode -End Reference.
# @param blend Double - Blend influence value from 0 to 1.
# @return the newly created operator.
def gear_spinePointAtOp(cns, startobj, endobj, blend=.5, axis="-Z"):

    node = pm.createNode("mgear_spinePointAt")

    # Inputs
    pm.setAttr(node+".blend", blend)
    pm.setAttr(node+".axe", ["X", "Y", "Z", "-X", "-Y", "-Z"].index(axis))

    pm.connectAttr(startobj+".rotate", node+".rotA")
    pm.connectAttr(endobj+".rotate", node+".rotB")

    # Outputs
    pm.setAttr(cns+".worldUpType", 3)

    pm.connectAttr(node+".pointAt", cns+".worldUpVector")

    return node

# Spine Point At World Matrix ========================================
## Apply a SpinePointAt operator
# @param cns Constraint - The constraint to apply the operator on (must be a curve, path or direction constraint)
# @param startobj dagNode - Start Reference.
# @param endobj dagNode -End Reference.
# @param blend Double - Blend influence value from 0 to 1.
# @return the newly created operator.
def gear_spinePointAtOpWM(cns, startobj, endobj, blend=.5, axis="-Z"):

    node = pm.createNode("mgear_spinePointAt")

    # Inputs
    pm.setAttr(node+".blend", blend)
    pm.setAttr(node+".axe", ["X", "Y", "Z", "-X", "-Y", "-Z"].index(axis))

    dem_node1 = pm.createNode("decomposeMatrix")
    dem_node2 = pm.createNode("decomposeMatrix")
    pm.connectAttr(startobj+".worldMatrix", dem_node1+".inputMatrix")
    pm.connectAttr(endobj+".worldMatrix", dem_node2+".inputMatrix")



    pm.connectAttr(dem_node1+".outputRotate", node+".rotA")
    pm.connectAttr(dem_node2+".outputRotate", node+".rotB")

    # Outputs
    pm.setAttr(cns+".worldUpType", 3)

    pm.connectAttr(node+".pointAt", cns+".worldUpVector")

    return node

# ========================================================
## Apply a sn_ikfk2bone_op operator
# @param out List of dagNode - The constrained outputs order must be respected (BoneA, BoneB,  Center, CenterN, Eff), set it to None if you don't want one of the output.
# @param root dagNode - Object that will act as the root of the chain.
# @param eff dagNode - Object that will act as the eff controler of the chain.
# @param upv dagNode - Object that will act as the up vector of the chain.
# @param fk0 dagNode - Object that will act as the first fk controler of the chain.
# @param fk1 dagNode - Object that will act as the second fk controler of the chain.
# @param fk2 dagNode - Object that will act as the fk effector controler of the chain.
# @param lengthA Double - Length of first bone.
# @param lengthB Double - Length of second bone.
# @param negate Boolean - Use with negative Scale.
# @param blend Double - Default blend value (0 for full ik, 1 for full fk).
# @return the newly created operator.
def gear_ikfk2bone_op(out=[], root=None, eff=None, upv=None, fk0=None, fk1=None, fk2=None, lengthA=5, lengthB=3, negate=False, blend=0):

    node = pm.createNode("mgear_ikfk2Bone")

    # Inputs
    pm.setAttr(node+".lengthA", lengthA)
    pm.setAttr(node+".lengthB", lengthB)
    pm.setAttr(node+".negate", negate)
    pm.setAttr(node+".blend", blend)

    pm.connectAttr(root+".worldMatrix", node+".root")
    pm.connectAttr(eff+".worldMatrix", node+".ikref")
    pm.connectAttr(upv+".worldMatrix", node+".upv")
    pm.connectAttr(fk0+".worldMatrix", node+".fk0")
    pm.connectAttr(fk1+".worldMatrix", node+".fk1")
    pm.connectAttr(fk2+".worldMatrix", node+".fk2")


    # Outputs
    if out[0] is not None:
        pm.connectAttr(out[0]+".parentMatrix", node+".inAparent")

        dm_node = pm.createNode("decomposeMatrix")
        pm.connectAttr(node+".outA", dm_node+".inputMatrix")
        pm.connectAttr(dm_node+".outputTranslate", out[0]+".translate")
        pm.connectAttr(dm_node+".outputRotate", out[0]+".rotate")
        pm.connectAttr(dm_node+".outputScale", out[0]+".scale")

    if out[1] is not None:
        pm.connectAttr(out[1]+".parentMatrix", node+".inBparent")

        dm_node = pm.createNode("decomposeMatrix")
        pm.connectAttr(node+".outB", dm_node+".inputMatrix")
        pm.connectAttr(dm_node+".outputTranslate", out[1]+".translate")
        pm.connectAttr(dm_node+".outputRotate", out[1]+".rotate")
        pm.connectAttr(dm_node+".outputScale", out[1]+".scale")

    if out[2] is not None:
        pm.connectAttr(out[2]+".parentMatrix", node+".inCenterparent")

        dm_node = pm.createNode("decomposeMatrix")
        pm.connectAttr(node+".outCenter", dm_node+".inputMatrix")
        pm.connectAttr(dm_node+".outputTranslate", out[2]+".translate")
        pm.connectAttr(dm_node+".outputRotate", out[2]+".rotate")
        #connectAttr(dm_node+".outputScale", out[2]+".scale") # the scaling is not working with FK blended to 1. \
        #The output is from the solver I need to review the c++ solver

    if out[3] is not None:
        pm.connectAttr(out[3]+".parentMatrix", node+".inEffparent")

        dm_node = pm.createNode("decomposeMatrix")
        pm.connectAttr(node+".outEff", dm_node+".inputMatrix")
        pm.connectAttr(dm_node+".outputTranslate", out[3]+".translate")
        pm.connectAttr(dm_node+".outputRotate", out[3]+".rotate")
        pm.connectAttr(dm_node+".outputScale", out[3]+".scale")

    return node


# sn_rollsplinekine_op ==================================
## Apply a sn_rollsplinekine_op operator
# @param out dagNode - constrained Object.
# @param ctrl List of dagNode - Objects that will act as controler of the bezier curve. Objects must have a parent that will be used as an input for the operator.
# @param u Double - Position of the object on the bezier curve (from 0 to 1).
# @return the newly created operator.
def gear_rollsplinekine_op(out, controlers=[], u=.5, subdiv=10):

    node = pm.createNode("mgear_rollSplineKine")

    # Inputs
    pm.setAttr(node+".u", u)
    pm.setAttr(node+".subdiv", subdiv)

    dm_node = pm.createNode("decomposeMatrix")

    pm.connectAttr(node+".output", dm_node+".inputMatrix")
    pm.connectAttr(dm_node+".outputTranslate", out+".translate")
    pm.connectAttr(dm_node+".outputRotate", out+".rotate")
    # connectAttr(dm_node+".outputScale", out+".scale")

    pm.connectAttr(out+".parentMatrix", node+".outputParent")

    for i, obj in enumerate(controlers):
        pm.connectAttr(obj+".parentMatrix", node+".ctlParent[%s]"%i)

        pm.connectAttr(obj+".worldMatrix", node+".inputs[%s]"%i)
        pm.connectAttr(obj+".rx", node+".inputsRoll[%s]"%i)

    return node

# gear_squashstretch2_op ==================================
## Apply a sn_squashstretch2_op operator
# @param out dagNode - Constrained object.
# @param sclref dagNode - Global scaling reference object.
# @param length Double - Rest Length of the S&S.
# @param axis String - 'x' for scale all except x axis...
# @return the newly created operator.
def gear_squashstretch2_op(out, sclref=None, length=5, axis="x"):

    node = pm.createNode("mgear_squashStretch2")

    pm.setAttr(node+".global_scaleX", 1)
    pm.setAttr(node+".global_scaleY", 1)
    pm.setAttr(node+".global_scaleZ", 1)
    pm.setAttr(node+".driver_ctr", length)
    pm.setAttr(node+".driver_max", length * 2)
    pm.setAttr(node+".axis", "xyz".index(axis))

    pm.connectAttr(node+".output", out+".scale")

    if sclref is not None:
        dm_node = pm.createNode("decomposeMatrix")
        pm.connectAttr(sclref+".worldMatrix", dm_node+".inputMatrix")
        pm.connectAttr(dm_node+".outputScale", node+".global_scale")

    return node

# gear_inverseRotorder_op =====================================
## Apply a sn_inverseRotorder_op operator
# @param out_obj dagNode - output object
# @param in_obj dagNode - input object
# @return the newly created operator.
def gear_inverseRotorder_op(out_obj, in_obj):

    node = pm.createNode("mgear_inverseRotOrder")

    pm.connectAttr(in_obj+".ro", node+".ro")
    pm.connectAttr(node+".output", out_obj+".ro")

    return node
