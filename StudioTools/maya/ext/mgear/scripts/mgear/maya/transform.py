'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.transform
# @author Jeremie Passerin, Miquel Campos
#
#############################################
# GLOBAL
#############################################
import math


from pymel import util as pmu
import pymel.core as pm
import pymel.core.datatypes as dt
import pymel.core.nodetypes as nt
import maya.OpenMaya as om

import mgear.maya.vector as vec

#############################################
# TRANSFORM
#############################################
def getTranslation(node, worldSpace=True):
    return node.getTranslation(space="world")

def getTransform(node, worldSpace=True):
    return node.getMatrix(worldSpace=True)

def getTransformLookingAt(pos, lookat, normal, axis="xy", negate=False):

    normal.normalize()

    if negate:
        a = pos - lookat
        # normal *= -1
    else:
        a = lookat - pos

    a.normalize()
    c = pmu.cross(a, normal)
    c.normalize()
    b = pmu.cross(c, a)
    b.normalize()

    if axis == "xy":
        X = a
        Y = b
        Z = c
    elif axis == "xz":
        X = a
        Z = b
        Y = -c
    elif axis == "yx":
        Y = a
        X = b
        Z = -c
    elif axis == "yz":
        Y = a
        Z = b
        X = c
    elif axis == "zx":
        Z = a
        X = b
        Y = c
    elif axis == "zy":
        Z = a
        Y = b
        X = -c

    elif axis == "x-y":
        X = a
        Y = -b
        Z = -c
    if axis == "-xy":
        X = -a
        Y = b
        Z = c

    m = dt.Matrix()
    m[0] = [X[0], X[1], X[2], 0.0]
    m[1] = [Y[0], Y[1], Y[2], 0.0]
    m[2] = [Z[0], Z[1], Z[2], 0.0]
    m[3] = [pos[0], pos[1], pos[2], 1.0]

    return m

# ===========================================================
def getChainTransform(positions, normal, negate=False):

    # Draw
    transforms = []
    for i in range(len(positions)-1):
        v0 = positions[i-1]
        v1 = positions[i]
        v2 = positions[i+1]

        # Normal Offset
        if i > 0:
            normal = vec.getTransposedVector(normal, [v0, v1], [v1, v2])

        t = getTransformLookingAt(v1, v2, normal, "xz", negate)
        transforms.append(t)

    return transforms

# getChainTransform2 is using the latest position on the chain
def getChainTransform2(positions, normal, negate=False):

    # Draw
    transforms = []
    for i in range(len(positions)):
        if i == len(positions)-1:
            v0 = positions[i-1]
            v1 = positions[i]
            v2 = positions[i-2]

        else:
            v0 = positions[i-1]
            v1 = positions[i]
            v2 = positions[i+1]

        # Normal Offset
        if i > 0:
            normal = vec.getTransposedVector(normal, [v0, v1], [v1, v2])

        if i == len(positions)-1:
            t = getTransformLookingAt(v1, normal, v2, "yz", negate)
        else:
            t = getTransformLookingAt(v1, v2, normal, "xz", negate)
        transforms.append(t)

    return transforms

def getTransformFromPos(pos):

    m = dt.Matrix()
    m[0] = [1.0, 0, 0, 0.0]
    m[1] = [0, 1.0, 0, 0.0]
    m[2] = [0, 0, 1.0, 0.0]
    m[3] = [pos[0], pos[1], pos[2], 1.0]

    return m

def getOffsetPosition(parent, offset=[0,0,0]):
    offsetVec = dt.Vector(offset[0],offset[1],offset[2])
    return offsetVec + parent.getTranslation(space="world")

def getPositionFromMatrix(in_m):

    pos = in_m[3][:3]

    return pos

def setMatrixPosition(in_m, pos):

    m = dt.Matrix()
    m[0] = in_m[0]
    m[1] = in_m[1]
    m[2] = in_m[2]
    m[3] = [pos[0], pos[1], pos[2], 1.0]

    return m

def setMatrixRotation(m, rot):

    # for v in rot:
        # v.normalize()

    X = rot[0]
    Y = rot[1]
    Z = rot[2]

    m[0] = [X[0], X[1], X[2], 0.0]
    m[1] = [Y[0], Y[1], Y[2], 0.0]
    m[2] = [Z[0], Z[1], Z[2], 0.0]

    return m

# filterTransform ==========================================
## Retrieve a transformation filtered.
# @param t SITransformation - Reference transformation.
# @param translation Boolean - True to match translation.
# @param rotation Boolean - True to match rotation.
# @param scaling Boolean - True to match scaling.
# @return SITransformation - The filtered transformation
def getFilteredTransform(m, translation=True, rotation=True, scaling=True):

    t = dt.Vector(m[3][0],m[3][1],m[3][2])
    x = dt.Vector(m[0][0],m[0][1],m[0][2])
    y = dt.Vector(m[1][0],m[1][1],m[1][2])
    z = dt.Vector(m[2][0],m[2][1],m[2][2])

    out = dt.Matrix()

    if translation:
        out = setMatrixPosition(out, t)

    if rotation and scaling:
        out = setMatrixRotation(out, [x,y,z])
    elif rotation and not scaling:
        out = setMatrixRotation(out, [x.normal(), y.normal(), z.normal()])
    elif not rotation and scaling:
        out = setMatrixRotation(out, [dt.Vector(1,0,0) * x.length(), dt.Vector(0,1,0) * y.length(), dt.Vector(0,0,1) * z.length()])

    return out

##########################################################
# ROTATION
##########################################################

# setRefPose =============================================
def getRotationFromAxis(in_a, in_b, axis="xy", negate=False):

    a = dt.Vector(in_a.x, in_a.y, in_a.z)
    b = dt.Vector(in_b.x, in_b.y, in_b.z)
    c = dt.Vector()

    if negate:
        a *= -1

    a.normalize()
    c = a ^ b
    c.normalize()
    b = c ^ a
    b.normalize()

    if axis == "xy":
      x = a
      y = b
      z = c
    elif axis == "xz":
      x = a
      z = b
      y = -c
    elif axis == "yx":
      y = a
      x = b
      z = -c
    elif axis == "yz":
      y = a
      z = b
      x = c
    elif axis == "zx":
      z = a
      x = b
      y = c
    elif axis == "zy":
      z = a
      y = b
      x = -c

    m = dt.Matrix()
    setMatrixRotation(m, [x,y,z])

    return m

def getSymmetricalTransform(t, axis="yz", fNegScale=False):

    if axis == "yz":
        mirror =   dt.TransformationMatrix(-1,0,0,0,
                                            0,1,0,0,
                                            0,0,1,0,
                                            0,0,0,1)

    if axis == "xy":
        mirror =   dt.TransformationMatrix(1,0,0,0,
                                            0,1,0,0,
                                            0,0,-1,0,
                                            0,0,0,1)
    if axis == "zx":
        mirror =   dt.TransformationMatrix(1,0,0,0,
                                            0,-1,0,0,
                                            0,0,1,0,
                                            0,0,0,1)
    t *= mirror

    #TODO: getSymmetricalTransform: add freeze negative scaling procedure.

    return t

def resetTransform(node, t=True, r=True, s=True):

    trsDic = {"tx":0, "ty":0, "tz":0, "rx":0, "ry":0, "rz":0, "sx":1, "sy":1, "sz":1}
    tAxis = ["tx", "ty", "tz"]
    rAxis = ["rx", "ry", "rz"]
    sAxis = ["sx", "sy", "sz"]
    axis = []

    if t: axis =  axis + tAxis
    if t: axis =  axis + rAxis
    if t: axis =  axis + sAxis

    for a in axis:
        try: node.attr(a).set(trsDic[a])
        except: pass



#matching functions
def matchWorldTransform(source, target):

    sWM = source.getMatrix(worldSpace=True)
    target.setMatrix(sWM, worldSpace=True)

def quaternionDotProd(q1, q2):

    dot = q1.x * q2.x + q1.y * q2.y + q1.z * q2.z + q1.w * q2.w
    return dot

def quaternionSlerp(q1, q2, blend):

    dot = quaternionDotProd(q1, q2)
    if dot < 0.0:
        dot = quaternionDotProd(q1, q2.negateIt())

    arcos = math.acos(round(dot, 10))
    sin = math.sin(arcos)

    if sin > 0.001:
        w1 = math.sin((1.0 - blend) * arcos) / sin
        w2 = math.sin(blend * arcos) / sin
    else:
        w1 = 1.0 - blend
        w2 = blend

    result = dt.Quaternion(q1).scaleIt(w1) + dt.Quaternion(q2).scaleIt(w2)

    return result



def convert2TransformMatrix(tm):

    if isinstance(tm, nt.Transform):
        tm = dt.TransformationMatrix(tm.getMatrix(worldSpace=True))
    if isinstance(tm, dt.Matrix):
        tm = dt.TransformationMatrix(tm)

    return tm

#interpolate transform
def getInterpolateTransformMatrix(t1, t2, blend=.5 ):

        #check if the input transforms are transformMatrix
        t1 = convert2TransformMatrix(t1)
        t2 = convert2TransformMatrix(t2)

        if (blend == 1.0):
            return t2
        elif (blend == 0.0):
            return t1

        # translate
        pos = vec.linearlyInterpolate(t1.getTranslation(space="world"),
                t2.getTranslation(space="world"), blend)

        # scale
        scaleA = dt.Vector(*t1.getScale(space="world"))
        scaleB = dt.Vector(*t2.getScale(space="world"))


        vs = vec.linearlyInterpolate(scaleA, scaleB, blend)

        # rotate
        q = quaternionSlerp(dt.Quaternion(t1.getRotationQuaternion()),
            dt.Quaternion(t2.getRotationQuaternion()), blend)

        # out
        result = dt.TransformationMatrix()

        result.setTranslation(pos, space="world")
        result.setRotationQuaternion(q.x, q.y, q.z, q.w)
        result.setScale([vs.x, vs.y, vs.z], space="world")

        return result