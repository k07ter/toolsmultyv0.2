'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26


To load the menu:

import mGear_menu
mGear_menu.CreateMenu()

'''

from functools import partial
import pymel.core as pm


#mGear modules
import mGear_riggingTools
import mGear_postSpring
import mGear_proxySlicer
import mGear_rope
import mGear_guidesTemplates

import mgear
import mgear.maya.synoptic as syn
import mgear.maya.skin as skin



def CreateMenu():

    if pm.menu('mGear', exists=1):
        pm.deleteUI('mGear')
    mGearM = pm.menu('mGear', p='MayaWindow', tearOff=1, allowOptionBoxes=1, label='mGear')


    ## Rigging Tools
    riggingM = pm.menuItem(parent='mGear', subMenu=True, tearOff=True, label='Rigging')
    pm.menuItem(label="Add NPO", command=partial(mGear_riggingTools.addNPO))
    pm.menuItem(label="Add SHD", command=partial(mGear_riggingTools.addShd, False, False))
    pm.setParent(riggingM, menu=True)
    pm.menuItem( divider=True )
    # pm.menuItem(subMenu=True, tearOff=True, label='Transform')
    pm.menuItem(label="Match All Trasform", command=partial(mGear_riggingTools.matchWorldXform))
    pm.setParent(riggingM, menu=True)
    pm.menuItem( divider=True )
    pm.menuItem(subMenu=True, tearOff=True, label='CTL as Parent')
    pm.menuItem(label="Square", command=partial(mGear_riggingTools.createCTL, "square"))
    pm.menuItem(label="Circle", command=partial(mGear_riggingTools.createCTL, "circle"))
    pm.menuItem(label="Cube", command=partial(mGear_riggingTools.createCTL, "cube"))
    pm.menuItem(label="Diamond", command=partial(mGear_riggingTools.createCTL, "diamond"))
    pm.menuItem(label="Sphere", command=partial(mGear_riggingTools.createCTL, "sphere"))
    pm.menuItem(label="Cross", command=partial(mGear_riggingTools.createCTL, "cross"))
    pm.menuItem(label="Cross Arrow", command=partial(mGear_riggingTools.createCTL, "crossarrow"))
    pm.menuItem(label="Pyramid", command=partial(mGear_riggingTools.createCTL, "pyramid"))
    pm.menuItem(label="Cube With Peak", command=partial(mGear_riggingTools.createCTL, "cubewithpeak"))
    pm.setParent(riggingM, menu=True)
    pm.menuItem(subMenu=True, tearOff=True, label='CTL as Child')
    pm.menuItem(label="Square", command=partial(mGear_riggingTools.createCTL, "square", True))
    pm.menuItem(label="Circle", command=partial(mGear_riggingTools.createCTL, "circle", True))
    pm.menuItem(label="Cube", command=partial(mGear_riggingTools.createCTL, "cube", True))
    pm.menuItem(label="Diamond", command=partial(mGear_riggingTools.createCTL, "diamond", True))
    pm.menuItem(label="Sphere", command=partial(mGear_riggingTools.createCTL, "sphere", True))
    pm.menuItem(label="Cross", command=partial(mGear_riggingTools.createCTL, "cross", True))
    pm.menuItem(label="Cross Arrow", command=partial(mGear_riggingTools.createCTL, "crossarrow", True))
    pm.menuItem(label="Pyramid", command=partial(mGear_riggingTools.createCTL, "pyramid", True))
    pm.menuItem(label="Cube With Peak", command=partial(mGear_riggingTools.createCTL, "cubewithpeak", True))
    pm.setParent(riggingM, menu=True)
    pm.menuItem( divider=True )
    pm.menuItem(label="Duplicate symmetrical", command=partial(mGear_riggingTools.duplicateSym))
    pm.menuItem( divider=True )
    pm.menuItem(label="Spring", command=partial(mGear_postSpring.spring_UI))
    pm.menuItem(label="Rope", command=partial(mGear_rope.rope_UI))

    ## Guides and templates tools
    guidesM = pm.menuItem(parent='mGear', subMenu=True, tearOff=True, label='Guides and Templates')
    pm.menuItem(label="Guides UI", command=partial(mGear_guidesTemplates.guideUI))
    pm.menuItem( divider=True )
    pm.menuItem(label="Build From Selection", command=partial(mGear_guidesTemplates.buildFromSelection))
    pm.menuItem( divider=True )
    pm.menuItem(label="Import Biped Guide", command=partial(mGear_guidesTemplates.bipedGuide))


    ## skinning tools
    skinM = pm.menuItem(parent='mGear', subMenu=True, tearOff=True, label='Skinning')
    pm.menuItem(label="Copy Skin", command=partial(skin.skinCopy, None, None))
    pm.menuItem(label="Select Skin Deformers", command=partial(skin.selectDeformers))


    ## Modeling tools
    modelM = pm.menuItem(parent='mGear', subMenu=True, tearOff=True, label='Modeling')
    pm.menuItem(label="Proxy Slicer", command=partial(mGear_proxySlicer.slice))

    ## Animation Tools
    animationM = pm.menuItem(parent='mGear', subMenu=True, tearOff=True, label='Animation')
    pm.menuItem(label="Synoptic", command=partial(syn.open))


    ## util Tools
    pm.setParent(mGearM, menu=True)
    pm.menuItem( divider=True )
    utilM = pm.menuItem(parent='mGear', subMenu=True, tearOff=True, label='Utilities')
    pm.menuItem(label="reload", command=partial(mgear.reloadModule, "mgear"))

