'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miqueltd.com  www.miqueltd.com
    Date:       2014 / 11 / 26

'''
import pymel.core as pm
import mgear
import mgear.maya.icon as ic
import mgear.maya.applyop as aop
import mgear.maya.node as nod
import mgear.maya.transform as tra
import mgear.maya.utils as uti



def addNPO(*args):
    oSel = pm.selected()
    for obj in oSel:
        oParent = obj.getParent()
        oTra = pm.PyNode(pm.createNode("transform", n= obj.name() + "_npo", p=oParent, ss=True))
        oTra.setTransformation(obj.getMatrix())
        pm.parent(obj, oTra)


def selectDeformers(*args):
    oSel = pm.selected()[0]
    oColl = pm.skinCluster(oSel, query=True, influence=True)
    pm.select(oColl)


#Icon creator

def createCTL(type = "square", child=False, *args):
    iconList =[]
    if child:
        if len(pm.selected()) > 0:
            for x in pm.selected():
                oChilds = [item for item in x.listRelatives(ad=True, type="transform")  if item.longName().split("|")[-2] == x.name()]

                icon = ic.create(None, x.name() + "_ctl",None, [1, 0, 0], type)
                iconList.append(icon)
                icon.setTransformation(x.getMatrix(worldSpace=True))
                pm.parent(icon, x)
                for child in oChilds:

                    pm.parent(child, icon)
        else:

           icon = ic.create(None, type + "_ctl", pm.datatypes.Matrix(), [1, 0, 0], type)
           iconList.append(icon)
    else:
        if len(pm.selected()) > 0:
            for x in pm.selected():
                oParent = x.getParent()
                icon = ic.create(oParent, x.name() + "_ctl", x.getMatrix(), [1, 0, 0], type)
                iconList.append(icon)
                icon.setTransformation(x.getMatrix())
                pm.parent(x, icon)
        else:

           icon = ic.create(None, type + "_ctl", pm.datatypes.Matrix(), [1, 0, 0], type)
           iconList.append(icon)

    try:
        defSet = pm.PyNode("rig_controlers_grp")
        for ico in iconList:
            pm.sets(defSet, add=ico)
    except:
        print "not rig_controlers_grp found"
        pass

def addShd(obj=False, parent=False, *args):
    if not obj:
        oSel = pm.selected()
    else:
        oSel = [obj]

    for obj in oSel:
        if not parent:
            try:
                parent = pm.PyNode("shd_org")
            except:
                parent = obj

        shd = pm.PyNode(pm.createNode("joint", n=obj.name()+"_shd"))

        try:
            defSet = pm.PyNode("rig_deformers_grp")
            pm.sets(defSet, add=shd)
        except:
            pm.sets(n="rig_deformers_grp")
            defSet = pm.PyNode("rig_deformers_grp")
            pm.sets(defSet, add=shd)

        parent.addChild(shd)

        shd.setAttr("jointOrient", 0, 0, 0)
        mulmat_node = aop.gear_mulmatrix_op(obj+".worldMatrix", shd+".parentInverseMatrix")
        dm_node = nod.createDecomposeMatrixNode(mulmat_node+".output")
        pm.connectAttr(dm_node+".outputTranslate", shd+".t")
        pm.connectAttr(dm_node+".outputRotate", shd+".r")
        pm.connectAttr(dm_node+".outputScale", shd+".s")


def duplicateSym(*args):
    oSelection = pm.selected()
    if oSelection:
        oSel = oSelection[0]
        oTarget = pm.duplicate()[0]

        t = oSel.getTransformation()
        t = tra.getSymmetricalTransform(t)
        oTarget.setTransformation(t)

        #Quick rename
        pm.select(oTarget, hi=True)

        for x in pm.selected():
            x.rename(uti.convertRLName(x.name().split("|")[-1]))
        oTarget.rename(uti.convertRLName(oSel.name()))
    else:
        pm.displayWarning("Select something before duplicate symmetry.")


def matchWorldXform(*args):
    if len(pm.selected()) !=2:
        mgear.log( "2 objects must be selected. Source and Target transform", mgear.sev_warning)
    else:
        source, target = pm.selected()
        tra.matchWorldTransform(source, target)

def connectLocalTransfrom(*args):
    source = pm.selected()[0]
    target = pm.selected()[1]

    pm.connectAttr(source + ".translate", target + ".translate")
    pm.connectAttr(source + ".scale", target + ".scale")
    pm.connectAttr(source + ".rotate", target + ".rotate")

def replaceShape(*args):

    oSel =  pm.selected()

    if len(oSel) !=2:
        print "2 objects must be selected"
    else:
        source = oSel[0]
        source2 = pm.duplicate(source)[0]
        target = oSel[1]
        shape = target.getShape()
        pm.delete(shape)
        pm.parent(source2.getShape(), target, r=True, s=True)

        pm.rename( target.getShape(), target.name() + "Shape" )

        pm.delete(source2)
