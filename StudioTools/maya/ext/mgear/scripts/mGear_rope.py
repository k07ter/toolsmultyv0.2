'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''
import pymel.core as pm


import mgear.maya.applyop as aop

import mGear_riggingTools as rt


def rope(DEF_nb=10, ropeName="rope",  keepRatio=False, ctls=False, offCtls=False, ctlsShape="cube", lvlType="transform"):

    if len( pm.selected()) !=2:
        print "You need to select 2 nurbsCurve"
        return
    oCrv = pm.selected()[0]
    oCrvUpV = pm.selected()[1]
    if oCrv.getShape().type() != "nurbsCurve" or  oCrvUpV.getShape().type() != "nurbsCurve":
        print "One of the selected objects is not of type: 'nurbsCurve'"
        print oCrv.getShape().type()
        print  oCrvUpV.getShape().type()
        return
    if keepRatio:
        oCrv = pm.selected()[0]
        arclen_node = pm.arclen(oCrv, ch=True)
        alAttr = pm.getAttr(arclen_node + ".arcLength")
        muldiv_node =  pm.createNode("multiplyDivide")
        pm.connectAttr(arclen_node+".arcLength", muldiv_node+".input1X")
        pm.setAttr(muldiv_node+".input2X", alAttr)
        pm.setAttr(muldiv_node+".operation", 2)
        pm.addAttr(oCrv, ln="length_ratio", k=True, w=True)
        pm.connectAttr(muldiv_node+".outputX", oCrv+".length_ratio")

    root =   pm.PyNode(pm.createNode(lvlType, n= ropeName + "_root", ss=True))
    step = 1.000/(DEF_nb -1)
    i = 0.000
    for x in range(DEF_nb):

        oTransUpV = pm.PyNode(pm.createNode(lvlType, n= ropeName + str(x).zfill(3) + "_upv", p=root, ss=True))
        oTrans = pm.PyNode(pm.createNode(lvlType, n= ropeName + str(x).zfill(3) + "_lvl", p=root, ss=True))

        cnsUpv = aop.pathCns(oTransUpV, oCrvUpV, cnsType=False, u=i, tangent=False)
        cns = aop.pathCns(oTrans, oCrv, cnsType=False, u=i, tangent=False)

        if keepRatio:
            muldiv_node2 =  pm.createNode("multiplyDivide")
            condition_node =  pm.createNode("condition")
            pm.setAttr(muldiv_node2+".operation", 2)
            pm.setAttr(muldiv_node2+".input1X", i)
            pm.connectAttr(oCrv+".length_ratio", muldiv_node2+".input2X")
            pm.connectAttr(muldiv_node2+".outputX", condition_node+".colorIfFalseR")
            pm.connectAttr(muldiv_node2+".outputX", condition_node+".secondTerm")
            pm.connectAttr(muldiv_node2+".input1X", condition_node+".colorIfTrueR")
            pm.connectAttr(muldiv_node2+".input1X", condition_node+".firstTerm")
            pm.setAttr(condition_node+".operation", 4)


            pm.connectAttr(condition_node+".outColorR", cnsUpv+".uValue")
            pm.connectAttr(condition_node+".outColorR", cns+".uValue")

        cns.setAttr("worldUpType", 1)
        cns.setAttr("frontAxis", 0)
        cns.setAttr("upAxis", 1)

        pm.connectAttr(oTransUpV.attr("worldMatrix[0]"),cns.attr("worldUpMatrix"))
        rt.addShd(oTrans)
        i += step

def rope_UI(*args):

    if pm.window("mGear_rope_window", exists = True):
        pm.deleteUI("mGear_rope_window")

    window = pm.window("mGear_rope_window", title="mGear rope rig generator", w=350, h=150, mxb=False, sizeable=False)

    pm.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 100), (2, 250)] )


    pm.text("Nb of deformers: ")
    nbDeformers = pm.intField( "nbDeformers", annotation="number of deformers", w=50, value= 10)
    pm.text(label="Keep position " )
    pm.checkBox("keepRatio", label=" (base on ratio) ")
    pm.text(label="Name: " )
    RopeName = pm.textField("RopeName", text="Rope")
    # pm.text(label="Controls: " )
    # pm.checkBox("controls", label="add control x each crv point ")
    # pm.text(label="Offset Controls: " )
    # pm.checkBox("offsetControls", label="add layer controls to move in global xform")
    # pm.text(label="Controls Shape: " )
    # pm.optionMenu( "controlShape")
    # pm.menuItem( label='Cube' )
    # pm.menuItem( label='Sphere' )




    pm.separator(h=10)
    pm.button(label="Build the rope!", w=150, h=50,  command=build_rope)
    pm.separator(h=10)
    pm.separator(h=10)
    pm.separator(h=10)
    pm.text(label="Instructions:  Select ctl crv + upv crv", align="left" )


    pm.showWindow(window)

def build_rope(*args):
        DEF_nb = pm.intField("nbDeformers", q=True, v=True)
        ropeName = pm.textField("RopeName", q=True, text=True)
        # ctls = pm.checkBox("controls", q=True, v=True)
        # offCtls = pm.checkBox("offsetControls", q=True, v=True)
        # ctlsShape = pm.optionMenu("controlShape", q=True, v=True)
        #mGear_rope(DEF_nb, ropeName, ctls, offCtls, ctlsShape)
        keepRatio = pm.checkBox("keepRatio", q=True, v=True)
        rope(DEF_nb, ropeName, keepRatio)

