'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

import pymel.core as pm


import mgear.maya.attribute as att
import mgear.maya.applyop as aop


def postSpring(dist = 5, hostUI = False,  hostUI2 = False, invertX=False ):

    oSel = pm.selected()

    if not hostUI2:
            hostUI2 = oSel[0]

    aSpring_active = att.addAttribute(hostUI2, "spring_active_%s"%oSel[0].name(), "double", 1.0, "___spring_active_______%s"%oSel[0].name(), "spring_active_%s"%oSel[0].name(), 0, 1)
    aSpring_intensity = att.addAttribute(hostUI2, "spring_intensity_%s"%oSel[0].name(), "double", 1.0, "___spring_intensity_______%s"%oSel[0].name(), "spring_intensity_%s"%oSel[0].name(), 0, 1)

    if invertX:
        dist = dist *-1
        #aim constraint
        aimAxis = "-xy"
    else:
        #aim constraint
        aimAxis = "xy"

    for obj in oSel:

        oParent = obj.getParent()



        oNpo = pm.PyNode(pm.createNode("transform", n= obj.name() + "_npo", p=oParent, ss=True))
        oNpo.setTransformation(obj.getMatrix())
        pm.parent(obj, oNpo)

        oSpring_cns = pm.PyNode(pm.createNode("transform", n= obj.name() + "_spr_cns", p=oNpo, ss=True))
        oSpring_cns.setTransformation(obj.getMatrix())
        pm.parent(obj, oSpring_cns)


        oSpringLvl = pm.PyNode(pm.createNode("transform", n= obj.name() + "_spr_lvl", p=oNpo, ss=True))
        oM = obj.getTransformation()
        oM.addTranslation([dist, 0,0], "object")
        oSpringLvl.setTransformation(oM.asMatrix())

        oSpringDriver = pm.PyNode(pm.createNode("transform", n= obj.name() + "_spr", p=oSpringLvl, ss=True))


        try:
                defSet = pm.PyNode("rig_PLOT_grp")
                pm.sets(defSet, add=oSpring_cns)
        except:
            defSet   =  pm.sets(name="rig_PLOT_grp")
            pm.sets(defSet, remove=obj)
            pm.sets(defSet, add=oSpring_cns)

        #adding attributes:
        if not hostUI:
            hostUI = obj


        aSpring_damping = att.addAttribute(hostUI, "spring_damping_%s"%obj.name(), "double", .5, "damping_%s"%obj.name(), "damping_%s"%obj.name(), 0, 1)
        aSpring_stiffness_ = att.addAttribute(hostUI, "spring_stiffness_%s"%obj.name(), "double", .5, "stiffness_%s"%obj.name(), "stiffness_%s"%obj.name(), 0, 1)




        cns = aop.aimCns(oSpring_cns, oSpringDriver, aimAxis, 2, [0,1,0], oNpo, False)

        #change from fcurves to spring
        pb_node = pm.createNode("pairBlend")

        pm.connectAttr(cns+".constraintRotateX", pb_node+".inRotateX2")
        pm.connectAttr(cns+".constraintRotateY", pb_node+".inRotateY2")
        pm.connectAttr(cns+".constraintRotateZ", pb_node+".inRotateZ2")
        pm.setAttr(pb_node+".translateXMode", 2)
        pm.setAttr(pb_node+".translateYMode", 2)
        pm.setAttr(pb_node+".translateZMode", 2)


        pm.connectAttr( pb_node+".outRotateX", oSpring_cns+".rotateX", f=True)
        pm.connectAttr( pb_node+".outRotateY", oSpring_cns+".rotateY", f=True)
        pm.connectAttr( pb_node+".outRotateZ", oSpring_cns+".rotateZ", f=True)
        pm.setKeyframe( oSpring_cns, at="rotateX")
        pm.setKeyframe( oSpring_cns, at="rotateY")
        pm.setKeyframe( oSpring_cns, at="rotateZ")

        #add sprint op
        springOP = aop.gear_spring_op(oSpringDriver)

        #connecting attributes
        pm.connectAttr(aSpring_active, pb_node+".weight")
        pm.connectAttr(aSpring_intensity, springOP+".intensity")
        pm.connectAttr(aSpring_damping, springOP+".damping")
        pm.connectAttr(aSpring_stiffness_, springOP+".stiffness")


def spring_UI(*args):

    if pm.window("mGear_spring_window", exists = True):
        pm.deleteUI("mGear_spring_window")

    window = pm.window("mGear_spring_window", title="mGear post Spring", w=350, h=200, mxb=False, sizeable=False)

    pm.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 100), (2, 250)] )


    pm.text("spring Distance: ")
    distance = pm.floatField( "distance", annotation="distane in X local for the spring  position", w=50, value= 5)
    pm.text(label="Invert X to -X: " )
    pm.checkBox("invertX", label=" Invert X direction to -X ")
    pm.text(label="UI Host: " )
    hostUI = pm.textField("hostUI")




    pm.separator(h=10)
    pm.button(label="Spring Me bro!!", w=150, h=50,  command=build_spring)
    pm.separator(h=10)

    pm.separator(h=10)
    pm.separator(h=10)
    pm.text(label="Instructions: Select controls in order from root to tip", align="left" )
    pm.separator(h=10)
    pm.separator(h=10)
    pm.separator(h=10)
    pm.button(label="Baker", w=50, h=50,  command=bake_spring)


    pm.showWindow(window)

def build_spring(*args):
        dist = pm.floatField("distance", q=True, v=True)
        hostName = pm.textField("hostUI", q=True, text=True)
        try:
            hostUI = pm.PyNode(hostName)
        except:
            hostUI = False
        invertX = pm.checkBox("invertX", q=True, v=True)


        postSpring(dist, hostUI, hostUI, invertX )

def bake_spring(*args):
        pm.BakeSimulationOptions()