# ===============================================================================
# Render sender script for "bears"
# Code writer E. Melkov and M. Pavlov (Parovoz studio,  Moscow 2016)
# ===============================================================================

import maya.cmds as cmds
import maya.mel as mm
import sys
import os
import re
import shutil
from helper.EnvManagerV2 import PiplineTools

def cleanUpFaceControls():
    for msk in ['*:*:', '*:*_', '*_*_', '*_*:', '*:*:*:']:
        faceControls = cmds.ls('%sface_control' % msk)
        for fc in faceControls:
            if cmds.attributeQuery('face_text', node=fc, ex=True):
                print ('Clean up: setAttr %s.face_text to 0' % fc)
                cmds.setAttr('%s.face_text' % fc, 0)

def proxyToRenderChar():
    sel = cmds.ls(tr=True, sns=False)
    for i in sel:
        if i[-9:] == "character":
            cmds.setAttr("%s.proxy" % i, False)
            if cmds.attributeQuery("head", n=i, ex=True):
                cmds.setAttr("%s.head" % i, False)

def _autoRenderProx(file, mode='Draft'):
    print '------------------------------------------------------------'
    print '|	   %sAutoRender%s is starting					   |' % ('moriki-doriki', mode.title())
    print '------------------------------------------------------------'
    if 'Bot' in mode:
        print(mode)
    else:
        batchRenderProxy(file, mode=mode)

def autoRenderDraft(file):
    _autoRenderProx(file)

def autoRenderFinal(file):
    _autoRenderProx(file, mode='final')

def autoRenderFinal_4k():
    _autoRenderProx(mode='final_4k')

def cameraBot():
    _autoRenderProx(mode='cameraBot')
    mm.eval("source multyCameraBot;")

def hideObjectsBot():
    _autoRenderProx(mode='hideObjectsBot')
    mm.eval("source multyHideObjectsBot;")

def objectCorrectBot():
    print '------------------------------------------------------------'
    print '|	   %sHideObjectsBot is starting					   |' % 'moriki-doriki'
    print '------------------------------------------------------------'
    _autoRenderProx(mode='objectCorrectBot')
    mm.eval("source multyObjectCorrectBot;")

def batchRenderProxy(file, mode='Draft'):

        def render_log(msg):
            print('AF COMMAND RENDER: %s' % msg)

        pipeline = PipelineTools()
        parentUser = os.getenv('BATCH_INITIAL_USER')
        if parentUser is None:
            print('\nWARNING: BATCH_INITIAL_USER not set. ')
            parentUser = os.getenv('USER')
        print('\n***createRenderJob: BATCH_INITIAL_USER = %s') % parentUser

        # init some vars
        workSpace = os.path.dirname(__file__)+ os.sep + 'workspace.mel'
        #assDir = '%s/temp/data' % pipeline.project_path
        assDir = pipeline.get_temp_dir_path(pipeline.applic)
        print(assDir)
        fileName = os.path.basename(file).split('.')[0]
        # re.findall('([0-9a-z_]{9,})', file)[0] #re.findall('[^\.]', filePath) #.split('/')[0]
        proj = str(cmds.textFieldButtonGrp('working_dir', q=1, text=1))
        version = ''
        if fileName:
            sceneName = fileName.split('_v')
            if sceneName[0] != sceneName[-1]:
                sceneName, version = sceneName

        imageFilePrefix = 'v%s/%s' % (version, sceneName)
        print(workSpace,assDir,proj,fileName,sceneName,version,imageFilePrefix)

        # overwrite workspace for any case:
        try:
            #shutil.copyfile(workSpace, proj + '/workspace.mel')
            render_log('copied workspace to >>> %s ' % proj)
        except Exception as e:
            render_log('cannot copy workspace to %s - ERROR: %s' % (proj, str(e)))
            return

        # set project:
        mm.eval('setProject "%s";' % proj)
        render_log('set project = %s' % proj)

        cmds.setAttr('defaultRenderGlobals.imageFilePrefix', imageFilePrefix, type="string")
        render_log('Set image prefix: %s' % imageFilePrefix)

        render_log('Set %s options...' % mode)
        try:
            mm.eval('source "globalRender_%s.mel";' % mode)
            render_log('Sourcing mel script with %s options..OK' % mode)
        except Exception as err:
            render_log('Error sourcing mel script with %s options: %s ' % (mode, err))

        cleanUpFaceControls()
        proxyToRenderChar()

        # set exr format:
        cmds.setAttr('defaultRenderGlobals.imageFormat', 51)
        cmds.setAttr('defaultRenderGlobals.imfPluginKey', 'exr', type='string')

        # get frame range
        startFrame = int(cmds.playbackOptions(q=True, min=True))
        endFrame = int(cmds.playbackOptions(q=True, max=True))

        # meArnoldRender setup:
        from meArnoldRender import meArnoldRender
        meArnoldRender = meArnoldRender(NoGUI=True)

        # --------overwrite meArnoldRender params
        try:
                render_log('Updating meArnoldRender params ...')
                userName = 'batchuser'
                if parentUser: userName = parentUser.strip()

                # set job params
                me_jp = {'job_overwrite_workspace': 0, 'job_start': startFrame, 'job_end': endFrame, 'job_new_user': userName,
                         'job_paused': 0, 'job_name': str(meArnoldRender.job_param['job_name'] + "_%s" % mode.upper())}

                me_jp['job_name'] = sceneName + "_" + mode.upper()
                meArnoldRender.job_param.update(me_jp)

                # set ass-params
                me_ap = {'ass_deferred': 1, 'ass_def_task_size': 10, 'ass_local_assgen': 0, 'ass_dirname': assDir}
                #
                meArnoldRender.ass_param.update(me_ap)
                render_log('Overwrite meArnoldRender: OK.')
        except Exception as err:
                render_log('Error attempting overwrite meArnoldRender: %s' % err)

        # ------------------------------------------------
        try:
            meArnoldRender.submitJob()
            render_log('Well done!..Job has been submited.')
        except Exception as err:
            render_log('Error! Job has not been submited: %s ' % err)
