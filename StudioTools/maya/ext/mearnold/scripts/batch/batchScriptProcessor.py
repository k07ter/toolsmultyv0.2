# ===============================================================================
# 
# Script for batch script of Maya scenes.
# Script create backup files *.*.bck
# Using:
# Copy to your scripts folder "batch" including:
# 	batchScriptProcessor.py - main script
# 	batchScriptProcessorFuncModule.py - editable script for batch procedures
# 	mayabatch.bat - main command to run maya in batch mode 
# Run command:
# 	------------------------------------------------
# 	from batch import batchScriptProcessor
# 	reload (batchScriptProcessor)
# 	batchScriptProcessor.batchScriptProcessor_Win()
# ------------------------------------------------
# Copyrights studio "Parovoz" 2015, Moscow
# Code wrote by Evgeniy Melkov
# ===============================================================================

import maya.cmds as cmds
import os
import subprocess
import glob
import shutil
import af
import sys
import re

from functools import partial

sys.path.append(os.environ['CGRU_LOCATION'] + '/plugins/maya/afanasy')
from maya_ui_proc import *

prefix = 'batchScriptProcessor_'
save_frame_bgc = [0, 0, 0]
def_frame_bgc = [0.75, 0.5, 0]


def convertToBatchString(commands):
    result = ''
    for c in commands:
        # c.replace('"','\\\"') + ';\n'
        result = result + c.replace('"', '\\\"') + ';'
    return ('"%s"' % (result))


def batchScriptProcessor_FindFiles(path, mask, checkSubfolders):
    #	Find files in path with mask and check or not subfolders(bool)
    result = []
    if (checkSubfolders):
        dirs = []
        for dir, subdirs, files in os.walk(path):
            dirs.append(dir)
        for d in dirs:
            fileList = glob.glob(d + '/' + mask)
            for f in fileList:
                result.append(f.replace('\\', '/'))
    else:
        resultTemp = glob.glob(path + '/' + mask)
        for f in resultTemp: result.append(f.replace('\\', '/'))
    return result


def batchScriptProcessor_Start(fileList, function):
    #	Main batch process:
    set_cmd = 'set' if 'win' in sys.platform else 'export'
    maya_batch_path = ('%s BATCH_INITIAL_USER=%s && ' % (set_cmd, os.environ['USER'])) + 'python ' + os.environ['TOOLS_PATH'] + '/maya/mayabatch.py'
    # remotely = False
    remotely = cmds.checkBox('send_to_afanacy', q=True, v=True)
    print(remotely)
    # return
    if (remotely):
        print('Afanasy job setup.......')
        job = af.Job('batchJob: %s' % function)
        job.offLine()
        batch_block = af.Block('batch_block %s' % function, 'maya')
        batch_block.setParser('generic')
        # batch_block.setHostsMask("parovoz.*")
        batch_block.setHostsMaskExclude("loco0(22|20|23)")

        for file in fileList:
            #	Make backup
            if cmds.checkBox('check_backup', q=1, v=1):
                try:
                    shutil.copy(file, file.replace('.', '_backup.'))
                except Exception as e:
                    print('Backup creation error: %s. Check if file %s exists' % (file, e))
                    return

            commands = []
            if cmds.checkBox('bsp_load_all_refs', q=1, v=1):
                commands.append(
                    'file -f -options "v=0;"  -ignoreVersion  -loadReferenceDepth "all" -typ "mayaAscii" -o "%s"' % (
                        file))
            else:
                commands.append('file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -o "%s"' % (file))

            commands.append('python("from batch import batchScriptProcessorFuncModule")')
            commands.append('python("batchScriptProcessorFuncModule.%s()")' % (function))  # working!
            # commands.append('python("batchScriptProcessorFuncModule.%s(%s)")' % (function, args))
            if cmds.checkBox('force_save', q=1, v=1): commands.append('python("cmds.file(save=1, f=1)")')

            commandToBatch = convertToBatchString(commands)
            # print commandToBatch
            comShell = ''
            if cmds.checkBox('make_log', q=1, v=1):
                logFile = file + '_debug.log'
                log = open(logFile, "w")
                log.close()
                comShell = '%s -command %s -log %s' % (maya_batch_path, commandToBatch, logFile)
            else:
                comShell = '%s -command %s ' % (maya_batch_path, commandToBatch)
            # ----------------------------

            # --set project to third folder from end?
            cmd_proj = ''
            if cmds.checkBox('bsp_set_project', q=True, v=True):
                proj = '/'.join(file.split('/')[:-2])
                print('Set project: %s ' % proj)
                cmd_proj = ' -proj %s' % proj
            comShell += cmd_proj
            # --

            task = af.Task(file)
            task.setCommand(comShell)
            batch_block.tasks.append(task)

        # os.system(comShell)
        job.blocks.append(batch_block)
        job.send()
    else:
        for file in fileList:
            #	Make backup
            if cmds.checkBox('check_backup', q=1, v=1):
                try:
                    shutil.copy(file, file.replace('.', '_backup.'))
                except Exception as e:
                    print('Backup creation error: %s. Check if file %s exists' % (file, e))
                    return

            commands = []
            if cmds.checkBox('bsp_load_all_refs', q=1, v=1):
                commands.append(
                    'file -f -options "v=0;"  -ignoreVersion  -loadReferenceDepth "all" -typ "mayaAscii" -o "%s"' % (
                        file))
            else:
                commands.append('file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -o "%s"' % (file))

            commands.append('python("from batch import batchScriptProcessorFuncModule")')
            commands.append('python("batchScriptProcessorFuncModule.%s()")' % (function))
            commands.append('python("cmds.file(save=1, f=1)")')
            commandToBatch = convertToBatchString(commands)
            # print (commandToBatch)
            comShell = ''
            if cmds.checkBox('make_log', q=1, v=1):
                logFile = file + '_debug.log'
                log = open(logFile, "w")
                log.close()
                comShell = '%s -command %s -log %s' % (maya_batch_path, commandToBatch, logFile)
            else:
                comShell = '%s -command %s ' % (maya_batch_path, commandToBatch)
            os.system(comShell)
            # ----------------------------


def batchScriptProcessor_StartButtonCommand(*args):
    function = cmds.optionMenu('batch_script_name', q=1, value=1)

    logState = 'ON' if cmds.checkBox('make_log', q=1, v=1) else 'OFF'
    backupState = 'ON' if cmds.checkBox('check_backup', q=1, v=1) else 'WARNING! OFF'

    '''
	cmds.confirmDialog( title='Start batch process',
						message='Start << %s >> ? \nBackup: %s\nLog: %s' %  (function, backupState, logState), 
						button=['Yes','No'], 
						defaultButton='Yes', 
						cancelButton='No', 
						dismissString='No' 
						)
	'''
    if not cmds.textScrollList('batchScriptProcessor_filesList', q=1, ni=1):
        print('WARNING: No one file found!')
        return

    fileList = cmds.textScrollList('batchScriptProcessor_filesList', q=1, ai=1)
    print('ALL ITEMS :')
    print(fileList)
    # return
    batchScriptProcessor_Start(fileList, function)


def scanFiles(*args):
    path = cmds.textFieldButtonGrp('working_dir', q=1, text=1)
    if path.endswith('/') or path.endswith('\\'): path = path[:-1]
    mask = cmds.textField('mask_field', q=1, tx=1)
    checkSubfolders = cmds.checkBox('check_subfolders', q=True, v=True)
    # Achtung: This scan must be just the same as in batchScriptProcessor_Batch(...)
    fileList = []
    if cmds.radioButton('scan_scenes', q=1, sl=1):
        # if not cmds.checkBox('read_from_file', q=1, v=1):
        if (cmds.checkBox('use_regex', q=True, v=True)):
            rePattern = mask
            fileListAll = batchScriptProcessor_FindFiles(path, '*.*', checkSubfolders)
            for f in fileListAll:
                try:
                    if re.match(rePattern, f): fileList.append(f)
                except:
                    fileList = ['Error regular expression!']
        else:
            fileList = batchScriptProcessor_FindFiles(path, mask, checkSubfolders)
    else:
        readFilePath = cmds.textFieldButtonGrp('working_dir_file', forceChangeCommand=1, q=1, text=1)
        if os.path.exists(readFilePath):
            print('READ FILE = %s' % readFilePath)
            # read scenes from file:
            readFile = open(readFilePath)
            scenes = []
            for line in readFile:
                stripedLine = line.strip()
                if stripedLine:
                    # if cmds.file(stripedLine, q =1 , exists =1 ):
                    fileList.append(stripedLine.lower())
            readFile.close()

    cmds.textScrollList('batchScriptProcessor_filesList', e=1, ra=True)
    cmds.textScrollList('batchScriptProcessor_filesList', e=1, append=fileList)


#	Parse batch module "batchScriptProcessorFuncModule.py to find all avalable functions 
def batchScriptProcessor_ParseFunctions():
    #	Import module with all functions from batchScriptProcessorFuncModule.py:
    import batchScriptProcessorFuncModule as bmod
    reload(bmod)
    funcs = []
    path = os.path.abspath(bmod.__file__).replace('pyc', 'py')
    modfile = open(path, 'r')
    modfileContent = modfile.readlines()
    modfile.close()
    tipLine = 'No discription'
    tipLines = []
    for line in modfileContent:
        if 'def' in line and '():' in line:
            funcs.append(line.split()[1].replace('():', ''))
        tipLine = line

    funcs.sort()
    #	Return list of all functions in batchScriptProcessorFuncModule
    return funcs


#	main "batchScriptProcessorWin" window:
def batchScriptProcessor_Win():
    functions = batchScriptProcessor_ParseFunctions()
    #	Check if the window exists.
    if cmds.window('batchScriptProcessor_Win', exists=True):
        cmds.deleteUI('batchScriptProcessor_Win', window=True)
    # Create a window.
    win = cmds.window('batchScriptProcessor_Win', title="Batch Script processor", widthHeight=(500, 500))
    cmds.columnLayout(adjustableColumn=True)

    # --------Radio button for method:
    cmds.rowLayout(numberOfColumns=4)

    def scenesSourceChanged(*args):
        setDefaultIntValue(args[0], args[1], None, args[2])

        stateScan = getDefaultStrValue(prefix, 'scan_scenes', 1)
        stateRead = 1 - stateScan
        cmds.checkBox('check_subfolders', e=1, enable=stateScan)
        cmds.textFieldButtonGrp('working_dir', e=1, enable=stateScan)
        # cmds.textFieldButtonGrp('working_dir', e= 1, enable = stateScan)
        cmds.checkBox('use_regex', e=1, enable=stateScan)
        cmds.textField('mask_field', e=1, enable=stateScan)

        cmds.textFieldButtonGrp('working_dir_file', e=1, enable=stateRead)

        scanFiles()

    cmds.radioCollection('scenes_source_collection')
    cmds.radioButton('scan_scenes',
                     label='scan',
                     cc=partial(scenesSourceChanged, prefix, 'scan_scenes')
                     )
    cmds.radioButton('read_scenes',
                     label='from file',
                     cc=partial(scenesSourceChanged, prefix, 'read_scenes')
                     )

    cmds.setParent('..')

    cmds.separator()

    #	----------Working directory:
    def workingDirChanged(*args):
        setDefaultStrValue(args[0], args[1], None, args[2])
        scanFiles()

    def workingDirButton(*args):
        path = cmds.fileDialog2(dialogStyle=1, fileMode=3, dir=args[0])
        cmds.textFieldButtonGrp('working_dir', forceChangeCommand=1, e=1, text=path[0])

    cmds.textFieldButtonGrp('working_dir',
                            cc=partial(workingDirChanged, prefix, 'working_dir'),
                            text=getDefaultStrValue(prefix, 'working_dir', '//alpha/proj'),
                            buttonLabel='browse',
                            en=1 - getDefaultIntValue(prefix, 'read_from_file', 0),
                            buttonCommand=partial(workingDirButton,
                                                  getDefaultStrValue(prefix, 'working_dir', '//alpha/proj'))
                            )

    # --------------------------Check subfolders:
    def checkSubfoldersChanged(*args):
        setDefaultIntValue(prefix, 'check_subfolders', None, args[0])
        scanFiles()

    cmds.checkBox('check_subfolders',
                  v=getDefaultIntValue(prefix, 'check_subfolders', 1),
                  cc=partial(checkSubfoldersChanged),
                  label='Scan all subfolders '
                  )
    cmds.text(label=' ')
    cmds.separator()

    # Mask field:
    def maskFieldChanged(*args):
        setDefaultStrValue(args[0], args[1], None, args[2])
        scanFiles()

    # Reg expression:
    def useRegExChanged(*args):
        setDefaultIntValue(prefix, 'use_regex', None, args[0])
        scanFiles()

    def useCommonMask(*args):
        setDefaultIntValue(prefix, 'common_mask', None, args[0])
        #cmds.textScrollList('Job\'s masks:', list = ['generate_ass', 'render', 'anim'])

        #def enable_deferred(self, arg):
        """enable_deferred
        :param arg: True if enabled
        """
        arg = True
        ass_def_frame = prefix + 'MainWnd|f0|t0|tc1|fr3'
        ass_def = ass_def_frame + '|fc3|'
        ass_compressed = prefix + 'winMain|f0|t0|tc1|fr1|fc1|ass_compressed'

        setDefaultIntValue(
            prefix,
            'common_mask',
            {'af_hostsmask': '*.*'},
            arg
        )

        cmds.checkBoxGrp(
            'Use common hostsmask for all jobs',
            edit=True,
            enable={'af_hostsmask': '*.*'}
        )

        cmds.intFieldGrp(
            ass_def + 'ass_def_task_size',
            edit=True,
            enable=arg
        )
        cmds.checkBoxGrp(
            ass_compressed,
            e=True,
            enable=not arg
        )

        bg_color = save_frame_bgc
        if arg:
            bg_color = def_frame_bgc

        cmds.frameLayout(
            ass_def_frame=prefix + 'MainWnd|f0|t0|tc1|fr3',
            edit=True,
            bgc=bg_color
        )  # , enableBackground=False

        #self.setResolvedPath()

    cmds.checkBox('use_regex',
                  v=getDefaultIntValue(prefix, 'use_regex', 1),
                  cc=partial(useRegExChanged),
                  label='Use regular expressions'
                  )

    cmds.text(label=' ')

    cmds.text(label='file mask:')
    cmds.textField('mask_field',
                   cc=partial(maskFieldChanged, prefix, 'mask_field'),
                   tx=getDefaultStrValue(prefix, 'mask_field', '*.*'))
    cmds.text(label=' ')

    cmds.checkBox('common_mask',
                  v=getDefaultIntValue(prefix, 'common_mask', 1),
                  cc=partial(useCommonMask),
                  label='Common hostsmask'
                  )
    cmds.separator()

    # Read from file:
    def workingDirFileChanged(*args):
        setDefaultStrValue(args[0], args[1], None, args[2])
        scanFiles()

    def workingDirFileButton(*args):
        path = cmds.fileDialog2(dialogStyle=2, fileMode=1, dir=args[0])
        cmds.textFieldButtonGrp('working_dir_file', forceChangeCommand=1, e=1, text=path[0])

    cmds.textFieldButtonGrp('working_dir_file',
                            cc=partial(workingDirFileChanged, prefix, 'working_dir_file'),
                            text=getDefaultStrValue(prefix, 'working_dir_file', 'c:/scenes.txt'),
                            buttonLabel='browse',
                            buttonCommand=partial(workingDirFileButton,
                                                  getDefaultStrValue(prefix, 'working_dir_file', 'c:/scenes.txt'))
                            )

    # Get all batch functions and create menu: #MAKE DEFAULT VALUE!!!

    def batchScriptNameChanged(*args):
        setDefaultStrValue(args[0], args[1], None, args[2])

    cmds.text(label=' ')
    cmds.optionMenu('batch_script_name',
                    cc=partial(batchScriptNameChanged, prefix, 'batch_script_name'),
                    label='choose function:')
    for f in functions:
        cmds.menuItem('%s_item' % f, label=f)
    items = cmds.optionMenu('batch_script_name', q=1, ils=True)
    val = getDefaultStrValue(prefix, 'batch_script_name', functions[0])
    # print (items, val)
    if ('%s_item' % val) in items:
        cmds.optionMenu('batch_script_name', e=1, sl=(items.index('%s_item' % val) + 1))
    else:
        cmds.optionMenu('batch_script_name', e=1, sl=1)

    cmds.text(label=' ')
    cmds.separator()
    cmds.text(label=' ')

    def setProjectBefore(*args):
        setDefaultIntValue(prefix, 'bsp_set_project', None, args[0])

    cmds.checkBox('bsp_set_project',
                  v=getDefaultIntValue(prefix, 'bsp_set_project', 1),
                  cc=partial(setProjectBefore),
                  label='Set project before open scene (help : for  "../X/folder/scene.ma" scene will be set project to "X" )'
                  )

    def loadAllReferences(*args):
        setDefaultIntValue(prefix, 'bsp_load_all_refs', None, args[0])

    cmds.checkBox('bsp_load_all_refs',
                  v=getDefaultIntValue(prefix, 'bsp_load_all_refs', 1),
                  cc=partial(loadAllReferences),
                  label='load all references in the scene'
                  )

    cmds.text(label=' ')

    cmds.textScrollList('batchScriptProcessor_filesList', numberOfRows=12)

    # check save

    def checkForceSaveChanged(*args):
        setDefaultIntValue(prefix, 'force_save', None, args[0])

    cmds.checkBox('force_save',
                  v=getDefaultIntValue(prefix, 'force_save', 1),
                  cc=partial(checkForceSaveChanged),
                  label='Save scene after processing'
                  )

    # Check afanasy:
    def checkSendToAfanasyChanged(*args):
        setDefaultIntValue(prefix, 'send_to_afanacy', None, args[0])

    cmds.checkBox('send_to_afanacy',
                  v=getDefaultIntValue(prefix, 'send_to_afanacy', 1),
                  cc=partial(checkSendToAfanasyChanged),
                  label='Send to Afanasy'
                  )

    # Check backup:
    def checkBackupChanged(*args):
        setDefaultIntValue(prefix, 'check_backup', None, args[0])

    cmds.checkBox('check_backup',
                  v=getDefaultIntValue(prefix, 'check_backup', 1),
                  cc=partial(checkBackupChanged),
                  label='Backup files before batch'
                  )

    # Make log:
    def makeLogChanged(*args):
        setDefaultIntValue(prefix, 'make_log', None, args[0])

    cmds.checkBox('make_log',
                  v=getDefaultIntValue(prefix, 'make_log', 1),
                  cc=partial(makeLogChanged),
                  label='Make logfile '
                  )

    cmds.button('batchScriptProcessor_startBut', label='Start batch', command=batchScriptProcessor_StartButtonCommand,
                bgc=[0.5, 0, 0])

    # ----radio
    if (getDefaultStrValue(prefix, 'scan_scenes', 1)):
        cmds.radioCollection('scenes_source_collection', edit=True, select='scan_scenes')
    else:
        cmds.radioCollection('scenes_source_collection', edit=True, select='read_scenes')
    # --------

    cmds.showWindow('batchScriptProcessor_Win')
    scanFiles()
