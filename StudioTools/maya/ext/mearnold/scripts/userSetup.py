import maya.utils as utils

def meArnoldLoader():
    import meArnold_menu
    meArnold_menu.CreateMenu()

utils.executeDeferred('meArnoldLoader()')
