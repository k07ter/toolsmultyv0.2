'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

##########################################################
# GLOBAL
##########################################################
# built-in
import os
import sys, string, exceptions

## Debug mode for the logger
logDebug = False

# Severity for logged messages
sev_fatal = 1
sev_error = 2
sev_warning = 4
sev_info = 8
sev_verbose = 16
sev_comment = 32

# gear version
VERSION = [0,1,0]

## Log version of Gear
def logInfos():
    print "GEAR version : "+getVersion()

def getVersion():
    return ".".join([str(i) for i in VERSION])

##########################################################
# METHODS
##########################################################
# ========================================================
## reload a module and its sub-modules from a given module name.
# @param name String - The name of the module to reload.
def reloadModule(name="mgear", *args):

    debugMode = setDebug(False)
    module = __import__(name, globals(), locals(), ["*"], -1)

    path = module.__path__[0]

    __reloadRecursive(path, name)

    setDebug(debugMode)

def __reloadRecursive(path, parentName):

    for root, dirs, files in os.walk(path, True, None):

        # parse all the files of given path and reload python modules
        for sfile in files:
            if sfile.endswith(".py"):
                if sfile == "__init__.py":
                    name = parentName
                else:
                    name = parentName+"."+sfile[:-3]

                log("reload : %s"%name)
                try:
                    module = __import__(name, globals(), locals(), ["*"], -1)
                    reload(module)
                except ImportError, e:
                    for arg in e.args:
                        log(arg, sev_error)
                except Exception, e:
                    for arg in e.args:
                        log(arg, sev_error)

        # Now reload sub modules
        for dirName in dirs:
            __reloadRecursive(path+"/"+dirName, parentName+"."+dirName)
        break

##########################################################
# LOGGER
##########################################################
# ========================================================
## Set the debug mode to given value.
# @param b Boolean
# @return Boolean - The previous value of the debug mode
def setDebug(b):
    global logDebug
    original_value = logDebug
    logDebug = b
    return original_value

## Toggle the debug mode value.
# @param Boolean - The new debug mode value.
def toggleDebug():
    global logDebug
    logDebug = not logDebug
    return logDebug

# ========================================================
## Log a message using severity and additional info from the file itself.\n
## Severity has been taken from Softimage one : \n
## 1.Fatal\n
## 2.Error\n
## 4.Warning\n
## 8.Info\n
## 16.Verbose\n
## 32.Comment\n
# @param message String
# @param severity Int4
# @param infos Boolean - Add extra infos from the module, class, method and line number.
def log(message, severity=sev_comment, infos=False):

    message = str(message)

    if infos or logDebug:
        message = getInfos(1) +"\n"+ message

    sys.stdout.write(message + "\n")

# ========================================================
## Exception
class FakeException(exceptions.Exception):
    pass

## Get information from where the method has been fired. \n
## Such as module name, method, line number...
# @param level
# @return String
def getInfos(level):

    try:
        raise FakeException("this is fake")
    except Exception, e:
        #get the current execution frame
        f = sys.exc_info()[2].tb_frame

    #go back as many call-frames as was specified
    while level >= 0:
        f = f.f_back
        level = level-1

    infos = ""

    # Module Name
    moduleName = f.f_globals["__name__"]
    if moduleName != "__ax_main__":
        infos += moduleName + " | "

    # Class Name
    #if there is a self variable in the caller's local namespace then
    #we'll make the assumption that the caller is a class method
    obj = f.f_locals.get("self", None)
    if obj:
        infos += obj.__class__.__name__+"::"

    # Function Name
    functionName = f.f_code.co_name
    if functionName != "<module>":
        infos += functionName+"()"

    # Line Number
    lineNumber = str(f.f_lineno)
    infos += " line "+lineNumber+""

    if infos:
        infos = "["+infos+"]"

    return infos