//Maya ASCII 2015ff05 scene
//Name: biped_guide.ma
//Last modified: Sun, Jan 25, 2015 12:14:46 PM
//Codeset: 1252
requires maya "2015ff05";
//requires -nodeType "mgear_curveCns" "mGear_solvers2015" "1.0.2";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
//fileInfo "product" "Maya 2015";
//fileInfo "version" "2015";
fileInfo "cutIdentifier" "201410051530-933320-1";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -n "biped_guide";
	addAttr -ci true -sn "rig_name" -ln "rig_name" -dt "string";
	addAttr -ci true -k true -sn "mode" -ln "mode" -min 0 -max 1 -en "Final:WIP" -at "enum";
	addAttr -ci true -k true -sn "step" -ln "step" -min 0 -max 5 -en "All Steps:Objects:Properties:Operators:Connect:Finalize" 
		-at "enum";
	addAttr -ci true -sn "ismodel" -ln "ismodel" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "L_color_fk" -ln "L_color_fk" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "L_color_ik" -ln "L_color_ik" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "R_color_fk" -ln "R_color_fk" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "R_color_ik" -ln "R_color_ik" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "C_color_fk" -ln "C_color_fk" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "C_color_ik" -ln "C_color_ik" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "shadow_rig" -ln "shadow_rig" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "synoptic" -ln "synoptic" -dt "string";
	addAttr -ci true -sn "comments" -ln "comments" -dt "string";
	setAttr ".rig_name" -type "string" "rig";
	setAttr -k on ".step" 5;
	setAttr ".ismodel" yes;
	setAttr ".L_color_fk" 14;
	setAttr ".L_color_ik" 26;
	setAttr ".R_color_fk" 6;
	setAttr ".R_color_ik" 18;
	setAttr ".C_color_fk" 13;
	setAttr ".C_color_ik" 22;
	setAttr ".shadow_rig" yes;
	setAttr ".synoptic" -type "string" "biped_body,biped_hands";
	setAttr ".comments" -type "string" "";
createNode transform -n "controllers_org" -p "|biped_guide";
	setAttr ".v" no;
createNode transform -n "shoulder_L0_fk0_ctl" -p "controllers_org";
	setAttr -l on -k off ".v";
	setAttr -k on ".ro";
createNode nurbsCurve -n "shoulder_L0_fk0_ctlShape" -p "shoulder_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr -av ".cp[2].xv";
	setAttr -av ".cp[2].yv";
	setAttr -av ".cp[2].zv";
	setAttr -av ".cp[3].xv";
	setAttr -av ".cp[3].yv";
	setAttr -av ".cp[3].zv";
	setAttr -av ".cp[4].xv";
	setAttr -av ".cp[4].yv";
	setAttr -av ".cp[4].zv";
	setAttr -av ".cp[5].xv";
	setAttr -av ".cp[5].yv";
	setAttr -av ".cp[5].zv";
	setAttr -av ".cp[6].xv";
	setAttr -av ".cp[6].yv";
	setAttr -av ".cp[6].zv";
	setAttr -av ".cp[7].xv";
	setAttr -av ".cp[7].yv";
	setAttr -av ".cp[7].zv";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.18737092775348402 -1.0583420232946612 -0.18737092775348341
		-2.9142684086364282e-017 -1.0583420232946612 -0.26498250722340622
		-0.18737092775348341 -1.0583420232946612 -0.18737092775348341
		-0.26498250722340622 -1.0583420232946612 -7.3401362833235698e-017
		-0.18737092775348393 -1.0583420232946612 0.18737092775348341
		-7.8755749274859369e-017 -1.0583420232946612 0.26498250722340633
		0.18737092775348341 -1.0583420232946612 0.18737092775348363
		0.26498250722340622 -1.0583420232946612 1.4570655996140223e-016
		0.18737092775348402 -1.0583420232946612 -0.18737092775348341
		-2.9142684086364282e-017 -1.0583420232946612 -0.26498250722340622
		-0.18737092775348341 -1.0583420232946612 -0.18737092775348341
		;
createNode nurbsCurve -n "shoulder_L0_fk0_ctlShape1" -p "shoulder_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.23911198073343229 -1.0576823886090336 0
		0.23911198073343229 -1.0576823886090336 0
		;
createNode nurbsCurve -n "shoulder_L0_fk0_ctlShape2" -p "shoulder_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 -1.0576823886090336 0.23911198073343229
		0 -1.0576823886090336 -0.23911198073343229
		;
createNode nurbsCurve -n "shoulder_L0_fk0_ctlShape3" -p "shoulder_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 -1.0576823886090336 0
		;
createNode transform -n "shoulder_R0_fk0_ctl" -p "controllers_org";
	setAttr -l on -k off ".v";
	setAttr -k on ".ro";
createNode nurbsCurve -n "shoulder_R0_fk0_ctlShape" -p "shoulder_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr -av ".cp[2].xv";
	setAttr -av ".cp[2].yv";
	setAttr -av ".cp[2].zv";
	setAttr -av ".cp[3].xv";
	setAttr -av ".cp[3].yv";
	setAttr -av ".cp[3].zv";
	setAttr -av ".cp[4].xv";
	setAttr -av ".cp[4].yv";
	setAttr -av ".cp[4].zv";
	setAttr -av ".cp[5].xv";
	setAttr -av ".cp[5].yv";
	setAttr -av ".cp[5].zv";
	setAttr -av ".cp[6].xv";
	setAttr -av ".cp[6].yv";
	setAttr -av ".cp[6].zv";
	setAttr -av ".cp[7].xv";
	setAttr -av ".cp[7].yv";
	setAttr -av ".cp[7].zv";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.18737092775348402 1.0583420232946612 -0.18737092775348341
		-2.9142684086364282e-017 1.0583420232946612 -0.26498250722340622
		-0.18737092775348341 1.0583420232946612 -0.18737092775348341
		-0.26498250722340622 1.0583420232946612 -7.3401362833235698e-017
		-0.18737092775348393 1.0583420232946612 0.18737092775348341
		-7.8755749274859369e-017 1.0583420232946612 0.26498250722340633
		0.18737092775348341 1.0583420232946612 0.18737092775348363
		0.26498250722340622 1.0583420232946612 1.4570655996140223e-016
		0.18737092775348402 1.0583420232946612 -0.18737092775348341
		-2.9142684086364282e-017 1.0583420232946612 -0.26498250722340622
		-0.18737092775348341 1.0583420232946612 -0.18737092775348341
		;
createNode nurbsCurve -n "shoulder_R0_fk0_ctlShape1" -p "shoulder_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.23911198073343229 1.0576823886090336 0
		0.23911198073343229 1.0576823886090336 0
		;
createNode nurbsCurve -n "shoulder_R0_fk0_ctlShape2" -p "shoulder_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 1.0576823886090336 0.23911198073343229
		0 1.0576823886090336 -0.23911198073343229
		;
createNode nurbsCurve -n "shoulder_R0_fk0_ctlShape3" -p "shoulder_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 1.0576823886090336 0
		;
createNode transform -n "faceUI_C0_ctl" -p "controllers_org";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "faceUI_C0_ctlShape" -p "faceUI_C0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.23052826874163423 0.23511278884044096 0
		-3.7194550706577315e-017 0.33060072426361486 0
		-0.2305282687416339 0.23511278884044051 0
		-0.32601620416480936 0.0045845200988057222 0
		-0.23052826874163412 -0.22594374864282551 0
		-9.8235044885680447e-017 -0.32143168406599854 0
		0.2305282687416339 -0.22594374864282596 0
		0.32601620416480936 0.004584520098804834 0
		0.23052826874163423 0.23511278884044096 0
		-3.7194550706577315e-017 0.33060072426361486 0
		-0.2305282687416339 0.23511278884044051 0
		;
createNode transform -n "global_C0_ctl" -p "controllers_org";
	addAttr -ci true -k true -sn "local_C0" -ln "local_C0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "armUI_L0" -ln "armUI_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "faceUI_C1" -ln "faceUI_C1" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "mouth_C0" -ln "mouth_C0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "mouth_C0_siderot" -ln "mouth_C0_siderot" -nn "Sides Rotation" 
		-min 0 -max 100 -at "double";
	addAttr -ci true -k true -sn "mouth_C0_vertrot" -ln "mouth_C0_vertrot" -nn "Vertical Rotation" 
		-min 0 -max 100 -at "double";
	addAttr -ci true -k true -sn "mouth_C0_fronttrans" -ln "mouth_C0_fronttrans" -nn "Frontal Translation" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "mouth_C0_verttrans" -ln "mouth_C0_verttrans" -nn "Vertical Translation" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "mouth_C0_floowlips" -ln "mouth_C0_floowlips" -nn "FollowLips" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "mouth_C0_lipsAlignSpeed" -ln "mouth_C0_lipsAlignSpeed" 
		-nn "Lips Align Speed" -min 0 -max 100 -at "double";
	addAttr -ci true -k true -sn "eye_L0" -ln "eye_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "eye_L0_trackupeyelid" -ln "eye_L0_trackupeyelid" -nn "Eyelid Up Tracking" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "eye_L0_trackloweyelid" -ln "eye_L0_trackloweyelid" 
		-nn "Eyelid Low Tracking" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "eye_R0" -ln "eye_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "eye_R0_trackupeyelid" -ln "eye_R0_trackupeyelid" -nn "Eyelid Up Tracking" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "eye_R0_trackloweyelid" -ln "eye_R0_trackloweyelid" 
		-nn "Eyelid Low Tracking" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "eyeslook_C0" -ln "eyeslook_C0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "eyeslook_C0_ikref" -ln "eyeslook_C0_ikref" -nn "Ik Ref" 
		-min 0 -max 3 -en "neck_C0_head:local_C0_root:body_C0_root:spine_C0_eff" -at "enum";
	addAttr -ci true -k true -sn "spineUI_C0" -ln "spineUI_C0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "armUI_R0" -ln "armUI_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "legUI_L0" -ln "legUI_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "legUI_R0" -ln "legUI_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	setAttr ".ro" 2;
	setAttr -k on ".mouth_C0_siderot" 20;
	setAttr -k on ".mouth_C0_vertrot" 40;
	setAttr -k on ".mouth_C0_fronttrans" 1;
	setAttr -k on ".mouth_C0_verttrans" 0.2;
	setAttr -k on ".mouth_C0_floowlips" 0.05;
	setAttr -k on ".mouth_C0_lipsAlignSpeed" 10;
createNode nurbsCurve -n "global_C0_ctlShape" -p "global_C0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 30;
	setAttr ".cc" -type "nurbsCurve" 
		1 24 2 no 3
		25 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
		25
		1.7479403719685447 0 1.7479403719685447
		1.7479403719685447 0 5.2438211159056332
		3.4958807439370894 0 5.2438211159056332
		0 0 8.7397018598427216
		-3.4958807439370894 0 5.2438211159056332
		-1.7479403719685447 0 5.2438211159056332
		-1.7479403719685447 0 1.7479403719685447
		-5.2438211159056332 0 1.7479403719685447
		-5.2438211159056332 0 3.4958807439370894
		-8.7397018598427216 0 0
		-5.2438211159056332 0 -3.4958807439370894
		-5.2438211159056332 0 -1.7479403719685447
		-1.7479403719685447 0 -1.7479403719685447
		-1.7479403719685447 0 -5.2438211159056332
		-3.4958807439370894 0 -5.2438211159056332
		0 0 -8.7397018598427216
		3.4958807439370894 0 -5.2438211159056332
		1.7479403719685447 0 -5.2438211159056332
		1.7479403719685447 0 -1.7479403719685447
		5.2438211159056332 0 -1.7479403719685447
		5.2438211159056332 0 -3.4958807439370894
		8.7397018598427216 0 0
		5.2438211159056332 0 3.4958807439370894
		5.2438211159056332 0 1.7479403719685447
		1.7479403719685447 0 1.7479403719685447
		;
createNode transform -n "local_C0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr -k on ".ro" 2;
createNode nurbsCurve -n "local_C0_ctlShape" -p "local_C0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 2 no 3
		5 0 1 2 3 4
		5
		3.7906311373092909 0 3.7906311373092909
		3.7906311373092909 0 -3.7906311373092909
		-3.7906311373092909 0 -3.7906311373092909
		-3.7906311373092909 0 3.7906311373092909
		3.7906311373092909 0 3.7906311373092909
		;
createNode transform -n "body_C0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3866647412954306e-016 10.353589031507756 -0.0085554946225459023 ;
	setAttr -k on ".ro" 2;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "body_C0_ctlShape" -p "body_C0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 2 no 3
		5 0 1 2 3 4
		5
		2.9511938779689615 1.7042166211496383e-015 2.9511938779689575
		2.9511938779689615 1.7042166211496383e-015 -2.9511938779689664
		-2.9511938779689615 1.7042166211496383e-015 -2.9511938779689664
		-2.9511938779689615 1.7042166211496383e-015 2.9511938779689575
		2.9511938779689615 1.7042166211496383e-015 2.9511938779689575
		;
createNode transform -n "spine_C0_ik0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3866647412954303e-016 10.353589031507758 -0.0085554946225459023 ;
	setAttr -k on ".ro" 2;
createNode nurbsCurve -n "spine_C0_ik0_ctlShape" -p "spine_C0_ik0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		1.1299956480347135 4.2096614360395013e-015 -2.7280508189079815
		4.4632530039078567e-016 4.1597166766033506e-015 -1.9123909981184462
		-1.1299956480347155 4.2096614360395013e-015 -2.7280508189079815
		-1.3522646430496592 4.1254188292106982e-015 -1.3522646430496592
		-2.7280508189079815 4.11180877854596e-015 -1.1299956480347149
		-1.9123909981184477 4.0426165008751521e-015 8.7194412288708007e-017
		-2.7280508189079815 3.9734242232043473e-015 1.1299956480347151
		-1.3522646430496592 3.9598141725396067e-015 1.3522646430496605
		-1.1299956480347149 3.8755715657108012e-015 2.728050818907982
		1.3748133076949442e-016 3.9255163251469528e-015 1.9123909981184462
		1.1299956480347164 3.8755715657108012e-015 2.7280508189079815
		1.3522646430496594 3.9598141725396067e-015 1.3522646430496583
		2.7280508189079837 3.9734242232043473e-015 1.1299956480347144
		1.9123909981184462 4.0426165008751521e-015 -3.2955879061091523e-016
		2.7280508189079815 4.11180877854596e-015 -1.1299956480347171
		1.3522646430496583 4.1254188292106982e-015 -1.3522646430496605
		1.1299956480347135 4.2096614360395013e-015 -2.7280508189079815
		4.4632530039078567e-016 4.1597166766033506e-015 -1.9123909981184462
		-1.1299956480347155 4.2096614360395013e-015 -2.7280508189079815
		;
createNode transform -n "spine_C0_ik1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3866647412954286e-016 12.729595719664276 -0.0085554946225447417 ;
	setAttr -k on ".ro" 2;
createNode nurbsCurve -n "spine_C0_ik1_ctlShape" -p "spine_C0_ik1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		1.1238755642912628 0.0047666211574854608 -2.6851265609608923
		4.3763111230714902e-016 0.0047666211574854279 -1.88230066258291
		-1.1238755642912643 0.0047666211574854608 -2.6851265609608923
		-1.3449407450566309 0.0047666211574854062 -1.3309875627443022
		-2.7132756297316849 0.0047666211574853967 -1.1122158382382279
		-1.9020334422472693 0.0047666211574853568 8.3161981871438323e-017
		-2.7132756297316849 0.0047666211574853099 1.1122158382382279
		-1.3449407450566309 0.0047666211574852995 1.3309875627443044
		-1.1238755642912639 0.0047666211574852414 2.6851265609608932
		1.304598490905504e-016 0.0047666211574852735 1.88230066258291
		1.1238755642912648 0.0047666211574852414 2.6851265609608923
		1.3449407450566311 0.0047666211574852995 1.3309875627443013
		2.7132756297316867 0.0047666211574853099 1.1122158382382272
		1.9020334422472687 0.0047666211574853568 -3.2703385711928908e-016
		2.7132756297316849 0.0047666211574853967 -1.1122158382382308
		1.3449407450566304 0.0047666211574854062 -1.3309875627443044
		1.1238755642912628 0.0047666211574854608 -2.6851265609608923
		4.3763111230714902e-016 0.0047666211574854279 -1.88230066258291
		-1.1238755642912643 0.0047666211574854608 -2.6851265609608923
		;
createNode transform -n "spine_C0_tan0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3866647412954298e-016 11.13767126541355 -0.0085554946225455189 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "spine_C0_tan0_ctlShape" -p "spine_C0_tan0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		9.4592675726131794e-018 0.15448156283425429 -2.193647873125689
		-5.9329647601094789e-031 0.21846992129678977 -2.0391663102913875
		-9.4592675726112566e-018 0.15448156283425118 -1.8846847474570012
		-1.3377424491305328e-017 -1.2507356261792779e-015 -1.8206963889946854
		-9.4592675726122242e-018 -0.15448156283424785 -1.8846847474570647
		-3.0606465976928153e-031 -0.2184699212967931 -2.0391663102913813
		9.4592675726127172e-018 -0.15448156283424996 -2.1936478731256739
		1.3377424491304052e-017 -6.0888794006785929e-015 -2.2576362315881089
		9.4592675726131794e-018 0.15448156283425429 -2.193647873125689
		-5.9329647601094789e-031 0.21846992129678977 -2.0391663102913875
		-9.4592675726112566e-018 0.15448156283425118 -1.8846847474570012
		;
createNode nurbsCurve -n "spine_C0_tan0_ctlShape1" -p "spine_C0_tan0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0 0.053066285894170402 -1.9861000243972056
		0 0.053066285894170402 -1.8799674526088643
		0 -0.053066285894170423 -1.8799674526088643
		0 -0.053066285894170423 -1.9861000243972056
		0 -0.15919885768251132 -1.9861000243972056
		0 -0.15919885768251132 -2.0922325961855459
		0 -0.053066285894170423 -2.0922325961855459
		0 -0.053066285894170423 -2.198365167973884
		0 0.053066285894170402 -2.198365167973884
		0 0.053066285894170402 -2.0922325961855459
		0 0.15919885768251132 -2.0922325961855459
		0 0.15919885768251132 -1.9861000243972056
		0 0.053066285894170402 -1.9861000243972056
		;
createNode nurbsCurve -n "spine_C0_tan0_ctlShape2" -p "spine_C0_tan0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		0 0.18414625387329212 -1.9686262881626884
		0 0 0
		0 -0.18600257024509961 -1.9760515536499184
		;
createNode transform -n "spine_C0_tan1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3866647412954291e-016 11.921753465025605 -0.0085554946225451355 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "spine_C0_tan1_ctlShape" -p "spine_C0_tan1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		9.4592675726131794e-018 0.15448156283425429 -2.193647873125689
		-5.9329647601094789e-031 0.21846992129678977 -2.0391663102913875
		-9.4592675726112566e-018 0.15448156283425118 -1.8846847474570012
		-1.3377424491305328e-017 -1.2507356261792779e-015 -1.8206963889946854
		-9.4592675726122242e-018 -0.15448156283424785 -1.8846847474570647
		-3.0606465976928153e-031 -0.2184699212967931 -2.0391663102913813
		9.4592675726127172e-018 -0.15448156283424996 -2.1936478731256739
		1.3377424491304052e-017 -6.0888794006785929e-015 -2.2576362315881089
		9.4592675726131794e-018 0.15448156283425429 -2.193647873125689
		-5.9329647601094789e-031 0.21846992129678977 -2.0391663102913875
		-9.4592675726112566e-018 0.15448156283425118 -1.8846847474570012
		;
createNode nurbsCurve -n "spine_C0_tan1_ctlShape1" -p "spine_C0_tan1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0 0.053066285894170402 -1.9861000243972056
		0 0.053066285894170402 -1.8799674526088643
		0 -0.053066285894170423 -1.8799674526088643
		0 -0.053066285894170423 -1.9861000243972056
		0 -0.15919885768251132 -1.9861000243972056
		0 -0.15919885768251132 -2.0922325961855459
		0 -0.053066285894170423 -2.0922325961855459
		0 -0.053066285894170423 -2.198365167973884
		0 0.053066285894170402 -2.198365167973884
		0 0.053066285894170402 -2.0922325961855459
		0 0.15919885768251132 -2.0922325961855459
		0 0.15919885768251132 -1.9861000243972056
		0 0.053066285894170402 -1.9861000243972056
		;
createNode nurbsCurve -n "spine_C0_tan1_ctlShape2" -p "spine_C0_tan1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		0 0.18414625387329212 -1.9686262881626884
		0 0 0
		0 -0.18600257024509961 -1.9760515536499184
		;
createNode transform -n "spine_C0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3866647412954301e-016 10.947971392475461 -0.0085554946225456108 ;
	setAttr -k on ".ro" 2;
createNode nurbsCurve -n "spine_C0_fk0_ctlShape" -p "spine_C0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.99438004073004838 6.4995810515121732e-017 -1.598645912133132
		-2.6063729710309334e-016 9.1917956727917053e-017 -2.2608267303709821
		-0.99438004073004793 6.4995810515121769e-017 -1.5986459121331333
		-2.520830834431361 2.6635524254040046e-032 -6.9346314330261988e-016
		-1.615405045343109 -6.4995810515121745e-017 1.5986459121331329
		-6.8837278830947352e-016 -9.1917956727916954e-017 2.2608267303709821
		1.615405045343107 -6.4995810515121769e-017 1.5986459121331333
		2.520830834431361 -4.9369313866090487e-032 1.175962328894826e-015
		0.99438004073004838 6.4995810515121732e-017 -1.598645912133132
		-2.6063729710309334e-016 9.1917956727917053e-017 -2.2608267303709821
		-0.99438004073004793 6.4995810515121769e-017 -1.5986459121331333
		;
createNode transform -n "spine_C0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3866647412954296e-016 11.541592355276828 -0.0085554946225453211 ;
	setAttr -k on ".ro" 2;
createNode nurbsCurve -n "spine_C0_fk1_ctlShape" -p "spine_C0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.99438004073004838 6.4995810515121732e-017 -1.598645912133132
		-2.6063729710309334e-016 9.1917956727916942e-017 -2.2608267303709821
		-0.99438004073004793 6.4995810515121769e-017 -1.5986459121331333
		-2.520830834431361 2.6635524254040046e-032 -6.5513102504256523e-016
		-1.615405045343109 -6.4995810515121745e-017 1.5986459121331329
		-6.8837278830947352e-016 -9.1917956727916954e-017 2.2608267303709821
		1.615405045343107 -6.4995810515121769e-017 1.5986459121331333
		2.520830834431361 -4.9369313866090487e-032 1.2142944471548803e-015
		0.99438004073004838 6.4995810515121732e-017 -1.598645912133132
		-2.6063729710309334e-016 9.1917956727916942e-017 -2.2608267303709821
		-0.99438004073004793 6.4995810515121769e-017 -1.5986459121331333
		;
createNode transform -n "spine_C0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.3866647412954291e-016 12.13521336266853 -0.0085554946225450297 ;
	setAttr ".ro" 2;
createNode nurbsCurve -n "spine_C0_fk2_ctlShape" -p "spine_C0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.99438004073004838 6.4995810515121732e-017 -1.598645912133132
		-2.6063729710309334e-016 9.1917956727916942e-017 -2.2608267303709821
		-0.99438004073004793 6.4995810515121769e-017 -1.5986459121331333
		-2.520830834431361 2.6635524254040046e-032 -6.5513102504256523e-016
		-1.615405045343109 -6.4995810515121745e-017 1.5986459121331329
		-6.8837278830947352e-016 -9.1917956727916954e-017 2.2608267303709821
		1.615405045343107 -6.4995810515121769e-017 1.5986459121331333
		2.520830834431361 -4.9369313866090487e-032 1.2142944471548803e-015
		0.99438004073004838 6.4995810515121732e-017 -1.598645912133132
		-2.6063729710309334e-016 9.1917956727916942e-017 -2.2608267303709821
		-0.99438004073004793 6.4995810515121769e-017 -1.5986459121331333
		;
createNode transform -n "shoulder_L0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.11673327753264884 14.596096560902705 0.40997110536358694 ;
	setAttr ".r" -type "double3" 0.84846981070292538 19.588532215103083 -5.8001935627412999 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1.0000000000000002 ;
createNode nurbsCurve -n "shoulder_L0_ctlShape" -p "shoulder_L0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.6373488484601606 0.11180709326024538 1.0164739347385556
		1.6373488484601606 0.11180709326024538 0.82005819098080412
		-0.32680858911735117 0.11180709326024538 0.82005819098080412
		-0.32680858911735117 -0.084608650497505822 0.82005819098080412
		-0.32680858911735117 -0.084608650497505822 1.0164739347385556
		-0.32680858911735117 0.11180709326024538 1.0164739347385556
		-0.32680858911735117 0.11180709326024538 0.82005819098080412
		-0.32680858911735117 0.11180709326024538 1.0164739347385556
		1.6373488484601606 0.11180709326024538 1.0164739347385556
		1.6373488484601606 -0.084608650497505822 1.0164739347385556
		-0.32680858911735117 -0.084608650497505822 1.0164739347385556
		1.6373488484601606 -0.084608650497505822 1.0164739347385556
		1.6373488484601606 -0.084608650497505822 0.82005819098080412
		1.6373488484601606 0.11180709326024538 0.82005819098080412
		1.6373488484601606 -0.084608650497505822 0.82005819098080412
		-0.32680858911735117 -0.084608650497505822 0.82005819098080412
		;
createNode transform -n "shoulder_L0_orbit_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.9577404853947684 14.40908758449403 -0.24853822861229696 ;
	setAttr ".r" -type "double3" 2.5046868116525194e-014 -6.3611093629270335e-015 6.6990432978325325e-014 ;
createNode nurbsCurve -n "shoulder_L0_orbit_ctlShape" -p "shoulder_L0_orbit_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.3682795195457837 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 0.92875698653666927 2.7755575615628914e-016
		-0.36827951954578325 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 1.6653160256282362 2.7755575615628914e-016
		0.3682795195457837 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 0.92875698653666927 2.7755575615628914e-016
		-0.36827951954578325 1.2970365060824527 2.7755575615628914e-016
		;
createNode nurbsCurve -n "shoulder_L0_orbit_ctl_0crvShape" -p "shoulder_L0_orbit_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.3682795195457837 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 -0.36827951954578309
		-0.36827951954578325 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 0.36827951954578386
		0.3682795195457837 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 -0.36827951954578309
		-0.36827951954578325 1.2970365060824527 2.7755575615628914e-016
		;
createNode nurbsCurve -n "shoulder_L0_orbit_ctl_1crvShape" -p "shoulder_L0_orbit_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		2.2204460492503131e-016 1.2970365060824527 0.36827951954578386
		2.2204460492503131e-016 0.92875698653666927 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 -0.36827951954578309
		2.2204460492503131e-016 1.6653160256282362 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 0.36827951954578386
		2.2204460492503131e-016 0.92875698653666927 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 -0.36827951954578309
		;
createNode transform -n "arm_L0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.9577404853947704 14.409087584494022 -0.24853822861229682 ;
	setAttr ".r" -type "double3" -73.053636912674435 8.7083905508289821 -40.362323240370742 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_L0_fk0_ctlShape" -p "arm_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.054641456687589345 0.8370262432015606 -0.83379498787537953
		-0.054641456687589407 1.1823954354182524 1.4231471083669065e-015
		-0.054641456687589456 0.83702624320156094 0.83379498787537876
		-0.054641456687589469 0.00323125532618233 1.1791641800920698
		-0.054641456687589456 -0.83056373254919558 0.83379498787537898
		-0.054641456687589407 -1.1759329247658885 1.6439237549004957e-015
		-0.054641456687589345 -0.83056373254919569 -0.83379498787537809
		-0.054641456687589338 0.0032312553261813577 -1.1791641800920698
		-0.054641456687589345 0.8370262432015606 -0.83379498787537953
		-0.054641456687589407 1.1823954354182524 1.4231471083669065e-015
		-0.054641456687589456 0.83702624320156094 0.83379498787537876
		;
createNode transform -n "arm_L0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.9921266473248513 12.659828774293096 -0.560147960664519 ;
	setAttr ".r" -type "double3" -78.750213132748314 -5.0790881887670443 -43.017636032621411 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_L0_fk1_ctlShape" -p "arm_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.052661992185914608 0.74368626358654955 -0.74368626358655021
		-0.052661992185914622 1.0517312001146728 1.479470702886422e-015
		-0.052661992185914698 0.74368626358654955 0.74368626358654966
		-0.052661992185914712 -3.9913906984826471e-016 1.0517312001146726
		-0.052661992185914698 -0.74368626358654955 0.74368626358654966
		-0.052661992185914622 -1.0517312001146728 1.6763878848499482e-015
		-0.052661992185914608 -0.74368626358654966 -0.74368626358654955
		-0.052661992185914587 -1.2687913386342633e-015 -1.0517312001146728
		-0.052661992185914608 0.74368626358654955 -0.74368626358655021
		-0.052661992185914622 1.0517312001146728 1.479470702886422e-015
		-0.052661992185914698 0.74368626358654955 0.74368626358654966
		;
createNode transform -n "arm_L0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.9607127890490803 10.822959309523604 -0.32084147633123944 ;
	setAttr ".r" -type "double3" -83.176838557401382 42.546902336944996 -9.2018679521914599 ;
	setAttr ".ro" 1;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_L0_fk2_ctlShape" -p "arm_L0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		9.6176866831985692e-016 0.31008497065817131 -0.31008497065817175
		9.4278143998084473e-016 0.43852637099284963 5.0030615453149077e-017
		9.2379421164183333e-016 0.31008497065817148 0.31008497065817148
		9.1592944415193979e-016 1.8842519114164171e-016 0.43852637099284963
		9.2379421164183333e-016 -0.31008497065817142 0.31008497065817148
		9.4278143998084473e-016 -0.43852637099284969 1.3213655391269997e-016
		9.6176866831985692e-016 -0.31008497065817148 -0.31008497065817131
		9.6963343580975065e-016 -1.7418214941727817e-016 -0.43852637099284963
		9.6176866831985692e-016 0.31008497065817131 -0.31008497065817175
		9.4278143998084473e-016 0.43852637099284963 5.0030615453149077e-017
		9.2379421164183333e-016 0.31008497065817148 0.31008497065817148
		;
createNode transform -n "arm_L0_ikcns_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.9607127890490803 10.822959309523599 -0.32084147633123961 ;
	setAttr ".r" -type "double3" 2.5046868116525194e-014 -6.3611093629270335e-015 6.6990432978325325e-014 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_L0_ikcns_ctlShape" -p "arm_L0_ikcns_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.38900473401072044 0 0
		-0.38900473401072044 0 0
		;
createNode nurbsCurve -n "arm_L0_ikcns_ctl1Shape" -p "arm_L0_ikcns_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.38900473401072044 0
		0 -0.38900473401072044 0
		;
createNode nurbsCurve -n "arm_L0_ikcns_ctl2Shape" -p "arm_L0_ikcns_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.38900473401072044
		0 0 -0.38900473401072044
		;
createNode transform -n "arm_L0_ik_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.9607127890490803 10.822959309523599 -0.32084147633123961 ;
	setAttr ".r" -type "double3" 6.8231614425986367 -9.2018679521914812 -42.546902336945045 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999967 ;
	setAttr ".invTx" yes;
	setAttr ".invRy" yes;
	setAttr ".invRz" yes;
createNode nurbsCurve -n "arm_L0_ik_ctlShape" -p "arm_L0_ik_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		-1.3877787807814457e-017 0.45050662509386025 -0.18660595405280803
		-3.0814879110195774e-033 0.23527802922032837 -6.5518763514851254e-017
		-4.8572257327350599e-017 0.45050662509386025 0.18660595405280858
		-6.9388939039072284e-018 0.16636668992590109 0.1663666899259012
		-4.163336342344337e-017 0.18660595405280841 0.45050662509386025
		1.3877787807814457e-017 -1.2737024464650119e-016 0.23527802922032853
		-1.3877787807814457e-017 -0.18660595405280844 0.45050662509386014
		-6.9388939039072284e-018 -0.1663666899259012 0.1663666899259012
		-2.0816681711721685e-017 -0.45050662509386036 0.18660595405280847
		1.5407439555097887e-033 -0.23527802922032837 -2.7522244716879273e-017
		1.3877787807814457e-017 -0.45050662509386025 -0.18660595405280858
		-1.3877787807814457e-017 -0.16636668992590109 -0.16636668992590112
		1.3877787807814457e-017 -0.18660595405280828 -0.45050662509386036
		6.9388939039072284e-018 -7.6097846718485941e-017 -0.23527802922032817
		4.163336342344337e-017 0.18660595405280861 -0.45050662509386025
		-1.3877787807814457e-017 0.16636668992590109 -0.16636668992590112
		-1.3877787807814457e-017 0.45050662509386025 -0.18660595405280803
		-3.0814879110195774e-033 0.23527802922032837 -6.5518763514851254e-017
		-4.8572257327350599e-017 0.45050662509386025 0.18660595405280858
		;
createNode transform -n "arm_L0_upv_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.3806325800321533 13.157600957241204 -3.7397618588520944 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".invTx" yes;
createNode nurbsCurve -n "arm_L0_upv_ctlShape" -p "arm_L0_upv_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		-0.12027045666498232 0.36081136999494701 0.94425699999999324
		-0.12027045666498232 0.12027045666498232 0.94425699999999324
		-0.36081136999494701 0.12027045666498232 0.94425699999999324
		-0.36081136999494701 -0.12027045666498232 0.94425699999999324
		-0.12027045666498232 -0.12027045666498232 0.94425699999999324
		-0.12027045666498232 -0.36081136999494701 0.94425699999999324
		0.12027045666498232 -0.36081136999494701 0.94425699999999324
		0.12027045666498232 -0.12027045666498232 0.94425699999999324
		0.36081136999494701 -0.12027045666498232 0.94425699999999324
		0.36081136999494701 0.12027045666498232 0.94425699999999324
		0.12027045666498232 0.12027045666498232 0.94425699999999324
		0.12027045666498232 0.36081136999494701 0.94425699999999324
		-0.12027045666498232 0.36081136999494701 0.94425699999999324
		;
createNode transform -n "arm_L0_mid_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.9921266473248505 12.659828774293093 -0.560147960664519 ;
	setAttr ".r" -type "double3" -78.793926153573565 0.77297307581247143 -41.855656197465642 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_L0_mid_ctlShape" -p "arm_L0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		9.4592675726131794e-018 1.7610178978175233 0.15448156283425266
		-5.9329647601094789e-031 1.6065363349832202 0.21846992129678791
		-9.4592675726112566e-018 1.4520547721488342 0.15448156283424924
		-1.3377424491305328e-017 1.3880664136865184 -3.1351922239688302e-015
		-9.4592675726122242e-018 1.4520547721488974 -0.15448156283424944
		-3.0606465976928153e-031 1.6065363349832145 -0.21846992129679496
		9.4592675726127172e-018 1.7610178978175082 -0.15448156283425193
		1.3377424491304052e-017 1.8250062562799427 -7.8536400786257447e-015
		9.4592675726131794e-018 1.7610178978175233 0.15448156283425266
		-5.9329647601094789e-031 1.6065363349832202 0.21846992129678791
		-9.4592675726112566e-018 1.4520547721488342 0.15448156283424924
		;
createNode nurbsCurve -n "arm_L0_mid_ctlShape1" -p "arm_L0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0 1.5534700490890385 0.053066285894168591
		0 1.4473374773006973 0.053066285894168758
		0 1.4473374773006973 -0.053066285894172088
		0 1.5534700490890387 -0.053066285894172255
		0 1.5534700490890387 -0.15919885768251327
		0 1.6596026208773793 -0.15919885768251299
		0 1.6596026208773795 -0.053066285894171922
		0 1.7657351926657183 -0.053066285894172421
		0 1.7657351926657183 0.05306628589416848
		0 1.6596026208773798 0.053066285894168591
		0 1.6596026208773795 0.15919885768250927
		0 1.5534700490890385 0.15919885768250933
		0 1.5534700490890385 0.053066285894168591
		;
createNode nurbsCurve -n "arm_L0_mid_ctlShape2" -p "arm_L0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		0 1.5359963128545218 0.1841462538732902
		0 -1.6930901125533633e-015 -1.859623566247138e-015
		0 1.5434215783417513 -0.18600257024510128
		;
createNode transform -n "finger_L0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.6755147938409509 10.100612620921169 -0.01517538801415016 ;
	setAttr ".r" -type "double3" 178.82811452781166 -2.898696009591919 -41.401670930328855 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L0_fk0_ctlShape" -p "finger_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L0_fk0_ctlShape1" -p "finger_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L0_fk0_ctlShape2" -p "finger_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.8053345519911677 9.986154379696849 -0.0064118963259415214 ;
	setAttr ".r" -type "double3" 178.82811452781166 -2.8986960095919194 -41.401670930328855 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999967 0.99999999999999978 ;
createNode nurbsCurve -n "finger_L0_fk1_ctlShape" -p "finger_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L0_fk1_ctlShape1" -p "finger_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L0_fk1_ctlShape2" -p "finger_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.93515431014138 9.8716961384725312 0.0023515953622660242 ;
	setAttr ".r" -type "double3" 178.82811452781166 -2.8986960095919199 -41.401670930328855 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L0_fk2_ctlShape" -p "finger_L0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L0_fk2_ctlShape1" -p "finger_L0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L0_fk2_ctlShape2" -p "finger_L0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L1_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.6961356305392306 10.113975839627155 -0.24993184127470136 ;
	setAttr ".r" -type "double3" 178.82949932961682 -0.80249541156140303 -41.444545673190426 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode nurbsCurve -n "finger_L1_fk0_ctlShape" -p "finger_L1_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L1_fk0_ctlShape1" -p "finger_L1_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L1_fk0_ctlShape2" -p "finger_L1_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L1_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.8260231676730818 9.9992849757934792 -0.24750473861896102 ;
	setAttr ".r" -type "double3" 178.82949932961682 -0.80249541156140269 -41.444545673190426 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
createNode nurbsCurve -n "finger_L1_fk1_ctlShape" -p "finger_L1_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L1_fk1_ctlShape1" -p "finger_L1_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L1_fk1_ctlShape2" -p "finger_L1_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L1_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.9559107048069366 9.8845941119598084 -0.24507763596322193 ;
	setAttr ".r" -type "double3" 178.82949932961682 -0.80249541156140303 -41.444545673190426 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L1_fk2_ctlShape" -p "finger_L1_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L1_fk2_ctlShape1" -p "finger_L1_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L1_fk2_ctlShape2" -p "finger_L1_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L2_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.6942738535038391 10.126138295933726 -0.45071084446270859 ;
	setAttr ".r" -type "double3" 178.82959483724636 0.32916155396792013 -41.467666490106105 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
createNode nurbsCurve -n "finger_L2_fk0_ctlShape" -p "finger_L2_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L2_fk0_ctlShape1" -p "finger_L2_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L2_fk0_ctlShape2" -p "finger_L2_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L2_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.824125692213304 10.01138566535278 -0.45170640231502907 ;
	setAttr ".r" -type "double3" 178.82959483724636 0.32916155396792007 -41.467666490106105 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999989 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L2_fk1_ctlShape" -p "finger_L2_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L2_fk1_ctlShape1" -p "finger_L2_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L2_fk1_ctlShape2" -p "finger_L2_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L2_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.9539775309227707 9.896633034771833 -0.45270196016734965 ;
	setAttr ".r" -type "double3" 178.82959483724636 0.32916155396792013 -41.467666490106105 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L2_fk2_ctlShape" -p "finger_L2_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L2_fk2_ctlShape1" -p "finger_L2_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L2_fk2_ctlShape2" -p "finger_L2_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L3_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.60179805066673 10.166912775559641 -0.64727392425547214 ;
	setAttr ".r" -type "double3" 178.80849157424618 0.31834942492464041 -44.886295167279883 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L3_fk0_ctlShape" -p "finger_L3_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L3_fk0_ctlShape1" -p "finger_L3_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L3_fk0_ctlShape2" -p "finger_L3_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L3_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.7245761419419852 10.044621031820165 -0.6482367808789008 ;
	setAttr ".r" -type "double3" 178.80849157424618 0.31834942492464041 -44.886295167279883 ;
createNode nurbsCurve -n "finger_L3_fk1_ctlShape" -p "finger_L3_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L3_fk1_ctlShape1" -p "finger_L3_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L3_fk1_ctlShape2" -p "finger_L3_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_L3_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.8473542332172377 9.9223292880806859 -0.64919963750232956 ;
	setAttr ".r" -type "double3" 178.80849157424618 0.31834942492464041 -44.886295167279883 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
createNode nurbsCurve -n "finger_L3_fk2_ctlShape" -p "finger_L3_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_L3_fk2_ctlShape1" -p "finger_L3_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_L3_fk2_ctlShape2" -p "finger_L3_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "thumb_L0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.1391583941786996 10.551403881847699 -0.25936483589254389 ;
	setAttr ".r" -type "double3" -84.105807434035867 -60.841949031538661 -82.465812349746486 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1.0000000000000002 ;
createNode nurbsCurve -n "thumb_L0_fk0_ctlShape" -p "thumb_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "thumb_L0_fk0_ctlShape1" -p "thumb_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "thumb_L0_fk0_ctlShape2" -p "thumb_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "thumb_L0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.17119139967485 10.309205873593072 0.17852387169304573 ;
	setAttr ".r" -type "double3" -97.113430832867465 -40.958773086758974 -64.654343112759108 ;
createNode nurbsCurve -n "thumb_L0_fk1_ctlShape" -p "thumb_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "thumb_L0_fk1_ctlShape1" -p "thumb_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "thumb_L0_fk1_ctlShape2" -p "thumb_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "thumb_L0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.286865866839622 10.064998502302396 0.41307973702877487 ;
	setAttr ".r" -type "double3" -93.801512386938825 -27.184462148079149 -67.46189682521107 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode nurbsCurve -n "thumb_L0_fk2_ctlShape" -p "thumb_L0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 -0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 -0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 -0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 -0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 -0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 -0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 -0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 -0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "thumb_L0_fk2_ctlShape1" -p "thumb_L0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 -0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "thumb_L0_fk2_ctlShape2" -p "thumb_L0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 -0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 -0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "armUI_L0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "shoulder_L0" -ln "shoulder_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "shoulder_L0_rotRef" -ln "shoulder_L0_rotRef" -nn "Ref" 
		-min 0 -max 3 -en "shoulder_L0_root:local_C0_root:body_C0_root:spine_C0_eff" -at "enum";
	addAttr -ci true -k true -sn "arm_L0" -ln "arm_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "arm_L0_blend" -ln "arm_L0_blend" -nn "Fk/Ik Blend" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_L0_roll" -ln "arm_L0_roll" -nn "Roll" -min -180 
		-max 180 -at "double";
	addAttr -ci true -k true -sn "arm_L0_aproll" -ln "arm_L0_aproll" -nn "Armpit Roll" 
		-min -360 -max 360 -at "double";
	addAttr -ci true -k true -sn "arm_L0_ikscale" -ln "arm_L0_ikscale" -nn "Scale" -dv 
		0.001 -min 0.001 -max 99 -at "double";
	addAttr -ci true -k true -sn "arm_L0_maxstretch" -ln "arm_L0_maxstretch" -nn "Max Stretch" 
		-dv 1 -min 1 -max 99 -at "double";
	addAttr -ci true -k true -sn "arm_L0_slide" -ln "arm_L0_slide" -nn "Slide" -min 
		0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_L0_softness" -ln "arm_L0_softness" -nn "Softness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_L0_reverse" -ln "arm_L0_reverse" -nn "Reverse" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_L0_roundness" -ln "arm_L0_roundness" -nn "Roundness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_L0_volume" -ln "arm_L0_volume" -nn "Volume" -min 
		0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_L0_ikref" -ln "arm_L0_ikref" -nn "Ik Ref" -min 
		0 -max 3 -en "local_C0_root:body_C0_root:spine_C0_eff:spine_C0_root" -at "enum";
	addAttr -ci true -k true -sn "arm_L0_upvref" -ln "arm_L0_upvref" -nn "UpV Ref" -min 
		0 -max 3 -en "local_C0_root:body_C0_root:spine_C0_eff:spine_C0_root" -at "enum";
	addAttr -ci true -k true -sn "finger_L0" -ln "finger_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "finger_L1" -ln "finger_L1" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "finger_L2" -ln "finger_L2" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "finger_L3" -ln "finger_L3" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "thumb_L0" -ln "thumb_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".arm_L0_aproll" -90;
	setAttr -k on ".arm_L0_ikscale" 1;
	setAttr -k on ".arm_L0_maxstretch" 1.5;
	setAttr -k on ".arm_L0_slide" 0.5;
	setAttr -k on ".arm_L0_volume" 1;
createNode nurbsCurve -n "armUI_L0_ctlShape" -p "armUI_L0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0.18982291243810895 0.28473436865716306 5.0809225893888913e-017
		0.28473436865716306 0.18982291243810895 5.0809225893888913e-017
		0.094911456219054474 0 5.0809225893888913e-017
		0.28473436865716306 -0.18982291243810895 5.0809225893888913e-017
		0.18982291243810895 -0.28473436865716306 5.0809225893888913e-017
		0 -0.094911456219054474 5.0809225893888913e-017
		-0.18982291243810895 -0.28473436865716306 5.0809225893888913e-017
		-0.28473436865716306 -0.18982291243810895 5.0809225893888913e-017
		-0.094911456219054474 0 5.0809225893888913e-017
		-0.28473436865716306 0.18982291243810895 5.0809225893888913e-017
		-0.18982291243810895 0.28473436865716306 5.0809225893888913e-017
		0 0.094911456219054474 5.0809225893888913e-017
		0.18982291243810895 0.28473436865716306 5.0809225893888913e-017
		;
createNode transform -n "neck_C0_ik_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.033704944915076397 16.688004287179361 -0.12562818605943307 ;
	setAttr -k on ".ro" 2;
createNode nurbsCurve -n "neck_C0_ik_ctlShape" -p "neck_C0_ik_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		3 6 0 no 3
		11 0 0 0 1 2 3 4 5 6 6 6
		9
		-0.47107016804958779 1.691346154938155 0.27604115314785505
		-0.47134363801412449 1.8355036701830889 0.2658334730592079
		-0.51145799018553961 2.1238187006729348 0.24541811288191667
		-0.21060034889991477 2.3012255924426102 0.23285607063507774
		-6.635744319809059e-016 2.2937725494545189 0.23338381473763242
		0.2106003488999198 2.3012255924426297 0.23285607063507641
		0.51145799018553273 2.1238187006728833 0.24541811288192034
		0.47134363801411494 1.8355036701830243 0.26583347305921301
		0.47107016804957669 1.6913461549380826 0.27604115314785937
		;
createNode transform -n "neck_C0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.033704944915076487 15.391673657547514 -0.30468053766879144 ;
	setAttr ".r" -type "double3" 9.3150108418808966 0 0 ;
	setAttr -k on ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode nurbsCurve -n "neck_C0_fk0_ctlShape" -p "neck_C0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.103715013872002 0.14033815668839064 -1.1214415556304045
		0.00060083544964732228 0.14033815668839067 -1.5783664091789305
		-1.1025133429724803 0.14033815668839064 -1.1214415556303752
		-1.5594381965211848 0.14033815668839059 -0.018327377208119916
		-1.1025133429725436 0.14033815668839053 1.0847868012140869
		0.0006008354496680866 0.1403381566883905 1.5417116547626972
		1.1037150138719702 0.14033815668839053 1.0847868012141044
		1.5606398674204689 0.14033815668839059 -0.018327377208099405
		1.103715013872002 0.14033815668839064 -1.1214415556304045
		0.00060083544964732228 0.14033815668839067 -1.5783664091789305
		-1.1025133429724803 0.14033815668839064 -1.1214415556303752
		;
createNode transform -n "neck_C0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.033704944915076446 16.037961497320303 -0.20053158175961999 ;
	setAttr ".r" -type "double3" 8.6312623880727362 0 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode nurbsCurve -n "neck_C0_fk1_ctlShape" -p "neck_C0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.8459652640294828 0.036729240017081548 0.63466858823918626
		0.8459652640294828 0.036729240017081548 -0.68764938218773608
		-0.83595561183837985 0.036729240017081548 -0.68764938218773608
		-0.83595561183837985 -0.11403840896986592 -0.68764938218773608
		-0.83595561183837985 -0.11403840896986592 0.63466858823918626
		-0.83595561183837985 0.036729240017081548 0.63466858823918626
		-0.83595561183837985 0.036729240017081548 -0.68764938218773608
		-0.83595561183837985 0.036729240017081548 0.63466858823918626
		0.8459652640294828 0.036729240017081548 0.63466858823918626
		0.8459652640294828 -0.11403840896986592 0.63466858823918626
		-0.83595561183837985 -0.11403840896986592 0.63466858823918626
		0.8459652640294828 -0.11403840896986592 0.63466858823918626
		0.8459652640294828 -0.11403840896986592 -0.68764938218773608
		0.8459652640294828 0.036729240017081548 -0.68764938218773608
		0.8459652640294828 -0.11403840896986592 -0.68764938218773608
		-0.83595561183837985 -0.11403840896986592 -0.68764938218773608
		;
createNode transform -n "neck_C0_head_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.033704944915076404 16.747843515283598 -0.12562818605943293 ;
	setAttr ".ro" 2;
createNode nurbsCurve -n "neck_C0_head_ctlShape" -p "neck_C0_head_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 6 0 no 3
		11 0 0 0 1 2 3 4 5 6 6 6
		9
		-1.3465007133138591 0.33060725937275492 0.16842574782652486
		-1.3602144826608129 0.33476951888543738 -0.1545983678145848
		-1.2268461871614942 0.3430940379107934 -0.8006465990967474
		-0.58603330408587628 0.34821630657386926 -1.1981749848015801
		0.039266878609193943 0.34800111488564056 -1.1814744151783265
		0.66456706130428078 0.34821630657386748 -1.1981749848016183
		1.3053799443798626 0.34309403791080051 -0.80064659909663527
		1.4387482398791775 0.33476951888543915 -0.15459836781443848
		1.4250344705322247 0.33060725937275848 0.1684257478266854
		;
createNode transform -n "faceUI_C1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "faceUI_C1_ctlShape" -p "faceUI_C1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.14587375446739353 0.14587375446739353 0.14587375446739353
		0.14587375446739353 0.14587375446739353 -0.14587375446739353
		-0.14587375446739353 0.14587375446739353 -0.14587375446739353
		-0.14587375446739353 -0.14587375446739353 -0.14587375446739353
		-0.14587375446739353 -0.14587375446739353 0.14587375446739353
		-0.14587375446739353 0.14587375446739353 0.14587375446739353
		-0.14587375446739353 0.14587375446739353 -0.14587375446739353
		-0.14587375446739353 0.14587375446739353 0.14587375446739353
		0.14587375446739353 0.14587375446739353 0.14587375446739353
		0.14587375446739353 -0.14587375446739353 0.14587375446739353
		-0.14587375446739353 -0.14587375446739353 0.14587375446739353
		0.14587375446739353 -0.14587375446739353 0.14587375446739353
		0.14587375446739353 -0.14587375446739353 -0.14587375446739353
		0.14587375446739353 0.14587375446739353 -0.14587375446739353
		0.14587375446739353 -0.14587375446739353 -0.14587375446739353
		-0.14587375446739353 -0.14587375446739353 -0.14587375446739353
		;
createNode transform -n "hoodSpring_C0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.033704944915076342 17.907986696410831 -0.12562818605943274 ;
	setAttr ".r" -type "double3" -90.000000000000213 83.129566026280074 90 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode nurbsCurve -n "hoodSpring_C0_fk0_ctlShape" -p "hoodSpring_C0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.8690299397613854 0.46209686732006433 0.067536076717666843
		1.8690299397613854 0.46209686732006433 -0.067536076717666843
		-0.095067538783352223 0.46209686732006433 -0.067536076717666843
		-0.095067538783352223 -0.46209686732006688 -0.067536076717666843
		-0.095067538783352223 -0.46209686732006688 0.067536076717666843
		-0.095067538783352223 0.46209686732006433 0.067536076717666843
		-0.095067538783352223 0.46209686732006433 -0.067536076717666843
		-0.095067538783352223 0.46209686732006433 0.067536076717666843
		1.8690299397613854 0.46209686732006433 0.067536076717666843
		1.8690299397613854 -0.46209686732006688 0.067536076717666843
		-0.095067538783352223 -0.46209686732006688 0.067536076717666843
		1.8690299397613854 -0.46209686732006688 0.067536076717666843
		1.8690299397613854 -0.46209686732006688 -0.067536076717666843
		1.8690299397613854 0.46209686732006433 -0.067536076717666843
		1.8690299397613854 -0.46209686732006688 -0.067536076717666843
		-0.095067538783352223 -0.46209686732006688 -0.067536076717666843
		;
createNode transform -n "mouth_C0_jaw_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.022287610940339698 16.157604685773055 1.050596672290427 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "mouth_C0_jaw_ctlShape" -p "mouth_C0_jaw_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		11
		0 0.14673896945860468 0.035311956902855025
		0.1032999965502812 0.10329999655028123 0.035311956902855018
		0.14673896945860454 -9.8010368995302261e-019 0.035311956902855046
		0.1032999965502812 -0.10329999655028123 0.035311956902855074
		0 -0.14673896945860468 0.035311956902855067
		-0.1032999965502812 -0.10329999655028123 0.035311956902855074
		-0.14673896945860454 -9.8010368995302261e-019 0.035311956902855046
		-0.1032999965502812 0.10329999655028123 0.035311956902855018
		0 0.14673896945860468 0.035311956902855025
		0.1032999965502812 0.10329999655028123 0.035311956902855018
		0.14673896945860454 -9.8010368995302261e-019 0.035311956902855046
		;
createNode transform -n "mouth_C0_lipup_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.006278507470107093 16.602640209517833 1.0763113988607707 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "mouth_C0_lipup_ctlShape" -p "mouth_C0_lipup_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 2 no 3
		5 0 1 2 3 4
		5
		0.1558846850169687 0.031165916248924112 0.053988842316000873
		0.1558846850169687 -0.03876142765028643 0.053988842316000846
		-0.1558846850169687 -0.03876142765028643 0.053988842316000846
		-0.1558846850169687 0.031165916248924112 0.053988842316000873
		0.1558846850169687 0.031165916248924112 0.053988842316000873
		;
createNode transform -n "mouth_C0_liplow_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.020931791831023262 16.447294367358392 1.0451220761794033 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "mouth_C0_liplow_ctlShape" -p "mouth_C0_liplow_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 2 no 3
		5 0 1 2 3 4
		5
		0.16652017595225202 0.10603285260268611 0.093267582420404366
		0.16652017595225202 -0.046202390230506596 0.093267582420404338
		-0.16652017595225202 -0.046202390230506596 0.093267582420404338
		-0.16652017595225202 0.10603285260268611 0.093267582420404366
		0.16652017595225202 0.10603285260268611 0.093267582420404366
		;
createNode transform -n "mouth_C0_teethup_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.006278507470107093 16.602640209517833 1.0763113988607707 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "mouth_C0_teethup_ctlShape" -p "mouth_C0_teethup_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 2 no 3
		5 0 1 2 3 4
		5
		0.25161841092054277 0.028448264289770191 0.020451623154253085
		0.25161841092054277 -0.0564886590166611 0.020451623154253057
		-0.25161841092054277 -0.0564886590166611 0.020451623154253057
		-0.25161841092054277 0.028448264289770191 0.020451623154253085
		0.25161841092054277 0.028448264289770191 0.020451623154253085
		;
createNode transform -n "mouth_C0_teethlow_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.020931791831023262 16.447294367358392 1.0451220761794033 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "mouth_C0_teethlow_ctlShape" -p "mouth_C0_teethlow_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 2 no 3
		5 0 1 2 3 4
		5
		0.24676275213805829 0.025490441092479974 0.050803246113579031
		0.24676275213805829 -0.066002831255819183 0.050803246113579004
		-0.24676275213805829 -0.066002831255819183 0.050803246113579004
		-0.24676275213805829 0.025490441092479974 0.050803246113579031
		0.24676275213805829 0.025490441092479974 0.050803246113579031
		;
createNode transform -n "eye_L0_Over_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.31964625090360638 17.226664263333614 0.76190092825511613 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_L0_Over_ctlShape" -p "eye_L0_Over_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.42820690825979102 0 0
		0 -0.42820690825979102 0
		-0.42820690825979102 0 0
		0 0.42820690825979102 0
		0.42820690825979102 0 0
		0 -0.42820690825979102 0
		-0.42820690825979102 0 0
		;
createNode nurbsCurve -n "eye_L0_Over_ctl_0crvShape" -p "eye_L0_Over_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.42820690825979102 0 0
		0 0 -0.42820690825979102
		-0.42820690825979102 0 0
		0 0 0.42820690825979102
		0.42820690825979102 0 0
		0 0 -0.42820690825979102
		-0.42820690825979102 0 0
		;
createNode nurbsCurve -n "eye_L0_Over_ctl_1crvShape" -p "eye_L0_Over_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.42820690825979102
		0 -0.42820690825979102 0
		0 0 -0.42820690825979102
		0 0.42820690825979102 0
		0 0 0.42820690825979102
		0 -0.42820690825979102 0
		0 0 -0.42820690825979102
		;
createNode transform -n "eye_L0_fk_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.31964625090360638 17.226664263333614 0.76190092825511613 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_L0_fk_ctlShape" -p "eye_L0_fk_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 2 no 3
		8 0 1 2 3 4 5 6 7
		8
		0 0.060042004442904542 0.36119951002891915
		0 0.060042004442904542 0.62138152928150558
		0 0.12008400888580908 0.62138152928150558
		0 0 0.76147953964828297
		0 -0.12008400888580908 0.62138152928150558
		0 -0.060042004442904542 0.62138152928150558
		0 -0.060042004442904542 0.36119951002891915
		0 0.060042004442904542 0.36119951002891915
		;
createNode transform -n "eye_L0_squashUp_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.31964625090360638 17.226664263333614 0.76190092825511613 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_L0_squashUp_ctlShape" -p "eye_L0_squashUp_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 2 no 3
		8 0 1 2 3 4 5 6 7
		8
		0 0.014999999999999999 -0.050000000000000003
		0 0.014999999999999999 0.014999999999999999
		0 0.029999999999999999 0.014999999999999999
		0 0 0.050000000000000003
		0 -0.029999999999999999 0.014999999999999999
		0 -0.014999999999999999 0.014999999999999999
		0 -0.014999999999999999 -0.050000000000000003
		0 0.014999999999999999 -0.050000000000000003
		;
createNode transform -n "eye_L0_squashDown_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.31964625090360638 17.226664263333614 0.76190092825511613 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_L0_squashDown_ctlShape" -p "eye_L0_squashDown_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 2 no 3
		8 0 1 2 3 4 5 6 7
		8
		0 0.014999999999999999 -0.050000000000000003
		0 0.014999999999999999 0.014999999999999999
		0 0.029999999999999999 0.014999999999999999
		0 0 0.050000000000000003
		0 -0.029999999999999999 0.014999999999999999
		0 -0.014999999999999999 0.014999999999999999
		0 -0.014999999999999999 -0.050000000000000003
		0 0.014999999999999999 -0.050000000000000003
		;
createNode transform -n "eye_L0_ik_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.3196462509036071 17.226664263333607 3.0177107101202449 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_L0_ik_ctlShape" -p "eye_L0_ik_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		11
		0 0.27700000000000002 -5.5511151231257827e-017
		0.19500000000000001 0.19499999999999998 -1.3877787807814457e-017
		0.27700000000000002 0 0
		0.19500000000000001 -0.19499999999999998 1.3877787807814457e-017
		0 -0.27700000000000002 5.5511151231257827e-017
		-0.19500000000000001 -0.19499999999999998 1.3877787807814457e-017
		-0.27700000000000002 0 0
		-0.19500000000000001 0.19499999999999998 -1.3877787807814457e-017
		0 0.27700000000000002 -5.5511151231257827e-017
		0.19500000000000001 0.19499999999999998 -1.3877787807814457e-017
		0.27700000000000002 0 0
		;
createNode transform -n "eye_R0_Over_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.3196462509036066 17.226664263333614 0.7617875667264955 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_R0_Over_ctlShape" -p "eye_R0_Over_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.42820690825979102 0 0
		0 -0.42820690825979102 0
		-0.42820690825979102 0 0
		0 0.42820690825979102 0
		0.42820690825979102 0 0
		0 -0.42820690825979102 0
		-0.42820690825979102 0 0
		;
createNode nurbsCurve -n "eye_R0_Over_ctl_0crvShape" -p "eye_R0_Over_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.42820690825979102 0 0
		0 0 -0.42820690825979102
		-0.42820690825979102 0 0
		0 0 0.42820690825979102
		0.42820690825979102 0 0
		0 0 -0.42820690825979102
		-0.42820690825979102 0 0
		;
createNode nurbsCurve -n "eye_R0_Over_ctl_1crvShape" -p "eye_R0_Over_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.42820690825979102
		0 -0.42820690825979102 0
		0 0 -0.42820690825979102
		0 0.42820690825979102 0
		0 0 0.42820690825979102
		0 -0.42820690825979102 0
		0 0 -0.42820690825979102
		;
createNode transform -n "eye_R0_fk_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.3196462509036066 17.226664263333614 0.7617875667264955 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_R0_fk_ctlShape" -p "eye_R0_fk_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 2 no 3
		8 0 1 2 3 4 5 6 7
		8
		0 0.060042004442904542 0.36119951002891915
		0 0.060042004442904542 0.62138152928150558
		0 0.12008400888580908 0.62138152928150558
		0 0 0.76147953964828297
		0 -0.12008400888580908 0.62138152928150558
		0 -0.060042004442904542 0.62138152928150558
		0 -0.060042004442904542 0.36119951002891915
		0 0.060042004442904542 0.36119951002891915
		;
createNode transform -n "eye_R0_squashUp_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.3196462509036066 17.226664263333614 0.7617875667264955 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_R0_squashUp_ctlShape" -p "eye_R0_squashUp_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 2 no 3
		8 0 1 2 3 4 5 6 7
		8
		0 0.014999999999999999 -0.050000000000000003
		0 0.014999999999999999 0.014999999999999999
		0 0.029999999999999999 0.014999999999999999
		0 0 0.050000000000000003
		0 -0.029999999999999999 0.014999999999999999
		0 -0.014999999999999999 0.014999999999999999
		0 -0.014999999999999999 -0.050000000000000003
		0 0.014999999999999999 -0.050000000000000003
		;
createNode transform -n "eye_R0_squashDown_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.3196462509036066 17.226664263333614 0.7617875667264955 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_R0_squashDown_ctlShape" -p "eye_R0_squashDown_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 2 no 3
		8 0 1 2 3 4 5 6 7
		8
		0 0.014999999999999999 -0.050000000000000003
		0 0.014999999999999999 0.014999999999999999
		0 0.029999999999999999 0.014999999999999999
		0 0 0.050000000000000003
		0 -0.029999999999999999 0.014999999999999999
		0 -0.014999999999999999 0.014999999999999999
		0 -0.014999999999999999 -0.050000000000000003
		0 0.014999999999999999 -0.050000000000000003
		;
createNode transform -n "eye_R0_ik_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.31964625090360621 17.2266642633336 3.0175973485916243 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
createNode nurbsCurve -n "eye_R0_ik_ctlShape" -p "eye_R0_ik_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		11
		0 0.27700000000000008 -1.3877787807814457e-017
		0.19500000000000001 0.19500000000000001 0
		0.27700000000000002 0 0
		0.19500000000000001 -0.19500000000000001 0
		0 -0.27700000000000008 1.3877787807814457e-017
		-0.19500000000000001 -0.19500000000000001 0
		-0.27700000000000002 0 0
		-0.19500000000000001 0.19500000000000001 0
		0 0.27700000000000008 -1.3877787807814457e-017
		0.19500000000000001 0.19500000000000001 0
		0.27700000000000002 0 0
		;
createNode transform -n "eyeslook_C0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3877787807814457e-016 17.223789624339691 3.0291498504836034 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-015 0 0 ;
	setAttr -k on ".ro";
createNode nurbsCurve -n "eyeslook_C0_ctlShape" -p "eyeslook_C0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 2 no 3
		5 0 1 2 3 4
		5
		0.81085803546283342 -0.28375894458698481 8.3266726846886741e-017
		0.81085803546283342 0.24370695322738767 -8.3266726846886741e-017
		-0.81085803546283342 0.24370695322738767 -8.3266726846886741e-017
		-0.81085803546283342 -0.28375894458698481 8.3266726846886741e-017
		0.81085803546283342 -0.28375894458698481 8.3266726846886741e-017
		;
createNode transform -n "spineUI_C0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "body_C0" -ln "body_C0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "spine_C0" -ln "spine_C0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "spine_C0_position" -ln "spine_C0_position" -nn "Position" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "spine_C0_maxstretch" -ln "spine_C0_maxstretch" -nn "Max Stretch" 
		-dv 1 -min 1 -at "double";
	addAttr -ci true -k true -sn "spine_C0_maxsquash" -ln "spine_C0_maxsquash" -nn "Max Squash" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "spine_C0_softness" -ln "spine_C0_softness" -nn "Softness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "spine_C0_lock_ori0" -ln "spine_C0_lock_ori0" -nn "Lock Ori 0" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "spine_C0_lock_ori1" -ln "spine_C0_lock_ori1" -nn "Lock Ori 1" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "spine_C0_tan0" -ln "spine_C0_tan0" -nn "Tangent 0" 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "spine_C0_tan1" -ln "spine_C0_tan1" -nn "Tangent 1" 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "spine_C0_volume" -ln "spine_C0_volume" -nn "Volume" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "neck_C0" -ln "neck_C0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "neck_C0_maxstretch" -ln "neck_C0_maxstretch" -nn "Max Stretch" 
		-dv 1 -min 1 -at "double";
	addAttr -ci true -k true -sn "neck_C0_maxsquash" -ln "neck_C0_maxsquash" -nn "MaxSquash" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "neck_C0_softness" -ln "neck_C0_softness" -nn "Softness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "neck_C0_lock_ori" -ln "neck_C0_lock_ori" -nn "Lock Ori" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "neck_C0_tan0" -ln "neck_C0_tan0" -nn "Tangent 0" -min 
		0 -at "double";
	addAttr -ci true -k true -sn "neck_C0_tan1" -ln "neck_C0_tan1" -nn "Tangent 1" -min 
		0 -at "double";
	addAttr -ci true -k true -sn "neck_C0_volume" -ln "neck_C0_volume" -nn "Volume" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "neck_C0_ikref" -ln "neck_C0_ikref" -nn "Ik Ref" -min 
		0 -max 2 -en "spine_C0_eff:body_C0_root:local_C0_root" -at "enum";
	addAttr -ci true -k true -sn "neck_C0_headref" -ln "neck_C0_headref" -nn "Head Ref" 
		-min 0 -max 3 -en "self:spine_C0_eff:body_C0_root:local_C0_root" -at "enum";
	addAttr -ci true -k true -sn "hoodSpring_C0" -ln "hoodSpring_C0" -min 0 -max 0 -en 
		"---------------" -at "enum";
	addAttr -ci true -k true -sn "hoodSpring_C0_spring_active" -ln "hoodSpring_C0_spring_active" 
		-nn "Spring active" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodSpring_C0_spring_intensity" -ln "hoodSpring_C0_spring_intensity" 
		-nn "Spring chain intensity" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodSpring_C0_damping_0" -ln "hoodSpring_C0_damping_0" 
		-nn "damping_0" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodSpring_C0_stiffness_0" -ln "hoodSpring_C0_stiffness_0" 
		-nn "stiffness_0" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodySpring_C1" -ln "hoodySpring_C1" -min 0 -max 0 
		-en "---------------" -at "enum";
	addAttr -ci true -k true -sn "hoodySpring_C1_spring_active" -ln "hoodySpring_C1_spring_active" 
		-nn "Spring active" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodySpring_C1_spring_intensity" -ln "hoodySpring_C1_spring_intensity" 
		-nn "Spring chain intensity" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodySpring_C1_damping_0" -ln "hoodySpring_C1_damping_0" 
		-nn "damping_0" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodySpring_C1_stiffness_0" -ln "hoodySpring_C1_stiffness_0" 
		-nn "stiffness_0" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodySpring_C0" -ln "hoodySpring_C0" -min 0 -max 0 
		-en "---------------" -at "enum";
	addAttr -ci true -k true -sn "hoodySpring_C0_spring_active" -ln "hoodySpring_C0_spring_active" 
		-nn "Spring active" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodySpring_C0_spring_intensity" -ln "hoodySpring_C0_spring_intensity" 
		-nn "Spring chain intensity" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodySpring_C0_damping_0" -ln "hoodySpring_C0_damping_0" 
		-nn "damping_0" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "hoodySpring_C0_stiffness_0" -ln "hoodySpring_C0_stiffness_0" 
		-nn "stiffness_0" -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".spine_C0_maxstretch" 1.5;
	setAttr -k on ".spine_C0_maxsquash" 0.5;
	setAttr -k on ".spine_C0_lock_ori0" 1;
	setAttr -k on ".spine_C0_lock_ori1" 1;
	setAttr -k on ".spine_C0_tan0" 1;
	setAttr -k on ".spine_C0_tan1" 1;
	setAttr -k on ".spine_C0_volume" 1;
	setAttr -k on ".neck_C0_maxstretch" 1.5;
	setAttr -k on ".neck_C0_maxsquash" 0.5;
	setAttr -k on ".neck_C0_lock_ori" 1;
	setAttr -k on ".neck_C0_tan0" 1;
	setAttr -k on ".neck_C0_tan1" 1;
	setAttr -k on ".neck_C0_volume" 1;
	setAttr -k on ".hoodSpring_C0_spring_active" 1;
	setAttr -k on ".hoodSpring_C0_damping_0" 0.5;
	setAttr -k on ".hoodSpring_C0_stiffness_0" 0.5;
	setAttr -k on ".hoodySpring_C1_spring_active" 1;
	setAttr -k on ".hoodySpring_C1_damping_0" 0.5;
	setAttr -k on ".hoodySpring_C1_stiffness_0" 0.5;
	setAttr -k on ".hoodySpring_C0_spring_active" 1;
	setAttr -k on ".hoodySpring_C0_damping_0" 0.5;
	setAttr -k on ".hoodySpring_C0_stiffness_0" 0.5;
createNode nurbsCurve -n "spineUI_C0_ctlShape" -p "spineUI_C0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 6 0 no 3
		7 0 1 2 3 4 5 6
		7
		-0.30452302062406922 -0.43221788562881325 0
		0.28385076409009891 -0.43221788562881325 0
		0.28385076409009891 0.15615589908535488 0
		0.16351240020026414 0.45034279144244094 0
		-0.16893957588621505 0.45034279144244094 0
		-0.30452302062406922 0.15615589908535488 0
		-0.30452302062406922 -0.43221788562881325 0
		;
createNode transform -n "shoulder_R0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.11673327753264909 14.596096560902707 0.40997110536358694 ;
	setAttr ".r" -type "double3" -179.15153018929709 -19.58853221510309 5.8001935627414056 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000004 ;
createNode nurbsCurve -n "shoulder_R0_ctlShape" -p "shoulder_R0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.32680858911735178 0.084608650497506085 -0.82005819098080324
		0.32680858911735178 0.084608650497506085 -1.0164739347385543
		-1.6373488484601604 0.084608650497506085 -1.0164739347385543
		-1.6373488484601604 -0.1118070932602452 -1.0164739347385543
		-1.6373488484601604 -0.1118070932602452 -0.82005819098080324
		-1.6373488484601604 0.084608650497506085 -0.82005819098080324
		-1.6373488484601604 0.084608650497506085 -1.0164739347385543
		-1.6373488484601604 0.084608650497506085 -0.82005819098080324
		0.32680858911735178 0.084608650497506085 -0.82005819098080324
		0.32680858911735178 -0.1118070932602452 -0.82005819098080324
		-1.6373488484601604 -0.1118070932602452 -0.82005819098080324
		0.32680858911735178 -0.1118070932602452 -0.82005819098080324
		0.32680858911735178 -0.1118070932602452 -1.0164739347385543
		0.32680858911735178 0.084608650497506085 -1.0164739347385543
		0.32680858911735178 -0.1118070932602452 -1.0164739347385543
		-1.6373488484601604 -0.1118070932602452 -1.0164739347385543
		;
createNode transform -n "shoulder_R0_orbit_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.9577404853947686 14.409087584494026 -0.24853822861229719 ;
	setAttr ".r" -type "double3" -1.4312496066585827e-014 6.3611093629270335e-015 6.1623246953355635e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode nurbsCurve -n "shoulder_R0_orbit_ctlShape" -p "shoulder_R0_orbit_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.36827951954578386 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 0.92875698653666905 2.7755575615628914e-016
		-0.36827951954578342 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 1.6653160256282362 2.7755575615628914e-016
		0.36827951954578386 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 0.92875698653666905 2.7755575615628914e-016
		-0.36827951954578342 1.2970365060824527 2.7755575615628914e-016
		;
createNode nurbsCurve -n "shoulder_R0_orbit_ctl_0crvShape" -p "shoulder_R0_orbit_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.36827951954578386 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 -0.36827951954578331
		-0.36827951954578342 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 0.36827951954578397
		0.36827951954578386 1.2970365060824527 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 -0.36827951954578331
		-0.36827951954578342 1.2970365060824527 2.7755575615628914e-016
		;
createNode nurbsCurve -n "shoulder_R0_orbit_ctl_1crvShape" -p "shoulder_R0_orbit_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		2.2204460492503131e-016 1.2970365060824527 0.36827951954578397
		2.2204460492503131e-016 0.92875698653666905 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 -0.36827951954578331
		2.2204460492503131e-016 1.6653160256282362 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 0.36827951954578397
		2.2204460492503131e-016 0.92875698653666905 2.7755575615628914e-016
		2.2204460492503131e-016 1.2970365060824527 -0.36827951954578331
		;
createNode transform -n "arm_R0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.9577404853947706 14.409087584494022 -0.24853822861229741 ;
	setAttr ".r" -type "double3" 106.94636308732609 -8.7083905508289661 40.362323240370785 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_R0_fk0_ctlShape" -p "arm_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.2472965854107303e-015 0.8337949878753782 -0.83379498787537953
		1.1962413672583964e-015 1.1791641800920698 1.3876498381165692e-015
		1.1451861491060607e-015 0.83379498787537865 0.83379498787537876
		1.1240383853174478e-015 2.8828471199917801e-016 1.1791641800920698
		1.1451861491060603e-015 -0.83379498787537831 0.83379498787537898
		1.1962413672583964e-015 -1.17916418009207 1.6084264846501593e-015
		1.2472965854107303e-015 -0.83379498787537865 -0.83379498787537809
		1.2684443491993448e-015 -6.8673894839659927e-016 -1.1791641800920698
		1.2472965854107303e-015 0.8337949878753782 -0.83379498787537953
		1.1962413672583964e-015 1.1791641800920698 1.3876498381165692e-015
		1.1451861491060607e-015 0.83379498787537865 0.83379498787537876
		;
createNode transform -n "arm_R0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.9921266473248487 12.659828774293095 -0.560147960664519 ;
	setAttr ".r" -type "double3" 101.24978686725223 5.0790881887670629 43.017636032621553 ;
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_R0_fk1_ctlShape" -p "arm_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.8388571621958399e-015 0.74368626358654955 -0.74368626358654877
		2.793319512082285e-015 1.0517312001146728 3.7646752364070994e-015
		2.7477818619687289e-015 0.74368626358654955 0.74368626358655077
		2.7289195496930939e-015 -9.7044020322843417e-016 1.0517312001146728
		2.7477818619687289e-015 -0.74368626358654955 0.74368626358655077
		2.793319512082285e-015 -1.0517312001146728 3.9615924183706276e-015
		2.8388571621958399e-015 -0.74368626358654966 -0.74368626358654844
		2.8577194744714781e-015 -1.8400924720144327e-015 -1.0517312001146728
		2.8388571621958399e-015 0.74368626358654955 -0.74368626358654877
		2.793319512082285e-015 1.0517312001146728 3.7646752364070994e-015
		2.7477818619687289e-015 0.74368626358654955 0.74368626358655077
		;
createNode transform -n "arm_R0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.9607127890490759 10.822959309523602 -0.32084147633123755 ;
	setAttr ".r" -type "double3" 96.823161442598973 42.54690233694506 -9.2018679521918632 ;
	setAttr ".ro" 1;
	setAttr ".s" -type "double3" 1 1 0.99999999999999967 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_R0_fk2_ctlShape" -p "arm_R0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		9.6176866831985692e-016 0.31008497065817131 -0.31008497065817175
		9.4278143998084473e-016 0.43852637099284963 5.0030615453149077e-017
		9.2379421164183333e-016 0.31008497065817148 0.31008497065817148
		9.1592944415193979e-016 1.8842519114164171e-016 0.43852637099284963
		9.2379421164183333e-016 -0.31008497065817142 0.31008497065817148
		9.4278143998084473e-016 -0.43852637099284969 1.3213655391269997e-016
		9.6176866831985692e-016 -0.31008497065817148 -0.31008497065817131
		9.6963343580975065e-016 -1.7418214941727817e-016 -0.43852637099284963
		9.6176866831985692e-016 0.31008497065817131 -0.31008497065817175
		9.4278143998084473e-016 0.43852637099284963 5.0030615453149077e-017
		9.2379421164183333e-016 0.31008497065817148 0.31008497065817148
		;
createNode transform -n "arm_R0_ikcns_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.9607127890490732 10.822959309523604 -0.320841476331238 ;
	setAttr ".r" -type "double3" -1.4312496066585827e-014 6.3611093629270335e-015 6.1623246953355635e-015 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_R0_ikcns_ctlShape" -p "arm_R0_ikcns_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.38900473401072044 0 0
		-0.38900473401072044 0 0
		;
createNode nurbsCurve -n "arm_R0_ikcns_ctl1Shape" -p "arm_R0_ikcns_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.38900473401072044 0
		0 -0.38900473401072044 0
		;
createNode nurbsCurve -n "arm_R0_ikcns_ctl2Shape" -p "arm_R0_ikcns_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.38900473401072044
		0 0 -0.38900473401072044
		;
createNode transform -n "arm_R0_ik_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.9607127890490732 10.822959309523604 -0.320841476331238 ;
	setAttr ".r" -type "double3" 6.8231614425989537 9.2018679521918543 42.546902336945045 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999989 ;
	setAttr ".invTx" yes;
	setAttr ".invRy" yes;
	setAttr ".invRz" yes;
createNode nurbsCurve -n "arm_R0_ik_ctlShape" -p "arm_R0_ik_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		-1.3877787807814457e-017 0.45050662509386025 -0.18660595405280803
		-3.0814879110195774e-033 0.23527802922032837 -6.5518763514851254e-017
		-4.8572257327350599e-017 0.45050662509386025 0.18660595405280858
		-6.9388939039072284e-018 0.16636668992590109 0.1663666899259012
		-4.163336342344337e-017 0.18660595405280841 0.45050662509386025
		1.3877787807814457e-017 -1.2737024464650119e-016 0.23527802922032853
		-1.3877787807814457e-017 -0.18660595405280844 0.45050662509386014
		-6.9388939039072284e-018 -0.1663666899259012 0.1663666899259012
		-2.0816681711721685e-017 -0.45050662509386036 0.18660595405280847
		1.5407439555097887e-033 -0.23527802922032837 -2.7522244716879273e-017
		1.3877787807814457e-017 -0.45050662509386025 -0.18660595405280858
		-1.3877787807814457e-017 -0.16636668992590109 -0.16636668992590112
		1.3877787807814457e-017 -0.18660595405280828 -0.45050662509386036
		6.9388939039072284e-018 -7.6097846718485941e-017 -0.23527802922032817
		4.163336342344337e-017 0.18660595405280861 -0.45050662509386025
		-1.3877787807814457e-017 0.16636668992590109 -0.16636668992590112
		-1.3877787807814457e-017 0.45050662509386025 -0.18660595405280803
		-3.0814879110195774e-033 0.23527802922032837 -6.5518763514851254e-017
		-4.8572257327350599e-017 0.45050662509386025 0.18660595405280858
		;
createNode transform -n "arm_R0_upv_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.3806325800321719 13.157600957241229 -3.7397618588520896 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".invTx" yes;
createNode nurbsCurve -n "arm_R0_upv_ctlShape" -p "arm_R0_upv_ctl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		-0.12027045666498232 0.36081136999494701 0.94425699999999324
		-0.12027045666498232 0.12027045666498232 0.94425699999999324
		-0.36081136999494701 0.12027045666498232 0.94425699999999324
		-0.36081136999494701 -0.12027045666498232 0.94425699999999324
		-0.12027045666498232 -0.12027045666498232 0.94425699999999324
		-0.12027045666498232 -0.36081136999494701 0.94425699999999324
		0.12027045666498232 -0.36081136999494701 0.94425699999999324
		0.12027045666498232 -0.12027045666498232 0.94425699999999324
		0.36081136999494701 -0.12027045666498232 0.94425699999999324
		0.36081136999494701 0.12027045666498232 0.94425699999999324
		0.12027045666498232 0.12027045666498232 0.94425699999999324
		0.12027045666498232 0.36081136999494701 0.94425699999999324
		-0.12027045666498232 0.36081136999494701 0.94425699999999324
		;
createNode transform -n "arm_R0_mid_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.9921266473248482 12.659828774293089 -0.56014796066451888 ;
	setAttr ".r" -type "double3" 101.20607384642697 -0.77297307581243879 41.855656197465812 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "arm_R0_mid_ctlShape" -p "arm_R0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		9.4592675726131794e-018 -1.7610178978175268 -0.15448156283425266
		-5.9329647601094789e-031 -1.606536334983224 -0.2184699212967883
		-9.4592675726112566e-018 -1.4520547721488377 -0.15448156283424944
		-1.3377424491305328e-017 -1.3880664136865217 2.9203312906209065e-015
		-9.4592675726122242e-018 -1.4520547721489014 0.15448156283424944
		-3.0606465976928153e-031 -1.6065363349832178 0.21846992129679466
		9.4592675726127172e-018 -1.7610178978175119 0.15448156283425199
		1.3377424491304052e-017 -1.8250062562799467 7.916334901434111e-015
		9.4592675726131794e-018 -1.7610178978175268 -0.15448156283425266
		-5.9329647601094789e-031 -1.606536334983224 -0.2184699212967883
		-9.4592675726112566e-018 -1.4520547721488377 -0.15448156283424944
		;
createNode nurbsCurve -n "arm_R0_mid_ctlShape1" -p "arm_R0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0 -1.5534700490890423 -0.053066285894168806
		0 -1.4473374773007011 -0.053066285894168862
		0 -1.4473374773007011 0.053066285894171984
		0 -1.5534700490890423 0.053066285894172095
		0 -1.5534700490890423 0.15919885768251302
		0 -1.6596026208773835 0.15919885768251299
		0 -1.6596026208773833 0.053066285894172206
		0 -1.7657351926657221 0.053066285894171929
		0 -1.7657351926657221 -0.053066285894168695
		0 -1.6596026208773831 -0.053066285894168806
		0 -1.6596026208773833 -0.15919885768250966
		0 -1.5534700490890425 -0.15919885768250966
		0 -1.5534700490890423 -0.053066285894168806
		;
createNode nurbsCurve -n "arm_R0_mid_ctlShape2" -p "arm_R0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		0 -1.5359963128545246 -0.18414625387329039
		0 -1.6653345369377354e-015 1.8873791418627653e-015
		0 -1.5434215783417549 0.18600257024510136
		;
createNode transform -n "finger_R0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.6755147938409465 10.100612620921167 -0.015175388014151666 ;
	setAttr ".r" -type "double3" -1.1718854721883627 2.8986960095919141 41.401670930327789 ;
	setAttr ".s" -type "double3" 1.0000000000000009 0.99999999999999911 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R0_fk0_ctlShape" -p "finger_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R0_fk0_ctlShape1" -p "finger_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R0_fk0_ctlShape2" -p "finger_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.8053345519911641 9.9861543796968526 -0.0064118963259433177 ;
	setAttr ".r" -type "double3" -1.1718854721883627 2.8986960095919141 41.401670930327789 ;
	setAttr ".s" -type "double3" 1.0000000000000009 0.99999999999999911 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R0_fk1_ctlShape" -p "finger_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R0_fk1_ctlShape1" -p "finger_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R0_fk1_ctlShape2" -p "finger_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.9351543101413764 9.8716961384725295 0.0023515953622644378 ;
	setAttr ".r" -type "double3" -1.1718854721883627 2.8986960095919141 41.401670930327789 ;
	setAttr ".s" -type "double3" 1.0000000000000009 0.99999999999999911 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R0_fk2_ctlShape" -p "finger_R0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R0_fk2_ctlShape1" -p "finger_R0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R0_fk2_ctlShape2" -p "finger_R0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R1_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.696135630539227 10.11397583962715 -0.24993184127470328 ;
	setAttr ".r" -type "double3" -1.1705006703831728 0.80249541156148663 41.444545673190653 ;
	setAttr ".s" -type "double3" 1.0000000000000007 0.99999999999999889 1 ;
createNode nurbsCurve -n "finger_R1_fk0_ctlShape" -p "finger_R1_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R1_fk0_ctlShape1" -p "finger_R1_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R1_fk0_ctlShape2" -p "finger_R1_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R1_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.8260231676730809 9.9992849757934774 -0.24750473861896283 ;
	setAttr ".r" -type "double3" -1.1705006703831731 0.80249541156148663 41.444545673190653 ;
	setAttr ".s" -type "double3" 1.0000000000000007 0.999999999999999 1 ;
createNode nurbsCurve -n "finger_R1_fk1_ctlShape" -p "finger_R1_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R1_fk1_ctlShape1" -p "finger_R1_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R1_fk1_ctlShape2" -p "finger_R1_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R1_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.9559107048069331 9.8845941119598049 -0.24507763596322377 ;
	setAttr ".r" -type "double3" -1.1705006703831731 0.80249541156148663 41.444545673190653 ;
	setAttr ".s" -type "double3" 1.0000000000000009 0.999999999999999 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R1_fk2_ctlShape" -p "finger_R1_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R1_fk2_ctlShape1" -p "finger_R1_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R1_fk2_ctlShape2" -p "finger_R1_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R2_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.6942738535038346 10.126138295933725 -0.45071084446270959 ;
	setAttr ".r" -type "double3" -1.1704051627536103 -0.32916155396787211 41.467666490105053 ;
	setAttr ".s" -type "double3" 1.0000000000000011 0.99999999999999922 1 ;
createNode nurbsCurve -n "finger_R2_fk0_ctlShape" -p "finger_R2_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R2_fk0_ctlShape1" -p "finger_R2_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R2_fk0_ctlShape2" -p "finger_R2_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R2_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.8241256922133049 10.011385665352785 -0.45170640231503006 ;
	setAttr ".r" -type "double3" -1.1704051627536105 -0.32916155396787211 41.46766649010506 ;
	setAttr ".s" -type "double3" 1.0000000000000009 0.999999999999999 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R2_fk1_ctlShape" -p "finger_R2_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R2_fk1_ctlShape1" -p "finger_R2_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R2_fk1_ctlShape2" -p "finger_R2_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R2_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.9539775309227645 9.8966330347718294 -0.45270196016735048 ;
	setAttr ".r" -type "double3" -1.1704051627536105 -0.32916155396787211 41.46766649010506 ;
	setAttr ".s" -type "double3" 1.0000000000000009 0.999999999999999 1.0000000000000004 ;
createNode nurbsCurve -n "finger_R2_fk2_ctlShape" -p "finger_R2_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R2_fk2_ctlShape1" -p "finger_R2_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R2_fk2_ctlShape2" -p "finger_R2_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R3_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.6017980506667309 10.166912775559643 -0.64727392425547348 ;
	setAttr ".r" -type "double3" -1.1915084257538773 -0.31834942492451401 44.88629516728129 ;
	setAttr ".s" -type "double3" 1.0000000000000007 0.999999999999999 1 ;
createNode nurbsCurve -n "finger_R3_fk0_ctlShape" -p "finger_R3_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R3_fk0_ctlShape1" -p "finger_R3_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R3_fk0_ctlShape2" -p "finger_R3_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R3_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.7245761419419825 10.044621031820162 -0.64823678087890169 ;
	setAttr ".r" -type "double3" -1.1915084257538773 -0.31834942492451401 44.88629516728129 ;
	setAttr ".s" -type "double3" 1.0000000000000011 0.99999999999999889 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R3_fk1_ctlShape" -p "finger_R3_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R3_fk1_ctlShape1" -p "finger_R3_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R3_fk1_ctlShape2" -p "finger_R3_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "finger_R3_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.8473542332172395 9.9223292880806913 -0.64919963750233078 ;
	setAttr ".r" -type "double3" -1.1915084257538773 -0.31834942492451401 44.88629516728129 ;
	setAttr ".s" -type "double3" 1.0000000000000011 0.999999999999999 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R3_fk2_ctlShape" -p "finger_R3_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "finger_R3_fk2_ctlShape1" -p "finger_R3_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "finger_R3_fk2_ctlShape2" -p "finger_R3_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "thumb_R0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.1391583941787022 10.551403881847703 -0.25936483589254394 ;
	setAttr ".r" -type "double3" 95.894192565964389 60.84194903153832 82.465812349746699 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000007 0.99999999999999956 ;
createNode nurbsCurve -n "thumb_R0_fk0_ctlShape" -p "thumb_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "thumb_R0_fk0_ctlShape1" -p "thumb_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "thumb_R0_fk0_ctlShape2" -p "thumb_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "thumb_R0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.1711913996748518 10.30920587359307 0.17852387169304476 ;
	setAttr ".r" -type "double3" 85.023372584416578 40.95877308675918 64.65434311275888 ;
	setAttr ".s" -type "double3" 1.0000000000000009 1.0000000000000002 0.99999999999999933 ;
createNode nurbsCurve -n "thumb_R0_fk1_ctlShape" -p "thumb_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "thumb_R0_fk1_ctlShape1" -p "thumb_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "thumb_R0_fk1_ctlShape2" -p "thumb_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "thumb_R0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.2868658668396185 10.064998502302396 0.41307973702877498 ;
	setAttr ".r" -type "double3" 90.562491114002853 27.184462148078723 67.461896825210985 ;
	setAttr ".s" -type "double3" 1.0000000000000011 1 0.99999999999999922 ;
createNode nurbsCurve -n "thumb_R0_fk2_ctlShape" -p "thumb_R0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		0.00092940026424227502 0.3043989218767108 -4.8790710457949188e-016
		0.045896110452547483 0.32302474309200735 -4.4872062271919112e-016
		0.06452193166784384 0.3679914532803123 -4.7916580021720449e-016
		0.045896110452547483 0.4129581634686173 -3.497737958506486e-016
		0.00092940026424229985 0.43158398468391379 -4.8790710457949188e-016
		-0.044037309924062865 0.41295816346861736 -6.8467074832879423e-016
		-0.062663131139359132 0.3679914532803123 -4.3045351622038412e-016
		-0.044037309924062851 0.32302474309200735 -5.3853389633833061e-016
		;
createNode nurbsCurve -n "thumb_R0_fk2_ctlShape1" -p "thumb_R0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		-2.4485405031468695e-015 0.42410500051515737 -4.7663592839332091e-016
		;
createNode nurbsCurve -n "thumb_R0_fk2_ctlShape2" -p "thumb_R0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.057383924331838407 0.36672107618332161 -4.3880280485850674e-016
		0.057383924331833522 0.36672107618332161 -4.5706991135731424e-016
		;
createNode transform -n "armUI_R0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "shoulder_R0" -ln "shoulder_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "shoulder_R0_rotRef" -ln "shoulder_R0_rotRef" -nn "Ref" 
		-min 0 -max 3 -en "shoulder_R0_root:local_C0_root:body_C0_root:spine_C0_eff" -at "enum";
	addAttr -ci true -k true -sn "arm_R0" -ln "arm_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "arm_R0_blend" -ln "arm_R0_blend" -nn "Fk/Ik Blend" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_R0_roll" -ln "arm_R0_roll" -nn "Roll" -min -180 
		-max 180 -at "double";
	addAttr -ci true -k true -sn "arm_R0_aproll" -ln "arm_R0_aproll" -nn "Armpit Roll" 
		-min -360 -max 360 -at "double";
	addAttr -ci true -k true -sn "arm_R0_ikscale" -ln "arm_R0_ikscale" -nn "Scale" -dv 
		0.001 -min 0.001 -max 99 -at "double";
	addAttr -ci true -k true -sn "arm_R0_maxstretch" -ln "arm_R0_maxstretch" -nn "Max Stretch" 
		-dv 1 -min 1 -max 99 -at "double";
	addAttr -ci true -k true -sn "arm_R0_slide" -ln "arm_R0_slide" -nn "Slide" -min 
		0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_R0_softness" -ln "arm_R0_softness" -nn "Softness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_R0_reverse" -ln "arm_R0_reverse" -nn "Reverse" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_R0_roundness" -ln "arm_R0_roundness" -nn "Roundness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_R0_volume" -ln "arm_R0_volume" -nn "Volume" -min 
		0 -max 1 -at "double";
	addAttr -ci true -k true -sn "arm_R0_ikref" -ln "arm_R0_ikref" -nn "Ik Ref" -min 
		0 -max 3 -en "local_C0_root:body_C0_root:spine_C0_eff:spine_C0_root" -at "enum";
	addAttr -ci true -k true -sn "arm_R0_upvref" -ln "arm_R0_upvref" -nn "UpV Ref" -min 
		0 -max 3 -en "local_C0_root:body_C0_root:spine_C0_eff:spine_C0_root" -at "enum";
	addAttr -ci true -k true -sn "finger_R0" -ln "finger_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "finger_R1" -ln "finger_R1" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "finger_R2" -ln "finger_R2" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "finger_R3" -ln "finger_R3" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "thumb_R0" -ln "thumb_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".arm_R0_aproll" 90;
	setAttr -k on ".arm_R0_ikscale" 1;
	setAttr -k on ".arm_R0_maxstretch" 1.5;
	setAttr -k on ".arm_R0_slide" 0.5;
	setAttr -k on ".arm_R0_volume" 1;
createNode nurbsCurve -n "armUI_R0_ctlShape" -p "armUI_R0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0.18982291243810895 0.28473436865716306 0
		0.28473436865716306 0.18982291243810895 0
		0.094911456219054474 0 0
		0.28473436865716306 -0.18982291243810895 0
		0.18982291243810895 -0.28473436865716306 0
		0 -0.094911456219054474 0
		-0.18982291243810895 -0.28473436865716306 0
		-0.28473436865716306 -0.18982291243810895 0
		-0.094911456219054474 0 0
		-0.28473436865716306 0.18982291243810895 0
		-0.18982291243810895 0.28473436865716306 0
		0 0.094911456219054474 0
		0.18982291243810895 0.28473436865716306 0
		;
createNode transform -n "leg_L0_root_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.83666277387714916 9.2595813774367706 -0.21240322780572582 ;
createNode nurbsCurve -n "leg_L0_root_ctlShape" -p "leg_L0_root_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		11
		1.5 0.36835137534151341 -5.8348396146179342e-017
		1.7593087299335564 0.25930872993355636 -7.2226183953993799e-017
		1.8683513753415135 -8.3266726846886741e-017 -8.3266726846886716e-017
		1.7593087299335564 -0.25930872993355647 -1.2773733518525163e-016
		1.5 -0.36835137534151352 -1.1385954737743717e-016
		1.2406912700664436 -0.25930872993355647 -1.2773733518525163e-016
		1.1316486246584865 -8.3266726846886741e-017 -8.3266726846886716e-017
		1.2406912700664436 0.25930872993355636 -7.2226183953993799e-017
		1.5 0.36835137534151341 -5.8348396146179342e-017
		1.7593087299335564 0.25930872993355636 -7.2226183953993799e-017
		1.8683513753415135 -8.3266726846886741e-017 -8.3266726846886716e-017
		;
createNode transform -n "leg_L0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.83666277387714916 9.2595813774367706 -0.21240322780572582 ;
	setAttr ".r" -type "double3" 89.999999999999815 0.010207956136975406 -89.999999999999957 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999978 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "leg_L0_fk0_ctlShape" -p "leg_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.2463303594800008 0.80569068601603255 -0.97228587551901746
		1.2463303594800008 1.1372867313766624 2.8307370544442467e-016
		1.2463303594800006 0.80569068601603322 0.97228587551901624
		1.2463303594800006 0.0051470160771187809 1.3750198716627913
		1.2463303594800006 -0.7953966538617957 0.97228587551901646
		1.2463303594800008 -1.1269926992224248 5.4052070163714801e-016
		1.2463303594800008 -0.79539665386179614 -0.97228587551901602
		1.2463303594800008 0.0051470160771178433 -1.3750198716627915
		1.2463303594800008 0.80569068601603255 -0.97228587551901746
		1.2463303594800008 1.1372867313766624 2.8307370544442467e-016
		1.2463303594800006 0.80569068601603322 0.97228587551901624
		;
createNode transform -n "leg_L0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.83666277387715127 5.2702163648508309 -0.21311398283584834 ;
	setAttr ".r" -type "double3" 89.999999999999815 2.0808125755073896 -89.999999999999957 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "leg_L0_fk1_ctlShape" -p "leg_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		3.0339434448456478e-017 0.49548056581832317 -0.49548056581832423
		-4.8951117325516511e-033 0.70071533607256875 1.6158821081948432e-016
		-3.0339434448456442e-017 0.49548056581832373 0.49548056581832373
		-4.290643967173664e-017 2.0304977388025608e-016 0.70071533607256875
		-3.033943444845646e-017 -0.49548056581832345 0.49548056581832384
		-1.2928547640248697e-032 -0.70071533607256897 2.9278417545663892e-016
		3.0339434448456423e-017 -0.49548056581832373 -0.49548056581832317
		4.290643967173664e-017 -3.7635557391413309e-016 -0.70071533607256875
		3.0339434448456478e-017 0.49548056581832317 -0.49548056581832423
		-4.8951117325516511e-033 0.70071533607256875 1.6158821081948432e-016
		-3.0339434448456442e-017 0.49548056581832373 0.49548056581832373
		;
createNode transform -n "leg_L0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.83666277387715249 0.98523424150988159 -0.36880026183914283 ;
	setAttr ".r" -type "double3" 3.1805546814635161e-014 -89.999999999999815 0 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "leg_L0_fk2_ctlShape" -p "leg_L0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.40678097448603279 1.1102230246251565e-016 -0.40678097448603356
		-0.57527517103349246 -5.5511151231257827e-017 6.5632018426984188e-017
		-0.40678097448603295 1.3877787807814457e-017 0.40678097448603301
		-5.9921419699177088e-017 -3.5225444841755613e-017 0.57527517103349235
		0.4067809744860329 -1.1102230246251565e-016 0.40678097448603318
		0.57527517103349246 -5.5511151231257827e-017 1.7334163617071053e-016
		0.40678097448603306 1.3877787807814457e-017 -0.40678097448603273
		4.1576034918909266e-016 3.5225444841755539e-017 -0.57527517103349235
		-0.40678097448603279 1.1102230246251565e-016 -0.40678097448603356
		-0.57527517103349246 -5.5511151231257827e-017 6.5632018426984188e-017
		-0.40678097448603295 1.3877787807814457e-017 0.40678097448603301
		;
createNode transform -n "leg_L0_ikcns_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.8366627738771526 0.98523424150988159 -0.36880026183914283 ;
	setAttr ".invTx" yes;
createNode nurbsCurve -n "leg_L0_ikcns_ctlShape" -p "leg_L0_ikcns_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.51049551405186766 0 0
		-0.51049551405186766 0 0
		;
createNode nurbsCurve -n "leg_L0_ikcns_ctl1Shape" -p "leg_L0_ikcns_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.51049551405186766 0
		0 -0.51049551405186766 0
		;
createNode nurbsCurve -n "leg_L0_ikcns_ctl2Shape" -p "leg_L0_ikcns_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.51049551405186766
		0 0 -0.51049551405186766
		;
createNode transform -n "leg_L0_ik_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.8366627738771526 0.98523424150988159 -0.36880026183914283 ;
	setAttr ".ro" 3;
	setAttr ".invTx" yes;
	setAttr ".invRy" yes;
	setAttr ".invRz" yes;
createNode nurbsCurve -n "leg_L0_ik_ctlShape" -p "leg_L0_ik_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.59754091709378965 -0.9974960225469085 -0.83731600105947124
		2.066352013092537e-016 -0.26538705442912447 -1.0936615871299982
		-0.69155560304189323 -0.9974960225469085 -0.88882263398257533
		-0.53070536446845373 -0.9974960225469085 -3.2112695072372299e-016
		-0.6462987904112919 -0.9974960225469085 2.1703668801365321
		-8.5362897150498471e-019 -0.67328495465734184 2.7606728868371655
		0.96624184826016768 -0.9974960225469085 1.9096823044179536
		0.63304175377859195 -0.9974960225469085 5.9521325992805852e-016
		0.59754091709378965 -0.9974960225469085 -0.83731600105947124
		2.066352013092537e-016 -0.26538705442912447 -1.0936615871299982
		-0.69155560304189323 -0.9974960225469085 -0.88882263398257533
		;
createNode transform -n "leg_L0_upv_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.83666277387716403 5.1898215767905009 4.0402555803510189 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".invTx" yes;
createNode nurbsCurve -n "leg_L0_upv_ctlShape" -p "leg_L0_upv_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		-0.12027045666498232 0.36081136999494701 0.94425699999999324
		-0.12027045666498232 0.12027045666498232 0.94425699999999324
		-0.36081136999494701 0.12027045666498232 0.94425699999999324
		-0.36081136999494701 -0.12027045666498232 0.94425699999999324
		-0.12027045666498232 -0.12027045666498232 0.94425699999999324
		-0.12027045666498232 -0.36081136999494701 0.94425699999999324
		0.12027045666498232 -0.36081136999494701 0.94425699999999324
		0.12027045666498232 -0.12027045666498232 0.94425699999999324
		0.36081136999494701 -0.12027045666498232 0.94425699999999324
		0.36081136999494701 0.12027045666498232 0.94425699999999324
		0.12027045666498232 0.12027045666498232 0.94425699999999324
		0.12027045666498232 0.36081136999494701 0.94425699999999324
		-0.12027045666498232 0.36081136999494701 0.94425699999999324
		;
createNode transform -n "leg_L0_mid_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.83666277387715104 5.2702163402312454 -0.21311588945501317 ;
	setAttr ".r" -type "double3" 89.999999999999815 1.0455111302958222 -89.999999999999986 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "leg_L0_mid_ctlShape" -p "leg_L0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.15448156283425502 -1.7610178978175248 1.078664017785038e-016
		-0.21846992129679035 -1.6065363349832213 7.3171932258967653e-017
		-0.15448156283425138 -1.4520547721488353 1.2868308349022548e-016
		7.80939110457505e-016 -1.3880664136865195 1.2439972695382107e-016
		0.15448156283424724 -1.4520547721488994 1.078664017785038e-016
		0.21846992129679316 -1.606536334983216 1.0092750787459657e-016
		0.15448156283425002 -1.7610178978175102 1.2174418958631825e-016
		5.665920418808194e-015 -1.8250062562799441 9.7644877971212064e-017
		-0.15448156283425502 -1.7610178978175248 1.078664017785038e-016
		-0.21846992129679035 -1.6065363349832213 7.3171932258967653e-017
		-0.15448156283425138 -1.4520547721488353 1.2868308349022548e-016
		;
createNode nurbsCurve -n "leg_L0_mid_ctlShape1" -p "leg_L0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		-0.053066285894171054 -1.5534700490890403 1.1307057220643422e-016
		-0.053066285894170749 -1.4473374773006988 1.1654001915838783e-016
		0.053066285894170347 -1.4473374773006991 1.1133584873045741e-016
		0.053066285894170291 -1.5534700490890401 1.1827474263436464e-016
		0.15919885768251074 -1.5534700490890407 1.0092750787459657e-016
		0.15919885768251069 -1.6596026208773818 1.0092750787459657e-016
		0.053066285894169674 -1.6596026208773811 1.1480529568241102e-016
		0.053066285894169569 -1.7657351926657201 1.1480529568241102e-016
		-0.053066285894170652 -1.7657351926657203 1.0439695482655018e-016
		-0.053066285894170652 -1.6596026208773811 1.0439695482655018e-016
		-0.15919885768251163 -1.6596026208773811 1.1480529568241102e-016
		-0.15919885768251196 -1.5534700490890403 7.3171932258967653e-017
		-0.053066285894171054 -1.5534700490890403 1.1307057220643422e-016
		;
createNode nurbsCurve -n "leg_L0_mid_ctlShape2" -p "leg_L0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		-0.18414625387329245 -1.5359963128545231 7.3171932258967653e-017
		-3.6977854932234928e-032 -1.110223024625157e-016 1.1102230246251564e-016
		0.18600257024509875 -1.5434215783417526 1.078664017785038e-016
		;
createNode transform -n "foot_L0_heel_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 0 2.2204460492503131e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -90 -89.055637214679848 90 ;
	setAttr -k on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_L0_heel_ctlShape" -p "foot_L0_heel_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.6613381477509392e-016 0.10248560095429093 -0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 -0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 -0.73446618905870076
		2.2204460492503131e-016 5.6519166520644008e-017 -0.69201526319547679
		1.1102230246251565e-016 -0.10248560095429093 -0.73446618905870076
		1.6911482431301163e-016 -0.1449365268175149 -0.83695179001299169
		-1.1102230246251565e-016 -0.10248560095429093 -0.93943739096728152
		2.2204460492503131e-016 -6.3325504211831524e-017 -0.9818883168305057
		6.6613381477509392e-016 0.10248560095429093 -0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 -0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 -0.73446618905870076
		;
createNode nurbsCurve -n "foot_L0_heel_ctlShape1" -p "foot_L0_heel_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		1.98143424909718e-016 0 -0.96659630343203529
		;
createNode nurbsCurve -n "foot_L0_heel_ctlShape2" -p "foot_L0_heel_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		1.1102230246251565e-016 0.13078621819644001 -0.83581008523559541
		1.1102230246251565e-016 -0.13078621819644001 -0.83581008523559541
		;
createNode transform -n "foot_L0_tip_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.0070922738054122 -0.033084372433994577 -3.3306690738754696e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -90 -89.055637214679848 90 ;
	setAttr -k on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_L0_tip_ctlShape" -p "foot_L0_tip_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr -av ".cp[2].xv";
	setAttr -av ".cp[2].yv";
	setAttr -av ".cp[2].zv";
	setAttr -av ".cp[3].xv";
	setAttr -av ".cp[3].yv";
	setAttr -av ".cp[3].zv";
	setAttr -av ".cp[4].xv";
	setAttr -av ".cp[4].yv";
	setAttr -av ".cp[4].zv";
	setAttr -av ".cp[5].xv";
	setAttr -av ".cp[5].yv";
	setAttr -av ".cp[5].zv";
	setAttr -av ".cp[6].xv";
	setAttr -av ".cp[6].yv";
	setAttr -av ".cp[6].zv";
	setAttr -av ".cp[7].xv";
	setAttr -av ".cp[7].yv";
	setAttr -av ".cp[7].zv";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.26806985473777034 1.0252826352171598 -0.26806985473776956
		-4.3035077968938024e-017 1.0252826352171598 -0.37910802423354006
		-0.26806985473776956 1.0252826352171598 -0.26806985473776956
		-0.37910802423354006 1.0252826352171598 -1.103782881440497e-016
		-0.26806985473777023 1.0252826352171598 0.26806985473776956
		-1.1401603183214001e-016 1.0252826352171598 0.37910802423354051
		0.26806985473776956 1.0252826352171598 0.2680698547377699
		0.37910802423354006 1.0252826352171598 2.0309739209984588e-016
		0.26806985473777034 1.0252826352171598 -0.26806985473776956
		-4.3035077968938024e-017 1.0252826352171598 -0.37910802423354006
		-0.26806985473776956 1.0252826352171598 -0.26806985473776956
		;
createNode nurbsCurve -n "foot_L0_tip_ctlShape1" -p "foot_L0_tip_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.34209530106828362 1.0262859032048515 0
		0.34209530106828362 1.0262859032048515 0
		;
createNode nurbsCurve -n "foot_L0_tip_ctlShape2" -p "foot_L0_tip_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 1.0262859032048515 0.34209530106828362
		0 1.0262859032048515 -0.34209530106828362
		;
createNode nurbsCurve -n "foot_L0_tip_ctlShape3" -p "foot_L0_tip_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 1.0262859032048515 0
		;
createNode transform -n "foot_L0_roll_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -5.5511151231257827e-017 0 1.1102230246251565e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 2.8249000307521022e-030 0 ;
	setAttr -l on -k off ".ry";
	setAttr -l on ".ro";
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_L0_roll_ctlShape" -p "foot_L0_roll_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.26078403078628681 0.62958834397457708 -0.8657551247211025
		1.846562374842261e-016 0.32880383226232718 -0.8657551247211025
		-0.26078403078628754 0.62958834397457708 -0.8657551247211025
		-0.23249941947281577 0.23249941947281583 -0.8657551247211025
		-0.62958834397457697 0.26078403078628748 -0.8657551247211025
		-0.32880383226232723 -1.7800142535511012e-016 -0.8657551247211025
		-0.62958834397457686 -0.26078403078628754 -0.8657551247211025
		-0.23249941947281577 -0.23249941947281599 -0.8657551247211025
		-0.26078403078628737 -0.62958834397457719 -0.8657551247211025
		1.3155565246656949e-016 -0.32880383226232718 -0.8657551247211025
		0.26078403078628781 -0.62958834397457708 -0.8657551247211025
		0.2324994194728161 -0.23249941947281583 -0.8657551247211025
		0.62958834397457741 -0.26078403078628726 -0.8657551247211025
		0.32880383226232712 -1.0634764202533316e-016 -0.8657551247211025
		0.62958834397457708 0.26078403078628781 -0.8657551247211025
		0.2324994194728161 0.23249941947281583 -0.8657551247211025
		0.26078403078628681 0.62958834397457708 -0.8657551247211025
		1.846562374842261e-016 0.32880383226232718 -0.8657551247211025
		-0.26078403078628754 0.62958834397457708 -0.8657551247211025
		;
createNode transform -n "foot_L0_bk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 3.4694469519536142e-018 1.1102230246251565e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -90 -89.055637214679848 90 ;
	setAttr -k on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_L0_bk0_ctlShape" -p "foot_L0_bk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.6613381477509392e-016 0.10248560095429093 -0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 -0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 -0.73446618905870076
		2.2204460492503131e-016 5.6519166520644008e-017 -0.69201526319547679
		1.1102230246251565e-016 -0.10248560095429093 -0.73446618905870076
		1.6911482431301163e-016 -0.1449365268175149 -0.83695179001299169
		-1.1102230246251565e-016 -0.10248560095429093 -0.93943739096728152
		2.2204460492503131e-016 -6.3325504211831524e-017 -0.9818883168305057
		6.6613381477509392e-016 0.10248560095429093 -0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 -0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 -0.73446618905870076
		;
createNode nurbsCurve -n "foot_L0_bk0_ctlShape1" -p "foot_L0_bk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		1.98143424909718e-016 0 -0.96659630343203529
		;
createNode nurbsCurve -n "foot_L0_bk0_ctlShape2" -p "foot_L0_bk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		1.1102230246251565e-016 0.13078621819644001 -0.83581008523559541
		1.1102230246251565e-016 -0.13078621819644001 -0.83581008523559541
		;
createNode transform -n "foot_L0_bk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -5.5511151231257827e-017 -1.1102230246251565e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -90 -10.00764104901584 ;
	setAttr -k on ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_L0_bk1_ctlShape" -p "foot_L0_bk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.6613381477509392e-016 0.10248560095429093 -0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 -0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 -0.73446618905870076
		2.2204460492503131e-016 5.6519166520644008e-017 -0.69201526319547679
		1.1102230246251565e-016 -0.10248560095429093 -0.73446618905870076
		1.6911482431301163e-016 -0.1449365268175149 -0.83695179001299169
		-1.1102230246251565e-016 -0.10248560095429093 -0.93943739096728152
		2.2204460492503131e-016 -6.3325504211831524e-017 -0.9818883168305057
		6.6613381477509392e-016 0.10248560095429093 -0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 -0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 -0.73446618905870076
		;
createNode nurbsCurve -n "foot_L0_bk1_ctlShape1" -p "foot_L0_bk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		1.98143424909718e-016 0 -0.96659630343203529
		;
createNode nurbsCurve -n "foot_L0_bk1_ctlShape2" -p "foot_L0_bk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		1.1102230246251565e-016 0.13078621819644001 -0.83581008523559541
		1.1102230246251565e-016 -0.13078621819644001 -0.83581008523559541
		;
createNode transform -n "foot_L0_bk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.6653345369377348e-016 1.1102230246251565e-016 1.1102230246251565e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -90 -14.767527951520169 ;
	setAttr -k on ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_L0_bk2_ctlShape" -p "foot_L0_bk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.6613381477509392e-016 0.10248560095429093 -0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 -0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 -0.73446618905870076
		2.2204460492503131e-016 5.6519166520644008e-017 -0.69201526319547679
		1.1102230246251565e-016 -0.10248560095429093 -0.73446618905870076
		1.6911482431301163e-016 -0.1449365268175149 -0.83695179001299169
		-1.1102230246251565e-016 -0.10248560095429093 -0.93943739096728152
		2.2204460492503131e-016 -6.3325504211831524e-017 -0.9818883168305057
		6.6613381477509392e-016 0.10248560095429093 -0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 -0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 -0.73446618905870076
		;
createNode nurbsCurve -n "foot_L0_bk2_ctlShape1" -p "foot_L0_bk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		1.98143424909718e-016 0 -0.96659630343203529
		;
createNode nurbsCurve -n "foot_L0_bk2_ctlShape2" -p "foot_L0_bk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		1.1102230246251565e-016 0.13078621819644001 -0.83581008523559541
		1.1102230246251565e-016 -0.13078621819644001 -0.83581008523559541
		;
createNode transform -n "foot_L0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.83666277387714927 0.30431196584359432 0.55042018545206306 ;
	setAttr ".r" -type "double3" 90.000000000000881 -75.232472048479835 -90.000000000000909 ;
	setAttr -k on ".ro";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode nurbsCurve -n "foot_L0_fk0_ctlShape" -p "foot_L0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 0 no 3
		13 0 0 0 1 2 3 4 5 6 7 8 8 8
		11
		-0.010688074753943066 0.002794223148687909 -0.747701542084941
		-0.010688074753943066 0.08393624424597998 -0.747701542084941
		-0.01013156670511739 0.24799658211594267 -0.70877012217336033
		-0.0077543654438998466 0.4553651335764215 -0.54246916622225094
		-0.004196632337346573 0.59420866774734948 -0.29358219720904127
		-5.5071253258009846e-017 0.64290762783983535 3.0418165026757096e-017
		0.004196632337346573 0.59420866774734948 0.29358219720904116
		0.0077543654439000687 0.4553651335764215 0.54246916622225128
		0.010131566705117168 0.24799658211594267 0.70877012217336033
		0.010688074753943066 0.083936244245980035 0.747701542084941
		0.010688074753943066 0.002794223148687909 0.747701542084941
		;
createNode transform -n "foot_L0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.83666277387714716 0.13927624906416441 1.1764917847694865 ;
	setAttr ".r" -type "double3" 90.000000000001279 -79.992358737188624 -90.000000000001322 ;
	setAttr -k on ".ro";
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999978 ;
createNode nurbsCurve -n "foot_L0_fk1_ctlShape" -p "foot_L0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 0 no 3
		13 0 0 0 1 2 3 4 5 6 7 8 8 8
		11
		-0.010688074753943066 0.002794223148687909 -0.747701542084941
		-0.010688074753943066 0.08393624424597998 -0.747701542084941
		-0.01013156670511739 0.24799658211594267 -0.70877012217336033
		-0.0077543654438998466 0.4553651335764215 -0.54246916622225094
		-0.004196632337346573 0.59420866774734948 -0.29358219720904127
		-5.5071253258009846e-017 0.64290762783983535 3.0418165026757096e-017
		0.004196632337346573 0.59420866774734948 0.29358219720904116
		0.0077543654439000687 0.4553651335764215 0.54246916622225128
		0.010131566705117168 0.24799658211594267 0.70877012217336033
		0.010688074753943066 0.083936244245980035 0.747701542084941
		0.010688074753943066 0.002794223148687909 0.747701542084941
		;
createNode transform -n "legUI_L0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "leg_L0" -ln "leg_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "leg_L0_blend" -ln "leg_L0_blend" -nn "Fk/Ik Blend" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_L0_roll" -ln "leg_L0_roll" -nn "Roll" -min -180 
		-max 180 -at "double";
	addAttr -ci true -k true -sn "leg_L0_ikscale" -ln "leg_L0_ikscale" -nn "Scale" -dv 
		0.001 -min 0.001 -max 99 -at "double";
	addAttr -ci true -k true -sn "leg_L0_maxstretch" -ln "leg_L0_maxstretch" -nn "Max Stretch" 
		-dv 1 -min 1 -max 99 -at "double";
	addAttr -ci true -k true -sn "leg_L0_slide" -ln "leg_L0_slide" -nn "Slide" -min 
		0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_L0_softness" -ln "leg_L0_softness" -nn "Softness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_L0_reverse" -ln "leg_L0_reverse" -nn "Reverse" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_L0_roundness" -ln "leg_L0_roundness" -nn "Roundness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_L0_volume" -ln "leg_L0_volume" -nn "Volume" -min 
		0 -max 1 -at "double";
	addAttr -ci true -k true -sn "foot_L0" -ln "foot_L0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "foot_L0_angle_0" -ln "foot_L0_angle_0" -nn "Angle 0" 
		-at "double";
	addAttr -ci true -k true -sn "foot_L0_angle_1" -ln "foot_L0_angle_1" -nn "Angle 1" 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".leg_L0_blend" 1;
	setAttr -k on ".leg_L0_ikscale" 1;
	setAttr -k on ".leg_L0_maxstretch" 1.5;
	setAttr -k on ".leg_L0_slide" 0.5;
	setAttr -k on ".leg_L0_volume" 1;
	setAttr -k on ".foot_L0_angle_0" -20;
	setAttr -k on ".foot_L0_angle_1" -20;
createNode nurbsCurve -n "legUI_L0_ctlShape" -p "legUI_L0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 26;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0.15996622010953834 0.23994933016430778 0
		0.23994933016430778 0.15996622010953834 0
		0.079983110054769171 0 0
		0.23994933016430778 -0.15996622010953834 0
		0.15996622010953834 -0.23994933016430778 0
		0 -0.079983110054769171 0
		-0.15996622010953834 -0.23994933016430778 0
		-0.23994933016430778 -0.15996622010953834 0
		-0.079983110054769171 0 0
		-0.23994933016430778 0.15996622010953834 0
		-0.15996622010953834 0.23994933016430778 0
		0 0.079983110054769171 0
		0.15996622010953834 0.23994933016430778 0
		;
createNode transform -n "hoodySpring_C1_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.033704944915076342 10.385933161263839 -0.12562818605943341 ;
	setAttr ".r" -type "double3" -90.000000000000213 83.129566026280088 90 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode nurbsCurve -n "hoodySpring_C1_fk0_ctlShape" -p "hoodySpring_C1_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.8932829534263775 0.14091632277031407 0.15211703516035996
		1.8932829534263775 0.14091632277031407 -0.15211703516035996
		-8.9191467583520508e-017 0.14091632277031407 -0.15211703516035996
		-8.9191467583520508e-017 -0.14091632277031407 -0.15211703516035996
		-8.9191467583520508e-017 -0.14091632277031407 0.15211703516035996
		-8.9191467583520508e-017 0.14091632277031407 0.15211703516035996
		-8.9191467583520508e-017 0.14091632277031407 -0.15211703516035996
		-8.9191467583520508e-017 0.14091632277031407 0.15211703516035996
		1.8932829534263775 0.14091632277031407 0.15211703516035996
		1.8932829534263775 -0.14091632277031407 0.15211703516035996
		-8.9191467583520508e-017 -0.14091632277031407 0.15211703516035996
		1.8932829534263775 -0.14091632277031407 0.15211703516035996
		1.8932829534263775 -0.14091632277031407 -0.15211703516035996
		1.8932829534263775 0.14091632277031407 -0.15211703516035996
		1.8932829534263775 -0.14091632277031407 -0.15211703516035996
		-8.9191467583520508e-017 -0.14091632277031407 -0.15211703516035996
		;
createNode transform -n "hoodySpring_C0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.033704944915076696 10.385933161263836 0.61545434476593097 ;
	setAttr ".r" -type "double3" 61.891685605129993 -87.185640329974092 118.0795878916762 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode nurbsCurve -n "hoodySpring_C0_fk0_ctlShape" -p "hoodySpring_C0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.3448796059148775 0.10535595076907452 0.11373015242254544
		1.3448796059148775 0.10535595076907452 -0.11373015242254544
		-0.07063154078343839 0.10535595076907452 -0.11373015242254544
		-0.07063154078343839 -0.10535595076907407 -0.11373015242254544
		-0.07063154078343839 -0.10535595076907407 0.11373015242254544
		-0.07063154078343839 0.10535595076907452 0.11373015242254544
		-0.07063154078343839 0.10535595076907452 -0.11373015242254544
		-0.07063154078343839 0.10535595076907452 0.11373015242254544
		1.3448796059148775 0.10535595076907452 0.11373015242254544
		1.3448796059148775 -0.10535595076907407 0.11373015242254544
		-0.07063154078343839 -0.10535595076907407 0.11373015242254544
		1.3448796059148775 -0.10535595076907407 0.11373015242254544
		1.3448796059148775 -0.10535595076907407 -0.11373015242254544
		1.3448796059148775 0.10535595076907452 -0.11373015242254544
		1.3448796059148775 -0.10535595076907407 -0.11373015242254544
		-0.07063154078343839 -0.10535595076907407 -0.11373015242254544
		;
createNode transform -n "leg_R0_root_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.83666277387714916 9.2595813774367706 -0.21240322780572593 ;
createNode nurbsCurve -n "leg_R0_root_ctlShape" -p "leg_R0_root_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		11
		-1.5 0.36835137534151352 -2.6809827310655074e-017
		-1.2406912700664434 0.25930872993355647 -1.2932039502840617e-017
		-1.1316486246584865 2.775557561562892e-017 2.775557561562891e-017
		-1.2406912700664434 -0.25930872993355647 1.4823536112788296e-017
		-1.5 -0.36835137534151352 2.8701323920602756e-017
		-1.7593087299335566 -0.25930872993355647 1.4823536112788296e-017
		-1.8683513753415135 2.775557561562892e-017 2.775557561562891e-017
		-1.7593087299335566 0.25930872993355647 -1.2932039502840617e-017
		-1.5 0.36835137534151352 -2.6809827310655074e-017
		-1.2406912700664434 0.25930872993355647 -1.2932039502840617e-017
		-1.1316486246584865 2.775557561562892e-017 2.775557561562891e-017
		;
createNode transform -n "leg_R0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.83666277387714916 9.2595813774367688 -0.21240322780572596 ;
	setAttr ".r" -type "double3" -90.000000000000185 -0.010207956136969041 89.999999999999943 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999967 0.99999999999999978 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "leg_R0_fk0_ctlShape" -p "leg_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.246 0.80054366993891435 -0.97228587551901746
		-1.246 1.1321397152995429 4.2058714151798376e-016
		-1.246 0.8005436699389149 0.97228587551901624
		-1.246 1.0366028880717231e-017 1.3750198716627913
		-1.246 -0.80054366993891468 0.97228587551901646
		-1.246 -1.1321397152995445 6.7803413771070617e-016
		-1.246 -0.80054366993891501 -0.97228587551901602
		-1.246 -9.257741863050967e-016 -1.3750198716627915
		-1.246 0.80054366993891435 -0.97228587551901746
		-1.246 1.1321397152995429 4.2058714151798376e-016
		-1.246 0.8005436699389149 0.97228587551901624
		;
createNode transform -n "leg_R0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.83666277387715104 5.2702163648508282 -0.21311398283584854 ;
	setAttr ".r" -type "double3" -90.000000000000185 -2.080812575507399 89.999999999999957 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999989 0.99999999999999978 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "leg_R0_fk1_ctlShape" -p "leg_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		3.0339434448456478e-017 0.49548056581832317 -0.49548056581832423
		-4.8951117325516511e-033 0.70071533607256875 1.6158821081948432e-016
		-3.0339434448456442e-017 0.49548056581832373 0.49548056581832373
		-4.290643967173664e-017 2.0304977388025608e-016 0.70071533607256875
		-3.033943444845646e-017 -0.49548056581832345 0.49548056581832384
		-1.2928547640248697e-032 -0.70071533607256897 2.9278417545663892e-016
		3.0339434448456423e-017 -0.49548056581832373 -0.49548056581832317
		4.290643967173664e-017 -3.7635557391413309e-016 -0.70071533607256875
		3.0339434448456478e-017 0.49548056581832317 -0.49548056581832423
		-4.8951117325516511e-033 0.70071533607256875 1.6158821081948432e-016
		-3.0339434448456442e-017 0.49548056581832373 0.49548056581832373
		;
createNode transform -n "leg_R0_fk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.83666277387715249 0.98523424150988159 -0.36880026183914294 ;
	setAttr ".r" -type "double3" -179.99999999999997 89.999999999999815 0 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "leg_R0_fk2_ctlShape" -p "leg_R0_fk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.40678097448603279 1.1102230246251565e-016 -0.40678097448603356
		-0.57527517103349246 -5.5511151231257827e-017 6.5632018426984188e-017
		-0.40678097448603295 1.3877787807814457e-017 0.40678097448603301
		-5.9921419699177088e-017 -3.5225444841755613e-017 0.57527517103349235
		0.4067809744860329 -1.1102230246251565e-016 0.40678097448603318
		0.57527517103349246 -5.5511151231257827e-017 1.7334163617071053e-016
		0.40678097448603306 1.3877787807814457e-017 -0.40678097448603273
		4.1576034918909266e-016 3.5225444841755539e-017 -0.57527517103349235
		-0.40678097448603279 1.1102230246251565e-016 -0.40678097448603356
		-0.57527517103349246 -5.5511151231257827e-017 6.5632018426984188e-017
		-0.40678097448603295 1.3877787807814457e-017 0.40678097448603301
		;
createNode transform -n "leg_R0_ikcns_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.83666277387715249 0.98523424150988159 -0.36880026183914294 ;
	setAttr ".invTx" yes;
createNode nurbsCurve -n "leg_R0_ikcns_ctlShape" -p "leg_R0_ikcns_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.51049551405186766 0 0
		-0.51049551405186766 0 0
		;
createNode nurbsCurve -n "leg_R0_ikcns_ctl1Shape" -p "leg_R0_ikcns_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.51049551405186766 0
		0 -0.51049551405186766 0
		;
createNode nurbsCurve -n "leg_R0_ikcns_ctl2Shape" -p "leg_R0_ikcns_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.51049551405186766
		0 0 -0.51049551405186766
		;
createNode transform -n "leg_R0_ik_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.83666277387715249 0.98523424150988159 -0.36880026183914294 ;
	setAttr ".ro" 3;
	setAttr ".invTx" yes;
	setAttr ".invRy" yes;
	setAttr ".invRz" yes;
createNode nurbsCurve -n "leg_R0_ik_ctlShape" -p "leg_R0_ik_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.60079338105270397 -0.9974960225469085 -0.83731600105947124
		-0.0023400164574747598 -0.26538705442912447 -1.0936615871299982
		0.69027159490794987 -0.9974960225469085 -0.88882263398257533
		0.52917573734157808 -0.9974960225469085 -3.2112695072372299e-016
		0.64494567493352206 -0.9974960225469085 2.1703668801365321
		-0.0023400164574745538 -0.67328495465734184 2.7606728868371655
		-0.97517024986435474 -0.9974960225469085 1.9096823044179536
		-0.63634842766490285 -0.9974960225469085 5.9521325992805852e-016
		-0.60079338105270397 -0.9974960225469085 -0.83731600105947124
		-0.0023400164574747598 -0.26538705442912447 -1.0936615871299982
		0.69027159490794987 -0.9974960225469085 -0.88882263398257533
		;
createNode transform -n "leg_R0_upv_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.83666277387716714 5.1898215767905 4.0402555803510189 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".invTx" yes;
createNode nurbsCurve -n "leg_R0_upv_ctlShape" -p "leg_R0_upv_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		-0.12027045666498232 0.36081136999494701 0.94425699999999324
		-0.12027045666498232 0.12027045666498232 0.94425699999999324
		-0.36081136999494701 0.12027045666498232 0.94425699999999324
		-0.36081136999494701 -0.12027045666498232 0.94425699999999324
		-0.12027045666498232 -0.12027045666498232 0.94425699999999324
		-0.12027045666498232 -0.36081136999494701 0.94425699999999324
		0.12027045666498232 -0.36081136999494701 0.94425699999999324
		0.12027045666498232 -0.12027045666498232 0.94425699999999324
		0.36081136999494701 -0.12027045666498232 0.94425699999999324
		0.36081136999494701 0.12027045666498232 0.94425699999999324
		0.12027045666498232 0.12027045666498232 0.94425699999999324
		0.12027045666498232 0.36081136999494701 0.94425699999999324
		-0.12027045666498232 0.36081136999494701 0.94425699999999324
		;
createNode transform -n "leg_R0_mid_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.83666277387715104 5.2702163402312454 -0.21311588945501331 ;
	setAttr ".r" -type "double3" -90.000000000000185 -1.045511130295816 89.999999999999972 ;
	setAttr ".invTx" yes;
	setAttr ".invTy" yes;
	setAttr ".invTz" yes;
createNode nurbsCurve -n "leg_R0_mid_ctlShape" -p "leg_R0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.15448156283425341 1.761017897817525 -8.8728442806161126e-016
		0.21846992129678883 1.6065363349832218 -8.9422332196551849e-016
		0.1544815628342501 1.4520547721488359 -8.7340664025379681e-016
		-2.2231399260917288e-015 1.38806641368652 -8.7480099520882011e-016
		-0.15448156283424874 1.4520547721488994 -8.8034553415770404e-016
		-0.21846992129679446 1.606536334983216 -8.8034553415770404e-016
		-0.15448156283425099 1.7610178978175102 -9.0116221586942572e-016
		-7.1636323856736773e-015 1.8250062562799447 -9.0155584419142976e-016
		0.15448156283425341 1.761017897817525 -8.8728442806161126e-016
		0.21846992129678883 1.6065363349832218 -8.9422332196551849e-016
		0.1544815628342501 1.4520547721488359 -8.7340664025379681e-016
		;
createNode nurbsCurve -n "leg_R0_mid_ctlShape1" -p "leg_R0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0.053066285894169528 1.5534700490890403 -8.9075387501356488e-016
		0.053066285894169528 1.4473374773006993 -8.9075387501356488e-016
		-0.053066285894171485 1.4473374773006991 -8.8381498110965765e-016
		-0.053066285894171436 1.5534700490890405 -8.9075387501356488e-016
		-0.15919885768251213 1.5534700490890407 -9.0810110977333295e-016
		-0.15919885768251235 1.6596026208773815 -9.0810110977333295e-016
		-0.053066285894171082 1.6596026208773818 -8.9422332196551849e-016
		-0.053066285894171526 1.7657351926657203 -8.9248859848954169e-016
		0.053066285894169306 1.7657351926657205 -8.9075387501356488e-016
		0.053066285894169243 1.6596026208773813 -8.8208025763368084e-016
		0.1591988576825101 1.6596026208773813 -9.2197889758114741e-016
		0.15919885768251046 1.5534700490890403 -8.8728442806161126e-016
		0.053066285894169528 1.5534700490890403 -8.9075387501356488e-016
		;
createNode nurbsCurve -n "leg_R0_mid_ctlShape2" -p "leg_R0_mid_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		0.1841462538732912 1.5359963128545231 -8.8728442806161126e-016
		-8.9858676055598607e-016 1.0408340855860839e-017 -8.8817841970012504e-016
		-0.18600257024510045 1.5434215783417533 -9.0810110977333295e-016
		;
createNode transform -n "foot_R0_heel_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -3.4694469519536142e-018 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 90.000000000001549 89.055637214679848 -90 ;
	setAttr -k on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_R0_heel_ctlShape" -p "foot_R0_heel_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.6613381477509392e-016 0.10248560095429093 0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 0.73446618905870076
		2.2204460492503131e-016 5.6519166520644008e-017 0.69201526319547679
		1.1102230246251565e-016 -0.10248560095429093 0.73446618905870076
		1.6911482431301163e-016 -0.1449365268175149 0.83695179001299169
		-1.1102230246251565e-016 -0.10248560095429093 0.93943739096728152
		2.2204460492503131e-016 -6.3325504211831524e-017 0.9818883168305057
		6.6613381477509392e-016 0.10248560095429093 0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 0.73446618905870076
		;
createNode nurbsCurve -n "foot_R0_heel_ctlShape1" -p "foot_R0_heel_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		1.98143424909718e-016 0 0.96659630343203529
		;
createNode nurbsCurve -n "foot_R0_heel_ctlShape2" -p "foot_R0_heel_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		1.1102230246251565e-016 0.13078621819644001 0.83581008523559541
		1.1102230246251565e-016 -0.13078621819644001 0.83581008523559541
		;
createNode transform -n "foot_R0_tip_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.0070922738054118 0.033084372433994834 1.1102230246251565e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 90.000000000001549 89.055637214679848 -90 ;
	setAttr -k on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_R0_tip_ctlShape" -p "foot_R0_tip_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr -av ".cp[2].xv";
	setAttr -av ".cp[2].yv";
	setAttr -av ".cp[2].zv";
	setAttr -av ".cp[3].xv";
	setAttr -av ".cp[3].yv";
	setAttr -av ".cp[3].zv";
	setAttr -av ".cp[4].xv";
	setAttr -av ".cp[4].yv";
	setAttr -av ".cp[4].zv";
	setAttr -av ".cp[5].xv";
	setAttr -av ".cp[5].yv";
	setAttr -av ".cp[5].zv";
	setAttr -av ".cp[6].xv";
	setAttr -av ".cp[6].yv";
	setAttr -av ".cp[6].zv";
	setAttr -av ".cp[7].xv";
	setAttr -av ".cp[7].yv";
	setAttr -av ".cp[7].zv";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.26806985473777034 -1.0252826352171598 -0.26806985473776956
		-4.3035077968938024e-017 -1.0252826352171598 -0.37910802423354006
		-0.26806985473776956 -1.0252826352171598 -0.26806985473776956
		-0.37910802423354006 -1.0252826352171598 -1.103782881440497e-016
		-0.26806985473777023 -1.0252826352171598 0.26806985473776956
		-1.1401603183214001e-016 -1.0252826352171598 0.37910802423354051
		0.26806985473776956 -1.0252826352171598 0.2680698547377699
		0.37910802423354006 -1.0252826352171598 2.0309739209984588e-016
		0.26806985473777034 -1.0252826352171598 -0.26806985473776956
		-4.3035077968938024e-017 -1.0252826352171598 -0.37910802423354006
		-0.26806985473776956 -1.0252826352171598 -0.26806985473776956
		;
createNode nurbsCurve -n "foot_R0_tip_ctlShape1" -p "foot_R0_tip_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		-0.34209530106828362 -1.0262859032048515 0
		0.34209530106828362 -1.0262859032048515 0
		;
createNode nurbsCurve -n "foot_R0_tip_ctlShape2" -p "foot_R0_tip_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr -av ".cp[0].xv";
	setAttr -av ".cp[0].yv";
	setAttr -av ".cp[0].zv";
	setAttr -av ".cp[1].xv";
	setAttr -av ".cp[1].yv";
	setAttr -av ".cp[1].zv";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 -1.0262859032048515 0.34209530106828362
		0 -1.0262859032048515 -0.34209530106828362
		;
createNode nurbsCurve -n "foot_R0_tip_ctlShape3" -p "foot_R0_tip_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 -1.0262859032048515 0
		;
createNode transform -n "foot_R0_roll_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 1.1102230246251565e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -180 0 0 ;
	setAttr -l on -k off ".ry";
	setAttr -l on ".ro";
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_R0_roll_ctlShape" -p "foot_R0_roll_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.26078403078628665 0.62958834397457708 0.89242875242165043
		-1.1679166883285452e-016 0.32880383226232723 0.89242875242165043
		-0.26078403078628787 0.62958834397457708 0.89242875242165043
		-0.2324994194728163 0.23249941947281585 0.89242875242165043
		-0.62958834397457719 0.26078403078628754 0.89242875242165043
		-0.32880383226232757 -1.2665767319153388e-016 0.89242875242165043
		-0.62958834397457708 -0.26078403078628748 0.89242875242165043
		-0.2324994194728163 -0.23249941947281597 0.89242875242165043
		-0.26078403078628765 -0.62958834397457708 0.89242875242165043
		-1.6989225385051123e-016 -0.32880383226232718 0.89242875242165043
		0.26078403078628748 -0.62958834397457697 0.89242875242165043
		0.23249941947281566 -0.2324994194728158 0.89242875242165043
		0.62958834397457708 -0.26078403078628715 0.89242875242165043
		0.32880383226232668 -5.5003889861756486e-017 0.89242875242165043
		0.62958834397457686 0.26078403078628787 0.89242875242165043
		0.23249941947281566 0.23249941947281585 0.89242875242165043
		0.26078403078628665 0.62958834397457708 0.89242875242165043
		-1.1679166883285452e-016 0.32880383226232723 0.89242875242165043
		-0.26078403078628787 0.62958834397457708 0.89242875242165043
		;
createNode transform -n "foot_R0_bk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 3.4694469519536142e-018 -1.1102230246251565e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 90.000000000001549 89.055637214679848 -90 ;
	setAttr -k on ".ro";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_R0_bk0_ctlShape" -p "foot_R0_bk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.6613381477509392e-016 0.10248560095429093 0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 0.73446618905870076
		2.2204460492503131e-016 5.6519166520644008e-017 0.69201526319547679
		1.1102230246251565e-016 -0.10248560095429093 0.73446618905870076
		1.6911482431301163e-016 -0.1449365268175149 0.83695179001299169
		-1.1102230246251565e-016 -0.10248560095429093 0.93943739096728152
		2.2204460492503131e-016 -6.3325504211831524e-017 0.9818883168305057
		6.6613381477509392e-016 0.10248560095429093 0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 0.73446618905870076
		;
createNode nurbsCurve -n "foot_R0_bk0_ctlShape1" -p "foot_R0_bk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		1.98143424909718e-016 0 0.96659630343203529
		;
createNode nurbsCurve -n "foot_R0_bk0_ctlShape2" -p "foot_R0_bk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		1.1102230246251565e-016 0.13078621819644001 0.83581008523559541
		1.1102230246251565e-016 -0.13078621819644001 0.83581008523559541
		;
createNode transform -n "foot_R0_bk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 5.5511151231257827e-017 -1.1102230246251565e-016 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -89.999999999999332 169.99235895098414 ;
	setAttr -k on ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_R0_bk1_ctlShape" -p "foot_R0_bk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.6613381477509392e-016 0.10248560095429093 0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 0.73446618905870076
		2.2204460492503131e-016 5.6519166520644008e-017 0.69201526319547679
		1.1102230246251565e-016 -0.10248560095429093 0.73446618905870076
		1.6911482431301163e-016 -0.1449365268175149 0.83695179001299169
		-1.1102230246251565e-016 -0.10248560095429093 0.93943739096728152
		2.2204460492503131e-016 -6.3325504211831524e-017 0.9818883168305057
		6.6613381477509392e-016 0.10248560095429093 0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 0.73446618905870076
		;
createNode nurbsCurve -n "foot_R0_bk1_ctlShape1" -p "foot_R0_bk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		1.98143424909718e-016 0 0.96659630343203529
		;
createNode nurbsCurve -n "foot_R0_bk1_ctlShape2" -p "foot_R0_bk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		1.1102230246251565e-016 0.13078621819644001 0.83581008523559541
		1.1102230246251565e-016 -0.13078621819644001 0.83581008523559541
		;
createNode transform -n "foot_R0_bk2_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -3.3306690738754696e-016 -5.5511151231257827e-017 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -89.999999999999304 165.23247204847982 ;
	setAttr -k on ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "foot_R0_bk2_ctlShape" -p "foot_R0_bk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.6613381477509392e-016 0.10248560095429093 0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 0.73446618905870076
		2.2204460492503131e-016 5.6519166520644008e-017 0.69201526319547679
		1.1102230246251565e-016 -0.10248560095429093 0.73446618905870076
		1.6911482431301163e-016 -0.1449365268175149 0.83695179001299169
		-1.1102230246251565e-016 -0.10248560095429093 0.93943739096728152
		2.2204460492503131e-016 -6.3325504211831524e-017 0.9818883168305057
		6.6613381477509392e-016 0.10248560095429093 0.93943739096728218
		9.4610194221698996e-017 0.1449365268175149 0.83695179001299169
		3.3306690738754696e-016 0.10248560095429093 0.73446618905870076
		;
createNode nurbsCurve -n "foot_R0_bk2_ctlShape1" -p "foot_R0_bk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		1.98143424909718e-016 0 0.96659630343203529
		;
createNode nurbsCurve -n "foot_R0_bk2_ctlShape2" -p "foot_R0_bk2_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		1.1102230246251565e-016 0.13078621819644001 0.83581008523559541
		1.1102230246251565e-016 -0.13078621819644001 0.83581008523559541
		;
createNode transform -n "foot_R0_fk0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.83666277387714716 0.30431196584359405 0.55042018545206306 ;
	setAttr ".r" -type "double3" -89.999999999998892 75.232472048479821 90.000000000000583 ;
	setAttr -k on ".ro";
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000004 1.0000000000000002 ;
createNode nurbsCurve -n "foot_R0_fk0_ctlShape" -p "foot_R0_fk0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 0 no 3
		13 0 0 0 1 2 3 4 5 6 7 8 8 8
		11
		-0.010688074753943066 -0.0052801994153324882 -0.747701542084941
		-0.010688074753943066 -0.085531104398958155 -0.747701542084941
		-0.01013156670511739 -0.24778970244641713 -0.70877012217336033
		-0.0077543654438998466 -0.45288089557670946 -0.54246916622225094
		-0.004196632337346573 -0.59019962535029991 -0.29358219720904127
		-5.5071253258009846e-017 -0.63836376479781831 3.0418165026757096e-017
		0.004196632337346573 -0.59019962535029991 0.29358219720904116
		0.0077543654439000687 -0.45288089557670946 0.54246916622225128
		0.010131566705117168 -0.24778970244641713 0.70877012217336033
		0.010688074753943066 -0.085531104398958308 0.747701542084941
		0.010688074753943066 -0.0052801994153324882 0.747701542084941
		;
createNode transform -n "foot_R0_fk1_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.83666277387714394 0.13927624906416403 1.1764917847694865 ;
	setAttr ".r" -type "double3" -89.99999999999838 79.992358737188596 90.00000000000118 ;
	setAttr -k on ".ro";
	setAttr ".s" -type "double3" 0.99999999999999967 1.0000000000000009 1.0000000000000002 ;
createNode nurbsCurve -n "foot_R0_fk1_ctlShape" -p "foot_R0_fk1_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 0 no 3
		13 0 0 0 1 2 3 4 5 6 7 8 8 8
		11
		-0.010688074753943066 -0.0027635364928795133 -0.747701542084941
		-0.010688074753943066 -0.083014441476505219 -0.747701542084941
		-0.01013156670511739 -0.24527303952396398 -0.70877012217336033
		-0.0077543654438998466 -0.45036423265425723 -0.54246916622225094
		-0.004196632337346573 -0.58768296242784623 -0.29358219720904127
		-5.5071253258009846e-017 -0.63584710187536531 3.0418165026757096e-017
		0.004196632337346573 -0.58768296242784623 0.29358219720904116
		0.0077543654439000687 -0.45036423265425723 0.54246916622225128
		0.010131566705117168 -0.24527303952396398 0.70877012217336033
		0.010688074753943066 -0.083014441476505219 0.747701542084941
		0.010688074753943066 -0.0027635364928795133 0.747701542084941
		;
createNode transform -n "legUI_R0_ctl" -p "controllers_org";
	addAttr -ci true -sn "invTx" -ln "invTx" -nn "Invert Mirror TX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTy" -ln "invTy" -nn "Invert Mirror TY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invTz" -ln "invTz" -nn "Invert Mirror TZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRx" -ln "invRx" -nn "Invert Mirror RX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRy" -ln "invRy" -nn "Invert Mirror RY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invRz" -ln "invRz" -nn "Invert Mirror RZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSx" -ln "invSx" -nn "Invert Mirror SX" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSy" -ln "invSy" -nn "Invert Mirror SY" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "invSz" -ln "invSz" -nn "Invert Mirror SZ" -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "leg_R0" -ln "leg_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "leg_R0_blend" -ln "leg_R0_blend" -nn "Fk/Ik Blend" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_R0_roll" -ln "leg_R0_roll" -nn "Roll" -min -180 
		-max 180 -at "double";
	addAttr -ci true -k true -sn "leg_R0_ikscale" -ln "leg_R0_ikscale" -nn "Scale" -dv 
		0.001 -min 0.001 -max 99 -at "double";
	addAttr -ci true -k true -sn "leg_R0_maxstretch" -ln "leg_R0_maxstretch" -nn "Max Stretch" 
		-dv 1 -min 1 -max 99 -at "double";
	addAttr -ci true -k true -sn "leg_R0_slide" -ln "leg_R0_slide" -nn "Slide" -min 
		0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_R0_softness" -ln "leg_R0_softness" -nn "Softness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_R0_reverse" -ln "leg_R0_reverse" -nn "Reverse" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_R0_roundness" -ln "leg_R0_roundness" -nn "Roundness" 
		-min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "leg_R0_volume" -ln "leg_R0_volume" -nn "Volume" -min 
		0 -max 1 -at "double";
	addAttr -ci true -k true -sn "foot_R0" -ln "foot_R0" -min 0 -max 0 -en "---------------" 
		-at "enum";
	addAttr -ci true -k true -sn "foot_R0_angle_0" -ln "foot_R0_angle_0" -nn "Angle 0" 
		-at "double";
	addAttr -ci true -k true -sn "foot_R0_angle_1" -ln "foot_R0_angle_1" -nn "Angle 1" 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".leg_R0_blend" 1;
	setAttr -k on ".leg_R0_ikscale" 1;
	setAttr -k on ".leg_R0_maxstretch" 1.5;
	setAttr -k on ".leg_R0_slide" 0.5;
	setAttr -k on ".leg_R0_volume" 1;
	setAttr -k on ".foot_R0_angle_0" -20;
	setAttr -k on ".foot_R0_angle_1" -20;
createNode nurbsCurve -n "legUI_R0_ctlShape" -p "legUI_R0_ctl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 2 no 3
		13 0 1 2 3 4 5 6 7 8 9 10 11 12
		13
		0.15996622010953834 0.23994933016430778 1.5069991277924252e-017
		0.23994933016430778 0.15996622010953834 1.5069991277924252e-017
		0.079983110054769171 0 1.5069991277924252e-017
		0.23994933016430778 -0.15996622010953834 1.5069991277924252e-017
		0.15996622010953834 -0.23994933016430778 1.5069991277924252e-017
		0 -0.079983110054769171 1.5069991277924252e-017
		-0.15996622010953834 -0.23994933016430778 1.5069991277924252e-017
		-0.23994933016430778 -0.15996622010953834 1.5069991277924252e-017
		-0.079983110054769171 0 1.5069991277924252e-017
		-0.23994933016430778 0.15996622010953834 1.5069991277924252e-017
		-0.15996622010953834 0.23994933016430778 1.5069991277924252e-017
		0 0.079983110054769171 1.5069991277924252e-017
		0.15996622010953834 0.23994933016430778 1.5069991277924252e-017
		;
createNode transform -n "biped_guide" -p "controllers_org";
	addAttr -ci true -sn "rig_name" -ln "rig_name" -dt "string";
	addAttr -ci true -k true -sn "mode" -ln "mode" -min 0 -max 1 -en "Final:WIP" -at "enum";
	addAttr -ci true -k true -sn "step" -ln "step" -min 0 -max 5 -en "All Steps:Objects:Properties:Operators:Connect:Finalize" 
		-at "enum";
	addAttr -ci true -sn "ismodel" -ln "ismodel" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "importSkinPack" -ln "importSkinPack" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "skinPack" -ln "skinPack" -dt "string";
	addAttr -ci true -sn "L_color_fk" -ln "L_color_fk" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "L_color_ik" -ln "L_color_ik" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "R_color_fk" -ln "R_color_fk" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "R_color_ik" -ln "R_color_ik" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "C_color_fk" -ln "C_color_fk" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "C_color_ik" -ln "C_color_ik" -min 0 -max 31 -at "long";
	addAttr -ci true -sn "shadow_rig" -ln "shadow_rig" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "synoptic" -ln "synoptic" -dt "string";
	addAttr -ci true -sn "comments" -ln "comments" -dt "string";
	setAttr ".rig_name" -type "string" "rig";
	setAttr -k on ".step" 5;
	setAttr ".ismodel" yes;
	setAttr ".skinPack" -type "string" "";
	setAttr ".L_color_fk" 6;
	setAttr ".L_color_ik" 5;
	setAttr ".R_color_fk" 30;
	setAttr ".R_color_ik" 9;
	setAttr ".C_color_fk" 13;
	setAttr ".C_color_ik" 4;
	setAttr ".shadow_rig" yes;
	setAttr ".synoptic" -type "string" "biped_body,biped_hands";
	setAttr ".comments" -type "string" "";
createNode transform -n "local_C0_root" -p "|biped_guide";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	addAttr -ci true -sn "shadow" -ln "shadow" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "default_rotorder" -ln "default_rotorder" -min 0 -max 5 -at "long";
	setAttr ".comp_type" -type "string" "control_02";
	setAttr ".comp_name" -type "string" "local";
	setAttr ".comp_side" -type "string" "C";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".icon" -type "string" "square";
	setAttr ".k_tx" yes;
	setAttr ".k_ty" yes;
	setAttr ".k_tz" yes;
	setAttr ".k_ro" yes;
	setAttr ".k_rx" yes;
	setAttr ".k_ry" yes;
	setAttr ".k_rz" yes;
	setAttr ".k_sx" yes;
	setAttr ".k_sy" yes;
	setAttr ".k_sz" yes;
	setAttr ".default_rotorder" 2;
createNode nurbsCurve -n "local_C0_rootShape" -p "local_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "local_C0_root1Shape" -p "local_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "local_C0_root2Shape" -p "local_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "local_C0_root3Shape" -p "local_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "body_C0_root" -p "local_C0_root";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	addAttr -ci true -sn "shadow" -ln "shadow" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "default_rotorder" -ln "default_rotorder" -min 0 -max 5 -at "long";
	setAttr ".t" -type "double3" -1.3866647412954306e-016 10.353589031507756 -0.0085554946225459023 ;
	setAttr ".r" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.9535389960287504 1.953538996028749 1.9535389960287504 ;
	setAttr ".comp_type" -type "string" "control_02";
	setAttr ".comp_name" -type "string" "body";
	setAttr ".comp_side" -type "string" "C";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "spineUI_C0_root";
	setAttr ".icon" -type "string" "square";
	setAttr ".k_tx" yes;
	setAttr ".k_ty" yes;
	setAttr ".k_tz" yes;
	setAttr ".k_ro" yes;
	setAttr ".k_rx" yes;
	setAttr ".k_ry" yes;
	setAttr ".k_rz" yes;
	setAttr ".default_rotorder" 2;
createNode nurbsCurve -n "body_C0_rootShape" -p "body_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "body_C0_root1Shape" -p "body_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "body_C0_root2Shape" -p "body_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "body_C0_root3Shape" -p "body_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "spine_C0_root" -p "body_C0_root";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "position" -ln "position" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "maxstretch" -ln "maxstretch" -dv 1 -min 1 -at "double";
	addAttr -ci true -sn "maxsquash" -ln "maxsquash" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "softness" -ln "softness" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "lock_ori" -ln "lock_ori" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "division" -ln "division" -dv 3 -min 3 -at "long";
	addAttr -ci true -k true -sn "st_profile" -ln "st_profile" -at "double";
	addAttr -ci true -k true -sn "sq_profile" -ln "sq_profile" -at "double";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-016 -1.2325951644078309e-032 ;
	setAttr ".r" -type "double3" 6.3611093629270367e-015 1.4124500153760495e-030 90.000000000000014 ;
	setAttr ".s" -type "double3" 0.51189149642410525 0.51189149642410525 0.51189149642410436 ;
	setAttr ".comp_type" -type "string" "spine_ik_01";
	setAttr ".comp_name" -type "string" "spine";
	setAttr ".comp_side" -type "string" "C";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "spineUI_C0_root";
	setAttr ".maxstretch" 1.5;
	setAttr ".maxsquash" 0.5;
	setAttr ".lock_ori" 1;
	setAttr ".division" 5;
	setAttr -k on ".st_profile";
	setAttr -k on ".sq_profile";
createNode nurbsCurve -n "spine_C0_rootShape" -p "spine_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "spine_C0_root1Shape" -p "spine_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "spine_C0_root2Shape" -p "spine_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "spine_C0_root3Shape" -p "spine_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "spine_C0_eff" -p "spine_C0_root";
	setAttr ".t" -type "double3" 2.3760066881565169 -1.6878859421254333e-015 7.3955709864469857e-032 ;
	setAttr ".r" -type "double3" 6.3611093629270493e-015 1.4124500153760515e-030 -6.3611093629270406e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999933 1 ;
createNode nurbsCurve -n "spine_C0_effShape" -p "spine_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "spine_C0_eff1Shape" -p "spine_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "spine_C0_eff2Shape" -p "spine_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "spine_C0_eff3Shape" -p "spine_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "spine_C0_eff3_0crvShape" -p "spine_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "spine_C0_eff3_1crvShape" -p "spine_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "shoulder_L0_root" -p "spine_C0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "refArray" -ln "refArray" -dt "string";
	setAttr ".t" -type "double3" 1.8665008412384338 -0.41852659998613168 -0.11673327753264971 ;
	setAttr ".r" -type "double3" -6.3623615605976545e-015 1.7849340970487848e-016 179.19611077184621 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.999999999999999 ;
	setAttr ".comp_type" -type "string" "shoulder_01";
	setAttr ".comp_name" -type "string" "shoulder";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "armUI_L0_root";
	setAttr ".refArray" -type "string" "shoulder_L0_root,local_C0_root,body_C0_root,spine_C0_eff";
createNode nurbsCurve -n "shoulder_L0_rootShape" -p "shoulder_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "shoulder_L0_root1Shape" -p "shoulder_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "shoulder_L0_root2Shape" -p "shoulder_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "shoulder_L0_root3Shape" -p "shoulder_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "shoulder_L0_tip" -p "shoulder_L0_root";
	setAttr ".t" -type "double3" 0.19622949105844434 -0.65582077355763002 -1.8410072078621214 ;
	setAttr ".r" -type "double3" -6.3623615605976608e-015 1.7849340970487867e-016 4.8702243559910078e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999867 0.99999999999999911 ;
createNode nurbsCurve -n "shoulder_L0_tipShape" -p "shoulder_L0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "shoulder_L0_tip1Shape" -p "shoulder_L0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "shoulder_L0_tip2Shape" -p "shoulder_L0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "shoulder_L0_tip3Shape" -p "shoulder_L0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "shoulder_L0_tip3_0crvShape" -p "shoulder_L0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "shoulder_L0_tip3_1crvShape" -p "shoulder_L0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "arm_L0_root" -p "shoulder_L0_tip";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	addAttr -ci true -sn "upvrefarray" -ln "upvrefarray" -dt "string";
	addAttr -ci true -sn "maxstretch" -ln "maxstretch" -dv 1 -min 1 -at "double";
	addAttr -ci true -sn "div0" -ln "div0" -dv 1 -min 1 -at "long";
	addAttr -ci true -sn "div1" -ln "div1" -dv 1 -min 1 -at "long";
	addAttr -ci true -k true -sn "st_profile" -ln "st_profile" -at "double";
	addAttr -ci true -k true -sn "sq_profile" -ln "sq_profile" -at "double";
	setAttr ".t" -type "double3" 3.5527136788005009e-015 -2.2204460492503131e-016 -1.3322676295501878e-015 ;
	setAttr ".r" -type "double3" -96.307283307451371 48.240718341633141 -6.0629030460035906 ;
	setAttr ".s" -type "double3" 1.0000000000000007 0.99999999999999933 1.0000000000000004 ;
	setAttr ".comp_type" -type "string" "arm_2jnt_01";
	setAttr ".comp_name" -type "string" "arm";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "armUI_L0_root";
	setAttr ".ikrefarray" -type "string" "local_C0_root,body_C0_root,spine_C0_eff,spine_C0_root";
	setAttr ".upvrefarray" -type "string" "local_C0_root,body_C0_root,spine_C0_eff,spine_C0_root";
	setAttr ".maxstretch" 1.5;
	setAttr ".div0" 5;
	setAttr ".div1" 5;
	setAttr -k on ".st_profile";
	setAttr -k on ".sq_profile";
createNode nurbsCurve -n "arm_L0_rootShape" -p "arm_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "arm_L0_root1Shape" -p "arm_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "arm_L0_root2Shape" -p "arm_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "arm_L0_root3Shape" -p "arm_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "arm_L0_elbow" -p "arm_L0_root";
	setAttr ".t" -type "double3" 2.6990161750824164 0.042436136907149091 -0.096150543189835735 ;
	setAttr ".r" -type "double3" 4.2481866297856175e-015 -10.688700162784263 -1.7422622547156487e-014 ;
	setAttr ".s" -type "double3" 1 0.99999999999999956 0.99999999999999967 ;
createNode nurbsCurve -n "arm_L0_elbowShape" -p "arm_L0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "arm_L0_elbow1Shape" -p "arm_L0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "arm_L0_elbow2Shape" -p "arm_L0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "arm_L0_elbow3Shape" -p "arm_L0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_L0_elbow3_0crvShape" -p "arm_L0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_L0_elbow3_1crvShape" -p "arm_L0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "arm_L0_wrist" -p "arm_L0_elbow";
	setAttr ".t" -type "double3" 2.7014894887411929 -0.078016774483520734 -0.050560325901132852 ;
	setAttr ".r" -type "double3" -3.5582455498873091e-014 -2.3854160110976384e-015 -1.398947098174969e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
createNode nurbsCurve -n "arm_L0_wristShape" -p "arm_L0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "arm_L0_wrist1Shape" -p "arm_L0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "arm_L0_wrist2Shape" -p "arm_L0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "arm_L0_wrist3Shape" -p "arm_L0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_L0_wrist3_0crvShape" -p "arm_L0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_L0_wrist3_1crvShape" -p "arm_L0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "arm_L0_eff" -p "arm_L0_wrist";
	setAttr ".t" -type "double3" 1.1065999409443981 -0.036487063347861337 -0.07849869998443107 ;
	setAttr ".r" -type "double3" -3.5980024834056031e-014 -2.3854160110976384e-015 -1.7120329496315339e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000004 ;
createNode nurbsCurve -n "arm_L0_effShape" -p "arm_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "arm_L0_eff1Shape" -p "arm_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "arm_L0_eff2Shape" -p "arm_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "arm_L0_eff3Shape" -p "arm_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_L0_eff3_0crvShape" -p "arm_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_L0_eff3_1crvShape" -p "arm_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "thumb_L0_root" -p "arm_L0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.73075326583805278 -0.08411543847520464 0.23252932764826889 ;
	setAttr ".r" -type "double3" 0 -46.280259360575748 0 ;
	setAttr ".s" -type "double3" 0.51359190808549116 0.51359190808549116 0.51359190808549116 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "thumb";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "thumb_L0_rootShape" -p "thumb_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "thumb_L0_root1Shape" -p "thumb_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "thumb_L0_root2Shape" -p "thumb_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "thumb_L0_root3Shape" -p "thumb_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "thumb_L0_0_loc" -p "thumb_L0_root";
	setAttr ".t" -type "double3" 0.99999999999999978 -5.8980598183211417e-017 1.3877787807814449e-017 ;
	setAttr ".r" -type "double3" 4.7708320221952775e-015 -7.951386703658789e-016 -3.379339349054986e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
createNode nurbsCurve -n "thumb_L0_0_locShape" -p "thumb_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "thumb_L0_0_loc1Shape" -p "thumb_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "thumb_L0_0_loc2Shape" -p "thumb_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "thumb_L0_0_loc3Shape" -p "thumb_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_L0_0_loc3_0crvShape" -p "thumb_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_L0_0_loc3_1crvShape" -p "thumb_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "thumb_L0_1_loc" -p "thumb_L0_0_loc";
	setAttr ".t" -type "double3" 1 -5.8980598183211429e-017 1.3877787807814449e-017 ;
	setAttr ".r" -type "double3" 4.7708320221952752e-015 -7.951386703658788e-016 -3.379339349054986e-015 ;
createNode nurbsCurve -n "thumb_L0_1_locShape" -p "thumb_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "thumb_L0_1_loc1Shape" -p "thumb_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "thumb_L0_1_loc2Shape" -p "thumb_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "thumb_L0_1_loc3Shape" -p "thumb_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_L0_1_loc3_0crvShape" -p "thumb_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_L0_1_loc3_1crvShape" -p "thumb_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "thumb_L0_2_loc" -p "thumb_L0_1_loc";
	setAttr ".t" -type "double3" 1 -5.8980598183211417e-017 1.3877787807814448e-017 ;
	setAttr ".r" -type "double3" 4.7708320221952752e-015 -7.951386703658788e-016 -3.379339349054986e-015 ;
createNode nurbsCurve -n "thumb_L0_2_locShape" -p "thumb_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "thumb_L0_2_loc1Shape" -p "thumb_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "thumb_L0_2_loc2Shape" -p "thumb_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "thumb_L0_2_loc3Shape" -p "thumb_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_L0_2_loc3_0crvShape" -p "thumb_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_L0_2_loc3_1crvShape" -p "thumb_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "thumb_L0_blade" -p "thumb_L0_root";
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
createNode nurbsCurve -n "thumb_L0_bladeShape" -p "thumb_L0_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.60000000000000131 0 0
		0 0.20000000000000043 0
		0 0 0
		;
createNode aimConstraint -n "thumb_L0_blade_aimConstraint1" -p "thumb_L0_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "chain_C0_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "thumb_L0_blade_pointConstraint1" -p "thumb_L0_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "chain_C0_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "thumb_L0_crv" -p "thumb_L0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".r" -type "double3" 4.7708320221952775e-015 -7.951386703658789e-016 -3.379339349054986e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
createNode nurbsCurve -n "thumb_L0_crvShape" -p "thumb_L0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "thumb_L0_crvShapeOrig" -p "thumb_L0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "finger_L0_root" -p "arm_L0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031148103377457569 2.833178546574487e-015 0.45446815993077583 ;
	setAttr ".s" -type "double3" 0.3987344327070963 0.3987344327070963 0.3987344327070963 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "finger";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "finger_L0_rootShape" -p "finger_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L0_root1Shape" -p "finger_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L0_root2Shape" -p "finger_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L0_root3Shape" -p "finger_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "finger_L0_0_loc" -p "finger_L0_root";
	setAttr ".t" -type "double3" 0.99999999999999978 -5.8980598183211417e-017 1.3877787807814449e-017 ;
	setAttr ".r" -type "double3" 4.7708320221952775e-015 -7.951386703658789e-016 -3.379339349054986e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
createNode nurbsCurve -n "finger_L0_0_locShape" -p "finger_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L0_0_loc1Shape" -p "finger_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L0_0_loc2Shape" -p "finger_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L0_0_loc3Shape" -p "finger_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L0_0_loc3_0crvShape" -p "finger_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L0_0_loc3_1crvShape" -p "finger_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L0_1_loc" -p "finger_L0_0_loc";
	setAttr ".t" -type "double3" 1 -5.8980598183211429e-017 1.3877787807814449e-017 ;
	setAttr ".r" -type "double3" 4.7708320221952752e-015 -7.951386703658788e-016 -3.379339349054986e-015 ;
createNode nurbsCurve -n "finger_L0_1_locShape" -p "finger_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L0_1_loc1Shape" -p "finger_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L0_1_loc2Shape" -p "finger_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L0_1_loc3Shape" -p "finger_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L0_1_loc3_0crvShape" -p "finger_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L0_1_loc3_1crvShape" -p "finger_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L0_2_loc" -p "finger_L0_1_loc";
	setAttr ".t" -type "double3" 1 -5.8980598183211417e-017 1.3877787807814448e-017 ;
	setAttr ".r" -type "double3" 4.7708320221952752e-015 -7.951386703658788e-016 -3.379339349054986e-015 ;
createNode nurbsCurve -n "finger_L0_2_locShape" -p "finger_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L0_2_loc1Shape" -p "finger_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L0_2_loc2Shape" -p "finger_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L0_2_loc3Shape" -p "finger_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L0_2_loc3_0crvShape" -p "finger_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L0_2_loc3_1crvShape" -p "finger_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L0_blade" -p "finger_L0_root";
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
createNode nurbsCurve -n "finger_L0_bladeShape" -p "finger_L0_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.60000000000000131 0 0
		0 0.20000000000000043 0
		0 0 0
		;
createNode aimConstraint -n "finger_L0_blade_aimConstraint1" -p "finger_L0_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "chain_C0_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "finger_L0_blade_pointConstraint1" -p "finger_L0_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "chain_C0_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "finger_L0_crv" -p "finger_L0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".r" -type "double3" 4.7708320221952775e-015 -7.951386703658789e-016 -3.379339349054986e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
createNode nurbsCurve -n "finger_L0_crvShape" -p "finger_L0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "finger_L0_crvShapeOrig" -p "finger_L0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "finger_L1_root" -p "arm_L0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031148103377457 7.8024814986622752e-015 0.11942617708530555 ;
	setAttr ".r" -type "double3" -2.1187907739193131e-014 -9.9707801125614718e-016 -6.5277451049425872e-015 ;
	setAttr ".s" -type "double3" 0.3987344327070963 0.39873443270709619 0.39873443270709624 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "finger";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".comp_index" 1;
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "finger_L1_rootShape" -p "finger_L1_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L1_root1Shape" -p "finger_L1_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L1_root2Shape" -p "finger_L1_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L1_root3Shape" -p "finger_L1_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "finger_L1_0_loc" -p "finger_L1_root";
	setAttr ".t" -type "double3" 0.99999999999999645 0 -2.2204460492503131e-016 ;
	setAttr ".r" -type "double3" -2.2065098102653154e-014 -7.9513867036587959e-016 1.9133024255678975e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999978 1 ;
createNode nurbsCurve -n "finger_L1_0_locShape" -p "finger_L1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L1_0_loc1Shape" -p "finger_L1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L1_0_loc2Shape" -p "finger_L1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L1_0_loc3Shape" -p "finger_L1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L1_0_loc3_0crvShape" -p "finger_L1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L1_0_loc3_1crvShape" -p "finger_L1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L1_1_loc" -p "finger_L1_0_loc";
	setAttr ".t" -type "double3" 1.0000000000000051 -3.5527136788005009e-015 -2.2204460492503131e-016 ;
	setAttr ".r" -type "double3" -2.1866313435061677e-014 -1.5902773407317576e-015 1.8636062586700288e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L1_1_locShape" -p "finger_L1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L1_1_loc1Shape" -p "finger_L1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L1_1_loc2Shape" -p "finger_L1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L1_1_loc3Shape" -p "finger_L1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L1_1_loc3_0crvShape" -p "finger_L1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L1_1_loc3_1crvShape" -p "finger_L1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L1_2_loc" -p "finger_L1_1_loc";
	setAttr ".t" -type "double3" 0.99999999999999833 -3.5527136788005009e-015 -2.2204460492503131e-016 ;
	setAttr ".r" -type "double3" -2.2263882770244617e-014 7.9513867036587939e-016 1.9133024255678971e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999978 ;
createNode nurbsCurve -n "finger_L1_2_locShape" -p "finger_L1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L1_2_loc1Shape" -p "finger_L1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L1_2_loc2Shape" -p "finger_L1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L1_2_loc3Shape" -p "finger_L1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L1_2_loc3_0crvShape" -p "finger_L1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L1_2_loc3_1crvShape" -p "finger_L1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L1_blade" -p "finger_L1_root";
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999978 1 ;
createNode nurbsCurve -n "finger_L1_bladeShape" -p "finger_L1_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.23924065962425778 0 0
		0 0.07974688654141926 0
		0 0 0
		;
createNode aimConstraint -n "finger_L1_blade_aimConstraint1" -p "finger_L1_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "finger_L1_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "finger_L1_blade_pointConstraint1" -p "finger_L1_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "finger_L1_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.6645352591003757e-015 -3.5527136788005009e-015 
		0 ;
	setAttr -k on ".w0";
createNode transform -n "finger_L1_crv" -p "finger_L1_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 3.8968057566131824 -30.157913717968565 -1.4788493414315511 ;
	setAttr ".r" -type "double3" 4.9337935535729258 3.8104210806425542 41.562588836241147 ;
	setAttr ".s" -type "double3" 2.5079349009584675 2.5079349009584591 2.5079349009584613 ;
createNode nurbsCurve -n "finger_L1_crvShape" -p "finger_L1_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "finger_L1_crvShapeOrig" -p "finger_L1_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "finger_L2_root" -p "arm_L0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031148103377460493 8.8817841970012523e-015 -0.21759175506122969 ;
	setAttr ".r" -type "double3" -2.1187907739193135e-014 -9.9707801125614718e-016 -6.5277451049425872e-015 ;
	setAttr ".s" -type "double3" 0.3987344327070963 0.39873443270709613 0.39873443270709619 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "finger";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".comp_index" 2;
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "finger_L2_rootShape" -p "finger_L2_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L2_root1Shape" -p "finger_L2_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L2_root2Shape" -p "finger_L2_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L2_root3Shape" -p "finger_L2_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "finger_L2_0_loc" -p "finger_L2_root";
	setAttr ".t" -type "double3" 0.9999999999999889 -3.5527136788005009e-015 0 ;
	setAttr ".r" -type "double3" -2.1667528767470214e-014 0 1.9133024255678971e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L2_0_locShape" -p "finger_L2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L2_0_loc1Shape" -p "finger_L2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L2_0_loc2Shape" -p "finger_L2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L2_0_loc3Shape" -p "finger_L2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L2_0_loc3_0crvShape" -p "finger_L2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L2_0_loc3_1crvShape" -p "finger_L2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L2_1_loc" -p "finger_L2_0_loc";
	setAttr ".t" -type "double3" 1.0000000000000084 0 -3.3306690738754696e-016 ;
	setAttr ".r" -type "double3" -2.0474820761921391e-014 -2.3854160110976376e-015 -1.2921003393445538e-015 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L2_1_locShape" -p "finger_L2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L2_1_loc1Shape" -p "finger_L2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L2_1_loc2Shape" -p "finger_L2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L2_1_loc3Shape" -p "finger_L2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L2_1_loc3_0crvShape" -p "finger_L2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L2_1_loc3_1crvShape" -p "finger_L2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L2_2_loc" -p "finger_L2_1_loc";
	setAttr ".t" -type "double3" 0.999999999999996 -7.1054273576010019e-015 -1.1102230246251565e-016 ;
	setAttr ".r" -type "double3" -2.0872390097104324e-014 -2.3854160110976384e-015 -4.4478069373591372e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 0.99999999999999967 ;
createNode nurbsCurve -n "finger_L2_2_locShape" -p "finger_L2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L2_2_loc1Shape" -p "finger_L2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L2_2_loc2Shape" -p "finger_L2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L2_2_loc3Shape" -p "finger_L2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L2_2_loc3_0crvShape" -p "finger_L2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L2_2_loc3_1crvShape" -p "finger_L2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L2_blade" -p "finger_L2_root";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L2_bladeShape" -p "finger_L2_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.23924065962425778 0 0
		0 0.07974688654141926 0
		0 0 0
		;
createNode aimConstraint -n "finger_L2_blade_aimConstraint1" -p "finger_L2_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "finger_L2_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "finger_L2_blade_pointConstraint1" -p "finger_L2_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "finger_L2_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -3.1086244689504383e-015 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "finger_L2_crv" -p "finger_L2_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 3.8968057566131775 -30.157913717968587 -0.63858585936706924 ;
	setAttr ".r" -type "double3" 4.933793553572924 3.8104210806425542 41.562588836241154 ;
	setAttr ".s" -type "double3" 2.5079349009584679 2.5079349009584599 2.5079349009584622 ;
createNode nurbsCurve -n "finger_L2_crvShape" -p "finger_L2_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "finger_L2_crvShapeOrig" -p "finger_L2_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "finger_L3_root" -p "arm_L0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031148103377459525 1.2074730309688762e-014 -0.5321312907263378 ;
	setAttr ".r" -type "double3" -2.1187907739193135e-014 -9.9707801125614718e-016 -6.5277451049425872e-015 ;
	setAttr ".s" -type "double3" 0.3987344327070963 0.39873443270709608 0.39873443270709613 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "finger";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".comp_index" 3;
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "finger_L3_rootShape" -p "finger_L3_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L3_root1Shape" -p "finger_L3_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L3_root2Shape" -p "finger_L3_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L3_root3Shape" -p "finger_L3_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "finger_L3_0_loc" -p "finger_L3_root";
	setAttr ".t" -type "double3" 0.99999999999998934 -1.0658141036401503e-014 4.163336342344337e-016 ;
	setAttr ".r" -type "double3" -2.1667528767470207e-014 -1.5902773407317588e-015 1.8884543421189634e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000004 ;
createNode nurbsCurve -n "finger_L3_0_locShape" -p "finger_L3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L3_0_loc1Shape" -p "finger_L3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L3_0_loc2Shape" -p "finger_L3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L3_0_loc3Shape" -p "finger_L3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L3_0_loc3_0crvShape" -p "finger_L3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L3_0_loc3_1crvShape" -p "finger_L3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L3_1_loc" -p "finger_L3_0_loc";
	setAttr ".t" -type "double3" 1.0000000000000075 -3.5527136788005009e-015 -1.6653345369377348e-016 ;
	setAttr ".r" -type "double3" -2.0474820761921394e-014 -7.9513867036587919e-016 -1.2424041724466863e-015 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
createNode nurbsCurve -n "finger_L3_1_locShape" -p "finger_L3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L3_1_loc1Shape" -p "finger_L3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L3_1_loc2Shape" -p "finger_L3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L3_1_loc3Shape" -p "finger_L3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L3_1_loc3_0crvShape" -p "finger_L3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L3_1_loc3_1crvShape" -p "finger_L3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L3_2_loc" -p "finger_L3_1_loc";
	setAttr ".t" -type "double3" 0.999999999999994 -3.5527136788005009e-015 -4.163336342344337e-016 ;
	setAttr ".r" -type "double3" -2.0872390097104321e-014 -7.9513867036587919e-016 -4.4229588539102027e-015 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1 ;
createNode nurbsCurve -n "finger_L3_2_locShape" -p "finger_L3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_L3_2_loc1Shape" -p "finger_L3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_L3_2_loc2Shape" -p "finger_L3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_L3_2_loc3Shape" -p "finger_L3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L3_2_loc3_0crvShape" -p "finger_L3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_L3_2_loc3_1crvShape" -p "finger_L3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_L3_blade" -p "finger_L3_root";
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000004 ;
createNode nurbsCurve -n "finger_L3_bladeShape" -p "finger_L3_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.23924065962425778 0 0
		0 0.07974688654141926 0
		0 0 0
		;
createNode aimConstraint -n "finger_L3_blade_aimConstraint1" -p "finger_L3_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "finger_L3_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "finger_L3_blade_pointConstraint1" -p "finger_L3_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "finger_L3_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.6645352591003757e-015 0 -2.7755575615628914e-017 ;
	setAttr -k on ".w0";
createNode transform -n "finger_L3_crv" -p "finger_L3_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 3.8968057566131784 -30.157913717968594 0.20663317491207731 ;
	setAttr ".r" -type "double3" 4.9337935535729258 3.8104210806425542 41.562588836241162 ;
	setAttr ".s" -type "double3" 2.5079349009584679 2.5079349009584599 2.5079349009584626 ;
createNode nurbsCurve -n "finger_L3_crvShape" -p "finger_L3_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "finger_L3_crvShapeOrig" -p "finger_L3_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "armUI_L0_root" -p "arm_L0_wrist";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.020154460490452664 0.56073114764517484 -0.29276117198398777 ;
	setAttr ".r" -type "double3" -3.4986101496098707e-014 -2.3854160110976384e-015 -1.7120329496315339e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999933 1.0000000000000004 ;
	setAttr ".comp_type" -type "string" "control_01";
	setAttr ".comp_name" -type "string" "armUI";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".icon" -type "string" "cross";
createNode nurbsCurve -n "armUI_L0_rootShape" -p "armUI_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "armUI_L0_root1Shape" -p "armUI_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "armUI_L0_root2Shape" -p "armUI_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "armUI_L0_root3Shape" -p "armUI_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "arm_L0_crv" -p "arm_L0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 8.0474849025393915 -12.097066318887974 0.64443564200118342 ;
	setAttr ".r" -type "double3" -2.1534408611046332 -4.1959370793365993 41.5865209405626 ;
	setAttr ".s" -type "double3" 1.0000000000000016 0.99999999999999845 0.99999999999999967 ;
createNode nurbsCurve -n "arm_L0_crvShape" -p "arm_L0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "arm_L0_crvShapeOrig" -p "arm_L0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "shoulder_L0_blade" -p "shoulder_L0_root";
	setAttr ".s" -type "double3" 1.0000000000000013 0.999999999999999 0.99999999999999911 ;
createNode nurbsCurve -n "shoulder_L0_bladeShape" -p "shoulder_L0_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.59999999999999998 0 0
		0 0.19999999999999998 0
		0 0 0
		;
createNode aimConstraint -n "shoulder_L0_blade_aimConstraint1" -p "shoulder_L0_blade";
	addAttr -ci true -sn "w0" -ln "shoulder_L1_tipW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" 0.24131102765602061 172.26382641912258 1.7920697411327537 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "shoulder_L0_blade_pointConstraint1" -p "shoulder_L0_blade";
	addAttr -ci true -k true -sn "w0" -ln "shoulder_L1_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.7763568394002505e-015 2.7755575615628914e-017 -1.3877787807814457e-017 ;
	setAttr -k on ".w0";
createNode transform -n "shoulder_L0_crv" -p "shoulder_L0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 14.600411841401032 -0.20514673749567702 0.11673327753264968 ;
	setAttr ".r" -type "double3" -90.803889228153793 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000013 0.999999999999999 0.99999999999999911 ;
createNode nurbsCurve -n "shoulder_L0_crvShape" -p "shoulder_L0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "shoulder_L0_crvShapeOrig" -p "shoulder_L0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "neck_C0_root" -p "spine_C0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "headrefarray" -ln "headrefarray" -dt "string";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	addAttr -ci true -sn "maxstretch" -ln "maxstretch" -dv 1 -min 1 -at "double";
	addAttr -ci true -sn "maxsquash" -ln "maxsquash" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "softness" -ln "softness" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "division" -ln "division" -dv 3 -min 3 -at "long";
	addAttr -ci true -k true -sn "st_profile" -ln "st_profile" -at "double";
	addAttr -ci true -k true -sn "sq_profile" -ln "sq_profile" -at "double";
	setAttr ".t" -type "double3" 2.6620779378832378 0.29612504304624582 -0.033704944915076702 ;
	setAttr ".r" -type "double3" 180 2.5444437451708156e-014 89.999999999999986 ;
	setAttr ".s" -type "double3" 0.59839228104243303 0.59839228104243103 0.59839228104243103 ;
	setAttr ".comp_type" -type "string" "neck_ik_01";
	setAttr ".comp_name" -type "string" "neck";
	setAttr ".comp_side" -type "string" "C";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "spineUI_C0_root";
	setAttr ".headrefarray" -type "string" "spine_C0_eff,body_C0_root,local_C0_root";
	setAttr ".ikrefarray" -type "string" "spine_C0_eff,body_C0_root,local_C0_root";
	setAttr ".maxstretch" 1.5;
	setAttr ".maxsquash" 0.5;
	setAttr -k on ".st_profile";
	setAttr -k on ".sq_profile";
createNode nurbsCurve -n "neck_C0_rootShape" -p "neck_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "neck_C0_root1Shape" -p "neck_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "neck_C0_root2Shape" -p "neck_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "neck_C0_root3Shape" -p "neck_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "neck_C0_neck" -p "neck_C0_root";
	setAttr ".t" -type "double3" -0.29922236178822137 2.1663558683838069 4.163336342344337e-017 ;
	setAttr ".r" -type "double3" 7.0167092985348807e-015 -2.5444437451708254e-014 -1.491808562398274e-029 ;
	setAttr ".s" -type "double3" 0.999999999999999 0.99999999999999922 0.99999999999999978 ;
createNode nurbsCurve -n "neck_C0_neckShape" -p "neck_C0_neck";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "neck_C0_neck1Shape" -p "neck_C0_neck";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "neck_C0_neck2Shape" -p "neck_C0_neck";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "neck_C0_neck3Shape" -p "neck_C0_neck";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_neck3_0crvShape" -p "neck_C0_neck";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_neck3_1crvShape" -p "neck_C0_neck";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "neck_C0_head" -p "neck_C0_neck";
	setAttr ".t" -type "double3" -1.3877787807814457e-016 0.099999999999997868 2.7755575615628914e-017 ;
	setAttr ".r" -type "double3" 7.0167092985348752e-015 -2.5444437451708229e-014 -1.4918085623982732e-029 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1.0000000000000002 ;
createNode nurbsCurve -n "neck_C0_headShape" -p "neck_C0_head";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "neck_C0_head1Shape" -p "neck_C0_head";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "neck_C0_head2Shape" -p "neck_C0_head";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "neck_C0_head3Shape" -p "neck_C0_head";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_head3_0crvShape" -p "neck_C0_head";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_head3_1crvShape" -p "neck_C0_head";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "neck_C0_eff" -p "neck_C0_head";
	setAttr ".t" -type "double3" 1.1102230246251565e-015 3.0746209978282089 2.7061686225238191e-016 ;
	setAttr ".r" -type "double3" 7.016709298534872e-015 -2.5444437451708226e-014 -1.4918085623982738e-029 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
createNode nurbsCurve -n "neck_C0_effShape" -p "neck_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "neck_C0_eff1Shape" -p "neck_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "neck_C0_eff2Shape" -p "neck_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "neck_C0_eff3Shape" -p "neck_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_eff3_0crvShape" -p "neck_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_eff3_1crvShape" -p "neck_C0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "faceUI_C0_root" -p "neck_C0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.36437974075928714 1.6519679385263402 0.12867375435876308 ;
	setAttr ".r" -type "double3" -1.3360063179915759e-029 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0146515803711991 1.014651580371198 1.0146515803711931 ;
	setAttr ".comp_type" -type "string" "control_01";
	setAttr ".comp_name" -type "string" "faceUI";
	setAttr ".comp_side" -type "string" "C";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".icon" -type "string" "cube";
createNode nurbsCurve -n "faceUI_C0_rootShape" -p "faceUI_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "faceUI_C0_root1Shape" -p "faceUI_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "faceUI_C0_root2Shape" -p "faceUI_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "faceUI_C0_root3Shape" -p "faceUI_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "mouth_C0_root" -p "neck_C0_head";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	setAttr ".t" -type "double3" -0.27051668598627476 -0.012930669119853633 -0.026937392263365875 ;
	setAttr ".r" -type "double3" -1.3360063179915759e-029 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 0.95913545105009512 0.95913545105009346 0.95913545105009024 ;
	setAttr ".comp_type" -type "string" "mouth_01";
	setAttr ".comp_name" -type "string" "mouth";
	setAttr ".comp_side" -type "string" "C";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "faceUI_C0_root";
createNode nurbsCurve -n "mouth_C0_rootShape" -p "mouth_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "mouth_C0_root1Shape" -p "mouth_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "mouth_C0_root2Shape" -p "mouth_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "mouth_C0_root3Shape" -p "mouth_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "mouth_C0_rotcenter" -p "mouth_C0_root";
	setAttr ".t" -type "double3" -2.9455604622086184e-015 -0.36339614713020651 1.5758629599022298 ;
	setAttr ".r" -type "double3" -1.2722218725854047e-014 -2.5444437451708213e-014 -7.0167092985348957e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999944 1.0000000000000002 0.99999999999999922 ;
createNode nurbsCurve -n "mouth_C0_rotcenterShape" -p "mouth_C0_rotcenter";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "mouth_C0_rotcenter1Shape" -p "mouth_C0_rotcenter";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "mouth_C0_rotcenter2Shape" -p "mouth_C0_rotcenter";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "mouth_C0_rotcenter3Shape" -p "mouth_C0_rotcenter";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "mouth_C0_rotcenter3_0crvShape" -p "mouth_C0_rotcenter";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "mouth_C0_rotcenter3_1crvShape" -p "mouth_C0_rotcenter";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "mouth_C0_lipup" -p "mouth_C0_rotcenter";
	setAttr ".t" -type "double3" -0.019701231159258842 0.12388352783450429 0.23628786867351459 ;
	setAttr ".r" -type "double3" -1.2722218725854058e-014 -2.5444437451708219e-014 -7.0167092985348933e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000004 1.0000000000000004 ;
createNode nurbsCurve -n "mouth_C0_lipupShape" -p "mouth_C0_lipup";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "mouth_C0_lipup1Shape" -p "mouth_C0_lipup";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "mouth_C0_lipup2Shape" -p "mouth_C0_lipup";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "mouth_C0_lipup3Shape" -p "mouth_C0_lipup";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "mouth_C0_lipup3_0crvShape" -p "mouth_C0_lipup";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "mouth_C0_lipup3_1crvShape" -p "mouth_C0_lipup";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "mouth_C0_C1_crv" -p "mouth_C0_lipup";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.010939324093837558 -28.927521856106338 -1.875305440677586 ;
	setAttr ".r" -type "double3" -1.2722218725854061e-014 -2.5444437451708197e-014 -7.0167092985348878e-015 ;
	setAttr ".s" -type "double3" 1.742344680788962 1.7423446807889622 1.7423446807889635 ;
createNode nurbsCurve -n "mouth_C0_C1_crvShape" -p "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "mouth_C0_C1_crvShapeOrig" -p "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "mouth_C0_liplow" -p "mouth_C0_rotcenter";
	setAttr ".t" -type "double3" 0.0058298409030715127 -0.14678247393467814 0.1819453182022237 ;
	setAttr ".r" -type "double3" -1.2722218725854058e-014 -2.5444437451708219e-014 -7.0167092985348933e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000004 1.0000000000000004 ;
createNode nurbsCurve -n "mouth_C0_liplowShape" -p "mouth_C0_liplow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "mouth_C0_liplow1Shape" -p "mouth_C0_liplow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "mouth_C0_liplow2Shape" -p "mouth_C0_liplow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "mouth_C0_liplow3Shape" -p "mouth_C0_liplow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "mouth_C0_liplow3_0crvShape" -p "mouth_C0_liplow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "mouth_C0_liplow3_1crvShape" -p "mouth_C0_liplow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "mouth_C0_C1_crv" -p "mouth_C0_liplow";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.036470396156167907 -28.656855854337159 -1.8209628902062949 ;
	setAttr ".r" -type "double3" -1.2722218725854061e-014 -2.5444437451708197e-014 -7.0167092985348878e-015 ;
	setAttr ".s" -type "double3" 1.742344680788962 1.7423446807889622 1.7423446807889635 ;
createNode nurbsCurve -n "mouth_C0_C1_crvShape" -p "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "mouth_C0_C1_crvShapeOrig" -p "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "mouth_C0_jaw" -p "mouth_C0_root";
	setAttr ".t" -type "double3" 0.0081921451162980281 -1.0149178968545485 1.7673469115179634 ;
	setAttr ".r" -type "double3" -1.2722218725854047e-014 -2.5444437451708213e-014 -7.0167092985348957e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999944 1.0000000000000002 0.99999999999999922 ;
createNode nurbsCurve -n "mouth_C0_jawShape" -p "mouth_C0_jaw";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "mouth_C0_jaw1Shape" -p "mouth_C0_jaw";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "mouth_C0_jaw2Shape" -p "mouth_C0_jaw";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "mouth_C0_jaw3Shape" -p "mouth_C0_jaw";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "mouth_C0_jaw3_0crvShape" -p "mouth_C0_jaw";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "mouth_C0_jaw3_1crvShape" -p "mouth_C0_jaw";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "mouth_C0_crv" -p "mouth_C0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.03064055525309933 -29.167034475402062 -0.063154612101841076 ;
	setAttr ".r" -type "double3" -1.2722218725854047e-014 -2.5444437451708207e-014 -7.0167092985348957e-015 ;
	setAttr ".s" -type "double3" 1.7423446807889607 1.7423446807889633 1.7423446807889631 ;
createNode nurbsCurve -n "mouth_C0_crvShape" -p "mouth_C0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "mouth_C0_crvShapeOrig" -p "mouth_C0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "mouth_C0_crv1" -p "mouth_C0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.03064055525309933 -29.167034475402062 -0.063154612101841076 ;
	setAttr ".r" -type "double3" -1.2722218725854047e-014 -2.5444437451708207e-014 -7.0167092985348957e-015 ;
	setAttr ".s" -type "double3" 1.7423446807889607 1.7423446807889633 1.7423446807889631 ;
createNode nurbsCurve -n "mouth_C0_crv1Shape" -p "mouth_C0_crv1";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "mouth_C0_crv1ShapeOrig" -p "mouth_C0_crv1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "eye_L0_root" -p "neck_C0_head";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	setAttr ".t" -type "double3" -1.4831894435008808 0.80017868414994453 0.47784925549240875 ;
	setAttr ".r" -type "double3" -1.3360063179915759e-029 -89.999999999999986 0 ;
	setAttr ".comp_type" -type "string" "eye_01";
	setAttr ".comp_name" -type "string" "eye";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "faceUI_C0_root";
	setAttr ".ikrefarray" -type "string" "eyeslook_C0_root";
createNode nurbsCurve -n "eye_L0_rootShape" -p "eye_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "eye_L0_root1Shape" -p "eye_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "eye_L0_root2Shape" -p "eye_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "eye_L0_root3Shape" -p "eye_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "eye_L0_look" -p "eye_L0_root";
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -1.0658141036401503e-014 
		3.7697842257179186 ;
	setAttr ".r" -type "double3" -1.2722218725854001e-014 -2.5444437451708087e-014 -7.0167092985348862e-015 ;
	setAttr ".s" -type "double3" 0.999999999999999 1 0.99999999999999989 ;
createNode nurbsCurve -n "eye_L0_lookShape" -p "eye_L0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "eye_L0_look1Shape" -p "eye_L0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "eye_L0_look2Shape" -p "eye_L0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "eye_L0_look3Shape" -p "eye_L0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "eye_L0_look3_0crvShape" -p "eye_L0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "eye_L0_look3_1crvShape" -p "eye_L0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "eye_L0_crv" -p "eye_L0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.53417509053888101 -28.788246120628177 -1.2732465848787988 ;
	setAttr ".r" -type "double3" -1.2722218725854002e-014 -2.5444437451708084e-014 -7.0167092985348831e-015 ;
	setAttr ".s" -type "double3" 1.6711445512932537 1.6711445512932535 1.6711445512932475 ;
createNode nurbsCurve -n "eye_L0_crvShape" -p "eye_L0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "eye_L0_crvShapeOrig" -p "eye_L0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "eye_R0_root" -p "neck_C0_head";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	setAttr ".t" -type "double3" -1.483 0.80017868414994453 -0.59050092558534806 ;
	setAttr ".r" -type "double3" -1.3360063179915759e-029 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".comp_type" -type "string" "eye_01";
	setAttr ".comp_name" -type "string" "eye";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "faceUI_C0_root";
	setAttr ".ikrefarray" -type "string" "eyeslook_C0_root";
createNode nurbsCurve -n "eye_R0_rootShape" -p "eye_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "eye_R0_root1Shape" -p "eye_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "eye_R0_root2Shape" -p "eye_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "eye_R0_root3Shape" -p "eye_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "eye_R0_look" -p "eye_R0_root";
	setAttr ".t" -type "double3" -8.8817841970012523e-016 -2.4868995751603507e-014 
		3.7697842257179204 ;
	setAttr ".r" -type "double3" -1.2722218725854007e-014 -2.5444437451708077e-014 -7.0167092985348815e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999911 0.99999999999999978 0.99999999999999967 ;
createNode nurbsCurve -n "eye_R0_lookShape" -p "eye_R0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "eye_R0_look1Shape" -p "eye_R0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "eye_R0_look2Shape" -p "eye_R0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "eye_R0_look3Shape" -p "eye_R0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "eye_R0_look3_0crvShape" -p "eye_R0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "eye_R0_look3_1crvShape" -p "eye_R0_look";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "eye_R0_crv" -p "eye_R0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0.53417509053887569 -28.788246120628177 -1.2730571413779181 ;
	setAttr ".r" -type "double3" -1.2722218725854007e-014 -2.5444437451708077e-014 -7.0167092985348815e-015 ;
	setAttr ".s" -type "double3" 1.6711445512932541 1.6711445512932535 1.6711445512932479 ;
createNode nurbsCurve -n "eye_R0_crvShape" -p "eye_R0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "eye_R0_crvShapeOrig" -p "eye_R0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "eyeslook_C0_root" -p "neck_C0_head";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	addAttr -ci true -sn "shadow" -ln "shadow" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "default_rotorder" -ln "default_rotorder" -min 0 -max 5 -at "long";
	setAttr ".t" -type "double3" -5.2720901263085063 0.79537474685831455 -0.056325835046470506 ;
	setAttr ".r" -type "double3" -1.3360063179915756e-029 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.671144551293259 1.671144551293255 1.6711445512932506 ;
	setAttr ".comp_type" -type "string" "control_02";
	setAttr ".comp_name" -type "string" "eyeslook";
	setAttr ".comp_side" -type "string" "C";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "faceUI_C0_root";
	setAttr ".icon" -type "string" "square";
	setAttr ".ikrefarray" -type "string" "neck_C0_head,local_C0_root,body_C0_root,spine_C0_eff";
	setAttr ".k_tx" yes;
	setAttr ".k_ty" yes;
	setAttr ".k_tz" yes;
	setAttr ".k_ro" yes;
	setAttr ".k_rx" yes;
	setAttr ".k_ry" yes;
	setAttr ".k_rz" yes;
	setAttr ".k_sx" yes;
	setAttr ".k_sy" yes;
	setAttr ".k_sz" yes;
createNode nurbsCurve -n "eyeslook_C0_rootShape" -p "eyeslook_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "eyeslook_C0_root1Shape" -p "eyeslook_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "eyeslook_C0_root2Shape" -p "eyeslook_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "eyeslook_C0_root3Shape" -p "eyeslook_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "neck_C0_tan1" -p "neck_C0_neck";
	setAttr ".t" -type "double3" 3.0531133177191805e-016 -0.41850601712122426 2.0816681711721685e-017 ;
	setAttr ".r" -type "double3" 7.0167092985348752e-015 -2.5444437451708229e-014 -1.4918085623982732e-029 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1.0000000000000002 ;
createNode nurbsCurve -n "neck_C0_tanShape1" -p "neck_C0_tan1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "neck_C0_tanShape4" -p "neck_C0_tan1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "neck_C0_tanShape5" -p "neck_C0_tan1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "neck_C0_tanShape6" -p "neck_C0_tan1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_tan2_0crvShape1" -p "neck_C0_tan1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_tan2_1crvShape1" -p "neck_C0_tan1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "neck_C0_head_crv" -p "neck_C0_neck";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.20994285862208203 -27.888067436478234 -0.056325835046472664 ;
	setAttr ".r" -type "double3" -1.3360063179915759e-029 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.6711445512932543 1.671144551293253 1.6711445512932479 ;
createNode nurbsCurve -n "neck_C0_head_crvShape" -p "neck_C0_head_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "neck_C0_head_crvShapeOrig" -p "neck_C0_head_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "neck_C0_tan0" -p "neck_C0_root";
	setAttr ".t" -type "double3" -0.069051314258821384 0.43071127027696576 -2.7755575615628914e-017 ;
	setAttr ".r" -type "double3" 7.0167092985348807e-015 -2.5444437451708254e-014 -1.491808562398274e-029 ;
	setAttr ".s" -type "double3" 0.999999999999999 0.99999999999999922 0.99999999999999978 ;
createNode nurbsCurve -n "neck_C0_tanShape0" -p "neck_C0_tan0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "neck_C0_tanShape1" -p "neck_C0_tan0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "neck_C0_tanShape2" -p "neck_C0_tan0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "neck_C0_tanShape3" -p "neck_C0_tan0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_tan1_0crvShape0" -p "neck_C0_tan0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "neck_C0_tan1_1crvShape0" -p "neck_C0_tan0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "neck_C0_blade" -p "neck_C0_root";
	setAttr ".s" -type "double3" 1.6711445512932543 1.6711445512932519 1.6711445512932466 ;
createNode nurbsCurve -n "neck_C0_bladeShape" -p "neck_C0_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.35903536862545982 0 0
		0 0.11967845620848661 0
		0 0 0
		;
createNode aimConstraint -n "neck_C0_blade_aimConstraint1" -p "neck_C0_blade";
	addAttr -ci true -sn "w0" -ln "neck_C1_tan0W0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -7.2920380833476326 -0.12338324311047354 89.035871767502854 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "neck_C0_blade_pointConstraint1" -p "neck_C0_blade";
	addAttr -ci true -k true -sn "w0" -ln "neck_C1_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.1102230246251565e-016 -3.5527136788005009e-015 
		0 ;
	setAttr -k on ".w0";
createNode transform -n "neck_C0_neck_crv" -p "neck_C0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.50916522041030332 -25.72171156809441 -0.056325835046472622 ;
	setAttr ".r" -type "double3" -1.3360063179915759e-029 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.6711445512932543 1.6711445512932519 1.6711445512932466 ;
createNode nurbsCurve -n "neck_C0_neck_crvShape" -p "neck_C0_neck_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "neck_C0_neck_crvShapeOrig" -p "neck_C0_neck_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 1 0 no 3
		6 0 0 0 1 1 1
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "spineUI_C0_root" -p "spine_C0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.761552665481636 -0.025771020942814887 -2.8297085603095362 ;
	setAttr ".r" -type "double3" 90.000000000000014 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 0.88775990675476169 0.88775990675475958 0.88775990675475902 ;
	setAttr ".comp_type" -type "string" "control_01";
	setAttr ".comp_name" -type "string" "spineUI";
	setAttr ".comp_side" -type "string" "C";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".icon" -type "string" "cube";
createNode nurbsCurve -n "spineUI_C0_rootShape" -p "spineUI_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "spineUI_C0_root1Shape" -p "spineUI_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "spineUI_C0_root2Shape" -p "spineUI_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "spineUI_C0_root3Shape" -p "spineUI_C0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "shoulder_R0_root" -p "spine_C0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "refArray" -ln "refArray" -dt "string";
	setAttr ".t" -type "double3" 1.8665008412384356 -0.41852659998613168 0.11673327753264968 ;
	setAttr ".r" -type "double3" 3.8165403979891624e-014 1.2900712135558961e-014 179.19611077184621 ;
	setAttr ".s" -type "double3" 1 1 -0.999999999999999 ;
	setAttr ".comp_type" -type "string" "shoulder_01";
	setAttr ".comp_name" -type "string" "shoulder";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "armUI_R0_root";
	setAttr ".refArray" -type "string" "shoulder_R0_root,local_C0_root,body_C0_root,spine_C0_eff";
createNode nurbsCurve -n "shoulder_R0_rootShape" -p "shoulder_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "shoulder_R0_root1Shape" -p "shoulder_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "shoulder_R0_root2Shape" -p "shoulder_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "shoulder_R0_root3Shape" -p "shoulder_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "shoulder_R0_tip" -p "shoulder_R0_root";
	setAttr ".t" -type "double3" 0.19622949105844967 -0.65582077355763091 -1.8410072078621214 ;
	setAttr ".r" -type "double3" -3.8165403979891662e-014 -1.2900712135558975e-014 -1.6896696745274926e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999845 0.99999999999999922 ;
createNode nurbsCurve -n "shoulder_R0_tipShape" -p "shoulder_R0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "shoulder_R0_tip1Shape" -p "shoulder_R0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "shoulder_R0_tip2Shape" -p "shoulder_R0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "shoulder_R0_tip3Shape" -p "shoulder_R0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "shoulder_R0_tip3_0crvShape" -p "shoulder_R0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "shoulder_R0_tip3_1crvShape" -p "shoulder_R0_tip";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "arm_R0_root" -p "shoulder_R0_tip";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	addAttr -ci true -sn "upvrefarray" -ln "upvrefarray" -dt "string";
	addAttr -ci true -sn "maxstretch" -ln "maxstretch" -dv 1 -min 1 -at "double";
	addAttr -ci true -sn "div0" -ln "div0" -dv 1 -min 1 -at "long";
	addAttr -ci true -sn "div1" -ln "div1" -dv 1 -min 1 -at "long";
	addAttr -ci true -k true -sn "st_profile" -ln "st_profile" -at "double";
	addAttr -ci true -k true -sn "sq_profile" -ln "sq_profile" -at "double";
	setAttr ".t" -type "double3" 1.7763568394002505e-015 -2.7755575615628914e-016 -8.8817841970012523e-016 ;
	setAttr ".r" -type "double3" -96.307283307451399 48.240718341633134 -6.0629030460036306 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999911 1.0000000000000007 ;
	setAttr ".comp_type" -type "string" "arm_2jnt_01";
	setAttr ".comp_name" -type "string" "arm";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "armUI_R0_root";
	setAttr ".ikrefarray" -type "string" "local_C0_root,body_C0_root,spine_C0_eff,spine_C0_root";
	setAttr ".upvrefarray" -type "string" "local_C0_root,body_C0_root,spine_C0_eff,spine_C0_root";
	setAttr ".maxstretch" 1.5;
	setAttr ".div0" 5;
	setAttr ".div1" 5;
	setAttr -k on ".st_profile";
	setAttr -k on ".sq_profile";
createNode nurbsCurve -n "arm_R0_rootShape" -p "arm_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "arm_R0_root1Shape" -p "arm_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "arm_R0_root2Shape" -p "arm_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "arm_R0_root3Shape" -p "arm_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "arm_R0_elbow" -p "arm_R0_root";
	setAttr ".t" -type "double3" 2.6990161750824146 0.042436136907152644 -0.096150543189835291 ;
	setAttr ".r" -type "double3" -2.0229460141836278e-015 -10.688700162784265 2.9180996254598823e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999956 0.99999999999999978 ;
createNode nurbsCurve -n "arm_R0_elbowShape" -p "arm_R0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "arm_R0_elbow1Shape" -p "arm_R0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "arm_R0_elbow2Shape" -p "arm_R0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "arm_R0_elbow3Shape" -p "arm_R0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_R0_elbow3_0crvShape" -p "arm_R0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_R0_elbow3_1crvShape" -p "arm_R0_elbow";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "arm_R0_wrist" -p "arm_R0_elbow";
	setAttr ".t" -type "double3" 2.7014894887411964 -0.078016774483522511 -0.050560325901133074 ;
	setAttr ".r" -type "double3" -3.5184886163690139e-014 5.5659706925611528e-015 2.2860236773019021e-014 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1 ;
createNode nurbsCurve -n "arm_R0_wristShape" -p "arm_R0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "arm_R0_wrist1Shape" -p "arm_R0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "arm_R0_wrist2Shape" -p "arm_R0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "arm_R0_wrist3Shape" -p "arm_R0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_R0_wrist3_0crvShape" -p "arm_R0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_R0_wrist3_1crvShape" -p "arm_R0_wrist";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "arm_R0_eff" -p "arm_R0_wrist";
	setAttr ".t" -type "double3" 1.106599940944399 -0.036487063347861337 -0.078498699984430875 ;
	setAttr ".r" -type "double3" -3.3992178158141344e-014 7.1562480332929104e-015 -8.9453100416161372e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999978 1.0000000000000004 ;
createNode nurbsCurve -n "arm_R0_effShape" -p "arm_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "arm_R0_eff1Shape" -p "arm_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "arm_R0_eff2Shape" -p "arm_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "arm_R0_eff3Shape" -p "arm_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_R0_eff3_0crvShape" -p "arm_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "arm_R0_eff3_1crvShape" -p "arm_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "thumb_R0_root" -p "arm_R0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.73075326583805289 -0.084115438475205195 0.23252932764826872 ;
	setAttr ".r" -type "double3" -4.9001807703589291e-014 -46.280259360575769 3.8361415173667063e-014 ;
	setAttr ".s" -type "double3" 0.51359190808549093 0.51359190808549127 0.51359190808549104 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "thumb";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "thumb_R0_rootShape" -p "thumb_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "thumb_R0_root1Shape" -p "thumb_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "thumb_R0_root2Shape" -p "thumb_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "thumb_R0_root3Shape" -p "thumb_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "thumb_R0_0_loc" -p "thumb_R0_root";
	setAttr ".t" -type "double3" 1 3.5527136788005009e-015 -1.3322676295501878e-015 ;
	setAttr ".r" -type "double3" -1.4421827633761136e-013 -9.5416640443905535e-015 3.5980024834056038e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
createNode nurbsCurve -n "thumb_R0_0_locShape" -p "thumb_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "thumb_R0_0_loc1Shape" -p "thumb_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "thumb_R0_0_loc2Shape" -p "thumb_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "thumb_R0_0_loc3Shape" -p "thumb_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_R0_0_loc3_0crvShape" -p "thumb_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_R0_0_loc3_1crvShape" -p "thumb_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "thumb_R0_1_loc" -p "thumb_R0_0_loc";
	setAttr ".t" -type "double3" 1.0000000000000007 -3.5527136788005009e-015 3.9968028886505635e-015 ;
	setAttr ".r" -type "double3" -1.3457721995942507e-013 -9.5416640443905503e-015 2.3655375443384907e-014 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 0.99999999999999989 ;
createNode nurbsCurve -n "thumb_R0_1_locShape" -p "thumb_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "thumb_R0_1_loc1Shape" -p "thumb_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "thumb_R0_1_loc2Shape" -p "thumb_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "thumb_R0_1_loc3Shape" -p "thumb_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_R0_1_loc3_0crvShape" -p "thumb_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_R0_1_loc3_1crvShape" -p "thumb_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "thumb_R0_2_loc" -p "thumb_R0_1_loc";
	setAttr ".t" -type "double3" 0.99999999999999889 7.1054273576010019e-015 -3.5527136788005009e-015 ;
	setAttr ".r" -type "double3" -1.5057938570053838e-013 6.3611093629270351e-015 2.584200678689108e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
createNode nurbsCurve -n "thumb_R0_2_locShape" -p "thumb_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "thumb_R0_2_loc1Shape" -p "thumb_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "thumb_R0_2_loc2Shape" -p "thumb_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "thumb_R0_2_loc3Shape" -p "thumb_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_R0_2_loc3_0crvShape" -p "thumb_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "thumb_R0_2_loc3_1crvShape" -p "thumb_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "thumb_R0_blade" -p "thumb_R0_root";
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
createNode nurbsCurve -n "thumb_R0_bladeShape" -p "thumb_R0_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.30815514485129453 0 0
		0 0.10271838161709818 0
		0 0 0
		;
createNode aimConstraint -n "thumb_R0_blade_aimConstraint1" -p "thumb_R0_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "thumb_R0_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "thumb_R0_blade_pointConstraint1" -p "thumb_R0_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "thumb_R0_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 8.8817841970012523e-016 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "thumb_R0_crv" -p "thumb_R0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 2.598714865786369 -23.249749444068133 -3.7534999914429417 ;
	setAttr ".r" -type "double3" 138.80494631611683 -35.838503727314631 -125.25686269567345 ;
	setAttr ".s" -type "double3" 1.9470711751041545 1.9470711751041481 -1.9470711751041503 ;
createNode nurbsCurve -n "thumb_R0_crvShape" -p "thumb_R0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "thumb_R0_crvShapeOrig" -p "thumb_R0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "finger_R0_root" -p "arm_R0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031148103377456327 3.5527136788005009e-015 0.45446815993077583 ;
	setAttr ".r" -type "double3" -3.5146999896779177e-014 4.9853900562807381e-015 1.1372921065890435e-014 ;
	setAttr ".s" -type "double3" 0.39873443270709613 0.39873443270709635 0.3987344327070963 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "finger";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "finger_R0_rootShape" -p "finger_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R0_root1Shape" -p "finger_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R0_root2Shape" -p "finger_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R0_root3Shape" -p "finger_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "finger_R0_0_loc" -p "finger_R0_root";
	setAttr ".t" -type "double3" 0.99999999999999778 0 -2.2204460492503131e-016 ;
	setAttr ".r" -type "double3" -3.538367083128164e-014 5.5659706925611543e-015 3.8017567676868597e-015 ;
	setAttr ".s" -type "double3" 1 0.99999999999999956 0.99999999999999989 ;
createNode nurbsCurve -n "finger_R0_0_locShape" -p "finger_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R0_0_loc1Shape" -p "finger_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R0_0_loc2Shape" -p "finger_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R0_0_loc3Shape" -p "finger_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R0_0_loc3_0crvShape" -p "finger_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R0_0_loc3_1crvShape" -p "finger_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R0_1_loc" -p "finger_R0_0_loc";
	setAttr ".t" -type "double3" 1.0000000000000002 0 4.4408920985006262e-016 ;
	setAttr ".r" -type "double3" -3.4986101496098681e-014 4.7708320221952767e-015 1.3368268895526346e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999989 ;
createNode nurbsCurve -n "finger_R0_1_locShape" -p "finger_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R0_1_loc1Shape" -p "finger_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R0_1_loc2Shape" -p "finger_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R0_1_loc3Shape" -p "finger_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R0_1_loc3_0crvShape" -p "finger_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R0_1_loc3_1crvShape" -p "finger_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R0_2_loc" -p "finger_R0_1_loc";
	setAttr ".t" -type "double3" 0.99999999999999944 3.5527136788005009e-015 2.2204460492503131e-016 ;
	setAttr ".r" -type "double3" -3.6178809501647495e-014 3.975693351829396e-015 1.9779074425351246e-014 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R0_2_locShape" -p "finger_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R0_2_loc1Shape" -p "finger_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R0_2_loc2Shape" -p "finger_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R0_2_loc3Shape" -p "finger_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R0_2_loc3_0crvShape" -p "finger_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R0_2_loc3_1crvShape" -p "finger_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R0_blade" -p "finger_R0_root";
	setAttr ".s" -type "double3" 1 0.99999999999999956 0.99999999999999989 ;
createNode nurbsCurve -n "finger_R0_bladeShape" -p "finger_R0_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.23924065962425767 0 0
		0 0.079746886541419218 0
		0 0 0
		;
createNode aimConstraint -n "finger_R0_blade_aimConstraint1" -p "finger_R0_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "finger_R0_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "finger_R0_blade_pointConstraint1" -p "finger_R0_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "finger_R0_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.3322676295501878e-015 0 -4.4408920985006262e-016 ;
	setAttr -k on ".w0";
createNode transform -n "finger_R0_crv" -p "finger_R0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 3.8968057566131966 -30.157913717968551 -1.4788493414315405 ;
	setAttr ".r" -type "double3" 175.06620644642709 -3.8104210806425574 -138.43741116375887 ;
	setAttr ".s" -type "double3" 2.5079349009584671 2.5079349009584595 -2.5079349009584608 ;
createNode nurbsCurve -n "finger_R0_crvShape" -p "finger_R0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "finger_R0_crvShapeOrig" -p "finger_R0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "finger_R1_root" -p "arm_R0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031148103377456993 8.8817841970012523e-015 0.11942617708530534 ;
	setAttr ".r" -type "double3" -3.4897730393965151e-014 4.9853900562807381e-015 1.1372921065890435e-014 ;
	setAttr ".s" -type "double3" 0.39873443270709613 0.39873443270709624 0.39873443270709624 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "finger";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".comp_index" 1;
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "finger_R1_rootShape" -p "finger_R1_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R1_root1Shape" -p "finger_R1_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R1_root2Shape" -p "finger_R1_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R1_root3Shape" -p "finger_R1_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "finger_R1_0_loc" -p "finger_R1_root";
	setAttr ".t" -type "double3" 0.99999999999999822 -3.5527136788005009e-015 -1.1102230246251565e-016 ;
	setAttr ".r" -type "double3" -3.478731682850723e-014 5.5659706925611559e-015 -2.4102640945465721e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999967 0.99999999999999978 ;
createNode nurbsCurve -n "finger_R1_0_locShape" -p "finger_R1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R1_0_loc1Shape" -p "finger_R1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R1_0_loc2Shape" -p "finger_R1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R1_0_loc3Shape" -p "finger_R1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R1_0_loc3_0crvShape" -p "finger_R1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R1_0_loc3_1crvShape" -p "finger_R1_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R1_1_loc" -p "finger_R1_0_loc";
	setAttr ".t" -type "double3" 1.0000000000000027 -3.5527136788005009e-015 -1.1102230246251565e-016 ;
	setAttr ".r" -type "double3" -3.5184886163690151e-014 4.7708320221952736e-015 3.8017567676868589e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000004 ;
createNode nurbsCurve -n "finger_R1_1_locShape" -p "finger_R1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R1_1_loc1Shape" -p "finger_R1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R1_1_loc2Shape" -p "finger_R1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R1_1_loc3Shape" -p "finger_R1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R1_1_loc3_0crvShape" -p "finger_R1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R1_1_loc3_1crvShape" -p "finger_R1_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R1_2_loc" -p "finger_R1_1_loc";
	setAttr ".t" -type "double3" 1.0000000000000013 -3.5527136788005009e-015 -1.1102230246251565e-016 ;
	setAttr ".r" -type "double3" -3.4787316828507211e-014 3.1805546814635183e-015 1.9853618675698055e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999967 1.0000000000000002 0.99999999999999978 ;
createNode nurbsCurve -n "finger_R1_2_locShape" -p "finger_R1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R1_2_loc1Shape" -p "finger_R1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R1_2_loc2Shape" -p "finger_R1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R1_2_loc3Shape" -p "finger_R1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R1_2_loc3_0crvShape" -p "finger_R1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R1_2_loc3_1crvShape" -p "finger_R1_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R1_blade" -p "finger_R1_root";
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999967 0.99999999999999978 ;
createNode nurbsCurve -n "finger_R1_bladeShape" -p "finger_R1_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.23924065962425767 0 0
		0 0.079746886541419218 0
		0 0 0
		;
createNode aimConstraint -n "finger_R1_blade_aimConstraint1" -p "finger_R1_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "finger_R1_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "finger_R1_blade_pointConstraint1" -p "finger_R1_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "finger_R1_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.6645352591003757e-015 3.5527136788005009e-015 -5.5511151231257827e-016 ;
	setAttr -k on ".w0";
createNode transform -n "finger_R1_crv" -p "finger_R1_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 3.8968057566131957 -30.157913717968576 -0.63858585936705892 ;
	setAttr ".r" -type "double3" 175.06620644642709 -3.8104210806425565 -138.43741116375887 ;
	setAttr ".s" -type "double3" 2.5079349009584675 2.5079349009584599 -2.5079349009584608 ;
createNode nurbsCurve -n "finger_R1_crvShape" -p "finger_R1_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "finger_R1_crvShapeOrig" -p "finger_R1_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "finger_R2_root" -p "arm_R0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031148103377459213 8.8817841970012523e-015 -0.21759175506122963 ;
	setAttr ".r" -type "double3" -3.5146999896779196e-014 4.9853900562807381e-015 1.1372921065890435e-014 ;
	setAttr ".s" -type "double3" 0.39873443270709613 0.39873443270709619 0.39873443270709619 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "finger";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".comp_index" 2;
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "finger_R2_rootShape" -p "finger_R2_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R2_root1Shape" -p "finger_R2_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R2_root2Shape" -p "finger_R2_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R2_root3Shape" -p "finger_R2_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "finger_R2_0_loc" -p "finger_R2_root";
	setAttr ".t" -type "double3" 0.99999999999999289 -7.1054273576010019e-015 -2.4980018054066022e-016 ;
	setAttr ".r" -type "double3" -3.598002483405605e-014 4.7708320221952767e-015 -2.5345045117912405e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999956 1 ;
createNode nurbsCurve -n "finger_R2_0_locShape" -p "finger_R2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R2_0_loc1Shape" -p "finger_R2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R2_0_loc2Shape" -p "finger_R2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R2_0_loc3Shape" -p "finger_R2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R2_0_loc3_0crvShape" -p "finger_R2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R2_0_loc3_1crvShape" -p "finger_R2_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R2_1_loc" -p "finger_R2_0_loc";
	setAttr ".t" -type "double3" 1.0000000000000102 7.1054273576010019e-015 -1.3877787807814457e-016 ;
	setAttr ".r" -type "double3" -3.5184886163690151e-014 4.7708320221952752e-015 7.2307922836397132e-015 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R2_1_locShape" -p "finger_R2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R2_1_loc1Shape" -p "finger_R2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R2_1_loc2Shape" -p "finger_R2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R2_1_loc3Shape" -p "finger_R2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R2_1_loc3_0crvShape" -p "finger_R2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R2_1_loc3_1crvShape" -p "finger_R2_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R2_2_loc" -p "finger_R2_1_loc";
	setAttr ".t" -type "double3" 0.99999999999999167 -7.1054273576010019e-015 1.6653345369377348e-016 ;
	setAttr ".r" -type "double3" -3.4588532160915748e-014 3.9756933518293967e-015 1.0485891215450034e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode nurbsCurve -n "finger_R2_2_locShape" -p "finger_R2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R2_2_loc1Shape" -p "finger_R2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R2_2_loc2Shape" -p "finger_R2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R2_2_loc3Shape" -p "finger_R2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R2_2_loc3_0crvShape" -p "finger_R2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R2_2_loc3_1crvShape" -p "finger_R2_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R2_blade" -p "finger_R2_root";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999956 1 ;
createNode nurbsCurve -n "finger_R2_bladeShape" -p "finger_R2_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.23924065962425767 0 0
		0 0.079746886541419218 0
		0 0 0
		;
createNode aimConstraint -n "finger_R2_blade_aimConstraint1" -p "finger_R2_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "finger_R2_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "finger_R2_blade_pointConstraint1" -p "finger_R2_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "finger_R2_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 8.8817841970012523e-016 0 -2.4980018054066022e-016 ;
	setAttr -k on ".w0";
createNode transform -n "finger_R2_crv" -p "finger_R2_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 3.8968057566131891 -30.157913717968576 0.2066331749120873 ;
	setAttr ".r" -type "double3" 175.06620644642709 -3.8104210806425574 -138.43741116375887 ;
	setAttr ".s" -type "double3" 2.5079349009584679 2.5079349009584599 -2.5079349009584613 ;
createNode nurbsCurve -n "finger_R2_crvShape" -p "finger_R2_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "finger_R2_crvShapeOrig" -p "finger_R2_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "finger_R3_root" -p "arm_R0_eff";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "mode" -ln "mode" -min 0 -at "long";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "neutralpose" -ln "neutralpose" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031148103377459879 1.0658141036401503e-014 -0.53213129072633791 ;
	setAttr ".r" -type "double3" -3.539626939959324e-014 4.9853900562807381e-015 1.1372921065890435e-014 ;
	setAttr ".s" -type "double3" 0.39873443270709613 0.39873443270709613 0.39873443270709613 ;
	setAttr ".comp_type" -type "string" "chain_01";
	setAttr ".comp_name" -type "string" "finger";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".comp_index" 3;
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".blend" 1;
createNode nurbsCurve -n "finger_R3_rootShape" -p "finger_R3_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R3_root1Shape" -p "finger_R3_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R3_root2Shape" -p "finger_R3_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R3_root3Shape" -p "finger_R3_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "finger_R3_0_loc" -p "finger_R3_root";
	setAttr ".t" -type "double3" 0.99999999999998979 -1.4210854715202004e-014 2.2204460492503131e-016 ;
	setAttr ".r" -type "double3" -3.6576378836830448e-014 4.7708320221952775e-015 1.0361650798205367e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999978 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R3_0_locShape" -p "finger_R3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R3_0_loc1Shape" -p "finger_R3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R3_0_loc2Shape" -p "finger_R3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R3_0_loc3Shape" -p "finger_R3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R3_0_loc3_0crvShape" -p "finger_R3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R3_0_loc3_1crvShape" -p "finger_R3_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R3_1_loc" -p "finger_R3_0_loc";
	setAttr ".t" -type "double3" 1.0000000000000089 0 -3.3306690738754696e-016 ;
	setAttr ".r" -type "double3" -3.6775163504421911e-014 7.1562480332929119e-015 6.957463365701441e-016 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R3_1_locShape" -p "finger_R3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R3_1_loc1Shape" -p "finger_R3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R3_1_loc2Shape" -p "finger_R3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R3_1_loc3Shape" -p "finger_R3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R3_1_loc3_0crvShape" -p "finger_R3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R3_1_loc3_1crvShape" -p "finger_R3_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R3_2_loc" -p "finger_R3_1_loc";
	setAttr ".t" -type "double3" 0.99999999999999656 -3.5527136788005009e-015 -1.1102230246251565e-016 ;
	setAttr ".r" -type "double3" -3.5582455498873091e-014 3.1805546814635168e-015 1.0311954631307497e-014 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode nurbsCurve -n "finger_R3_2_locShape" -p "finger_R3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "finger_R3_2_loc1Shape" -p "finger_R3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "finger_R3_2_loc2Shape" -p "finger_R3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "finger_R3_2_loc3Shape" -p "finger_R3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R3_2_loc3_0crvShape" -p "finger_R3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "finger_R3_2_loc3_1crvShape" -p "finger_R3_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "finger_R3_blade" -p "finger_R3_root";
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999978 1.0000000000000002 ;
createNode nurbsCurve -n "finger_R3_bladeShape" -p "finger_R3_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.23924065962425767 0 0
		0 0.079746886541419218 0
		0 0 0
		;
createNode aimConstraint -n "finger_R3_blade_aimConstraint1" -p "finger_R3_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "finger_R3_0_locW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr -k on ".w0";
createNode pointConstraint -n "finger_R3_blade_pointConstraint1" -p "finger_R3_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "finger_R3_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -2.2204460492503131e-015 -3.5527136788005009e-015 
		-4.4408920985006262e-016 ;
	setAttr -k on ".w0";
createNode transform -n "finger_R3_crv" -p "finger_R3_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 3.8968057566131868 -30.157913717968587 0.99547785413788192 ;
	setAttr ".r" -type "double3" 175.06620644642709 -3.8104210806425574 -138.43741116375887 ;
	setAttr ".s" -type "double3" 2.5079349009584679 2.5079349009584599 -2.5079349009584617 ;
createNode nurbsCurve -n "finger_R3_crvShape" -p "finger_R3_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "finger_R3_crvShapeOrig" -p "finger_R3_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "armUI_R0_root" -p "arm_R0_wrist";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.020154460490453996 0.56073114764517662 -0.29276117198398777 ;
	setAttr ".r" -type "double3" -3.4588532160915767e-014 7.1562480332929104e-015 -8.9453100416161372e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999933 1.0000000000000004 ;
	setAttr ".comp_type" -type "string" "control_01";
	setAttr ".comp_name" -type "string" "armUI";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".icon" -type "string" "cross";
createNode nurbsCurve -n "armUI_R0_rootShape" -p "armUI_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "armUI_R0_root1Shape" -p "armUI_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "armUI_R0_root2Shape" -p "armUI_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "armUI_R0_root3Shape" -p "armUI_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "arm_R0_crv" -p "arm_R0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 8.0474849025393898 -12.09706631888797 0.64443564200118919 ;
	setAttr ".r" -type "double3" 2.1534408611046545 175.8040629206634 41.586520940562629 ;
	setAttr ".s" -type "double3" 1.000000000000002 0.99999999999999833 -0.99999999999999978 ;
createNode nurbsCurve -n "arm_R0_crvShape" -p "arm_R0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "arm_R0_crvShapeOrig" -p "arm_R0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "shoulder_R0_blade" -p "shoulder_R0_root";
	setAttr ".s" -type "double3" 1.0000000000000016 0.99999999999999889 0.99999999999999911 ;
createNode nurbsCurve -n "shoulder_R0_bladeShape" -p "shoulder_R0_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.59999999999999998 0 0
		0 0.19999999999999998 0
		0 0 0
		;
createNode aimConstraint -n "shoulder_R0_blade_aimConstraint1" -p "shoulder_R0_blade";
	addAttr -dcb 0 -ci true -sn "w0" -ln "shoulder_R0_tipW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -72.295501616513164 69.603165003464781 -73.342182243856271 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "shoulder_R0_blade_pointConstraint1" -p "shoulder_R0_blade";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "shoulder_R0_rootW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.7763568394002505e-015 2.7755575615628914e-017 0 ;
	setAttr -k on ".w0";
createNode transform -n "shoulder_R0_crv" -p "shoulder_R0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 14.600411841401034 -0.20514673749567697 0.11673327753264995 ;
	setAttr ".r" -type "double3" 90.803889228153793 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000013 0.999999999999999 -0.99999999999999889 ;
createNode nurbsCurve -n "shoulder_R0_crvShape" -p "shoulder_R0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "shoulder_R0_crvShapeOrig" -p "shoulder_R0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "spine_C0_blade" -p "spine_C0_root";
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999956 0.99999999999999845 ;
createNode nurbsCurve -n "spine_C0_bladeShape" -p "spine_C0_blade";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 2 no 3
		4 0 1 2 3
		4
		0 0 0
		0.30713489785446313 0 0
		0 0.10237829928482105 0
		0 0 0
		;
createNode aimConstraint -n "spine_C0_blade_aimConstraint1" -p "spine_C0_blade";
	addAttr -ci true -sn "w0" -ln "spine_C1_effW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -2.0686593905763162e-019 -6.1558569930344794e-016 
		0.038508187569216132 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "spine_C0_blade_pointConstraint1" -p "spine_C0_blade";
	addAttr -ci true -k true -sn "w0" -ln "spine_C1_rootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.7763568394002505e-015 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "spine_C0_crv" -p "spine_C0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -10.353589031507754 -0.0085554946225435899 -1.3676677270614162e-016 ;
	setAttr ".r" -type "double3" 90.000000000000014 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999956 0.99999999999999845 ;
createNode nurbsCurve -n "spine_C0_crvShape" -p "spine_C0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "spine_C0_crvShapeOrig" -p "spine_C0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 0
		;
createNode transform -n "leg_L0_root" -p "spine_C0_root";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	addAttr -ci true -sn "upvrefarray" -ln "upvrefarray" -dt "string";
	addAttr -ci true -sn "maxstretch" -ln "maxstretch" -dv 1 -min 1 -at "double";
	addAttr -ci true -sn "div0" -ln "div0" -dv 1 -min 1 -at "long";
	addAttr -ci true -sn "div1" -ln "div1" -dv 1 -min 1 -at "long";
	addAttr -ci true -k true -sn "st_profile" -ln "st_profile" -at "double";
	addAttr -ci true -k true -sn "sq_profile" -ln "sq_profile" -at "double";
	setAttr ".t" -type "double3" -0.80615537849444507 0.20384773318317964 -0.83666277387714949 ;
	setAttr ".r" -type "double3" 6.3611093629270446e-015 1.4124500153760515e-030 -6.3611093629270406e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000013 0.99999999999999822 0.99999999999999967 ;
	setAttr ".comp_type" -type "string" "leg_2jnt_01";
	setAttr ".comp_name" -type "string" "leg";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "legUI_L0_root";
	setAttr ".blend" 1;
	setAttr ".ikrefarray" -type "string" "local_C0_root";
	setAttr ".upvrefarray" -type "string" "";
	setAttr ".maxstretch" 1.5;
	setAttr ".div0" 2;
	setAttr ".div1" 2;
	setAttr -k on ".st_profile";
	setAttr -k on ".sq_profile";
createNode nurbsCurve -n "leg_L0_rootShape" -p "leg_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "leg_L0_root1Shape" -p "leg_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "leg_L0_root2Shape" -p "leg_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "leg_L0_root3Shape" -p "leg_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "leg_L0_knee" -p "leg_L0_root";
	setAttr ".t" -type "double3" -4.1282487498381935 0.00071075503012346664 -2.6645352591003757e-015 ;
	setAttr ".r" -type "double3" 90.000000000000014 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999833 1.0000000000000013 ;
createNode nurbsCurve -n "leg_L0_kneeShape" -p "leg_L0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "leg_L0_knee1Shape" -p "leg_L0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "leg_L0_knee2Shape" -p "leg_L0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "leg_L0_knee3Shape" -p "leg_L0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_L0_knee3_0crvShape" -p "leg_L0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_L0_knee3_1crvShape" -p "leg_L0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "leg_L0_ankle" -p "leg_L0_knee";
	setAttr ".t" -type "double3" 0 -4.511462181044803 -0.1556862790032941 ;
	setAttr ".r" -type "double3" 3.6017475392089263e-029 -6.3611093629270209e-015 -1.2722218725854092e-014 ;
	setAttr ".s" -type "double3" 1.0000000000000009 1.0000000000000002 0.99999999999999922 ;
createNode nurbsCurve -n "leg_L0_ankleShape" -p "leg_L0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "leg_L0_ankle1Shape" -p "leg_L0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "leg_L0_ankle2Shape" -p "leg_L0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "leg_L0_ankle3Shape" -p "leg_L0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_L0_ankle3_0crvShape" -p "leg_L0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_L0_ankle3_1crvShape" -p "leg_L0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "leg_L0_eff" -p "leg_L0_ankle";
	setAttr ".t" -type "double3" -3.3306690738754696e-016 2.55351295663786e-015 2.1377206638691337 ;
	setAttr ".r" -type "double3" -1.2722218725854099e-014 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000013 0.99999999999999967 1.0000000000000009 ;
createNode nurbsCurve -n "leg_L0_effShape" -p "leg_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "leg_L0_eff1Shape" -p "leg_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "leg_L0_eff2Shape" -p "leg_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "leg_L0_eff3Shape" -p "leg_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_L0_eff3_0crvShape" -p "leg_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_L0_eff3_1crvShape" -p "leg_L0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_L0_root" -p "leg_L0_ankle";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "roll" -ln "roll" -min 0 -max 1 -at "long";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 7.7715611723760958e-016 -5.5511151231257827e-017 ;
	setAttr ".r" -type "double3" -1.2722218725854099e-014 -89.999999999999986 0 ;
	setAttr ".comp_type" -type "string" "foot_bk_01";
	setAttr ".comp_name" -type "string" "foot";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "leg_2jnt_01";
	setAttr ".ui_host" -type "string" "legUI_L0_root";
createNode nurbsCurve -n "foot_L0_rootShape" -p "foot_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_L0_root1Shape" -p "foot_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_L0_root2Shape" -p "foot_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_L0_root3Shape" -p "foot_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "foot_L0_0_loc" -p "foot_L0_root";
	setAttr ".t" -type "double3" 0.91922044729120567 -0.60341075628672103 -2.2204460492503131e-016 ;
	setAttr ".r" -type "double3" 2.2316492133303003e-014 9.9642276506885299e-016 -22.490244653448048 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999956 ;
createNode nurbsCurve -n "foot_L0_0_locShape" -p "foot_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_L0_0_loc1Shape" -p "foot_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_L0_0_loc2Shape" -p "foot_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_L0_0_loc3Shape" -p "foot_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_0_loc3_0crvShape" -p "foot_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_0_loc3_1crvShape" -p "foot_L0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_L0_1_loc" -p "foot_L0_0_loc";
	setAttr ".t" -type "double3" 0.64158599405857042 0.087004870626030018 1.1102230246251565e-016 ;
	setAttr ".r" -type "double3" -1.908332808878106e-014 -5.6498000615042044e-030 22.490244653448041 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode nurbsCurve -n "foot_L0_1_locShape" -p "foot_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_L0_1_loc1Shape" -p "foot_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_L0_1_loc2Shape" -p "foot_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_L0_1_loc3Shape" -p "foot_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_1_loc3_0crvShape" -p "foot_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_1_loc3_1crvShape" -p "foot_L0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_L0_2_loc" -p "foot_L0_1_loc";
	setAttr ".t" -type "double3" 0.53459438469900267 -0.094336926401681065 0 ;
	setAttr ".r" -type "double3" -5.6498000615042177e-030 89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1.0000000000000002 ;
createNode nurbsCurve -n "foot_L0_2_locShape" -p "foot_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_L0_2_loc1Shape" -p "foot_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_L0_2_loc2Shape" -p "foot_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_L0_2_loc3Shape" -p "foot_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_2_loc3_0crvShape" -p "foot_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_2_loc3_1crvShape" -p "foot_L0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "legUI_L0_root" -p "foot_L0_0_loc";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.2629167783492141 0.98171302400523941 -1.5009992656912388 ;
	setAttr ".r" -type "double3" -22.490244653448045 89.999999999999972 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000009 1 ;
	setAttr ".comp_type" -type "string" "control_01";
	setAttr ".comp_name" -type "string" "legUI";
	setAttr ".comp_side" -type "string" "L";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".icon" -type "string" "cross";
createNode nurbsCurve -n "legUI_L0_rootShape" -p "legUI_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "legUI_L0_root1Shape" -p "legUI_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "legUI_L0_root2Shape" -p "legUI_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "legUI_L0_root3Shape" -p "legUI_L0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "foot_L0_crv" -p "foot_L0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0.36880026183914227 -0.90772272213030936 0.83666277387715304 ;
	setAttr ".r" -type "double3" 6.3611093629270059e-015 89.999999999999972 0 ;
	setAttr ".s" -type "double3" 0.99999999999999933 0.99999999999999978 0.99999999999999956 ;
createNode nurbsCurve -n "foot_L0_crvShape" -p "foot_L0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "foot_L0_crvShapeOrig" -p "foot_L0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "foot_L0_heel" -p "foot_L0_root";
	setAttr ".t" -type "double3" -0.46207288565175297 -0.90468437724146278 -7.7715611723760958e-016 ;
	setAttr ".r" -type "double3" -1.908332808878105e-014 0 -6.3611093629270114e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1.0000000000000002 0.99999999999999933 ;
createNode nurbsCurve -n "foot_L0_heelShape" -p "foot_L0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_L0_heel1Shape" -p "foot_L0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_L0_heel2Shape" -p "foot_L0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_L0_heel3Shape" -p "foot_L0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_heel3_0crvShape" -p "foot_L0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_heel3_1crvShape" -p "foot_L0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_L0_outpivot" -p "foot_L0_root";
	setAttr ".t" -type "double3" 1.3551807145991006 -0.90468437724146333 -0.60296077958637584 ;
	setAttr ".r" -type "double3" -1.908332808878105e-014 0 -6.3611093629270114e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1.0000000000000002 0.99999999999999933 ;
createNode nurbsCurve -n "foot_L0_outpivotShape" -p "foot_L0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_L0_outpivot1Shape" -p "foot_L0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_L0_outpivot2Shape" -p "foot_L0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_L0_outpivot3Shape" -p "foot_L0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_outpivot3_0crvShape" -p "foot_L0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_outpivot3_1crvShape" -p "foot_L0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_L0_inpivot" -p "foot_L0_root";
	setAttr ".t" -type "double3" 1.3551807145991011 -0.90468437724146311 0.44204124507771864 ;
	setAttr ".r" -type "double3" -1.908332808878105e-014 0 -6.3611093629270114e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1.0000000000000002 0.99999999999999933 ;
createNode nurbsCurve -n "foot_L0_inpivotShape" -p "foot_L0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_L0_inpivot1Shape" -p "foot_L0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_L0_inpivot2Shape" -p "foot_L0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_L0_inpivot3Shape" -p "foot_L0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_inpivot3_0crvShape" -p "foot_L0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_L0_inpivot3_1crvShape" -p "foot_L0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_L0_1" -p "foot_L0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0.36880026183914227 -0.90772272213030936 0.83666277387715304 ;
	setAttr ".r" -type "double3" 6.3611093629270059e-015 89.999999999999972 0 ;
	setAttr ".s" -type "double3" 0.99999999999999933 0.99999999999999978 0.99999999999999956 ;
createNode nurbsCurve -n "foot_L0_Shape1" -p "foot_L0_1";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "foot_L0_Shape1Orig1" -p "foot_L0_1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "leg_L0_crv" -p "leg_L0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -9.5474336530132966 -0.21240322780572365 0.8366627738771496 ;
	setAttr ".r" -type "double3" 90.000000000000014 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000007 0.99999999999999833 1.0000000000000002 ;
createNode nurbsCurve -n "leg_L0_crvShape" -p "leg_L0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "leg_L0_crvShapeOrig" -p "leg_L0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "leg_R0_root" -p "spine_C0_root";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "blend" -ln "blend" -min 0 -max 1 -at "long";
	addAttr -ci true -sn "ikrefarray" -ln "ikrefarray" -dt "string";
	addAttr -ci true -sn "upvrefarray" -ln "upvrefarray" -dt "string";
	addAttr -ci true -sn "maxstretch" -ln "maxstretch" -dv 1 -min 1 -at "double";
	addAttr -ci true -sn "div0" -ln "div0" -dv 1 -min 1 -at "long";
	addAttr -ci true -sn "div1" -ln "div1" -dv 1 -min 1 -at "long";
	addAttr -ci true -k true -sn "st_profile" -ln "st_profile" -at "double";
	addAttr -ci true -k true -sn "sq_profile" -ln "sq_profile" -at "double";
	setAttr ".t" -type "double3" -0.80615537849444507 0.20384773318318011 0.83666277387714927 ;
	setAttr ".r" -type "double3" -4.1347210859025791e-014 6.3611093629270367e-015 -1.5902773407317578e-014 ;
	setAttr ".s" -type "double3" 1.0000000000000013 0.99999999999999822 -0.99999999999999967 ;
	setAttr ".comp_type" -type "string" "leg_2jnt_01";
	setAttr ".comp_name" -type "string" "leg";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "legUI_R0_root";
	setAttr ".blend" 1;
	setAttr ".ikrefarray" -type "string" "local_C0_root";
	setAttr ".upvrefarray" -type "string" "";
	setAttr ".maxstretch" 1.5;
	setAttr ".div0" 2;
	setAttr ".div1" 2;
	setAttr -k on ".st_profile";
	setAttr -k on ".sq_profile";
createNode nurbsCurve -n "leg_R0_rootShape" -p "leg_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "leg_R0_root1Shape" -p "leg_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "leg_R0_root2Shape" -p "leg_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "leg_R0_root3Shape" -p "leg_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "leg_R0_knee" -p "leg_R0_root";
	setAttr ".t" -type "double3" -4.1282487498381926 0.00071075503012349439 -2.7755575615628914e-015 ;
	setAttr ".r" -type "double3" 90.000000000000014 89.999999999999957 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999822 1.0000000000000013 ;
createNode nurbsCurve -n "leg_R0_kneeShape" -p "leg_R0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "leg_R0_knee1Shape" -p "leg_R0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "leg_R0_knee2Shape" -p "leg_R0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "leg_R0_knee3Shape" -p "leg_R0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_R0_knee3_0crvShape" -p "leg_R0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_R0_knee3_1crvShape" -p "leg_R0_knee";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "leg_R0_ankle" -p "leg_R0_knee";
	setAttr ".t" -type "double3" -1.9984014443252818e-015 -4.5114621810448039 -0.15568627900329512 ;
	setAttr ".r" -type "double3" 1.2722218725854067e-014 4.2002810794633489e-014 -4.4527765540489317e-014 ;
	setAttr ".s" -type "double3" 1.0000000000000011 1.0000000000000002 0.99999999999999911 ;
createNode nurbsCurve -n "leg_R0_ankleShape" -p "leg_R0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "leg_R0_ankle1Shape" -p "leg_R0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "leg_R0_ankle2Shape" -p "leg_R0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "leg_R0_ankle3Shape" -p "leg_R0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_R0_ankle3_0crvShape" -p "leg_R0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_R0_ankle3_1crvShape" -p "leg_R0_ankle";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "leg_R0_eff" -p "leg_R0_ankle";
	setAttr ".t" -type "double3" 8.8817841970012523e-016 2.55351295663786e-015 2.1377206638691337 ;
	setAttr ".r" -type "double3" -4.4527765540489361e-014 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000016 0.99999999999999967 1.0000000000000011 ;
createNode nurbsCurve -n "leg_R0_effShape" -p "leg_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "leg_R0_eff1Shape" -p "leg_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "leg_R0_eff2Shape" -p "leg_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "leg_R0_eff3Shape" -p "leg_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_R0_eff3_0crvShape" -p "leg_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "leg_R0_eff3_1crvShape" -p "leg_R0_eff";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_R0_root" -p "leg_R0_ankle";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "roll" -ln "roll" -min 0 -max 1 -at "long";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 7.7715611723760958e-016 -5.5511151231257827e-017 ;
	setAttr ".r" -type "double3" -4.4527765540489361e-014 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr ".comp_type" -type "string" "foot_bk_01";
	setAttr ".comp_name" -type "string" "foot";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "leg_2jnt_01";
	setAttr ".ui_host" -type "string" "legUI_R0_root";
createNode nurbsCurve -n "foot_R0_rootShape" -p "foot_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_R0_root1Shape" -p "foot_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_R0_root2Shape" -p "foot_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_R0_root3Shape" -p "foot_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "foot_R0_0_loc" -p "foot_R0_root";
	setAttr ".t" -type "double3" 0.91922044729120544 -0.60341075628672092 -4.4408920985006262e-016 ;
	setAttr ".r" -type "double3" -2.3890039270631383e-014 3.5072486578978986e-014 -22.490244653448055 ;
	setAttr ".s" -type "double3" 0.99999999999999944 1 0.99999999999999956 ;
createNode nurbsCurve -n "foot_R0_0_locShape" -p "foot_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_R0_0_loc1Shape" -p "foot_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_R0_0_loc2Shape" -p "foot_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_R0_0_loc3Shape" -p "foot_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_0_loc3_0crvShape" -p "foot_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_0_loc3_1crvShape" -p "foot_R0_0_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_R0_1_loc" -p "foot_R0_0_loc";
	setAttr ".t" -type "double3" 0.64158599405857042 0.087004870626029907 -1.1102230246251565e-016 ;
	setAttr ".r" -type "double3" -4.1347210859025608e-014 -2.544443745170814e-014 22.490244653448038 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999978 ;
createNode nurbsCurve -n "foot_R0_1_locShape" -p "foot_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_R0_1_loc1Shape" -p "foot_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_R0_1_loc2Shape" -p "foot_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_R0_1_loc3Shape" -p "foot_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_1_loc3_0crvShape" -p "foot_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_1_loc3_1crvShape" -p "foot_R0_1_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_R0_2_loc" -p "foot_R0_1_loc";
	setAttr ".t" -type "double3" 0.53459438469900245 -0.094336926401681065 -3.3306690738754696e-016 ;
	setAttr ".r" -type "double3" -3.9756933518293912e-015 89.999999999999957 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1.0000000000000002 ;
createNode nurbsCurve -n "foot_R0_2_locShape" -p "foot_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_R0_2_loc1Shape" -p "foot_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_R0_2_loc2Shape" -p "foot_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_R0_2_loc3Shape" -p "foot_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_2_loc3_0crvShape" -p "foot_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_2_loc3_1crvShape" -p "foot_R0_2_loc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "legUI_R0_root" -p "foot_R0_0_loc";
	addAttr -ci true -sn "comp_type" -ln "comp_type" -dt "string";
	addAttr -ci true -sn "comp_name" -ln "comp_name" -dt "string";
	addAttr -ci true -sn "comp_side" -ln "comp_side" -dt "string";
	addAttr -ci true -sn "comp_index" -ln "comp_index" -min 0 -at "long";
	addAttr -ci true -sn "connector" -ln "connector" -dt "string";
	addAttr -ci true -sn "ui_host" -ln "ui_host" -dt "string";
	addAttr -ci true -sn "icon" -ln "icon" -dt "string";
	addAttr -ci true -sn "k_tx" -ln "k_tx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ty" -ln "k_ty" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_tz" -ln "k_tz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ro" -ln "k_ro" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rx" -ln "k_rx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_ry" -ln "k_ry" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_rz" -ln "k_rz" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sx" -ln "k_sx" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sy" -ln "k_sy" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "k_sz" -ln "k_sz" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.2629167783492146 0.98171302400523841 -1.5009992656912385 ;
	setAttr ".r" -type "double3" -22.490244653448041 89.999999999999957 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000007 1 ;
	setAttr ".comp_type" -type "string" "control_01";
	setAttr ".comp_name" -type "string" "legUI";
	setAttr ".comp_side" -type "string" "R";
	setAttr ".connector" -type "string" "standard";
	setAttr ".ui_host" -type "string" "";
	setAttr ".icon" -type "string" "cross";
createNode nurbsCurve -n "legUI_R0_rootShape" -p "legUI_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "legUI_R0_root1Shape" -p "legUI_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "legUI_R0_root2Shape" -p "legUI_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "legUI_R0_root3Shape" -p "legUI_R0_root";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.125 0.125 0.125
		0.125 0.125 -0.125
		-0.125 0.125 -0.125
		-0.125 -0.125 -0.125
		-0.125 -0.125 0.125
		-0.125 0.125 0.125
		-0.125 0.125 -0.125
		-0.125 0.125 0.125
		0.125 0.125 0.125
		0.125 -0.125 0.125
		-0.125 -0.125 0.125
		0.125 -0.125 0.125
		0.125 -0.125 -0.125
		0.125 0.125 -0.125
		0.125 -0.125 -0.125
		-0.125 -0.125 -0.125
		;
createNode transform -n "foot_R0_crv" -p "foot_R0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0.36880026183914238 -0.90772272213030891 0.83666277387715304 ;
	setAttr ".r" -type "double3" -6.3611093629270193e-015 -89.999999999999957 0 ;
	setAttr ".s" -type "double3" 0.99999999999999911 0.99999999999999978 -0.99999999999999933 ;
createNode nurbsCurve -n "foot_R0_crvShape" -p "foot_R0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "foot_R0_crvShapeOrig" -p "foot_R0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "foot_R0_heel" -p "foot_R0_root";
	setAttr ".t" -type "double3" -0.46207288565175308 -0.90468437724146267 -1.1102230246251565e-016 ;
	setAttr ".r" -type "double3" -4.1347210859025589e-014 -2.5444437451708134e-014 -6.3611093629269996e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999933 1.0000000000000002 0.99999999999999911 ;
createNode nurbsCurve -n "foot_R0_heelShape" -p "foot_R0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_R0_heel1Shape" -p "foot_R0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_R0_heel2Shape" -p "foot_R0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_R0_heel3Shape" -p "foot_R0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_heel3_0crvShape" -p "foot_R0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_heel3_1crvShape" -p "foot_R0_heel";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_R0_outpivot" -p "foot_R0_root";
	setAttr ".t" -type "double3" 1.3551807145990997 -0.90468437724146367 -0.60296077958637606 ;
	setAttr ".r" -type "double3" -4.1347210859025589e-014 -2.5444437451708134e-014 -6.3611093629269996e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999933 1.0000000000000002 0.99999999999999911 ;
createNode nurbsCurve -n "foot_R0_outpivotShape" -p "foot_R0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_R0_outpivot1Shape" -p "foot_R0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_R0_outpivot2Shape" -p "foot_R0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_R0_outpivot3Shape" -p "foot_R0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_outpivot3_0crvShape" -p "foot_R0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_outpivot3_1crvShape" -p "foot_R0_outpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_R0_inpivot" -p "foot_R0_root";
	setAttr ".t" -type "double3" 1.3551807145991008 -0.90468437724146289 0.44204124507771814 ;
	setAttr ".r" -type "double3" -4.1347210859025589e-014 -2.5444437451708134e-014 -6.3611093629269996e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999933 1.0000000000000002 0.99999999999999911 ;
createNode nurbsCurve -n "foot_R0_inpivotShape" -p "foot_R0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0.25 0 0
		-0.25 0 0
		;
createNode nurbsCurve -n "foot_R0_inpivot1Shape" -p "foot_R0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0.25 0
		0 -0.25 0
		;
createNode nurbsCurve -n "foot_R0_inpivot2Shape" -p "foot_R0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0.25
		0 0 -0.25
		;
createNode nurbsCurve -n "foot_R0_inpivot3Shape" -p "foot_R0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		0 0.1875 0
		0.1875 0 0
		0 -0.1875 0
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_inpivot3_0crvShape" -p "foot_R0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		0 0 0.1875
		0.1875 0 0
		0 0 -0.1875
		-0.1875 0 0
		;
createNode nurbsCurve -n "foot_R0_inpivot3_1crvShape" -p "foot_R0_inpivot";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 0 1 2 3 4 5 6 7 8
		7
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		0 0.1875 0
		0 0 0.1875
		0 -0.1875 0
		0 0 -0.1875
		;
createNode transform -n "foot_R0_1" -p "foot_R0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0.36880026183914238 -0.90772272213030891 0.83666277387715304 ;
	setAttr ".r" -type "double3" -6.3611093629270193e-015 -89.999999999999957 0 ;
	setAttr ".s" -type "double3" 0.99999999999999911 0.99999999999999978 -0.99999999999999933 ;
createNode nurbsCurve -n "foot_R0_Shape1" -p "foot_R0_1";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "foot_R0_Shape1Orig" -p "foot_R0_1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode transform -n "leg_R0_crv" -p "leg_R0_root";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -9.5474336530132948 -0.21240322780572407 0.8366627738771496 ;
	setAttr ".r" -type "double3" -90.000000000000014 -89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000007 0.99999999999999822 -1.0000000000000002 ;
createNode nurbsCurve -n "leg_R0_crvShape" -p "leg_R0_crv";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "leg_R0_crvShapeOrig" -p "leg_R0_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 0
		0 0 0
		0 0 0
		0 0 0
		;
createNode animCurveUU -n "spine_C1_root_st_profile";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 -1 1 0;
createNode animCurveUU -n "spine_C1_root_sq_profile";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 1 1 0;
createNode animCurveUU -n "arm_L1_root_st_profile";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 -0.5 1 0;
createNode animCurveUU -n "arm_L1_root_sq_profile";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 0.5 1 0;
createNode mgear_curveCns -n "mgear_curveCns99";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns99Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns99GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns99GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak245";
createNode objectSet -n "tweakSet245";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8059";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts490";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns100";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns100Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns100GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns100GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak246";
createNode objectSet -n "tweakSet246";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8061";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts492";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns101";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns101Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns101GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns101GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak247";
createNode objectSet -n "tweakSet247";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8063";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts494";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns102";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns102Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns102GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns102GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak248";
createNode objectSet -n "tweakSet248";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8065";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts496";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns103";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns103Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns103GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns103GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak249";
createNode objectSet -n "tweakSet249";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8067";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts498";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns74";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns74Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns74GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns74GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak220";
createNode objectSet -n "tweakSet220";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8009";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts440";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns73";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns73Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns73GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns73GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak219";
createNode objectSet -n "tweakSet219";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8007";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts438";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode animCurveUU -n "neck_C1_root_st_profile1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 -1 1 0;
createNode animCurveUU -n "neck_C1_root_sq_profile1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 1 1 0;
createNode mgear_curveCns -n "mgear_curveCns84";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns84Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns84GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns84GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak230";
createNode objectSet -n "tweakSet230";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8029";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts460";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns85";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns85Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns85GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns85GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak231";
createNode objectSet -n "tweakSet231";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8031";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts462";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns83";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns83Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns83GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns83GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak229";
createNode objectSet -n "tweakSet229";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8027";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts458";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns86";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns86Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns86GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns86GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak232";
createNode objectSet -n "tweakSet232";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8033";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts464";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns87";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns87Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns87GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns87GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak233";
createNode objectSet -n "tweakSet233";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8035";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts466";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns88";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns88Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns88GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns88GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak234";
createNode objectSet -n "tweakSet234";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8037";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts468";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns81";
	setAttr -s 3 ".inputs";
createNode objectSet -n "mgear_curveCns81Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns81GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns81GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak227";
createNode objectSet -n "tweakSet227";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8023";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts454";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns80";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns80Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns80GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns80GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak226";
createNode objectSet -n "tweakSet226";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8021";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts452";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode animCurveUU -n "arm_R0_root_st_profile";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 -0.5 1 0;
createNode animCurveUU -n "arm_R0_root_sq_profile";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 0.5 1 0;
createNode mgear_curveCns -n "mgear_curveCns106";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns106Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns106GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns106GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak252";
createNode objectSet -n "tweakSet252";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8073";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts504";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns107";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns107Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns107GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns107GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak253";
createNode objectSet -n "tweakSet253";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8075";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts506";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns108";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns108Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns108GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns108GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak254";
createNode objectSet -n "tweakSet254";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8077";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts508";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns109";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns109Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns109GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns109GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak255";
createNode objectSet -n "tweakSet255";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8079";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts510";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns110";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns110Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns110GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns110GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak256";
createNode objectSet -n "tweakSet256";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8081";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts512";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns105";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns105Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns105GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns105GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak251";
createNode objectSet -n "tweakSet251";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8071";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts502";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns104";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns104Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns104GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns104GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak250";
createNode objectSet -n "tweakSet250";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8069";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts500";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns72";
	setAttr -s 2 ".inputs";
createNode objectSet -n "mgear_curveCns72Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns72GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns72GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak218";
createNode objectSet -n "tweakSet218";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8005";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts436";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode animCurveUU -n "leg_L1_root_st_profile";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 -1 1 0;
createNode animCurveUU -n "leg_L1_root_sq_profile";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 1 1 0;
createNode mgear_curveCns -n "mgear_curveCns97";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns97Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns97GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns97GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak243";
createNode objectSet -n "tweakSet243";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8055";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts486";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns98";
	setAttr -s 5 ".inputs";
createNode objectSet -n "mgear_curveCns98Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns98GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns98GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak244";
createNode objectSet -n "tweakSet244";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8057";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts488";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns96";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns96Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns96GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns96GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak242";
createNode objectSet -n "tweakSet242";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8053";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts484";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode animCurveUU -n "leg_R0_root_st_profile";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 -1 1 0;
createNode animCurveUU -n "leg_R0_root_sq_profile";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 0.5 1 1 0;
createNode mgear_curveCns -n "mgear_curveCns112";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns112Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns112GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns112GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak258";
createNode objectSet -n "tweakSet258";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8085";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts516";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns113";
	setAttr -s 5 ".inputs";
createNode objectSet -n "mgear_curveCns113Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns113GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns113GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak259";
createNode objectSet -n "tweakSet259";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8087";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts518";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode mgear_curveCns -n "mgear_curveCns111";
	setAttr -s 4 ".inputs";
createNode objectSet -n "mgear_curveCns111Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "mgear_curveCns111GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "mgear_curveCns111GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode tweak -n "tweak257";
createNode objectSet -n "tweakSet257";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8083";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts514";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "spine_C1_root_st_profile.o" "spine_C0_root.st_profile";
connectAttr "spine_C1_root_sq_profile.o" "spine_C0_root.sq_profile";
connectAttr "arm_L1_root_st_profile.o" "arm_L0_root.st_profile";
connectAttr "arm_L1_root_sq_profile.o" "arm_L0_root.sq_profile";
connectAttr "thumb_L0_blade_aimConstraint1.crx" "thumb_L0_blade.rx";
connectAttr "thumb_L0_blade_aimConstraint1.cry" "thumb_L0_blade.ry";
connectAttr "thumb_L0_blade_aimConstraint1.crz" "thumb_L0_blade.rz";
connectAttr "thumb_L0_blade_pointConstraint1.ctx" "thumb_L0_blade.tx";
connectAttr "thumb_L0_blade_pointConstraint1.cty" "thumb_L0_blade.ty";
connectAttr "thumb_L0_blade_pointConstraint1.ctz" "thumb_L0_blade.tz";
connectAttr "thumb_L0_blade.pim" "thumb_L0_blade_aimConstraint1.cpim";
connectAttr "thumb_L0_blade.t" "thumb_L0_blade_aimConstraint1.ct";
connectAttr "thumb_L0_blade.rp" "thumb_L0_blade_aimConstraint1.crp";
connectAttr "thumb_L0_blade.rpt" "thumb_L0_blade_aimConstraint1.crt";
connectAttr "thumb_L0_blade.ro" "thumb_L0_blade_aimConstraint1.cro";
connectAttr "thumb_L0_0_loc.t" "thumb_L0_blade_aimConstraint1.tg[0].tt";
connectAttr "thumb_L0_0_loc.rp" "thumb_L0_blade_aimConstraint1.tg[0].trp";
connectAttr "thumb_L0_0_loc.rpt" "thumb_L0_blade_aimConstraint1.tg[0].trt";
connectAttr "thumb_L0_0_loc.pm" "thumb_L0_blade_aimConstraint1.tg[0].tpm";
connectAttr "thumb_L0_blade_aimConstraint1.w0" "thumb_L0_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "thumb_L0_root.wm" "thumb_L0_blade_aimConstraint1.wum";
connectAttr "thumb_L0_blade.pim" "thumb_L0_blade_pointConstraint1.cpim";
connectAttr "thumb_L0_blade.rp" "thumb_L0_blade_pointConstraint1.crp";
connectAttr "thumb_L0_blade.rpt" "thumb_L0_blade_pointConstraint1.crt";
connectAttr "thumb_L0_root.t" "thumb_L0_blade_pointConstraint1.tg[0].tt";
connectAttr "thumb_L0_root.rp" "thumb_L0_blade_pointConstraint1.tg[0].trp";
connectAttr "thumb_L0_root.rpt" "thumb_L0_blade_pointConstraint1.tg[0].trt";
connectAttr "thumb_L0_root.pm" "thumb_L0_blade_pointConstraint1.tg[0].tpm";
connectAttr "thumb_L0_blade_pointConstraint1.w0" "thumb_L0_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns99.og[0]" "thumb_L0_crvShape.cr";
connectAttr "tweak245.pl[0].cp[0]" "thumb_L0_crvShape.twl";
connectAttr "mgear_curveCns99GroupId.id" "thumb_L0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns99Set.mwc" "thumb_L0_crvShape.iog.og[0].gco";
connectAttr "groupId8059.id" "thumb_L0_crvShape.iog.og[1].gid";
connectAttr "tweakSet245.mwc" "thumb_L0_crvShape.iog.og[1].gco";
connectAttr "finger_L0_blade_aimConstraint1.crx" "finger_L0_blade.rx";
connectAttr "finger_L0_blade_aimConstraint1.cry" "finger_L0_blade.ry";
connectAttr "finger_L0_blade_aimConstraint1.crz" "finger_L0_blade.rz";
connectAttr "finger_L0_blade_pointConstraint1.ctx" "finger_L0_blade.tx";
connectAttr "finger_L0_blade_pointConstraint1.cty" "finger_L0_blade.ty";
connectAttr "finger_L0_blade_pointConstraint1.ctz" "finger_L0_blade.tz";
connectAttr "finger_L0_blade.pim" "finger_L0_blade_aimConstraint1.cpim";
connectAttr "finger_L0_blade.t" "finger_L0_blade_aimConstraint1.ct";
connectAttr "finger_L0_blade.rp" "finger_L0_blade_aimConstraint1.crp";
connectAttr "finger_L0_blade.rpt" "finger_L0_blade_aimConstraint1.crt";
connectAttr "finger_L0_blade.ro" "finger_L0_blade_aimConstraint1.cro";
connectAttr "finger_L0_0_loc.t" "finger_L0_blade_aimConstraint1.tg[0].tt";
connectAttr "finger_L0_0_loc.rp" "finger_L0_blade_aimConstraint1.tg[0].trp";
connectAttr "finger_L0_0_loc.rpt" "finger_L0_blade_aimConstraint1.tg[0].trt";
connectAttr "finger_L0_0_loc.pm" "finger_L0_blade_aimConstraint1.tg[0].tpm";
connectAttr "finger_L0_blade_aimConstraint1.w0" "finger_L0_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "finger_L0_root.wm" "finger_L0_blade_aimConstraint1.wum";
connectAttr "finger_L0_blade.pim" "finger_L0_blade_pointConstraint1.cpim";
connectAttr "finger_L0_blade.rp" "finger_L0_blade_pointConstraint1.crp";
connectAttr "finger_L0_blade.rpt" "finger_L0_blade_pointConstraint1.crt";
connectAttr "finger_L0_root.t" "finger_L0_blade_pointConstraint1.tg[0].tt";
connectAttr "finger_L0_root.rp" "finger_L0_blade_pointConstraint1.tg[0].trp";
connectAttr "finger_L0_root.rpt" "finger_L0_blade_pointConstraint1.tg[0].trt";
connectAttr "finger_L0_root.pm" "finger_L0_blade_pointConstraint1.tg[0].tpm";
connectAttr "finger_L0_blade_pointConstraint1.w0" "finger_L0_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns100.og[0]" "finger_L0_crvShape.cr";
connectAttr "tweak246.pl[0].cp[0]" "finger_L0_crvShape.twl";
connectAttr "mgear_curveCns100GroupId.id" "finger_L0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns100Set.mwc" "finger_L0_crvShape.iog.og[0].gco";
connectAttr "groupId8061.id" "finger_L0_crvShape.iog.og[1].gid";
connectAttr "tweakSet246.mwc" "finger_L0_crvShape.iog.og[1].gco";
connectAttr "finger_L1_blade_aimConstraint1.crx" "finger_L1_blade.rx";
connectAttr "finger_L1_blade_aimConstraint1.cry" "finger_L1_blade.ry";
connectAttr "finger_L1_blade_aimConstraint1.crz" "finger_L1_blade.rz";
connectAttr "finger_L1_blade_pointConstraint1.ctx" "finger_L1_blade.tx";
connectAttr "finger_L1_blade_pointConstraint1.cty" "finger_L1_blade.ty";
connectAttr "finger_L1_blade_pointConstraint1.ctz" "finger_L1_blade.tz";
connectAttr "finger_L1_blade.pim" "finger_L1_blade_aimConstraint1.cpim";
connectAttr "finger_L1_blade.t" "finger_L1_blade_aimConstraint1.ct";
connectAttr "finger_L1_blade.rp" "finger_L1_blade_aimConstraint1.crp";
connectAttr "finger_L1_blade.rpt" "finger_L1_blade_aimConstraint1.crt";
connectAttr "finger_L1_blade.ro" "finger_L1_blade_aimConstraint1.cro";
connectAttr "finger_L1_0_loc.t" "finger_L1_blade_aimConstraint1.tg[0].tt";
connectAttr "finger_L1_0_loc.rp" "finger_L1_blade_aimConstraint1.tg[0].trp";
connectAttr "finger_L1_0_loc.rpt" "finger_L1_blade_aimConstraint1.tg[0].trt";
connectAttr "finger_L1_0_loc.pm" "finger_L1_blade_aimConstraint1.tg[0].tpm";
connectAttr "finger_L1_blade_aimConstraint1.w0" "finger_L1_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "finger_L1_root.wm" "finger_L1_blade_aimConstraint1.wum";
connectAttr "finger_L1_blade.pim" "finger_L1_blade_pointConstraint1.cpim";
connectAttr "finger_L1_blade.rp" "finger_L1_blade_pointConstraint1.crp";
connectAttr "finger_L1_blade.rpt" "finger_L1_blade_pointConstraint1.crt";
connectAttr "finger_L1_root.t" "finger_L1_blade_pointConstraint1.tg[0].tt";
connectAttr "finger_L1_root.rp" "finger_L1_blade_pointConstraint1.tg[0].trp";
connectAttr "finger_L1_root.rpt" "finger_L1_blade_pointConstraint1.tg[0].trt";
connectAttr "finger_L1_root.pm" "finger_L1_blade_pointConstraint1.tg[0].tpm";
connectAttr "finger_L1_blade_pointConstraint1.w0" "finger_L1_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns101.og[0]" "finger_L1_crvShape.cr";
connectAttr "tweak247.pl[0].cp[0]" "finger_L1_crvShape.twl";
connectAttr "mgear_curveCns101GroupId.id" "finger_L1_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns101Set.mwc" "finger_L1_crvShape.iog.og[0].gco";
connectAttr "groupId8063.id" "finger_L1_crvShape.iog.og[1].gid";
connectAttr "tweakSet247.mwc" "finger_L1_crvShape.iog.og[1].gco";
connectAttr "finger_L2_blade_aimConstraint1.crx" "finger_L2_blade.rx";
connectAttr "finger_L2_blade_aimConstraint1.cry" "finger_L2_blade.ry";
connectAttr "finger_L2_blade_aimConstraint1.crz" "finger_L2_blade.rz";
connectAttr "finger_L2_blade_pointConstraint1.ctx" "finger_L2_blade.tx";
connectAttr "finger_L2_blade_pointConstraint1.cty" "finger_L2_blade.ty";
connectAttr "finger_L2_blade_pointConstraint1.ctz" "finger_L2_blade.tz";
connectAttr "finger_L2_blade.pim" "finger_L2_blade_aimConstraint1.cpim";
connectAttr "finger_L2_blade.t" "finger_L2_blade_aimConstraint1.ct";
connectAttr "finger_L2_blade.rp" "finger_L2_blade_aimConstraint1.crp";
connectAttr "finger_L2_blade.rpt" "finger_L2_blade_aimConstraint1.crt";
connectAttr "finger_L2_blade.ro" "finger_L2_blade_aimConstraint1.cro";
connectAttr "finger_L2_0_loc.t" "finger_L2_blade_aimConstraint1.tg[0].tt";
connectAttr "finger_L2_0_loc.rp" "finger_L2_blade_aimConstraint1.tg[0].trp";
connectAttr "finger_L2_0_loc.rpt" "finger_L2_blade_aimConstraint1.tg[0].trt";
connectAttr "finger_L2_0_loc.pm" "finger_L2_blade_aimConstraint1.tg[0].tpm";
connectAttr "finger_L2_blade_aimConstraint1.w0" "finger_L2_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "finger_L2_root.wm" "finger_L2_blade_aimConstraint1.wum";
connectAttr "finger_L2_blade.pim" "finger_L2_blade_pointConstraint1.cpim";
connectAttr "finger_L2_blade.rp" "finger_L2_blade_pointConstraint1.crp";
connectAttr "finger_L2_blade.rpt" "finger_L2_blade_pointConstraint1.crt";
connectAttr "finger_L2_root.t" "finger_L2_blade_pointConstraint1.tg[0].tt";
connectAttr "finger_L2_root.rp" "finger_L2_blade_pointConstraint1.tg[0].trp";
connectAttr "finger_L2_root.rpt" "finger_L2_blade_pointConstraint1.tg[0].trt";
connectAttr "finger_L2_root.pm" "finger_L2_blade_pointConstraint1.tg[0].tpm";
connectAttr "finger_L2_blade_pointConstraint1.w0" "finger_L2_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns102.og[0]" "finger_L2_crvShape.cr";
connectAttr "tweak248.pl[0].cp[0]" "finger_L2_crvShape.twl";
connectAttr "mgear_curveCns102GroupId.id" "finger_L2_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns102Set.mwc" "finger_L2_crvShape.iog.og[0].gco";
connectAttr "groupId8065.id" "finger_L2_crvShape.iog.og[1].gid";
connectAttr "tweakSet248.mwc" "finger_L2_crvShape.iog.og[1].gco";
connectAttr "finger_L3_blade_aimConstraint1.crx" "finger_L3_blade.rx";
connectAttr "finger_L3_blade_aimConstraint1.cry" "finger_L3_blade.ry";
connectAttr "finger_L3_blade_aimConstraint1.crz" "finger_L3_blade.rz";
connectAttr "finger_L3_blade_pointConstraint1.ctx" "finger_L3_blade.tx";
connectAttr "finger_L3_blade_pointConstraint1.cty" "finger_L3_blade.ty";
connectAttr "finger_L3_blade_pointConstraint1.ctz" "finger_L3_blade.tz";
connectAttr "finger_L3_blade.pim" "finger_L3_blade_aimConstraint1.cpim";
connectAttr "finger_L3_blade.t" "finger_L3_blade_aimConstraint1.ct";
connectAttr "finger_L3_blade.rp" "finger_L3_blade_aimConstraint1.crp";
connectAttr "finger_L3_blade.rpt" "finger_L3_blade_aimConstraint1.crt";
connectAttr "finger_L3_blade.ro" "finger_L3_blade_aimConstraint1.cro";
connectAttr "finger_L3_0_loc.t" "finger_L3_blade_aimConstraint1.tg[0].tt";
connectAttr "finger_L3_0_loc.rp" "finger_L3_blade_aimConstraint1.tg[0].trp";
connectAttr "finger_L3_0_loc.rpt" "finger_L3_blade_aimConstraint1.tg[0].trt";
connectAttr "finger_L3_0_loc.pm" "finger_L3_blade_aimConstraint1.tg[0].tpm";
connectAttr "finger_L3_blade_aimConstraint1.w0" "finger_L3_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "finger_L3_root.wm" "finger_L3_blade_aimConstraint1.wum";
connectAttr "finger_L3_blade.pim" "finger_L3_blade_pointConstraint1.cpim";
connectAttr "finger_L3_blade.rp" "finger_L3_blade_pointConstraint1.crp";
connectAttr "finger_L3_blade.rpt" "finger_L3_blade_pointConstraint1.crt";
connectAttr "finger_L3_root.t" "finger_L3_blade_pointConstraint1.tg[0].tt";
connectAttr "finger_L3_root.rp" "finger_L3_blade_pointConstraint1.tg[0].trp";
connectAttr "finger_L3_root.rpt" "finger_L3_blade_pointConstraint1.tg[0].trt";
connectAttr "finger_L3_root.pm" "finger_L3_blade_pointConstraint1.tg[0].tpm";
connectAttr "finger_L3_blade_pointConstraint1.w0" "finger_L3_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns103.og[0]" "finger_L3_crvShape.cr";
connectAttr "tweak249.pl[0].cp[0]" "finger_L3_crvShape.twl";
connectAttr "mgear_curveCns103GroupId.id" "finger_L3_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns103Set.mwc" "finger_L3_crvShape.iog.og[0].gco";
connectAttr "groupId8067.id" "finger_L3_crvShape.iog.og[1].gid";
connectAttr "tweakSet249.mwc" "finger_L3_crvShape.iog.og[1].gco";
connectAttr "mgear_curveCns74.og[0]" "arm_L0_crvShape.cr";
connectAttr "tweak220.pl[0].cp[0]" "arm_L0_crvShape.twl";
connectAttr "mgear_curveCns74GroupId.id" "arm_L0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns74Set.mwc" "arm_L0_crvShape.iog.og[0].gco";
connectAttr "groupId8009.id" "arm_L0_crvShape.iog.og[1].gid";
connectAttr "tweakSet220.mwc" "arm_L0_crvShape.iog.og[1].gco";
connectAttr "shoulder_L0_blade_aimConstraint1.crx" "shoulder_L0_blade.rx";
connectAttr "shoulder_L0_blade_aimConstraint1.cry" "shoulder_L0_blade.ry";
connectAttr "shoulder_L0_blade_aimConstraint1.crz" "shoulder_L0_blade.rz";
connectAttr "shoulder_L0_blade_pointConstraint1.ctx" "shoulder_L0_blade.tx";
connectAttr "shoulder_L0_blade_pointConstraint1.cty" "shoulder_L0_blade.ty";
connectAttr "shoulder_L0_blade_pointConstraint1.ctz" "shoulder_L0_blade.tz";
connectAttr "shoulder_L0_blade.pim" "shoulder_L0_blade_aimConstraint1.cpim";
connectAttr "shoulder_L0_blade.t" "shoulder_L0_blade_aimConstraint1.ct";
connectAttr "shoulder_L0_blade.rp" "shoulder_L0_blade_aimConstraint1.crp";
connectAttr "shoulder_L0_blade.rpt" "shoulder_L0_blade_aimConstraint1.crt";
connectAttr "shoulder_L0_blade.ro" "shoulder_L0_blade_aimConstraint1.cro";
connectAttr "shoulder_L0_tip.t" "shoulder_L0_blade_aimConstraint1.tg[0].tt";
connectAttr "shoulder_L0_tip.rp" "shoulder_L0_blade_aimConstraint1.tg[0].trp";
connectAttr "shoulder_L0_tip.rpt" "shoulder_L0_blade_aimConstraint1.tg[0].trt";
connectAttr "shoulder_L0_tip.pm" "shoulder_L0_blade_aimConstraint1.tg[0].tpm";
connectAttr "shoulder_L0_blade_aimConstraint1.w0" "shoulder_L0_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "shoulder_L0_root.wm" "shoulder_L0_blade_aimConstraint1.wum";
connectAttr "shoulder_L0_blade.pim" "shoulder_L0_blade_pointConstraint1.cpim";
connectAttr "shoulder_L0_blade.rp" "shoulder_L0_blade_pointConstraint1.crp";
connectAttr "shoulder_L0_blade.rpt" "shoulder_L0_blade_pointConstraint1.crt";
connectAttr "shoulder_L0_root.t" "shoulder_L0_blade_pointConstraint1.tg[0].tt";
connectAttr "shoulder_L0_root.rp" "shoulder_L0_blade_pointConstraint1.tg[0].trp"
		;
connectAttr "shoulder_L0_root.rpt" "shoulder_L0_blade_pointConstraint1.tg[0].trt"
		;
connectAttr "shoulder_L0_root.pm" "shoulder_L0_blade_pointConstraint1.tg[0].tpm"
		;
connectAttr "shoulder_L0_blade_pointConstraint1.w0" "shoulder_L0_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns73.og[0]" "shoulder_L0_crvShape.cr";
connectAttr "tweak219.pl[0].cp[0]" "shoulder_L0_crvShape.twl";
connectAttr "mgear_curveCns73GroupId.id" "shoulder_L0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns73Set.mwc" "shoulder_L0_crvShape.iog.og[0].gco";
connectAttr "groupId8007.id" "shoulder_L0_crvShape.iog.og[1].gid";
connectAttr "tweakSet219.mwc" "shoulder_L0_crvShape.iog.og[1].gco";
connectAttr "neck_C1_root_st_profile1.o" "neck_C0_root.st_profile";
connectAttr "neck_C1_root_sq_profile1.o" "neck_C0_root.sq_profile";
connectAttr "mgear_curveCns84.og[0]" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShape.cr"
		;
connectAttr "tweak230.pl[0].cp[0]" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShape.twl"
		;
connectAttr "mgear_curveCns84GroupId.id" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[0].gid"
		;
connectAttr "mgear_curveCns84Set.mwc" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[0].gco"
		;
connectAttr "groupId8029.id" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[1].gid"
		;
connectAttr "tweakSet230.mwc" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[1].gco"
		;
connectAttr "mgear_curveCns85.og[0]" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShape.cr"
		;
connectAttr "tweak231.pl[0].cp[0]" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShape.twl"
		;
connectAttr "mgear_curveCns85GroupId.id" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[0].gid"
		;
connectAttr "mgear_curveCns85Set.mwc" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[0].gco"
		;
connectAttr "groupId8031.id" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[1].gid"
		;
connectAttr "tweakSet231.mwc" "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[1].gco"
		;
connectAttr "mgear_curveCns83.og[0]" "mouth_C0_crvShape.cr";
connectAttr "tweak229.pl[0].cp[0]" "mouth_C0_crvShape.twl";
connectAttr "mgear_curveCns83GroupId.id" "mouth_C0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns83Set.mwc" "mouth_C0_crvShape.iog.og[0].gco";
connectAttr "groupId8027.id" "mouth_C0_crvShape.iog.og[1].gid";
connectAttr "tweakSet229.mwc" "mouth_C0_crvShape.iog.og[1].gco";
connectAttr "mgear_curveCns86.og[0]" "mouth_C0_crv1Shape.cr";
connectAttr "tweak232.pl[0].cp[0]" "mouth_C0_crv1Shape.twl";
connectAttr "mgear_curveCns86GroupId.id" "mouth_C0_crv1Shape.iog.og[0].gid";
connectAttr "mgear_curveCns86Set.mwc" "mouth_C0_crv1Shape.iog.og[0].gco";
connectAttr "groupId8033.id" "mouth_C0_crv1Shape.iog.og[1].gid";
connectAttr "tweakSet232.mwc" "mouth_C0_crv1Shape.iog.og[1].gco";
connectAttr "mgear_curveCns87.og[0]" "eye_L0_crvShape.cr";
connectAttr "tweak233.pl[0].cp[0]" "eye_L0_crvShape.twl";
connectAttr "mgear_curveCns87GroupId.id" "eye_L0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns87Set.mwc" "eye_L0_crvShape.iog.og[0].gco";
connectAttr "groupId8035.id" "eye_L0_crvShape.iog.og[1].gid";
connectAttr "tweakSet233.mwc" "eye_L0_crvShape.iog.og[1].gco";
connectAttr "mgear_curveCns88.og[0]" "eye_R0_crvShape.cr";
connectAttr "tweak234.pl[0].cp[0]" "eye_R0_crvShape.twl";
connectAttr "mgear_curveCns88GroupId.id" "eye_R0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns88Set.mwc" "eye_R0_crvShape.iog.og[0].gco";
connectAttr "groupId8037.id" "eye_R0_crvShape.iog.og[1].gid";
connectAttr "tweakSet234.mwc" "eye_R0_crvShape.iog.og[1].gco";
connectAttr "mgear_curveCns81.og[0]" "neck_C0_head_crvShape.cr";
connectAttr "tweak227.pl[0].cp[0]" "neck_C0_head_crvShape.twl";
connectAttr "mgear_curveCns81GroupId.id" "neck_C0_head_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns81Set.mwc" "neck_C0_head_crvShape.iog.og[0].gco";
connectAttr "groupId8023.id" "neck_C0_head_crvShape.iog.og[1].gid";
connectAttr "tweakSet227.mwc" "neck_C0_head_crvShape.iog.og[1].gco";
connectAttr "neck_C0_blade_aimConstraint1.crx" "neck_C0_blade.rx";
connectAttr "neck_C0_blade_aimConstraint1.cry" "neck_C0_blade.ry";
connectAttr "neck_C0_blade_aimConstraint1.crz" "neck_C0_blade.rz";
connectAttr "neck_C0_blade_pointConstraint1.ctx" "neck_C0_blade.tx";
connectAttr "neck_C0_blade_pointConstraint1.cty" "neck_C0_blade.ty";
connectAttr "neck_C0_blade_pointConstraint1.ctz" "neck_C0_blade.tz";
connectAttr "neck_C0_blade.pim" "neck_C0_blade_aimConstraint1.cpim";
connectAttr "neck_C0_blade.t" "neck_C0_blade_aimConstraint1.ct";
connectAttr "neck_C0_blade.rp" "neck_C0_blade_aimConstraint1.crp";
connectAttr "neck_C0_blade.rpt" "neck_C0_blade_aimConstraint1.crt";
connectAttr "neck_C0_blade.ro" "neck_C0_blade_aimConstraint1.cro";
connectAttr "neck_C0_tan0.t" "neck_C0_blade_aimConstraint1.tg[0].tt";
connectAttr "neck_C0_tan0.rp" "neck_C0_blade_aimConstraint1.tg[0].trp";
connectAttr "neck_C0_tan0.rpt" "neck_C0_blade_aimConstraint1.tg[0].trt";
connectAttr "neck_C0_tan0.pm" "neck_C0_blade_aimConstraint1.tg[0].tpm";
connectAttr "neck_C0_blade_aimConstraint1.w0" "neck_C0_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "neck_C0_root.wm" "neck_C0_blade_aimConstraint1.wum";
connectAttr "neck_C0_blade.pim" "neck_C0_blade_pointConstraint1.cpim";
connectAttr "neck_C0_blade.rp" "neck_C0_blade_pointConstraint1.crp";
connectAttr "neck_C0_blade.rpt" "neck_C0_blade_pointConstraint1.crt";
connectAttr "neck_C0_root.t" "neck_C0_blade_pointConstraint1.tg[0].tt";
connectAttr "neck_C0_root.rp" "neck_C0_blade_pointConstraint1.tg[0].trp";
connectAttr "neck_C0_root.rpt" "neck_C0_blade_pointConstraint1.tg[0].trt";
connectAttr "neck_C0_root.pm" "neck_C0_blade_pointConstraint1.tg[0].tpm";
connectAttr "neck_C0_blade_pointConstraint1.w0" "neck_C0_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns80.og[0]" "neck_C0_neck_crvShape.cr";
connectAttr "tweak226.pl[0].cp[0]" "neck_C0_neck_crvShape.twl";
connectAttr "mgear_curveCns80GroupId.id" "neck_C0_neck_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns80Set.mwc" "neck_C0_neck_crvShape.iog.og[0].gco";
connectAttr "groupId8021.id" "neck_C0_neck_crvShape.iog.og[1].gid";
connectAttr "tweakSet226.mwc" "neck_C0_neck_crvShape.iog.og[1].gco";
connectAttr "arm_R0_root_st_profile.o" "arm_R0_root.st_profile";
connectAttr "arm_R0_root_sq_profile.o" "arm_R0_root.sq_profile";
connectAttr "thumb_R0_blade_aimConstraint1.crx" "thumb_R0_blade.rx";
connectAttr "thumb_R0_blade_aimConstraint1.cry" "thumb_R0_blade.ry";
connectAttr "thumb_R0_blade_aimConstraint1.crz" "thumb_R0_blade.rz";
connectAttr "thumb_R0_blade_pointConstraint1.ctx" "thumb_R0_blade.tx";
connectAttr "thumb_R0_blade_pointConstraint1.cty" "thumb_R0_blade.ty";
connectAttr "thumb_R0_blade_pointConstraint1.ctz" "thumb_R0_blade.tz";
connectAttr "thumb_R0_blade.pim" "thumb_R0_blade_aimConstraint1.cpim";
connectAttr "thumb_R0_blade.t" "thumb_R0_blade_aimConstraint1.ct";
connectAttr "thumb_R0_blade.rp" "thumb_R0_blade_aimConstraint1.crp";
connectAttr "thumb_R0_blade.rpt" "thumb_R0_blade_aimConstraint1.crt";
connectAttr "thumb_R0_blade.ro" "thumb_R0_blade_aimConstraint1.cro";
connectAttr "thumb_R0_0_loc.t" "thumb_R0_blade_aimConstraint1.tg[0].tt";
connectAttr "thumb_R0_0_loc.rp" "thumb_R0_blade_aimConstraint1.tg[0].trp";
connectAttr "thumb_R0_0_loc.rpt" "thumb_R0_blade_aimConstraint1.tg[0].trt";
connectAttr "thumb_R0_0_loc.pm" "thumb_R0_blade_aimConstraint1.tg[0].tpm";
connectAttr "thumb_R0_blade_aimConstraint1.w0" "thumb_R0_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "thumb_R0_root.wm" "thumb_R0_blade_aimConstraint1.wum";
connectAttr "thumb_R0_blade.pim" "thumb_R0_blade_pointConstraint1.cpim";
connectAttr "thumb_R0_blade.rp" "thumb_R0_blade_pointConstraint1.crp";
connectAttr "thumb_R0_blade.rpt" "thumb_R0_blade_pointConstraint1.crt";
connectAttr "thumb_R0_root.t" "thumb_R0_blade_pointConstraint1.tg[0].tt";
connectAttr "thumb_R0_root.rp" "thumb_R0_blade_pointConstraint1.tg[0].trp";
connectAttr "thumb_R0_root.rpt" "thumb_R0_blade_pointConstraint1.tg[0].trt";
connectAttr "thumb_R0_root.pm" "thumb_R0_blade_pointConstraint1.tg[0].tpm";
connectAttr "thumb_R0_blade_pointConstraint1.w0" "thumb_R0_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns106.og[0]" "thumb_R0_crvShape.cr";
connectAttr "tweak252.pl[0].cp[0]" "thumb_R0_crvShape.twl";
connectAttr "mgear_curveCns106GroupId.id" "thumb_R0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns106Set.mwc" "thumb_R0_crvShape.iog.og[0].gco";
connectAttr "groupId8073.id" "thumb_R0_crvShape.iog.og[1].gid";
connectAttr "tweakSet252.mwc" "thumb_R0_crvShape.iog.og[1].gco";
connectAttr "finger_R0_blade_aimConstraint1.crx" "finger_R0_blade.rx";
connectAttr "finger_R0_blade_aimConstraint1.cry" "finger_R0_blade.ry";
connectAttr "finger_R0_blade_aimConstraint1.crz" "finger_R0_blade.rz";
connectAttr "finger_R0_blade_pointConstraint1.ctx" "finger_R0_blade.tx";
connectAttr "finger_R0_blade_pointConstraint1.cty" "finger_R0_blade.ty";
connectAttr "finger_R0_blade_pointConstraint1.ctz" "finger_R0_blade.tz";
connectAttr "finger_R0_blade.pim" "finger_R0_blade_aimConstraint1.cpim";
connectAttr "finger_R0_blade.t" "finger_R0_blade_aimConstraint1.ct";
connectAttr "finger_R0_blade.rp" "finger_R0_blade_aimConstraint1.crp";
connectAttr "finger_R0_blade.rpt" "finger_R0_blade_aimConstraint1.crt";
connectAttr "finger_R0_blade.ro" "finger_R0_blade_aimConstraint1.cro";
connectAttr "finger_R0_0_loc.t" "finger_R0_blade_aimConstraint1.tg[0].tt";
connectAttr "finger_R0_0_loc.rp" "finger_R0_blade_aimConstraint1.tg[0].trp";
connectAttr "finger_R0_0_loc.rpt" "finger_R0_blade_aimConstraint1.tg[0].trt";
connectAttr "finger_R0_0_loc.pm" "finger_R0_blade_aimConstraint1.tg[0].tpm";
connectAttr "finger_R0_blade_aimConstraint1.w0" "finger_R0_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "finger_R0_root.wm" "finger_R0_blade_aimConstraint1.wum";
connectAttr "finger_R0_blade.pim" "finger_R0_blade_pointConstraint1.cpim";
connectAttr "finger_R0_blade.rp" "finger_R0_blade_pointConstraint1.crp";
connectAttr "finger_R0_blade.rpt" "finger_R0_blade_pointConstraint1.crt";
connectAttr "finger_R0_root.t" "finger_R0_blade_pointConstraint1.tg[0].tt";
connectAttr "finger_R0_root.rp" "finger_R0_blade_pointConstraint1.tg[0].trp";
connectAttr "finger_R0_root.rpt" "finger_R0_blade_pointConstraint1.tg[0].trt";
connectAttr "finger_R0_root.pm" "finger_R0_blade_pointConstraint1.tg[0].tpm";
connectAttr "finger_R0_blade_pointConstraint1.w0" "finger_R0_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns107.og[0]" "finger_R0_crvShape.cr";
connectAttr "tweak253.pl[0].cp[0]" "finger_R0_crvShape.twl";
connectAttr "mgear_curveCns107GroupId.id" "finger_R0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns107Set.mwc" "finger_R0_crvShape.iog.og[0].gco";
connectAttr "groupId8075.id" "finger_R0_crvShape.iog.og[1].gid";
connectAttr "tweakSet253.mwc" "finger_R0_crvShape.iog.og[1].gco";
connectAttr "finger_R1_blade_aimConstraint1.crx" "finger_R1_blade.rx";
connectAttr "finger_R1_blade_aimConstraint1.cry" "finger_R1_blade.ry";
connectAttr "finger_R1_blade_aimConstraint1.crz" "finger_R1_blade.rz";
connectAttr "finger_R1_blade_pointConstraint1.ctx" "finger_R1_blade.tx";
connectAttr "finger_R1_blade_pointConstraint1.cty" "finger_R1_blade.ty";
connectAttr "finger_R1_blade_pointConstraint1.ctz" "finger_R1_blade.tz";
connectAttr "finger_R1_blade.pim" "finger_R1_blade_aimConstraint1.cpim";
connectAttr "finger_R1_blade.t" "finger_R1_blade_aimConstraint1.ct";
connectAttr "finger_R1_blade.rp" "finger_R1_blade_aimConstraint1.crp";
connectAttr "finger_R1_blade.rpt" "finger_R1_blade_aimConstraint1.crt";
connectAttr "finger_R1_blade.ro" "finger_R1_blade_aimConstraint1.cro";
connectAttr "finger_R1_0_loc.t" "finger_R1_blade_aimConstraint1.tg[0].tt";
connectAttr "finger_R1_0_loc.rp" "finger_R1_blade_aimConstraint1.tg[0].trp";
connectAttr "finger_R1_0_loc.rpt" "finger_R1_blade_aimConstraint1.tg[0].trt";
connectAttr "finger_R1_0_loc.pm" "finger_R1_blade_aimConstraint1.tg[0].tpm";
connectAttr "finger_R1_blade_aimConstraint1.w0" "finger_R1_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "finger_R1_root.wm" "finger_R1_blade_aimConstraint1.wum";
connectAttr "finger_R1_blade.pim" "finger_R1_blade_pointConstraint1.cpim";
connectAttr "finger_R1_blade.rp" "finger_R1_blade_pointConstraint1.crp";
connectAttr "finger_R1_blade.rpt" "finger_R1_blade_pointConstraint1.crt";
connectAttr "finger_R1_root.t" "finger_R1_blade_pointConstraint1.tg[0].tt";
connectAttr "finger_R1_root.rp" "finger_R1_blade_pointConstraint1.tg[0].trp";
connectAttr "finger_R1_root.rpt" "finger_R1_blade_pointConstraint1.tg[0].trt";
connectAttr "finger_R1_root.pm" "finger_R1_blade_pointConstraint1.tg[0].tpm";
connectAttr "finger_R1_blade_pointConstraint1.w0" "finger_R1_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns108.og[0]" "finger_R1_crvShape.cr";
connectAttr "tweak254.pl[0].cp[0]" "finger_R1_crvShape.twl";
connectAttr "mgear_curveCns108GroupId.id" "finger_R1_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns108Set.mwc" "finger_R1_crvShape.iog.og[0].gco";
connectAttr "groupId8077.id" "finger_R1_crvShape.iog.og[1].gid";
connectAttr "tweakSet254.mwc" "finger_R1_crvShape.iog.og[1].gco";
connectAttr "finger_R2_blade_aimConstraint1.crx" "finger_R2_blade.rx";
connectAttr "finger_R2_blade_aimConstraint1.cry" "finger_R2_blade.ry";
connectAttr "finger_R2_blade_aimConstraint1.crz" "finger_R2_blade.rz";
connectAttr "finger_R2_blade_pointConstraint1.ctx" "finger_R2_blade.tx";
connectAttr "finger_R2_blade_pointConstraint1.cty" "finger_R2_blade.ty";
connectAttr "finger_R2_blade_pointConstraint1.ctz" "finger_R2_blade.tz";
connectAttr "finger_R2_blade.pim" "finger_R2_blade_aimConstraint1.cpim";
connectAttr "finger_R2_blade.t" "finger_R2_blade_aimConstraint1.ct";
connectAttr "finger_R2_blade.rp" "finger_R2_blade_aimConstraint1.crp";
connectAttr "finger_R2_blade.rpt" "finger_R2_blade_aimConstraint1.crt";
connectAttr "finger_R2_blade.ro" "finger_R2_blade_aimConstraint1.cro";
connectAttr "finger_R2_0_loc.t" "finger_R2_blade_aimConstraint1.tg[0].tt";
connectAttr "finger_R2_0_loc.rp" "finger_R2_blade_aimConstraint1.tg[0].trp";
connectAttr "finger_R2_0_loc.rpt" "finger_R2_blade_aimConstraint1.tg[0].trt";
connectAttr "finger_R2_0_loc.pm" "finger_R2_blade_aimConstraint1.tg[0].tpm";
connectAttr "finger_R2_blade_aimConstraint1.w0" "finger_R2_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "finger_R2_root.wm" "finger_R2_blade_aimConstraint1.wum";
connectAttr "finger_R2_blade.pim" "finger_R2_blade_pointConstraint1.cpim";
connectAttr "finger_R2_blade.rp" "finger_R2_blade_pointConstraint1.crp";
connectAttr "finger_R2_blade.rpt" "finger_R2_blade_pointConstraint1.crt";
connectAttr "finger_R2_root.t" "finger_R2_blade_pointConstraint1.tg[0].tt";
connectAttr "finger_R2_root.rp" "finger_R2_blade_pointConstraint1.tg[0].trp";
connectAttr "finger_R2_root.rpt" "finger_R2_blade_pointConstraint1.tg[0].trt";
connectAttr "finger_R2_root.pm" "finger_R2_blade_pointConstraint1.tg[0].tpm";
connectAttr "finger_R2_blade_pointConstraint1.w0" "finger_R2_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns109.og[0]" "finger_R2_crvShape.cr";
connectAttr "tweak255.pl[0].cp[0]" "finger_R2_crvShape.twl";
connectAttr "mgear_curveCns109GroupId.id" "finger_R2_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns109Set.mwc" "finger_R2_crvShape.iog.og[0].gco";
connectAttr "groupId8079.id" "finger_R2_crvShape.iog.og[1].gid";
connectAttr "tweakSet255.mwc" "finger_R2_crvShape.iog.og[1].gco";
connectAttr "finger_R3_blade_aimConstraint1.crx" "finger_R3_blade.rx";
connectAttr "finger_R3_blade_aimConstraint1.cry" "finger_R3_blade.ry";
connectAttr "finger_R3_blade_aimConstraint1.crz" "finger_R3_blade.rz";
connectAttr "finger_R3_blade_pointConstraint1.ctx" "finger_R3_blade.tx";
connectAttr "finger_R3_blade_pointConstraint1.cty" "finger_R3_blade.ty";
connectAttr "finger_R3_blade_pointConstraint1.ctz" "finger_R3_blade.tz";
connectAttr "finger_R3_blade.pim" "finger_R3_blade_aimConstraint1.cpim";
connectAttr "finger_R3_blade.t" "finger_R3_blade_aimConstraint1.ct";
connectAttr "finger_R3_blade.rp" "finger_R3_blade_aimConstraint1.crp";
connectAttr "finger_R3_blade.rpt" "finger_R3_blade_aimConstraint1.crt";
connectAttr "finger_R3_blade.ro" "finger_R3_blade_aimConstraint1.cro";
connectAttr "finger_R3_0_loc.t" "finger_R3_blade_aimConstraint1.tg[0].tt";
connectAttr "finger_R3_0_loc.rp" "finger_R3_blade_aimConstraint1.tg[0].trp";
connectAttr "finger_R3_0_loc.rpt" "finger_R3_blade_aimConstraint1.tg[0].trt";
connectAttr "finger_R3_0_loc.pm" "finger_R3_blade_aimConstraint1.tg[0].tpm";
connectAttr "finger_R3_blade_aimConstraint1.w0" "finger_R3_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "finger_R3_root.wm" "finger_R3_blade_aimConstraint1.wum";
connectAttr "finger_R3_blade.pim" "finger_R3_blade_pointConstraint1.cpim";
connectAttr "finger_R3_blade.rp" "finger_R3_blade_pointConstraint1.crp";
connectAttr "finger_R3_blade.rpt" "finger_R3_blade_pointConstraint1.crt";
connectAttr "finger_R3_root.t" "finger_R3_blade_pointConstraint1.tg[0].tt";
connectAttr "finger_R3_root.rp" "finger_R3_blade_pointConstraint1.tg[0].trp";
connectAttr "finger_R3_root.rpt" "finger_R3_blade_pointConstraint1.tg[0].trt";
connectAttr "finger_R3_root.pm" "finger_R3_blade_pointConstraint1.tg[0].tpm";
connectAttr "finger_R3_blade_pointConstraint1.w0" "finger_R3_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns110.og[0]" "finger_R3_crvShape.cr";
connectAttr "tweak256.pl[0].cp[0]" "finger_R3_crvShape.twl";
connectAttr "mgear_curveCns110GroupId.id" "finger_R3_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns110Set.mwc" "finger_R3_crvShape.iog.og[0].gco";
connectAttr "groupId8081.id" "finger_R3_crvShape.iog.og[1].gid";
connectAttr "tweakSet256.mwc" "finger_R3_crvShape.iog.og[1].gco";
connectAttr "mgear_curveCns105.og[0]" "arm_R0_crvShape.cr";
connectAttr "tweak251.pl[0].cp[0]" "arm_R0_crvShape.twl";
connectAttr "mgear_curveCns105GroupId.id" "arm_R0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns105Set.mwc" "arm_R0_crvShape.iog.og[0].gco";
connectAttr "groupId8071.id" "arm_R0_crvShape.iog.og[1].gid";
connectAttr "tweakSet251.mwc" "arm_R0_crvShape.iog.og[1].gco";
connectAttr "shoulder_R0_blade_aimConstraint1.crx" "shoulder_R0_blade.rx";
connectAttr "shoulder_R0_blade_aimConstraint1.cry" "shoulder_R0_blade.ry";
connectAttr "shoulder_R0_blade_aimConstraint1.crz" "shoulder_R0_blade.rz";
connectAttr "shoulder_R0_blade_pointConstraint1.ctx" "shoulder_R0_blade.tx";
connectAttr "shoulder_R0_blade_pointConstraint1.cty" "shoulder_R0_blade.ty";
connectAttr "shoulder_R0_blade_pointConstraint1.ctz" "shoulder_R0_blade.tz";
connectAttr "shoulder_R0_blade.pim" "shoulder_R0_blade_aimConstraint1.cpim";
connectAttr "shoulder_R0_blade.t" "shoulder_R0_blade_aimConstraint1.ct";
connectAttr "shoulder_R0_blade.rp" "shoulder_R0_blade_aimConstraint1.crp";
connectAttr "shoulder_R0_blade.rpt" "shoulder_R0_blade_aimConstraint1.crt";
connectAttr "shoulder_R0_blade.ro" "shoulder_R0_blade_aimConstraint1.cro";
connectAttr "shoulder_R0_tip.t" "shoulder_R0_blade_aimConstraint1.tg[0].tt";
connectAttr "shoulder_R0_tip.rp" "shoulder_R0_blade_aimConstraint1.tg[0].trp";
connectAttr "shoulder_R0_tip.rpt" "shoulder_R0_blade_aimConstraint1.tg[0].trt";
connectAttr "shoulder_R0_tip.pm" "shoulder_R0_blade_aimConstraint1.tg[0].tpm";
connectAttr "shoulder_R0_blade_aimConstraint1.w0" "shoulder_R0_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "shoulder_R0_root.wm" "shoulder_R0_blade_aimConstraint1.wum";
connectAttr "shoulder_R0_blade.pim" "shoulder_R0_blade_pointConstraint1.cpim";
connectAttr "shoulder_R0_blade.rp" "shoulder_R0_blade_pointConstraint1.crp";
connectAttr "shoulder_R0_blade.rpt" "shoulder_R0_blade_pointConstraint1.crt";
connectAttr "shoulder_R0_root.t" "shoulder_R0_blade_pointConstraint1.tg[0].tt";
connectAttr "shoulder_R0_root.rp" "shoulder_R0_blade_pointConstraint1.tg[0].trp"
		;
connectAttr "shoulder_R0_root.rpt" "shoulder_R0_blade_pointConstraint1.tg[0].trt"
		;
connectAttr "shoulder_R0_root.pm" "shoulder_R0_blade_pointConstraint1.tg[0].tpm"
		;
connectAttr "shoulder_R0_blade_pointConstraint1.w0" "shoulder_R0_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns104.og[0]" "shoulder_R0_crvShape.cr";
connectAttr "tweak250.pl[0].cp[0]" "shoulder_R0_crvShape.twl";
connectAttr "mgear_curveCns104GroupId.id" "shoulder_R0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns104Set.mwc" "shoulder_R0_crvShape.iog.og[0].gco";
connectAttr "groupId8069.id" "shoulder_R0_crvShape.iog.og[1].gid";
connectAttr "tweakSet250.mwc" "shoulder_R0_crvShape.iog.og[1].gco";
connectAttr "spine_C0_blade_aimConstraint1.crx" "spine_C0_blade.rx";
connectAttr "spine_C0_blade_aimConstraint1.cry" "spine_C0_blade.ry";
connectAttr "spine_C0_blade_aimConstraint1.crz" "spine_C0_blade.rz";
connectAttr "spine_C0_blade_pointConstraint1.ctx" "spine_C0_blade.tx";
connectAttr "spine_C0_blade_pointConstraint1.cty" "spine_C0_blade.ty";
connectAttr "spine_C0_blade_pointConstraint1.ctz" "spine_C0_blade.tz";
connectAttr "spine_C0_blade.pim" "spine_C0_blade_aimConstraint1.cpim";
connectAttr "spine_C0_blade.t" "spine_C0_blade_aimConstraint1.ct";
connectAttr "spine_C0_blade.rp" "spine_C0_blade_aimConstraint1.crp";
connectAttr "spine_C0_blade.rpt" "spine_C0_blade_aimConstraint1.crt";
connectAttr "spine_C0_blade.ro" "spine_C0_blade_aimConstraint1.cro";
connectAttr "spine_C0_eff.t" "spine_C0_blade_aimConstraint1.tg[0].tt";
connectAttr "spine_C0_eff.rp" "spine_C0_blade_aimConstraint1.tg[0].trp";
connectAttr "spine_C0_eff.rpt" "spine_C0_blade_aimConstraint1.tg[0].trt";
connectAttr "spine_C0_eff.pm" "spine_C0_blade_aimConstraint1.tg[0].tpm";
connectAttr "spine_C0_blade_aimConstraint1.w0" "spine_C0_blade_aimConstraint1.tg[0].tw"
		;
connectAttr "spine_C0_root.wm" "spine_C0_blade_aimConstraint1.wum";
connectAttr "spine_C0_blade.pim" "spine_C0_blade_pointConstraint1.cpim";
connectAttr "spine_C0_blade.rp" "spine_C0_blade_pointConstraint1.crp";
connectAttr "spine_C0_blade.rpt" "spine_C0_blade_pointConstraint1.crt";
connectAttr "spine_C0_root.t" "spine_C0_blade_pointConstraint1.tg[0].tt";
connectAttr "spine_C0_root.rp" "spine_C0_blade_pointConstraint1.tg[0].trp";
connectAttr "spine_C0_root.rpt" "spine_C0_blade_pointConstraint1.tg[0].trt";
connectAttr "spine_C0_root.pm" "spine_C0_blade_pointConstraint1.tg[0].tpm";
connectAttr "spine_C0_blade_pointConstraint1.w0" "spine_C0_blade_pointConstraint1.tg[0].tw"
		;
connectAttr "mgear_curveCns72.og[0]" "spine_C0_crvShape.cr";
connectAttr "tweak218.pl[0].cp[0]" "spine_C0_crvShape.twl";
connectAttr "mgear_curveCns72GroupId.id" "spine_C0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns72Set.mwc" "spine_C0_crvShape.iog.og[0].gco";
connectAttr "groupId8005.id" "spine_C0_crvShape.iog.og[1].gid";
connectAttr "tweakSet218.mwc" "spine_C0_crvShape.iog.og[1].gco";
connectAttr "leg_L1_root_st_profile.o" "leg_L0_root.st_profile";
connectAttr "leg_L1_root_sq_profile.o" "leg_L0_root.sq_profile";
connectAttr "mgear_curveCns97.og[0]" "foot_L0_crvShape.cr";
connectAttr "tweak243.pl[0].cp[0]" "foot_L0_crvShape.twl";
connectAttr "mgear_curveCns97GroupId.id" "foot_L0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns97Set.mwc" "foot_L0_crvShape.iog.og[0].gco";
connectAttr "groupId8055.id" "foot_L0_crvShape.iog.og[1].gid";
connectAttr "tweakSet243.mwc" "foot_L0_crvShape.iog.og[1].gco";
connectAttr "mgear_curveCns98.og[0]" "foot_L0_Shape1.cr";
connectAttr "tweak244.pl[0].cp[0]" "foot_L0_Shape1.twl";
connectAttr "mgear_curveCns98GroupId.id" "foot_L0_Shape1.iog.og[0].gid";
connectAttr "mgear_curveCns98Set.mwc" "foot_L0_Shape1.iog.og[0].gco";
connectAttr "groupId8057.id" "foot_L0_Shape1.iog.og[1].gid";
connectAttr "tweakSet244.mwc" "foot_L0_Shape1.iog.og[1].gco";
connectAttr "mgear_curveCns96.og[0]" "leg_L0_crvShape.cr";
connectAttr "tweak242.pl[0].cp[0]" "leg_L0_crvShape.twl";
connectAttr "mgear_curveCns96GroupId.id" "leg_L0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns96Set.mwc" "leg_L0_crvShape.iog.og[0].gco";
connectAttr "groupId8053.id" "leg_L0_crvShape.iog.og[1].gid";
connectAttr "tweakSet242.mwc" "leg_L0_crvShape.iog.og[1].gco";
connectAttr "leg_R0_root_st_profile.o" "leg_R0_root.st_profile";
connectAttr "leg_R0_root_sq_profile.o" "leg_R0_root.sq_profile";
connectAttr "mgear_curveCns112.og[0]" "foot_R0_crvShape.cr";
connectAttr "tweak258.pl[0].cp[0]" "foot_R0_crvShape.twl";
connectAttr "mgear_curveCns112GroupId.id" "foot_R0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns112Set.mwc" "foot_R0_crvShape.iog.og[0].gco";
connectAttr "groupId8085.id" "foot_R0_crvShape.iog.og[1].gid";
connectAttr "tweakSet258.mwc" "foot_R0_crvShape.iog.og[1].gco";
connectAttr "mgear_curveCns113.og[0]" "foot_R0_Shape1.cr";
connectAttr "tweak259.pl[0].cp[0]" "foot_R0_Shape1.twl";
connectAttr "mgear_curveCns113GroupId.id" "foot_R0_Shape1.iog.og[0].gid";
connectAttr "mgear_curveCns113Set.mwc" "foot_R0_Shape1.iog.og[0].gco";
connectAttr "groupId8087.id" "foot_R0_Shape1.iog.og[1].gid";
connectAttr "tweakSet259.mwc" "foot_R0_Shape1.iog.og[1].gco";
connectAttr "mgear_curveCns111.og[0]" "leg_R0_crvShape.cr";
connectAttr "tweak257.pl[0].cp[0]" "leg_R0_crvShape.twl";
connectAttr "mgear_curveCns111GroupId.id" "leg_R0_crvShape.iog.og[0].gid";
connectAttr "mgear_curveCns111Set.mwc" "leg_R0_crvShape.iog.og[0].gco";
connectAttr "groupId8083.id" "leg_R0_crvShape.iog.og[1].gid";
connectAttr "tweakSet257.mwc" "leg_R0_crvShape.iog.og[1].gco";
connectAttr "mgear_curveCns99GroupParts.og" "mgear_curveCns99.ip[0].ig";
connectAttr "mgear_curveCns99GroupId.id" "mgear_curveCns99.ip[0].gi";
connectAttr "thumb_L0_root.wm" "mgear_curveCns99.inputs[0]";
connectAttr "thumb_L0_0_loc.wm" "mgear_curveCns99.inputs[1]";
connectAttr "thumb_L0_1_loc.wm" "mgear_curveCns99.inputs[2]";
connectAttr "thumb_L0_2_loc.wm" "mgear_curveCns99.inputs[3]";
connectAttr "mgear_curveCns99GroupId.msg" "mgear_curveCns99Set.gn" -na;
connectAttr "thumb_L0_crvShape.iog.og[0]" "mgear_curveCns99Set.dsm" -na;
connectAttr "mgear_curveCns99.msg" "mgear_curveCns99Set.ub[0]";
connectAttr "tweak245.og[0]" "mgear_curveCns99GroupParts.ig";
connectAttr "mgear_curveCns99GroupId.id" "mgear_curveCns99GroupParts.gi";
connectAttr "groupParts490.og" "tweak245.ip[0].ig";
connectAttr "groupId8059.id" "tweak245.ip[0].gi";
connectAttr "groupId8059.msg" "tweakSet245.gn" -na;
connectAttr "thumb_L0_crvShape.iog.og[1]" "tweakSet245.dsm" -na;
connectAttr "tweak245.msg" "tweakSet245.ub[0]";
connectAttr "thumb_L0_crvShapeOrig.ws" "groupParts490.ig";
connectAttr "groupId8059.id" "groupParts490.gi";
connectAttr "mgear_curveCns100GroupParts.og" "mgear_curveCns100.ip[0].ig";
connectAttr "mgear_curveCns100GroupId.id" "mgear_curveCns100.ip[0].gi";
connectAttr "finger_L0_root.wm" "mgear_curveCns100.inputs[0]";
connectAttr "finger_L0_0_loc.wm" "mgear_curveCns100.inputs[1]";
connectAttr "finger_L0_1_loc.wm" "mgear_curveCns100.inputs[2]";
connectAttr "finger_L0_2_loc.wm" "mgear_curveCns100.inputs[3]";
connectAttr "mgear_curveCns100GroupId.msg" "mgear_curveCns100Set.gn" -na;
connectAttr "finger_L0_crvShape.iog.og[0]" "mgear_curveCns100Set.dsm" -na;
connectAttr "mgear_curveCns100.msg" "mgear_curveCns100Set.ub[0]";
connectAttr "tweak246.og[0]" "mgear_curveCns100GroupParts.ig";
connectAttr "mgear_curveCns100GroupId.id" "mgear_curveCns100GroupParts.gi";
connectAttr "groupParts492.og" "tweak246.ip[0].ig";
connectAttr "groupId8061.id" "tweak246.ip[0].gi";
connectAttr "groupId8061.msg" "tweakSet246.gn" -na;
connectAttr "finger_L0_crvShape.iog.og[1]" "tweakSet246.dsm" -na;
connectAttr "tweak246.msg" "tweakSet246.ub[0]";
connectAttr "finger_L0_crvShapeOrig.ws" "groupParts492.ig";
connectAttr "groupId8061.id" "groupParts492.gi";
connectAttr "mgear_curveCns101GroupParts.og" "mgear_curveCns101.ip[0].ig";
connectAttr "mgear_curveCns101GroupId.id" "mgear_curveCns101.ip[0].gi";
connectAttr "finger_L1_root.wm" "mgear_curveCns101.inputs[0]";
connectAttr "finger_L1_0_loc.wm" "mgear_curveCns101.inputs[1]";
connectAttr "finger_L1_1_loc.wm" "mgear_curveCns101.inputs[2]";
connectAttr "finger_L1_2_loc.wm" "mgear_curveCns101.inputs[3]";
connectAttr "mgear_curveCns101GroupId.msg" "mgear_curveCns101Set.gn" -na;
connectAttr "finger_L1_crvShape.iog.og[0]" "mgear_curveCns101Set.dsm" -na;
connectAttr "mgear_curveCns101.msg" "mgear_curveCns101Set.ub[0]";
connectAttr "tweak247.og[0]" "mgear_curveCns101GroupParts.ig";
connectAttr "mgear_curveCns101GroupId.id" "mgear_curveCns101GroupParts.gi";
connectAttr "groupParts494.og" "tweak247.ip[0].ig";
connectAttr "groupId8063.id" "tweak247.ip[0].gi";
connectAttr "groupId8063.msg" "tweakSet247.gn" -na;
connectAttr "finger_L1_crvShape.iog.og[1]" "tweakSet247.dsm" -na;
connectAttr "tweak247.msg" "tweakSet247.ub[0]";
connectAttr "finger_L1_crvShapeOrig.ws" "groupParts494.ig";
connectAttr "groupId8063.id" "groupParts494.gi";
connectAttr "mgear_curveCns102GroupParts.og" "mgear_curveCns102.ip[0].ig";
connectAttr "mgear_curveCns102GroupId.id" "mgear_curveCns102.ip[0].gi";
connectAttr "finger_L2_root.wm" "mgear_curveCns102.inputs[0]";
connectAttr "finger_L2_0_loc.wm" "mgear_curveCns102.inputs[1]";
connectAttr "finger_L2_1_loc.wm" "mgear_curveCns102.inputs[2]";
connectAttr "finger_L2_2_loc.wm" "mgear_curveCns102.inputs[3]";
connectAttr "mgear_curveCns102GroupId.msg" "mgear_curveCns102Set.gn" -na;
connectAttr "finger_L2_crvShape.iog.og[0]" "mgear_curveCns102Set.dsm" -na;
connectAttr "mgear_curveCns102.msg" "mgear_curveCns102Set.ub[0]";
connectAttr "tweak248.og[0]" "mgear_curveCns102GroupParts.ig";
connectAttr "mgear_curveCns102GroupId.id" "mgear_curveCns102GroupParts.gi";
connectAttr "groupParts496.og" "tweak248.ip[0].ig";
connectAttr "groupId8065.id" "tweak248.ip[0].gi";
connectAttr "groupId8065.msg" "tweakSet248.gn" -na;
connectAttr "finger_L2_crvShape.iog.og[1]" "tweakSet248.dsm" -na;
connectAttr "tweak248.msg" "tweakSet248.ub[0]";
connectAttr "finger_L2_crvShapeOrig.ws" "groupParts496.ig";
connectAttr "groupId8065.id" "groupParts496.gi";
connectAttr "mgear_curveCns103GroupParts.og" "mgear_curveCns103.ip[0].ig";
connectAttr "mgear_curveCns103GroupId.id" "mgear_curveCns103.ip[0].gi";
connectAttr "finger_L3_root.wm" "mgear_curveCns103.inputs[0]";
connectAttr "finger_L3_0_loc.wm" "mgear_curveCns103.inputs[1]";
connectAttr "finger_L3_1_loc.wm" "mgear_curveCns103.inputs[2]";
connectAttr "finger_L3_2_loc.wm" "mgear_curveCns103.inputs[3]";
connectAttr "mgear_curveCns103GroupId.msg" "mgear_curveCns103Set.gn" -na;
connectAttr "finger_L3_crvShape.iog.og[0]" "mgear_curveCns103Set.dsm" -na;
connectAttr "mgear_curveCns103.msg" "mgear_curveCns103Set.ub[0]";
connectAttr "tweak249.og[0]" "mgear_curveCns103GroupParts.ig";
connectAttr "mgear_curveCns103GroupId.id" "mgear_curveCns103GroupParts.gi";
connectAttr "groupParts498.og" "tweak249.ip[0].ig";
connectAttr "groupId8067.id" "tweak249.ip[0].gi";
connectAttr "groupId8067.msg" "tweakSet249.gn" -na;
connectAttr "finger_L3_crvShape.iog.og[1]" "tweakSet249.dsm" -na;
connectAttr "tweak249.msg" "tweakSet249.ub[0]";
connectAttr "finger_L3_crvShapeOrig.ws" "groupParts498.ig";
connectAttr "groupId8067.id" "groupParts498.gi";
connectAttr "mgear_curveCns74GroupParts.og" "mgear_curveCns74.ip[0].ig";
connectAttr "mgear_curveCns74GroupId.id" "mgear_curveCns74.ip[0].gi";
connectAttr "arm_L0_root.wm" "mgear_curveCns74.inputs[0]";
connectAttr "arm_L0_elbow.wm" "mgear_curveCns74.inputs[1]";
connectAttr "arm_L0_wrist.wm" "mgear_curveCns74.inputs[2]";
connectAttr "arm_L0_eff.wm" "mgear_curveCns74.inputs[3]";
connectAttr "mgear_curveCns74GroupId.msg" "mgear_curveCns74Set.gn" -na;
connectAttr "arm_L0_crvShape.iog.og[0]" "mgear_curveCns74Set.dsm" -na;
connectAttr "mgear_curveCns74.msg" "mgear_curveCns74Set.ub[0]";
connectAttr "tweak220.og[0]" "mgear_curveCns74GroupParts.ig";
connectAttr "mgear_curveCns74GroupId.id" "mgear_curveCns74GroupParts.gi";
connectAttr "groupParts440.og" "tweak220.ip[0].ig";
connectAttr "groupId8009.id" "tweak220.ip[0].gi";
connectAttr "groupId8009.msg" "tweakSet220.gn" -na;
connectAttr "arm_L0_crvShape.iog.og[1]" "tweakSet220.dsm" -na;
connectAttr "tweak220.msg" "tweakSet220.ub[0]";
connectAttr "arm_L0_crvShapeOrig.ws" "groupParts440.ig";
connectAttr "groupId8009.id" "groupParts440.gi";
connectAttr "mgear_curveCns73GroupParts.og" "mgear_curveCns73.ip[0].ig";
connectAttr "mgear_curveCns73GroupId.id" "mgear_curveCns73.ip[0].gi";
connectAttr "shoulder_L0_root.wm" "mgear_curveCns73.inputs[0]";
connectAttr "shoulder_L0_tip.wm" "mgear_curveCns73.inputs[1]";
connectAttr "mgear_curveCns73GroupId.msg" "mgear_curveCns73Set.gn" -na;
connectAttr "shoulder_L0_crvShape.iog.og[0]" "mgear_curveCns73Set.dsm" -na;
connectAttr "mgear_curveCns73.msg" "mgear_curveCns73Set.ub[0]";
connectAttr "tweak219.og[0]" "mgear_curveCns73GroupParts.ig";
connectAttr "mgear_curveCns73GroupId.id" "mgear_curveCns73GroupParts.gi";
connectAttr "groupParts438.og" "tweak219.ip[0].ig";
connectAttr "groupId8007.id" "tweak219.ip[0].gi";
connectAttr "groupId8007.msg" "tweakSet219.gn" -na;
connectAttr "shoulder_L0_crvShape.iog.og[1]" "tweakSet219.dsm" -na;
connectAttr "tweak219.msg" "tweakSet219.ub[0]";
connectAttr "shoulder_L0_crvShapeOrig.ws" "groupParts438.ig";
connectAttr "groupId8007.id" "groupParts438.gi";
connectAttr "mgear_curveCns84GroupParts.og" "mgear_curveCns84.ip[0].ig";
connectAttr "mgear_curveCns84GroupId.id" "mgear_curveCns84.ip[0].gi";
connectAttr "mouth_C0_lipup.wm" "mgear_curveCns84.inputs[0]";
connectAttr "mouth_C0_rotcenter.wm" "mgear_curveCns84.inputs[1]";
connectAttr "mgear_curveCns84GroupId.msg" "mgear_curveCns84Set.gn" -na;
connectAttr "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[0]" "mgear_curveCns84Set.dsm"
		 -na;
connectAttr "mgear_curveCns84.msg" "mgear_curveCns84Set.ub[0]";
connectAttr "tweak230.og[0]" "mgear_curveCns84GroupParts.ig";
connectAttr "mgear_curveCns84GroupId.id" "mgear_curveCns84GroupParts.gi";
connectAttr "groupParts460.og" "tweak230.ip[0].ig";
connectAttr "groupId8029.id" "tweak230.ip[0].gi";
connectAttr "groupId8029.msg" "tweakSet230.gn" -na;
connectAttr "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[1]" "tweakSet230.dsm"
		 -na;
connectAttr "tweak230.msg" "tweakSet230.ub[0]";
connectAttr "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_lipup|mouth_C0_C1_crv|mouth_C0_C1_crvShapeOrig.ws" "groupParts460.ig"
		;
connectAttr "groupId8029.id" "groupParts460.gi";
connectAttr "mgear_curveCns85GroupParts.og" "mgear_curveCns85.ip[0].ig";
connectAttr "mgear_curveCns85GroupId.id" "mgear_curveCns85.ip[0].gi";
connectAttr "mouth_C0_liplow.wm" "mgear_curveCns85.inputs[0]";
connectAttr "mouth_C0_rotcenter.wm" "mgear_curveCns85.inputs[1]";
connectAttr "mgear_curveCns85GroupId.msg" "mgear_curveCns85Set.gn" -na;
connectAttr "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[0]" "mgear_curveCns85Set.dsm"
		 -na;
connectAttr "mgear_curveCns85.msg" "mgear_curveCns85Set.ub[0]";
connectAttr "tweak231.og[0]" "mgear_curveCns85GroupParts.ig";
connectAttr "mgear_curveCns85GroupId.id" "mgear_curveCns85GroupParts.gi";
connectAttr "groupParts462.og" "tweak231.ip[0].ig";
connectAttr "groupId8031.id" "tweak231.ip[0].gi";
connectAttr "groupId8031.msg" "tweakSet231.gn" -na;
connectAttr "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShape.iog.og[1]" "tweakSet231.dsm"
		 -na;
connectAttr "tweak231.msg" "tweakSet231.ub[0]";
connectAttr "|biped_guide|local_C0_root|body_C0_root|spine_C0_root|spine_C0_eff|neck_C0_root|neck_C0_neck|neck_C0_head|mouth_C0_root|mouth_C0_rotcenter|mouth_C0_liplow|mouth_C0_C1_crv|mouth_C0_C1_crvShapeOrig.ws" "groupParts462.ig"
		;
connectAttr "groupId8031.id" "groupParts462.gi";
connectAttr "mgear_curveCns83GroupParts.og" "mgear_curveCns83.ip[0].ig";
connectAttr "mgear_curveCns83GroupId.id" "mgear_curveCns83.ip[0].gi";
connectAttr "mouth_C0_root.wm" "mgear_curveCns83.inputs[0]";
connectAttr "mouth_C0_rotcenter.wm" "mgear_curveCns83.inputs[1]";
connectAttr "mgear_curveCns83GroupId.msg" "mgear_curveCns83Set.gn" -na;
connectAttr "mouth_C0_crvShape.iog.og[0]" "mgear_curveCns83Set.dsm" -na;
connectAttr "mgear_curveCns83.msg" "mgear_curveCns83Set.ub[0]";
connectAttr "tweak229.og[0]" "mgear_curveCns83GroupParts.ig";
connectAttr "mgear_curveCns83GroupId.id" "mgear_curveCns83GroupParts.gi";
connectAttr "groupParts458.og" "tweak229.ip[0].ig";
connectAttr "groupId8027.id" "tweak229.ip[0].gi";
connectAttr "groupId8027.msg" "tweakSet229.gn" -na;
connectAttr "mouth_C0_crvShape.iog.og[1]" "tweakSet229.dsm" -na;
connectAttr "tweak229.msg" "tweakSet229.ub[0]";
connectAttr "mouth_C0_crvShapeOrig.ws" "groupParts458.ig";
connectAttr "groupId8027.id" "groupParts458.gi";
connectAttr "mgear_curveCns86GroupParts.og" "mgear_curveCns86.ip[0].ig";
connectAttr "mgear_curveCns86GroupId.id" "mgear_curveCns86.ip[0].gi";
connectAttr "mouth_C0_root.wm" "mgear_curveCns86.inputs[0]";
connectAttr "mouth_C0_jaw.wm" "mgear_curveCns86.inputs[1]";
connectAttr "mgear_curveCns86GroupId.msg" "mgear_curveCns86Set.gn" -na;
connectAttr "mouth_C0_crv1Shape.iog.og[0]" "mgear_curveCns86Set.dsm" -na;
connectAttr "mgear_curveCns86.msg" "mgear_curveCns86Set.ub[0]";
connectAttr "tweak232.og[0]" "mgear_curveCns86GroupParts.ig";
connectAttr "mgear_curveCns86GroupId.id" "mgear_curveCns86GroupParts.gi";
connectAttr "groupParts464.og" "tweak232.ip[0].ig";
connectAttr "groupId8033.id" "tweak232.ip[0].gi";
connectAttr "groupId8033.msg" "tweakSet232.gn" -na;
connectAttr "mouth_C0_crv1Shape.iog.og[1]" "tweakSet232.dsm" -na;
connectAttr "tweak232.msg" "tweakSet232.ub[0]";
connectAttr "mouth_C0_crv1ShapeOrig.ws" "groupParts464.ig";
connectAttr "groupId8033.id" "groupParts464.gi";
connectAttr "mgear_curveCns87GroupParts.og" "mgear_curveCns87.ip[0].ig";
connectAttr "mgear_curveCns87GroupId.id" "mgear_curveCns87.ip[0].gi";
connectAttr "eye_L0_root.wm" "mgear_curveCns87.inputs[0]";
connectAttr "eye_L0_look.wm" "mgear_curveCns87.inputs[1]";
connectAttr "mgear_curveCns87GroupId.msg" "mgear_curveCns87Set.gn" -na;
connectAttr "eye_L0_crvShape.iog.og[0]" "mgear_curveCns87Set.dsm" -na;
connectAttr "mgear_curveCns87.msg" "mgear_curveCns87Set.ub[0]";
connectAttr "tweak233.og[0]" "mgear_curveCns87GroupParts.ig";
connectAttr "mgear_curveCns87GroupId.id" "mgear_curveCns87GroupParts.gi";
connectAttr "groupParts466.og" "tweak233.ip[0].ig";
connectAttr "groupId8035.id" "tweak233.ip[0].gi";
connectAttr "groupId8035.msg" "tweakSet233.gn" -na;
connectAttr "eye_L0_crvShape.iog.og[1]" "tweakSet233.dsm" -na;
connectAttr "tweak233.msg" "tweakSet233.ub[0]";
connectAttr "eye_L0_crvShapeOrig.ws" "groupParts466.ig";
connectAttr "groupId8035.id" "groupParts466.gi";
connectAttr "mgear_curveCns88GroupParts.og" "mgear_curveCns88.ip[0].ig";
connectAttr "mgear_curveCns88GroupId.id" "mgear_curveCns88.ip[0].gi";
connectAttr "eye_R0_root.wm" "mgear_curveCns88.inputs[0]";
connectAttr "eye_R0_look.wm" "mgear_curveCns88.inputs[1]";
connectAttr "mgear_curveCns88GroupId.msg" "mgear_curveCns88Set.gn" -na;
connectAttr "eye_R0_crvShape.iog.og[0]" "mgear_curveCns88Set.dsm" -na;
connectAttr "mgear_curveCns88.msg" "mgear_curveCns88Set.ub[0]";
connectAttr "tweak234.og[0]" "mgear_curveCns88GroupParts.ig";
connectAttr "mgear_curveCns88GroupId.id" "mgear_curveCns88GroupParts.gi";
connectAttr "groupParts468.og" "tweak234.ip[0].ig";
connectAttr "groupId8037.id" "tweak234.ip[0].gi";
connectAttr "groupId8037.msg" "tweakSet234.gn" -na;
connectAttr "eye_R0_crvShape.iog.og[1]" "tweakSet234.dsm" -na;
connectAttr "tweak234.msg" "tweakSet234.ub[0]";
connectAttr "eye_R0_crvShapeOrig.ws" "groupParts468.ig";
connectAttr "groupId8037.id" "groupParts468.gi";
connectAttr "mgear_curveCns81GroupParts.og" "mgear_curveCns81.ip[0].ig";
connectAttr "mgear_curveCns81GroupId.id" "mgear_curveCns81.ip[0].gi";
connectAttr "neck_C0_neck.wm" "mgear_curveCns81.inputs[0]";
connectAttr "neck_C0_head.wm" "mgear_curveCns81.inputs[1]";
connectAttr "neck_C0_eff.wm" "mgear_curveCns81.inputs[2]";
connectAttr "mgear_curveCns81GroupId.msg" "mgear_curveCns81Set.gn" -na;
connectAttr "neck_C0_head_crvShape.iog.og[0]" "mgear_curveCns81Set.dsm" -na;
connectAttr "mgear_curveCns81.msg" "mgear_curveCns81Set.ub[0]";
connectAttr "tweak227.og[0]" "mgear_curveCns81GroupParts.ig";
connectAttr "mgear_curveCns81GroupId.id" "mgear_curveCns81GroupParts.gi";
connectAttr "groupParts454.og" "tweak227.ip[0].ig";
connectAttr "groupId8023.id" "tweak227.ip[0].gi";
connectAttr "groupId8023.msg" "tweakSet227.gn" -na;
connectAttr "neck_C0_head_crvShape.iog.og[1]" "tweakSet227.dsm" -na;
connectAttr "tweak227.msg" "tweakSet227.ub[0]";
connectAttr "neck_C0_head_crvShapeOrig.ws" "groupParts454.ig";
connectAttr "groupId8023.id" "groupParts454.gi";
connectAttr "mgear_curveCns80GroupParts.og" "mgear_curveCns80.ip[0].ig";
connectAttr "mgear_curveCns80GroupId.id" "mgear_curveCns80.ip[0].gi";
connectAttr "neck_C0_root.wm" "mgear_curveCns80.inputs[0]";
connectAttr "neck_C0_tan0.wm" "mgear_curveCns80.inputs[1]";
connectAttr "neck_C0_tan1.wm" "mgear_curveCns80.inputs[2]";
connectAttr "neck_C0_neck.wm" "mgear_curveCns80.inputs[3]";
connectAttr "mgear_curveCns80GroupId.msg" "mgear_curveCns80Set.gn" -na;
connectAttr "neck_C0_neck_crvShape.iog.og[0]" "mgear_curveCns80Set.dsm" -na;
connectAttr "mgear_curveCns80.msg" "mgear_curveCns80Set.ub[0]";
connectAttr "tweak226.og[0]" "mgear_curveCns80GroupParts.ig";
connectAttr "mgear_curveCns80GroupId.id" "mgear_curveCns80GroupParts.gi";
connectAttr "groupParts452.og" "tweak226.ip[0].ig";
connectAttr "groupId8021.id" "tweak226.ip[0].gi";
connectAttr "groupId8021.msg" "tweakSet226.gn" -na;
connectAttr "neck_C0_neck_crvShape.iog.og[1]" "tweakSet226.dsm" -na;
connectAttr "tweak226.msg" "tweakSet226.ub[0]";
connectAttr "neck_C0_neck_crvShapeOrig.ws" "groupParts452.ig";
connectAttr "groupId8021.id" "groupParts452.gi";
connectAttr "mgear_curveCns106GroupParts.og" "mgear_curveCns106.ip[0].ig";
connectAttr "mgear_curveCns106GroupId.id" "mgear_curveCns106.ip[0].gi";
connectAttr "thumb_R0_root.wm" "mgear_curveCns106.inputs[0]";
connectAttr "thumb_R0_0_loc.wm" "mgear_curveCns106.inputs[1]";
connectAttr "thumb_R0_1_loc.wm" "mgear_curveCns106.inputs[2]";
connectAttr "thumb_R0_2_loc.wm" "mgear_curveCns106.inputs[3]";
connectAttr "mgear_curveCns106GroupId.msg" "mgear_curveCns106Set.gn" -na;
connectAttr "thumb_R0_crvShape.iog.og[0]" "mgear_curveCns106Set.dsm" -na;
connectAttr "mgear_curveCns106.msg" "mgear_curveCns106Set.ub[0]";
connectAttr "tweak252.og[0]" "mgear_curveCns106GroupParts.ig";
connectAttr "mgear_curveCns106GroupId.id" "mgear_curveCns106GroupParts.gi";
connectAttr "groupParts504.og" "tweak252.ip[0].ig";
connectAttr "groupId8073.id" "tweak252.ip[0].gi";
connectAttr "groupId8073.msg" "tweakSet252.gn" -na;
connectAttr "thumb_R0_crvShape.iog.og[1]" "tweakSet252.dsm" -na;
connectAttr "tweak252.msg" "tweakSet252.ub[0]";
connectAttr "thumb_R0_crvShapeOrig.ws" "groupParts504.ig";
connectAttr "groupId8073.id" "groupParts504.gi";
connectAttr "mgear_curveCns107GroupParts.og" "mgear_curveCns107.ip[0].ig";
connectAttr "mgear_curveCns107GroupId.id" "mgear_curveCns107.ip[0].gi";
connectAttr "finger_R0_root.wm" "mgear_curveCns107.inputs[0]";
connectAttr "finger_R0_0_loc.wm" "mgear_curveCns107.inputs[1]";
connectAttr "finger_R0_1_loc.wm" "mgear_curveCns107.inputs[2]";
connectAttr "finger_R0_2_loc.wm" "mgear_curveCns107.inputs[3]";
connectAttr "mgear_curveCns107GroupId.msg" "mgear_curveCns107Set.gn" -na;
connectAttr "finger_R0_crvShape.iog.og[0]" "mgear_curveCns107Set.dsm" -na;
connectAttr "mgear_curveCns107.msg" "mgear_curveCns107Set.ub[0]";
connectAttr "tweak253.og[0]" "mgear_curveCns107GroupParts.ig";
connectAttr "mgear_curveCns107GroupId.id" "mgear_curveCns107GroupParts.gi";
connectAttr "groupParts506.og" "tweak253.ip[0].ig";
connectAttr "groupId8075.id" "tweak253.ip[0].gi";
connectAttr "groupId8075.msg" "tweakSet253.gn" -na;
connectAttr "finger_R0_crvShape.iog.og[1]" "tweakSet253.dsm" -na;
connectAttr "tweak253.msg" "tweakSet253.ub[0]";
connectAttr "finger_R0_crvShapeOrig.ws" "groupParts506.ig";
connectAttr "groupId8075.id" "groupParts506.gi";
connectAttr "mgear_curveCns108GroupParts.og" "mgear_curveCns108.ip[0].ig";
connectAttr "mgear_curveCns108GroupId.id" "mgear_curveCns108.ip[0].gi";
connectAttr "finger_R1_root.wm" "mgear_curveCns108.inputs[0]";
connectAttr "finger_R1_0_loc.wm" "mgear_curveCns108.inputs[1]";
connectAttr "finger_R1_1_loc.wm" "mgear_curveCns108.inputs[2]";
connectAttr "finger_R1_2_loc.wm" "mgear_curveCns108.inputs[3]";
connectAttr "mgear_curveCns108GroupId.msg" "mgear_curveCns108Set.gn" -na;
connectAttr "finger_R1_crvShape.iog.og[0]" "mgear_curveCns108Set.dsm" -na;
connectAttr "mgear_curveCns108.msg" "mgear_curveCns108Set.ub[0]";
connectAttr "tweak254.og[0]" "mgear_curveCns108GroupParts.ig";
connectAttr "mgear_curveCns108GroupId.id" "mgear_curveCns108GroupParts.gi";
connectAttr "groupParts508.og" "tweak254.ip[0].ig";
connectAttr "groupId8077.id" "tweak254.ip[0].gi";
connectAttr "groupId8077.msg" "tweakSet254.gn" -na;
connectAttr "finger_R1_crvShape.iog.og[1]" "tweakSet254.dsm" -na;
connectAttr "tweak254.msg" "tweakSet254.ub[0]";
connectAttr "finger_R1_crvShapeOrig.ws" "groupParts508.ig";
connectAttr "groupId8077.id" "groupParts508.gi";
connectAttr "mgear_curveCns109GroupParts.og" "mgear_curveCns109.ip[0].ig";
connectAttr "mgear_curveCns109GroupId.id" "mgear_curveCns109.ip[0].gi";
connectAttr "finger_R2_root.wm" "mgear_curveCns109.inputs[0]";
connectAttr "finger_R2_0_loc.wm" "mgear_curveCns109.inputs[1]";
connectAttr "finger_R2_1_loc.wm" "mgear_curveCns109.inputs[2]";
connectAttr "finger_R2_2_loc.wm" "mgear_curveCns109.inputs[3]";
connectAttr "mgear_curveCns109GroupId.msg" "mgear_curveCns109Set.gn" -na;
connectAttr "finger_R2_crvShape.iog.og[0]" "mgear_curveCns109Set.dsm" -na;
connectAttr "mgear_curveCns109.msg" "mgear_curveCns109Set.ub[0]";
connectAttr "tweak255.og[0]" "mgear_curveCns109GroupParts.ig";
connectAttr "mgear_curveCns109GroupId.id" "mgear_curveCns109GroupParts.gi";
connectAttr "groupParts510.og" "tweak255.ip[0].ig";
connectAttr "groupId8079.id" "tweak255.ip[0].gi";
connectAttr "groupId8079.msg" "tweakSet255.gn" -na;
connectAttr "finger_R2_crvShape.iog.og[1]" "tweakSet255.dsm" -na;
connectAttr "tweak255.msg" "tweakSet255.ub[0]";
connectAttr "finger_R2_crvShapeOrig.ws" "groupParts510.ig";
connectAttr "groupId8079.id" "groupParts510.gi";
connectAttr "mgear_curveCns110GroupParts.og" "mgear_curveCns110.ip[0].ig";
connectAttr "mgear_curveCns110GroupId.id" "mgear_curveCns110.ip[0].gi";
connectAttr "finger_R3_root.wm" "mgear_curveCns110.inputs[0]";
connectAttr "finger_R3_0_loc.wm" "mgear_curveCns110.inputs[1]";
connectAttr "finger_R3_1_loc.wm" "mgear_curveCns110.inputs[2]";
connectAttr "finger_R3_2_loc.wm" "mgear_curveCns110.inputs[3]";
connectAttr "mgear_curveCns110GroupId.msg" "mgear_curveCns110Set.gn" -na;
connectAttr "finger_R3_crvShape.iog.og[0]" "mgear_curveCns110Set.dsm" -na;
connectAttr "mgear_curveCns110.msg" "mgear_curveCns110Set.ub[0]";
connectAttr "tweak256.og[0]" "mgear_curveCns110GroupParts.ig";
connectAttr "mgear_curveCns110GroupId.id" "mgear_curveCns110GroupParts.gi";
connectAttr "groupParts512.og" "tweak256.ip[0].ig";
connectAttr "groupId8081.id" "tweak256.ip[0].gi";
connectAttr "groupId8081.msg" "tweakSet256.gn" -na;
connectAttr "finger_R3_crvShape.iog.og[1]" "tweakSet256.dsm" -na;
connectAttr "tweak256.msg" "tweakSet256.ub[0]";
connectAttr "finger_R3_crvShapeOrig.ws" "groupParts512.ig";
connectAttr "groupId8081.id" "groupParts512.gi";
connectAttr "mgear_curveCns105GroupParts.og" "mgear_curveCns105.ip[0].ig";
connectAttr "mgear_curveCns105GroupId.id" "mgear_curveCns105.ip[0].gi";
connectAttr "arm_R0_root.wm" "mgear_curveCns105.inputs[0]";
connectAttr "arm_R0_elbow.wm" "mgear_curveCns105.inputs[1]";
connectAttr "arm_R0_wrist.wm" "mgear_curveCns105.inputs[2]";
connectAttr "arm_R0_eff.wm" "mgear_curveCns105.inputs[3]";
connectAttr "mgear_curveCns105GroupId.msg" "mgear_curveCns105Set.gn" -na;
connectAttr "arm_R0_crvShape.iog.og[0]" "mgear_curveCns105Set.dsm" -na;
connectAttr "mgear_curveCns105.msg" "mgear_curveCns105Set.ub[0]";
connectAttr "tweak251.og[0]" "mgear_curveCns105GroupParts.ig";
connectAttr "mgear_curveCns105GroupId.id" "mgear_curveCns105GroupParts.gi";
connectAttr "groupParts502.og" "tweak251.ip[0].ig";
connectAttr "groupId8071.id" "tweak251.ip[0].gi";
connectAttr "groupId8071.msg" "tweakSet251.gn" -na;
connectAttr "arm_R0_crvShape.iog.og[1]" "tweakSet251.dsm" -na;
connectAttr "tweak251.msg" "tweakSet251.ub[0]";
connectAttr "arm_R0_crvShapeOrig.ws" "groupParts502.ig";
connectAttr "groupId8071.id" "groupParts502.gi";
connectAttr "mgear_curveCns104GroupParts.og" "mgear_curveCns104.ip[0].ig";
connectAttr "mgear_curveCns104GroupId.id" "mgear_curveCns104.ip[0].gi";
connectAttr "shoulder_R0_root.wm" "mgear_curveCns104.inputs[0]";
connectAttr "shoulder_R0_tip.wm" "mgear_curveCns104.inputs[1]";
connectAttr "mgear_curveCns104GroupId.msg" "mgear_curveCns104Set.gn" -na;
connectAttr "shoulder_R0_crvShape.iog.og[0]" "mgear_curveCns104Set.dsm" -na;
connectAttr "mgear_curveCns104.msg" "mgear_curveCns104Set.ub[0]";
connectAttr "tweak250.og[0]" "mgear_curveCns104GroupParts.ig";
connectAttr "mgear_curveCns104GroupId.id" "mgear_curveCns104GroupParts.gi";
connectAttr "groupParts500.og" "tweak250.ip[0].ig";
connectAttr "groupId8069.id" "tweak250.ip[0].gi";
connectAttr "groupId8069.msg" "tweakSet250.gn" -na;
connectAttr "shoulder_R0_crvShape.iog.og[1]" "tweakSet250.dsm" -na;
connectAttr "tweak250.msg" "tweakSet250.ub[0]";
connectAttr "shoulder_R0_crvShapeOrig.ws" "groupParts500.ig";
connectAttr "groupId8069.id" "groupParts500.gi";
connectAttr "mgear_curveCns72GroupParts.og" "mgear_curveCns72.ip[0].ig";
connectAttr "mgear_curveCns72GroupId.id" "mgear_curveCns72.ip[0].gi";
connectAttr "spine_C0_root.wm" "mgear_curveCns72.inputs[0]";
connectAttr "spine_C0_eff.wm" "mgear_curveCns72.inputs[1]";
connectAttr "mgear_curveCns72GroupId.msg" "mgear_curveCns72Set.gn" -na;
connectAttr "spine_C0_crvShape.iog.og[0]" "mgear_curveCns72Set.dsm" -na;
connectAttr "mgear_curveCns72.msg" "mgear_curveCns72Set.ub[0]";
connectAttr "tweak218.og[0]" "mgear_curveCns72GroupParts.ig";
connectAttr "mgear_curveCns72GroupId.id" "mgear_curveCns72GroupParts.gi";
connectAttr "groupParts436.og" "tweak218.ip[0].ig";
connectAttr "groupId8005.id" "tweak218.ip[0].gi";
connectAttr "groupId8005.msg" "tweakSet218.gn" -na;
connectAttr "spine_C0_crvShape.iog.og[1]" "tweakSet218.dsm" -na;
connectAttr "tweak218.msg" "tweakSet218.ub[0]";
connectAttr "spine_C0_crvShapeOrig.ws" "groupParts436.ig";
connectAttr "groupId8005.id" "groupParts436.gi";
connectAttr "mgear_curveCns97GroupParts.og" "mgear_curveCns97.ip[0].ig";
connectAttr "mgear_curveCns97GroupId.id" "mgear_curveCns97.ip[0].gi";
connectAttr "foot_L0_root.wm" "mgear_curveCns97.inputs[0]";
connectAttr "foot_L0_0_loc.wm" "mgear_curveCns97.inputs[1]";
connectAttr "foot_L0_1_loc.wm" "mgear_curveCns97.inputs[2]";
connectAttr "foot_L0_2_loc.wm" "mgear_curveCns97.inputs[3]";
connectAttr "mgear_curveCns97GroupId.msg" "mgear_curveCns97Set.gn" -na;
connectAttr "foot_L0_crvShape.iog.og[0]" "mgear_curveCns97Set.dsm" -na;
connectAttr "mgear_curveCns97.msg" "mgear_curveCns97Set.ub[0]";
connectAttr "tweak243.og[0]" "mgear_curveCns97GroupParts.ig";
connectAttr "mgear_curveCns97GroupId.id" "mgear_curveCns97GroupParts.gi";
connectAttr "groupParts486.og" "tweak243.ip[0].ig";
connectAttr "groupId8055.id" "tweak243.ip[0].gi";
connectAttr "groupId8055.msg" "tweakSet243.gn" -na;
connectAttr "foot_L0_crvShape.iog.og[1]" "tweakSet243.dsm" -na;
connectAttr "tweak243.msg" "tweakSet243.ub[0]";
connectAttr "foot_L0_crvShapeOrig.ws" "groupParts486.ig";
connectAttr "groupId8055.id" "groupParts486.gi";
connectAttr "mgear_curveCns98GroupParts.og" "mgear_curveCns98.ip[0].ig";
connectAttr "mgear_curveCns98GroupId.id" "mgear_curveCns98.ip[0].gi";
connectAttr "foot_L0_root.wm" "mgear_curveCns98.inputs[0]";
connectAttr "foot_L0_heel.wm" "mgear_curveCns98.inputs[1]";
connectAttr "foot_L0_outpivot.wm" "mgear_curveCns98.inputs[2]";
connectAttr "foot_L0_heel.wm" "mgear_curveCns98.inputs[3]";
connectAttr "foot_L0_inpivot.wm" "mgear_curveCns98.inputs[4]";
connectAttr "mgear_curveCns98GroupId.msg" "mgear_curveCns98Set.gn" -na;
connectAttr "foot_L0_Shape1.iog.og[0]" "mgear_curveCns98Set.dsm" -na;
connectAttr "mgear_curveCns98.msg" "mgear_curveCns98Set.ub[0]";
connectAttr "tweak244.og[0]" "mgear_curveCns98GroupParts.ig";
connectAttr "mgear_curveCns98GroupId.id" "mgear_curveCns98GroupParts.gi";
connectAttr "groupParts488.og" "tweak244.ip[0].ig";
connectAttr "groupId8057.id" "tweak244.ip[0].gi";
connectAttr "groupId8057.msg" "tweakSet244.gn" -na;
connectAttr "foot_L0_Shape1.iog.og[1]" "tweakSet244.dsm" -na;
connectAttr "tweak244.msg" "tweakSet244.ub[0]";
connectAttr "foot_L0_Shape1Orig1.ws" "groupParts488.ig";
connectAttr "groupId8057.id" "groupParts488.gi";
connectAttr "mgear_curveCns96GroupParts.og" "mgear_curveCns96.ip[0].ig";
connectAttr "mgear_curveCns96GroupId.id" "mgear_curveCns96.ip[0].gi";
connectAttr "leg_L0_root.wm" "mgear_curveCns96.inputs[0]";
connectAttr "leg_L0_knee.wm" "mgear_curveCns96.inputs[1]";
connectAttr "leg_L0_ankle.wm" "mgear_curveCns96.inputs[2]";
connectAttr "leg_L0_eff.wm" "mgear_curveCns96.inputs[3]";
connectAttr "mgear_curveCns96GroupId.msg" "mgear_curveCns96Set.gn" -na;
connectAttr "leg_L0_crvShape.iog.og[0]" "mgear_curveCns96Set.dsm" -na;
connectAttr "mgear_curveCns96.msg" "mgear_curveCns96Set.ub[0]";
connectAttr "tweak242.og[0]" "mgear_curveCns96GroupParts.ig";
connectAttr "mgear_curveCns96GroupId.id" "mgear_curveCns96GroupParts.gi";
connectAttr "groupParts484.og" "tweak242.ip[0].ig";
connectAttr "groupId8053.id" "tweak242.ip[0].gi";
connectAttr "groupId8053.msg" "tweakSet242.gn" -na;
connectAttr "leg_L0_crvShape.iog.og[1]" "tweakSet242.dsm" -na;
connectAttr "tweak242.msg" "tweakSet242.ub[0]";
connectAttr "leg_L0_crvShapeOrig.ws" "groupParts484.ig";
connectAttr "groupId8053.id" "groupParts484.gi";
connectAttr "mgear_curveCns112GroupParts.og" "mgear_curveCns112.ip[0].ig";
connectAttr "mgear_curveCns112GroupId.id" "mgear_curveCns112.ip[0].gi";
connectAttr "foot_R0_root.wm" "mgear_curveCns112.inputs[0]";
connectAttr "foot_R0_0_loc.wm" "mgear_curveCns112.inputs[1]";
connectAttr "foot_R0_1_loc.wm" "mgear_curveCns112.inputs[2]";
connectAttr "foot_R0_2_loc.wm" "mgear_curveCns112.inputs[3]";
connectAttr "mgear_curveCns112GroupId.msg" "mgear_curveCns112Set.gn" -na;
connectAttr "foot_R0_crvShape.iog.og[0]" "mgear_curveCns112Set.dsm" -na;
connectAttr "mgear_curveCns112.msg" "mgear_curveCns112Set.ub[0]";
connectAttr "tweak258.og[0]" "mgear_curveCns112GroupParts.ig";
connectAttr "mgear_curveCns112GroupId.id" "mgear_curveCns112GroupParts.gi";
connectAttr "groupParts516.og" "tweak258.ip[0].ig";
connectAttr "groupId8085.id" "tweak258.ip[0].gi";
connectAttr "groupId8085.msg" "tweakSet258.gn" -na;
connectAttr "foot_R0_crvShape.iog.og[1]" "tweakSet258.dsm" -na;
connectAttr "tweak258.msg" "tweakSet258.ub[0]";
connectAttr "foot_R0_crvShapeOrig.ws" "groupParts516.ig";
connectAttr "groupId8085.id" "groupParts516.gi";
connectAttr "mgear_curveCns113GroupParts.og" "mgear_curveCns113.ip[0].ig";
connectAttr "mgear_curveCns113GroupId.id" "mgear_curveCns113.ip[0].gi";
connectAttr "foot_R0_root.wm" "mgear_curveCns113.inputs[0]";
connectAttr "foot_R0_heel.wm" "mgear_curveCns113.inputs[1]";
connectAttr "foot_R0_outpivot.wm" "mgear_curveCns113.inputs[2]";
connectAttr "foot_R0_heel.wm" "mgear_curveCns113.inputs[3]";
connectAttr "foot_R0_inpivot.wm" "mgear_curveCns113.inputs[4]";
connectAttr "mgear_curveCns113GroupId.msg" "mgear_curveCns113Set.gn" -na;
connectAttr "foot_R0_Shape1.iog.og[0]" "mgear_curveCns113Set.dsm" -na;
connectAttr "mgear_curveCns113.msg" "mgear_curveCns113Set.ub[0]";
connectAttr "tweak259.og[0]" "mgear_curveCns113GroupParts.ig";
connectAttr "mgear_curveCns113GroupId.id" "mgear_curveCns113GroupParts.gi";
connectAttr "groupParts518.og" "tweak259.ip[0].ig";
connectAttr "groupId8087.id" "tweak259.ip[0].gi";
connectAttr "groupId8087.msg" "tweakSet259.gn" -na;
connectAttr "foot_R0_Shape1.iog.og[1]" "tweakSet259.dsm" -na;
connectAttr "tweak259.msg" "tweakSet259.ub[0]";
connectAttr "foot_R0_Shape1Orig.ws" "groupParts518.ig";
connectAttr "groupId8087.id" "groupParts518.gi";
connectAttr "mgear_curveCns111GroupParts.og" "mgear_curveCns111.ip[0].ig";
connectAttr "mgear_curveCns111GroupId.id" "mgear_curveCns111.ip[0].gi";
connectAttr "leg_R0_root.wm" "mgear_curveCns111.inputs[0]";
connectAttr "leg_R0_knee.wm" "mgear_curveCns111.inputs[1]";
connectAttr "leg_R0_ankle.wm" "mgear_curveCns111.inputs[2]";
connectAttr "leg_R0_eff.wm" "mgear_curveCns111.inputs[3]";
connectAttr "mgear_curveCns111GroupId.msg" "mgear_curveCns111Set.gn" -na;
connectAttr "leg_R0_crvShape.iog.og[0]" "mgear_curveCns111Set.dsm" -na;
connectAttr "mgear_curveCns111.msg" "mgear_curveCns111Set.ub[0]";
connectAttr "tweak257.og[0]" "mgear_curveCns111GroupParts.ig";
connectAttr "mgear_curveCns111GroupId.id" "mgear_curveCns111GroupParts.gi";
connectAttr "groupParts514.og" "tweak257.ip[0].ig";
connectAttr "groupId8083.id" "tweak257.ip[0].gi";
connectAttr "groupId8083.msg" "tweakSet257.gn" -na;
connectAttr "leg_R0_crvShape.iog.og[1]" "tweakSet257.dsm" -na;
connectAttr "tweak257.msg" "tweakSet257.ub[0]";
connectAttr "leg_R0_crvShapeOrig.ws" "groupParts514.ig";
connectAttr "groupId8083.id" "groupParts514.gi";
// End of biped_guide.ma
