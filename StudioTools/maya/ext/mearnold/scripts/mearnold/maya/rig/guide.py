'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.rig.guide
# @author Jeremie Passerin, Miquel Campos
#

##########################################################
# GLOBAL
##########################################################
# Built-in
import os

# from functools import partial
import datetime

# maya
# import maya.cmds as cmds

# pymel
import pymel.core as pm
import pymel.core.datatypes as dt

# mgear
import mgear
import mgear.maya.attribute as att
import mgear.maya.dag as dag
import mgear.maya.vector as vec
import mgear.maya.utils as uti




#
GUIDE_UI_WINDOW_NAME = "guide_UI_window"
GUIDE_DOCK_NAME = "Guide_Components"

COMPONENT_PATH = os.path.join(os.path.dirname(__file__), "component")
TEMPLATE_PATH = os.path.join(COMPONENT_PATH, "templates")
VERSION = 1.0

##########################################################
# GUIDE
##########################################################
## The main guide class.\n
# Provide the methods to add parameters, set parameter values, create property...
class MainGuide(object):

    def __init__(self):

        # Parameters names, definition and values.
        self.paramNames = [] ## List of parameter name cause it's actually important to keep them sorted.
        self.paramDefs = {} ## Dictionary of parameter definition.
        self.values = {} ## Dictionary of options values.

        # We will check a few things and make sure the guide we are loading is up to date.
        # If parameters or object are missing a warning message will be display and the guide should be updated.
        self.valid = True


    # ====================================================
    # PROPERTY AND PARAMETERS

    ## Add a property (sn_PSet) with all the parameters from the parameter definition list.
    # @param self
    # @param parent X3DObject - The parent of the property.


    def addPropertyParamenters(self, parent):
        for scriptName in self.paramNames:
            paramDef = self.paramDefs[scriptName]
            paramDef.create(parent)

        return parent

    # ----------------------------------------------------
    # Set Parameters values

    ## Set the value of parameter with matching scriptname.
    # @param self
    # @param scriptName String - Scriptname of the parameter to edit.
    # @param value Variant - New value.
    # @return Boolean - False if the parameter wasn't found.
    def setParamDefValue(self, scriptName, value):

        if not scriptName in self.paramDefs.keys():
            mgear.log("Can't find parameter definition for : " + scriptName, mgear.sev_warning)
            return False

        self.paramDefs[scriptName].value = value
        self.values[scriptName] = value

        return True


    def setParamDefValuesFromProperty(self, node):

        for scriptName, paramDef in self.paramDefs.items():
            if not pm.attributeQuery(scriptName, node=node, exists=True):
                mgear.log("Can't find parameter '%s' in %s"%(scriptName, node), mgear.sev_warning)
                self.valid = False
            else:
                cnx = pm.listConnections(node+"."+scriptName, destination=False, source=True)
                if cnx:
                    paramDef.value = None
                    self.values[scriptName] = cnx[0]
                else:
                    paramDef.value = pm.getAttr(node+"."+scriptName)
                    self.values[scriptName] = pm.getAttr(node+"."+scriptName)

    def addColorParam(self, scriptName, defaultValue=False):

            paramDef = att.colorParamDef(scriptName, defaultValue)
            self.paramDefs[scriptName] = paramDef
            self.paramNames.append(scriptName)

            return paramDef
    def addParam(self, scriptName,  valueType, value, minimum=None, maximum=None, keyable=False, readable=True, storable=True, writable=True, niceName=None, shortName=None):

            paramDef = att.ParamDef2(scriptName, valueType, value, niceName, shortName, minimum, maximum, keyable, readable, storable, writable)
            self.paramDefs[scriptName] = paramDef
            self.values[scriptName] = value
            self.paramNames.append(scriptName)

            return paramDef

    ## Add a paramDef to the list.\n
    # Note that animatable and keyable are false per default.
    # @param self
    # @param scriptName String - Parameter scriptname.
    # @return FCurveParamDef - The newly created parameter definition.
    def addFCurveParam(self, scriptName, keys, interpolation=0):

        paramDef = att.FCurveParamDef(scriptName, keys, interpolation)
        self.paramDefs[scriptName] = paramDef
        self.values[scriptName] = None
        self.paramNames.append(scriptName)

        return paramDef


    def addEnumParam(self, scriptName, enum, defaultValue=0):

        paramDef = att.enumParamDef(scriptName, enum, defaultValue)
        self.paramDefs[scriptName] = paramDef
        self.paramNames.append(scriptName)

        return paramDef

##########################################################
# RIG GUIDE
##########################################################
## Rig guide class.\n
# This is the class for complete rig guide definition.\n
# It contains the component guide in correct hierarchy order and the options to generate the rig.\n
# Provide the methods to add more component, import/export guide.
class RigGuide(MainGuide):

    def __init__(self):

        # Parameters names, definition and values.
        self.paramNames = [] ## List of parameter name cause it's actually important to keep them sorted.
        self.paramDefs = {} ## Dictionary of parameter definition.
        self.values = {} ## Dictionary of options values.


        # We will check a few things and make sure the guide we are loading is up to date.
        # If parameters or object are missing a warning message will be display and the guide should be updated.
        self.valid = True

        self.controllers = {} ## Dictionary of controllers
        # Keys are the component fullname (ie. 'arm_L0')
        self.components = {} ## Dictionary of component
        self.componentsIndex = [] ## List of component name sorted by order creation (hierarchy order)
        self.parents = [] ## List of the parent of each component, in same order as self.components

        self.addParameters()

    # =====================================================
    # PARAMETERS FOR RIG OPTIONS

    ## Add more parameter to the parameter definition list.
    # @param self
    def addParameters(self):

        # --------------------------------------------------
        # Main Tab
        self.pRigName = self.addParam("rig_name", "string", "rig")
        self.pMode = self.addEnumParam("mode", ["Final", "WIP"], 0)
        self.pStep = self.addEnumParam("step", ["All Steps", "Objects", "Properties", "Operators", "Connect", "Finalize"], 5)
        self.pShadowRig = self.addParam("ismodel", "bool", True)


        # --------------------------------------------------
        # Colors


        #Index color
        self.pLColorIndexfk =  self.addParam("L_color_fk", "long", 6, 0, 31)
        self.pLColorIndexik =  self.addParam("L_color_ik", "long", 5, 0, 31)
        self.pRColorIndexfk =  self.addParam("R_color_fk", "long", 30, 0, 31)
        self.pRColorIndexik =  self.addParam("R_color_ik", "long", 9, 0, 31)
        self.pCColorIndexfk =  self.addParam("C_color_fk", "long", 13, 0, 31)
        self.pCColorIndexik =  self.addParam("C_color_ik", "long", 4, 0, 31)

        #RGB colors for Maya 2015 and up
        # self.pLColorfk = self.addColorParam("L_RGB_fk", [0, 1, 0])
        # self.pLColorik = self.addColorParam("L_RGB_ik", [0, .5, 0])
        # self.pRColorfk = self.addColorParam("R_RGB_fk", [0, 0, 1])
        # self.pRColorik = self.addColorParam("R_RGB_ik", [0, 0, .6])
        # self.pCColorfk = self.addColorParam("C_RGB_fk", [1, 0, 0])
        # self.pCColorik = self.addColorParam("C_RGB_ik", [.6, 0, 0])

        '''
        setAttr "biped_guide.R_color_fk" 1;
        setAttr "biped_guide.R_color_ik" 2;
        setAttr "biped_guide.C_color_fk" 1;
        setAttr "biped_guide.C_color_ik" 2;
        setAttr "biped_guide.L_color_fk" 1;
        setAttr "biped_guide.L_color_ik" 1;
        '''

        # --------------------------------------------------
        # Settings
        self.pShadowRig = self.addParam("shadow_rig", "bool", True)

        self.pSynoptic = self.addParam("synoptic", "string", "")

        # --------------------------------------------------
        # Comments
        self.pComments = self.addParam("comments", "string", "")

    # =====================================================
    # SET
    ## set the guide hierarchy from selection.
    # @param self
    def setFromSelection(self):

        selection = pm.ls(selection=True)
        if not selection:
            mgear.log("Select one or more guide root or a guide model", mgear.sev_error)
            self.valid = False
            return False

        for node in selection:
            self.setFromHierarchy(node, node.hasAttr("ismodel"))

        return True

    ## set the guide from given hierarchy.
    # @param self
    # @param root X3DObject - The root of the hierarchy to parse.
    # @param branch Boolean - True to parse children components
    def setFromHierarchy(self, root, branch=True):

        startTime = datetime.datetime.now()
        # Start
        mgear.log("Checking guide")

        # Get the model and the root
        self.model = root.getParent(generations=-1)
        while True:
            if root.hasAttr("comp_type") or self.model == root:
                break
            root = root.getParent()
            mgear.log( root)

        # ---------------------------------------------------
        # First check and set the options
        mgear.log("Get options")
        # if not root.hasAttr("rig_name"):
        #     mgear.log("%s is not a proper rig guideeee."%self.model, mgear.sev_error)
        #     self.valid = False
        #     return

        self.setParamDefValuesFromProperty(self.model)

        # ---------------------------------------------------
        # Get the controllers
        mgear.log("Get controllers")
        self.controllers_org = dag.findChild(self.model, "controllers_org")
        if self.controllers_org:
            for child in self.controllers_org.getChildren():
                self.controllers[child.name().split("|")[-1]] = child

        # ---------------------------------------------------
        # Components
        mgear.log("Get components")
        self.findComponentRecursive(root, branch)
        # Parenting
        if self.valid:
            # for name  in self.componentsIndex:
            #     mgear.log("Get parenting for: " + name)
            #     compChild = self.components[name]
            #     compChild_parent = compChild.root.getParent()
            #     for name in self.componentsIndex:
            #         compParent = self.components[name]
            #         for localName, element in compParent.getObjects(self.model, False).items():
            #             if element is not None and element == compChild_parent:
            #                 compChild.parentComponent = compParent
            #                 compChild.parentLocalName = localName
            #                 break

            for name in self.componentsIndex:
                mgear.log("Get parenting for: " + name)
                compParent = self.components[name]
                for localName, element in compParent.getObjects(self.model, False).items():
                    for name  in self.componentsIndex:
                        compChild = self.components[name]
                        compChild_parent = compChild.root.getParent()
                        if element is not None and element == compChild_parent:
                            compChild.parentComponent = compParent
                            compChild.parentLocalName = localName
                            # break


            # More option values
            self.addOptionsValues()

        # End
        if not self.valid:
            mgear.log("The guide doesn't seem to be up to date. Check logged messages and update the guide.", mgear.sev_warning)

        endTime = datetime.datetime.now()
        finalTime = endTime - startTime
        mgear.log("Guide loaded from hierarchy in  [ " + str(finalTime) + " ]" )

    ## Gather or change some options values according to some others.
    # @param self
    def addOptionsValues(self):

        # Get rig size to adapt size of object to the scale of the character
        maximum = 1
        v = dt.Vector()
        for comp in self.components.values():
            for pos in comp.apos:
                d = vec.getDistance(v, pos)
                maximum = max(d, maximum)

        self.values["size"] = max(maximum * .05, .1)

    def findComponentRecursive(self, node, branch=True):

        if node.hasAttr("comp_type"):
            comp_type = node.getAttr("comp_type")
            comp_guide = self.getComponentGuide(comp_type)

            if comp_guide:
                comp_guide.setFromHierarchy(node)
                mgear.log(comp_guide.fullName+" ("+comp_type+")")
                if not comp_guide.valid:
                    self.valid = False

                self.componentsIndex.append(comp_guide.fullName)
                self.components[comp_guide.fullName] = comp_guide

        if branch:
            for child in node.getChildren():
                self.findComponentRecursive(child)

    def getComponentGuide(self, comp_type):

        # Check component type
        path = os.path.join(COMPONENT_PATH, comp_type, "guide.py")
        if not os.path.exists(path):
            mgear.log("Can't find guide definition for : " + comp_type + ".\n"+ path, mgear.sev_error)
            return False

        # Import module and get class
        module_name = "mgear.maya.rig.component."+comp_type+".guide"
        module = __import__(module_name, globals(), locals(), ["*"], -1)
        ComponentGuide = getattr(module , "Guide")

        return ComponentGuide()

    # =====================================================
    # DRAW

    ## Create the initial rig guide hierarchy (model, options...)
    # @param self
    def initialHierarchy(self):

        self.model = pm.group(n="guide", em=True, w=True)

        # Options
        self.options = self.addPropertyParamenters(self.model)

        #the basic org nulls (Maya groups)
        self.controllers_org = pm.group(n="controllers_org", em=True, p=self.model)
        self.controllers_org.attr('visibility').set(0)

    ## Add a new component to the guide.
    # @param self
    # @param parent X3DObject - Parent of this new component guide.
    # @param compType String - Type of component to add.
    def drawNewComponent(self, parent, comp_type):

        comp_guide = self.getComponentGuide(comp_type)

        if not comp_guide:
            mgear.log("Not component guide of type: " + comp_type + " have been found.", mgear.sev_error)
            return
        if parent is None:
            self.initialHierarchy()
            parent = self.model
        else:
            parent = parent

        comp_guide.drawFromUI(parent)


    ## Duplicate the guide hierarchy.
    # @param self
    # @param root X3DObject
    # @param symmetrize Boolean
    # @return X3DObject - The root of the newly created hierarchy.
    def duplicate(self, root, symmetrize=False):

        if not pm.attributeQuery("comp_type", node=root, ex=True):
            mgear.log("Select a root to duplicate", mgear.sev_error)
            return

        self.setFromHierarchy(root)
        for name in self.componentsIndex:
            comp_guide = self.components[name]
            if symmetrize:
                if not comp_guide.symmetrize():
                    return

        # Draw
        if pm.attributeQuery("ismodel", node=root, ex=True):
            self.draw()

        else:

            for name in self.componentsIndex:
                comp_guide = self.components[name]

                if comp_guide.parentComponent is None:
                    parent = comp_guide.root.getParent()
                    if symmetrize:
                        parent = dag.findChild(self.model, uti.convertRLName(comp_guide.root.name()))
                        if not parent:
                            parent = comp_guide.root.getParent()

                    else:
                        parent = comp_guide.root.getParent()

                else:
                    parent = dag.findChild(self.model, comp_guide.parentComponent.getName(comp_guide.parentLocalName))
                    if not parent:
                        mgear.log("Unable to find parent (%s.%s) for guide %s"%(comp_guide.parentComponent.getFullName, comp_guide.parentLocalName, comp_guide.getFullName ))
                        parent = self.model

                comp_guide.root = None # Reset the root so we force the draw to duplicate
                comp_guide.setIndex(self.model)


                comp_guide.draw(parent)

        pm.select(self.components[self.componentsIndex[0]].root)


    def updateProperties(self, root, newName, newSide, newIndex):

        if not pm.attributeQuery("comp_type", node=root, ex=True):
            mgear.log("Select a root to edit properties", mgear.sev_error)
            return
        self.setFromHierarchy(root, False)
        name = "_".join(root.name().split("|")[-1].split("_")[0:2])
        comp_guide = self.components[name]
        comp_guide.rename(root, newName, newSide, newIndex)
