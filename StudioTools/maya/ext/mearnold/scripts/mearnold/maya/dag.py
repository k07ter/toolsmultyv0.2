'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.dag
# @author Jeremie Passerin, Miquel Campos
#
#############################################
# GLOBAL
#############################################


#############################################
# DAG
#############################################
# ===========================================
# Returns the first parent of the hierarchy. (usually the 'Model' in Softimage)
def getTopParent(node):
    return node.getParent(generations=-1)

# ===========================================
def getShapes(node):
    return node.listRelatives(shapes=True)

# ===========================================
def findChild(node, name):
    return __findChildren(node, name, True)

def findChildren(node, name):
    return __findChildren(node, name, False)

def findChildrenPartial(node, name):
    return __findChildren(node, name, False, True)

def __findChildren(node, name, firstOnly=False, partialName=False):

    if partialName:
        children = [item for item in node.listRelatives(ad=True, type="transform") if item.name().split("|")[-1].split("_")[-1] == name]
    else:
        children = [item for item in node.listRelatives(ad=True, type="transform") if item.name().split("|")[-1] == name]
    if not children:
        return False
    if firstOnly:
        return children[0]

    return children

def findComponentChildren(node, name, sideIndex):

    children = []
    for item in node.listRelatives(ad=True, type="transform"):
        checkName = item.name().split("|")[-1].split("_")
        if checkName[0] == name and checkName[1] == sideIndex:
            children.append(item)

    return children