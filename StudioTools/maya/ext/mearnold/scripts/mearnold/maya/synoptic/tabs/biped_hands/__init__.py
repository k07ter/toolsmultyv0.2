'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.synoptic.tabs.biped_hands
# @author Jeremie Passerin, Miquel Campos
#
##################################################
# GLOBAL
##################################################
import os

import PySide.QtGui as QtGui
from PySide import QtCore
import pysideuic
import xml.etree.ElementTree as xml
from cStringIO import StringIO

import mgear.maya.synoptic.utils as syn_uti
import mgear.maya.synoptic.widgets as mwi




def loadUiType(uiFile):
    """
    Pyside lacks the "loadUiType" command, so we have to convert the ui file to py code in-memory first
    and then execute it in a special frame to retrieve the form_class.
    """
    parsed = xml.parse(uiFile)
    widget_class = parsed.find('widget').get('class')
    form_class = parsed.find('class').text

    with open(uiFile, 'r') as f:
        o = StringIO()
        frame = {}

        pysideuic.compileUi(f, o, indent=0)
        pyc = compile(o.getvalue(), '<string>', 'exec')
        exec pyc in frame

        #Fetch the base_class and form class based on their type in the xml from designer
        form_class = frame['Ui_%s'%form_class]
        base_class = eval('QtGui.%s'%widget_class)
    return form_class, base_class

UI_PATH = os.path.join(os.path.dirname(__file__), "widget.ui")
BG_PATH = os.path.join(os.path.dirname(__file__), "background.bmp")

##################################################
# SYNOPTIC TAB WIDGET
##################################################
#where UI_PATH is the path to our designer .ui file
form_class, base_class = loadUiType(UI_PATH)

class SynopticTab(base_class, form_class):

    # ============================================
    # INIT
    def __init__(self, parent=None):
        super(SynopticTab, self).__init__(parent)
        self.setupUi(self)

        # Retarget background Image to absolute path
        self.img_background.setPixmap(QtGui.QPixmap(BG_PATH))

        # Connect signal
        self.b_selRight.clicked.connect(self.selRight_clicked)
        self.b_selLeft.clicked.connect(self.selLeft_clicked)
        self.b_keyRight.clicked.connect(self.keyRight_clicked)
        self.b_keyLeft.clicked.connect(self.keyLeft_clicked)
        self.b_keySel.clicked.connect(self.keySel_clicked)

        self.rubberband = QtGui.QRubberBand(QtGui.QRubberBand.Rectangle, self)

    def mousePressEvent(self, event):
        self.origin = event.pos()
        self.rubberband.setGeometry(QtCore.QRect(self.origin, QtCore.QSize()))
        self.rubberband.show()
        QtGui.QWidget.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if self.rubberband.isVisible():
            self.rubberband.setGeometry(QtCore.QRect(self.origin, event.pos()).normalized())
        QtGui.QWidget.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        if self.rubberband.isVisible():
            self.rubberband.hide()
            selected = []
            rect = self.rubberband.geometry()
            for child in self.findChildren(mwi.SelectButton):
                if rect.intersects(child.geometry()):
                    selected.append(child)
            if selected:
                firstLoop = True
                for wi in selected:
                    wi.rectangleSelection(event,firstLoop)
                    firstLoop = False
            else:
                print ' Nothing Selected\n'
        QtGui.QWidget.mouseReleaseEvent(self, event)

    # ============================================
    # BUTTONS
    def selRight_clicked(self):
        model = syn_uti.getModel(self)
        # i : num of fingers, j : finger length
        object_names = ["finger_R%s_fk%s_ctl"%(i,j) for i in range(4) for j in range(3)]
        thumb_names = ["thumb_R0_fk%s_ctl"%j for j in range(3)]
        object_names.extend(thumb_names)
        syn_uti.selectObj(model, object_names, None, None)

    def selLeft_clicked(self):
        model = syn_uti.getModel(self)
        # i : num of fingers, j : finger length
        object_names = ["finger_L%s_fk%s_ctl"%(i,j) for i in range(4) for j in range(3)]
        thumb_names = ["thumb_L0_fk%s_ctl"%j for j in range(3)]
        object_names.extend(thumb_names)
        syn_uti.selectObj(model, object_names, None, None)

    def keyRight_clicked(self):
        model = syn_uti.getModel(self)
        # i : num of fingers, j : finger length
        object_names = ["finger_R%s_fk%s_ctl"%(i,j) for i in range(4) for j in range(3)]
        thumb_names = ["thumb_R0_fk%s_ctl"%j for j in range(3)]
        object_names.extend(thumb_names)
        syn_uti.keyObj(model, object_names)

    def keyLeft_clicked(self):
        model = syn_uti.getModel(self)
        # i : num of fingers, j : finger length
        object_names = ["finger_L%s_fk%s_ctl"%(i,j) for i in range(4) for j in range(3)]
        thumb_names = ["thumb_L0_fk%s_ctl"%j for j in range(3)]
        object_names.extend(thumb_names)
        syn_uti.keyObj(model, object_names)

    def keySel_clicked(self):
        syn_uti.keySel()
