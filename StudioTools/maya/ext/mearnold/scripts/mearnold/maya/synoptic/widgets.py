'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.synoptic.widgets
# @author Jeremie Passerin, Miquel Campos
#
##################################################
# GLOBAL
##################################################
import PySide.QtGui as QtGui
import PySide.QtCore as QtCore

import mgear.maya.synoptic.utils as syn_uti


##################################################
# PROMOTED WIDGETS
##################################################
# They must be declared first because they are used in the widget.ui
class resetTransform(QtGui.QPushButton):

    def mousePressEvent(self, event):


        mouse_button = event.button()

        syn_uti.resetSelTrans()


class resetBindPose(QtGui.QPushButton):

    def mousePressEvent(self, event):

        model = syn_uti.getModel(self)
        mouse_button = event.button()

        syn_uti.bindPose(model)

class MirrorPoseButton(QtGui.QPushButton):

    def mousePressEvent(self, event):

        mouse_button = event.button()
        syn_uti.mirrorPose()

class FlipPoseButton(QtGui.QPushButton):

    def mousePressEvent(self, event):

        mouse_button = event.button()
        syn_uti.mirrorPose(True)

class QuickSelButton(QtGui.QPushButton):

    def mousePressEvent(self, event):

        model = syn_uti.getModel(self)
        channel = str(self.property("channel"))
        mouse_button = event.button()

        syn_uti.quickSel(model, channel, mouse_button)

class SelectButton(QtGui.QWidget):
    over = False
    color_over = QtGui.QColor(255, 255, 255, 255)

    def enterEvent(self, event):
        self.over = True
        self.repaint()
        self.update()

    def leaveEvent(self, event):
        self.over = False
        self.repaint()
        self.update()

    def rectangleSelection(self,event, firstLoop):
        if firstLoop:
            key_modifier = event.modifiers()
        else:
            key_modifier = QtCore.Qt.ShiftModifier
        model = syn_uti.getModel(self)
        object = str(self.property("object")).split(",") #mq: .toString() removed

        mouse_button = event.button()
        syn_uti.selectObj(model, object, mouse_button, key_modifier)

    def mousePressEvent(self, event):

        model = syn_uti.getModel(self)
        object = str(self.property("object")).split(",") #mq: .toString() removed
        mouse_button = event.button()
        key_modifier = event.modifiers()

        syn_uti.selectObj(model, object, mouse_button, key_modifier)

    def paintEvent(self, event):
        painter = QtGui.QPainter()
        painter.begin(self)
        if self.over:
            painter.setBrush(self.color_over)
        else:
            painter.setBrush(self.color)
        self.drawShape(painter)
        painter.end()

class SelectBtn_RFk(SelectButton):
    color = QtGui.QColor(0, 0, 192, 255)

class SelectBtn_RIk(SelectButton):
    color = QtGui.QColor(0, 128, 192, 255)

class SelectBtn_CFk(SelectButton):
    color = QtGui.QColor(128, 0, 128, 255)

class SelectBtn_CIk(SelectButton):
    color = QtGui.QColor(192, 64, 192, 255)

class SelectBtn_LFk(SelectButton):
    color = QtGui.QColor(192, 0, 0, 255)

class SelectBtn_LIk(SelectButton):
    color = QtGui.QColor(192, 128, 0, 255)

class SelectBtn_yellow(SelectButton):
    color = QtGui.QColor(255, 192, 0, 255)

class SelectBtn_green(SelectButton):
    color = QtGui.QColor(0, 192, 0, 255)

class SelectBtn_darkGreen(SelectButton):
    color = QtGui.QColor(0, 100, 0, 255)

class SelectBtn_Box(SelectButton):
    def drawShape(self, painter):
        painter.drawRect(0, 0, self.width()-1, self.height()-1)

class SelectBtn_Circle(SelectButton):
    def drawShape(self, painter):
        painter.drawEllipse(0, 0, self.width()-1, self.height()-1)

# ------------------------------------------
class SelectBtn_RFkBox(SelectBtn_RFk, SelectBtn_Box):
    pass
class SelectBtn_RIkBox(SelectBtn_RIk, SelectBtn_Box):
    pass
class SelectBtn_CFkBox(SelectBtn_CFk, SelectBtn_Box):
    pass
class SelectBtn_CIkBox(SelectBtn_CIk, SelectBtn_Box):
    pass
class SelectBtn_LFkBox(SelectBtn_LFk, SelectBtn_Box):
    pass
class SelectBtn_LIkBox(SelectBtn_LIk, SelectBtn_Box):
    pass
class SelectBtn_yellowBox(SelectBtn_yellow, SelectBtn_Box):
    pass
class SelectBtn_greenBox(SelectBtn_green, SelectBtn_Box):
    pass
class SelectBtn_darkGreenBox(SelectBtn_darkGreen, SelectBtn_Box):
    pass

class SelectBtn_RFkCircle(SelectBtn_RFk, SelectBtn_Circle):
    pass
class SelectBtn_RIkCircle(SelectBtn_RIk, SelectBtn_Circle):
    pass
class SelectBtn_CFkCircle(SelectBtn_CFk, SelectBtn_Circle):
    pass
class SelectBtn_CIkCircle(SelectBtn_CIk, SelectBtn_Circle):
    pass
class SelectBtn_LFkCircle(SelectBtn_LFk, SelectBtn_Circle):
    pass
class SelectBtn_LIkCircle(SelectBtn_LIk, SelectBtn_Circle):
    pass
class SelectBtn_greenCircle(SelectBtn_green, SelectBtn_Circle):
    pass
