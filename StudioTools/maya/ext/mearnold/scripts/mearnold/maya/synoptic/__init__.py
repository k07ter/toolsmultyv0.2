'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

## @package mgear.maya.synoptic
# @author Jeremie Passerin, Miquel Campos
#
##################################################
# GLOBAL
##################################################
import os
import shiboken

import maya.cmds as cmds
import maya.OpenMayaUI as mui
import pymel.core as pm
import PySide.QtGui as QtGui
import PySide.QtCore as QtCore

from shiboken import wrapInstance


# PySide Wrapinstance

def wrapinstance2(ptr, base=None):
    """
    Utility to convert a pointer to a Qt class instance (PySide/PyQt compatible)

    :param ptr: Pointer to QObject in memory
    :type ptr: long or Swig instance
    :param base: (Optional) Base class to wrap with (Defaults to QObject, which should handle anything)
    :type base: QtGui.QWidget
    :return: QWidget or subclass instance
    :rtype: QtGui.QWidget
    """
    if ptr is None:
        return None
    ptr = long(ptr) #Ensure type
    if globals().has_key('shiboken'):
        if base is None:
            qObj = shiboken.wrapInstance(long(ptr), QtCore.QObject)
            metaObj = qObj.metaObject()
            cls = metaObj.className()
            superCls = metaObj.superClass().className()
            if hasattr(QtGui, cls):
                base = getattr(QtGui, cls)
            elif hasattr(QtGui, superCls):
                base = getattr(QtGui, superCls)
            else:
                base = QtGui.QWidget
        print base
        return shiboken.wrapInstance(long(ptr), base)
    elif globals().has_key('sip'):
        base = QtCore.QObject
        return sip.wrapinstance(long(ptr), base)
    else:
        return None

def getMayaWindow():
    'Get the maya main window as a QMainWindow instance'
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr),QtGui.QWidget)


TAB_PATH = os.path.join(os.path.dirname(__file__), "tabs")
SYNOPTIC_WIDGET_NAME = "synoptic_view"



##################################################
# OPEN
##################################################
def open(*args):

    global synoptic_window
    synoptic_window = Synoptic(getMayaWindow())
    synoptic_window.show()
    if (cmds.dockControl('synoptic_dock', q=1, ex=1)):
        cmds.deleteUI('synoptic_dock')
    allowedAreas = ['right', 'left']
    synoptic_dock = cmds.dockControl('synoptic_dock',aa=allowedAreas, a='left', content=SYNOPTIC_WIDGET_NAME, label='Synoptic View', w=350)

##################################################
# SYNOPTIC
##################################################
class Synoptic(QtGui.QDialog):

    def __init__(self, parent=None):
        super(Synoptic, self).__init__(parent)
        self.create_widgets()

    def create_widgets(self):

        # Widgets
        self.model_list = QtGui.QComboBox()
        self.model_list.setObjectName("model_list")
        self.refresh_button = QtGui.QPushButton("Refresh")
        self.refresh_button.setObjectName("refresh_button")
        self.tabs = QtGui.QTabWidget()
        self.tabs.setObjectName("synoptic_tab")

        # Layout
        self.setObjectName(SYNOPTIC_WIDGET_NAME)

        self.vbox = QtGui.QVBoxLayout(self)
        self.hbox = QtGui.QHBoxLayout(self)

        self.vbox.addLayout(self.hbox)
        self.hbox.addWidget(self.model_list)
        self.hbox.addWidget(self.refresh_button)
        self.vbox.addWidget(self.tabs)

        # Connect Signal
        self.refresh_button.clicked.connect(self.updateModelList)
        self.model_list.currentIndexChanged.connect(self.updateTabs)

        # Initialise
        self.updateModelList()
        self.updateTabs()

    # Singal Methods =============================
    def updateModelList(self):
        rig_models = [item for item in pm.ls(transforms=True) if item.getParent() is None and item.hasAttr("is_rig")]
        self.model_list.clear()
        for item in rig_models:
            self.model_list.addItem(item.name(), item.name()    )

    def updateTabs(self):
        self.tabs.clear()
        tab_names = pm.ls(self.model_list.currentText())[0].getAttr("synoptic").split(",")

        for i, tab_name in enumerate(tab_names):
            module_name = "mgear.maya.synoptic.tabs."+tab_name
            module = __import__(module_name, globals(), locals(), ["*"], -1)
            SynopticTab = getattr(module , "SynopticTab")

            tab = SynopticTab()
            self.tabs.insertTab(i, tab, tab_name)
