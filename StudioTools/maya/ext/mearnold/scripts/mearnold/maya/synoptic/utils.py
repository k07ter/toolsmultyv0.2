'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''
## @package mgear.maya.synoptic.utils
# @author Jeremie Passerin, Miquel Campos
#

#TODO:   add  match IK/FK


##################################################
# GLOBAL
##################################################
import PySide.QtCore as QtCore

import pymel.core as pm

import mgear
import mgear.maya.dag as dag
import mgear.maya.transform as tra

SYNOPTIC_WIDGET_NAME = "synoptic_view"
CTRL_GRP_SUFFIX = "_controllers_grp"

##################################################
#
##################################################
def getSynopticWidget(widget, max_iter=20):

    parent = widget.parentWidget()
    for i in range(max_iter):
        if parent.objectName() == SYNOPTIC_WIDGET_NAME:
            return parent
        parent = parent.parentWidget()

    return False

def getModel(widget):

    syn_widget = getSynopticWidget(widget, max_iter=20)
    model_name = syn_widget.model_list.currentText()
    model = pm.PyNode(model_name)

    return model

def getControlers(model):

    ctl_set = pm.PyNode(model.name()+CTRL_GRP_SUFFIX)
    members = ctl_set.members()

    return members

##################################################
# SELECT
##################################################
# ================================================
def selectObj(model, object_names, mouse_button, key_modifier):



    nodes = []
    for name in object_names:
        if  len(model.split(":")) == 2:
            node = dag.findChild(model, model.split(":")[0] + ":" + name)
        else:
            node = dag.findChild(model, name)
        if not node and len(model.split(":")) == 2:
            mgear.log("Can't find object : %s:%s"%( model.split(":")[0], name), mgear.sev_error)
        elif not node:
            mgear.log("Can't find object : %s"%( name), mgear.sev_error)
        nodes.append(node)

    if not nodes:
        return
    if mouse_button == QtCore.Qt.RightButton:
        mirrorPose(False, nodes)
        return
    if mouse_button == QtCore.Qt.MiddleButton:
        mirrorPose(True, nodes)
        return
    # Key pressed
    if key_modifier is None:
        pm.select(nodes)
    elif key_modifier == QtCore.Qt.NoModifier:# No Key
        pm.select(nodes)
    elif key_modifier == QtCore.Qt.ControlModifier: # ctrl
        pm.select(nodes, add=True)
    elif key_modifier == QtCore.Qt.ShiftModifier: # shift
        pm.select(nodes, toggle=True)
    elif int(key_modifier) == QtCore.Qt.ControlModifier + QtCore.Qt.ShiftModifier: # ctrl + shift
        pm.select(nodes, deselect=True)
    elif key_modifier == QtCore.Qt.AltModifier: # alt
        pm.select(nodes)
    elif int(key_modifier) == QtCore.Qt.ControlModifier + QtCore.Qt.AltModifier: # ctrl + alt
        pm.select(nodes, add=True)
    elif int(key_modifier) == QtCore.Qt.ShiftModifier + QtCore.Qt.AltModifier: # shift + alt
        pm.select(nodes, toggle=True)
    elif int(key_modifier) == QtCore.Qt.ControlModifier + QtCore.Qt.AltModifier + QtCore.Qt.ShiftModifier: # Ctrl + alt + shift
        pm.select(nodes, deselect=True)
    else:
        pm.select(nodes)

# ================================================
def quickSel(model, channel, mouse_button):

    qs_attr = model.attr("quicksel%s"%channel)

    if mouse_button == QtCore.Qt.LeftButton: # Call Selection
        names = qs_attr.get().split(",")
        if not names:
            return
        pm.select(clear=True)
        for name in names:
            ctl = dag.findChild(model, name)
            if ctl:
                ctl.select(add=True)
    elif mouse_button == QtCore.Qt.MidButton: # Save Selection
        names = [ sel.name().split("|")[-1] for sel in pm.ls(selection=True) if sel.name().endswith("_ctl") ]
        qs_attr.set(",".join(names))

    elif mouse_button == QtCore.Qt.RightButton: # Key Selection
        names = qs_attr.get().split(",")
        if not names:
            return
        else:
            keyObj(model, names)

# ================================================
def selAll(model):

    controlers = getControlers(model)
    pm.select(controlers)

##################################################
# KEY
##################################################
# ================================================
def keySel():
    pm.setKeyframe()

# ================================================
def keyObj(model, object_names):

    nodes = []
    for name in object_names:
        if  len(model.split(":")) == 2:
            node = dag.findChild(model, model.split(":")[0] + ":" + name)
        else:
            node = dag.findChild(model, name)
        if not node and len(model.split(":")) == 2:
            mgear.log("Can't find object : %s:%s"%( model.split(":")[0], name), mgear.sev_error)
        elif not node:
            mgear.log("Can't find object : %s"%( name), mgear.sev_error)
        nodes.append(node)

    if not nodes:
        return

    pm.setKeyframe(*nodes)

# ================================================
def keyAll(model):

    controlers = getControlers(model)
    pm.setKeyframe(controlers)

##################################################
# POSE
##################################################
# ================================================
def mirrorPose(flip=False, nodes=False):

    axis = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]
    aDic = {"tx":"invTx", "ty":"invTy", "tz":"invTz", "rx":"invRx", "ry":"invRy", "rz":"invRz", "sx":"invSx", "sy":"invSy", "sz":"invSz"}
    mapDic = {"L":"R", "R":"L"}
    if not nodes:
        nodes = pm.selected()
    for oSel in nodes:
        nameParts = oSel.name().split("|")[-1].split("_")
        print nameParts

        if nameParts[1][0] == "C":
            if not oSel.attr("tx").isLocked():
                oSel.attr("tx").set(oSel.attr("tx").get()*-1)
            if not oSel.attr("ry").isLocked():
                oSel.attr("ry").set(oSel.attr("ry").get()*-1)
            if not oSel.attr("rz").isLocked():
                oSel.attr("rz").set(oSel.attr("rz").get()*-1)
        else:
            nameParts[1] = mapDic[nameParts[1][0]] + nameParts[1][1:]
            oTarget = pm.PyNode("_".join(nameParts))
            for a in axis:
                if not oSel.attr(a).isLocked():
                    if oSel.attr(aDic[a]).get():
                        inv = -1
                    else:
                        inv = 1
                    if flip:
                        flipVal = oTarget.attr(a).get()

                    oTarget.attr(a).set(oSel.attr(a).get()*inv)

                    if flip:
                        oSel.attr(a).set(flipVal*inv)


def bindPose(model):
    if len(model.name().split(":")) == 2:
        dagPoseName = model.name().split(":")[0]+':dagPose1'
    else:
        dagPoseName = 'dagPose1'
    pm.dagPose( dagPoseName, restore=True )


def resetSelTrans():
    with pm.UndoChunk():
        for obj in pm.selected():
            tra.resetTransform(obj)





