#coding: utf-8
import os, sys, json, shutil
from glob import glob
sys.path.append(r"\\parovoz-frm01\tank\shared_core\studio\install\core\python\tank_vendor")
#sys.path.append('//parovoz-frm01/Shotgun/utils')
#sys.path.append('//parovoz-frm01/Shotgun/utils2')
#sys.path.append('C:/Python27_x32/Lib/site-packages/shotgun_api3-3.0.32-py2.7.egg')
#from shotgun_config import *
#sys.path.append('//parovoz-frm01/Shotgun/utils2')
#sys.path.append('%s\\maya' % os.getenv('TOOLS_PATH'))
#import shotgun_api3 as sg
from shotgun_api3 import Shotgun
#from helpers import ProjectHelper, get_config_path

class ShotgunProjectData(object):

    all_fields = ['id', 'code', 'name', 'sg_status_list', 'sg_shot', 'content', 'tasks', 'sg_process', 'entity',
              'sg_name', 'reference', 'sg_reference', 'link', 'meta']
    ds = None
    info = {}
    check_update = False

    def __init__(self, project_location=None):
            self.location = os.getenv('PROJECT_PATH')
            self.task_info_file = 'task_info.json'
            #print(os.path.dirname(task_info_file))
            #os.chdir('repository')
            #print(os.path.exists('%s/task_info.json'% os.getcwd()))
            if os.path.exists(self.task_info_file):
                try:
                    self.user_tasks = json.load(open(self.task_info_file))
                except:
                    self.user_tasks = []
                    print(u'Ошибка загрузки')

                self.init_info()

    def auth(self, auth_data_file):
        if auth_data_file:
            try:
                auth_data = json.load(open(auth_data_file))
                self.lgn, psw = auth_data['lgn'], auth_data['psw']
                self.ds = Shotgun(SERVER_PATH, login=self.lgn, password=psw)
            except:
                print(u'Не удалось пройти авторизацию! Проверьте логин и пароль в %s' % auth_data_file)
        else:
            print(u'Не указаны данные для авторизации!')

    def init_info(self):
        if self.ds:
            proj_filter = [['name', 'is', os.path.basename(self.location[1:])]]
            proj_fields = ['name', 'sg_code', 'tasks']
            self.info = self.ds.find_one("Project", proj_filter, proj_fields)
            #tsk_fields = ['login', 'name', 'task_assignees', 'content', 'department', 'project', 'entity']
            #tsk_filter = [['login', 'is', self.lgn]]

            usr_flt = ['login', 'name', 'department', 'projects']

            #self.user_id =self.ds_shg.find('HumanUser', user_filter, user_fields)['id']
            #self.user_name =self.ds_shg.find('HumanUser', user_filter, user_fields)['name']
            #self.user_login =self.ds_shg.find('HumanUser', user_filter, user_fields)['login']
            #self.user_department =self.ds_shg.find('HumanUser', user_filter, user_fields)['department']

            user_info = self.ds.find_one('HumanUser', [['login', 'is', self.lgn]], usr_flt)
            self.user_id = 150# user_info['id']
            self.user_name = user_info['name']
            self.user_department = user_info['department']['name']
            self.user_projects = list(map(lambda x: x['name'], user_info['projects']))
            self.active_project_id =  self.info['id'] #ds.find_one('Project', [['name', 'is', self.info['name']]])['id']

            _prj_filter = [['project', 'is', {'type': 'Project', 'id': self.active_project_id}], ['content', 'is', 'Animation'],
                         ['task_assignees', 'is', {'type': 'HumanUser', 'id': self.user_id}]]

            prj_filter = [['project', 'is', {'type': 'Project', 'id': self.active_project_id}],
                          ['task_assignees', 'is', {'type': 'HumanUser', 'id': self.user_id}]]

            prj_fields = ['project', 'content', 'entity', 'sg_status_list', 'entity.Shot.sg_reference', 'entity.Shot.assets']
            self.user_tasks = self.ds.find('Task', prj_filter, prj_fields)
            try:
                with open(self.task_info_file, 'wb') as ti:
                    ti.write(json.dumps(self.user_tasks))
            except:
                print(u'Ошибка сохранения')
            #print(self.user_tasks )
            #print(self.ds.find('Shot', [['code', 'is', self.user_tasks[0]['entity']['name']]]))

            #print(projInst.ds_shg.find('HumanUser', [['department', 'is', {'type': 'Department', 'id': 4}]],
            #                           ['id', 'firstname', 'email', 'sg_active_tasks', 'users']))
            #
            #print(self.info)
            #print(self.user_tasks)
            self.comment =  '|Project:\n' + \
                   '|       Name: "%(name)s"\n' + \
                   '|       Id: "%(id)s"\n' + \
                   '|       Code: "%(sg_code)s"\n'
            #       % self.user_tasks['project']
        else:
            self.comment = u'Загружено из кэша.'
            if self.user_tasks:
                self.info = self.user_tasks[0]['project']
                self.user_projects = [self.info['name']]
                self.active_project_id = self.info['id']

    def check_updates(self):
        auth_data_file = 'C:\\sources\\tools\\shg_auth.json'
        self.auth(auth_data_file)
        self.init_info()
        ass_filter = [['project', 'is', {'type': 'Project', 'id': self.active_project_id}]]
        tsk_cnt = -1
        lst4upd = []
        for ech_tsk in self.user_tasks:
            tsk_cnt += 1
            _assets =  ech_tsk.get('entity.Shot.assets')
            ass_cnt = -1
            for ech_ass in _assets:
                ass_cnt += 1
                ass_links = self.ds.find_one('Asset', [['project', 'is', {'type': 'Project', 'id': 168}],
                                               ['id', 'is', ech_ass['id']]],
                                               ['sg_reference'])['sg_reference']
                if ass_links:
                    self.user_tasks[tsk_cnt]['entity.Shot.assets'][ass_cnt]['sg_reference'] = ass_links
                    lst4upd.append(ass_links['local_path'])
        if lst4upd and self.location[1] == ':':
            from repository_manager import ftp_connector, read_config
            from ftplib import FTP, Error
            from zipfile import ZipFile
            #root_dir, spiski, config_path = 'D:\\rep', '', 'repository_manager.config'
            ftp, root_dir, spiski  = ftp_connector()
            #spiski = open(os.path.dirname(config_path) + '\\..\\' + spiski).read()

            l4u = set(lst4upd)
            ma_msk = '%s3d\\*.ma'
            tex_msk = '%s3d\\textures\\*.zip'
            for pth in l4u:
                full_list = glob(ma_msk % pth) + glob(tex_msk % pth)
                for file_name in full_list:
                    dest_path = '%s\\%s%s' % (self.location, root_dir, file_name.split(root_dir)[-1])
                    if dest_path.split('.')[0] == dest_path and  not os.path.exists(dest_path):
                        os.makedirs(dest_path)
                    else:
                        _ech = file_name.split(root_dir)[-1].replace('\\', '/')[1:]
                        print(u'***  Копирование из %s в %s ...' % (_ech, dest_path))
                        with open(dest_path, 'wb') as fname:
                            print(u'[СТАТУС]: Получение файла %s ...' % fname.name)
                            print(ftp.retrbinary('RETR ' + _ech, fname.write))
                            #if sCP == 'cp866':
                            cmnt = u'Создан файл %s (%d).'
                            #else:
                            #    cmnt = 'Created file %s (%d).'

                            print(u'[Итог]: %s\n' % (cmnt % (fname.name, ftp.size(_ech))))
            ftp.quit()
            print(u'\n---   Копирование завершено   ***\n')
            #open('lst4upd.lst', 'wb').write('\n'.join(lst4upd))
            #self.get_data_from_repos(lst4upd)

spd = ShotgunProjectData()
