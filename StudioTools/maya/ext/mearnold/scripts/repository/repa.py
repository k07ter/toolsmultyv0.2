# ===============================================================================
# 
# Script for batch script of Maya scenes.
# Script create backup files *.*.bck
# Using:
# Copy to your scripts folder "batch" including:
# 	batchScriptProcessor.py - main script
# 	batchScriptProcessorFuncModule.py - editable script for batch procedures
# 	mayabatch.bat - main command to run maya in batch mode 
# Run command:
# 	------------------------------------------------
# 	from batch import batchScriptProcessor
# 	reload (batchScriptProcessor)
# 	batchScriptProcessor.batchScriptProcessor_Win()
# ------------------------------------------------
# Copyrights studio "Parovoz" 2015, Moscow
# Code wrote by Evgeniy Melkov
# ===============================================================================

import maya.cmds as cmds
import os
import subprocess
import glob
import shutil
import af
import sys
import re
from functools import partial

sys.path.append(os.environ['CGRU_LOCATION'] + '/plugins/maya/afanasy')
from maya_ui_proc import *
