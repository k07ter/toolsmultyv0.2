'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26


To load the menu:

import meArnold_menu
meArnold_menu.CreateMenu()

'''

from functools import partial
import pymel.core as pm


#meArnold modules
import meArnold_riggingTools
import meArnold_postSpring
import meArnoldr_proxySlicer
import meArnold_rope
import meArnold_guidesTemplates

import mearnold
import mearnold.maya.synoptic as syn
import mearnold.maya.skin as skin



def CreateMenu():

    if pm.menu('meArnold', exists=1):
        pm.deleteUI('meArnold')
    meAarnoldM = pm.menu('meArnold', p='MayaWindow', tearOff=1, allowOptionBoxes=1, label='meArnold')


    ## Rigging Tools
    riggingM = pm.menuItem(parent='meArnold', subMenu=True, tearOff=True, label='Rigging')
    pm.menuItem(label="Add NPO", command=partial(meArnold_riggingTools.addNPO))
    pm.menuItem(label="Add SHD", command=partial(meArnold_riggingTools.addShd, False, False))
    pm.setParent(riggingM, menu=True)
    pm.menuItem( divider=True )
    # pm.menuItem(subMenu=True, tearOff=True, label='Transform')
    pm.menuItem(label="Match All Trasform", command=partial(meArnold_riggingTools.matchWorldXform))
    pm.setParent(riggingM, menu=True)
    pm.menuItem( divider=True )
    pm.menuItem(subMenu=True, tearOff=True, label='CTL as Parent')
    pm.menuItem(label="Square", command=partial(meArnold_riggingTools.createCTL, "square"))
    pm.menuItem(label="Circle", command=partial(meArnold_riggingTools.createCTL, "circle"))
    pm.menuItem(label="Cube", command=partial(meArnold_riggingTools.createCTL, "cube"))
    pm.menuItem(label="Diamond", command=partial(meArnold_riggingTools.createCTL, "diamond"))
    pm.menuItem(label="Sphere", command=partial(meArnold_riggingTools.createCTL, "sphere"))
    pm.menuItem(label="Cross", command=partial(meArnold_riggingTools.createCTL, "cross"))
    pm.menuItem(label="Cross Arrow", command=partial(meArnold_riggingTools.createCTL, "crossarrow"))
    pm.menuItem(label="Pyramid", command=partial(meArnold_riggingTools.createCTL, "pyramid"))
    pm.menuItem(label="Cube With Peak", command=partial(meArnold_riggingTools.createCTL, "cubewithpeak"))
    pm.setParent(riggingM, menu=True)
    pm.menuItem(subMenu=True, tearOff=True, label='CTL as Child')
    pm.menuItem(label="Square", command=partial(meArnold_riggingTools.createCTL, "square", True))
    pm.menuItem(label="Circle", command=partial(meArnold_riggingTools.createCTL, "circle", True))
    pm.menuItem(label="Cube", command=partial(meArnold_riggingTools.createCTL, "cube", True))
    pm.menuItem(label="Diamond", command=partial(meArnold_riggingTools.createCTL, "diamond", True))
    pm.menuItem(label="Sphere", command=partial(meArnold_riggingTools.createCTL, "sphere", True))
    pm.menuItem(label="Cross", command=partial(meArnold_riggingTools.createCTL, "cross", True))
    pm.menuItem(label="Cross Arrow", command=partial(meArnold_riggingTools.createCTL, "crossarrow", True))
    pm.menuItem(label="Pyramid", command=partial(meArnold_riggingTools.createCTL, "pyramid", True))
    pm.menuItem(label="Cube With Peak", command=partial(meArnold_riggingTools.createCTL, "cubewithpeak", True))
    pm.setParent(riggingM, menu=True)
    pm.menuItem( divider=True )
    pm.menuItem(label="Duplicate symmetrical", command=partial(meArnold_riggingTools.duplicateSym))
    pm.menuItem( divider=True )
    pm.menuItem(label="Spring", command=partial(meArnold_postSpring.spring_UI))
    pm.menuItem(label="Rope", command=partial(meArnold_rope.rope_UI))

    ## Guides and templates tools
    guidesM = pm.menuItem(parent='meArnold', subMenu=True, tearOff=True, label='Guides and Templates')
    pm.menuItem(label="Guides UI", command=partial(meArnold_guidesTemplates.guideUI))
    pm.menuItem( divider=True )
    pm.menuItem(label="Build From Selection", command=partial(meArnold_guidesTemplates.buildFromSelection))
    pm.menuItem( divider=True )
    pm.menuItem(label="Import Biped Guide", command=partial(meArnold_guidesTemplates.bipedGuide))


    ## skinning tools
    skinM = pm.menuItem(parent='meArnold', subMenu=True, tearOff=True, label='Skinning')
    pm.menuItem(label="Copy Skin", command=partial(skin.skinCopy, None, None))
    pm.menuItem(label="Select Skin Deformers", command=partial(skin.selectDeformers))


    ## Modeling tools
    modelM = pm.menuItem(parent='meArnold', subMenu=True, tearOff=True, label='Modeling')
    pm.menuItem(label="Proxy Slicer", command=partial(meArnold_proxySlicer.slice))

    ## Animation Tools
    animationM = pm.menuItem(parent='meArnold', subMenu=True, tearOff=True, label='Animation')
    pm.menuItem(label="Synoptic", command=partial(syn.open))


    ## util Tools
    pm.setParent(meArnoldM, menu=True)
    pm.menuItem( divider=True )
    utilM = pm.menuItem(parent='meArnold', subMenu=True, tearOff=True, label='Utilities')
    pm.menuItem(label="reload", command=partial(mearnold.reloadModule, "mearnold"))
