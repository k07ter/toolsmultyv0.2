# ===============================================================================
# 
# Script for batch script of Maya scenes.
# Script create backup files *.*.bck
# Using:
# Copy to your scripts folder "batch" including:
# 	batchScriptProcessor.py - main script
# 	batchScriptProcessorFuncModule.py - editable script for batch procedures
# 	mayabatch.bat - main command to run maya in batch mode 
# Run command:
# 	------------------------------------------------
# 	from batch import batchScriptProcessor
# 	reload (batchScriptProcessor)
# 	batchScriptProcessor.batchScriptProcessor_Win()
# ------------------------------------------------
# Copyrights studio "Parovoz" 2015, Moscow
# Code wrote by Evgeniy Melkov
# ===============================================================================

import maya.cmds as cmds
import os, sys, subprocess, json, re
from glob import glob
import shutil
import af, cgruconfig
from os import environ as _env
from functools import partial
import maya.mel as mel
from maya_ui_proc import *
from batch import SendRender as bsr
from helper.EnvManagerV2 import PiplineTools, Engine

#prefix = 'batchScriptProcessor_'
# Loading variables for Kick
pipline = PiplineTools(opts='maya')
kick = pipline.app
batch_cmd_line = kick.batchCmdLine()
#pipeline.init_env_kick()
#kick = pipline.app.kickCmd
#commands = []
#print(pipline.get_project_location())
PROJECT_PATH = os.getenv('PROJECT_PATH')
APP_ENVIRON = json.loads(os.getenv('APP_ENVIRON'))
#PROD = PROJECT_PATH + APP_ENVIRON.get('PROD')
PROD = pipline.get_prod_location()

def get_episode_content():
    #fileList = cmds.textScrollList('%s_filesList' % kick.prefix, q=1, ai=1, numberOfRows=8)
    #batchLog = cmds.textScrollList('%s_batchLog' % kick.prefix, q=1, ai=1)
    ow = os.walk(PROD)
    for root, _eps, files in ow:
        if root == PROD:
            table = dict.fromkeys(_eps, {})
            continue
        _root = os.path.basename(root)
        if 'anim' in _eps:
            _eps[:] = ['anim', 'layout', 'render', 'preview']

        info = 'Current processing path: ' + root
        cmds.textScrollList('%s_batchLog' % kick.prefix, e=1, si=1, append=[info])
        if _root in table and _eps:
            table[_root] = dict.fromkeys(_eps, {})

        if files:
            files = [ef for ef in files if ef.endswith('.ma')]
            if files:
              _eps[:] = []
              try:
                _file = files[0].split('.')[0]
                ep, sh, step, ver = _file.split('_')
                table[ep][sh][step] = files
                info = 'Scene added -^- [ episode: %s, shot: %s, type: %s ] -^-' % (ep, sh, step)
              except:
                info = 'Error parsing scene-files `%s`.'.ljust(120) % files

              cmds.textScrollList('%s_batchLog' % kick.prefix, e=1, si=len(table.get(_root) or []), append=[info])
              print(info)

    cmds.textScrollList('%s_projContent' % kick.prefix, e=1, si=1, append=table.keys())
    fst_ep = cmds.textScrollList('%s_projContent' % kick.prefix, e=1, si=1)
    print(fst_ep)
    cmds.textScrollList('%s_sceneContent' % kick.prefix, e=1, si=1, append=table['ep001'].keys())
    cmds.textScrollList('%s_shotContent' % kick.prefix, e=1, si=1, append=table['ep001']['sh001'].keys())

def convertToBatchString(commands):
    result = ''
    for c in commands:
        # c.replace('"','\\\"') + ';\n'
        result = result + c.replace('"', '\\\"') + ';'
    return ('"%s"' % (result))

def scanFiles():
    path = cmds.textFieldButtonGrp('working_dir', q=1, text=1)
    mask = cmds.textField('mask_field', q=1, tx=1)
    mask_use = cmds.checkBox('use_regex', q=True, v=True)
    if not mask_use:
        mask = '*.*'
    checkSubfolders = cmds.checkBox('check_subfolders', q=True, v=True)

    #	Find files in path with mask and check or not subfolders(bool)
    result = []
    tree = os.walk(path)
    try:
        for dir, subdirs, files in tree:
            fileList = [f.replace('\\', '/') for f in glob((dir + '/'+ mask).encode('utf-8'))]
            if fileList:
                result += fileList
            if not checkSubfolders:
                break
        if not result:
            result = ['No files found.']
    except:
        result = ['Error regular expression!']

    cmds.textScrollList('%s_filesList' % kick.prefix, e=1, ra=True, append=result)

print(PROD, kick.prefix)

def batchScriptProcessor_Start(file, function, maya_batch_path, batch_block):
            #	Make backup
            if cmds.checkBox('check_backup', q=1, v=1):
                try:
                    ##shutil.copy(file, file.replace('.', '_backup.'))
                    print('copied')
                except Exception as e:
                    print('Backup creation error: %s. Check if file %s exists' % (file, e))
                    return

            commands = ''
            # Setting options for Render to Arnold
            _cmd_opts = 'file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -o "%s"%s'
            ref = ''
            if cmds.checkBox('bsp_load_all_refs', q=1, v=1):
                ref = ' -loadReferenceDepth "all"'
            commands += _cmd_opts % (file, ref)

            rendType = getDefaultStrValue(kick.prefix, 'batch_script_name', function)
            mel.eval('python("from batch.SendRender import *; %s(%s)")' % (rendType, "'" + file + "'"))
            if cmds.checkBox('force_save', q=1, v=1):
                commands += 'python("cmds.file(save=1, f=1)")'

            commandToBatch = convertToBatchString(commands)
            comShell = '%s -command "%s"' % (maya_batch_path, commands)
            if cmds.checkBox('make_log', q=1, v=1):
                logFile = file + '_debug.log'
                log = open(str(logFile), "w")
                log.close()
                comShell += ' -log ' + logFile

            # --set project to third folder from end?
            cmd_proj = ''
            if cmds.checkBox('bsp_set_project', q=True, v=True):
                  #proj = '\\'.join(file.split('/')[:-2])
                  proj = os.path.dirname(file).replace('/anim','')
                  print('Set project: [%s] ' % proj)
                  cmd_proj = ' -proj %s' % proj
            comShell += cmd_proj

            if batch_block:
                task = af.Task(file)
                task.setCommand(comShell)
                batch_block.tasks.append(task)
            else:
                os.system(comShell)

                ## same as next both lines
                ##mel.eval('python("from helper.EnvManagerV2 import Engine; engine = Engine();")')
                ##mel.eval('python("engine.run(r"%s", ["--help"] ,communicate=False);")' % pipeline.get_maya_exe())

                ##engine = Engine()
                #kick.engine_start()
                ##engine.run(r"%s" % pipeline.get_maya_exe(mode='batch'), [] ,communicate=False)
                #' % pipeline.get_maya_exe())

def batchScriptProcessor_StartButtonCommand(*args):
    function = cmds.optionMenu('batch_script_name', q=1, value=1)
    logState = 'ON' if cmds.checkBox('make_log', q=1, v=1) else 'OFF'
    backupState = 'ON' if cmds.checkBox('check_backup', q=1, v=1) else 'WARNING! OFF'

    if not cmds.textScrollList('%s_filesList' % kick.prefix, q=1, ni=1):
        print('WARNING: No one file found!')
        return

    fileList = cmds.textScrollList('%s_filesList' % kick.prefix, q=1, ai=1)
    print('ALL ITEMS :')
    print('\n'.join(fileList))

    #	Main batch process:
    print(os.getenv('USERNAME'))
    maya_batch_path = 'export BATCH_INITIAL_USER=%s && %s' % (os.environ['USER'], kick.batchCmdLine())

    # remotely = False
    remotely = cmds.checkBox('send_to_afanacy', q=True, v=True)
    job = batch_block = None
    if (remotely):
        print('Afanasy job setup.......')
        cgru_vars = {'af_priority': 99, 'af_serverport': 51000, 'af_servername': '10.77.64.19',
                     'af_task_default_service': 'generic', 'af_task_default_capacity': 1000,
                     'af_maxrunningtasks': -1, 'ass_export_all_layers': True}
        cgruconfig.VARS.update(cgru_vars)

        job = af.Job('batchJob: %s' % function)
        job.offLine()
        batch_block = af.Block('batch_block ' + function, 'maya')
        batch_block.setParser(cgru_vars['af_task_default_service'])
        #batch_block.setHostsMask("parovoz.*")
        batch_block.setHostsMaskExclude(_env['BATCH_MASK'])

    map(lambda file: batchScriptProcessor_Start(file, function, maya_batch_path, batch_block), fileList)
    if batch_block:
        job.blocks.append(batch_block)
        job.send()

stateScan = getDefaultStrValue(kick.prefix, 'scan_scenes', 1)
projects = ['bears', 'moriki-doriki']

#	main "batchScriptProcessorWin" window:
def batchScriptProcessor_Win():
    #functions = [d.encode('utf-8') for d in dir(bsr) if d.startswith('auto') or d.endswith('Bot')]
    functions = list(filter(lambda x: x.startswith('auto') or x.endswith('Bot'), dir(bsr)))

    #	Check if the window exists.
    if cmds.window('%s_Win' % kick.prefix, exists=True):
        cmds.deleteUI('%s_Win' % kick.prefix, window=True)

    # Create a window.
    win = cmds.window('%s_Win' % kick.prefix, title="Batch script Processor", widthHeight=(600, 700))
    cmds.columnLayout(adjustableColumn=True)

    # --------Radio button for method:
    def scenesSourceChanged(*args):
        setDefaultIntValue(args[0], args[1], None, args[2])

        stateRead = 1 - stateScan
        cmds.checkBox('check_subfolders', e=1, enable=stateScan)
        cmds.textFieldButtonGrp('working_dir', e=1, enable=stateScan)
        # cmds.textFieldButtonGrp('working_dir', e= 1, enable = stateScan)
        cmds.textField('mask_field', e=1, enable=stateScan)
        cmds.checkBox('use_regex', e=1, enable=stateScan)
        cmds.textFieldButtonGrp('working_dir_file', e=1, enable=stateRead)
        scanFiles()

    cmds.rowLayout(numberOfColumns=4)
    cmds.radioCollection('scenes_source_collection')
    cmds.radioButton('scan_scenes', label='scan', cc=partial(scenesSourceChanged, kick.prefix, 'scan_scenes'))
    cmds.radioButton('read_scenes', label='from file', cc=partial(scenesSourceChanged, kick.prefix, 'read_scenes'))

    cmds.setParent('..')
    cmds.separator()

    #	----------Working directory:
    def workingDirChanged(*args):
        setDefaultStrValue(args[0], args[1], None, args[2])
        scanFiles()

    def workingDirButton(*args):
        path = cmds.fileDialog2(dialogStyle=1, fileMode=3, dir=args[0])
        cmds.textFieldButtonGrp('working_dir', forceChangeCommand=1, e=1, text=path[0])

    def changeProject(*args):
        if args[0]:
            cmds.optionMenu('select_project',
                            cc=partial(changeProject, kick.prefix, 'select_project'),
                            label='Select project:')
            for prj in projects:
                cmds.menuItem(prj, label=prj)

            #print(pipline.get_prod_location(args[1]))
            #setDefaultIntValue(kick.prefix, 'change_project', args[0])
            print(os.path.basename(PROJECT_PATH), args, PROJECT_PATH)

    cmds.rowColumnLayout(numberOfColumns=3) #, columnWidth=[(1, 200), (2, 100)]) #, columnOffset=[1, "right", 3])
    DEF_PROJ_PATH = getDefaultStrValue(kick.prefix, 'working_dir', '//alpha/prj')
    #getDefaultStrValue(kick.prefix, 'working_dir', PROJECT_PATH)
    #print(kick.prefix, PROD, DEF_PROJ_PATH) #getDefaultStrValue(kick.prefix, 'working_dir', PROJECT_PATH))
    #if os.path.isdir(PROJECT_PATH):
    #    DEF_PROJ_PATH = PROJECT_PATH
    cmds.textFieldButtonGrp('working_dir',
                            cc=partial(workingDirChanged, kick.prefix, 'working_dir'),
                            text=PROD,
                            buttonLabel='browse',
                            en=1 - getDefaultIntValue(kick.prefix, 'read_from_file', 0),
                            buttonCommand=partial(workingDirButton, PROD)
                            )

    cmds.checkBox('changeProject',
                  v=getDefaultIntValue(kick.prefix, 'change_project', 0),
                  cc=partial(changeProject),
                  label='Change project',
                  en=stateScan
                  )

    # --------------------------Check subfolders:
    def checkSubfoldersChanged(*args):
        setDefaultIntValue(prefix, 'check_subfolders', None, args[0])
        scanFiles()

    cmds.checkBox('check_subfolders',
                  v=getDefaultIntValue(kick.prefix, 'check_subfolders', 1),
                  cc=partial(checkSubfoldersChanged),
                  label='Scan all subfolders '
                  )

    cmds.setParent('..')
    cmds.checkBox('display_search_process',
                  v=getDefaultIntValue(kick.prefix, 'display_search_process', 1),
                  cc=partial(checkSubfoldersChanged),
                  label='Display search process '
                  )

    cmds.columnLayout(adjustableColumn=True)
    cmds.text(label='Processing ...', al="left", bgc=[0.8, 0.3, 0])
    cmds.textScrollList('%s_batchLog' % kick.prefix, en=1, numberOfRows=8)

    #cmds.separator()

    cmds.setParent('..')
    cmds.text(label='Finded content of project:', al="left")
    #cmds.columnLayout(adjustableColumn=True)
    cmds.rowLayout(numberOfColumns=3)
    #cmds.text(label='Finded scenes:')
    cmds.textScrollList('%s_projContent' % kick.prefix, en=1, numberOfRows=5)
    #cmds.text(label='Finded shots:')
    cmds.textScrollList('%s_sceneContent' % kick.prefix, en=1, numberOfRows=5)
    #cmds.text(label='Finded vers:')
    cmds.textScrollList('%s_shotContent' % kick.prefix, en=1, numberOfRows=5)
    cmds.setParent('..')

    #cmds.checkBox('choose_subfolders',
    #              v=getDefaultIntValue(kick.prefix, 'choose_subfolders', 1),
    #              cc=partial(selProject),
    #              label='Proj'
    #              )
    #print('ss  %s' % stateScan)
    #cmds.textField('new_proj', e=1, enable=stateScan)
    #cmds.textFieldButtonGrp('new_loc', e=1, enable=stateScan)
    # cmds.textFieldButtonGrp('working_dir', e= 1, enable = stateScan)
    #cmds.textField('show_log', cc=partial(get_shot_list()), label='')
    cmds.rowLayout(numberOfColumns=7)
    cmds.text(label=' ')

    # Reg expression:
    def useRegExChanged(*args):
        setDefaultIntValue(kick.prefix, 'use_regex', None, args[0])
        scanFiles()

    cmds.checkBox('use_regex',
                  v=getDefaultIntValue(kick.prefix, 'use_regex', 1),
                  cc=partial(useRegExChanged),
                  label='Use regular expressions'
                  )
    # Mask field:
    def maskFieldChanged(*args):
        setDefaultStrValue(args[0], args[1], None, args[2])
        scanFiles()

    cmds.text(label='file mask:', al="left")
    cmds.textField('mask_field',
                   cc=partial(maskFieldChanged, kick.prefix, 'mask_field'),
                   tx=getDefaultStrValue(kick.prefix, 'mask_field', ''))

    # Read from file:
    def workingDirFileChanged(*args):
        setDefaultStrValue(args[0], args[1], None, args[2])
        scanFiles()

    def workingDirFileButton(*args):
        path = cmds.fileDialog2(dialogStyle=2, fileMode=1, dir=args[0])
        cmds.textFieldButtonGrp('working_dir_file', forceChangeCommand=1, e=1, text=path[0])

    cmds.textFieldButtonGrp('working_dir_file',
                            cc=partial(workingDirFileChanged, kick.prefix, 'working_dir_file'),
                            text=getDefaultStrValue(kick.prefix, 'working_dir_file', '%s/scenes.txt' % PROJECT_PATH),
                            buttonLabel='browse',
                            buttonCommand=partial(workingDirFileButton,
                                                  getDefaultStrValue(kick.prefix, 'working_dir_file', '%s/scenes.txt' % PROJECT_PATH))
                            )

    # Get all batch functions and create menu: #MAKE DEFAULT VALUE!!!
    def batchScriptNameChanged(*args):
        setDefaultStrValue(args[0], args[1], None, args[2])

    #cmds.text(label=' ')
    cmds.optionMenu('batch_script_name',
                    cc=partial(batchScriptNameChanged, kick.prefix, 'batch_script_name'),
                    label='choose function:')

    #functions = ['autoRenderDraft', 'autoRenderFinal']
    #['autoRenderDraft', 'autoRenderFinal', 'autoRenderFinal_4k', 'cameraBot','hideObjectsBot','objectCorrectBot']:
        #if f.startswith('auto') or 'Bot' in f:
        ##cmds.menuItem('%s_item' % f, label=f)
    for f in functions:
        cmds.menuItem(f, label=f)
    items = cmds.optionMenu('batch_script_name', q=1, ils=True)
    val = getDefaultStrValue(kick.prefix, 'batch_script_name', functions[0])
    #sl = 1
    #if ('%s_item' % val) in items:
    #    sl = (items.index('%s_item' % val) + 1)
    #    cmds.optionMenu('batch_script_name', e=1, sl=(items.index('%s_item' % val) + 1))
    #else:
    cmds.setParent('..')

    sl = items.index(val)+1
    cmds.optionMenu('batch_script_name', e=1, sl=sl)

    #cmds.text(label=' ')
    cmds.separator()
    cmds.text(label=' ')

    def setProjectBefore(*args):
        setDefaultIntValue(kick.prefix, 'bsp_set_project', None, args[0])

    cmds.checkBox('bsp_set_project',
                  v=getDefaultIntValue(kick.prefix, 'bsp_set_project', 1),
                  cc=partial(setProjectBefore),
                  label='Set project before open scene (help : for  "../X/folder/scene.ma" scene will be set project to "X" )'
                  )

    def loadAllReferences(*args):
        setDefaultIntValue(kick.prefix, 'bsp_load_all_refs', None, args[0])

    cmds.checkBox('bsp_load_all_refs',
                  v=getDefaultIntValue(kick.prefix, 'bsp_load_all_refs', 1),
                  cc=partial(loadAllReferences),
                  label='load all references in the scene'
                  )

    #check make move from sequences
    def makePreview(*args):
        setDefaultIntValue(prefix, 'make_preview', None, args[0])

    cmds.checkBox('make_preview',
                  v=getDefaultIntValue(kick.prefix, 'make_preview', 1),
                  cc=partial(makePreview),
                  label='make preview'
                  )

    #check publish to shotgun
    def publishToShotgun(*args):
        setDefaultIntValue(prefix, 'publish_to_shotgun', None, args[0])

    cmds.checkBox('publish_to_shotgun',
                  v=getDefaultIntValue(kick.prefix, 'publish_to_shotgun', 1),
                  cc=partial(publishToShotgun),
                  label='publish to shotgun'
                  )

    cmds.text(label=' ')
    cmds.textScrollList('%s_filesList' % kick.prefix, numberOfRows=8)

    # check save
    def checkForceSaveChanged(*args):
        setDefaultIntValue(kick.prefix, 'force_save', None, args[0])

    cmds.checkBox('force_save',
                  v=getDefaultIntValue(kick.prefix, 'force_save', 1),
                  cc=partial(checkForceSaveChanged),
                  label='Save scene after processing'
                  )

    # Check afanasy:
    def checkSendToAfanasyChanged(*args):
        setDefaultIntValue(kick.prefix, 'send_to_afanacy', None, args[0])

    cmds.checkBox('send_to_afanacy',
                  v=getDefaultIntValue(kick.prefix, 'send_to_afanacy', 1),
                  cc=partial(checkSendToAfanasyChanged),
                  label='Send to Afanasy'
                  )

    # Check backup:
    def checkBackupChanged(*args):
        setDefaultIntValue(kick.prefix, 'check_backup', None, args[0])

    cmds.checkBox('check_backup',
                  v=getDefaultIntValue(kick.prefix, 'check_backup', 1),
                  cc=partial(checkBackupChanged),
                  label='Backup files before batch'
                  )

    # Make log:
    def makeLogChanged(*args):
        setDefaultIntValue(kick.prefix, 'make_log', None, args[0])

    cmds.checkBox('make_log',
                  v=getDefaultIntValue(kick.prefix, 'make_log', 1),
                  cc=partial(makeLogChanged),
                  label='Make logfile '
                  )

    cmds.button('%s_startBut' % kick.prefix, label='Start batch', command=batchScriptProcessor_StartButtonCommand,
                bgc=[0.5, 0, 0])

    # ----radio
    _select = 'read_scenes'
    if (getDefaultStrValue(kick.prefix, 'scan_scenes', 1)):
        _select = 'scan_scenes'
    #    cmds.radioCollection('scenes_source_collection', edit=True, select='scan_scenes')
    #else:
    cmds.radioCollection('scenes_source_collection', edit=True, select=_select)
    # --------

    cmds.showWindow('%s_Win' % kick.prefix)
    get_episode_content()
    scanFiles()
