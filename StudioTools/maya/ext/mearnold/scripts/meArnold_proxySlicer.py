'''

    This file is part of MGEAR.

    MGEAR is free software: you can redistribute it and/or modify
    it under the terms of the FreeBSD License

    Copyright (c) 2014, Jeremie Passerin, Miquel Campos
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those
    of the authors and should not be interpreted as representing official policies,
    either expressed or implied, of the FreeBSD Projectself.

    Author:     Jeremie Passerin      geerem@hotmail.com  www.jeremiepasserin.com
    Author:     Miquel Campos         hello@miquel-campos.com  www.miquel-campos.com
    Date:       2014 / 11 / 26

'''

#TODO: Slicer proxy: This tool is slow and dirty. Should be review and  complete rewrite.
import datetime

import pymel.core as pm

import mgear
import mgear.maya.applyop as aop
import mgear.maya.node as nod
import mgear.maya.transform as tra




def slice(parent=False, *args):
    startTime = datetime.datetime.now()
    oSel = pm.selected()[0]
    oColl = pm.skinCluster(oSel, query=True, influence=True)
    oFaces = oSel.faces
    nFaces = oSel.numFaces()


    faceGroups = []
    for x in oColl:
        faceGroups.append([])
    sCluster = pm.listConnections(pm.selected()[0].getShape(), type="skinCluster")

    sCName = sCluster[0].name()
    for iFace in range(nFaces):
        faceVtx =  oFaces[iFace].getVertices()
        oSum = False
        for iVtx in faceVtx:
            values = pm.skinPercent(sCName, oSel.vtx[iVtx], q=True, v=True  )
            if oSum:
                oSum = [L+l for L, l in zip(oSum, values)]
            else:
                oSum = values

        print "adding face: " + str(iFace) + " to group in: " + str( oColl[ oSum.index(max(oSum))])

        faceGroups[oSum.index(max(oSum))].append(iFace)


    original = oSel
    if not parent:
        try:
            parentGroup = pm.PyNode("ProxyGeo")
        except:
            parentGroup = pm.createNode("transform", n="ProxyGeo" )
    try:
        proxySet = pm.PyNode("rig_proxyGeo_grp")
    except:
        proxySet  = pm.sets(name="rig_proxyGeo_grp")

    for boneList in faceGroups:

        if len(boneList):
            newObj = pm.duplicate(original, rr=True, name= oColl[faceGroups.index(boneList)] + "_Proxy")
            for trans in ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]:

                pm.setAttr(newObj[0].name() + "." + trans, lock=0)

            c = list(newObj[0].faces)


            for index in reversed(boneList):

                c.pop(index)

            pm.delete(c)
            if parent:
                pm.parent(newObj, pm.PyNode(oColl[faceGroups.index(boneList)]), a=True)
            else:
                pm.parent(newObj, parentGroup, a=True)
                dummyCopy = pm.duplicate(newObj[0])[0]
                pm.delete(newObj[0].listRelatives(c=True))
                tra.matchWorldTransform(pm.PyNode(oColl[faceGroups.index(boneList)]), newObj[0])
                pm.parent(dummyCopy.listRelatives(c=True)[0], newObj[0], shape=True)
                pm.delete(dummyCopy)
                pm.rename(newObj[0].listRelatives(c=True)[0],newObj[0].name() + "_offset" )
                mulmat_node = aop.gear_mulmatrix_op(pm.PyNode(oColl[faceGroups.index(boneList)]).name()+".worldMatrix", newObj[0].name()+".parentInverseMatrix")
                dm_node = nod.createDecomposeMatrixNode(mulmat_node+".output")
                pm.connectAttr(dm_node+".outputTranslate", newObj[0].name()+".t")
                pm.connectAttr(dm_node+".outputRotate", newObj[0].name()+".r")
                pm.connectAttr(dm_node+".outputScale", newObj[0].name()+".s")

            print "Creating proxy for: " + str(oColl[faceGroups.index(boneList)])

            pm.sets(proxySet, add=newObj)


    endTime = datetime.datetime.now()
    finalTime = endTime - startTime
    mgear.log("=============== Slicing for: %s finish ======= [ %s  ] ======" %(oSel.name(), str(finalTime) ))

