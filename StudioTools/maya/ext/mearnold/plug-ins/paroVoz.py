"Fourth order Runge Kutta method to solve the second order differential equation for mass-spring-damper systems"

__author__ = "Konstantin"
__version__ = "Version: 1.0"
__date__ = "Date: 2008/12/04"

import os, sys, math
from maya import OpenMaya, OpenMayaAnim, OpenMayaMPx, cmds, mel

nodeName = "ParovoZ"
spNodeId = OpenMaya.MTypeId(0x58805)

# node definition
class spNode(OpenMayaMPx.MPxNode):
	currentTime = OpenMaya.MObject()
	targetPosition = OpenMaya.MObject()
	springConstant = OpenMaya.MObject()
	dampingRatio = OpenMaya.MObject()
	frameIterations = OpenMaya.MObject()
	outPosition = OpenMaya.MObject()
	outVelocity = OpenMaya.MObject()


renderSetupVersion = '1.0'

# List of all available initializers
import maya.app.renderSetup.model.initialize as modelInitializer
import maya.app.renderSetup.lightEditor.model.initialize as lightEditorModelInitializer
modules = [ modelInitializer, lightEditorModelInitializer ]

# UI components  -  batch mode (i.e. Maya IO) does not need them
if not cmds.about(batch=True):
    # Add all available UI related initializers
    import maya.app.renderSetup.views.initialize as viewsInitializer
    import maya.app.renderSetup.lightEditor.views.initialize as lightEditorViewsInitializer
    modules += [ viewsInitializer, lightEditorViewsInitializer ]

def _loadPreferredPresets(renderer):
    cmds.evalDeferred("import maya.mel as mel; import maya.cmds as cmds;\nif not cmds.about(batch=True) and \"" + renderer + 
        "\" == cmds.preferredRenderer(query=True) and cmds.renderer(\"" + renderer + 
        "\", exists=True): mel.eval(\"loadPreferredRenderGlobalsPreset(\\\"" + renderer + 
        "\\\")\")")

def initializePlugin(mobject):
    """ Initialize all the needed nodes """
    print("Init parovoz plugin ...\n")

    sys.path.append(os.getenv('MAYA_CGRU_LOCATION'))
    from afanasy.meArnoldRender import meArnoldRender as meAR
    mear = meAR() or None

    if mear:
	print("Arnold's plugin loaded ...\n")
	#print(mear.meArnoldRenderMainWnd)

    #mplugin = OpenMaya.MFnPlugin(mobject, "Autodesk", renderSetupVersion, "Any")
    # Does an early out if batch.
    #mel.eval("loadRenderLayerFilters()")
    
    #for module in modules:
    #    module.initialize(mplugin)
        
    #if not cmds.about(batch=True):
    #    cmds.callbacks(addCallback=_loadPreferredPresets, hook="rendererRegistered", owner="renderSetup")
    #    cmds.callbacks(addCallback=_loadPreferredPresets, hook="currentRendererChanged", owner="renderSetup")

def uninitializePlugin(mobject):
    """ Uninitialize all the nodes """
    print("Parovoz closing ...")
    #mplugin = OpenMaya.MFnPlugin(mobject, "Autodesk", renderSetupVersion, "Any")

    # Does an early out if batch.
    #mel.eval("unloadRenderLayerFilters()")
    
    #for module in reversed(modules):
    #    module.uninitialize(mplugin)

    #if not cmds.about(batch=True):
    #    cmds.callbacks(removeCallback=_loadPreferredPresets, hook="rendererRegistered", owner="renderSetup")
    #    cmds.callbacks(removeCallback=_loadPreferredPresets, hook="currentRendererChanged", owner="renderSetup")
