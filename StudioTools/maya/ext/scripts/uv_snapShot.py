import maya.cmds as cmds
import maya.mel as mm
import string
import re
import sys
import os
import subprocess


'''
Command:
-------------------------------
import uv_snapShot as uvss
uvss.uvSS_assignLambertUI()
-------------------------------
'''

def uvSS_openBrowser(*args):
    path = cmds.file(q=1,sn=1)
    if not path: return
    dirName = string.join(path.split('/')[:-1], '/') + '/'
    #print 'dirPath =', dirName
    texDir = ''
    #check if file in the version or root directory:
    if ('/versions/' in dirName):
        texDir = string.join(dirName.split('/')[:-2], '/') + '/' + 'textures/'
        #print 'ver!',texDir   
    else:
        texDir = dirName + 'textures/'
        #print 'root!',texDir
    texDir=texDir.replace('//alpha','x:')  
    #print sys.platform
    if sys.platform=='win32':
        subprocess.Popen(['start', texDir], shell= True)
    elif sys.platform=='darwin':
        subprocess.Popen(['open', texDir])
    else:
        try:
            subprocess.Popen(['xdg-open', texDir])
        except OSError:
            print ('OS error open %s' % texDir)

def uvSS_colorChoose(*args):
    result = cmds.colorEditor().split()
    cmds.button('color_shoose',e=1, bgc = (float(result[0]), float(result[1]),float(result[2])))
    #print float(result[0]), float(result[1]),float(result[2])
    return 0
    

def uvSS_ErrorWin(errTxt):
    if cmds.window('uvssErrorWindow', exists=True):
        cmds.deleteUI('uvssErrorWindow')
    cmds.window('uvssErrorWindow', resizeToFitChildren = True, bgc = (0.5,0.3,0.3), title='Error')
    cl = cmds.columnLayout()
    cmds.text(l=errTxt + '\n'*3)
    cmds.button(label = 'Close', w =500, c= "maya.cmds.deleteUI('uvssErrorWindow')")
    cmds.showWindow('uvssErrorWindow')
	

def attachFileToMat(mat, path):
    fileNode = '%s_file' % (mat)
    place2dTexNode = '%s_2dtex' % (mat)
    cmds.shadingNode ('file',asTexture=1, n = fileNode)
    cmds.shadingNode ('place2dTexture',asUtility=1, n = place2dTexNode)
    cmds.connectAttr ( '%s.coverage'%(place2dTexNode),  '%s.coverage'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.translateFrame'%(place2dTexNode),  '%s.translateFrame'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.rotateFrame'%(place2dTexNode),  '%s.rotateFrame'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.mirrorU'%(place2dTexNode),  '%s.mirrorU'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.mirrorV'%(place2dTexNode),  '%s.mirrorV'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.stagger'%(place2dTexNode),  '%s.stagger'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.wrapU'%(place2dTexNode),  '%s.wrapU'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.wrapV'%(place2dTexNode),  '%s.wrapV'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.repeatUV'%(place2dTexNode),  '%s.repeatUV'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.offset'%(place2dTexNode),  '%s.offset'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.rotateUV'%(place2dTexNode),  '%s.rotateUV'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.noiseUV'%(place2dTexNode),  '%s.noiseUV'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.vertexUvOne'%(place2dTexNode),  '%s.vertexUvOne'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.vertexUvTwo'%(place2dTexNode),  '%s.vertexUvTwo'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.vertexUvThree'%(place2dTexNode),  '%s.vertexUvThree'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.vertexCameraOne'%(place2dTexNode),  '%s.vertexCameraOne'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.outUV'%(place2dTexNode),  '%s.uv'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.outUvFilterSize'%(place2dTexNode),  '%s.uvFilterSize'%(fileNode), f = 1)
    cmds.connectAttr ( '%s.outColor'%(fileNode), '%s.color'%(mat), f = 1)
    cmds.setAttr ('%s.fileTextureName' % (fileNode), path, type = 'string') 
    cmds.select(fileNode, r=1)
    print ('OK: assigned texture %s to %s' % (path, mat)) 


def checkScene():
    print ('---------------checkScene----------------')
    #HERE SET CORRECT PATH PATTERN:
    #dirPattern = 'D:/prj/(.*?)/0_assets/(.*?)(/3d/versions/|/3d/)$'
    dirPattern = '//alpha/(.*?)/0_assets/(.*?)(/3d/versions/|/3d/)$'

    path = cmds.file(q=1,sn=1)  
    if (len(path)==0):
        uvSS_ErrorWin('ERROR: untitled file. Save file')
        return 1
    cFolder = path.split('/')[-2] 
    dirName = string.join(path.split('/')[:-1], '/') + '/'
    fileName =   path.split('/')[-1]
    
    print  ('-------\nDEBUG: \npath %s\ndirName %s\nfile %s\nwork folder: %s\n-------' %  (path, dirName, fileName, cFolder))
    
    #---Directory check:
    if re.match(dirPattern, dirName) is None:
        print ('ERROR: %s wrong directory' % (dirName))
        uvSS_ErrorWin('ERROR: %s wrong directory' % (dirName))
        return 1  
    else:
        print ('OK:%s correct directory' % (dirName))
        
        
    #find vars for path:
    vars =  re.findall(dirPattern, dirName)
    var1 = vars[0][0]
    var2 = vars[0][1]
    var2 = var2.split('/')[-1] #take last word for name
    print ('Vars = %s, %s' % (var1, var2))
    
    
    #---check file type
    if (cmds.checkBox('check_file_box', q=1, v=1)==1):
        filePattern_v = '^%s(_model_v|_uv_v|_texture_v)[0-9]*?\.ma$' % (var2)#version
        filePattern = '^%s\.ma$' % (var2)#in 3d
        print (filePattern_v)
        print (filePattern)
        if re.match(filePattern_v, fileName) is None and re.match(filePattern, fileName) is None:
            #this incorrect name file:
            print ('ERROR: wrong file name or maya file type (must be *.ma).')
            uvSS_ErrorWin('ERROR: wrong file name or maya file type (must be *.ma).')    
            return 1
        
        if re.match(filePattern_v, fileName) is not None:
            print ('OK: this version file type')
            #this version file:
            #check if in version folder
            if (cFolder == 'versions'): 
                print ('OK:version file in correct version directory')
                return 0
            else:
                print 'ERROR: version type file must be in version directory "versions"'
                uvSS_ErrorWin('ERROR: version type file must be in version directory "versions"')
                return 1
        if re.match(filePattern, fileName) is not None:
            print 'OK: this root file type'
            #this root file
            #check if in root folder 
            if (cFolder == '3d'): 
                print 'OK:root file in correct root directory'
                return 0
            else:
                print 'ERROR: root type file must be in root directory "3d"'
                uvSS_ErrorWin('ERROR: root type file must be in root directory "3d"')
                return 1
    else:
        print "WARNING: file name checking is disabled"
    return 0
    
def checkNameField(*args):
    baseNamePattern = '^[a-z][a-z0-9_]+$' 
    base_name =  cmds.textField('base_name_fld', q =1, tx = 1)
    if re.match(baseNamePattern, base_name) is None:
        #this incorrect name file: 
        print 'ERROR:incorrect base name:'
        #uvSS_ErrorWin('ERROR:incorrect base name:')
        
        cmds.button('assign_butt',e=1, en =0 )
        cmds.textField('base_name_fld', e = 1, bgc = (1,0.8,0.8))
        return 1
    cmds.button('assign_butt',e = 1, en =1 )
    cmds.textField('base_name_fld', e = 1, bgc = (0.8,1,0.8))
    print 'OK base name:'
    return 0
    
def createLambert(*args):
    if checkNameField():
        return 1
    #get selection
    selGeo = cmds.ls(sl=1)
    if len(selGeo) == 0:
        uvSS_ErrorWin('ERROR: Nothing selected')
        return 1
    
    #CHECK IF GEO or None
    res = cmds.radioCollection( 'resolutionRadioButtns', q =True, sl =True).split('_')[-1]
    base_name =  cmds.textField('base_name_fld', q =1, tx = 1)
    print 'current resolution = ', res
    nameMat = '%s_shdrtex_%sres' % (base_name, res)
    #check if exist same material name with other resolution
    ress = ['2048', '4096', '8192']
    ress.remove(res)
    #print 'Remains=', ress 
    for r in ress:
        temp = '%s_shdrtex_%sres' % (base_name, r)
        #print temp, nameMat
        if cmds.objExists(temp):
            print 'ERROR: Material exists with other resolution:', temp
            uvSS_ErrorWin('ERROR: Material exists with other resolution:' + temp)
            return 1
    
    #CHECK IF MAT NOT EXIST - create
    if not cmds.objExists(nameMat):
        lam = cmds.shadingNode('lambert', n = nameMat, asShader=True)
    cmds.select(selGeo, r=1)
    cmds.hyperShade (assign=nameMat)
        
    #set color
    #col =  cmds.colorSliderGrp( 'lambertColorGrp', q=1, rgb=1)
    col = cmds.button ('color_shoose', q=1, bgc=1)
    print 'Color',col
    cmds.setAttr (nameMat + '.color', col[0], col[1], col[2], type = 'double3')

    #here clean up materials
    mats = cmds.ls('*_shdrtex_*res', et='lambert')
    emptyMats = []
    for mat in mats:
        cmds.hyperShade(objects=mat)
        sel = cmds.ls(sl=1, r=1)
        if len(sel) == 0:
            emptyMats.append(mat)    
    '''
    if len(emptyMats)>0: 
        cmds.delete(emptyMats)
        print emptyMats, ' ..... are not assigned and has been deleted'
    '''
    if len(emptyMats)>0: 
        '''
        cmds.delete(emptyMats)
        print emptyMats, ' ..... are not assigned and has been deleted'
        '''
        for m in emptyMats:
            fileNode = '%s_file' % (m)
            place2dTexNode = '%s_2dtex' % (m)
            if cmds.objExists(fileNode):
                cmds.delete(fileNode)
            if cmds.objExists(place2dTexNode):
                cmds.delete(place2dTexNode)
        cmds.delete(emptyMats)
        print emptyMats, ' ..... are not assigned and has been deleted (create Lambert proc)'
    
    

def uvSS_assignLambertUI():
    if cmds.window('uvssUI', exists=True):
        cmds.deleteUI('uvssUI')
    cmds.window('uvssUI', title='UV Snapshot')
    cl = cmds.columnLayout(adj=True)
    #row1
    cmds.rowLayout(nc = 3, adj=2)
    cmds.text(l='Name:')
    cmds.textField('base_name_fld', tx = 'type_name_here', ec = checkNameField, cc = checkNameField, rfc = checkNameField, bgc = (1,0.8,0.8))
    
    #cmds.colorSliderGrp( 'lambertColorGrp', label='color', rgb=(0.2, 0.2, 1) )
    cmds.button('color_shoose', w=25, h =20, l='', c=uvSS_colorChoose, bgc = (1,1,0))
    
    cmds.setParent ('..')
    rl = cmds.rowLayout(nc=3)
    resolution = cmds.radioCollection('resolutionRadioButtns')
    r2048 = cmds.radioButton('r_2048',label='2048' )
    r4096 = cmds.radioButton('r_4096',label='4096' )
    r8192 = cmds.radioButton('r_8192',label='8192' )
    cmds.radioCollection( resolution, edit=True, select=r2048 )
    cmds.setParent ('..')
    cmds.button('assign_butt', p=cl, w=150, l='Assign material', c=createLambert, en = 0)
    cmds.button('snapshot_butt', p=cl, w=150, l='Snapshot', c=uvSS_createSnapShots, en = 1)
    cmds.button('texture_butt', p=cl, w=150, l='Assign texture', c=uvSS_assignTextures, en = 1)
    cmds.button('browse_butt', p=cl, w=150, l='Browse snapshots', c=uvSS_openBrowser, en = 1)
    cmds.checkBox( 'check_file_box', label='Check file name', v=1)
    
    cmds.showWindow()
    
    
    
    
def uvSS_assignTextures(*args):
    check = checkScene()
    if check:
        print 'Check failed'
        return 1
    #some things:
    fileType = 'png'
    path = cmds.file(q=1,sn=1)  
    dirName = string.join(path.split('/')[:-1], '/') + '/'
    print 'dirPath =', dirName
    texDir = ''
    #check if file in the version or root directory:
    if ('/versions/' in dirName):
        texDir = string.join(dirName.split('/')[:-2], '/') + '/' + 'textures/'
        print 'ver!',texDir   
    else:
        texDir = dirName + 'textures/'
        print 'root!',texDir
    
    #find materials:
    mats = cmds.ls('*_shdrtex_*res', et='lambert')
    if len(mats) == 0:
        print 'ERROR: No one named material found. Assign named material first using "Assign material"'
        uvSS_ErrorWin('ERROR: No one named material found. Assign named material first using "Assign material"')
        return 1
    for mat in mats:
        uvName = texDir + string.replace(mat, 'shdrtex','uv') + '.png'
        baseName = string.join(string.split(mat,'_')[:-2], '_')
        texName = texDir + baseName + '_diff.png'
        if cmds.file(texName, q=1, ex=1):
            attachFileToMat(mat, texName)
            cmds.rename(mat, baseName)
            
        
    

def uvSS_createSnapShots(*args):
    
    check = checkScene()
    if check:
        print 'Check failed'
        return 1
    #some things:
    fileType = 'png'
    path = cmds.file(q=1,sn=1)  
    dirName = string.join(path.split('/')[:-1], '/') + '/'
    print 'dirPath =', dirName
    texDir = ''
    #check if file in the version or root directory:
    if ('/versions/' in dirName):
        texDir = string.join(dirName.split('/')[:-2], '/') + '/' + 'textures/'
        print 'ver!',texDir   
    else:
        texDir = dirName + 'textures/'
        print 'root!',texDir
    
    #find materials:
    mats = cmds.ls('*_shdrtex_*res', et='lambert')
    if len(mats) == 0:
        print 'ERROR: No one named material found. Assign named material first using "Assign material"'
        uvSS_ErrorWin('ERROR: No one named material found. Assign named material first using "Assign material"')
        return 1
    
    #check if some materials have snapshots and delete its:
    for mat in mats:
        fileNode = '%s_file' % (mat)
        place2dTexNode = '%s_2dtex' % (mat)
        if cmds.objExists(fileNode):
            cmds.delete(fileNode)
        if cmds.objExists(place2dTexNode):
            cmds.delete(place2dTexNode)
    
    shots = ''
    #mats without objects:
    emptyMats = []
    for mat in mats:
        cmds.hyperShade(objects=mat)
        sel = cmds.ls(sl=1, r=1)
        
        if len(sel) == 0:
            print "Error: No one object has material %s. Assign material before" % (mat)
            uvSS_ErrorWin("Error: No one object has material %s. Assign material before" % (mat))
            emptyMats.append(mat)
            continue 
            
        cmds.select(sel, r=1)
        #make snapshot:
        res = mat.split('_')[-1][:4]
        uvName = texDir + string.replace(mat, 'shdrtex','uv') + '.png'
        cmds.uvSnapshot(sel, o=True, n= uvName, ff=fileType, xr=int(res), yr=int(res), uMin=0, uMax=1, vMin=0, vMax=1, aa=True)
        shots += (uvName +'......OK\n')
        #------------------------------------3d button
        #attachFileToMat(mat, uvName)
        #-------------------------------------
    if len(emptyMats)>0: 
        '''
        cmds.delete(emptyMats)
        print emptyMats, ' ..... are not assigned and has been deleted'
        '''
        for m in emptyMats:
            fileNode = '%s_file' % (m)
            place2dTexNode = '%s_2dtex' % (m)
            if cmds.objExists(fileNode):
                cmds.delete(fileNode)
            if cmds.objExists(place2dTexNode):
                cmds.delete(place2dTexNode)
        cmds.delete(emptyMats)
        print emptyMats, ' ..... are not assigned and has been deleted (uvSS_createSnapShots proc)'
        
            
    print shots
'''
def uvSS_assignUVshots():
    print 'ok'
'''
