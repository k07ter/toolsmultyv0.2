import maya.cmds as cmds
def getName(str):
    suff = ["_grp", "_geo", "_ikh", "_ikef"]
    for i in suff:
        str = str.replace(i, "")
    return str
def createCtrl(obj, size):
    ctrl = cmds.circle(c = (0, 0, 0), nr = (0, 1, 0), sw = 360, s = 8, ch = False, r = size, n = "%s_ctrl" % obj)[0]
    ctrlGrp = cmds.createNode("transform", n = "%s_grp" % ctrl)
    cmds.parent(ctrl, ctrlGrp)
    return ctrl, ctrlGrp
def setColor(ctrl, n):
    shapes = cmds.listRelatives(ctrl, s = True, ni = True)
    for i in shapes:
        cmds.setAttr("%s.overrideEnabled" % i, True)
        cmds.setAttr("%s.overrideColor" % i, n)
def shapeVis(ctrl1, ctrl2):
    shape = cmds.listRelatives(ctrl2, s = True, ni = True)
    if not cmds.attributeQuery("addCtrlVis", node = ctrl1, exists = True):
        cmds.addAttr(ctrl1, ln = "addCtrlVis", at = "bool", k = True)
    if shape is not None:
        for i in shape:
            cmds.connectAttr("%s.addCtrlVis" % ctrl1, "%s.visibility" % i)
def ph_sh_helper():
    sel = cmds.ls(sl=True)
    
    if not sel:
        cmds.error("Selection is empty")
    for i in sel:
        name = getName(i)
        node = ""
        nodeParent = cmds.listRelatives(i, p = True)
        ctrlGrp = ""
        size =  (cmds.getAttr("%s.boundingBoxSizeX" % i) + cmds.getAttr("%s.boundingBoxSizeZ" % i)) / 4
        if not "ctrl" in i:
            
            ctrl, ctrlGrp = createCtrl(name, size)
            cmds.delete(cmds.parentConstraint(i, ctrlGrp))
            node = ctrl
        else:
            node = i
            ctrlGrp = cmds.createNode("transform", n = "%s_grp" % node)
            #cmds.delete(cmds.parentConstraint(node, ctrlGrp))
            cmds.parent(node, ctrlGrp)
        
        # add ctrl
        addCtrl, addCtrlGrp = createCtrl("%s_add" % name, size*.75)
        cmds.delete(cmds.parentConstraint(node, addCtrlGrp))
        elements = i.split() if not "ctrl" in i else cmds.listRelatives(node,  c = True, type = "transform")
        if elements is not None:
            for i in elements:
                cmds.parent(i, addCtrl)
        cmds.parent(addCtrlGrp, node) 
        # ph/sn grps
        ph = cmds.createNode("transform", n = "%s_PH" % node)   
        sn = cmds.createNode("transform", n = "%s_SN" % node)    
        cmds.parent(sn, ph)
    
        cmds.parent(ctrlGrp, sn)
        # color
        setColor(node, 13)
        setColor(addCtrl, 17)
        # hide shape and create switch
        shapeVis(node, addCtrl)
        if nodeParent is not None:
            cmds.parent(ph, nodeParent[0])
        cmds.select(node)
        
        
            
        
        
            
        
            
            
            
    
    
    
#ph_sh_helper()