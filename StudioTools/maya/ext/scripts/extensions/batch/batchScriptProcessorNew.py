#===============================================================================
# 
# Script for batch script of Maya scenes.
# Script create backup files *.*.bck
# Using:
# Copy to your scripts folder "batch" including:
# 	batchScriptProcessor.py - main script
# 	batchScriptProcessorFuncModule.py - editable script for batch procedures
# 	mayabatch.bat - main command to run maya in batch mode 
# Run command:
# 	------------------------------------------------
# 	from batch import batchScriptProcessor
# 	reload (batchScriptProcessor)
# 	batchScriptProcessor.batchScriptProcessor_Win()
# ------------------------------------------------
# Copyrights studio "Parovoz" 2015, Moscow
# Code wrote by Evgeniy Melkov
#===============================================================================

import maya.cmds as cmds
import os
import subprocess
import glob
import shutil
import af
import sys
import re



from functools import partial
sys.path.append(os.environ['CGRU_LOCATION'] + '\\plugins\\maya\\afanasy')
from maya_ui_proc import *

prefix = 'batchScriptProcessor_'



def convertToBatchString(commands):
	result = ''
	for c in commands:
		#c.replace('"','\\\"') + ';\n'
		result = result + c.replace('"','\\\"') + ';'
	return ('"%s"' % (result))
		
	

def batchScriptProcessor_FindFiles(path, mask, checkSubfolders):
#	Find files in path with mask and check or not subfolders(bool) 
	result =[]
	if (checkSubfolders):
		dirs =[]
		for dir, subdirs, files in os.walk(path):
			dirs.append(dir)
		for d in dirs:
			fileList = glob.glob(d + '/' + mask)
			for f in fileList:
				result.append(f.replace('\\', '/'))
	else:
		resultTemp = glob.glob(path + '/' + mask)
		for f in resultTemp: result.append(f.replace('\\', '/'))
	return result


def batchScriptProcessor_Start(fileList, function):
#	Main batch process:
	dirBatch = os.path.dirname(__file__)
	batPath = dirBatch + '\\mayabatch.bat' #setup maya
	
	#remotely = False
	remotely = cmds.checkBox('batchScriptProcessor_checkBoxAfanasy',q = True, v = True)
	print (remotely)
	#return 
	if (remotely):
		print ('Afanasy job setup.......') 
		job = af.Job('somejob')
		job.offLine()
		batch_block = af.Block('batch_block %s' % function, 'maya')
	
		for file in fileList:
			logFile = file + '_debug.log'
			log = open(logFile, "w")
			log.close()
			
			#	Make backup
			shutil.copy(file, file.replace('.', '_backup.'))
			
			commands = []
			
			commands.append('file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -o "%s"' % (file))
			commands.append('python("from batch import batchScriptProcessorFuncModule")')
			commands.append('python("batchScriptProcessorFuncModule.%s()")' % (function))
			commands.append('python("cmds.file(save=1)")')
			
			commandToBatch = convertToBatchString(commands)
			#print commandToBatch
			comShell =  '%s -command %s -log %s' % (batPath, commandToBatch, logFile)
			#----------------------------
			
			task = af.Task(file)
			task.setCommand(comShell)
			batch_block.tasks.append(task)
		
		#os.system(comShell)
		job.blocks.append(batch_block)
		job.send()
	else:
		for file in fileList:
			logFile = file + '_debug.log'
			log = open(logFile, "w")
			log.close()
			
			#	Make backup
			shutil.copy(file, file.replace('.', '_backup.'))
			
			commands = []
			commands.append('file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -o "%s"' % (file))
			commands.append('python("from batch import batchScriptProcessorFuncModule")')
			commands.append('python("batchScriptProcessorFuncModule.%s()")' % (function))
			commands.append('python("cmds.file(save=1, f=1)")')
			commandToBatch = convertToBatchString(commands)
			print (commandToBatch)
			comShell =  '%s -command %s -log %s' % (batPath, commandToBatch, logFile)
			os.system(comShell)
			#----------------------------

	
def batchScriptProcessor_StartButtonCommand(*args):
	function = cmds.optionMenu('batchScriptProcessor_FuncMenu', q=1, value = 1)
	cmds.confirmDialog( title='Start batch process',
						message='Start << %s >> ? \nBackup:\nLog:' %  function, 
						button=['Yes','No'], 
						defaultButton='Yes', 
						cancelButton='No', 
						dismissString='No' 
						)
	if not cmds.textScrollList('batchScriptProcessor_filesList', q=1, ni=1 ):
		print ('WARNING: No one file found!')
		return
	
	fileList = cmds.textScrollList('batchScriptProcessor_filesList', q=1, ai =1 )
	print 'ALL ITEMS =', fileList;
	return 
	#batchScriptProcessor_Start(fileList, function)
	
	
def scanFiles(*args):
	path = cmds.textFieldButtonGrp('working_dir',q=1, text=1)
	mask = cmds.textField( 'mask_field', q=1, tx = 1)
	checkSubfolders = cmds.checkBox('check_subfolders', q = True, v =True)
	#Achtung: This scan must be just the same as in batchScriptProcessor_Batch(...)
	fileList =[]
	if not cmds.checkBox('read_from_file', q=1, v=1): 
		if (cmds.checkBox('use_regex', q = True, v =True)):
			rePattern = mask
			fileListAll = batchScriptProcessor_FindFiles(path,'*.*', checkSubfolders)
			for f in fileListAll:
				try:
					if re.match(rePattern, f): fileList.append(f)
				except:
					fileList = ['Error regular expression!'] 		
		else:
			fileList = batchScriptProcessor_FindFiles(path, mask, checkSubfolders)
	else:
		readFilePath = cmds.textFieldButtonGrp('working_dir_file', forceChangeCommand=1,  q = 1 , text = 1)
		if os.path.exists(readFilePath):
			print 'READ FILE = ', readFilePath
			#read scenes from file:
			readFile = open(readFilePath)
			scenes = []   
	        for line in readFile:
	        	stripedLine = line.strip()
	        	if stripedLine:
	        		#if cmds.file(stripedLine, q =1 , exists =1 ): 
	        		fileList.append(stripedLine)
	        readFile.close()
		
	
	
	cmds.textScrollList('batchScriptProcessor_filesList', e=1, ra=True)
	cmds.textScrollList('batchScriptProcessor_filesList', e=1, append=fileList)
		
	

#	Parse batch module "batchScriptProcessorFuncModule.py to find all avalable functions 
def batchScriptProcessor_ParseFunctions():
	#	Import module with all functions from batchScriptProcessorFuncModule.py:
	import batchScriptProcessorFuncModule as bmod
	reload(bmod)
	funcs =[]
	path = os.path.abspath(bmod.__file__).replace('pyc','py')
	modfile = open(path, 'r')
	modfileContent = modfile.readlines()
	modfile.close()
	tipLine ='No discription'
	tipLines =[]
	for line in modfileContent:
		if 'def' in line and '():' in line:
			funcs.append(line.split()[1].replace('():',''))
		tipLine = line
	
	funcs.sort()
	#	Return list of all functions in batchScriptProcessorFuncModule 
	return funcs

#	main "batchScriptProcessorWin" window:
def batchScriptProcessor_Win():
	
	functions = batchScriptProcessor_ParseFunctions()
	#    Check if the window exists.
	if cmds.window('batchScriptProcessor_Win', exists=True):
		cmds.deleteUI('batchScriptProcessor_Win', window=True)
		
	#    Create a window.
	win = cmds.window( 'batchScriptProcessor_Win', title="Batch script processor", widthHeight=(500, 500) )
	
	cmds.columnLayout( adjustableColumn=True )
	# Radio button for method:
	cmds.rowLayout(numberOfColumns=4)
	
	collection1 = cmds.radioCollection()
	cmds.radioButton( 'scan_scenes', label='scan' )
	cmds.radioButton( 'read_scenes', label='from file' )
	cmds.setParent('..')
	
	
	
	#	Working directory:
	def workingDirChanged(*args):
		setDefaultStrValue( args[0], args[1], None , args[2])
		scanFiles()
	
	def workingDirButton(*args):
		path = cmds.fileDialog2(dialogStyle=1, fileMode=3, dir = args[0])
		cmds.textFieldButtonGrp('working_dir', forceChangeCommand=1, e=1 , text= path[0])
		
	cmds.textFieldButtonGrp('working_dir',
							cc = partial(workingDirChanged, prefix, 'working_dir'), 
							text = getDefaultStrValue(prefix, 'working_dir', '//alpha/proj'),
							buttonLabel='browse', 
							en = 1 - getDefaultIntValue(prefix, 'read_from_file', 0),
							buttonCommand = partial(workingDirButton, getDefaultStrValue(prefix, 'working_dir', '//alpha/proj'))
							)
	
	
	
	
	# Check subfolders:
	def checkSubfoldersChanged(*args):
		setDefaultIntValue(prefix, 'check_subfolders',None ,args[0])
		scanFiles()
		
	
	cmds.checkBox('check_subfolders',
					v = getDefaultIntValue(prefix, 'check_subfolders', 1),
					cc = partial(checkSubfoldersChanged), 
					label ='Scan all subfolders ' 
					)
	cmds.text( label=' ')
	cmds.separator()
	
	
	# Read from file:
	def workingDirFileChanged(*args):
		setDefaultStrValue( args[0], args[1], None , args[2])
		scanFiles()
	
	
	def workingDirFileButton(*args):
		path = cmds.fileDialog2(dialogStyle=2, fileMode=1, dir = args[0])
		cmds.textFieldButtonGrp('working_dir_file', forceChangeCommand=1, e=1 , text= path[0])
	
	
	cmds.textFieldButtonGrp('working_dir_file',
							cc = partial(workingDirFileChanged, prefix, 'working_dir_file'), 
							text = getDefaultStrValue(prefix, 'working_dir_file', 'c:/scenes.txt'),
							buttonLabel='browse', 
							buttonCommand = partial(workingDirFileButton, getDefaultStrValue(prefix, 'working_dir_file', 'c:/scenes.txt'))
							)
	
	def readFromFileChanged(*args):
		setDefaultIntValue(prefix, 'read_from_file', None ,args[0])
		#mute some controls:
		state = 1-args[0]
		cmds.checkBox('check_subfolders', e=1, enable = state)
		cmds.textFieldButtonGrp('working_dir', e= 1, enable = state)
		
		scanFiles()
	
	
	cmds.checkBox('read_from_file',
					v = getDefaultIntValue(prefix, 'read_from_file', 0),
					cc = partial(readFromFileChanged), 
					label ='Read scenes from list' 
					)
	
	
	
	# Mask field:
	def maskFieldChanged(*args):
		setDefaultStrValue( args[0], args[1], None ,args[2])
		scanFiles()
		
	cmds.text( label=' ')
	cmds.text( label='file mask:')
	cmds.textField( 'mask_field',
					cc = partial(maskFieldChanged, prefix, 'mask_field'), 
					tx = getDefaultStrValue(prefix, 'mask_field', '*.*'))
	cmds.text( label=' ')
	
	
	
	
	# Reg expression:
	def useRegExChanged(*args):
		setDefaultIntValue(prefix, 'use_regex',None ,args[0])
		scanFiles()
		
	cmds.checkBox('use_regex',
					v = getDefaultIntValue(prefix, 'use_regex', 1),
					cc = partial(useRegExChanged),
					label='Use regular expressions' 
					)
	
	cmds.separator()
	
	# Get all batch functions and create menu: #MAKE DEFAULT VALUE!!!
	
	cmds.text( label=' ')
	cmds.optionMenu('batchScriptProcessor_FuncMenu', label='choose function:')
	for f in functions:
		cmds.menuItem(label=f )
	cmds.text( label=' ')
	cmds.separator()
	cmds.text( label=' ')
	
	#cmds.button('batchScriptProcessor_scanBut', label='Scan', command = scanFiles)
	cmds.textScrollList('batchScriptProcessor_filesList', numberOfRows=12)
	cmds.checkBox('batchScriptProcessor_checkBoxAfanasy',v= False , label='Afanasy process ')
	cmds.button('batchScriptProcessor_startBut', label='Start batch', command = batchScriptProcessor_StartButtonCommand, bgc =[0.5, 0, 0])
	
	
	cmds.showWindow( 'batchScriptProcessor_Win' )
	scanFiles()
	
	