import maya.cmds as cmds
import maya.mel as mm
import time
'''
DISCRIPTION:
Batch module "batchScriptProcessorFuncModule.py 
'''
def autoRenderDraft():
	print '------------------------------------------------------------'
	print '|	   bearsAutoRenderDraft is starting					   |'
	print '------------------------------------------------------------'
	import batchSendRender as bbsr
	reload(bbsr)
	bbsr.bearsBatchRenderFuncDraft()
	
def autoRenderFinal():
	print '------------------------------------------------------------'
	print '|	   bearsAutoRenderFinal is starting					   |'
	print '------------------------------------------------------------'
	import batchSendRender as bbsr
	reload(bbsr)
	bbsr.bearsBatchRenderFuncFinal()

def autoRenderFinal_4k():
	print '------------------------------------------------------------'
	print '|	   bearsAutoRenderFinal is starting					   |'
	print '------------------------------------------------------------'
	import batchSendRender as bbsr
	reload(bbsr)
	bbsr.bearsBatchRenderFuncFinal_4k()	
	
def cameraBot():
	print '------------------------------------------------------------'
	print '|	   bearsCameraBot is starting					   |'
	print '------------------------------------------------------------'
	mm.eval("source bearsCameraBot;")
#	import bearsCameraBot as bcb
#	reload(bcb)
#	bcb.bearsCameraOut()	
def hideObjectsBot():
	print '------------------------------------------------------------'
	print '|	   bearsHideObjectsBot is starting					   |'
	print '------------------------------------------------------------'
	mm.eval("source bearsHideObjectsBot;")
#	import bearsCameraBot as bcb
#	reload(bcb)
#	bcb.bearsCameraOut()	
def objectCorrectBot():
	print '------------------------------------------------------------'
	print '|	   bearsHideObjectsBot is starting					   |'
	print '------------------------------------------------------------'
	mm.eval("source bearsObjectCorrectBot;")
#	import bearsCameraBot as bcb
#	reload(bcb)
#	bcb.bearsCameraOut()	

