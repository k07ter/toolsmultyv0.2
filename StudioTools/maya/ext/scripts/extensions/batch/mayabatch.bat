call y:\tools\cgru-windows\setup.cmd 
rem override for Maya
call y:\tools\tools_setup.bat 
call y:\tools\maya\mayaGeneral.bat 2015-x64

rem Set Maya location:
set "MAYA_LOCATION=C:\Program Files\Autodesk\Maya%MAYA_VERSION%"

set PYTHONPATH=c:\Program Files\Autodesk\Maya2015\bin\python27.zip;%PYTHONPATH%
set PYTHONHOME=c:\Program Files\Autodesk\Maya2015\Python


"%MAYA_LOCATION%\bin\mayabatch.exe" %*
rem PAUSE
