# ===============================================================================
# Render sender script for "bears"
# Code writer E. Melkov and M. Pavlov (Parovoz studio,  Moscow 2016)
# ===============================================================================

import maya.cmds as cmds
import maya.mel as mm
import sys
import os
import re
import shutil


def cleanUpFaceControls():
    faceControls = cmds.ls('*:*:face_control')
    for fc in faceControls:
        if cmds.attributeQuery('face_text', node=fc, ex=True):
            print ('Clean up: setAttr %s.face_text to 0' % fc)
            cmds.setAttr('%s.face_text' % fc, 0)

    faceControls = cmds.ls('*:*_face_control')
    for fc in faceControls:
        if cmds.attributeQuery('face_text', node=fc, ex=True):
            print ('Clean up: setAttr %s.face_text to 0' % fc)
            cmds.setAttr('%s.face_text' % fc, 0)

    faceControls = cmds.ls('*_*_face_control')
    for fc in faceControls:
        if cmds.attributeQuery('face_text', node=fc, ex=True):
            print ('Clean up: setAttr %s.face_text to 0' % fc)
            cmds.setAttr('%s.face_text' % fc, 0)

    faceControls = cmds.ls('*_*:face_control')
    for fc in faceControls:
        if cmds.attributeQuery('face_text', node=fc, ex=True):
            print ('Clean up: setAttr %s.face_text to 0' % fc)
            cmds.setAttr('%s.face_text' % fc, 0)

    faceControls = cmds.ls('*:*:*:face_control')
    for fc in faceControls:
        if cmds.attributeQuery('face_text', node=fc, ex=True):
            print ('Clean up: setAttr %s.face_text to 0' % fc)
            cmds.setAttr('%s.face_text' % fc, 0)


def proxyToRenderChar():
    sel = cmds.ls(tr=True, sns=False)
    for i in sel:
        if i[-9:] == "character":
            cmds.setAttr("%s.proxy" % i, False)
            if cmds.attributeQuery("head", n=i, ex=True):
                cmds.setAttr("%s.head" % i, False)


def batchRenderFuncDraft():
    parentUser = ''
    try:
        parentUser = os.environ['BATCH_INITIAL_USER']
        print('\n***createRenderJob: BATCH_INITIAL_USER = %s') % parentUser

    except:
        print('\nWARNING: BATCH_INITIAL_USER not set.')

    #workSpace = '//parovoz-frm01/STRUCTURE/2_prod/ep001/sh001/workspace.mel'
    workSpace = os.path.join(os.environ['TOOLS_PATH'], 'maya', 'ext', 'scripts', 'workspace.mel')
    assDir = '//alpha/bears/temp/maya/data/'
    filePath = cmds.file(q=True, sn=True)
    fileName = filePath.split('/')[-1]
    scene = fileName.split('.')[0]
    version = scene.split('_')[-1]
    imageFilePrefix = '%s/%s' % (version, scene)

    # overwrite workspace for any case:

    proj = '/'.join(filePath.split('/')[:-2])
    try:
        shutil.copyfile(workSpace, proj + '/workspace.mel')
        print ('AF COMMAND RENDER: copied workspace to >>> %s ' % proj)
    except Exception as e:
        print ('AF COMMAND RENDER: cannot copy workspace to %s - ERROR: %s' % (proj, str(e)))
        return
    # set project:
    mm.eval('setProject "%s";' % proj)
    print ('AF COMMAND RENDER: set project = %s' % proj)

    cmds.setAttr('defaultRenderGlobals.imageFilePrefix', imageFilePrefix, type="string")
    print ('AF COMMAND RENDER: Set image prfix: %s' % imageFilePrefix)

    print ('AF COMMAND RENDER:Set draft options...')
    cmd = 'source "globalRender_draft.mel";'

    try:
        mm.eval(cmd)
        print ('AF COMMAND RENDER:Sourcing mel script with draft options..OK')
    except Exception as err:
        print ('AF COMMAND RENDER:Error sourcing mel script with draft options: %s ' % err)

    cleanUpFaceControls()
    proxyToRenderChar()

    # set exr format:
    cmds.setAttr('defaultRenderGlobals.imageFormat', 51)
    cmds.setAttr('defaultRenderGlobals.imfPluginKey', 'exr', type='string')

    # get frame range
    startFrame = int(cmds.playbackOptions(q=True, min=True))
    endFrame = int(cmds.playbackOptions(q=True, max=True))

    # meArnoldRender setup:
    from afanasy import meArnoldRender
    meArnoldRender = meArnoldRender.meArnoldRender(NoGUI=True)

    # --------overwrite meArnoldRender params
    try:
        userName = 'vbogdanov'
        if parentUser: userName = parentUser.strip()

        # set job params
        meArnoldRender.job_param['job_overwrite_workspace'] = 0
        meArnoldRender.job_param['job_start'] = startFrame
        meArnoldRender.job_param['job_end'] = endFrame
        meArnoldRender.job_param['job_new_user'] = userName
        meArnoldRender.job_param['job_paused'] = 0

        meArnoldRender.job_param['job_cleanup_ass'] = 0
        meArnoldRender.job_param['job_cleanup_maya'] = 0
        meArnoldRender.job_param["job_name"] += "_DRAFT"

        # set ass-params
        meArnoldRender.ass_param['ass_deferred'] = 1
        meArnoldRender.ass_param['ass_def_task_size'] = 10
        meArnoldRender.ass_param['ass_local_assgen'] = 0  # change to 1 for local
        meArnoldRender.ass_param['ass_dirname'] = assDir

        print ('AF COMMAND RENDER: overwrite meArnoldRender: OK ')
    except Exception as err:
        print ('AF COMMAND RENDER: Error attempting overwrite meArnoldRender: %s ' % err)

    # ------------------------------------------------
    try:
        meArnoldRender.submitJob()
        print ('AF COMMAND RENDER: Well done!..Job has been submited.')
    except Exception as err:
        print ('AF COMMAND RENDER: Error! Job has not been submited: %s ' % err)


def batchRenderFuncFinal():
    parentUser = ''
    try:
        parentUser = os.environ['BATCH_INITIAL_USER']
        print('\n***createRenderJob: BATCH_INITIAL_USER = %s') % parentUser

    except:
        print('\nWARNING: BATCH_INITIAL_USER not set. ')

    #workSpace = '//parovoz-frm01/STRUCTURE/2_prod/ep001/sh001/workspace.mel'
    workSpace = os.path.join(os.environ['TOOLS_PATH'], 'maya', 'ext', 'scripts', 'workspace.mel')
    assDir = '//alpha/bears/temp/maya/data/'
    filePath = cmds.file(q=True, sn=True)
    fileName = filePath.split('/')[-1]
    scene = fileName.split('.')[0]
    version = scene.split('_')[-1]
    imageFilePrefix = '%s/%s' % (version, scene)

    # overwrite workspace for any case:
    proj = '/'.join(filePath.split('/')[:-2])
    try:
        shutil.copyfile(workSpace, proj + '/workspace.mel')
        print ('AF COMMAND RENDER: copied workspace to >>> %s ' % proj)
    except Exception as e:
        print ('AF COMMAND RENDER: cannot copy workspace to %s - ERROR: %s' % (proj, str(e)))
        return
    # set project:
    mm.eval('setProject "%s";' % proj)
    print ('AF COMMAND RENDER: set project = %s' % proj)

    cmds.setAttr('defaultRenderGlobals.imageFilePrefix', imageFilePrefix, type="string")
    print ('AF COMMAND RENDER: Set image prfix: %s' % imageFilePrefix)

    print ('AF COMMAND RENDER:Set final options...')
    cmd = 'source "globalRender_final.mel";'

    try:
        mm.eval(cmd)
        print ('AF COMMAND RENDER:Sourcing mel script with final options..OK')
    except Exception as err:
        print ('AF COMMAND RENDER:Error sourcing mel script with final options: %s ' % err)


    cleanUpFaceControls()
    proxyToRenderChar()

    # set exr format:
    cmds.setAttr('defaultRenderGlobals.imageFormat', 51)
    cmds.setAttr('defaultRenderGlobals.imfPluginKey', 'exr', type='string')

    # get frame range
    startFrame = int(cmds.playbackOptions(q=True, min=True))
    endFrame = int(cmds.playbackOptions(q=True, max=True))

    # meArnoldRender setup:
    from afanasy import meArnoldRender
    meArnoldRender = meArnoldRender.meArnoldRender(NoGUI=True)
    meArnoldRender.initParameters()

    # --------overwrite meArnoldRender params
    try:
        userName = 'vbogdanov'

        if parentUser: userName = parentUser.strip()

        # set job params
        meArnoldRender.job_param['job_overwrite_workspace'] = 0
        meArnoldRender.job_param['job_start'] = startFrame
        meArnoldRender.job_param['job_end'] = endFrame
        meArnoldRender.job_param['job_new_user'] = userName
        meArnoldRender.job_param['job_paused'] = 0
        meArnoldRender.job_param["job_name"] = meArnoldRender.job_param["job_name"] + "_FINAL"

        # set ass-params
        meArnoldRender.ass_param['ass_deferred'] = 1
        meArnoldRender.ass_param['ass_def_task_size'] = 10
        meArnoldRender.ass_param['ass_local_assgen'] = 0  # change to 1 for local
        meArnoldRender.ass_param['ass_dirname'] = assDir
        print ('AF COMMAND RENDER: overwrite meArnoldRender: OK ')
    except Exception as err:
        print ('AF COMMAND RENDER: Error attempting overwrite meArnoldRender: %s ' % err)

    # ------------------------------------------------
    try:
        meArnoldRender.submitJob()
        print ('AF COMMAND RENDER: Well done!..Job has been submited.')
    except Exception as err:
        print ('AF COMMAND RENDER: Error! Job has not been submited: %s ' % err)


def batchRenderFuncFinal_4k():
    parentUser = ''
    try:
        parentUser = os.environ['BATCH_INITIAL_USER']
        print('\n***createRenderJob: BATCH_INITIAL_USER = %s') % parentUser

    except:
        print('\nWARNING: BATCH_INITIAL_USER not set. ')
    # init some vars
    #	workSpace = '//alpha/STRUCTURE/2_prod/ep001/sh001/workspace.mel'
    # workSpace = 'Y:/STRUCTURE/2_prod/ep001/sh001/workspace.mel'
    #workSpace = '//parovoz-frm01/STRUCTURE/2_prod/ep001/sh001/workspace.mel'
    workSpace = os.path.join(os.environ['TOOLS_PATH'], 'maya', 'ext', 'scripts', 'workspace.mel')
    assDir = '//alpha/bears/temp/maya/data/'
    filePath = cmds.file(q=True, sn=True)
    fileName = filePath.split('/')[-1]
    scene = fileName.split('.')[0]
    version = scene.split('_')[-1]
    imageFilePrefix = '%s/%s' % (version, scene)

    # overwrite workspace for any case:

    proj = '/'.join(filePath.split('/')[:-2])
    try:
        shutil.copyfile(workSpace, proj + '/workspace.mel')
        print ('AF COMMAND RENDER: copied workspace to >>> %s ' % proj)
    except Exception as e:
        print ('AF COMMAND RENDER: cannot copy workspace to %s - ERROR: %s' % (proj, str(e)))
        return
    # set project:
    mm.eval('setProject "%s";' % proj)
    print ('AF COMMAND RENDER: set project = %s' % proj)

    cmds.setAttr('defaultRenderGlobals.imageFilePrefix', imageFilePrefix, type="string")
    print ('AF COMMAND RENDER: Set image prfix: %s' % imageFilePrefix)

    print ('AF COMMAND RENDER:Set final options...')
    cmd = 'source "globalRender_final_4k.mel";'

    try:
        mm.eval(cmd)
        print ('AF COMMAND RENDER:Sourcing mel script with final options..OK')
    except Exception as err:
        print ('AF COMMAND RENDER:Error sourcing mel script with final options: %s ' % err)

    cleanUpFaceControls()
    proxyToRenderChar()

    # set exr format:
    cmds.setAttr('defaultRenderGlobals.imageFormat', 51)
    cmds.setAttr('defaultRenderGlobals.imfPluginKey', 'exr', type='string')

    # get frame range
    startFrame = int(cmds.playbackOptions(q=True, min=True))
    endFrame = int(cmds.playbackOptions(q=True, max=True))

    # meArnoldRender setup:
    from afanasy import meArnoldRender
    meArnoldRender = meArnoldRender.meArnoldRender(NoGUI=True)
    meArnoldRender.__init__(NoGUI=True)

    # --------overwrite meArnoldRender params
    try:
        userName = 'emelkov'  # <<<<<<< remove later!
        userName = 'vbogdanov'

        if parentUser: userName = parentUser.strip()

        # set job params
        meArnoldRender.job_param['job_overwrite_workspace'] = 0
        meArnoldRender.job_param['job_start'] = startFrame
        meArnoldRender.job_param['job_end'] = endFrame
        meArnoldRender.job_param['job_new_user'] = userName
        meArnoldRender.job_param['job_paused'] = 0
        meArnoldRender.job_param["job_name"] = meArnoldRender.job_param["job_name"] + "_FINAL"

        # set ass-params
        meArnoldRender.ass_param['ass_deferred'] = 1
        meArnoldRender.ass_param['ass_def_task_size'] = 10
        meArnoldRender.ass_param['ass_local_assgen'] = 0  # change to 1 for local
        meArnoldRender.ass_param['ass_dirname'] = assDir
        print ('AF COMMAND RENDER: overwrite meArnoldRender: OK ')
    except Exception as err:
        print ('AF COMMAND RENDER: Error attempting overwrite meArnoldRender: %s ' % err)

    # ------------------------------------------------
    try:
        meArnoldRender.submitJob()
        print ('AF COMMAND RENDER: Well done!..Job has been submited.')
    except Exception as err:
        print ('AF COMMAND RENDER: Error! Job has not been submited: %s ' % err)
