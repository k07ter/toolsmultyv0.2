﻿#target photoshop
fromTableToComp(this) ;

function SavePNG(fn) {

    // Путь к файлу и имя файла. Нужно обратить внимание на регистр и слэши.
    var pth= 'c:/sources/sht_gun/';
	var saveFile = new File(pth+fn+".png");
	pngSaveOptions = new PNGSaveOptions();
	app.activeDocument.saveAs(saveFile, pngSaveOptions, true, Extension.LOWERCASE);
}

function fromTableToComp(thisObj) {
    var name= activeDocument.name.split('.')[0];
    SavePNG(name);
    alert('Сохранение выполнено.');
}