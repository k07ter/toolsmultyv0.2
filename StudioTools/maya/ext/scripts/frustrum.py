 #==============================================================================
 # Select all objects in active viewport  
 # Usage:
 #
 # import frustrum
 # frustrum.frustrumObjectsSelection()
 # 
 # Code writer E. Melkov (Parovoz studio,  Moscow 2015) 
 #==============================================================================

import maya.OpenMaya as om
import maya.OpenMayaUI as omu
import maya.cmds as cmds
import maya.mel as mm

def frustrumObjectsSelection():
	gMainProgressBar = mm.eval('$tmp = $gMainProgressBar')
	startFrame = int(cmds.currentTime( query=True ))
	endFrame= int(cmds.playbackOptions(q=1, max=1))
	print startFrame, endFrame
	if (startFrame >=endFrame):
		startFrame = endFrame
	length = endFrame-startFrame
	view = omu.M3dView.active3dView()
	if (not length):
		om.MGlobal.selectFromScreen(0, 0, view.portWidth(), view.portHeight(), om.MGlobal.kAddToList )
	else:
		cmds.progressBar( gMainProgressBar, edit=True, beginProgress=True, isInterruptable=True, status='Example Calculation ...', maxValue=length )
		for f in range(startFrame, endFrame+1):
			if cmds.progressBar(gMainProgressBar, query=True, isCancelled=True ): 
				break
			
			cmds.currentTime(f)
			om.MGlobal.selectFromScreen(0, 0, view.portWidth(), view.portHeight(), om.MGlobal.kAddToList )
			cmds.progressBar(gMainProgressBar, edit=True, step=1)
		cmds.progressBar(gMainProgressBar, edit=True, endProgress=True)
	