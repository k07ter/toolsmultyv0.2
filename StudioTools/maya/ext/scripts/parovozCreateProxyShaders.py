import maya.cmds as cmds
#Create Proxy shaders
def  parovozCreateProxyShaders():
	smpls = 4
	selected = cmds.ls(type = 'shadingEngine')
	for node in selected:
		if (cmds.connectionInfo ('%s.surfaceShader' % node , isDestination = True)):
			shaderNode = cmds.connectionInfo ('%s.surfaceShader' % node, sourceFromDestination = True).split('.')[0]
			textureGrp = cmds.listConnections (shaderNode, type = "file")
			if textureGrp is not None:
				texture = textureGrp[0]
				print("Texture file found: %s\n" % texture)	
				#proxy_color = cmds.colorAtPoint (texture, o = 'RGB', u =.5, v = .5)
				proxy_colors = cmds.colorAtPoint (texture, o = 'RGB', su = smpls, sv = smpls)
				r=g=b=0.
				for i in range(smpls*smpls):
					r+= proxy_colors[i*3]
					g+= proxy_colors[1+i*3]
					b+= proxy_colors[2+i*3]
				r= r/smpls/smpls 
				g= g/smpls/smpls 
				b= b/smpls/smpls 
				#print r, g, b
				cmds.connectAttr("%s.outColor" % shaderNode, "%s.aiSurfaceShader" % node, f= True)
				lambertNode = cmds.createNode ("lambert",  n = "%s_matproxy" % shaderNode)
				cmds.connectAttr("%s.outColor" % lambertNode, "%s.surfaceShader" % node, f= True)
				cmds.setAttr ("%s.incandescence" % lambertNode, r, g, b)#-type double3 $proxy_color[0] $proxy_color[1] $proxy_color[2] ;
				cmds.setAttr ("%s.diffuse" % lambertNode, 0.222)
				
				
				
				

