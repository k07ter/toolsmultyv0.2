#===============================================================================
# Import last version sound to animation scene
# Code writer E. Melkov (Parovoz studio,  Moscow 2015)
#===============================================================================
import maya.cmds as cmds
import re
import os
import glob

def importSound(offset):
    scene_path= cmds.file(q=True, sn=True)
    scene_folder=os.path.dirname(scene_path)
    sound_folder=os.path.dirname(scene_folder)
    sound_files=[]
    try:
        for f in glob.glob(sound_folder + '/cut/*.wav'): sound_files.append(f.replace('\\', '/')) 
    except Exception as e:
        print ('...Search sound files error: %s' % e)
        return 1    
    if sound_files:
        last=sound_files[0]
    else:
        print ('...Error: No one sound file found')
        return 1
    for sf in sound_files[1:]:
        if last<=sf: last=sf
    print ('...Importing last version sound: %s' % last)
    prfx = last.split('/')[-1].split('.')[0]
    try:
        cmds.file(last, i=True, type="audio", ra=True, ignoreVersion=True, mergeNamespacesOnClash=False, rpr=prfx, options=("o=%s" % offset), pr =True)
        print ('...Import successful: %s' % last)
        return 0
    except Exception as e:
        print ('...Importing error: %s' % e)
        return 1  
    
    
    
