#===============================================================================
# Check everythng(...in future) 
# Code writer E. Melkov (Parovoz studio,  Moscow 2015)
#===============================================================================
import re

def checkMayaSceneProduction(scene):
	#-- cheat for lantern:
	if 'lantern' in scene:
		print ('Ugly guard: "Ok, you can go! Cheating Lantern-boy..."')
		return 'ok'
	#--
	scene_pattern = '^//alpha/(bears|crafts|lantern|patrol)/2_prod/ep[0-9]*/sh[0-9]*[a-z]{0,1}(_sub[0-9]*)*/(render|anim|layout)/ep[0-9]*_sh[0-9]*[a-z]{0,1}_(sub[0-9]*_)*(render|anim|layaut)_v[0-9]*\.ma$' 	
	test = re.match(scene_pattern, scene)
	if test is None:
		print ('...UGLY_GUARD: checkMayaSceneProduction(%s) - FAILED' % scene)
	else:
		print ('...UGLY_GUARD: checkMayaSceneProduction(%s) - OK' % scene)  
	return test
	
	
	