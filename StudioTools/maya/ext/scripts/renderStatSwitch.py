# Render Stats Switch
import maya.cmds as cmds

def getGeo():
	# get all geometry nodes 
	transforms = cmds.ls(tr=True)
	meshes = cmds.filterExpand(transforms, sm=12)
	toRenderArr = []
	notToRenderArr = []
	for i in meshes:
		# check if geo should be rendered
		isTrash = ("eyelid" in i or
		"pupil" in i or
		"eyebrow" in i or
		"mouth" in i)
		
		if (not isTrash):
			toRenderArr.append(i)
		else:
			notToRenderArr.append(i)
	# create two arrays, one should be rendered, other shouldn't
	resultArr = [toRenderArr, notToRenderArr]
	return resultArr
	
		
def changeRenderStats(toRender, notToRender):
	#check if lists are empty
	if (len(toRender) > 0):
		# change render stats accorging to the list geometry belongs to
		for i in toRender:
			shape = cmds.listRelatives(i, shapes=True)[0]
			cmds.setAttr(shape + ".castsShadows", 1)
			cmds.setAttr(shape + ".receiveShadows", 1)
			cmds.setAttr(shape + ".motionBlur", 1)
			cmds.setAttr(shape + ".primaryVisibility", 1)
			cmds.setAttr(shape + ".smoothShading", 1)
			cmds.setAttr(shape + ".visibleInReflections", 1)
			cmds.setAttr(shape + ".visibleInRefractions", 1)
			cmds.setAttr(shape + ".doubleSided", 1)
			cmds.setAttr(shape + ".opposite", 1)
	if (len(notToRender) > 0):
		for i in notToRender:
			shape = cmds.listRelatives(i, shapes=True)[0]
			cmds.setAttr(shape + ".castsShadows", 0)
			cmds.setAttr(shape + ".receiveShadows", 0)
			cmds.setAttr(shape + ".motionBlur", 0)
			cmds.setAttr(shape + ".primaryVisibility", 0)
			cmds.setAttr(shape + ".smoothShading", 0)
			cmds.setAttr(shape + ".visibleInReflections", 0)
			cmds.setAttr(shape + ".visibleInRefractions", 0)
			cmds.setAttr(shape + ".doubleSided", 0)
			cmds.setAttr(shape + ".opposite", 0)
# call the script
changeRenderStats(getGeo()[0], getGeo()[1])
	


