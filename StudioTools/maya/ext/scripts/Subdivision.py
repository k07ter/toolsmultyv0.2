import maya.cmds as mc

mc.select(hi=1)
listSel=mc.listRelatives(s=1,f=1)
i=listSel[0]
for i in listSel:
    mc.setAttr (i+'.useSmoothPreviewForRender', 0);
    mc.setAttr (i+'.renderSmoothLevel', 0);
    mc.setAttr (i+'.aiSubdivType', 1);
    mc.setAttr (i+'.aiSubdivIterations', 3);