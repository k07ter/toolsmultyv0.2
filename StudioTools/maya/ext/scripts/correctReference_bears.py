import maya.cmds as cmds
import maya.mel as mm
import sys
import os
import re
import shutil

# load all refs:
def loadAllReferences(refFile=None):
	chfiles=[]
	if refFile is None: 
		chfiles = cmds.file(q=1,r=1)
		if not chfiles: 
			print ('file has no references')
			return
	else:
		chfiles = cmds.referenceQuery(refFile, f=1, ch=1)
	for chf in chfiles:
		try:
			loaded = cmds.referenceQuery(chf, isLoaded=1)
			if loaded:
				print (chf, 'is loaded')
				loadAllReferences(chf)
			else:
				print (chf, 'try to load')
				refNode=cmds.file(chf, q=1, rfn=1)
				#print ('xxxxxxx', refNode)
				cmds.file(chf, lr=refNode)
				loadAllReferences(chf) 
		except:
			print (chf, ' has no children')