import os
import os.path
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from helper.EnvManagerV2 import *

#os.environ['peregrinel_LICENSE'] = '5053@10.77.54.17'
#os.environ['PRVZ_RLM_SERVER'] = '5053@10.77.54.17'
#os.environ['YETI_TMP'] = '\\\\omega\\moriki-doriki\\temp\\yeti_cache'

def main():
    print("start ass_deferred env configuration")

    maya_version = '2018'

    pipline = PiplineTools()
    maya_tool_path = pipline.get_maya_tools_path()
    mtoa_path = pipline.get_mtoa_path(maya_version=maya_version)
    maya_path = pipline.get_maya_path(maya_version=maya_version)

    #os.environ['PRVZ_RLM_SERVER'] = '5053@10.77.64.17'
    #os.environ['peregrinel_LICENSE'] = '5053@10.77.64.17'

    print(EnvVar('XBMLANGPATH')
          .appendPath([maya_path, 'icons']))

    print(EnvVar('MAYA_PLUG_IN_PATH')
          .appendPath([maya_tool_path, maya_version, 'plugins'])
          .appendPath([mtoa_path, 'plug-ins']))

    print(EnvVar('MAYA_MODULE_PATH')
          .appendPath([maya_tool_path, maya_version, 'modules']))

    print(EnvVar('MAYA_RENDER_DESC_PATH')
          .appendPath([maya_tool_path, maya_version, 'rendererDesc']))

    print(EnvVar('MAYA_PRESET_PATH')
          .appendPath([maya_tool_path, maya_version, 'presets']))

    print(EnvVar('ARNOLD_PLUGIN_PATH')
          .appendPath([mtoa_path, 'shaders']))

    print(EnvVar('MTOA_EXTENSIONS_PATH')
          .appendPath([mtoa_path, 'extensions']))

    print(EnvVar('MTOA_EXTENSIONS')
          .appendVar('MTOA_EXTENSIONS_PATH'))

    print(EnvVar('MTOA_PROCEDURALS')
          .appendPath([mtoa_path, 'procedurals']))

    print(EnvVar('PATH')
          .appendPath([mtoa_path, 'bin'])
          .appendVar('ARNOLD_PLUGIN_PATH')
          .appendVar('MTOA_EXTENSIONS')
          .appendVar('MTOA_PROCEDURALS')
          .appendPath([mtoa_path, 'plug-ins']))

    print(EnvVar('MAYA_LOCATION')
          .appendPath([maya_path]))

    if OsExtension.is_windows():
        print(EnvVar('PYTHONPATH')
              .appendWindowsPath([maya_path, 'bin', 'python27.zip'])
              .appendWindowsPath([maya_path, 'python'])
              .appendPath([mtoa_path, 'scripts']))

    print(EnvVar('PYTHONPATH')
        .appendPath([mtoa_path, 'scripts']))

    exe_path = os.path.join(maya_path, 'bin', 'Render.exe') if OsExtension.is_windows() \
        else os.path.join(maya_path, 'bin', 'Render')

    print("finish ass_deferred env configuration")

    print("start ass_deferred")

    return Engine.run(exe_path, sys.argv[1:])


if __name__ == "__main__":
    sys.exit(main())
