import os

PROJ_ID = 168
PROJ_NAME = 'moriki-doriki'
PROJ_STORAGE = '//omega'
APP_DIRS_PATH = ['/usr/autodesk/maya%s', 'C:\\Program Files\\Autodesk\\Maya%s']
APP_TOOLS_PATH = os.path.dirname(__file__)
VERSION = '2018'
all_deps = {}

for root, subd, files in os.walk(APP_TOOLS_PATH):
	subd[:] = [sd for sd in subd if sd[0] != '.']
	if files and '@' in ''.join(files):
		flag = [f_ for f_ in files if f_[0] == '@'][0]
		_root = os.path.basename(root)

		point = open(root + os.sep + flag).readline().encode('utf-8')
		key = point if point else  flag
		if not all_deps.get(key):
			all_deps[key] = root
		else:
			if type(all_deps[key]) == str:
				all_deps[key] = [all_deps[key]]
			all_deps[key].append(root)
		#print('Setup %s to %s' % (key, root))

		if not flag[1:] in subd:
			continue
	else:
		subd[:] = []
		continue
