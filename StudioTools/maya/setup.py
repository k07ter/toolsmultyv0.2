from distutils.core import setup
import py2exe, sys, os#

from helper.EnvManager import *

sys.argv.append('py2exe')

py2exe_options = {
                  'optimize': 2,
                  'excludes': ['_ssl', 'pyreadline', 'difflib', 'doctest', 'optparse', 'pickle', 'calendar', '_bz2' , 'select', 'unicodedata'],
                  'compressed': True}

setup(
    name='maya',
    version='1.0',
    options={'py2exe': py2exe_options},
    console=[
        {
            "script": "maya.py",
            "icon_resources": [(0, "icon2.ico")]
        },
    ]
)