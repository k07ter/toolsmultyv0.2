import os
import os.path
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from helper.EnvManager import *


def main():
    print("start mayabatch env configuration")

    maya_version = '2018'

    pipline = PiplineTools()
    tools = pipline.tools_path

    maya_tool_path = pipline.get_maya_tools_path()
    mtoa_path = pipline.get_mtoa_path(maya_version=maya_version)
    cgru_path = pipline.get_cgru_path()
    maya_path = pipline.get_maya_path(maya_version=maya_version)

    print(EnvVar('PRVZ_SHOTGUN_SCRIPTS')
          .appendPath([tools, 'shotgun', 'scripts']))

    print(EnvVar('TEMP')
          .appendPath([pipline.get_temp_dir_path('maya')]))

    print(EnvVar('TMPDIR')
          .appendPath([pipline.get_temp_dir_path('maya')]))

    print(EnvVar('MAYA_APP_DIR')
          .appendPath([pipline.get_pref_dir_path('maya')]))

    print(EnvVar('MAYA_SCRIPT_PATH')
          .appendPath([maya_tool_path, 'selector'])
          .appendPath([maya_tool_path, 'scripts']))

    print(EnvVar('MAYA_PLUG_IN_PATH')
          .appendPath([r'\\loco000\tools\toolsMultyV0.2\StudioTools\maya\ext\yeti\bin'])
          .appendPath([maya_tool_path, maya_version, 'plugins'])
          .appendPath([mtoa_path, 'plug-ins']))

    if OsExtension.is_windows():
        print(EnvVar('PYTHONHOME')
              .appendWindowsPath([maya_path, 'python']))

    print(EnvVar('PYTHONPATH')
          .appendPath([r'\\loco000\tools\toolsMultyV0.2\StudioTools\maya\ext\yeti'])
          .appendPath([maya_tool_path, 'python'])
          .appendPath([maya_tool_path, 'scripts'])
          .appendPath([maya_tool_path, 'scripts', 'batch'])
          .appendPath([maya_tool_path, 'scripts', 'studiolibrary'])
          .appendPath([mtoa_path, 'scripts'])
          .appendPath([cgru_path, 'afanasy', 'python'])
          .appendPath([cgru_path, 'lib', 'python'])
          .appendPath([cgru_path, 'plugins', 'maya'])
          .appendPath([cgru_path, 'plugins', 'maya', 'afanasy']))

    print(EnvVar('CGRU_PYTHON')
          .appendPath([cgru_path, 'lib', 'python']))

    print(EnvVar('CGRU_LOCATION')
          .appendPath([cgru_path]))

    print(EnvVar('XBMLANGPATH')
          .appendPath([maya_tool_path, 'icons']))

    print(EnvVar('MAYA_MODULE_PATH')
          .appendPath([maya_tool_path, maya_version, 'modules']))

    print(EnvVar('MAYA_RENDER_DESC_PATH')
          .appendPath([maya_tool_path, maya_version, 'rendererDesc']))

    print(EnvVar('MAYA_PRESET_PATH')
          .appendPath([maya_tool_path, maya_version, 'presets']))

    print(EnvVar('PATH')
          .appendPath([r'\\loco000\tools\toolsMultyV0.2\StudioTools\maya\ext\yeti\bin'])
          .appendPath([mtoa_path, 'bin'])
          .appendVar('CGRU_PYTHON')
          .appendVar('CGRU_LOCATION'))

    print("finish mayabatch env configuration")

    exe_path = os.path.join(maya_path, 'bin', 'mayabatch.exe') if OsExtension.is_windows() \
        else os.path.join(maya_path, 'bin', 'maya')

    print("run mayabatch")

    if OsExtension.is_windows():
        return Engine.run(exe_path, sys.argv[1:])
    else:
        args = ['-batch']
        for arg in sys.argv[1:]:
            args.append(arg)
        return Engine.run(exe_path, args)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as e:
        import traceback

        error = traceback.format_exc()
        print(error.upper())
