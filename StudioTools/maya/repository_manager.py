﻿#!/usr/bin/python
#coding: utf-8

import sys, os, re
from time import sleep
from ftplib import FTP, Error
from glob import glob
from json import load
from zipfile import ZipFile

try:
    sCP = sys.stdout.encoding
except:
    sCP = 'cp1251'

def read_config(file_config=None):
    # Чтение файла конфигурации
    config_path = os.getenv('PROJECT_PATH') + '\\.settings\\repository_manager.config' # + os.path.basename(sys.argv[0]).replace('.exe','.config').replace('.py','.config')
    print(config_path)
    try:
        config = load(file(config_path))

        # Если конфиг загружен
        ftp_adr = config.get('ftp_adr')
        ftp_port = config.get('ftp_port')
        #project = config.get('project')
        login = config.get('login')
        pswd = config.get('pswd')
        root_dir = config.get('root').encode('cp1251')
        spiski = config.get('spiski')
        print('Config is loaded.')
    except:
        # Конфиг не загружен
        ftp_adr = '10.77.64.59'
        ftp_port = 21
        #project = 'bears'
        login = 'ftpbears'
        pswd = 'PltDfvYth1'
        #root_dir = '/homes/ftp%s/0_assets/'
        root_dir = '0_assets'
        spiski = '*.lst'
        print('Config is internal.')

    return root_dir, spiski , ftp_adr, ftp_port, login, pswd

def ftp_connector():
    root_dir, spiski, ftp_adr, ftp_port, login, pswd = read_config()
    try:
        ftp = FTP()
        if sCP == 'cp866':
            cmnt = u'Подключаемся к хранилищу %s:%s...'
        else:
            cmnt = 'Connect to storage %s:%s ...'

        #print u'Подключаемся к хранилищу %s:%s...'.encode('cp1251').decode(sCP) % (ftp_adr, ftp_port)
        print(cmnt % (ftp_adr, ftp_port))
        ftp.connect(ftp_adr, ftp_port)
        ftp.login(login, pswd)
        ftp.cwd(root_dir)
        #ftp.set_pasv(0)

        if sCP == 'cp866':
            cmnt = u'Хранилище открыто'
        else:
            cmnt = 'Storage is open.'

        print(cmnt)
    except:
        if sCP == 'cp866':
            cmnt = u'Проблемы с FTP-подключением к (%s:%s)'
        else:
            cmnt = 'Ftp-connection (%s:%s) error.'

        print(cmnt % (ftp_adr, ftp_port))
        #print u'Проблемы с FTP-подключением к '.encode(sCP, CP) + ftp_adr
        #print 'Ftp-connection (%s:%s) error.' % (ftp_adr, ftp_port)
    return ftp, root_dir, spiski

def load_ftp_list(spiski):
  print(u'Текущее место поиска списка: %s ...' % os.path.dirname(os.getenv('PROJECT_PATH')))
  msk = glob(spiski)
  if not msk:
    rmt_lst = ftp.nlst(spiski)
    try:
        for each in rmt_lst:
            ftp.retrbinary('retr ' + each, file(each, 'wb').write)
    except:
        sleep(2)

    msk = glob(spiski)
    print(u'Списки, полученные с сервера:\n%s' % '\n'.join(msk))
    

  if not msk:
    if sCP == 'cp866':
        cmnt = u'Отсутствуют списки для загрузки.'
    else:
        cmnt = 'No lists for download.'

    print(cmnt)
    #print u'Отсутствуют списки для загрузки.'.encode(sCP, CP)
    sys.exit(1)
  return msk

def read_file_of_lst(f_lst):
    print(u'Файл')

def file_loader(_l):
    print(u'Закачка файлов ...\n')
    # перебираем все файлы из списка
    for ech in _l:
      if '/' not in ech:
        r_, l_ = _l.split('_')
        # ftp.cwd(r_)
        ech = ech.replace('\n', '')
        trg_path = re.sub(r'[\\][^\\]*$', '', os.path.join(r_, ech))
        # print(os.path.exists(root_dir + '\\' + ech))

        cat_name = os.path.dirname(root_dir + '\\' + ech)
        # создаем каталог для сцены, если его нет
        if not os.path.isdir(cat_name):
            os.makedirs(cat_name)
            # if trg_path.split('\\')[-1] == 'layout':
            #    map(lambda x: os.makedirs(trg_path + '\\' + x), ['cat', 'anim', 'layout'])

        _fls = []
        try:
            _ech = ech.replace('\\', '/')
            # если файл из списка сцен
            # if 'scenes' in l_:
            r_dn = os.path.dirname(_ech)
            # открытие файла для сохранения сцены
            with open(root_dir + '\\' + ech, 'wb') as fname:
                print(u'[СТАТУС]: Получение файла %s ...' % fname.name)
                print(ftp.retrbinary('RETR ' + _ech, fname.write))
                if sCP == 'cp866':
                    cmnt = u'Создан файл %s (%d).'
                else:
                    cmnt = 'Created file %s (%d).'

                print(u'[Итог]: %s\n' % (cmnt % (fname.name, ftp.size(_ech))))

                # print u'Получен файл ../%s (%d)'.encode(sCP, CP) % (_ech, ftp.size(_ech))
                # print 'File got ../%s (%d)' % (_ech, ftp.size(_ech))
        except:
            if _ech[-4:] == '.png':
                if sCP == 'cp866':
                    cmnt = u'Текстуры из %s:'
                else:
                    cmnt = 'Textures from %s:'

                print(cmnt % glob(_ech))
                # print u'Текстуры из %s:'.encode(sCP, CP) % _ech
                # print 'Textures from %s:' % _ech
                _ech = _ech.replace('.png', '.zip')
                # print(_ech)
                try:
                    _fls = ftp.nlst(_ech)
                except:
                    if sCP == 'cp866':
                        cmnt = u'Архив с текстурами не найден.'
                    else:
                        cmnt = 'Texture archive not found.'

                    print(cmnt)
                    # print u'Архив с текстурами не найден.'.encode(sCP, CP)
                    # print 'Texture archive not found.'

        if _fls:
            try:
                fname = open(_fls[0].replace('/', '\\'), 'wb')
                print(ftp.retrbinary('RETR ' + _fls[0], fname.write))
                if sCP == 'cp866':
                    cmnt = u'Получен архив текстур %s'
                else:
                    cmnt = 'Texture archive got %s'

                print(cmnt % fname.name)
                # print u'Получен архив текстур %s'.encode(sCP, CP) % fname.name
                # print 'Texture archive got %s' % fname.name
                fname.close()
                ZipFile(fname.name).extractall(loc + '\\' + fname.name.split('.')[0])
            except:
                if sCP == 'cp866':
                    cmnt = u'Проблемы с %s'
                else:
                    cmnt = 'Error with %s'

                print(cmnt % _fls[0])
                # print u'Проблемы с %s'.encode(sCP, CP) % _fls[0]
                # print 'Error with %s' % _fls[0]

            if os.path.exists(fname.name): os.remove(fname.name)


if __name__ == '__main__':
    if (sys.argv[-1] == '-h' and len(sys.argv) < 3) or len(sys.argv) == 2:
        mess = [u" <<<-- Утилита для получения файлов с ftp-сервера 'PAROVOZ' -->>>",
                u" 1.Если при запуске файла указать имя файла-списка, то обработается только он,",
                u"иначе все lst-файлы в текущем каталоге.",
                u" 2. Если в текущем каталоге лежит config-файл с именем утилиты, то настройки",
                u"будут браться из него.",
                u"    3. Утилиту необходимо поместить в корень проекта. В этом же каталоге",
                u"должны находиться каталоги '0_assets' и '2_prod'"]

        msg = '\n'.join(mess)
        print(msg)
        exit(1)

    root_dir, spiski = read_config()
    ftp = ftp_connector()
    spiski = open(os.path.dirname(config_path) + '\\..\\' + spiski).read()
    spiski = load_ftp_list(spiski)

    #loc = os.getcwd()
    #if root_dir in os.listdir(loc):
    #if os.path.isdir(root_dir):
    if os.path.exists(root_dir):
        loc = os.getcwd() + '\\' + root_dir

    # перебираем все найденные списки
    for _l in spiski:
        #print(file(_l).read())
        #r_, l_ = _l.split('#')
        #ftp.cwd(r_)
        for ech in file(_l).readlines():
            file_loader(ech)

    ftp.quit()
    print(u'Подключение закрыто.')
