﻿import sys, os
import os.path
try:
    from helper.EnvManagerV2 import *
except:
    sys.path.append(os.path.join(os.path.realpath('.'), ".."))
    from helper.EnvManagerV2 import *

def main():

    print("start maya env configuration")

    pipline = PiplineTools(opts='maya')

    maya_version = '2018'
    #maya_version = pipline.version
    tools = pipline.tools_path
    maya_tool_path = pipline.get_maya_tools_path()
    mtoa_path = pipline.get_mtoa_path(maya_version=maya_version)
    cgru_path = pipline.get_cgru_path()
    maya_path = pipline.get_maya_path(maya_version=maya_version)

    # set env vars
    print(EnvVar('PRVZ_SHOTGUN_SCRIPTS')
          .appendPath([tools, 'shotgun', 'scripts']))

    print(EnvVar('TEMP')
          .appendPath([pipline.get_temp_dir_path('maya')]))

    print(EnvVar('TMPDIR')
          .appendPath([pipline.get_temp_dir_path('maya')]))

    print(EnvVar('MAYA_APP_DIR')
          .appendPath([pipline.get_pref_dir_path('maya')]))

    print(EnvVar('MAYA_SCRIPT_PATH')
          .appendPath([r'%s\arnold\ext\windows\mtoadeploy\2018\scripts' % pipline.soft_tools_path])
          .appendPath([r'%s/arnold/ext/linux/mtoadeploy/2018/scripts' % pipline.soft_tools_path])
          .appendPath([maya_tool_path, 'selector'])
          .appendPath([maya_tool_path, 'scripts']))

    print(EnvVar('MAYA_PLUG_IN_PATH', False)
          .appendPath([maya_path, 'bin', 'plug-ins'])
          #.appendPath([maya_tool_path, maya_version, 'plugins'])
          ##.appendPath([r'Y:\toolsMultyV0.2\StudioTools\maya\ext\2018\modules\Yeti\v2.2.1\linux\plug-ins'])
          ##.appendPath([r'Y:\toolsMultyV0.2\StudioTools\maya\ext\2018\modules\Yeti\v2.2.1\windows\plug-ins'])
          .appendPath([r'%s/maya/2018/yeti/v2.2.1/linux/plug-ins' % pipline.soft_tools_path])
          .appendPath([r'%s\maya\2018\yeti\v2.2.1\windows\plug-ins' % pipline.soft_tools_path])
          .appendPath([mtoa_path, 'plug-ins']))

    print(EnvVar('PYTHONPATH')
          .appendPath([maya_tool_path, 'python'])
          .appendPath([maya_tool_path, 'scripts'])
          .appendPath([maya_tool_path, 'scripts', 'batch'])
          .appendPath([maya_tool_path, 'scripts', 'studiolibrary'])
          .appendPath([mtoa_path, 'scripts'])
          .appendPath([cgru_path, 'afanasy', 'python'])
          .appendPath([cgru_path, 'lib', 'python'])
          .appendPath([cgru_path, 'plugins', 'maya'])
          .appendPath([cgru_path, 'plugins', 'maya', 'afanasy']))

    print(EnvVar('CGRU_PYTHON')
          .appendPath([cgru_path, 'lib', 'python']))

    print(EnvVar('CGRU_LOCATION')
          .appendPath([cgru_path]))

    print(EnvVar('XBMLANGPATH')
          .appendPath([maya_tool_path, 'icons']))

    print(EnvVar('MAYA_MODULE_PATH')
          .appendPath([maya_tool_path, maya_version, 'modules'])
          #.appendPath([r'\\loco000\tools\toolsMultyV0.2\StudioTools\arnold\ext\Yeti\v2.2.1\windows\bin'])
          #.appendPath([r'\\loco000\tools\toolsMultyV0.2\StudioTools\arnold\ext\Yeti\v2.2.1\windows\plug-ins'])
          ##.appendPath(['\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\maya\\ext\\2018\\modules\\Yeti\\v2.2.1\\windows\\bin'])
          ##.appendPath(['\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\maya\\ext\\2018\\modules\\Yeti\\v2.2.1\\windows\\plug-ins'])
          ##.appendPath(['//loco000/tools/toolsMultyV0.2/StudioTools/maya/ext/2018/modules/Yeti/v2.2.1/linux/bin'])
          ##.appendPath(['//loco000/tools/toolsMultyV0.2/StudioTools/maya/ext/2018/modules/Yeti/v2.2.1/linux/plug-ins']))
          .appendPath(['%s\\maya\\2018\\yeti\\v2.2.1\\windows\\bin' % pipline.soft_tools_path])
          .appendPath(['%s\\maya\\2018\\yeti\\v2.2.1\\windows\\plug-ins' % pipline.soft_tools_path])
          .appendPath(['%s/maya/2018/yeti/v2.2.1/linux/bin' % pipline.soft_tools_path])
          .appendPath(['%s/maya/2018/yeti/v2.2.1/linux/plug-ins' % pipline.soft_tools_path]))
          ##.appendPath([maya_tool_path, maya_version, 'modules', 'Yeti', 'v2.2.1', 'linux']))
          # , 'Yeti', 'v2.2.1', 'windows'
          ##.appendPath(['//loco000/tools/toolsMultyV0.2/StudioTools/maya/ext/2018/modules/Yeti/v2.2.1/linux'])
          ##.appendPath(['\\\\loco000\\toolsMultyV0.2\\StudioTools\\maya\\ext\\2018\\modules\\Yeti\\v2.2.1\\windows']))

    os.environ["LD_LIBRARY_PATH"] = '%s/maya/2018/yeti/v2.2.1/linux/bin' % pipline.soft_tools_path
    #os.environ["LD_LIBRARY_PATH"] = '/loco000/tools/toolsMultyV0.2/StudioTools/arnold/ext/Yeti/v2.2.1/linux/bin'

    print(EnvVar('MAYA_RENDER_DESC_PATH')
          .appendPath([maya_tool_path, maya_version, 'rendererDesc']))

    print(EnvVar('MAYA_PRESET_PATH')
          .appendPath([pipline.get_pref_dir_path('maya'), maya_version, 'presets']))

    pth = EnvVar('PATH')
    pth.appendPath([mtoa_path, 'bin'])
    #.appendPath(['%s/maya/2018/yeti/v2.2.1/linux/bin' % pipline.soft_tools_path])
    #.appendPath(['%s\\maya\\2018\\yeti\\v2.2.1\\windows\\bin' % pipline.soft_tools_path])
    pth.appendVar('CGRU_PYTHON')
    pth.appendVar('CGRU_LOCATION')

    maya_path = os.path.join(maya_path, 'bin', 'maya.exe') if OsExtension.is_windows() else \
        os.path.join(maya_path, 'bin', 'maya')


    """
    YETI_LOCATION =  '\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\maya\\ext\\yeti'
    YETI_LOCATION2 = YETI_LOCATION.replace('\\', '/') + '/v2.2.1/linux'
    #'Y:\toolsMultyV0.2\StudioTools\maya\ext\yeti\v2.2.1\linux\plug-ins\'
    YETI_LOCATION_LNX = '//loco000/tools/toolsMultyV0.2/StudioTools/maya/ext/2018/modules/Yeti/linux'
    mdp = os.getenv('MAYA_MODULE_PATH')
    if not mdp:
        os.environ['MAYA_MODULE_PATH'] = '\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\maya\\ext\\bin\\plug-ins'
    os.environ['MAYA_MODULE_PATH'] += os.pathsep + YETI_LOCATION
    os.environ['MAYA_MODULE_PATH'] += os.pathsep + r'\\loco000\tools\toolsMultyV0.2\StudioTools\maya\ext\yeti\v2.2.1\windows\plug-ins'

    os.environ['MAYA_MODULE_PATH'] += os.pathsep + '//loco000/tools/toolsMultyV0.2/StudioTools/arnold/ext/Yeti/v2.2.1/linux/plug-ins'
    #os.environ['MAYA_MODULE_PATH'] += os.pathsep + '//loco000/tools/toolsMulty/StudioTools/arnold/ext/linux/mtoadeploy/2018/plug-ins'
    #os.environ['MAYA_MODULE_PATH'] += os.pathsep + '//loco000/tools/toolsMultyV0.2/StudioTools/arnold/ext/Yeti/v2.2.1/linux/plug-ins'
    os.environ['MAYA_MODULE_PATH'] += os.pathsep + YETI_LOCATION_LNX
    os.environ['MAYA_MODULE_PATH'] += os.pathsep + YETI_LOCATION2
    #os.environ['MAYA_MODULE_PATH'] += os.pathsep + '//loco000/tools/toolsMulty/StudioTools/arnold/ext/Yeti/v2.2.1/linux/plug-ins'
    #'//loco000/tools/toolsMulty/StudioTools/maya/ext/2018/modules/Yeti/linux'
    #'//loco000/tools/toolsMulty/StudioTools/arnold/ext/Yeti/v2.2.1/linux/plug-ins'

    os.environ['PATH'] += os.pathsep + '\\\\loco000\\tools\\toolsMultyV0.2\\StudioTools\\arnold\\ext\\Yeti\\v2.2.1\\windows\\plug-ins'
    os.environ['PATH'] += os.pathsep + '%s\\bin' % YETI_LOCATION
    os.environ['PATH'] += os.pathsep + '%s/bin' % YETI_LOCATION2
    os.environ['PATH'] += os.pathsep + '%s/plug-ins/' % YETI_LOCATION2
    os.environ['PATH'] += os.pathsep + '%s/plug-ins/' % '//loco000/tools/toolsMultyV0.2/StudioTools/arnold/ext/Yeti/v2.2.1/linux'
    os.environ['PATH'] += os.pathsep + '%s/bin' % YETI_LOCATION_LNX
    os.environ['PATH'] += os.pathsep + '%s/plug-ins' % YETI_LOCATION_LNX

    os.environ["LD_LIBRARY_PATH"] = '%s/bin' % YETI_LOCATION_LNX + os.pathsep + os.getenv('LD_LIBRARY_PATH', '')

    #os.environ['YETI_TMP'] = '%s/temp/.yeti' % PRVZ_PROJECT_PATH

    os.environ['MTOA_EXTENSIONS_PATH'] = os.pathsep + '%s/plug-ins' % YETI_LOCATION + os.pathsep + os.getenv(
        'MTOA_EXTENSIONS_PATH', '')

    os.environ['MTOA_EXTENSIONS_PATH'] = os.pathsep + '%s/plug-ins' % YETI_LOCATION2 + os.pathsep + os.getenv(
        'MTOA_EXTENSIONS_PATH', '')

    os.environ['MTOA_EXTENSIONS_PATH'] = os.pathsep + '%s/plug-ins' % YETI_LOCATION_LNX + os.pathsep + os.getenv(
        'MTOA_EXTENSIONS_PATH', '')

    os.environ['MTOA_PROCEDURAL_PATH'] = os.pathsep + '%s/bin' % YETI_LOCATION + os.pathsep + os.getenv(
        'MTOA_PROCEDURAL_PATH', '')

    os.environ['MTOA_PROCEDURAL_PATH'] = os.pathsep + '%s/bin' % YETI_LOCATION2 + os.pathsep + os.getenv(
        'MTOA_PROCEDURAL_PATH', '')

    os.environ['MTOA_PROCEDURAL_PATH'] = os.pathsep + '%s/bin' % YETI_LOCATION_LNX + os.pathsep + os.getenv(
        'MTOA_PROCEDURAL_PATH', '')
    """

    print("finish maya env configuration")

    print("run maya")

    return Engine.run(maya_path, communicate=False)


if __name__ == "__main__":
    sys.exit(main())
