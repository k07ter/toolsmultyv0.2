﻿import sys, os, yaml
#from os.path import join

#utils_paths = list(
#map(lambda x: sys.path.append(os.getcwd() + x), ['\\..', '\\helper'])

"""
for PREF_PATH in [['C:\\', 'Data', 'Shotgun'], ['I:\\']]:
    utils_paths += list(map(lambda x: join(*(PREF_PATH + [x])), ['', 'utils', 'utils2']))

for utils_path in utils_paths:
    if utils_path not in sys.path:
        sys.path.append(utils_path)
"""
apl, prms =  os.path.dirname(sys.argv[0]), sys.argv[1:]
#prms = ['--batch', '--shot="sadasd"', '--ver=2018']
try:
    from helper.EnvManagerV2 import *
except:
    from EnvManagerV2 import *

def to_start(pipeline=None):
    #pipeline = PipelineTools()
    #if isinstance(inst, object):
    #    pipeline.ver = inst.VER
    start_params = {}
    for prm in prms:
            try:
                k, v = prm.split('=')
            except:
                k, v = prm, True
            start_params[k] = v

    """
    if inst:
        start_params['MAYA_VERSION'] = inst.MAYA_VERSION
        start_params['MAYA_TOOLS_PATH'] = inst.MAYA_TOOLS_PATH
        start_params['MAYA_EXT_PATH'] = inst.MAYA_EXT_PATH
        start_params['MAYA_MODULES_PATH'] = inst.MAYA_MODULES_PATH
        start_params['MAYA_PLUGINS_PATH'] = inst.MAYA_PLUGINS_PATH
        start_params['MAYA_SELECTOR_PATH'] = inst.MAYA_SELECTOR_PATH
        # pipeline.maya_ver = inst.APP_VERSION
        # pipeline.maya_tools_path = inst.APP_TOOLS_PATH
        # pipeline.maya_ext_path = inst.MAYA_EXT_PATH
        # pipeline.proj_id = inst.PROJ_ID
        # pipeline.proj_title = inst.PROJ_TITLE
        # pipeline.init_opts(start_params)
    """

    #print(os.environ)
    if not pipeline and os.path.dirname(__file__) != 'maya':
        print(u'Не указаны параметры запуска приложения!')
        return

    maya_path = pipeline.get_maya_exe()
    #if pipeline.maya_batch: maya_path = maya_path.replace('maya', 'mayabatch')
    #print(maya_path)
    #print(pipeline.maya_shot)
    engine = Engine()
    #if prms: engine.init_opts(prms)
    #engine.pipeline = pipeline
    #pipeline.opts
    engine.run(maya_path, communicate=False)
    #engine.run(pipeline.get_defer_exe(), communicate=False)

if __name__ == '__main__':
    sys.exit(to_start())
