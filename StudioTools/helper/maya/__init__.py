#coding: utf-8
import sys, os, json
from sys import platform as _platform

class Maya(object):

	path = ''
	# sep = '/'
	os_t = 'linux'
	is_win = False
	ext = None

	def __init__(self, opts):
		"""
		:param os_type: тип операционной системы 
		:param path: путь для преобразования
		"""
		if 'win' in _platform:
			# self.sep = '\\'
			self.os_t = 'windows'
			self.is_win = True
			self.ext = '.exe'

		if type(opts) != str:
			try:
				self.name = opts.APL_NAME
				self.version = opts.VERSION
				self.dirs_path = opts.APP_DIRS_PATH  # opts.get('app_dir_path')
				self.mode = opts.MODE
				#self._tool_path = opts.APP_TOOLS_PATH
				info = u'Запуск основного приложения `%s`...\n' % self.name
			except:
				info = u'Не заданы расширенные параметры!'
		else:
			info = u"Запуск приложения '%s' в виде службы ...\n" % opts
			self.name = opts
			self.version = os.getenv('APP_VERSION', 'none')
			self.dirs_path = ['/usr/autodesk/maya%s', r'C:\Program Files\Autodesk\Maya%s']
			#self._tool_path = '//loco000/tools/toolsMultyV0.2/StudioTools/maya'
			self.mode = None

		_environ = json.loads(os.getenv('APP_ENVIRON', '{}'))
		if _environ:
			self.environ = _environ[self.name.upper()]

		if self.mode:
			print (info.encode('utf-8'))
		else:
			print (info)

		#self.assgen_cmd = 'arnoldExportAss %s -startFrame %d -endFrame %d -frameStep %d'
		self.assgen_ml = '<RenderLayer>/'
		self.assgen_cmd = 'arnoldExportAss -f "%s/%s/%s/%s<Scene>" -startFrame %d -endFrame %d -frameStep %d'
		self.cleanup_cmd = 'deletefiles'
		self.render_opts = ' %s -nstdin -dw -dp -o "%s" -i'
		self.deferred_opts = ' -r %s -proj "%s" -rl %s -rt 1 %s -s @#@ -e @#@ -b 1'

		mkmovie_cmd = '%s/utilities/moviemaker/makemovie.py'
		self.preview_cmd = mkmovie_cmd % os.getenv('CGRU_LOCATION')
		#self.renderCmd = 'python %s/arnold/kick.py -nstdin -dw -dp %s -o "%s" -i '
		_cmd_meta = [
			{
				'render': [],
				'deferred': [],
				'batch': []
			},
			{
				'render': [],
				'deferred': [],
				'batch': []
			}
		]

	def normalize_path(self, path, os_flag=False):
		"""
        Преобразование слэшей в путях для OS
        """
		if type(path) == list:
			_path = path
			if '\\' in _path[0] or '/' in _path[0]:
				_path = _path[self.is_win]

		if type(path) == str:
			if '/' in path:
				_path = path.split('/')
			elif '\\' in path:
				_path = path.split('\\')

		if os_flag:
			if type(self.path) == list:
				_path.append(self.os_t)
				_path = os.sep.join(_path)
			else:
				_path += os.sep + self.os_t
		return _path

	def init_environ(self, ospa):
		self.ospa = ospa
		self.dir_path = ospa.get_path() % self.version  #self.dirs_path)
		self.file_path = ospa.get_file(self.name, pref='%s/bin') #% self.version
		os.environ['APP_VERSION'] = self.version

	def tools_path(self, app=''):
		path_chain = [os.getcwd(), 'ext']
		if app: path_chain.append(app)
		return '/'.join(path_chain) #.replace('//', '/')

	#def app_path_exe(self):
	#	return self.dir_path + '/bin'

	def runCmdLine(self, mode=None, options=None):
		run_mode = {'batch': ['%s/maya --batch', '\"%s/launcher.py\" --batch'],
					#'deferred': ['python %s/maya/ass_deferred.py', 'python %s\\maya\\ass_deferred.py'],
					'deferred': ['/Render', '\\Render.exe'],
					'render': ['/kick', '\\kick.exe']
					}

		if mode:
			print('Prepare to %s mode starting ...\n' % mode)
			self.kick_path = '//loco000/tools/soft4tools/arnold/mtoadeploy/%s/%s/bin' % (self.version, self.ospa.os_t)
			if options is None:
				options = {'deferred': self.deferred_opts,
						   'render': self.render_opts}[mode]
				run_mode_line = 'python \"%s/launcher.py\"'  % os.getenv('TOOLS_PATH')
				run_mode_line += ' --apl_name=maya --project=moriki-doriki --mode=%s --options=' % mode
			else:
				run_mode_line = {'deferred': self.dir_path + '/bin',
								 'render': self.kick_path}[mode]
				run_mode_line += self.ospa.get_cmd_line(run_mode.get(mode))

			#run_mode_line = 'python \"%s\\launcher.py\"' % os.getenv('TOOLS_PATH') + options
			#run_mode_line += options
			#run_mode_line =  '\"%s\\bin\\Render.exe\"' % self.dir_path + options
			return run_mode_line, options
		else:
			#run_mode_line = self.ospa.get_file('maya', pref='bin')
			run_mode_line = self.file_path #% self.version #ospa.get_file('maya', pref='bin')
			return run_mode_line % self.dir_path, ''
		#run_mode_line = ['%s/bin/maya', r'%s\bin\maya.exe'] #[self.ext != '']

		#if type(run_mode_line) == list:
		#	run_mode_line = self.ospa.get_cmd_line(run_mode_line) % self.dir_path
		#return run_mode_line

	def batchCmdLine(self):
		self.prefix = 'batchScriptProcessor'
		cl = self.runCmdLine(mode='batch')
		return cl[0] + cl[1] + ' -command "%s"'
		#return self.cmdTemplate[1] % (self.app_path(), self.ext, self.mode[1], '-r')

	def deferredCmdLine(self):
		cl = self.runCmdLine(mode='deferred')
		return cl[0] + cl[1] #self.renderCmd
		#' -proj "%s" -rt %s -rl %s -s @#@ -e @#@ -b %s'
		#return self.cmdTemplate % (self.app_path(), self.ext, self.mode[1], '-r')

	def renderCmdLine(self):
		cl = self.runCmdLine(mode='render')  #ic self.renderCmd
		return cl[0] + cl[1]
		#+ ' -proj "%s" -rt %s -rl %s -s @#@ -e @#@ -b %s'
		#return self.cmdTemplate % (self.app_path(), self.ext, self.mode[1], '-r')
