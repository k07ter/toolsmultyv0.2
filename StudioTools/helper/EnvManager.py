﻿import sys
import os
import os.path
from sys import platform as _platform
from subprocess import Popen


class Engine(object):
    @staticmethod
    def run(path, args=None, communicate=True):
        try:
            if os.path.exists(path):
                if args is None:
                    app = Popen(path, env=os.environ)
                else:
                    app = Popen([path] + args, env=os.environ)
                if communicate:
                    app.communicate()
                return 0
            else:
                print('Engine not found! Search path: ' + path)
                return 1

        except Exception as exception:
            print("Error while trying to run engine: " + str(exception))
            return 1


class OsExtension(object):
    @staticmethod
    def is_linux():
        if _platform == "linux" or _platform == "linux2":
            return True
        return False

    @staticmethod
    def is_windows():
        return not OsExtension.is_linux()


class PiplineTools(object):
    def __init__(self):
        self.repName = 'omega'
        self.project_name = 'moriki-doriki'
        self.net_tools_path = '//loco000/tools'
        self.tools_path = 'Y:/toolsMultyV0.2/StudioTools'
        print(EnvVar('PROJECT_PATH').appendPath([self.get_project_location()]))
        print(EnvVar('TOOLS_PATH').appendPath([self.tools_path])) #print(os.environ['Path'])#print(EnvVar('PATH')) #print(self.get_cgru_path)

    def get_win_tool_path(self):
        return os.path.join('Y:\\', 'toolsMultyV0.2', 'StudioTools')

    def get_project_location(self):
        rn = '\\\\%s' % self.repName if OsExtension.is_windows() else '//%s' % self.repName
        return rn + self.project_name

    def get_temp_dir_path(self, engine):

        path = os.path.join('C:\\', '_temp', engine) if OsExtension.is_windows() else os.path.join(
            '/tmp')  # os.path.join('/tmp/_temp', engine)
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except Exception as e:
                print('can\'t create temp directory for {0}\nError:{1}'.format(engine), e)
        return path

    def get_pref_dir_path(self, engine):
        path = os.path.join('C:\\', '_prefs', engine) if OsExtension.is_windows() else os.path.join(
            '/tmp')  # os.path.join('/tmp/_prefs', engine)
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except Exception as e:
                print('can\'t create temp directory for {0}\nError:{1}'.format(engine), e)
        return path

    def get_shotgun_path(self):
        return os.path.join(self.tools_path, 'shotgun')

    def get_cgru_path(self):
        return os.path.join(self.tools_path, 'cgru', 'cgru-windows' if OsExtension.is_windows() else 'cgru-linux')

    def get_maya_tools_path(self):
        return os.path.join(self.tools_path, 'maya', 'ext')

    def get_arnold_tools_path(self):
        return os.path.join(self.tools_path, 'arnold', 'ext')

    def get_mtoa_path(self, maya_version=2018):
        if OsExtension.is_windows():
            return os.path.join(self.get_arnold_tools_path(), 'windows', 'mtoadeploy', str(maya_version))
        else:
            return os.path.join(self.get_arnold_tools_path(), 'linux', 'mtoadeploy', str(maya_version))

    def get_maya_path(self, maya_version=2018):
        if OsExtension.is_windows():
            return os.path.join('C:\Program Files\Autodesk\Maya' + str(maya_version))
        else:
            return os.path.join('/usr/autodesk/maya/')

    def get_nuke_dir_path(self):
        if OsExtension.is_windows():
            return os.path.join('C:\\', 'Program Files', 'Nuke9.0v8')
        else:
            return os.path.join('/usr/local/nuke')

    def get_nuke_exe_path(self):
        if OsExtension.is_windows():
            return os.path.join('C:\\', 'Program Files', 'Nuke9.0v8', 'Nuke9.0.exe')
        else:
            return os.path.join('/usr/local/nuke/Nuke9.0')


class EnvVar(object):
    """description of class"""

    def __init__(self, varName, clear=True):
        self.varName = varName

        # todo: check correct work for ass generation(possible problems with Python)
        if clear and (varName is not 'path' and varName is not 'PATH'):
            os.environ[self.varName] = ''

    def appendPath(self, *args):

        try:
            path = os.path.join(*args[0])

            if ";" in path:
                splited_paths = path.split(';')
                for s_path in splited_paths:
                    if not os.path.exists(s_path):
                        print("Path: '" + s_path + "' not found!")
            elif not os.path.exists(path):
                print("Path: '" + path + "' not found!")

            if self.varName in os.environ:
                # lst = os.environ[self.varName][-1:]
                if len(os.environ[self.varName]) > 0:
                    os.environ[self.varName] = path + os.pathsep + os.environ[self.varName]
                else:
                    os.environ[self.varName] = path
            else:
                os.environ[self.varName] = path

            if len(args) > 1 and args[1] and not os.path.exists(path):
                try:
                    os.makedirs(path)
                except Exception as exception:
                    print('Error while creating directory: ' + path + ' for var: ' + self.varName + os.linesep + str(
                        exception))

        except Exception as exception:
            print('Error while appending path to environment variable: ' + self.varName + os.linesep + str(exception))

        return self

    def appendLinuxPath(self, *args):
        if OsExtension.is_linux():
            self.appendPath(*args)
        return self

    def appendWindowsPath(self, *args):
        if not OsExtension.is_linux():
            self.appendPath(*args)
        return self

    def appendVar(self, varName):
        self.appendPath([os.environ[varName]])
        return self

    def setValue(self, value):
        os.environ[self.varName] = value
        return self

    def __str__(self):
        return self.varName + ': ' + os.environ[self.varName] + os.linesep
