import os

class ExtInstance(object):

	run_mode = ['', 'batch', 'render']

	def __init__(self, opts=None):
		self.name = opts.APL_NAME
		self.ext = None
		#os.path.dirname(__file__)
		self.version = opts.VERSION
		self.app_dirs_path = opts.APP_DIRS_PATH[1] #opts.get('app_dir_path')
		#self.app_tools_path = opts.TOOLS_DIR_PATH #opts.get('tools_dir_path')
		self.cmdTemplate = ['%s/bin/maya%s%s', '%s/bin/maya%s %s', '%s/bin/maya%s %s']

	def set_app_dir_path(self, dir_path):
		self.app_dir_path = dir_path
		#return '/'.join([os.getcwd(), 'ext', app, self.app_version])

	def tools_path(self, app=''):
		return '/'.join([os.getcwd(), 'ext', app, self.version])

	def app_path_exe(self):
		return self.app_dir_path + '/bin'

	def runCmdLine(self, mode=''):
		return 'python ' + self.cmdTemplate[0] % (self.app_dirs_path, self.ext, mode)

	def batchCmdLine(self):
		return self,runCmdLine(self, mode='batch')
		#return self.cmdTemplate[1] % (self.app_path(), self.ext, self.mode[1], '-r')

	def renderCmdLine(self):
		return self.runCmdLine(self, mode='render')
		#return self.cmdTemplate % (self.app_path(), self.ext, self.mode[1], '-r')
