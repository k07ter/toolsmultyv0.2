import os

class Arnold(object):

	def __init__(self):
		self.name = 'arnold'
		self.meArnoldRenderVer = '0.3.4'
		self.assgenCmd = 'arnoldExportAss'
		self.cleanupCmd = 'deletefiles'
		self.def_assgenCmd = '%s/bin/Render%s -r arnold '
		self.def_kickCmd = 'python %s/arnold/ext/kick.py -nstdin -dw -dp'
