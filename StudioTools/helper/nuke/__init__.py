#coding: utf-8
import os

class Nuke(object):

	run_mode = ['', 'batch', 'render']
	ext = '.exe'
	lm = ('basic', 'stand', 'extend')

	def __init__(self, opts):
		self.lnch_mode = self.lm[0]
		ver_digs = {'9': ('9.8', '9.8'), '11': ('11.1v1', '11.1')}
		if type(opts) != str:
			try:
				self.name = opts.APL_NAME
				#self.ext = None
				#os.path.dirname(__file__)
				self.version = '11'
				self.dir_suff, self.file_suff = ver_digs[self.version]
				#self.version = {'9': ('9.8','9.8' ), '11': ('11.1v1', '11.1')}
				self.app_dirs_path = opts.APP_DIRS_PATH #opts.get('app_dir_path')
				#self.app_tools_path = opts.TOOLS_DIR_PATH #opts.get('tools_dir_path')
				#self.cmdTemplate = ['%s/Nuke%s%s', '%s/bin/maya%s %s', '%s/bin/maya%s %s']
				self.dirs_path = ['/usr/Nuke%s', r'C:\Program Files\Nuke%s']
			except:
				print(u'Не заданы расширенные параметры!')
		else:
			self.name = opts
			self.version = '11'
			#self.ver_digs = {'9': ('9.8', '9.8'), '11': ('11.1v1', '11.1')}
			self.app_dirs_path = opts.APP_DIRS_PATH #opts.get('app_dir_path')
			self.dirs_path = ['/usr/Nuke%s', r'C:\Program Files\Nuke%s']
			#self.dirs_path = ['/usr/Nuke11.1v1/Nuke11.1', r'C:\Nuke11.1v1\Nuke11.1.exe']

	def init_environ(self, ospa):
		self.ospa = ospa
		self.dir_path = ospa.get_path() % self.dir_suff #self.ver_digs[self.version][0]  #self.dirs_path)
		self.file_path = ospa.get_file(self.name + self.file_suff)
		os.environ['APP_VERSION'] = self.version
		if self.lnch_mode == 'basic':
			os.environ['OFX_PLUGIN_PATH'] = r'Y:\soft4tools\nuke\plugins'

		if self.lnch_mode == 'basic':
			os.environ['OFX_PLUGIN_PATH'] = r'\\omega\soft4tools\nuke\plugins'

	def set_app_dir_path(self, dir_path):
		self.app_dir_path = dir_path
		#return '/'.join([os.getcwd(), 'ext', app, self.app_version])

	def tools_path(self, app=''):
		return '/'.join([os.getcwd(), 'ext', app, self.version])

	def app_path_exe(self):
		return self.app_dir_path + '/bin'

	#def runCmdLine(self, mode=''):
	#	#return self.app_dirs_path[self.ext != ''] % self.version + self.ext
	#	return self.dir_path

	def runCmdLine(self, mode=None):
		#run_mode = {'batch': ['%s/bin/maya --batch', '%s\\bin\\mayabatch.exe'],
		#			'deferred': ['python %s/maya/ass_deferred.py', 'python %s/maya/ass_deferred.py'],
		#			'render': ['%s/bin/Render', r'"%s\bin\Render.exe"']
		#			}
		if mode:
			#получаем путь к файлу через адаптер пути 'ospa'
			run_mode_line = self.ospa.get_cmd_line(run_mode.get(mode))#[self.ext != '']
		else:
			#run_mode_line = self.ospa.get_file('maya', pref='bin')
			run_mode_line = self.file_path #% self.version #ospa.get_file('maya', pref='bin')
			#run_mode_line = ['%s/bin/maya', r'%s\bin\maya.exe'] #[self.ext != '']

		#if type(run_mode_line) == list:
		#	run_mode_line = self.ospa.get_cmd_line(run_mode_line) % self.dir_path
		#	print(self.ext)
		return self.dir_path + '/' + run_mode_line


	def batchCmdLine(self):
		return self,runCmdLine(self, mode='batch')
		#return self.cmdTemplate[1] % (self.app_path(), self.ext, self.mode[1], '-r')

	def renderCmdLine(self):
		return self.runCmdLine(self, mode='render')
		#return self.cmdTemplate % (self.app_path(), self.ext, self.mode[1], '-r')
