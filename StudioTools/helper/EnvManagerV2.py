﻿import sys, os, json
from datetime import datetime
from sys import platform as _platform
from subprocess import Popen, PIPE
from . import maya, arnold, nuke

NET_TOOLS_PREF = '//loco000/tools'

class Engine(object):
    @staticmethod
    def run(path, args=None, communicate=True):
        try:
            if os.path.exists(path):
                #print(path)
                #print(os.environ)
                if args is None:
                    args = []
                elif type(args) == str:
                    args = args.split()
                #print(path, '--', args)
                app = Popen([path] + args, env=os.environ)

                if communicate:
                    app.communicate()
                return 0
            else:
                print('Engine not found! Search path: ' + path)
                return 1

        except Exception as exception:
            print("Error while trying to run engine: " + str(exception))
            return 1


class OsExtension(object):
    @staticmethod
    def is_linux():
        if _platform == "linux" or _platform == "linux2":
            return True
        return False

    @staticmethod
    def is_windows():
        return not OsExtension.is_linux()

class OSPathAdapter(object):
    """
    Класс преобразования путей в linux/windows
    """
    path = ''
    ext = ''
    #sep = '/'
    os_t = 'linux'
    is_win = False

    def __init__(self, path):
        """
        :param os_type: тип операционной системы 
        :param path: путь для преобразования
        """
        if 'win' in _platform:
            #self.sep = '\\'
            self.os_t = 'windows'
            self.is_win = True
            self.ext = '.exe'
        self.path = self.normalize_path(path)

    def normalize_path(self, path):
        """
        Преобразование слэшей в путях для OS
        """
        if type(path) == list:
            _path = path
        elif type(path) == str:
            if '/' in path:
                _path = path.split('/')
            elif '\\' in path:
                _path = path.split('\\')
        if '\\' in _path[0] or '/' in _path[0]:
            return _path[self.is_win]
        else:
            return os.sep.join(_path)

    def get_cmd_line(self, cmd):
        """
        Получение пути в соответствие с операционной системой
            cmd: список с двумя путями
        """
        return cmd[self.is_win]

    def get_path(self, os_flag=False):
        """
        :param platform: добавление названия платформы в конце пути к каталогу 
        :return: 
        """
        if type(self.path) == list and len(self.path) == 2:
            self.path = self.path[self.is_win]
        if os_flag:
            if type(self.path) == list:
                self.path.append(self.os_t)
            else:
                self.path += os.sep + self.os_t
        return self.path

    def get_file(self, fname, pref=None):
        if pref:
            fname = pref + os.sep + fname
        return fname + self.ext

nuke_name, nuke_exe = 'Nuke11.1v1', 'Nuke11.1'

class PiplineTools(object):

    started_apps = {'maya': maya.Maya,
                    'arnold': arnold.Arnold,
                    'nuke': nuke.Nuke,
                    'yeti': 'yeti'}
    lmodes = ('basic', 'stand', 'extend', 'serv')
    cfg = None
    is_win = 'win' in sys.platform
    lnch_mode = lmodes[0]
    soft_tools_path = '%s/soft4tools' % NET_TOOLS_PREF

    def __init__(self, opts=None): #project=None, app = None):
        msg = u"Не найден файл конфигурации!"
        if opts:
            if type(opts) != str:
                """ Если параметры импортированиы из модуля приложения """
                self.storage, self.project, apl_name = opts.PROJ_STORAGE, opts.PROJ_NAME, opts.APL_NAME
                self.cfg_path = '/'.join([self.storage, self.project, '.settings/config.yml'])
                if not os.path.exists(self.cfg_path):
                    self.cfg_path = self.cfg_path.replace('yml', 'json')
                try:
                    if not self.cfg_path:
                        self.monit(u'Недоступен файл конфигурации. Возможно, отсутствует подключение к хранилищу!')
                    import yaml
                    cfg = yaml.load(open(self.cfg_path))
                    self.title = cfg['PROJ_TITLE']
                    os.environ['APP_ENVIRON'] = json.dumps(cfg)
                except:
                    self.monit(u'Не заданы основные параметры запуска!')
            else:
                self.lnch_mode = self.lmodes[-1]
                apl_name = opts
                title = 'undefined'
                ae = os.getenv('APP_ENVIRON')
                if ae:
                    app_environ = json.loads(ae)
                    self.storage = app_environ['PROJ_STORAGE']
                    self.title = app_environ['PROJ_TITLE']
                    self.project = app_environ['PROJ_NAME']
                #self.monit('tools_path  serv: %s' % os.getenv('TOOLS_PATH', '_'))

            try:
                self.app = self.started_apps[apl_name](opts=opts)
                if self.app:
                    self.app.init_environ(OSPathAdapter(self.app.dirs_path))
                #if self.app.ext is None:
                #    self.app.ext = '.exe' if self.is_win else ''
            except Exception as e:
                self.monit(u'Приложение невозможно запустить! Ошибка: %s' % e)
                return

        # Если переменная установлена, значиь приложение запущено из консоли (вручную)
        self.tools_path = os.getenv('TOOLS_PATH', '')
        self.loc_tools_path = 'Y:'

        if not self.tools_path:
            self.tools_path = os.path.abspath(os.path.dirname(__file__) + '/..')
            try:
                if self.tools_path: # and self.project_id:
                    msg = u'Не корректное содержимое файла конфигурации!'
                    self.tools_path = self.tools_path.replace('\\', '/') #.replace('Y:', NET_TOOLS_PREF)
                    self.monit(u'Установка пути к инструментам: %s' % self.tools_path)

                    EnvVar('PROJECT_PATH')\
                      .appendPath([os.path.realpath(os.path.dirname(self.cfg_path) + '/..')])  # self.get_project_location()]))
            except Exception as e:
                msg= u' >! Ошибка !<: %s, info: %s' % (msg, e)
                if opts:
                    self.monit(msg)
                return

        #self.tools_path = '//loco000/tools/toolsMulty/StudioTools'
        pp = os.getenv('PROJECT_PATH')
        drive_path = self.tools_path.split(':')
        if len(drive_path) == 2 and drive_path[0] == 'Y':
            EnvVar('TOOLS_PATH').appendPath([NET_TOOLS_PREF + drive_path[1]])
        else:
            EnvVar('TOOLS_PATH').appendPath([self.tools_path])

        try:
            info = u'>>>  Приложение %s: Открытие проекта `%s` ... ' % \
                   (self.app.name, self.title)
            self.monit(info)
        except:
            self.monit(info.decode('utf-8'))

        self.init_environ()
        self.engine = Engine()

    def monit(self, info):
        """Отображение текущей операции"""
        mode = 'Info'.ljust(15, ' ')
        line_out = ("[ %s <|> %s " % (datetime.now().strftime('%H:%M:%S'), mode) + "]: %s \n" % info.rjust(150, " "))
        try:
            sys.stdout.write(line_out.encode('utf-8'))
        except:
            sys.stdout.write(line_out)

    def setup_yeti(self):
        #if OsExtension.is_windows() else '%s/arnold/Yeti/v2.2.1/maya/2018/linux' % self.soft_tools_path]))
        #Y:\soft4tools\arnold\Yeti\v2.2.1\maya\2018\linux\scripts

        self.monit('>>> Setting up yeti plugin.')
        #YETI_LOCATION = '//loco000/tools/toolsMulty/StudioTools/arnold/ext/Yeti/v2.2.1/windows' if OsExtension.is_windows() else '/loco000/tools/toolsMulty/StudioTools/arnold/ext/Yeti/v2.2.1/linux'
        ###os.environ['YETI_LOCATION'] = "//loco000/tools/soft4tools/arnold/Yeti/v2.2.1/maya/2018/" + self.app.ospa.os_t
        #r"Y:\toolsMultyV0.2\StudioTools\maya\ext\2018\linux\modules\yeti\v2.2.1\modules"
        ##    '/'.join([self.soft_tools_path, 'arnold', 'Yeti', 'v2.2.1', 'maya', self.app.version, self.app.ospa.os_t])
        #'%s/arnold/Yeti/v2.2.1/maya/%s/%s' % \
        ##YETI_LOCATION = "%s/arnold/Yeti/v2.2.1/maya/%s/%s" % (self.soft_tools_path, self.app.version, self.app.ospa.os_t)
        #os.environ['' \
        YETI_LOCATION = os.getenv('YETI_LOCATION')

        ##YETI_LOCATION = '//loco000/tools/toolsMulty/StudioTools/arnold/ext/Yeti/v2.2.1/windows' if OsExtension.is_windows() else '/loco000/tools/toolsMulty/StudioTools/arnold/ext/Yeti/v2.2.1/linux'
        ##mdp = os.getenv('MAYA_MODULE_PATH', '')
        ##if not mdp:
        ##    os.environ['MAYA_MODULE_PATH'] = '%s/bin/plug-ins' % self.app.dir_path  #'//loco000/tools/toolsMultyV0.2/StudioTools/maya/ext/bin/plug-ins'
        ##os.environ['MAYA_MODULE_PATH'] += os.pathsep + YETI_LOCATION + os.pathsep + YETI_LOCATION + '/bin'
        ##os.environ['PATH'] = os.pathsep + '%s/bin' % YETI_LOCATION + os.pathsep + '%s/plug-ins' % YETI_LOCATION + os.pathsep + os.getenv('PATH')

        # Для Пингвинов, чтобы видел .so библиотеки
        #YETI_LOCATION = '/loco000/tools/soft4tools/arnold/mtoadeploy/2018/linux/bin'
        ##os.environ["LD_LIBRARY_PATH"] = '%s/bin' % YETI_LOCATION + os.pathsep + os.getenv('LD_LIBRARY_PATH', '')
        ##os.environ['YETI_TMP']='//omega/moriki-doriki/temp/yeti_cache'
        MTOA_PATH = self.get_mtoa_path()
        EnvVar("LD_LIBRARY_PATH", False)\
            .appendPath(['%s/bin' % YETI_LOCATION])
            #.appendPath(['%s/lib' % self.app.dir_path])\
            #.appendPath(['%s/bin' % self.app.dir_path])\
            #.appendPath(['/usr:/lib64:/usr/lib64:/usr/local/lib64'])\
            #.appendPath(['/lib:/usr/local/lib'])
        """
        .appendPath(['%s/bin' % MTOA_PATH]) \
        .appendPath(['%s/plug-ins' % MTOA_PATH]) \
        .appendPath(['%s/plugins' % MTOA_PATH]) \
        .appendPath(['%s/procedurals' % MTOA_PATH]) \
        .appendPath(['%s/shaders' % MTOA_PATH]) \
        .appendPath(['%s/lib' % MTOA_PATH])
        """

        EnvVar("YETI_TMP").appendPath(['%s/temp/yeti_cache' % os.getenv('PROJECT_PATH')])
        EnvVar("PRVZ_RLM_SERVER").setValue("5053@10.77.64.17")
        os.environ['MTOA_EXTENSIONS_PATH'] = os.pathsep + '%s/plug-ins' % YETI_LOCATION + os.pathsep + os.getenv('MTOA_EXTENSIONS_PATH', '')
        os.environ['MTOA_PROCEDURALS'] = os.pathsep + '%s/bin' % YETI_LOCATION + os.pathsep + os.getenv('MTOA_PROCEDURAL_PATH', '')
        ##os.environ['ARNOLD_PLUGIN_PATH'] = os.pathsep + '%s/extensions' % MTOA_PATH+ os.pathsep + os.getenv('ARNOLD_PLUGIN_PATH', '')
        ##os.environ['ARNOLD_PLUGIN_PATH'] = os.pathsep + '%s/bin' % MTOA_PATH + os.pathsep + os.getenv('ARNOLD_PLUGIN_PATH', '')
        ##os.environ['PRVZ_RLM_SERVER'] = '5053@10.77.64.17'
        os.environ['peregrinel_LICENSE'] = os.getenv('PRVZ_RLM_SERVER')

    def init_environ(self):
        app_tool_path = self.app.tools_path()#.replace('Y:', NET_TOOLS_PREF)

        if self.lnch_mode == 'basic':
            EnvVar('CGRU_LOCATION').appendPath([self.get_cgru_path()])
            EnvVar('MAYA_CGRU_LOCATION').appendPath([self.get_cgru_path(), 'plugins', 'maya'])
            EnvVar('CGRU_PYTHON').appendPath([self.get_cgru_path(), 'lib', 'python'])
            EnvVar('APP_TOOL_PATH').appendPath([self.app.tools_path()])
            EnvVar('SOFT4TOOLS').appendPath([self.soft_tools_path])
            EnvVar('MTOA_PATH').appendPath(['%(SOFT4TOOLS)s', 'arnold', 'mtoadeploy', self.app.version, self.app.ospa.os_t])
            EnvVar('XBMLANGPATH')\
                .appendPath(['%(SOFT4TOOLS)s', 'arnold', 'mtoadeploy', self.app.version, self.app.ospa.os_t, 'icons'])
                ##.appendPath([mtoa_path, 'arnold', 'mtoadeploy', self.app.version, self.app.ospa.os_t, 'icons'])\

            ##EnvVar('PYTHONPATH').appendVar('TOOLS_PATH')
            EnvVar('NUKE_DISK_CACHE').appendPath([self.tools_path])
            ##EnvVar('RLM_LICENSE').setValue('5053@10.77.64.17')
            ##EnvVar('YETI_TMP').appendPath(['%s/temp/yeti_cache' % os.getenv('PROJECT_PATH')])

        if self.app.name == 'maya':
            info = u'Подключение окружения Maya ...'.rjust(152)
            self.monit(info)

            mpp = json.loads(os.getenv('APP_ENVIRON'))['MAYA']['ENVIRON'][0] #self.app.ospa.is_win]
            app_env = {
                'APP_VERSION': self.app.version,
                'OS_TYPE': self.app.ospa.os_t,
                'SOFT4TOOLS': self.soft_tools_path
            }

            mtoa_path = mpp['MTOA_PATH'] % app_env
            #mtoa_path = self.get_mtoa_path()  # r'Y:\toolsMultyV0.2\StudioTools\arnold\ext\mtoadeploy\2018'
            YETI_LOCATION = os.environ['YETI_LOCATION'] = self.get_yeti_path()
            self.setup_yeti()
            #"%s/arnold/Yeti/v2.2.1/maya/%s/%s" % (self.soft_tools_path, self.app.version , self.app.ospa.os_t)
            #YETI_LOCATION = os.getenv('YETI_LOCATION')
            #EnvVar('MAYA_MODULE_PATH').appendPath(['%s/bin/plug-ins' % self.app.dir_path])
            # + os.pathsep + YETI_LOCATION

            USD_ROOT_DIR = '%s/pixar/usd/%s'  % (self.soft_tools_path, self.app.ospa.os_t)
            EnvVar('USD_ROOT_DIR').appendPath([USD_ROOT_DIR])
            USD_PLUG_IN_PATH = USD_ROOT_DIR + '/third_party/maya/plugin'
            USD_LIB_PATH     = USD_ROOT_DIR + '/lib/python'
            USD_SCRIPTS_PATH = USD_PLUG_IN_PATH + '/../lib/usd/usdMaya/resources'

            if self.lnch_mode == 'basic':
                info = u'Подключение плагина mtoa в Maya'.rjust(152)
                self.monit(info) #.decode('utf-8')) #.decode('cp1251'))
                try:
                    mpp_paths = mpp['PLUG_IN_PATH']
                    app_env.update({
                        'APP_TOOLS_PATH': app_tool_path,
                        'APP_DIR_PATH': self.app.dir_path,
                        'YETI_PATH': self.get_yeti_path(),
                        'YETI_VERSION': 'v2.2.1',
                        'MTOA_PATH': mtoa_path,
                        'USD_ROOT_DIR': USD_ROOT_DIR,
                        'USD_LIB_PATH': USD_LIB_PATH,
                        'USD_PLUGIN_PATH': USD_PLUG_IN_PATH
                        #'USD_ROOT_PATH': USD_INSTALL_ROOT % (self.soft_tools_path, self.app.ospa.os_t),
                        #'USD_LIB_PATH': USD_LIB_PATH % (self.soft_tools_path, self.app.ospa.os_t),
                        #'USD_PLUGIN_PATH': USD_PLUG_IN_PATH % (self.soft_tools_path, self.app.ospa.os_t)
                    })
                    ##EnvVar('MTOA_PATH', False).setValue([mpp['MTOA_PATH'] % app_env])
                    map(lambda x: EnvVar('MAYA_PLUG_IN_PATH', False).appendPath([x % app_env]), mpp_paths)
                    ##EnvVar('MAYA_PLUG_IN_PATH', False).appendPath([self.app.dir_path + '/bin'])
                    ##EnvVar('MAYA_PLUG_IN_PATH', False).appendPath([self.app.dir_path + '/plugins'])
                    ##EnvVar('MAYA_PLUG_IN_PATH', False).appendPath([mtoa_path + '/plugins'])
                    ##EnvVar('MAYA_PLUG_IN_PATH', False).appendPath([mtoa_path + '/plugins'])
                    ##EnvVar('PXR_PLUGINPATH_NAME', False).appendPath([USD_PLUG_IN_PATH, 'share', 'usd', 'plugins'])
                    #EnvVar('MAYA_PLUG_IN_PATH', False).appendPath([USD_PLUG_IN_PATH])
                    res = 'Ok'
                except:
                    res = 'Error'

            if self.lnch_mode in ['basic', 'serv']:
                EnvVar('MAYA_APP_DIR').appendPath([self.get_pref_dir_path('maya')])
                info = u'Установка путей к модулям в MAYA_MODULE_PATH'.rjust(152)
                self.monit(info)
                try:
                    ##print(os.getenv('MAYA_MODULE_PATH'))
                    mpp_modpth = mpp['MODULE_PATH']
                    map(lambda x: EnvVar('MAYA_MODULE_PATH', False).appendPath([x % app_env]), mpp_modpth)
                    ##EnvVar('MAYA_MODULE_PATH', False).appendPath(['%s/bin/plug-ins' % self.app.tools_path()])
                    ##EnvVar('MAYA_MODULE_PATH', False).appendPath(['%s/plugins' % mtoa_path])
                    ##EnvVar('MAYA_MODULE_PATH', False).appendPath(['%s/plug-ins' % mtoa_path])
                    ##EnvVar('MAYA_MODULE_PATH', False).appendPath([USD_PLUG_IN_PATH + '/../lib'])
                    res = 'Ok'
                except:
                    res = 'Error'

                #?EnvVar("LD_LIBRARY_PATH")\
                #?    .appendPath(['%s/bin' % mtoa_path])
                #    #?.appendPath(['%s/lib' % self.app.dir_path])
                #    #?.appendPath(['%s/bin' % YETI_LOCATION])\
                #    #?.appendPath(['%s/plug-ins' % YETI_LOCATION])
                #    #?.appendPath(['%s/plug-ins' % mtoa_path])\
                #    #?.appendPath(['%s/plugins' % mtoa_path])\
                #    #.appendPath(['%s/arnold/Yeti/v2.2.1/maya/%s/%s/bin' % (self.soft_tools_path, self.app.version, self.app.ospa.os_t)])
                #    #.appendPath(['%s/arnold/mtoadeploy/%s/%s/plugins' % (self.soft_tools_path, self.app.version, self.app.ospa.os_t)])

                EnvVar('TEMP').appendPath([self.get_temp_dir_path('maya')])
                EnvVar('MAYA_PRESET_PATH').appendPath([mtoa_path + '/presets'])
                EnvVar('ARNOLD_PLUGIN_PATH', False).appendPath(["${FDBIN}/plugin"])
                EnvVar('ARNOLD_PLUGIN_PATH', False).appendPath(['%s/shaders' % mtoa_path])
                EnvVar('MTOA_EXTENSIONS_PATH').appendPath([mtoa_path + '/extensions'])
                EnvVar('MTOA_PROCEDURALS').appendPath([mtoa_path + '/procedurals'])
                os.environ['SYNCOLOR'] = str('%s/synColor/synColorConfig.xml' % self.tools_path)  #).encode('utf-8').decode('cp1251')
                #print(self.get_temp_dir_path('maya'))
                os.environ['MAYA_DISABLE_CIP'] = '1'
                #os.environ['DEPT_NAME'] = 'render'

                #EnvVar('SYNCOLOR').appendPath(['%s/synColor/synColorConfig.xml' % self.tools_path])
                #EnvVar('SYNCOLOR').appendPath(['%s/synColor' % self.tools_path])
                #EnvVar('SYNCOLOR').appendPath(['%s/synColor/Shared' % self.tools_path])

                if self.lnch_mode != 'serv':
                    self.monit(u'Подключение скриптов Maya'.rjust(152))
                    try:
                        mpp_scripts = mpp['SCRIPT_PATH']
                        map(lambda x: EnvVar('MAYA_SCRIPT_PATH', False).appendPath([x % app_env]), mpp_scripts)
                        # EnvVar('MAYA_SCRIPT_PATH', False).appendPath([mtoa_path + '/scripts'])
                        ##EnvVar('MAYA_SCRIPT_PATH', False).appendPath([app_tool_path + '/scripts'])
                        ##?EnvVar('MAYA_SCRIPT_PATH', False).appendPath([USD_SCRIPTS_PATH % (self.soft_tools_path, self.app.ospa.os_t)])
                        res = 'Ok'
                    except:
                        res = 'Error'

                    self.monit(u'Статус: %s.' % res)
                    self.monit('*' * 15)

                info = u'Подключение настроек рендера `Arnold` ... '.rjust(102)
                self.monit(info)
                try:
                    EnvVar('MAYA_RENDER_DESC_PATH').appendPath([mpp['RENDER_DESC_PATH'] % app_env])  # .replace('\\', '/')])
                    res = 'Ok'
                except:
                    res = 'Error'
                self.monit(u'Статус: %s.' % res)
                self.monit('*' * 15)
                ##EnvVar('MTOA_PATH', False).appendPath([mtoa_path])
                ##EnvVar('XBMLANGPATH', False).appendPath(['%s/icons' % mtoa_path])
                ##EnvVar('MAYA_RENDER_DESC_PATH', False).appendPath([mtoa_path])

                EnvVar('MAYA_LOCATION').appendPath([self.app.dir_path])
                info = u'Установка путей к модулям в PATH'.rjust(122)
                self.monit(info)
                try:
                    ##print(os.getenv('MAYA_MODULE_PATH'))
                    mpp_pth = mpp['PATH']
                    map(lambda x: EnvVar('PATH', False).appendPath([x % app_env]), mpp_pth)
                    #//loco000/tools/soft4tools/arnold/mtoadeploy/2018/windows/
                    EnvVar('PATH')\
                        .appendVar('MTOA_EXTENSIONS_PATH')\
                        .appendVar('MTOA_PROCEDURALS')\
                        .appendVar('ARNOLD_PLUGIN_PATH')\
                        .appendPath(['%(TOOLS_PATH)s', 'synColor', 'Shared'])\
                        .appendPath([self.app.dir_path, self.app.version, 'modules'])\
                        .appendPath([self.app.dir_path, 'modules'])
                        ##.appendPath([USD_ROOT_DIR, 'lib'])\
                        ##.appendPath([USD_ROOT_DIR, 'bin'])\
                        ##.appendPath([USD_LIB_PATH, '..'])
                    #EnvVar('MAYA_MODULE_PATH', False).appendPath([USD_PLUG_IN_PATH % (self.soft_tools_path, self.app.ospa.os_t) + '/../lib'])
                    #    .appendPath(['%s/bin' % YETI_LOCATION]) \
                    #    .appendPath(['%s/lib' % self.app.dir_path]) \
                    #    .appendPath(['%s/bin' % self.app.dir_path]) \
                    #    .appendPath(['/usr:/lib64:/usr/lib64:/usr/local/lib64']) \
                    res = 'Ok'
                except:
                    res = 'Error'

            if self.lnch_mode == 'basic':
                info = u'Установка путей PYTHONPATH'.rjust(122)
                self.monit(info)
                try:
                    mpp_pypth = mpp['PYTHONPATH']
                    #+ ['//parovoz-frm01/Shotgun/utils', '//parovoz-frm01/Shotgun/utils2', 'C:/Python27_x32/Lib/site-packages/shotgun_api3-3.0.32-py2.7.egg']
                    app_env.update({
                        'CGRU_PATH': os.getenv('CGRU_LOCATION')
                    })
                    map(lambda x: EnvVar('PYTHONPATH', False).
                        appendPath([x % app_env]),mpp_pypth)
                    ##EnvVar('PYTHONPATH', False).appendPath([app_tool_path, 'python'])
                    EnvVar('PYTHONPATH', False).appendPath([self.get_cgru_path(), 'lib'])
                    EnvVar('PYTHONPATH', False).appendPath([self.get_cgru_path(), 'lib', 'python'])
                    EnvVar('PYTHONPATH', False).appendPath([app_tool_path, 'bin'])

                    EnvVar('PYTHONPATH', False).appendPath([self.app.dir_path, 'maya', self.app.version, 'scripts'])
                    EnvVar('PYTHONPATH', False).appendPath([self.app.dir_path, 'maya', 'scripts'])
                    #?EnvVar('PYTHONPATH', False).appendPath(['%s/scripts/startup' % self.app.dir_path])
                    #?EnvVar('PYTHONPATH', False).appendPath([app_tool_path, 'bin', 'python27.zip'])
                    ##?EnvVar('PYTHONPATH', False).appendPath([USD_INSTALL_ROOT % self.app.ospa.os_t + '/lib'])
                    ##?EnvVar('PYTHONPATH', False).appendPath([USD_INSTALL_ROOT % self.app.ospa.os_t + '/bin'])
                    ##?EnvVar('PYTHONPATH', False).appendPath([USD_PLUG_IN_PATH % (self.soft_tools_path, self.app.ospa.os_t) + '/../lib'])
                    res = 'Ok'
                except:
                    res = 'Error'

                self.monit(u'Статус: %s.' % res)
                self.monit('*' * 15)

            if self.lnch_mode == 'stand':
                EnvVar('PRVZ_SHOTGUN_SCRIPTS').appendPath([self.tools_path, 'shotgun', 'scripts'])

        EnvVar('PATH')\
            .appendVar('CGRU_PYTHON')\
            .appendVar('CGRU_LOCATION')\
            .appendPath([self.app.dir_path, 'bin'])\
            .appendPath([mtoa_path, 'bin'])
        ##    .appendPath([USD_ROOT_DIR, 'lib'])\
        ##    .appendPath([USD_ROOT_DIR, 'bin'])\
        ##    .appendPath([USD_LIB_PATH, '..'])\
        #?.appendPath([mtoa_path, 'shaders'])\
        #?.appendPath([mtoa_path, 'procedurals'])\
        #?.appendPath([mtoa_path, 'extensions'])\
        #?.appendPath([self.get_cgru_path(), 'bin'])
        #?.appendPath([self.app.dir_path, 'bin'])\
        #?.appendVar('ARNOLD_PLUGIN_PATH')\
        #?.appendVar('MTOA_EXTENSIONS_PATH')\
        #?.appendVar('MTOA_PROCEDURALS')\
        #.appendPath([self.app.dir_path, 'lib']) \
        #.appendVar('MAYA_PLUG_IN_PATH')\
        #.appendVar('CGRU_PYTHON') \
        #.appendVar('CGRU_LOCATION')
        #.appendVar('MAYA_MODULE_PATH') \
        #EnvVar('PATH', False).appendPath(['%s/bin' % mtoa_path])
        ##EnvVar('PATH', False).appendPath(['//loco000/tools/soft4tools/arnold/mtoadeploy/2018/windows/bin'])
        ##EnvVar('PATH', False).appendPath(['//loco000/tools/soft4tools/arnold/mtoadeploy/2018/linux/bin'])

        #if self.lnch_mode == 'basic' or self.lnch_mode == 'serv':
        #EnvVar('PATH')\
                ##.appendPath([mtoa_path + '/plug-ins'])\
                ##.appendPath([mtoa_path + '/plugins'])\
                ##.appendPath([USD_LIB_PATH % self.app.ospa.os_t])\
                ##.appendPath([USD_INSTALL_ROOT % self.app.ospa.os_t + '/lib'])\
                ##.appendPath([USD_INSTALL_ROOT % self.app.ospa.os_t + '/bin'])\
                ##.appendPath([USD_PLUG_IN_PATH % self.app.ospa.os_t + '/../lib'])
                ##.appendPath(["//loco000/tools/soft4tools/arnold/mtoadeploy/2018/%s/bin" % self.app.ospa.os_t])\
                ##.appendPath(["//loco000/tools/soft4tools/pixar/usd/%s/third_party/maya/plugin" % self.app.ospa.os_t])
                ##.appendPath(["//loco000/tools/soft4tools/pixar/usd/linux/third_party/maya/plugin"])

        ##self.monit('-----Settings done!-----\n')
        ##EnvVar('PATH', False).appendPath([USD_LIB_PATH % self.app.ospa.os_t])
        ##EnvVar('PATH', False).appendPath([USD_INSTALL_ROOT % self.app.ospa.os_t + '/lib'])
        ##EnvVar('PATH', False).appendPath([USD_INSTALL_ROOT % self.app.ospa.os_t + '/bin'])
        ##EnvVar('PATH', False).appendPath([USD_PLUG_IN_PATH % self.app.ospa.os_t + '/../lib'])

    def get_win_tool_path(self):
        if self.tools_path[1] == ':':
            return self.tools_path.replace(self.loc_tools_path, NET_TOOLS_PREF)
        else:
            return self.tools_path

    def _get_path(self, path):
        if type(path) == list:
            _path = '/'.join(path)
        else:
            _path = path
        if os.path.exists(_path): return _path

    def get_project_location(self):
        _path = [self.storage, self.project]
        return self._get_path(_path)
        #return ('\\\\omega\\' if OsExtension.is_windows() else '//omega/') + self.project

    def get_prod_location(self, project=None):
        if not project:
            project = self.project
        _path = [self.storage, project, json.loads(os.getenv('APP_ENVIRON')).get('PROD')]
        return self._get_path(_path)

    def get_temp_dir_path(self, engine):

        path = os.path.join('C:\\', '_temp', engine) if OsExtension.is_windows() else '/var/tmp/maya'
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except Exception as e:
                print('can\'t create temp directory for {0}\nError:{1}'.format(engine, e))
        return path

    def get_pref_dir_path(self, engine):
        path = os.path.join('C:\\', '_prefs', engine) if OsExtension.is_windows() else '/var/tmp/maya'
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except Exception as e:
                print('can\'t create temp directory for {0}\nError:{1}'.format(engine, e))
        return path

    def get_shotgun_path(self):
        return '/'.join([self.tools_path, 'shotgun'])

    def get_cgru_path(self):
        return '/'.join([self.tools_path, 'cgru'])
        #, 'cgru-windows' if OsExtension.is_windows() else 'cgru-linux')

    def get_maya_tools_path(self):
        return '/'.join([self.tools_path, 'maya', 'ext'])

    def get_arnold_tools_path(self):
        #self.tools_path = '//loco000/tools/toolsMulty/StudioTools'
        return '/'.join([self.tools_path, 'arnold', 'ext'])

    def get_mtoa_path(self, maya_version='2018'):
        if not maya_version:
            maya_version = self.app.version
        return '/'.join([self.soft_tools_path, 'arnold', 'mtoadeploy', maya_version, self.app.ospa.os_t])

    def get_yeti_path(self, maya_version='2018'):
        if not maya_version:
            maya_version = self.app.version
        return '/'.join([self.soft_tools_path, 'arnold', 'Yeti', 'v2.2.1', 'maya', self.app.version, self.app.ospa.os_t])

    def get_maya_path(self, maya_version='2018'):
        if OsExtension.is_windows():
            return os.path.join('C:\Program Files\Autodesk\Maya' + maya_version)
        else:
            return os.path.join('/usr/autodesk/maya2018')

    def get_nuke_dir_path(self):
        if OsExtension.is_windows():
            return os.path.join('C:\\', 'Program Files', nuke_name)
        else:
            return os.path.join('/usr/local', nuke_name)

    def get_nuke_exe_path(self):
        if OsExtension.is_windows():
            return os.path.join('C:\\', 'Program Files', nuke_name, nuke_exe)
        else:
            return os.path.join('/usr/local', nuke_name, nuke_exe)


class EnvVar(object):
    """description of class"""

    def __init__(self, varName, clear=True):
        self.varName = varName

        # todo: check correct work for ass generation(possible problems with Python)
        if clear and (varName is not 'path' and varName is not 'PATH'):
            os.environ[self.varName] = ''

    def appendPath(self, *args):

        try:
            path = os.path.join(*args[0])
            if type(path) == unicode:
                path = path.encode('utf-8')
            else:
                path = str(path)

            mode = 'Checking'.ljust(15, ' ')
            if ';' in path or ':' in path:
                if os.pathsep == ';':
                    splited_paths = path.split(';')
                else:
                    path = path.replace('Y:', NET_TOOLS_PREF)
                    splited_paths = path.split(':')
                for s_path in splited_paths:
                    if not os.path.exists(s_path):
                        sys.stdout.write("[ %s <|> %s ]: Path: '%s' " % (datetime.now().strftime('%H:%M:%S'), mode, s_path.ljust(160, '.')) + " : Error!\n")
                        continue
            elif not os.path.exists(path):
                return
                sys.stdout.write("[ %s <|> %s ]: Path: '%s' " % (datetime.now().strftime('%H:%M:%S'), mode, path.ljust(160, '.')) + " : Error!\n")

            if self.varName in os.environ:
                # lst = os.environ[self.varName][-1:]
                if len(os.environ[self.varName]) > 0:
                    os.environ[self.varName] = path + os.pathsep + os.environ[self.varName]
                else:
                    os.environ[self.varName] = path
            else:
                os.environ[self.varName] = path

            if path in os.environ[self.varName]:
                mode = 'Setup'.ljust(15, ' ')
                if os.pathsep in path:
                    path_ = path.split(os.pathsep)
                    if len(path_) > 1:
                        for ech_p in path_:
                            #if type(ech_p) == unicode:
                            #    ech_p = ech_p.encode('utf-8')
                            #else:
                            #    ech_p = str(ech_p)
                            #print(path.index(ech_p))
                            var_title = ' ' * 10
                            if path.index(ech_p) == 0:
                                var_title = 'Variable: %s =' % self.varName
                            sys.stdout.write(("[ %s <|> %s ]: " + var_title + " %s " % (datetime.now().strftime('%H:%M:%S'), mode, ech_p)).ljust(160, '.') + " : Installed.\n")
                            #else:
                            #    sys.stdout.write(("[ %s <|> %s ]: %s " % (datetime.now().strftime('%H:%M:%S'), mode, ech_p)).ljust(144, '.') + " : Installed.\n")
                else:
                    sys.stdout.write(("[ %s <|> %s ]: Variable: %s = %s " % \
                                      (datetime.now().strftime('%H:%M:%S'), mode, self.varName, path)).ljust(160, '.') + " : Installed.\n")

            if len(args) > 1 and args[1] and not os.path.exists(path):
                try:
                    os.makedirs(path)
                except Exception as exception:
                    print('Error while creating directory: ' + path + ' for var: ' + self.varName + os.linesep + str(exception))

        except Exception as exception:
            print('Error while appending path to environment variable: ' + self.varName + os.linesep + str(exception))

        return self

    def appendLinuxPath(self, *args):
        if OsExtension.is_linux():
            self.appendPath(*args)
        return self

    def appendWindowsPath(self, *args):
        if not OsExtension.is_linux():
            self.appendPath(*args)
        return self

    def appendVar(self, varName):
        self.appendPath([os.environ[varName]])
        return self

    def append_group(self, arg):
        if type(arg) == list:
            for ech_pth in arg:
                if type(arg) == list:
                    self.appendPath([ech_pth])
                else:
                    self.appendVar(ech_pth)

    def setValue(self, value):
        os.environ[self.varName] = value
        return self

    def __str__(self):
        return self.varName + ': ' + os.environ[self.varName] + os.linesep
