﻿# coding: utf-8
import sys, os, json
from os.path import join
if not __file__ is None:
    fname = __file__.replace('\\', '/')
    TOOLS_PATH = os.path.dirname(fname)
else:
    TOOLS_PATH = os.path.realpath('.')
#TOOLS_PATH = TOOLS_PATH.replace('Y:', '//loco000/tools')
utils_paths = [TOOLS_PATH,
               '%s/cgru/lib/site-packages' % TOOLS_PATH]
#               '%s/cgru/Python27/Lib/site-packages' % TOOLS_PATH,
#               '%s/cgru/Python27/Lib/site-packages/yaml' % TOOLS_PATH]
#list(map(lambda x: join(os.getcwd(), x), ['..', 'helper', 'maya', 'nuke', 'arnold']))

#for PREF_PATH in [['//parovoz-frm01', 'Shotgun']]:
#    utils_paths += list(map(lambda x: join(*(PREF_PATH + [x])), ['', 'utils', 'utils2']))

for utils_path in utils_paths:
    if utils_path not in sys.path:
        sys.path.append(utils_path)

import yaml
os.environ['MAYA_RENDER_DESC_PATH'] = '%s/maya/ext/2018/rendererDesc' % TOOLS_PATH

#sys.path.append('//parovoz-frm01/Shotgun/utils')
#os.environ['PYTHONPATH'] = os.getcwd() + ';' + os.getenv('PYTHONPATH', '')
#print(sys.path)
#sys.path.append(r'C:\Python27_x32\Lib\site-packages\shotgun_api3-3.0.32-py2.7.egg')

#try:
from helper.EnvManagerV2 import *
#except:
#    from EnvManagerV2 import *

def _info(info, mode):
    try:
        print(info)
    except:
        if mode is None:
            print(info.encode('utf-8').decode('cp1251'))

def main():

    #file_name, prms = os.path.dirname(sys.argv[0]), \
    apl_env = None
    prms = sys.argv[1:]
    console_args = {
        '--apl_name': 'maya',
        '--project1': 'bears',
        '--version': '2018',
        '--options': '',
        '--mode': None
    }
    options_lst = []
    for prm in prms:
        if 'options' in prm:
            options_lst = prms[prms.index(prm):]
            break
        k, v = prm.split('=')
        if k != prm: # and k[:2] == '--':
            console_args[k] = v

    _options = ' '.join(options_lst).split('=')
    if len(_options) == 2:
        console_args[_options[0]] = _options[1]

    apl_name = console_args.get('--apl_name')
    project = console_args.get('--project')
    version = console_args.get('--version')
    options = console_args.get('--options')
    mode = console_args.get('--mode', None)
    storage = None

    _opts = {'APPLIC': apl_name,
             'PROJECT': project,
             'VERSION': version,
             'OPTIONS': options,
             'MODE': mode}

    if apl_name or project:
        # если задан хоть один из параметров
        #cfg_fp = '_%s.yml' % os.path.basename(__file__).split('.')[0]
        cfg_fp = '%s/_%s.yml' % (TOOLS_PATH, os.path.basename(__file__)[:-3])
        try:
            _info(u'Загрузка файла конфигурации %s ...' % cfg_fp, mode)
            #msg = u'Загрузка файла конфигурации %s ...' % cfg_fp
            #if mode:
            #    print(msg.encode('utf-8').decode('cp1251'))
            #else:
            #    print(msg)

            msg = u'Ошибка загрузки стартовых настроек приложения!'
            if os.path.exists(cfg_fp):
                cfg = yaml.load(open(cfg_fp))

            # читаем переменные для приложения
            apl_env = __import__(apl_name)
            #apl_env.PROJ_ID = cfg.get('PROJ_ID')
            apl_env.APL_NAME = apl_name
            apl_env.PROJ_STORAGE = cfg.get('PROJ_STORAGE')
            apl_env.PROJ_NAME = cfg.get('PROJ_NAME')
            apl_env.MODE = mode

            if apl_env.PROJ_STORAGE:
                if apl_env.APP_TOOLS_PATH:
                    os.environ['APPLIC'] = apl_name
                if version:
                    #if apl_name == 'maya':
                    apl_env.VERSION = version #apl_env.get('VERSION')
                    #else:
                    #    apl_env.NUKE_VERSION = version
                    #apl_env.APP_VERSION = apl_ver

                apl_env.PROJ_TITLE = cfg.get('PROJ_TITLE', '')
                if project:
                    apl_env.PROJ_NAME = project # cfg.get('PROJECT', '')
                #apl_env.ST_PROJECT_STORAGE = cfg.get('STORAGE', '')

            msg = u'Запуск приложения "%s%s" ...' % (apl_name, version)
        except:
            msg += u'\n    Формат запуска приложения:' + \
                   u'   [cmd_line]: %s --project="project_name" [--apl_name="apl_name"] [--version="version"]\n' % fname

    _info(msg, mode)
    if not apl_env: return
    apl_dir = os.path.dirname(cfg_fp) + '/' + apl_name
    os.chdir(apl_dir)
    if os.path.basename(apl_dir) != apl_name:
        _info(u"Ошибка загрузки настроек", mode) #.encode('utf-8').decode('cp1251'))
        return

    pipline = PiplineTools(opts=apl_env)
    #project=project, app=apl_name)
    if mode:
        app_cmd_line = pipline.app.runCmdLine(mode=mode, options=options)
    else:
        app_cmd_line = pipline.app.runCmdLine()

    if app_cmd_line[0]:
        _info('[ STARTING ]: %s\nOptions: > %s <\n' % (app_cmd_line[0], options), mode)
        pipline.engine.run(app_cmd_line[0], args=options, communicate=True)

if __name__ == '__main__':
    sys.exit(main())