import os

PROJ_ID = 168
PROJ_NAME = 'moriki-doriki'
PROJ_STORAGE = r'\\omega'
APP_DIRS_PATH = ['/usr/local/Nuke%s/Nuke%s', 'C:\\Program Files\\Nuke%s\\Nuke%s']
APP_TOOLS_PATH = os.path.dirname(__file__)
VERSION = '11'
#MAYA_EXT_PATH = 'ext'
#MAYA_MODULES_PATH = '%s/%s/modules'
#MAYA_PLUGINS_PATH = '%s/%s/plugins'
#MAYA_PRESETS_PATH = '%s/%s/presets'
#MAYA_SELECTOR_PATH = '%s/%s/selector'

#from . import apl
