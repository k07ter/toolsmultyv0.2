import getpass
import os
import os.path

try:
    from helper.EnvManagerV2 import *
except:
    import sys
    sys.path.append(os.path.abspath(os.path.join(os.path.realpath('.'), "..")))
    from helper.EnvManagerV2 import *


def main():
    print("start nuke env configuration")

    print('Current python:' + str(sys.version_info) + 'env: ')

    print(':'.join(x for x in sys.path if x))

    pipline = PiplineTools()

    orig_tool_path = pipline.tools_path

    if OsExtension.is_windows():
        pipline.tools_path = pipline.get_win_tool_path()  # Nuke can't use loco00 :(

    cgru_path = pipline.get_cgru_path()

    nuke_dir_path = pipline.get_nuke_dir_path()

    nuke_exe_path = pipline.get_nuke_exe_path()

    nuke_temp_path = pipline.get_temp_dir_path('nuke')

    nuke_pref_path = pipline.get_pref_dir_path('nuke')

    print(EnvVar('NUKE_TOOLS')
          .appendPath([pipline.tools_path, 'nuke', 'ext']))

    print(EnvVar('OFX_PLUGIN_PATH')
          #.appendPath([os.environ['NUKE_TOOLS'], 'plugins']))
          ##.appendPath(['Y:\\toolsBears\\StudioTools\\nuke\\ext', 'plugins'])
          .appendPath(['%s\\nuke\\ext\\plugins' % pipline.tools_path])
          .appendPath(['%s\\nuke\\plugins' % pipline.soft_tools_path])
          )

    print(EnvVar('FOUNDRY_LICENSE_FILE')
          .appendPath(['//pc999/lic/ekun/license/yrdnuof.lic']))

    #print(EnvVar('NUKE_TEMP_DIR')
    #      .appendPath([nuke_temp_path], True))

    #print(EnvVar('NUKE_PREFS_DIR')
    #      .appendPath([nuke_pref_path], True))

    print(EnvVar('HOME')
          .appendPath([nuke_pref_path, '.users', getpass.getuser(), '.profile']))

    print(EnvVar('AF_ROOT')
          .appendPath([cgru_path, 'afanasy']))

    print(EnvVar('AF_PYTHON')
          .appendPath([cgru_path, 'afanasy', 'python']))

    print(EnvVar('CGRU_PYTHONEXE')
          .setValue('python'))

    print(EnvVar('CGRU_PYTHON')
          .appendPath([cgru_path, 'lib', 'python']))

    print(EnvVar('CGRU_PYTHONDIR')
          .appendPath([cgru_path, 'lib', 'python']))

    print(EnvVar('CGRU_LOCATION')
          .appendPath([cgru_path]))

    print(EnvVar('NUKE_CGRU_PATH')
          .appendPath([cgru_path, 'plugins', 'nuke']))

    print(EnvVar('NUKE_AF_RENDER')
          .setValue('python ' + orig_tool_path.replace("\\", "/") + '/nuke/nukeRender.py  -i'))

    print(EnvVar('LM_LICENSE_FILE')
          .appendPath([nuke_dir_path, 'foundry.lic']))

    print(EnvVar('NUKE_PATH')
          .appendVar('NUKE_CGRU_PATH')
          .appendVar('NUKE_TOOLS'))

    print(EnvVar('PYTHON')
          .appendPath([cgru_path, 'python']))

    print(EnvVar('PYTHONPATH')
          .appendPath([cgru_path, 'afanasy', 'python'])
          .appendPath([os.environ['CGRU_PYTHON']]))

    print(EnvVar('PATH')
          .appendVar('CGRU_PYTHON')
          .appendVar('CGRU_LOCATION')
          .appendVar('NUKE_TOOLS')
          .appendVar('PYTHON')
          .appendPath([cgru_path, 'bin'])
          .appendPath([cgru_path, 'afanasy', 'bin'])
          .appendPath([nuke_dir_path, 'lib'])
          .appendPath([cgru_path, 'software_setup', 'bin']))

    print(EnvVar('CGRU_VERSION').setValue('2.0.5'))

    print(EnvVar('CGRU_DAILIES_FORMAT')
          .setValue('1920x1080'))
    print(EnvVar('CGRU_DAILIES_FPS')
          .setValue('25'))
    print(EnvVar('CGRU_DAILIES_CODEC')
          .appendPath([os.environ['CGRU_LOCATION'], 'utilities', 'moviemaker', 'codecs', 'h264rgb_high.ffmpeg']))
    print(EnvVar('CGRU_DAILIES_NAMING')
          .setValue('(s)'))
    print(EnvVar('CGRU_DAILIES_SLATE')
          .setValue(''))
    print(EnvVar('CGRU_DAILIES_TEMPLATE')
          .appendPath([os.environ['CGRU_LOCATION'], 'utilities', 'moviemaker', 'templates', 'parovoz_frame_template']))
    print(EnvVar('CGRU_DAILIES_LGSPATH')
          .setValue(' '))
    print(EnvVar('CGRU_DAILIES_LGFPATH')
          .appendPath([orig_tool_path, 'logo', 'parovoz_logo.png']))
    print(EnvVar('CGRU_DAILIES_LGFGRAV')
          .setValue('NorthEast'))
    print(EnvVar('CGRU_DAILIES_LGFSIZE')
          .setValue('0'))

    print(EnvVar('CGRU_LOCATION')
          .appendPath([pipline.get_cgru_path()]))

    print("finish nuke env configuration")

    # render task:
    if len(sys.argv) > 1 and OsExtension.is_linux():
        print(EnvVar('DISPLAY').setValue(':0'))
        # fix path to render.py
        sys.argv[3] = os.path.join(pipline.get_cgru_path(), 'plugins', 'nuke', 'render.py')
        # fix path to *.nk
        sys.argv[8] = str(sys.argv[8]).replace('\\', '/')

    print("run nuke: {0}".format(str(sys.argv[1:])))

    return Engine.run(nuke_exe_path, sys.argv[1:] + ['-nukex'], communicate=False)


if __name__ == "__main__":
    #try:
    sys.exit(main())
    #except Exception as e:
    #    import traceback
    #    error = traceback.format_exc()
    #    print(error.upper())
