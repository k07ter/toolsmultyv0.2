# Parovoz Tools by Denis Alymov
# Edited Nikita Gulyaev
import readFromWrite

# Menu
menubar=nuke.menu("Nuke")
menu=menubar.addMenu("PAROVOZ")

menu.addCommand("Clean unused channels",
                "import cleanChannels;reload(cleanChannels);cleanChannels.remove()")
menu.addCommand("Mask Id Group",
                "nuke.createNode('MaskIdGroup')",
                'F8')
menu.addCommand("Position to Mask",
                "nuke.createNode('Position_to_Mask_Cubic')",
                'F9')
menu.addCommand("DespillMadness",
                "nuke.createNode('DespillMadness')")
menu.addCommand("Relight use normals",
                "nuke.createNode('delightful')")

rfw = nuke.menu('Nuke')
rfw.addCommand('Read from Write',
               'readFromWrite.ReadFromWrite()',
               'shift+r')

# Toolbar Nodes
toolbar=nuke.toolbar("Nodes")

# Toolbar Draw
draw_bar = nuke.toolbar("Draw")
draw_bar.addCommand("Bezier",
                   "nuke.tcl('Bezier;BezierGeoTab')",
                   "p",
                   icon="Bezier.png")

nuke.addOnUserCreate(lambda:nuke.thisNode()['first_frame'].setValue(nuke.frame()), nodeClass='FrameHold')
