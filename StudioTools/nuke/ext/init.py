print('Starting Nuke add-ons initialization')

import os, sys
import scripts

nuke_tool_path = os.environ['NUKE_TOOLS']

print('Nuke add-ons root: ' + nuke_tool_path)

nuke.pluginAddPath(nuke_tool_path + '/scripts')
nuke.pluginAddPath(nuke_tool_path + '/metaProject')
nuke.pluginAddPath(nuke_tool_path + '/icons')
nuke.pluginAddPath(nuke_tool_path + '/gizmos')
#nuke.pluginAddPath(nuke_tool_path + '/gizmos2')

sys.path.append(nuke_tool_path + '/scripts')
sys.path.append(nuke_tool_path + '/metaProject')

nuke.knobDefault("black_clamp", "false")
nuke.knobDefault("_jpeg_quality", "1")

import projectSettings as settings

settings.scanProjects()

print('Nuke add-ons initialization finished')

try:
    import _metaProject
    reload(_metaProject)
except Exception as ex:
    print('EX: import _metaProject: %s' % ex)

