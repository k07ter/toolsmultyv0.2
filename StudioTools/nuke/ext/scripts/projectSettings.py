#! /usr/bin/env python
# emacs-mode: -*- python-*-
# -*- coding: utf-8 -*-

# source script wrote by Timur Khodzhaev

import os
import re
import nuke
import nukescripts

def scanProjects():
    projScanPath = ['//omega/moriki-doriki']
    projPath = os.getenv('PROJECT_PATH')
    if projPath:
        projPath = projPath.replace('\\', '/')
        for dr in projScanPath:
            if os.path.exists(dr):
                if os.path.isdir(dr):
                    s = dr
                    #					 for s in shows:
                    showPath = os.path.join(dr, s, '.settings/.nuke')
                    showPath = showPath.replace('\\', '/')
                    showName = s
                    if os.path.isdir(showPath):
                        print('scanProjects: Adding plugins path :' + showPath)
                        nuke.pluginAddPath(showPath)
                        prefFilePath = (showPath + '/.prefs')
                        if os.path.isfile(prefFilePath):
                            prefFile = open(prefFilePath, 'r')
                            prefContent = prefFile.readlines()
                            prefFile.close()
                            favP = 0
                            resP = 0
                            for line in prefContent:
                                if re.match('<<Favorites.*', line):
                                    favP = 1
                                elif re.match('Favorites>>.*', line):
                                    favP = 0
                                elif re.match('<<Formats.*', line):
                                    resP = 1
                                elif re.match('Formats>>.*', line):
                                    resP = 0
                                if (favP == 1):
                                    if re.match('Scripts.*', line):
                                        favList = line.split(':')[1].strip().split(',')
                                        for i in favList:
                                            i = i.strip().replace('\\', '/')
                                            favShowPath = ((s + '/') + i)
                                            favShowName = ((s + '/') + i)
                                            print('scanProjects: Adding favorite path :' + favShowPath)
                                            if os.path.isdir(favShowPath):
                                                nuke.addFavoriteDir(favShowName, favShowPath, nuke.SCRIPT)

                                    if re.match('Images.*', line):
                                        favList = line.split(':')[1].strip().split(',')
                                        for i in favList:
                                            i = i.strip().replace('\\', '/')
                                            favShowPath = ((s + '/') + i)
                                            favShowName = ((s + '/') + i)
                                            print('scanProjects: Adding favorite path :' + favShowPath)
                                            if os.path.isdir(favShowPath):
                                                nuke.addFavoriteDir(favShowName, favShowPath, nuke.IMAGE)

                                elif (resP == 1):
                                    if (not re.match('<<Formats.*', line)):
                                        fmtList = line.split('\n')
                                        for n in fmtList:
                                            n = n.strip()
                                            if (not (n == '')):
                                                fmtName = n.split(' ')[-1]
                                                oldFormats = nuke.formats()
                                                fmtExist = 1
                                                for fmt in oldFormats:
                                                    if (fmt.name() == fmtName):
                                                        fmtExist = 0

                                                if fmtExist:
                                                    print('scanProjects: Format found : ' + n)
                                                    nuke.addFormat(str(n))

    else:
        print(
            'Enviroment variable <PROJECT_PATH> for scaning projects folders not found. Refer to Documentation section. Init scripts.')
