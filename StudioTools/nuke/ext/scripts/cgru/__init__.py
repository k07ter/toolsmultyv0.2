import sys, os
sys.path.append(os.environ['TOOLS_PATH'])

from dailies import *
from docs import *
from pathcurdir import *
from pathmap import *
from rules import *
from afanasy import *
