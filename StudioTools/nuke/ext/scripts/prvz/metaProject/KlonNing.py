

import os,string,re
import nuke
import nukescripts
def klon_sc():
    nuke.scriptSave()

    ss = nuke.getInput('shots klonnig').split(',')
    shots = []
    if ss :
        for s in ss:
            shots.append('sh'+string.zfill( s,4 ))

    scr_name = nuke.scriptName()
    this_sh = scr_name .split('/')[-1].split('_')[1]
    nods_R = nuke.allNodes('Read')
    nods_C = nuke.allNodes('Camera2')    

    for sh in shots:
        F_R_kl = file_read_kl(scr_name,sh)
        #reads
        for r in nods_R :
            r['disable'].setValue(1)
            r_key = r['file'].value().split('/')[-2]
            
            if r_key =='illustr':
                if len(F_R_kl[1])>0:
                    shh = r['file'].value().split('/')[-1].split('_')[1]
                    f_file = r['file'].value().replace(shh,sh)
                    r['file'].setValue(f_file)
                    r['disable'].setValue(0)
            else:
                try:
                    r_file = F_R_kl[0][1][r_key]
                    list_exr=[]
                    list_exr = os.listdir(os.path.dirname(r_file))
                    f_max = max(list_exr).split('.')[1]
                    f_min = min(list_exr).split('.')[1]
        
                    r['file'].setValue(r_file)
                    r['first'].setValue(int(f_min))
                    r['last'].setValue(int(f_max))
                    r['origfirst'].setValue(int(f_min))
                    r['origlast'].setValue(int(f_max))
                    r['disable'].setValue(0)

                except: q=1
        nuke.root()['first_frame'].setValue(int(f_min))
        nuke.root()['last_frame'].setValue(int(f_max))
            
        #cam
        for c in nods_C :
            c_file = F_R_kl[3]
            c['file'].setValue(c_file)
            c['name'].setValue( os.path.basename(c_file).split('.')[0] )

        #saveAs
        nuke.scriptSaveAs(scr_name.replace(this_sh,sh)[:-7]+'v000.nk')



#if re.match('.*'+ r_key +'.*', ','.join(F_R_kl[0][0]) ):

def file_read_kl(scr_name,sh):    
    ep = scr_name.split('/')[-1].split('_')[0:2][0]
    sh = sh
    proj = scr_name.split('/')[3]#'envell'#'leo'
    Epizod = '//alpha/'+ proj +'/3_post/' +ep
    
    
    #########################
    #  max(V)read  ,  posledovatelnost ['bg','mg','membran','chars','fg','glass']
    #_beauty = {'chars_layer': '//alpha/envell/3_post/ep009/sh0307/in/v001/chars_layer',...>}
    # BB_key=['bg_layer', 'mg_layer', 'fg_layer', 'chars_layer', 'vik_glass_layer', 'kira_layer'] #
    beauty = {}
    fold = [f for f in os.listdir(Epizod +'/'+ sh +'/in/') if re.match('.*'+'v'+'.*',f)]
    for ff in fold:
        for vv in os.listdir(Epizod +'/'+ sh +'/in/'+ff+'/'):
            beauty[vv] = Epizod +'/'+ sh +'/in/'+ff+'/'+vv+'/'+ep+'_'+sh+'_render_'+ff+'.%04d.exr' 
    k = ['bg','mg','shadow','membran','chars','glass','shield','fg']
    b_key = beauty.keys()
    b_key.sort()
    BB_key =[]
    for i in k :
        for f in b_key :
            if re.match('.*'+i+'.*',f):
                b_key.remove(f)
                BB_key.append(f)
    BB_key.extend(b_key)

    #########################
    #  illustr  ['bg','mid','fg']
    # illustr = ['//alpha/envell/3_post/ep009/sh0307/in/illustr/ep009_sh0307_bg_01_illustr_v001.png',...]
    illustr = []
    pfx = ['bg','mid','fg']
    f_ill = Epizod +'/'+ sh +'/in/illustr/'
    try:
        illustr = [f_ill+ill for p in pfx for ill in os.listdir(f_ill) if re.match('.*'+p+'.*',ill) ]
    except: q=1    
    #########################
    #  cam 
    camFiles = Epizod +'/'+ sh +'/'+ 'data/abc'
    c = [i for i in filter( lambda i: i.endswith('.abc') , os.listdir( camFiles ) ) if re.match( '.*' + 'camera' + '.*' , i )]
    ccc = camFiles +'/'+ max(c)




    return [ BB_key , beauty ] , illustr , [ proj , ep , sh ] , ccc
