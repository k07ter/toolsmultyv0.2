import sys, os

sys.path.append('scripts')

import parovozNukeMenu
parovozNukeMenu.buildNukeMenu()
parovozNukeMenu.buildPropertiesMenu()

import disconnect_wiggle


# Toolbar Nodes
toolbar=nuke.toolbar("Nodes")

# Toolbar Draw
toolbar=nuke.toolbar("Draw")
toolbar.addCommand("Bezier", "nuke.tcl('Bezier;BezierGeoTab')", "p", icon="Bezier.png")

#Baha tools
import browseSelected
tb = nuke.toolbar( 'Nodes' )
ParMenu = tb.addMenu( 'Parovoz', 'Parovoz.png' )
ParMenu.addCommand( 'other_mask', 'nuke.createNode( "other_mask" )')
ParMenu.addCommand( 'XYZ Key_01', 'nuke.createNode( "xyzKey_v01" )'  )
ParMenu.addCommand( 'bg_cards', 'nuke.createNode( "bg_cards" )'  )
ParMenu.addCommand( 'fog_mask', 'nuke.createNode( "fog_mask" )'  )
ParMenu.addCommand( 'Pass_contr', 'nuke.createNode( "Pass_contr" )' )
ParMenu.addCommand( 'Pass_Denoise', 'nuke.createNode( "Pass_Denoise" )' )
ParMenu.addCommand( 'Light Rays/BahRay', 'nuke.createNode( "BahRay_v01" )')
ParMenu.addCommand( 'Light Rays/BahRay_Fog', 'nuke.createNode( "BahRay_Fog" )')
ParMenu.addSeparator()
ParMenu.addCommand( 'Open Explorer for Selected', 'browseSelected.browseSelected()', 'ctrl+e' )

# Baha tools
try:
    import animatedSnap3D
    animatedSnap3D.run()
except Exception as ex:
    print('EX: import animatedSnap3D: %s' % ex)

# Stereocamera tools:
import s3d_import_stereoCamera
import s3d_import_stereoCamera_LR
s3d_menu = tb.addMenu( 'S3D', 's3d.png' )
s3d_menu.addCommand('Import stereo camera', 's3d_import_stereoCamera.s3d_import_stereoCamera()')
s3d_menu.addCommand('Import stereo camera LR', 's3d_import_stereoCamera_LR.s3d_import_stereoCamera_LR()')
