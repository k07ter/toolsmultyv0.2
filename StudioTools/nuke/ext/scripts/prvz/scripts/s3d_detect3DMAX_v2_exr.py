import nuke
import os.path
import re

#$maxNegativeParallax = $imageHResolution * $focal * $interaxial/($nearParallaxPlane*$hAperture_mm);
#offset = -1*(abs($nearPlaneParallax_pix) - $maxNegativeParallax)
#if zeroParallax<0 then {filmTranslateH} = $offset/$imageHResolution

#1 detect FX exr
#2 scan all readers for *_layer_* and add them to {layersList}
#3 ask user to select which layer HIT should be apply to this FX reader and apply it

def collectRenderLayers():
    # we can put some filtering here for adding specific only readers
    layersList = {}
    for reader in nuke.allNodes('Read'):
        fullFilePath = reader['file'].getValue()
        if '_layer_' in fullFilePath:
            # we assume that layername is identical to the folderName in which exr sequence is stored.
            dirName = os.path.basename(os.path.dirname(fullFilePath))
            if '_layer' in dirName : 
                layerName = dirName.replace('_layer','')
                disabled = reader['disable'].getValue()
                if disabled != 1 and layerName not in layersList.keys():
                    layersList[layerName] = reader
    return layersList

def s3d_inject_HIT_to_metadata():
    import pickle
    import os.path
    import nuke

    filePath = nuke.getFilename('Get File Contents', '*.s3d')
    print(filePath)

    s3dFILE = open(filePath, 'rb')
    S3D_settings = pickle.load(s3dFILE)
    s3dFILE.close()

    #print('loaded hit: '+str(S3D_settings['HIT']))
    print('loading HIT: ok')
    camName = os.path.basename(filePath).replace('_s3Data.s3d','')	# leave alone cameraname only


    modifyMeta = nuke.createNode('ModifyMetaData')

    #==== create custom knob with HIT animation, to assign it to metadata by frame basis then.
    hitknob = nuke.Double_Knob("hit_store",'HIT')
    modifyMeta.addKnob(hitknob)
    modifyMeta['hit_store'].setAnimated()
    for key,value in S3D_settings['HIT'].items():
        modifyMeta['hit_store'].setValueAt(value,key)
    #===

    newdata = '{set '+ camName +'_HIT "[value knob.hit_store]"}'
    modifyMeta["metadata"].fromScript(newdata)

def getHITsList(node):
    # return list with metadata keys that contain HIT data .
    mdat = node.metadata()
    hitList = []
    for key in mdat.keys() :
        if '_HIT' in key: hitList.append(key)
    return hitList

def s3d_apply_HIT():
    '''
    add HIT data to Metadata stream

    '''
    selectedHIT = getHITsList(nuke.selectedNode())[0]

    if 'left' and 'right' in nuke.views():
        hit_T=nuke.createNode('Transform')
        hit_T.setName('s3d_HIT_transform')

        hit_T['translate'].splitView('right')
        hit_T['translate'].setExpression("-[metadata {0}]".format(selectedHIT),0)
        hit_T['translate'].setExpression("[metadata {0}]".format(selectedHIT),0,view = 'right')
    else: nuke.message('ERRORrrrTrrtrrrrr!! : there is no Left and Right views in nuke.views()')

def __detect3DMAX_exr():
	# we turned it off right now...
	pass
	
def detect3DMAX_exr():
    #this function is called "detect3DMAX_exr" because it start as 3dMax only.. but then the same techinque was spreaded to mantra exr's from houdini
    readNode=nuke.thisNode()
    knob = nuke.thisKnob()
    message = ''
    if readNode.Class() in ['Read','DeepRead'] and knob.name()=='file':
        filename = readNode['file'].value()
        fileExt = os.path.splitext(filename)[1]
        range_in = readNode['first'].getValue()
        metadataKeys = readNode.metadata().keys()
        if metadataKeys == []:
            print('whoops metadata is empty on this step, something wrong in script')
        if fileExt.lower() != '.exr':
            pass
        elif 'nuke/version' in metadataKeys : 
            print('S3D_MAX detect-----> PASS')
            print(filename)
            pass
        # for detecting 3Dmax exr files we search "exr/cameraFocalLength" key in exr metadata. Becouse 3Dmax have't that field, but Maya have.
        elif 'exr/cameraFocalLength' not in metadataKeys : 
            message = 'BobbyMcFerrin: \n{0}:\n 3DMAX exr image detected.\n Do you want apply automatic stereoscopic correction for that sequence?'.format(filename)
        elif 'exr/capDate' in metadataKeys: 
            message = 'BobbyMcFerrin: \n{0}:\n Houdini exr image detected.\n Do you want apply automatic stereoscopic correction for that sequence?'.format(filename)
        elif 'BobbyMcFerrin' in message:
            ask = nuke.ask(message)
            if ask ==1:
                layersList = collectRenderLayers()
                for layerName,reader in layersList.items():
                    print(layerName+' : '+ reader.name())
                #s3d_inject_HIT_to_metadata()
                #s3d_apply_HIT()
    else: pass

nuke.addKnobChanged(detect3DMAX_exr, nodeClass='Read')
nuke.addKnobChanged(detect3DMAX_exr, nodeClass='DeepRead')

#nuke.removeKnobChanged(detect3DMAX_exr, nodeClass='Read')