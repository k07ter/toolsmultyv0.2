#===============================================================================
# clean unused channels and save as clean file
# usage:
'''
import cleanChannels
cleanChannels.remove()

'''
# Code writer E. Melkov (Parovoz studio,  Moscow 2015)
#===============================================================================

import nuke 
import re

removed = 0

def incrementVersion(path):
	rex = re.compile('.*_v(?P<version>\d+)\.[a-zA-Z]+')
	try:
		version = re.match(rex, path).groupdict()['version']
	except Exception as err:
		print ("ERROR: Check file name!")
		return
	dig_number = len (version)
	version = '%s' % (int(version) + 1) #incremental
	new_version = '0'*(dig_number - len(version)) +  version #string
	new_path = re.sub('_v\d+', '_v%s' % new_version, path)
	#print new_path
	return new_path
	



def filtered(layer, data):
	global removed
	rex_ch =re.compile('\w+\.\w+')
	rex_layer_name = re.compile('.*add_layer {(?P<name>\w+)\s.*')
	layer_name = re.match(rex_layer_name, layer).groupdict()['name']

	# all channels in layer:
	all_channels_in_layer = rex_ch.findall(layer)
	res = 'add_layer {%s'% layer_name
	
	#print '------------',layer_name, '----------------------'
	
	for ch in all_channels_in_layer:
		# rex to find current channel in data:
		rex_current_ch =re.compile('[^\w]' + ch + '[^\w]')
		channels_in_data = rex_current_ch.findall(data)
		
		# rex to find current layer names in data:
		rex_layer_name_search = re.compile('({|\s)'+ layer_name +'\s')
		layers_in_data = rex_layer_name_search.findall(data)
		
		if len(channels_in_data)>1 or (len(layers_in_data)>1):
			#print len (channels_in_data), channels_in_data
			res+=' %s' % ch
		else:
			removed+=1
			print ('... Looks like unused trash: %s ( # %s )' % (ch, removed))
			
	res+='}'
	return res 
	
def remove():
	nuke.scriptSave()
	rex_start_block = re.compile('.*define_window_layout_xml.*</layout>\n*}', flags=re.S) 
	rex_layer = re.compile('add_layer \{.*\}')
	#fname = '//Alpha/crafts/3_post/ep777/sh777/comp/ep051_sh048_comp_v002.nk'
	fname = nuke.root().name()
	#out_fname = fname.replace('.nk', '_clean.nk')
	out_fname = incrementVersion(fname)
	data = ''
	with open(fname, 'r') as myfile: data=myfile.read()
	layers_obj = rex_layer.findall(data)
	
	#replace:
	for layer in layers_obj:
		#filtered(layer, data)
		filtered_layer = filtered(layer, data)
		print '>>> Clean layer:\n', filtered_layer
		newdata = data.replace(layer, filtered_layer)
		data = newdata
	#print res
	
	out = open(out_fname, "w")
	out.write(newdata)
	out.close()
	print ('RESULT: ==== %s channels have been removed.\nRESULT: === Saved to "%s".' % (removed, out_fname))



