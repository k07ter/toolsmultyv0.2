import nukescripts
import nuke

'''
This script dosome stuff
'''

import os
import os.path
import re


extList = ['.psd','.png']

def collectReadersForBBox():
    # we can put some filtering here for adding specific only readers
    readersToBBox = []
    for reader in nuke.selectedNodes('Read'):
        range_in = reader['first'].getValue()
        range_out = reader['last'].getValue()
        disabled = reader['disable'].getValue()
        if range_out - range_in == 0 and disabled != 1:
            #collect only one frame length images
            readersToBBox.append(reader)
    return readersToBBox


def assignBBox():
    
    readersToBBox = collectReadersForBBox()
    exr_names = []
    toolsToDelete = []
    for reader in readersToBBox:
        '''
        i['postage_stamp'].setValue(0)
        file = i['file'].getValue()
        write = nuke.nodes.Write()
        write = nuke.nodes.Write( channels = 'all', file_type = 'exr', autocrop = True )
        write['file'].setValue(file)
        write.setInput(0,i)'''
        
        readerPath = reader['file'].getValue()
        reader_fileDir = os.path.split(readerPath)[0]
        reader_filename,extension = os.path.splitext(readerPath)
        extension = extension.replace('.','_')
        #exr_folder = reader_filename + extension + '_EXR_layers'
        exr_folder = reader_fileDir + '/' + 'withBBox'
        if not os.path.exists(exr_folder):
            os.makedirs(exr_folder)
        exr_name = os.path.split(readerPath)[1]
        if exr_name in exr_names:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!! this name already in the list!: \n {0}'.format(exr_name))
        exr_names.append(exr_name)
        exr_filePath = exr_folder + '/' + exr_name
        
        print(exr_filePath)
        
        saverEXR = nuke.nodes.Write( channels = 'all', file_type = 'exr', autocrop = True,beforeFrameRender = 'import nuke, nukescripts;nukescripts.cache_clear("")' )
        saverEXR['file'].setValue(exr_filePath)
        toolsToDelete.append(saverEXR)
        saverEXR.setInput(0,reader)
        
        #comp.Render(True, renderIn, renderOut, 1)
        nuke.execute(saverEXR,1,1)
        #print("mememememe")
        #print(saverEXR.name())
        nuke.delete(saverEXR)

        reader['file'].setValue(exr_filePath)
        
    
#switchPassThrough(allSavers,True)

#assignBBox()

