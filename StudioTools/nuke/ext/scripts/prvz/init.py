# -*- coding: utf-8 -*-
print 'STUDIO: Starting CGRU Nuke add-ons...'

import os, sys
import nuke

print(os.getenv('NUKE_CGRU_PATH') + '\\scripts')
sys.path.append(os.getenv('NUKE_CGRU_PATH') + '\\scripts')
sys.path.append(os.getenv('NUKE_CGRU_PATH') + '\\gizmos')

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\scripts' )
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\gizmos' )
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\metaProject' )
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '\\animatedSnap3D-master' )

nuke.pluginAddPath( os.getenv('NUKE_CGRU_PATH') + '\\scripts' )
nuke.pluginAddPath( os.getenv('NUKE_CGRU_PATH') + '\\gizmos' )
nuke.pluginAddPath( os.getenv('NUKE_CGRU_PATH') + '\\metaProject' )

nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\scripts' )
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\gizmos' )

# Baha tools
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\icon' )
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\scripts' )
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\metaProject' )
nuke.pluginAddPath( os.path.dirname(os.path.abspath(__file__)) + '\\animatedSnap3D-master' )
try:
    import _metaProject
    reload(_metaProject)
except Exception as ex:

    print('EX: import _metaProject: %s' % ex)

#some scripts from cgru
sys.path.append(os.environ['CGRU_LOCATION'] + '/afanasy/python' )

#print(sys.path)

nuke.knobDefault("black_clamp", "false")
nuke.knobDefault("_jpeg_quality", "1")
nuke.knobDefault( 'Shuffle.label', '[value in]' )
nuke.knobDefault( 'Shuffle.note_font', 'Verdana Bold' )
nuke.knobDefault( 'ShuffleCopy.label', '[value in]\n[value out]' )
nuke.knobDefault( 'ShuffleCopy.note_font', 'Verdana Bold' )



#import prvz_dailies
import projectSettings as ps

if not 'PRVZ_PROJECT_PATH' in os.environ.keys():
    os.environ['PRVZ_PROJECT_PATH'] = os.environ['PRVZ_PROJECT_PATH'] + '/projects_ini/projects_settings/.default'
    print ('WARNING: Set to default project : PRVZ_PROJECT_PATH = %s' % os.environ['PRVZ_PROJECT_PATH'])

try:
    ps.init()
except Exception as err:
    print ('%s: Exception: %s' % (__file__, err))