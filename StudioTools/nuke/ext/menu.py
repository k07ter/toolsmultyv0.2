# Parovoz Tools by Denis Alymov
# Edited Nikita Gulyaev
# Menu
menubar=nuke.menu("Nuke")

menu=menubar.addMenu("PAROVOZ")

menu.addCommand("Clean unused channels", "import cleanChannels;reload(cleanChannels);cleanChannels.remove()")
menu.addCommand("Mask Id Group", "nuke.createNode('MaskIdGroup')", 'F8')
menu.addCommand("Position to Mask", "nuke.createNode('Position_to_Mask_Cubic')", 'F9')
menu.addCommand("DespillMadness", "nuke.createNode('DespillMadness')")
menu.addCommand("Relight use normals", "nuke.createNode('delightful')")

# Toolbar Nodes
toolbar=nuke.toolbar("Nodes")

# Toolbar Draw
toolbar=nuke.toolbar("Draw")
toolbar.addCommand("Bezier", "nuke.tcl('Bezier;BezierGeoTab')", "p", icon="Bezier.png")


import readFromWrite
nuke.menu('Nuke').addCommand('Read from Write',
                             'readFromWrite.ReadFromWrite()',
                             'shift+r')
							 


nuke.addOnUserCreate(lambda:nuke.thisNode()['first_frame'].setValue(nuke.frame()), nodeClass='FrameHold')