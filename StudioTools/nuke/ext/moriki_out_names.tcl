proc moriki_out_names {} {
	set node [selected_node]
	if {[class $node] == "Write"} {
		regexp "(ep\[0-9\]+)" [file rootname [file tail [value root.name]]] episode_name
		#regexp "(sc\[0-9\]+)" [file rootname [file tail [value root.name]]] scene_name
		regexp "(sh\[0-9\]*\[a-z\]*)" [file rootname [file tail [value root.name]]] shot_name
		set shot_vers [version_get [file tail [value root.name]] "v"]
		
		set d_path [getenv('PROJECT_PATH')]
		regsub -all "\\\\" $d_path "/" d_path

		knob $node.file //omega/moriki-doriki/3_post/$episode_name/$shot_name/out/v$shot_vers/$episode_name\_$shot_name\_comp_v$shot_vers.%05d.png
		knob $node.colorspace sRGB
		knob $node.file_type png	
	}
}