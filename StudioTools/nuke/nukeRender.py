import getpass
import os
import os.path
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from helper.EnvManager import *


def main():

    print("start nukeRender env configuration")

    print('Current python:' + str(sys.version_info) + 'env: ')

    print(':'.join(x for x in sys.path if x))

    pipline = PiplineTools()

    orig_tool_path = pipline.tools_path

    if OsExtension.is_windows():
        pipline.tools_path = 'Y:\\toolsBears\\'  # Nuke can't use loco00 :(

    tools_path = pipline.tools_path

    cgru_path = pipline.get_cgru_path()

    nuke_dir_path = pipline.get_nuke_dir_path()

    nuke_exe_path = pipline.get_nuke_exe_path()

    nuke_temp_path = pipline.get_temp_dir_path('nuke')

    nuke_pref_path = pipline.get_pref_dir_path('nuke')

    print(EnvVar('NUKE_TOOLS')
          .appendPath([pipline.tools_path, 'nuke', 'ext']))

    print(EnvVar('OFX_PLUGIN_PATH')
          .appendPath([os.environ['NUKE_TOOLS'], 'plugins']))

    print(EnvVar('FOUNDRY_LICENSE_FILE')
          .appendPath('//pc999/lic/ekun/license/yrdnuof.lic'))

    print(EnvVar('NUKE_TEMP_DIR')
          .appendPath([nuke_temp_path], True))

    print(EnvVar('NUKE_PREFS_DIR')
          .appendPath([nuke_pref_path], True))

    print(EnvVar('HOME')
          .appendPath([nuke_pref_path, '.users', getpass.getuser(), '.profile']))

    print(EnvVar('AF_ROOT')
          .appendPath([cgru_path, 'afanasy']))

    print(EnvVar('AF_PYTHON')
          .appendPath([cgru_path, 'afanasy', 'python']))

    print(EnvVar('CGRU_PYTHONEXE')
          .setValue('python'))

    print(EnvVar('CGRU_PYTHON')
          .appendPath([cgru_path, 'lib', 'python']))

    print(EnvVar('CGRU_LOCATION')
          .appendPath([cgru_path]))

    print(EnvVar('NUKE_CGRU_PATH')
          .appendPath([cgru_path, 'plugins', 'nuke']))

    print(EnvVar('NUKE_DIR')
          .appendPath([nuke_dir_path]))

    print(EnvVar('NUKE_EXEC')
          .appendPath([nuke_exe_path]))

    print(EnvVar('APP_DIR')
          .appendVar('NUKE_DIR'))

    print(EnvVar('APP_EXE')
          .appendVar('NUKE_EXEC'))

    print(EnvVar('NUKE_AF_RENDER')
          .setValue('python ' + orig_tool_path.replace("\\", "/") + '/nukeRender.py -i'))

    print(EnvVar('LM_LICENSE_FILE')
          .appendPath([nuke_dir_path, 'foundry.lic']))

    print(EnvVar('NUKE_PATH')
          .appendVar('NUKE_CGRU_PATH')
          .appendVar('NUKE_TOOLS'))

    print(EnvVar('PYTHON')
          .appendPath([cgru_path, 'python']))

    if OsExtension.is_windows():
        print(EnvVar('PYTHONHOME'))

    print(EnvVar('PYTHONPATH')
          .appendPath([cgru_path, 'afanasy', 'python'])
          .appendPath([os.environ['CGRU_PYTHON']]))

    print(EnvVar('PATH')
          .setValue('')
          .appendWindowsPath('C:\\Program Files (x86)\\QuickTime\\QTSystem\\')
          .appendVar('CGRU_PYTHON')
          .appendVar('CGRU_LOCATION')
          .appendVar('NUKE_TOOLS')
          .appendPath([cgru_path, 'bin'])
          .appendPath([cgru_path, 'afanasy', 'bin'])
          .appendPath([cgru_path, 'software_setup', 'bin']))

    # configuration for DAILIES

    print(EnvVar('CGRU_DAILIES_FORMAT')
          .setValue('1920x1080'))
    print(EnvVar('CGRU_DAILIES_FPS')
          .setValue('25'))
    print(EnvVar('CGRU_DAILIES_CODEC')
          .appendPath([os.environ['CGRU_LOCATION'], 'utilities', 'moviemaker', 'codecs', 'h264_good.ffmpeg']))
    print(EnvVar('CGRU_DAILIES_NAMING')
          .setValue('(s)'))
    print(EnvVar('CGRU_DAILIES_SLATE')
          .setValue(''))
    print(EnvVar('CGRU_DAILIES_TEMPLATE')
          .appendPath([os.environ['CGRU_LOCATION'], 'utilities', 'moviemaker', 'templates', 'parovoz_frame_template']))
    print(EnvVar('CGRU_DAILIES_LGSPATH')
          .setValue(' '))
    print(EnvVar('CGRU_DAILIES_LGFPATH')
          .appendPath([os.environ['TOOLS_PATH'], 'logo', 'parovoz_logo.png']))
    print(EnvVar('CGRU_DAILIES_LGFGRAV')
          .setValue('NorthEast'))
    print(EnvVar('CGRU_DAILIES_LGFSIZE')
          .setValue('0'))

    # override with loco:
    pipline.tools_path = orig_tool_path
    print(EnvVar('CGRU_LOCATION')
          .appendPath([pipline.get_cgru_path()]))

    # render task:
    if len(sys.argv) > 1 and OsExtension.is_linux():
        print(EnvVar('DISPLAY').setValue(':0'))
        # fix path to render.py
        sys.argv[3] = os.path.join(pipline.get_cgru_path(), 'plugins', 'nuke', 'render.py')
        # fix path to *.nk
        sys.argv[8] = str(sys.argv[8]).replace('\\', '/')

    print(EnvVar('PATH').setValue(''))

    print('Current machine env: ')
    for key in os.environ.keys():
        print(key + ':' + os.environ[key])

    del sys.path[1:]

    print("finish nukeRender env configuration")

    print("run nukeRender: {0}".format(str(sys.argv[1:])))

    return Engine.run(nuke_exe_path, sys.argv[1:])


if __name__ == "__main__":
    sys.exit(main())
