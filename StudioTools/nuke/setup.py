from __future__ import absolute_import
from distutils.core import setup
import py2exe, sys, os

from helper.EnvManager import *

sys.argv.append('py2exe')

py2exe_options = {
                  'optimize': 2,
                  'excludes': ['_socket', '_ssl'],
                  'compressed': True}

setup(
    name='Nuke',
    options={'py2exe': py2exe_options},
    console=[
        {
            "script": "startnuke.py",
            "icon_resources": [(0, "icon.ico")]
        }
    ],
)