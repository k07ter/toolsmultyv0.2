import sys
import os
from subprocess import Popen


def main():
    app = Popen(sys.argv[1:], env=os.environ)
    app.communicate()


if __name__ == "__main__":
    sys.exit(main())
